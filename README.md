# TaskIT
celery -A taskit_api worker --pool=solo -l info

**TaskIT Python - virtual**

- \TaskIT>pip install virtualenv
- \TaskIT\TaskITAPI>virtualenv taskit_env
- \TaskIT\TaskITAPI>cd taskit_env\scripts
- \TaskIT\TaskITAPI\taskit_env\scripts>activate
- \TaskIT\TaskITAPI\taskit_env\scripts>cd../..
- (taskit_env)\TaskIT\TaskITAPI>pip install -r requirements.txt
- (taskit_env)\TaskIT\TaskITAPI>python manage.py runserver


**TaskIT UI - Steps (Angular)**

- npm -v = 5.6.0
- node --version = v8.11.3
- npm uninstall -g @angular/cli
- npm cache clean --force
- npm install -g @angular/cli@6.0.8
- ng -v = Angular CLI 6.0.8


**Kendo**

- npm i @progress/kendo-angular-grid
- npm i @progress/kendo-data-query
- npm i @progress/kendo-angular-popup
- npm i @progress/kendo-angular-common
- npm i @progress/kendo-angular-l10n
- npm i @progress/kendo-angular-buttons
- npm i @progress/kendo-angular-dateinputs
- npm i @progress/kendo-angular-excel-export
- npm i @progress/kendo-angular-dropdowns
- npm i @progress/kendo-angular-pdf-export
- npm i @progress/kendo-angular-intl
- npm i @progress/kendo-angular-inputs
- npm i @progress/kendo-drawing
- npm i @progress/kendo-theme-default


**Docker**

- docker rmi $(docker images -f dangling=true -q)
- docker-compose -f docker-compose.yml -p taskit build --no-cache
- docker-compose -f docker-compose.yml -p taskit up
- docker-compose -f docker-compose.uat.yml -p taskit-uat build --no-cache
- docker-compose -f docker-compose.uat.yml -p taskit-uat up
- docker-compose -f docker-compose.qa.yml -p taskit-qa build --no-cache
- docker-compose -f docker-compose.qa.yml -p taskit-qa up

**SSL Certificate Key Generation**
- cd /etc/nginx/ssl/ -> openssl genrsa -des3 -out domain.key 2048
- pass phrase - bcspl36020210310


Conflicted on server

git add taskitUI/src/assets/js/main.js && git commit -m "removed merge conflicts"

**SSL Certificate Key Generation**
- cd /etc/nginx/ssl/ -> openssl genrsa -des3 -out domain.key 2048
- pass phrase - bcspl36020210310

*** An unhandled exception occurred: Could not find the implementation for builder @angular-devkit/build-angular:dev-server See ***
* npm uninstall @angular-devkit/build-angular
* npm install @angular-devkit/build-angular
* ng update --all --force

*** ERROR in ./src/polyfills.ts Module not found: Error: Can't resolve 'core-js/es7/reflect' ***
* npm install --save core-js@^2.5.0