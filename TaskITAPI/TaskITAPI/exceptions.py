from rest_framework.views import exception_handler
from rest_framework import exceptions
from rest_framework.response import Response
from rest_framework import status


def get_errors_from_list(errors, key, values) :
    for value in values :
        if 'code' not in value :
            errors = get_errors(value, errors)
            continue
        value['code'] = str(key) + '_' + str(value['code'])
        errors.append(value)
    return errors


def get_errors(data, errors) :
    if isinstance(data, dict) :
        if not 'code' in data :
            for key, values in data.items() :
                if isinstance(values, list):
                    errors = get_errors_from_list(errors, key, values)
                elif isinstance(values, dict) :
                    errors.append(values)
        else :
            errors.append(data)
    elif isinstance(data, list) :
        for value in data :
            errors.append(value)
    return errors



def api_exception_handler(exception, context):
    if isinstance(exception, exceptions.APIException):
        headers = {}
        if getattr(exception, 'auth_header', None):
            headers['WWW-Authenticate'] = exception.auth_header

        if getattr(exception, 'wait', None):
            headers['Retry-After'] = '%d' % exception.wait

        data = exception.get_full_details()
        # set_rollback()
        # if exception.status_code == status.HTTP_400_BAD_REQUEST :
        errors = []
        errors = get_errors(data, errors)
        data = {
            'errors' : errors
        }
        print(data)
        response = {
            'status_code' : exception.status_code,
            **data
        }
        return Response(response, status=exception.status_code, headers=headers)
    return exception_handler(exception, context)