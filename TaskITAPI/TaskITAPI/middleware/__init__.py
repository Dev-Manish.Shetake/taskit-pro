from .exception import ExceptionLogMiddleware
from TaskITAPI.settings import ENCRYPT 

if ENCRYPT :
    from .encryption import EncryptionMiddleware