import base64
import hashlib
from Cryptodome.Cipher import AES
from Cryptodome import Random
from django.utils import timezone
import string
import random
import pytz

BLOCK_SIZE = 16

message = 'Hey,This is a Demo message to encrypt.'
Key = 'ABj4PQgf3j5gblQ0iDp0/Gb07ukQWo0a'

def pad(data):
        padding_char = BLOCK_SIZE - len(data) % BLOCK_SIZE
        return data + (BLOCK_SIZE - len(data) % BLOCK_SIZE) * chr(padding_char)

def unpad(data):
        data = data[:-ord(data[len(data) - 1:])]
        return data

def encrypt():
    message = 'Hey,This is a Demo message to encrypt.'
    key = 'ABj4PQgf3j5gblQ0iDp0/Gb07ukQWo0a'
    # message = message.decode('utf-8')
    IV = Random.new().read(BLOCK_SIZE)
    aes = AES.new(key.encode("utf8"), AES.MODE_CBC, IV)
    message = pad(message)
    encrypted = base64.b64encode(IV + aes.encrypt(message.encode('utf8')))    
    print(encrypted)
    return encrypted

def decrypt():
#     enc = b'6AXjx0gJJR671yBZmIrVhkRBh/YG4l6zpO1CxJEoKc2OkKn5V49IJCiXCaovSCDUC5T4Yh8wbtr904zY3DHjfQ=='
    enc = b'xXNf/8BwkDe4sS0DrqWjdu4KhrgGAa0Uzbo/qtP8ENIQ9lMTc7a78DpUqjdNP3P/kJrX0n/qJ3U14ZYQFwT57c6SA7rlv4YsdaHsRMRp/Wva3uikhQihY3Bk9OM5v4V5pRi42dhdfDcQFNZVCsyQkSZa/a1/Y99cZ4NRQ5W9ObirJIjoq1KqnpEU/dGUHmsqqYiaZDp/X3r39Wdr69Q2hynBQKT9+JvDVzSgzXL1oKNbmVfodDaG9wOgodfhxRKRWuesXznl4RDJNBRMHTiKOv0Jl1H7mstPuW7/8NMDS+1+pFvWWH2f4DCwLBdb4BUyPMq1ed9z80IIbHt3TkXXEi1fymNpQ4FWC6QPQSr8vF4rOH1LcllUC547AdRZMqtqWAxctJ1D8GN++/r89e1YBGemc6rvt3dVbo9OYqi7ehddY7PizipXlqtib8zql8f/af39DNaC2CGBkyKtv+EyLpegn6p4mY0zL1cnydsdrCwQm9vTfAly08psO2rNk7MNeNx1NxRk4zxEuwahkDQrxWqQWBoDtKuuvnAit3yS8Lq2TJ9BjQ5Mxogkjue+pogMDRX0XWkCu1CTGZ/QQvqmwXIMV7NE2IE98IygdVvuGN+NQ0dESMu7e7gb6uLsqnab'
#     key = 'ABj4PQgf3j5gblQ0iDp0/Gb07ukQWo0a'
    key = '9z8mg1wmcgkjl1cq'
    encrypted = base64.b64decode(enc)
    # IV = Random.new().read(BLOCK_SIZE)
    IV = encrypted[:BLOCK_SIZE]
    aes = AES.new(key.encode("utf8"), AES.MODE_CBC, IV)
    decrypted = unpad(aes.decrypt(encrypted[BLOCK_SIZE:]))   
    print(decrypted)
    return decrypted