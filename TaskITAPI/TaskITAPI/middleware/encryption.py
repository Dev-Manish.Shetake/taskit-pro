import base64
import hashlib
from Cryptodome.Cipher import AES
from Cryptodome import Random
from django.utils import timezone
import string
import random
import pytz
from TaskITAPI.urls import url_blacklists
from TaskITAPI.settings import DEBUG, STATIC_URL, MEDIA_URL

BLOCK_SIZE = 16

METHODS_TO_DECRYPT = ['POST', 'PUT', 'PATCH']
METHODS_TO_NOT_ENCRYPT = ['DELETE', 'OPTIONS']
SKIP_CONTENT_TYPES = ['multipart/form-data']
LOGIN_URL = ['/api/v1/auth/login','/api/v1/auth/sign-up','/api/v1/auth/sign-up-register','/api/v1/social_auth/login','/api/v1/users/verify',
            '/api/v1/password/forgot-generate-otp','/api/v1/password/forgot-verify-otp','/api/v1/categories/subcategory-list','/api/v1/gigs']


class EncryptionMiddleware :

    def __init__(self, get_response):
        self.get_response = get_response
        self.key = None

    def pad(self, data):
        padding_char = BLOCK_SIZE - len(data) % BLOCK_SIZE
        return data + (BLOCK_SIZE - len(data) % BLOCK_SIZE) * chr(padding_char).encode('utf-8')

    def unpad(self, data):
        data = data[:-ord(data[len(data) - 1:])]
        return data

    def encrypt(self, key, message):
        # message = message.decode('utf-8')
        IV = Random.new().read(BLOCK_SIZE)
        aes = AES.new(key, AES.MODE_CBC, IV)
        message = self.pad(message)
        return base64.b64encode(IV + aes.encrypt(message))

    def decrypt(self,  key, encrypted):
        encrypted = base64.b64decode(encrypted)
        # IV = Random.new().read(BLOCK_SIZE)
        IV = encrypted[:BLOCK_SIZE]
        aes = AES.new(key, AES.MODE_CBC, IV)
        pad = self.unpad(aes.decrypt(encrypted[BLOCK_SIZE:]))
        return pad


    def process_request(self, request) :
        request_body = getattr(request, '_body', request.body)
        # request_body = b'9z8mg1wmcgkjl1cqzlZL4QR1hXJZiZV7Oeaf+/UXSm7ZyzgvF04fDxR788Tr9o4CNlA9wGtcHIaUPp4CKrMXU2mcPxybT5Fq5+6n4eUZ3yGh0jHevu8jYqxmcVc='
      
        if (request.path in LOGIN_URL):
            self.key = request_body[:16]
            request_body = request_body[16:] 
        else :
            # token = request.META.get('HTTP_AUTHORIZATION').split(' ')[1]
            token = request.META.get('HTTP_AUTHORIZATION')
            if token == None or len(token) < 0 :
                pass
            self.key = token.split('.')[1][:16]
            # self.key = token[:16]
        # print(self.key)    
        decrypted_data = self.decrypt(self.key, request_body)
        return decrypted_data


    def process_response(self, request, response) :
        if request.path in url_blacklists or  request.method in METHODS_TO_NOT_ENCRYPT :
            return response
    
        if DEBUG and (request.path.startswith(MEDIA_URL) or request.path.startswith(STATIC_URL)) :
            return response
           
        if request.path not in LOGIN_URL :
            token = request.META.get('HTTP_AUTHORIZATION')
            if token != None and len(token)>0 :
                token = token.split(' ')[1]
                self.key = token.split('.')[1][:16]
        else :
            if request.GET.get('str_code') != None :
                self.key = bytes(request.GET.get('str_code'), encoding='utf8')

        encrypted_body = self.encrypt(self.key, response.content)       
        response.content = encrypted_body       
        return response


    def __call__(self, request):
        if not request.path in url_blacklists and request.method in METHODS_TO_DECRYPT and request.content_type not in SKIP_CONTENT_TYPES :        
            request._body = self.process_request(request)
        response = self.get_response(request)
        return self.process_response(request, response)