from __future__ import unicode_literals

from django.utils import timezone
import pytz
from django.http import JsonResponse
import sys
import traceback
from django.utils.deprecation import MiddlewareMixin
from rest_framework import status
from TaskITAPI.settings import DEBUG
import json
from settings.models import ExceptionLog


def save_exception_log(data):
    ExceptionLog.objects.create(**data)

class ExceptionLogMiddleware(MiddlewareMixin):
    """
    This middleware provides logging of exception in requests.
    """

    def process_exception(self, request, exception):
        """
        Processes exceptions during handling of a http request.
        Logs them with *ERROR* level.
        """
        ALLOWED_ORIGIN = ['127.0.0.1:4200', 'localhost:4200']
        origin = ""
        if 'HTTP_REFERER' in request.META :
            origin = request.META['HTTP_REFERER'].split('/')[2]

        if DEBUG and request.META['REMOTE_ADDR'] == '127.0.0.1' and origin not in ALLOWED_ORIGIN:
            return None

        _, _, stacktrace = sys.exc_info()
        data = {
            "user_id": request.user.pk,
            "request_method": request.method,
            "request_path": request.get_full_path(),
            "remote_address": request.META['REMOTE_ADDR'],
            "traceback": ''.join(traceback.format_tb(stacktrace)),
            "exception": str(exception)
        }
        save_exception_log(data)

        if origin in ALLOWED_ORIGIN :
            return None

        if DEBUG :
            return JsonResponse(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else :
            response = {
                "status": status.HTTP_500_INTERNAL_SERVER_ERROR,
                "message": "Internal Server Error."
            }
            return JsonResponse(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

