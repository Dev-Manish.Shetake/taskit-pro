from rest_framework.pagination import PageNumberPagination
from django.utils.translation import gettext as _
from TaskITAPI.settings import DEFAULT_PAGE_SIZE,BASE_URL


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 15
    page_size_query_param = 'page_size'
    max_page_size = 100
    invalid_page_message = _('No more data.')


# Paginating Functions 
def get_filtered_url(self, url) :
    filters = self.request.GET
    parameters = filters.keys()
    values = filters.values()
    for key in parameters :
        if key == 'page' or key == 'page_size' :
            continue
        url = url + "&" + str(key) + "=" + str(filters[key])
    return url 


def get_paginated_response(self, count_queryset) :
    page_size = self.request.GET.get('page_size')
    page = self.request.GET.get('page')
    try :
        page_size = int(page_size) if page_size else DEFAULT_PAGE_SIZE
    except:    
        page_size = DEFAULT_PAGE_SIZE

    try :             
        page = int(page) if page else 1     
    except :
        page = 1           

    count = count_queryset.count()    
    # url = self.request.build_absolute_uri(self.request.path)
    url =  BASE_URL + self.request.path    
    previous = None
    next = None         
    if count > int(page_size) :
        next = url + "?page_size=" + str(page_size) + "&page=" + str(page + 1)
        next = get_filtered_url(self,next)
    if int(page) > 1 :
        previous = url + "?page_size=" + str(page_size) + "&page=" + str(page - 1)   
        previous = get_filtered_url(self,previous)

    res = {
        'count' : count,
        'previous' : previous,
        'next' : next
    }    
    return page, page_size, res
