from rest_framework.serializers import ModelSerializer
from django.urls import reverse


class StoredProcedureSerializer :
    
    def __init__(self, cursor, result_set=[], many=None) :
        self.cursor = cursor
        self.result_set = result_set
        self.many = many
        self.data = self.serialize(cursor)

    def convert_to_json(self, cursor, result, cnt) :
        _result = cursor.fetchall()
        if len(_result) == 1 :
            response = dict()
            keys = [x[0] for x in cursor.description] #this will extract row headers
            for key, value in zip(keys, _result[0]) :
                response[key] = value
        elif len(_result) > 1 :
            response = []
            keys =[x[0] for x in cursor.description] #this will extract row headers
            for res in _result :
                response.append(dict(zip(keys,res)))
        else : 
            return result
        result["result"+str(cnt)] = response
        return result

    def replace_keys(self, result) :
        k = not result
        if not result :
            for keys in self.result_set :
                result[keys[1]] = []
        else :
            for keys in self.result_set :
                if keys[0] in result :
                    result[keys[1]] = result.pop(keys[0])
        return result

    def serialize(self, cursor) :
        result = dict()
        cnt = 1
        result = self.convert_to_json(cursor, result, cnt)
        while(cursor.nextset()) :
            cnt = cnt + 1
            result = self.convert_to_json(cursor, result, cnt)
        cursor.close()
        keys = list(result.keys())
        if len(keys) == 1 and self.many != False:
            key = keys[0]
            result = result[key]
        result = self.replace_keys(result)

        if self.many == True and type(result) == dict :
            if not result :
                result = []
            else :
                result = [result]
        return result
