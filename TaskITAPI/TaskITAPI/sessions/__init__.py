from django.contrib.auth import authenticate, login
from rest_framework.authentication import SessionAuthentication


class CustomSessionAuthentication(SessionAuthentication):
    """
    Use Django's session framework for authentication.
    """

    def authenticate(self, request):
        """
        Returns a `User` if the request session currently has a logged in user.
        Otherwise returns `None`.
        """

        # Get the session-based user from the underlying HttpRequest object
        user = getattr(request._request, 'user', None)
        # Unauthenticated, CSRF validation not required
    
        if not user or not user.is_active:
            return None
        groups = user.groups.all()
        user.group_id = None
        if len(groups) > 0 :
            user.group_id = groups[0].id
        self.enforce_csrf(request)

        # CSRF passed with authenticated user
        return (user, None)


##### Session Authentication #####
def _login(request):
    from django.http import HttpResponse
    from django.shortcuts import redirect
    if request.method == 'POST':
        email = request.POST['email_address']
        password = request.POST['password']
        user = authenticate(request, email=email, password=password)
        if user is not None:
            # request.session.set_expiry(86400) #sets the exp. value of the session
            login(request, user)  #the user is now logged in
        return redirect('/api/v1/docs')
    else:
        from django.shortcuts import render
        return render(request, 'accounts/login.html')


def _logout(request):
    from django.shortcuts import redirect
    request.session.flush()
    return redirect('/api/v1/docs')
