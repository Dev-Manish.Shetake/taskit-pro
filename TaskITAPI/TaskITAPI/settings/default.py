import os 
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

BASE_URL = 'http://localhost:4200s'
RESET_LINK = "http://localhost:4200/reset?code="
VERIFICATION_LINK = 'http://localhost:4200/verify?code='
SUPPORT_MAIL = 'helpdesk@task-it.in'
SUPPORT_CALL = '+91 (022) 1234 5678'

ENCRYPT = False

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    # 'default': {
    #     'ENGINE': 'django.db.backends.sqlite3',
    #     'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    # },
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'taskit',
        'USER': 'root',
        'PASSWORD': 'pass123#',
        'HOST': '192.168.3.131',
        'PORT': '3306'
    }, 
    # 'default': {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': 'taskit_qa',
    #     'USER': 'root',
    #     'PASSWORD': 'pass123#',
    #     'HOST': '192.168.3.131',
    #     'PORT': '3306'
    # },    
}

RZP_CONFIG = {
        "key_id": "rzp_test_x6sLt3oki5sF9s",
        "key_secret": "TCHmzrnzs5FZZBYFu8hWSk5U",
        "mode": "test"       
  }

#### Caches #####
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': '.cache',
        'TIMEOUT': 300,
    }
}


CELERY_BROKER_URL = 'redis://127.0.0.1:6379'
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_ACCEPT_CONTENT = ['pickle']