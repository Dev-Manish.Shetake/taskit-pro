import os 
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
# BASE_URL = 'http://sw_apps.benchmarksolution.com'
# RESET_LINK = "http://sw_apps.benchmarksolution.com/reset?code="
# VERIFICATION_LINK = 'http://sw_apps.benchmarksolution.com/verify?code='
BASE_URL = 'http://49.248.119.130:3002/'
RESET_LINK = "http://49.248.119.130:3002/reset?code="
VERIFICATION_LINK = 'http://49.248.119.130:3002/verify?code='
# BASE_URL = 'http://192.168.3.221:3001'
# RESET_LINK = "http://192.168.3.221:3001/reset?code="
# VERIFICATION_LINK = 'http://192.168.3.221:3001/verify?code='
SUPPORT_MAIL = 'helpdesk@task-it.in'
SUPPORT_CALL = '+91 (022) 1234 5678'

ENCRYPT = False

# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    },
    # 'default': {
    #     'ENGINE': 'django.db.backends.mysql',
    #     'NAME': 'taskit',
    #     'USER': 'root',
    #     'PASSWORD': 'pass123#',
    #     'HOST': '192.168.3.131',
    #     'PORT': '3306'
    # }  
   }

RZP_CONFIG = {
        "key_id": "rzp_test_x6sLt3oki5sF9s",
        "key_secret": "TCHmzrnzs5FZZBYFu8hWSk5U",
        "mode": "test"       
  }

PAYU_CONFIG = {
        "merchant_key": "XpaABz5t",
        "merchant_salt": "wmZLu9DL9d",
        "mode": "test",
        "success_url" : "http://127.0.0.1:8000/api/v1/payments/success",
        "failure_url" : "http://127.0.0.1:8000/api/v1/payments/failure",
        "auth_header" : "HOsLbv38FpvFSO/olcItcPbtkIroGXJ66ZWGEv7wAT0="
  }

#### Caches #####
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': 'redis://redis:6379/1',
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient"
        },
        'TIMEOUT': 300,
        'OPTIONS': {
            'MAX_ENTRIES': 1000000
        } 
    }
}

CELERY_BROKER_URL = 'redis://redis:6379'
CELERY_ACCEPT_CONTENT = ['pickle']
CELERY_TASK_SERIALIZER = 'pickle'
CELERY_ACCEPT_CONTENT = ['pickle']