"""TaskitAPI URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path,include
from django.conf.urls import url
from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework_swagger.views import get_swagger_view
from TaskITAPI.sessions import _login,_logout
'''
Swagger schema generators 
'''
schema_view = get_swagger_view(title='TaskIT API')

schema_view1 = get_schema_view(
    openapi.Info(
        title="TaskIT API Documentation",
        default_version='v1',
    ),
    public=False,
    permission_classes=[],
)


'''
Index page to redirect on swagger
'''
def index(request):
    from django.shortcuts import redirect
    return redirect('/api/v1/redocs')

API_PATHS = [
    path('auth/',include('jwtauth.urls')),
    path('user/',include('accounts.urls.user')),
    path('groups/', include('accounts.urls.groups')),
    path('password/', include('accounts.urls.password')),
    path('social_auth/',include('social_auth.urls')),
    # Masters API
    path('categories/',include('masters.urls.category')),
    path('subcategories/',include('masters.urls.sub_category')),
    path('subcategories_metadata/',include('masters.urls.sub_category_metadata')),
    path('subcategories_price_scope/',include('masters.urls.sub_category_price_scope')),
    path('metadata_types/',include('masters.urls.metadata')),
    path('extra_services/',include('masters.urls.extra_service_price')),
    path('service_delivery/',include('masters.urls.service_delivery')),
    path('service_level/',include('accounts.urls.service_level')),
    path('user_type/',include('accounts.urls.user_type')),    
    path('request_status/',include('masters.urls.request_status')),
    # Configs API
    path('languages/',include('configs.urls.language')),
    path('occupations/',include('configs.urls.occupation')),
    path('skills/',include('configs.urls.skill')),
    path('country/',include('configs.urls.country')),
    path('state/',include('configs.urls.state')),
    path('city/',include('configs.urls.city')),
    path('education/',include('configs.urls.education')),
    path('social_media/',include('configs.urls.social_media')),
    path('price_type/',include('configs.urls.price_type')),
    path('order_status/',include('configs.urls.order_status')),
    path('gig_status/',include('configs.urls.gig_status')),
    path('gig_gallery_type/',include('configs.urls.gig_gallery_type')),
    path('order_payment_status/',include('configs.urls.order_payment_status')),
    path('order_cancel_reason/',include('configs.urls.order_cancel_reason')),
    path('deactivation_reason/',include('configs.urls.deactivate_reason')),
    path('voucher_type/',include('configs.urls.voucher_type')),
    path('refund_status/',include('configs.urls.refund_status')),
    # Transational API
    path('settings/',include('settings.urls')),
    path('gigs/',include('gigs.urls.gig')),
    path('gigs_extra/',include('gigs.urls.gigs_ext')),
    path('home_screen/',include('gigs.urls.home_screen')),
    path('sellers/',include('sellers.urls')),
    path('buyers/',include('buyers.urls')),  
    path('requests/',include('_requests.urls')),
    path('orders/',include('orders.urls.orders')),
    path('orders_wallet/',include('orders.urls.order_wallet')),
    path('orders_chat/',include('orders.urls.order_chat')),
    path('notifications/',include('notifications.urls.notifications')), 
    path('app_notifications/',include('notifications.urls.app_notifications')),   
    path('dashboard/',include('dashboard.urls')),  
    path('payments/',include('payments.urls')),  
]


SWAGGER_PATHS = []
if settings.DEBUG :
    SWAGGER_PATHS = [
        path('docs/', schema_view),
        path('redocs/',
            schema_view1.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui'
        )
    ]

if settings.DEBUG :
    API_PATHS = [*SWAGGER_PATHS, *API_PATHS]

urlpatterns = [
    path('', index, name='session-login'),
    path('accounts/login/', _login, name='session-login'),
    path('accounts/logout/', _logout, name='session-logout'),
    path('api/v1/', include(API_PATHS))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
        path('api/admin/', admin.site.urls)
    ] + urlpatterns

url_blacklists = [
    '/api/v1/redocs/'
]