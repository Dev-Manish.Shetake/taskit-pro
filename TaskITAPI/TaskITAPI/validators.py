from django.core.validators import RegexValidator
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
import re
import requests
import json

def validate_education_year(value):
    if len(str(value)) != 4 and str(value).isnumeric():
        raise ValidationError(_('Education Year number should contain exact 4 digit.'),code='invalid',params={'value': value},)                   
     
'''
validate mobile number with length and characters
'''
def validate_mobile_no(value):
    if len(str(value)) != 10 and not str(value).isnumeric():
        raise ValidationError(_('Mobile number should contain exact 10 digits.'),code='invalid',params={'value': value})
    return value     