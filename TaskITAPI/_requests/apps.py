from django.apps import AppConfig


class RequestsConfig(AppConfig):
    name = '_requests'
