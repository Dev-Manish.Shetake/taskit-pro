# Generated by Django 2.2 on 2021-04-26 12:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('_requests', '0016_servicerequest_reject_reason'),
    ]

    operations = [
        migrations.RenameField(
            model_name='servicerequest',
            old_name='reject_reason',
            new_name='reject_comment',
        ),
    ]
