from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from configs.models import OrderStatus,OrderCancelReason,City
from masters.models import Category,SubCategory,ServiceDelivery,MetaDataTypeDetails,SubCategoryMetaDataType,RequestStatus
from orders.models import Order
from gigs.models import Gig
from masters.models import SubCategoryPriceScope,SubCategoryPriceScopeDetails
from configs.models import PriceType



def gig_request_attachment_path(instance, filename):
    req = ServiceRequest.objects.last()    
    if req :
        count = req.id + 1
    else :
        count = 1 
    return 'Request/Attachments/FilePath/{0}_{1}'.format(  
        count,
        filename
        )

# Master Request related data stored in this table
class ServiceRequest(SoftDeleteModel) :       
    description = models.CharField(max_length=2500)   
    category = models.ForeignKey(Category,on_delete=models.DO_NOTHING)
    sub_category = models.ForeignKey(SubCategory,on_delete=models.DO_NOTHING)
    service_delivery = models.ForeignKey(ServiceDelivery,on_delete=models.DO_NOTHING,null=True)
    service_delivery_other = models.IntegerField(null=True)   
    budget = models.DecimalField(max_digits=9,decimal_places=2)
    order =  models.ForeignKey(Order,on_delete=models.DO_NOTHING,null=True)
    request_status =  models.ForeignKey(RequestStatus,on_delete=models.DO_NOTHING)
    seller_offer_count = models.IntegerField(null=True)
    reject_comment = models.CharField(max_length=500,null=True,blank=True)
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='request_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='request_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='request_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'request'
        default_permissions = ()


# Master Request Details related data stored in this table
class RequestDetails(SoftDeleteModel) :          
    request = models.ForeignKey(ServiceRequest,on_delete=models.DO_NOTHING)
    sub_category_metadata_type = models.ForeignKey(SubCategoryMetaDataType,on_delete=models.DO_NOTHING)
    metadata_type_dtl = models.ForeignKey(MetaDataTypeDetails,on_delete=models.DO_NOTHING)

    class Meta :
        db_table = 'request_dtl'
        default_permissions = ()


# Master Request Attachment related data stored in this table
class RequestAttachment(SoftDeleteModel) :          
    request = models.ForeignKey(ServiceRequest,on_delete=models.DO_NOTHING)
    file_path = models.FileField(upload_to=gig_request_attachment_path,null=True)

    class Meta :
        db_table = 'request_attachment'
        default_permissions = ()


# Master Request Offer related data stored in this table
class RequestOffer(SoftDeleteModel) :       
    seller_offer_description = models.CharField(max_length=1500,null=True)   
    request = models.ForeignKey(ServiceRequest,on_delete=models.DO_NOTHING)
    buyer = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='request_offer_buyer_id')
    seller = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='request_seller_buyer_id')
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    price_type = models.ForeignKey(PriceType,on_delete=models.DO_NOTHING,null=True)
    offer_price = models.DecimalField(max_digits=9,decimal_places=2,null=True)
    offer_delivery_time = models.IntegerField(null=True)
    is_sent_offer = models.BooleanField(default=False)
    is_system_offer = models.BooleanField(default=False)
    notify_date = models.DateTimeField(null=True)
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='request_seller_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='request_seller_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='request_seller_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'request_offer'
        default_permissions = ()


# Master Request offer details related data stored in this table
class RequestOfferDetail(models.Model) :          
    request_offer = models.ForeignKey(RequestOffer,on_delete=models.DO_NOTHING)
    sub_category_price_scope =  models.ForeignKey(SubCategoryPriceScope,on_delete=models.DO_NOTHING)
    sub_category_price_scope_dtl = models.ForeignKey(SubCategoryPriceScopeDetails,on_delete=models.DO_NOTHING)
    value = models.CharField(max_length=200)
    class Meta :
        db_table = 'request_offer_dtl'
        default_permissions = ()