from rest_framework import serializers
from django.db import transaction
from django.db.models import Prefetch
from django.utils import timezone
from rest_framework.exceptions import ValidationError,PermissionDenied
from django.utils.translation import gettext as _
from _requests.models import ServiceRequest,RequestDetails,RequestAttachment
from masters.serializers import CategoryMinSerializer,CategoryMacroSerializer,SubCategoryMinSerializer,\
    SubCategoryMacroSerializer,RequestStatusMinSerializer,ServiceDeliverySerializer,ServiceDeliveryMinSerializer
from masters.values import REQUEST_STATUS


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/requests/list')
'''
class ServiceRequestListSerializer(serializers.ModelSerializer) :   
    sub_category = SubCategoryMinSerializer(read_only=True)    
    category = CategoryMinSerializer(read_only=True)    
    service_delivery = ServiceDeliverySerializer()
    request_status = RequestStatusMinSerializer(read_only=True)
   
    def setup_eager_loading(self,queryset) :        
        return queryset.order_by('-created_date')

    class Meta :
        model = ServiceRequest
        fields = [
            'id',
            'description',
            'category',
            'sub_category',
            'service_delivery',
            'service_delivery_other',
            'budget',
            'request_status',
            'seller_offer_count',
            'reject_comment',
            'created_by',
            'created_user',
            'created_date'            
        ]


class ServiceRequestMinSerializer(serializers.ModelSerializer) : 
    sub_category = SubCategoryMinSerializer(read_only=True)    
    service_delivery = ServiceDeliverySerializer()
    
    class Meta :
        model = ServiceRequest
        fields = [
            'id',
            'description',
            'seller_offer_count',
            'sub_category',
            'service_delivery',
            'service_delivery_other',
            'budget'
        ]


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/requests/counts')
'''
class RequestCountSerializer(serializers.Serializer) :
    total_active = serializers.IntegerField()
    total_pending = serializers.IntegerField() 
    total_unapproved = serializers.IntegerField()
           
        
class RequestDetailsSerializer(serializers.ModelSerializer) :
    sub_category_metadata_type_id = serializers.IntegerField()
    metadata_type_dtl_id = serializers.IntegerField()

    class Meta :
        model = RequestDetails
        fields =[
            'id',
            'sub_category_metadata_type_id',
            'metadata_type_dtl_id',  
        ]


class RequestAttachmentSerializer(serializers.ModelSerializer) :    

    class Meta :
        model = RequestAttachment
        fields =[
            'id',
            'file_path',            
        ]


class ServiceRequestSerializer(serializers.ModelSerializer) :
    category_id = serializers.IntegerField()
    sub_category_id = serializers.IntegerField()
    service_delivery_id = serializers.IntegerField(allow_null=True)
    details = RequestDetailsSerializer(many=True)
    # attachments = RequestAttachmentSerializer(many=True)

    class Meta :
        model = ServiceRequest
        fields =[
            'id',
            'description',
            'category_id',        
            'sub_category_id',
            'service_delivery_id', 
            'service_delivery_other',
            'budget',
            'details',
            # 'attachments'   
        ]


class ServiceRequestDetailserializer(serializers.ModelSerializer) :
    category_id = serializers.IntegerField(write_only=True)
    category = CategoryMacroSerializer(read_only=True)
    sub_category_id = serializers.IntegerField(write_only=True)
    sub_category = SubCategoryMacroSerializer(read_only=True)
    service_delivery_id = serializers.IntegerField(write_only=True)
    service_delivery = ServiceDeliveryMinSerializer(read_only=True)
    request_status = RequestStatusMinSerializer(read_only=True)
    # details = RequestDetailsSerializer(many=True,write_only=True)
    attachments = RequestAttachmentSerializer(many=True,read_only=True)

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('requestattachment_set',RequestAttachment.objects.all(),to_attr='attachments')
        )
        return queryset
    class Meta :
        model = ServiceRequest
        fields =[
            'id',
            'description',
            'category_id',
            'category',  
            'sub_category_id',      
            'sub_category',
            'service_delivery_id',
            'service_delivery', 
            'service_delivery_other',
            'budget',
            'request_status',
            # 'details',
            'attachments',
            'created_by',
            'created_user',
            'created_date'
        ]

        read_only_fields = ['created_by','created_user','created_date']

    def update(self,service_request,validated_data) :
        if service_request.request_status_id != REQUEST_STATUS['Unapproved'] :
            raise PermissionDenied
        update_fields = []
        for attr, value in validated_data.items():
            setattr(service_request, attr, value)
            update_fields.append(attr)
        service_request.request_status_id = REQUEST_STATUS['Pending']     
        service_request.modified_by = self.context['request'].user
        service_request.modified_user = self.context['request'].user.user_name
        service_request.modified_date = timezone.now()
        update_fields = update_fields + ['request_status_id','modified_by','modified_user','modified_date']
        service_request.save(update_fields=update_fields)
        return service_request    

