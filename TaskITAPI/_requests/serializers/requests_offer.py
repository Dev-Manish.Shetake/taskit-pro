from rest_framework import serializers
from _requests.models import ServiceRequest,RequestOffer,RequestOfferDetail,RequestAttachment
from _requests.serializers import ServiceRequestMinSerializer,RequestAttachmentSerializer,ServiceRequestMinSerializer
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.db import transaction
from django.db.models import Prefetch
from django.utils import timezone
from accounts.serializers import BuyerInfoMinSerializer,SellerInfoMinSerializer
from masters.serializers import ServiceDeliverySerializer
from gigs.serializers import SellerRequestOfferGigListSerializer,SellerRequestOfferDetailsGigSerializer
from orders.serializers import OrderGigPriceSerializer,OrderSubCategoryPriceDetailsSerializer      
from gigs.models import Gig,GigGallery,GigPrice,GigSubCategoryPriceDetails
from gigs.serializers import GigsGalleryMinSerializer
from configs.values import PRICE_TYPE,GALLERY_TYPE
from gigs.serializers.gigs_pricing import GigPriceSerializer,GigSubCategoryPriceDetailsMinSerializer
from django.db.models import OuterRef,Subquery


'''
Serializer to send/receive data in specific format for POST/GET method

'''
class RequestOfferDetailsSerializer(serializers.ModelSerializer) :  
    sub_category_price_scope_id = serializers.IntegerField(write_only=True)
    sub_category_price_scope_dtl_id = serializers.IntegerField(write_only=True)
   
    class Meta :
        model = RequestOfferDetail
        fields = [
            'id',
            'sub_category_price_scope_id',         
            'sub_category_price_scope_dtl_id',
            'value'
        ]


'''
Serializer to send/receive data in specific format for POST/GET method

'''
class RequestOfferDetailsMinSerializer(serializers.ModelSerializer) :  
  
    class Meta :
        model = RequestOfferDetail
        fields = [
            'id',
            'sub_category_price_scope_id',         
            'sub_category_price_scope_dtl_id',
            'value'
        ]

class RequestOfferDetailsUpdateSerializer(serializers.ModelSerializer) :  
    id = serializers.IntegerField()
    sub_category_price_scope_id = serializers.IntegerField(write_only=True)
    sub_category_price_scope_dtl_id = serializers.IntegerField(write_only=True)
   
    class Meta :
        model = RequestOfferDetail
        fields = [
            'id',
            'sub_category_price_scope_id',         
            'sub_category_price_scope_dtl_id',          
            'value'
        ]


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/requests/seller-list/')
'''
class RequestOfferListSerializer(serializers.ModelSerializer) :  
    buyer = BuyerInfoMinSerializer()
    # seller = SellerInfoMinSerializer() 
    request = ServiceRequestMinSerializer()
      
    def setup_eager_loading(self,queryset) :           
        queryset = BuyerInfoMinSerializer.setup_eager_loading(self,queryset,'buyer__')  
        # queryset = SellerInfoMinSerializer.setup_eager_loading(self,queryset,'seller__')  
        return queryset.order_by('-created_date')

    class Meta :
        model = RequestOffer
        fields = [
            'id',
            'request',
            'buyer',
            # 'seller',
            'gig',
            'is_sent_offer',
            'is_system_offer',          
            'created_by',
            'created_user',
            'created_date'            
        ]       


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/requests/seller-list/')
'''
class RequestOfferDetailsGETSerializer(serializers.ModelSerializer) :
    request = ServiceRequestMinSerializer()
    gig = SellerRequestOfferDetailsGigSerializer()
    request_offer_details = RequestOfferDetailsMinSerializer(many=True)

    def setup_eager_loading(self,queryset) :  
        queryset = queryset.prefetch_related(
            Prefetch('requestofferdetail_set',RequestOfferDetail.objects.all(),to_attr='request_offer_details')
        )
        queryset = SellerRequestOfferDetailsGigSerializer.setup_eager_loading(self,queryset,'gig__') 
        return queryset

    class Meta :
        model = RequestOffer
        fields = [
            'id',
            'seller_offer_description',
            'request',  
            'gig',
            # 'price_type',
            'offer_price',
            'offer_delivery_time',
            'is_sent_offer',
            'is_system_offer',    
            'request_offer_details',      
            'created_by',
            'created_user',
            'created_date'            
        ]  



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/requests/seller-list/')
'''
class RequestSendOfferSerializer(serializers.Serializer) :  
    seller_offer_description = serializers.CharField(max_length=1500,allow_null=True)
    gig_id = serializers.IntegerField()
    price_type_id = serializers.IntegerField()
    offer_price = serializers.DecimalField(max_digits=9,decimal_places=2,allow_null=True)
    offer_delivery_time = serializers.IntegerField(allow_null=True)
    details = RequestOfferDetailsSerializer(many=True)


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/request/seller-request-offers/:id')
'''
class RequestOfferUpdateSerializer(serializers.ModelSerializer) :
    details = RequestOfferDetailsUpdateSerializer(many=True,required=False)

    class Meta :
        model = RequestOffer
        fields = [
            'id',
            'seller_offer_description',          
            'offer_price',
            'offer_delivery_time',
            'details'            
        ]    

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/requests/seller-list/')
'''
class SellerRequestInfoSerializer(serializers.ModelSerializer) :
    created_by = BuyerInfoMinSerializer()
    service_delivery = ServiceDeliverySerializer()   
    request_attachment = RequestAttachmentSerializer(many=True)

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('requestattachment_set',RequestAttachment.objects.all(),to_attr='request_attachment')
        )
        queryset = BuyerInfoMinSerializer.setup_eager_loading(self,queryset,'created_by__')
        return queryset

    class Meta :
        model = ServiceRequest
        fields = [
            'id',
            'description',
            'service_delivery',
            'service_delivery_other',
            'budget',
            'seller_offer_count',
            'created_by',
            'request_attachment'
        ] 
   
'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/requests/seller-list/')
'''
class SellerRequestOfferListSerializer(serializers.ModelSerializer) :
    request_offer_details = RequestOfferDetailsSerializer(many=True)
    gig = SellerRequestOfferGigListSerializer()
   
    def setup_eager_loading(self,queryset) :             
        queryset = queryset.prefetch_related(
            Prefetch('requestofferdetail_set',RequestOfferDetail.objects.all(),to_attr='request_offer_details')
        )
        queryset = SellerRequestOfferGigListSerializer.setup_eager_loading(self,queryset,'gig__')        
        return queryset
        
    class Meta :
        model = RequestOffer
        fields = [
            'id',
            'gig', 
            'offer_price',
            'offer_delivery_time',
            'request_offer_details',     
            'created_date',
        ]

  

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/requests/seller-list/')
'''
class RequestOfferOrderCreateSerializer(serializers.Serializer) :
    request_offer_id = serializers.IntegerField()   
    gig_id = serializers.IntegerField()
    amount = serializers.DecimalField(max_digits=9,decimal_places=2)
    delivery_time = serializers.IntegerField()
    order_gig_price = OrderGigPriceSerializer()
    sub_category_price_details = OrderSubCategoryPriceDetailsSerializer(many=True)

    def validate_request_offer_id(self,value) :
        if not RequestOffer.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for request offer id.'),code='invalid_input')
        return value

    def validate_gig_id(self,value) :
        if not Gig.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for gig id.'),code='invalid_input')
        return value 


   
'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/requests/seller-gig-list/:sub_category_id')
'''
class SellerGigListtSerializer(serializers.ModelSerializer) :
    gallery = GigsGalleryMinSerializer(many=True)
    gigs_pricing = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('giggallery_set',GigGallery.objects.filter(gig_gallery_type_id=GALLERY_TYPE['Photo']),to_attr='gallery'),
        )
        return queryset
        
    class Meta :
        model = Gig
        fields = [
            'id',
            'title', 
            'description',
            'gallery',     
            'gigs_pricing'     
        ]    

    def get_gigs_pricing(self,instance) :    
        gigs_pricing = []   
        gig_price_details = []
        gig_price = GigPrice.objects.filter(gig_id=instance.id)
        for gig in gig_price :
            price = GigPriceSerializer(gig).data
            gig_price_dtl = GigSubCategoryPriceDetails.objects.filter(gig_price=gig,sub_category_price_scope__price_scope_name__in=['Delivery Time','Max Revisions Allowed'])
            for dtl in gig_price_dtl :
                gig_price_details.append(GigSubCategoryPriceDetailsMinSerializer(dtl).data)
            res = {
                'gig_price' : price,
                'gig_price_dtl' : gig_price_details
            }   
            gigs_pricing.append(res)
            gig_price_details = []
        return gigs_pricing    