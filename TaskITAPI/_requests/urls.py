from django.urls import include,path
from _requests.views import ServiceRequestListView,ServiceRequestCreateView,ServiceRequestAttachmentsView,ServiceRequestDetailsView,ServiceRequestApproveView,\
    ServiceRequestRejectView,RequestOfferListView,RequestOfferUpdateView,RequestOfferSendView,RequestOfferDestroyView,ServiceRequestCountsView,SellerRequesInfoView,SellerRequestOfferListView,\
        RequestOfferOrderCreateView,SellerGigListView,ServiceRequestAttachmentsDeleteView,RequestOfferDetailsGETView

urlpatterns = [
    path('list',ServiceRequestListView.as_view()),
    path('counts',ServiceRequestCountsView.as_view()),
    path('create',ServiceRequestCreateView.as_view()),
    path('attachments/<int:id>',ServiceRequestAttachmentsView.as_view()),
    path('attachments-delete/<int:pk>',ServiceRequestAttachmentsDeleteView.as_view()),
    path('<int:pk>',ServiceRequestDetailsView.as_view()),
    path('approve/<int:id>',ServiceRequestApproveView.as_view()),
    path('reject/<int:id>',ServiceRequestRejectView.as_view()),
    path('seller-request-offers',RequestOfferListView.as_view()),
    path('seller-request-offers/<int:pk>',RequestOfferDetailsGETView.as_view()),
    path('seller-request-offers-update/<int:id>',RequestOfferUpdateView.as_view()),
    path('seller-gig-list/<int:sub_category_id>',SellerGigListView.as_view()),
    path('send-offer/<int:id>',RequestOfferSendView.as_view()),
    path('offer-delete/<int:pk>',RequestOfferDestroyView.as_view()),
    path('seller-request-offer-info/<int:pk>',SellerRequesInfoView.as_view()),
    path('seller-request-offer-list/<int:request_id>',SellerRequestOfferListView.as_view()),
    path('request-offer-order-create',RequestOfferOrderCreateView.as_view()),
]