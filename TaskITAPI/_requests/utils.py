from _requests.models import RequestOffer
from django.utils import timezone
from accounts.values import USER_TYPE
from rest_framework.exceptions import PermissionDenied
from accounts.models import User

def get_user_data(request, queryset) :
    _user = User.objects.get(id=request.user.id)
    if _user.user_type_id == USER_TYPE['back_office'] :
        return queryset   
    elif _user.user_type_id == USER_TYPE['buyer']:
        queryset = queryset.filter(buyer=_user) 
    elif _user.user_type_id == USER_TYPE['seller']:
        queryset = queryset.filter(seller=_user)        
    else :
        raise PermissionDenied
    return queryset