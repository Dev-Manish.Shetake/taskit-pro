from rest_framework.generics import GenericAPIView,ListAPIView,CreateAPIView,RetrieveDestroyAPIView,RetrieveUpdateDestroyAPIView,DestroyAPIView
from rest_framework import serializers
from accounts.models import User
from _requests.models import ServiceRequest,RequestDetails,RequestAttachment
from django.db import transaction
from rest_framework.exceptions import NotFound
from rest_framework import status
from rest_framework.response import Response
from _requests.serializers import ServiceRequestSerializer,RequestAttachmentSerializer,ServiceRequestDetailserializer,RequestCountSerializer
from masters.values import REQUEST_STATUS
from django.utils import timezone
from rest_framework.exceptions import PermissionDenied
from accounts.values import USER_TYPE
from _requests.serializers import ServiceRequestListSerializer
from django.db.models import Q
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db import connection
from notifications.tasks import notify_user
from notifications import events


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/requests/list')
'''
class ServiceRequestListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. status_id = To filter by request Status 
    2. search = Filter by Category/SubCategory
    3. budget = Filter by Budget
    4. service_delivery_name = Filter by service delivery name
    5. buyer_name = Filter by buyer name

    E.g. http://127.0.0.1:8000/api/v1/requests/list?status_id=&search=&budget=   

    """
    serializer_class = ServiceRequestListSerializer
    pagination_class = StandardResultsSetPagination
    
    def get_user_data(self,queryset) :
        _user = User.objects.get(id=self.request.user.id)
        if _user.user_type_id == USER_TYPE['back_office'] :
            return queryset   
        elif _user.user_type_id == USER_TYPE['buyer']:
            queryset = queryset.filter(created_by=_user) 
        return queryset

    def apply_filters(self,queryset) :
        q_objects = Q()        
        status_id = self.request.GET.get('status_id')     
        search = self.request.GET.get('search')
        budget = self.request.GET.get('budget')   
        service_delivery_name = self.request.GET.get('service_delivery_name')   
        buyer_name = self.request.GET.get('buyer_name')   

        if search :
            queryset = queryset.filter(
                Q(category__category_name__icontains=search) |
                Q(sub_category__sub_category_name__icontains=search)                             
            )
        if budget :
            q_objects.add(Q(budget=budget), Q.AND)              
        if status_id :
            q_objects.add(Q(request_status_id=status_id), Q.AND)  
        if service_delivery_name :
            q_objects.add(Q(service_delivery__service_delivery_name__icontains=service_delivery_name), Q.AND)  
        if buyer_name :
            q_objects.add(Q(created_by__user_name__icontains=buyer_name), Q.AND)                 
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL             
        if is_active == 'false' :    
            queryset = ServiceRequest.all_objects.filter(is_active=False)
        else : 
            queryset = ServiceRequest.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = self.get_user_data(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send data in specific format for POST method
(endpoint = '/requests/counts/')
'''
class ServiceRequestCountsView(GenericAPIView) :
    """
    Orders Counts 

    """
    serializer_class = RequestCountSerializer
   
    def get(self, request, *args, **kwargs) :   
        if request.user.user_type_id == USER_TYPE['back_office'] :
            raise PermissionDenied   
        with connection.cursor() as cursor:
            cursor.callproc('sp_get_request_tab_count',[request.user.id])  
            from TaskITAPI.serializers import StoredProcedureSerializer         
            data = StoredProcedureSerializer(cursor).data           
            if type(data) == dict and not data :
               data = []             
        return Response(data, status=status.HTTP_200_OK)  


'''
View to send data in specific format for POST method
(endpoint = '/requests/create/')
'''
class ServiceRequestCreateView(CreateAPIView) :
    """
    Service Request POST API .
   
    """  
    serializer_class = ServiceRequestSerializer

    @transaction.atomic
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        if 'details' in validated_data :
            details = validated_data.pop('details')                  
        req = ServiceRequest.objects.create(
            **validated_data,
            request_status_id = REQUEST_STATUS['Pending'],
            created_by = self.request.user,
            created_user = self.request.user.user_name
        )            
        if len(details) > 0 :
            for dtl in details :
                req_dtl = RequestDetails(
                    request= req,
                    **dtl
                )  
                req_dtl.save()
        res = {
            'request_id' : req.id
        }        
        notify_user.delay(events.POST_REQUEST_SUBMIT,{'request' : req})
        return Response(res,status=status.HTTP_201_CREATED)

'''
View to send data in specific format for POST method
(endpoint = '/requests/')
'''
class ServiceRequestAttachmentsView(GenericAPIView) :

    class RequestAttachmentCreateSerializer(serializers.Serializer) :
        attachments = RequestAttachmentSerializer(many=True)

    serializer_class = RequestAttachmentCreateSerializer
    queryset = ServiceRequest.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id' 

    def patch(self, request, id) :
        req = self.get_object()  
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        
        if 'attachments' in validated_data :
            attachments = validated_data.pop('attachments')  
       
        for attachment in attachments :
            req_attachment = RequestAttachment(
                request= req,
                file_path = attachment['file_path']          
            )
            req_attachment.save()
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for Delete method
(endpoint = '/requests/attachments-delete/:id')
'''
class ServiceRequestAttachmentsDeleteView(DestroyAPIView) :

    queryset = RequestAttachment.objects.all()

    def delete(self,request,*args,**kwargs) :
        req_attachment = self.get_object()
        req_attachment.delete()
        return Response(status=status.HTTP_204_NO_CONTENT) 

'''
View to send data in specific format for POST method
(endpoint = '/requests/details/')
'''
class ServiceRequestDetailsView(RetrieveUpdateDestroyAPIView) :

    serializer_class = ServiceRequestDetailserializer
    queryset = ServiceRequest.objects.all()

    def get_queryset(self) :               
        is_active =  self.request.GET.get('is_active') 
        if self.request.method == 'GET' and is_active == 'false' :  
            queryset = ServiceRequest.objects.filter(is_active=False)
        else :
            queryset = ServiceRequest.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        service_request = self.get_object()
        service_request.request_status_id = REQUEST_STATUS['Deleted']   
        service_request.is_active = False
        service_request.deleted_by = request.user
        service_request.deleted_user = request.user.user_name
        service_request.deleted_date = timezone.now()
        service_request.save()
        return Response(status=status.HTTP_204_NO_CONTENT)    


'''
View to send data in specific format for POST method
(endpoint = '/requests/approve/:id')
'''
class ServiceRequestApproveView(GenericAPIView) :

    queryset = ServiceRequest.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'

    def patch(self,request,id) :
        service_request = self.get_object()    
        if request.user.user_type_id != USER_TYPE['back_office'] :
            raise PermissionDenied
        service_request.request_status_id = REQUEST_STATUS['Active']          
        service_request.modified_by = request.user
        service_request.modified_user = request.user.user_name
        service_request.modified_date = timezone.now()
        service_request.save()
        notify_user.delay(events.POST_REQUEST_APPROVED_TO_BUYER,{'request' : service_request})
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for POST method
(endpoint = '/requests/reject/:id')
'''
class ServiceRequestRejectView(GenericAPIView) :

    class RequestRejectSerializer(serializers.Serializer) :
        reject_comment = serializers.CharField(max_length=500)
    
    serializer_class = RequestRejectSerializer
    queryset = ServiceRequest.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'

    def patch(self,request,id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        service_request = self.get_object() 
        if request.user.user_type_id != USER_TYPE['back_office'] or service_request.request_status_id != REQUEST_STATUS['Pending'] :
            raise PermissionDenied   
        service_request.request_status_id = REQUEST_STATUS['Unapproved']      
        service_request.reject_comment = validated_data['reject_comment']    
        service_request.modified_by = request.user
        service_request.modified_user = request.user.user_name
        service_request.modified_date = timezone.now()
        service_request.save()
        notify_user.delay(events.POST_REQUEST_REJECTED,{'request' : service_request})
        return Response(status=status.HTTP_201_CREATED)