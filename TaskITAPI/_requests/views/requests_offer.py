from rest_framework.generics import CreateAPIView,ListAPIView,GenericAPIView,DestroyAPIView,RetrieveAPIView,UpdateAPIView
from _requests.models import ServiceRequest,RequestOffer,RequestOfferDetail
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db import transaction
from _requests.serializers import RequestOfferListSerializer,RequestOfferDetailsGETSerializer,RequestSendOfferSerializer,SellerRequestInfoSerializer,SellerRequestOfferListSerializer,\
    RequestOfferOrderCreateSerializer,SellerGigListtSerializer,RequestOfferUpdateSerializer
from django.db.models import Q
from _requests.utils import get_user_data
from rest_framework.exceptions import PermissionDenied
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from gigs.models import Gig
from orders.models import Order,OrderGigPrice,OrderSubCategoryPriceDetails
from configs.values import ORDER_STATUS
from masters.models import SubCategory
from django.utils import timezone
from settings.values import configs
from settings.models import SystemConfiguration
from rest_framework.response import Response
from rest_framework import status
from accounts.values import GROUP
from notifications.tasks import notify_user
from notifications import events
import datetime

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/request/seller-request-offers')
'''
class RequestOfferListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. is_sent_offer = To filter by is_sent_offer(false/true)
    2. search = To filter by Delivery Duration & Buyer name & Sub Category
    3. budget = To filter by budget
   
    E.g. http://127.0.0.1:8000/api/v1/requests/seller-request-offers?is_sent_offer=true   

    """
    serializer_class = RequestOfferListSerializer
    pagination_class = StandardResultsSetPagination
 

    def apply_filters(self,queryset) :
        q_objects = Q()        
        is_sent_offer = self.request.GET.get('is_sent_offer')
        search = self.request.GET.get('search')
        budget = self.request.GET.get('budget')

        if search :
            queryset = queryset.filter(
                Q(request__service_delivery__service_delivery_value__icontains=search) |
                Q(request__created_by__user_name__icontains=search) |
                Q(request__sub_category__sub_category_name__icontains=search) |
                Q(request__budget=search)
            )
        if budget :
            q_objects.add(Q(request__budget=budget), Q.AND)     
        if is_sent_offer :
            if is_sent_offer == 'true' :
                q_objects.add(Q(is_sent_offer=True), Q.AND) 
            elif is_sent_offer == 'false':
                q_objects.add(Q(is_sent_offer=False), Q.AND) 
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL             
        if is_active == 'false' :    
            queryset = RequestOffer.all_objects.filter(is_active=False)
        else : 
            queryset = RequestOffer.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = get_user_data(self.request,queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/request/seller-request-offers')
'''
class RequestOfferDetailsGETView(RetrieveAPIView) :

    serializer_class = RequestOfferDetailsGETSerializer
    queryset = RequestOffer.objects.all()

    def get_queryset(self) :       
        queryset = RequestOffer.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset
 


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/request/seller-request-offers/:id')
'''
class RequestOfferUpdateView(GenericAPIView) :

    queryset = RequestOffer.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    serializer_class = RequestOfferUpdateSerializer

    @transaction.atomic
    def patch(self,request,id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data     
        details = []      
        update_fields = []
        request_offer = self.get_object()    
        if 'details' in validated_data :
            details = validated_data.pop('details')                
        for attr, value in validated_data.items():
            setattr(request_offer, attr, value)
            update_fields.append(attr)
        request_offer.modified_by = request.user
        request_offer.modified_user = request.user.user_name
        request_offer.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        request_offer.save(update_fields=update_fields)    
        for dtl in details :
            RequestOfferDetail.objects.filter(id=dtl['id']).update(
                sub_category_price_scope_id = dtl['sub_category_price_scope_id'],
                sub_category_price_scope_dtl_id = dtl['sub_category_price_scope_dtl_id'],
                value = dtl['value']
            )
        return Response(status=status.HTTP_201_CREATED)        
 

    
'''
View to send data in specific format for POST method
(endpoint = '/requests/offer-delete/:id')
'''
class RequestOfferDestroyView(DestroyAPIView) : 
    queryset = RequestOffer.objects.all()
    
    def delete(self,request,*args,**kwargs) :
        request_offer = self.get_object()
        request_offer.is_active = False
        request_offer.deleted_by = request.user
        request_offer.deleted_user = request.user.user_name
        request_offer.deleted_date = timezone.now()
        request_offer.save()
        request_count = RequestOffer.objects.filter(request=request_offer.request).count()
        service_request = ServiceRequest.objects.get(id=request_offer.request_id)
        service_request.seller_offer_count = request_count
        service_request.save(update_fields=['seller_offer_count'])
        notify_user.delay(events.REQUEST_OFFER_REJECTED,{'request_offer':request_offer})
        return Response(status=status.HTTP_204_NO_CONTENT)    


'''
View to send data in specific format for POST method
(endpoint = '/requests/send-offer/:id')
'''
class RequestOfferSendView(GenericAPIView) :
  
    queryset = RequestOffer.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    serializer_class = RequestSendOfferSerializer

    @transaction.atomic
    def patch(self,request,id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data     
        details = []
        if 'details' in validated_data :
            details = validated_data.pop('details')               
        request_offer = self.get_object()    
        if request_offer.is_sent_offer :
            raise PermissionDenied
        for dtl in details :
            RequestOfferDetail.objects.create(
                request_offer = request_offer,
                **dtl
            )
        request_offer.seller_offer_description = validated_data['seller_offer_description']
        request_offer.gig_id = validated_data['gig_id']
        request_offer.price_type_id = validated_data['price_type_id']
        request_offer.offer_price = validated_data['offer_price']
        request_offer.offer_delivery_time = validated_data['offer_delivery_time']
        request_offer.is_sent_offer = True       
        request_offer.modified_by = request.user
        request_offer.modified_user = request.user.user_name
        request_offer.modified_date = timezone.now()
        request_offer.save()
        request_count = RequestOffer.objects.filter(request=request_offer.request).count()
        service_request = ServiceRequest.objects.get(id=request_offer.request_id)
        service_request.seller_offer_count = request_count
        service_request.save(update_fields=['seller_offer_count'])
        notify_user.delay(events.REQUEST_OFFER_SUBMIT,{'request_offer':request_offer})
        return Response(status=status.HTTP_201_CREATED)        


'''
View to send data in specific format for POST method
(endpoint = '/requests/seller-request-offer-info/:id')
'''
class SellerRequesInfoView(RetrieveAPIView) :

    queryset = ServiceRequest.objects.all()
    serializer_class = SellerRequestInfoSerializer

    def get_queryset(self) :       
        queryset = ServiceRequest.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send data in specific format for POST method
(endpoint = '/requests/seller-request-offer-list/:id')
'''
class SellerRequestOfferListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. sort_by = To filter by (date/price/delivery/rating)   
   
    E.g. http://127.0.0.1:8000/api/v1/requests/seller-request-offer-list?sort_by=   

    """
    queryset = RequestOffer.objects.all()
    serializer_class = SellerRequestOfferListSerializer
    pagination_class = StandardResultsSetPagination

    def apply_filter(self,queryset) :
        q_objects = Q()
        sort_by = self.request.GET.get('sort_by')
        if sort_by :
            if sort_by.lower() == 'date' :
                queryset = queryset.order_by('-created_date') 
            elif sort_by.lower() == 'price' :
                queryset = queryset.order_by('offer_price')            
            elif sort_by.lower() == 'delivery' :
                queryset = queryset.order_by('offer_delivery_time')    
            elif sort_by.lower() == 'rating' :
                queryset = queryset.order_by('-gig__rating')            
        return queryset

    def get_queryset(self) :    
        request_id = self.kwargs['request_id']
        if not ServiceRequest.objects.filter(id=request_id).exists() :
            raise ValidationError(_('Invalid input recieved for request id.'),code='request_id_invalid_input')
        queryset = RequestOffer.objects.filter(request_id=request_id)     
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        queryset = self.apply_filter(queryset)
        return queryset        


'''
View to send data in specific format for POST method
(endpoint = '/requests/request-offer-order-create')
'''
class RequestOfferOrderCreateView(CreateAPIView) :

    serializer_class = RequestOfferOrderCreateSerializer

    @transaction.atomic
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        # if request.user.group_id != GROUP['buyer'] :
        #     raise PermissionDenied
            
        request_offer = RequestOffer.objects.get(id= validated_data['request_offer_id'])
        gig = Gig.objects.get(id= validated_data['gig_id'])

        order_gig_price = None
        if 'order_gig_price' in validated_data :
            order_gig_price = validated_data.pop('order_gig_price')
        sub_category_price_details = []    
        if 'sub_category_price_details' in validated_data :
            sub_category_price_details = validated_data.pop('sub_category_price_details')    

        amount = validated_data['amount']
        # service_fee_sys = SystemConfiguration.objects.filter(code_name=configs.SERVICE_FEE_PERC)
        # service_fee = int((int(total_amount) * int(service_fee_sys[0].field_value))/100)
        service_fee = int((int(amount) * int(5))/100)
        total_amount = int(amount) + service_fee

        order = Order.objects.create(
            order_no = datetime.datetime.now().strftime('%Y%m%d%H-%M-%S'),
            seller = gig.created_by,
            buyer = request.user,
            gig = gig,
            amount = validated_data['amount'],
            extra_service_amount = 0,
            extra_service_custom_amount = 0,
            service_fee = service_fee,
            total_amount = total_amount,
            delivery_time = validated_data['delivery_time'],
            order_status_id = ORDER_STATUS['Draft'],
            created_by = self.request.user,
            created_user = self.request.user.user_name
            )     

        if order_gig_price :
            order_gig_price = OrderGigPrice.objects.create(
                order = order,
                **order_gig_price               
            ) 

        if len(sub_category_price_details) > 0 :
            sub_category_price_details_list = [] 
            for price_dtl in sub_category_price_details :
                order_price_dtl = OrderSubCategoryPriceDetails(
                   order=order,
                    **price_dtl
                )
                sub_category_price_details_list.append(order_price_dtl)
            if len(sub_category_price_details_list) > 0 :
                OrderSubCategoryPriceDetails.objects.bulk_create(sub_category_price_details_list)  

        ServiceRequest.objects.filter(id=request_offer.request_id).update(
            order = order,
            modified_by = self.request.user,
            modified_user = self.request.user.user_name,
            modified_date = timezone.now()
        )        
        res = {
            'order_id' : order.id,
            'order_no' : order.order_no
        } 
        return Response(res,status=status.HTTP_201_CREATED)


'''
View to send data in specific format for POST method
(endpoint = '/requests/seller-gig-list/:sub_category_id')
'''
class SellerGigListView(ListAPIView) :

    queryset = Gig.objects.all()
    serializer_class = SellerGigListtSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self) :    
        sub_category_id = self.kwargs['sub_category_id']
        user_id = self.request.user.id       
        if not SubCategory.objects.filter(id=sub_category_id).exists() :
            raise ValidationError(_('Invalid input recieved for sub category id.'),code='sub_category_id_invalid_input')
        queryset = Gig.objects.filter(sub_category_id=sub_category_id,created_by_id=user_id)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset  