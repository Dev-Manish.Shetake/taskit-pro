from django.db import models
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404
import string
import random


def generate_random_password() :
    password = ''.join(random.choices(string.ascii_lowercase +
                             string.digits, k = 8)) 
    return password

def get_user_type_id(user_type, user_type_name) :
    user_type_id = None
    # user_type_id = cache.get('%s_user_type_id' % user_type)
    if (user_type_id == None):
        from accounts.models import UserType
        user_type_obj = get_object_or_404(UserType, user_type_name__iexact=user_type_name)
        user_type_id = user_type_obj.id
        # cache.set('%s_user_type_id' % user_type, user_type_id)
    return user_type_id


class UserManager(BaseUserManager) :

    def _create_user(self, email_address, password, **extra_fields): 
        user = self.model(email_address=email_address, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    # django -> Creating a normal user
    def create_user(self, email_address, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email_address, password, **extra_fields)

    # django -> Creating a super user with createsuperuser command
    def create_superuser(self, email_address, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email_address, password, **extra_fields)    


    def create(self, email_address, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        password = generate_random_password()
        return self._create_user(email_address, password, **extra_fields)

    def create_backoffice_user(self, email_address, password=None, **extra_fields) :
        group_id = None
        if 'group_id' in extra_fields :
            group_id = extra_fields.pop('group_id')
        user_type_id = get_user_type_id('user_type', 'Back Office')
        extra_fields['user_type_id'] = user_type_id
        extra_fields['is_staff'] = True
        user = self.create(email_address, password, **extra_fields)
        group = Group.objects.get(id=1)
        user.groups.add(group)
        user._groups = [group]
        return user

    def get_queryset(self):
        return super().get_queryset().filter(is_active=True)


class AllUserManager(BaseUserManager):
    def get_queryset(self):
        return super().get_queryset()
