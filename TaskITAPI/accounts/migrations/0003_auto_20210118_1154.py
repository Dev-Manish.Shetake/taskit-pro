# Generated by Django 2.2 on 2021-01-18 11:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20210116_1739'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='rating',
            field=models.DecimalField(decimal_places=1, max_digits=5, null=True),
        ),
    ]
