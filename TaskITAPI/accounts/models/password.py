from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from configs.models import UserDeactivationReason


# Reset password related data stored in this table
class ResetPassword(models.Model) :
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    otp = models.CharField(max_length=10)
    email_address = models.EmailField(null=True)
    mobile_no = models.CharField(null=True, max_length=20)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta :
        db_table = 'user_reset_token'
        default_permissions = ()


class UserVerificationCode(models.Model) :
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    verification_code = models.CharField(max_length=16)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta :
        db_table = 'user_verification_code'
        default_permissions = ()


class UserOTPVerification(models.Model) :
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    mobile_no = models.CharField(max_length=10)
    otp = models.CharField(max_length=16)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta :
        db_table = 'user_otp_verification'
        default_permissions = ()


class UserDeactivation(models.Model) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)   
    user_deactivation_reason = models.ForeignKey(UserDeactivationReason, on_delete=models.DO_NOTHING)
    other_reason = models.CharField(max_length=1000,null=True)

    class Meta :
        db_table = 'mst_user_deactivation' 
        default_permissions = ()                