from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from accounts.managers import UserManager, AllUserManager
from softdelete.models import SoftDeleteModel


# User Type Model
class UserType(SoftDeleteModel):
    user_type_name = models.CharField(max_length=100, unique=True)
    created_by_id = models.IntegerField()
    created_user = models.CharField(max_length=50, null=True,validators=[])
    created_date = models.DateTimeField(auto_now_add=True) 
    modified_by_id = models.IntegerField(null=True)
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(null=True, blank=True)
    deleted_by_id = models.IntegerField(null=True)
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  
    def __str__(self):
        return self.user_type_name

    class Meta : 
        db_table = 'mst_user_type'
        default_permissions = ()



# User Level Model
class Level(SoftDeleteModel):
    seq_no = models.IntegerField()
    level_name = models.CharField(max_length=100, unique=True)
    range_from = models.IntegerField()
    range_to = models.IntegerField()
    no_of_orders = models.IntegerField()
    created_by_id = models.IntegerField()
    created_user = models.CharField(max_length=50, null=True,validators=[])
    created_date = models.DateTimeField(auto_now_add=True) 
    modified_by_id = models.IntegerField(null=True)
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(null=True, blank=True)
    deleted_by_id = models.IntegerField(null=True)
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  
    def __str__(self):
        return self.level_name

    class Meta : 
        db_table = 'mst_level'
        default_permissions = ()


# Custom User Model 
class User(AbstractBaseUser,PermissionsMixin) :
    user_name = models.CharField(max_length=200)
    user_type = models.ForeignKey(UserType,on_delete=models.DO_NOTHING)
    email_address = models.EmailField(unique=True)
    password = models.CharField(max_length=150)
    is_staff = models.BooleanField(default=False)
    is_verified_email_address = models.BooleanField(default=False,null=True)
    verified_email_address_date = models.DateTimeField(null=True, blank=True)   
    mobile_no = models.CharField(max_length=50,null=True)
    is_verified_mobile_no = models.BooleanField(default=False,null=True)
    verified_mobile_no_date = models.DateTimeField(null=True, blank=True)
    lang_id = models.IntegerField()
    time_zone = models.CharField(max_length=50, default='Asia/Kolkata')
    firebase_device_id = models.CharField(max_length=1000,null=True)
    rated_no_of_records = models.IntegerField(null=True)
    level = models.ForeignKey(Level,on_delete=models.DO_NOTHING,null=True)
    rating = models.DecimalField(max_digits=5,decimal_places=1,null=True)
    auth_provider = models.CharField(max_length=10,null=True)
    is_seller = models.BooleanField(null=True)
    is_active = models.BooleanField(default=True)
    is_out_of_office = models.BooleanField(default=False)
    created_by = models.ForeignKey('self',on_delete=models.DO_NOTHING, related_name = 'user_created_by')
    created_user = models.CharField(max_length=50, null=True,validators=[])
    created_date = models.DateTimeField(auto_now_add=True) 
    modified_by = models.ForeignKey('self', null=True, on_delete=models.DO_NOTHING, related_name='user_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(null=True, blank=True)
    deleted_by = models.ForeignKey('self', null=True, on_delete=models.DO_NOTHING, related_name='user_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
   
    objects = UserManager()
    all_objects = AllUserManager()

    USERNAME_FIELD = 'email_address'

    def __str__(self):
        return self.user_name

    def delete(self):
        self.is_active = False
        self.save()    

    class Meta :
        db_table = 'mst_user'
        default_permissions = ()

    def save(self, *args, **kwargs):
        if self.email_address != None :
            self.email_address = self.email_address.lower()  
        super().save(*args, **kwargs)


class UserActiveLoggedIn(models.Model) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)   
    access_token = models.CharField(max_length=200)
    created_date = models.DateTimeField()

    class Meta :
        db_table = 'user_active_logged_in' 
        default_permissions = ()
