from django.db import models
from .user import User
from softdelete.models import SoftDeleteModel
from configs.models import Language,LanguageProficiency,SocialMedia,Skill,SkillLevel,EducationTitle,EducationMajor,Country,City


def profile_picture(instance, filename):
    return 'users/{0}/{1}_{2}/{3}'.format(
        instance.user.user_name.lower().replace(" ", "_"),
        instance.user.id,
        'profile_picture',
        filename
    )

class UserPersonalInfo(models.Model) :
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING)   
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    profile_picture = models.FileField(null=True,upload_to=profile_picture)
    description = models.CharField(max_length=300,null=True)
    personal_website = models.CharField(max_length=200,null=True,blank=True)

    class Meta :
        db_table = 'mst_user_personal_info' 
        default_permissions = ()


class UserLanguage(SoftDeleteModel) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)   
    lang = models.ForeignKey(Language,on_delete=models.DO_NOTHING)
    lang_proficiency = models.ForeignKey(LanguageProficiency,on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'user_language_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_language_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_language_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  

    class Meta :
        db_table = 'mst_user_lang' 
        default_permissions = ()


class UserSocialAccount(SoftDeleteModel) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)   
    social_media = models.ForeignKey(SocialMedia,on_delete=models.DO_NOTHING)
    social_account_id = models.CharField(max_length=200)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'user_social_account_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_social_account_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_social_account_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  
    class Meta :
        db_table = 'mst_user_social_account' 
        default_permissions = ()        


class UserSkill(SoftDeleteModel) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)   
    skill = models.ForeignKey(Skill,on_delete=models.DO_NOTHING)
    skill_level = models.ForeignKey(SkillLevel,on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'user_skill_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_skill_account_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_skill_account_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  
    class Meta :
        db_table = 'mst_user_skill' 
        default_permissions = ()      


class UserEducation(SoftDeleteModel) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)   
    country = models.ForeignKey(Country,on_delete=models.DO_NOTHING)  
    university_name = models.CharField(max_length=200)
    education_title = models.ForeignKey(EducationTitle,on_delete=models.DO_NOTHING) 
    education_major = models.ForeignKey(EducationMajor,on_delete=models.DO_NOTHING)
    education_year = models.IntegerField()
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'user_education_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_education_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_education_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  
    class Meta :
        db_table = 'mst_user_education' 
        default_permissions = ()        


class UserCertification(SoftDeleteModel) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING) 
    certification_name = models.CharField(max_length=200)
    certification_from = models.CharField(max_length=200)
    certification_year = models.IntegerField()
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'user_certification_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_certification_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_certification_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  
    class Meta :
        db_table = 'mst_user_certification' 
        default_permissions = ()  


class UserAddress(SoftDeleteModel) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING) 
    address_nick_name = models.CharField(max_length=50)
    company_name = models.CharField(max_length=200,null=True)
    address_line1 = models.CharField(max_length=200)
    address_line2 = models.CharField(max_length=200,null=True)
    landmark = models.CharField(max_length=200,null=True)
    city = models.ForeignKey(City,on_delete=models.DO_NOTHING)
    pincode = models.IntegerField()
    is_default = models.BooleanField(null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'user_address_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_address_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_address_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  
    class Meta :
        db_table = 'mst_user_address' 
        default_permissions = ()          


class UserBank(SoftDeleteModel) :
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING) 
    bank_account_name = models.CharField(max_length=50)
    bank_account_no = models.CharField(max_length=50)
    bank_name = models.CharField(max_length=100)
    branch_name = models.CharField(max_length=100)
    bank_address = models.CharField(max_length=500,null=True)
    bank_ifsc_code = models.CharField(max_length=25)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'user_bank_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_bank_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='user_bank_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
  
    class Meta :
        db_table = 'mst_user_bank' 
        default_permissions = ()          