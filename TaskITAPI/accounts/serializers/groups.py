from rest_framework import serializers 
from django.contrib.auth.models import Group
from django.db.models import Prefetch


'''
Serializer to send data in specific format for GET method
(endpoint = '/groups/)
'''
class GroupSerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self, queryset) :       
        return queryset

    class Meta :
        model = Group
        fields = [
            'id',
            'name'
        ]
       
