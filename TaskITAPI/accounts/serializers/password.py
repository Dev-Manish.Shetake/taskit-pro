from rest_framework import serializers

'''
Serializers for POST method. Reset password
'''
class ResetPasswordSerializers(serializers.Serializer) :
    current_password = serializers.CharField(max_length=50)
    new_password = serializers.CharField(max_length=50)
  

'''
Serializers for POST method. Reset password with OTP
'''
class PasswordOTPSerializer(serializers.Serializer):
    otp = serializers.CharField(max_length=10)
    password = serializers.CharField(max_length=50)  