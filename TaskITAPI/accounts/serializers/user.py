from rest_framework import serializers
from accounts.models import User,UserEducation,Level,UserPersonalInfo,UserType,UserBank
# from django.core.exceptions import ValidationError
from django.utils.translation import gettext as _
from rest_framework.exceptions import ValidationError
from django.utils import timezone
from notifications.tasks import notify_user
from notifications import events
from configs.models import GenLedger
from configs.serializers import CountrySerializer
from accounts.serializers.user_info import UserPersonalInfoMinSerializer,UserPersonalInfoSerializer,UserPersonalInfoMacroSerializer
from django.db.models import Prefetch
from configs.values import LEDGER_TYPE
from django.db import transaction

class UserTypeSerailizer(serializers.ModelSerializer) :

    class Meta :
        model = UserType
        fields =[
            'id',
            'user_type_name',
        ]


class UserLevelSerailizer(serializers.ModelSerializer) :

    class Meta :
        model = Level
        fields =[
            'id',
            'level_name',
        ]


class UserMacroSerializer(serializers.ModelSerializer) :

    class Meta :
        model = User
        fields =[
            'id',
            'user_name', 
        ]


class UserMinSerializer(serializers.ModelSerializer) :
    level = UserLevelSerailizer()    

    class Meta :
        model = User
        fields =[
            'id',
            'user_name',   
            'level',    
            'rating'         
        ]


class UserBankMacroSerializer(serializers.ModelSerializer) :

    class Meta :
        model = UserBank
        fields =[
            'id',
            'bank_account_name', 
            'bank_account_no'
        ]


class UserBankMinSerializer(serializers.ModelSerializer) :
    user_bank_info = UserBankMacroSerializer(many=True)

    def setup_eager_loading(self,queryset,prefetch_prefix = '') :
        queryset = queryset.prefetch_related(
                Prefetch(prefetch_prefix + 'userbank_set', queryset=UserBank.objects.all(), to_attr = 'user_bank_info')
            )
        return queryset

    class Meta :
        model = User
        fields =[
            'id',
            'user_name', 
            'user_bank_info'
        ]


class BuyerInfoMinSerializer(serializers.ModelSerializer) :
    level = UserLevelSerailizer()    
    user_info = UserPersonalInfoMacroSerializer()

    def setup_eager_loading(self,queryset,prefetch_prefix = '') :
        queryset = queryset.prefetch_related(
                Prefetch(prefetch_prefix + 'userpersonalinfo', queryset=UserPersonalInfo.objects.all(), to_attr = 'user_info')
            )
        return queryset

    class Meta :
        model = User
        fields =[
            'id',
            'user_name',   
            'level',    
            'rating',
            'user_info'         
        ]


class SellerInfoMinSerializer(serializers.ModelSerializer) :
    level = UserLevelSerailizer()    
    user_info = UserPersonalInfoMacroSerializer()

    def setup_eager_loading(self,queryset,prefetch_prefix = '') :
        queryset = queryset.prefetch_related(
            Prefetch(prefetch_prefix + 'userpersonalinfo', queryset=UserPersonalInfo.objects.all(), to_attr = 'user_info') 
            )
        return queryset

    class Meta :
        model = User
        fields =[
            'id',
            'user_name',   
            'level',    
            'rating',
            'user_info'         
        ]        


class LoggedInUserSerailizer(serializers.ModelSerializer) :
    
    class Meta :
        model = UserPersonalInfo
        fields =[
            'id',
            'first_name',   
            'last_name',    

            'profile_picture'         
        ]
   
 
'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/backoffice-user-create')
'''
class BackOfficeUserCreateSerializer(serializers.ModelSerializer) :
    user_type_id = serializers.IntegerField(write_only=True)  

    def validate_user_name(self,value) :
        if User.objects.filter(user_name=value).exists() :
            raise ValidationError(_('User Name already exists'),code='not_unique')
        return value
    
    def validate_email_address(self, value) :
        if User.objects.filter(email_address=value).exists() :
            raise ValidationError(_('User with this email address already exists.'), code='not_unique')
        return value

    def validate_user_type_id(self, value) :
        if not UserType.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for user_type_id.'), code='invalid_input')
        return value    

    class Meta :
        model = User
        fields =[
            'id',
            'user_name',
            'user_type_id',
            'email_address'            
        ]

    def create_user_gen_ledger(self,user) :       
        gen_ledger = GenLedger.objects.create(
            gen_ledger_type_id = LEDGER_TYPE['User'],
            user = user,
            gen_ledger_name = user.user_name,
            created_by = user,
            created_user = user.user_name
        ) 
        return gen_ledger    

    @transaction.atomic
    def create(self,validated_data) :
        user = User.objects.create_backoffice_user(
            **validated_data, 
            lang_id = 1,
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name
        )
        self.create_user_gen_ledger(user)
        notify_user.delay(events.BACKOFFICE_REGISTER, { 'user' : user })
        return user    


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/backoffice-list')
'''
class UserListSerializer(serializers.ModelSerializer) :
    user_type_id = serializers.IntegerField(write_only=True)  
    user_type = UserTypeSerailizer(read_only=True)
    level_id = serializers.IntegerField(write_only=True)
    level = UserLevelSerailizer(read_only=True)
   
    def setup_eager_loading(self, queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = User
        fields =[
            'id',
            'user_name',
            'user_type_id',
            'user_type',
            'is_verified_email_address',
            'verified_email_address_date',
            'email_address',
            'level_id',
            'level',        
            'is_seller',  
            'is_active',
            'created_date',
            'modified_date'
        ]
        read_only_fields = [
            'is_verified_email_address',
            'verified_email_address_date'
        ]

    def update(self,user,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(user, attr, value)
            update_fields.append(attr)
        user.modified_by = self.context['request'].user
        user.modified_user = self.context['request'].user.user_name
        user.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        user.save(update_fields=update_fields)       
        return user

'''
Serializer to send/receive data in specific format for GET/PUT/PATCH method
(endpoint = '/users/backoffice-profile/:id')
'''
class UserProfileDetailsSerializer(serializers.ModelSerializer):   
    email_address = serializers.EmailField()       
    personal_info = UserPersonalInfoSerializer()

    def setup_eager_loading(self, queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('userpersonalinfo',UserPersonalInfo.objects.all(),to_attr='personal_info')
        )
        return queryset

    class Meta :
        model = User
        fields = [
            'id',
            'user_name',
            'email_address',           
            'personal_info',
            'is_active',
        ]
        read_only_fields = ['is_active']

      # Method to update references
    def update_personal_info(self, user, personal_info) :
        _personal_info = []  
        user_update_info_fields = []     
        if user.personal_info :
            for key, value in personal_info.items() : 
                if getattr(user.personal_info, key) != value :
                    user_update_info_fields.append(key)
                    setattr(user.personal_info, key, value)   
                user.personal_info.save(update_fields = user_update_info_fields)    
        else :
            UserPersonalInfo.objects.create(
                user=user,
                **personal_info
            )           

    def update(self,user,validated_data) :
        update_fields = []
        personal_info = None
        if 'personal_info' in validated_data :
            personal_info = validated_data.pop('personal_info')
        for attr, value in validated_data.items():
            setattr(user, attr, value)
            update_fields.append(attr)
        user.modified_by = self.context['request'].user
        user.modified_user = self.context['request'].user.user_name
        user.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        user.save(update_fields=update_fields)       
        if personal_info :
            self.update_personal_info(user, personal_info)
        return user    
