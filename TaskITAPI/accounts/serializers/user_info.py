from rest_framework import serializers
from accounts.models import User,Level,UserLanguage,UserEducation,UserCertification,UserSkill, UserSocialAccount, UserPersonalInfo,UserAddress,UserType,UserBank
from configs.serializers import CountrySerializer,LanguageSerializer,LanguageProficiencySerializer,SocialMediaSerializer,SkillSerializer,SkillLevelSerializer,\
    EducationTitleSerializer,EducationMajorSerializer,CitySerializer
from django.utils import timezone    
from gigs.models import Gig,GigSearchTag
from django.db.models import Q,OuterRef,Subquery,F,Count
from TaskITAPI.settings import  BASE_URL


class UserRatingSerializer(serializers.ModelSerializer) :

    class Meta :
        model = User
        fields = [
            'id',
            'rating',
            'rated_no_of_records'
        ]

'''
User Skill Serializer is a Min serializer used to create User Skill Records.
'''
class UserSkillSerializer(serializers.Serializer) :
    skill_id = serializers.IntegerField()
    skill_level_id = serializers.IntegerField()


'''
User Skill Serializer is a serializer used to used to GET Records.
'''
class UserSkillListSerializer(serializers.ModelSerializer) :
    skill_id = serializers.IntegerField(write_only=True)
    skill = SkillSerializer(read_only=True)
    skill_level_id = serializers.IntegerField(write_only=True)
    skill_level = SkillLevelSerializer(read_only=True)

    class Meta :
        model = UserSkill
        fields = [
            'id',
            'skill_id',
            'skill',
            'skill_level_id',
            'skill_level'
        ]

    def create(self,validated_data) :
        user_skill = UserSkill.objects.create(
            **validated_data,
            user = self.context['request'].user,
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name            
        )      
        return user_skill    


    def update(self,user_skill,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(user_skill, attr, value)
            update_fields.append(attr)
        user_skill.modified_by = self.context['request'].user
        user_skill.modified_user = self.context['request'].user.user_name
        user_skill.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        user_skill.save(update_fields=update_fields)       
        return user_skill        
    


'''
User Education Serializer is used to create User Education Records.
'''
class UserEducationSerializer(serializers.ModelSerializer) :
    country_id = serializers.IntegerField()
    education_title_id = serializers.IntegerField()
    education_major_id = serializers.IntegerField()
    education_year = serializers.IntegerField()

    class Meta :
        model = UserEducation
        fields = [
            'id',
            'country_id',
            'university_name',
            'education_title_id',
            'education_major_id',
            'education_year'
        ]

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/educations')
'''
class UniversityListSerializer(serializers.Serializer) :
    id = serializers.IntegerField()
    university_name = serializers.CharField()
    

 
'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/educations')
'''
class UserEducationListSerializer(serializers.ModelSerializer) :
    country_id = serializers.IntegerField(write_only=True)       
    country = CountrySerializer(read_only=True)
    education_title_id = serializers.IntegerField(write_only=True)
    education_title = EducationTitleSerializer(read_only=True)
    education_major_id = serializers.IntegerField(write_only=True)
    education_major = EducationMajorSerializer(read_only=True)

    
    def setup_eager_loading(self, queryset) :
        return queryset   

    class Meta :
        model = UserEducation
        fields =[
            'id',
            'country_id',
            'country',
            'university_name',
            'education_title_id', 
            'education_title',
            'education_major_id',
            'education_major',
            'education_year'           
        ] 

    def create(self,validated_data) :
        user_education = UserEducation.objects.create(
            **validated_data,
            user = self.context['request'].user,
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name            
        )      
        return user_education


    def update(self,user_education,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(user_education, attr, value)
            update_fields.append(attr)
        user_education.modified_by = self.context['request'].user
        user_education.modified_user = self.context['request'].user.user_name
        user_education.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        user_education.save(update_fields=update_fields)       
        return user_education


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/languages')
'''
class UserLanguageListSerializer(serializers.ModelSerializer) :    
    lang_id = serializers.IntegerField(write_only=True)
    lang = LanguageSerializer(read_only=True)
    lang_proficiency_id = serializers.IntegerField(write_only=True)      
    lang_proficiency = LanguageProficiencySerializer(read_only=True)

    def setup_eager_loading(self, queryset) :
        return queryset   

    class Meta :
        model = UserLanguage
        fields =[
            'id',
            'lang_id',
            'lang',
            'lang_proficiency_id',
            'lang_proficiency',
            'created_by'                    
        ] 
        read_only_fields = ['created_by']

    def create(self,validated_data) :
        user_language = UserLanguage.objects.create(
            **validated_data,
            user = self.context['request'].user,
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name            
        )      
        return user_language
        
    def update(self,user_language,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(user_language, attr, value)
            update_fields.append(attr)
        user_language.modified_by = self.context['request'].user
        user_language.modified_user = self.context['request'].user.user_name
        user_language.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        user_language.save(update_fields=update_fields)       
        return user_language    
    

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/social-accounts-linked')
'''
class UserSocialAccountListSerializer(serializers.ModelSerializer) :    
    social_media = SocialMediaSerializer()

    class Meta :
        model = UserSocialAccount
        fields =[
            'id',
            'social_media',
            'social_account_id',
            'is_active',            
        ] 


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = 'user-accounts-verify-details')
'''
class UserAccountVerifyDetailsSerializer(serializers.ModelSerializer) :    
    
    class Meta :
        model = User
        fields =[
            'id',
            'email_address',
            'is_verified_email_address',
            'verified_email_address_date',
            'mobile_no',
            'is_verified_mobile_no',
            'verified_mobile_no_date',
            'is_active',            
        ] 


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/')
'''
class UserPersonalInfoMinSerializer(serializers.ModelSerializer) :    
    user = UserRatingSerializer()
    profile_picture = serializers.SerializerMethodField()
       
    class Meta :
        model = UserPersonalInfo
        fields =[
            'id',   
            'first_name',
            'last_name',   
            'profile_picture',
            'description',
            'personal_website',
            'user'         
        ]

    def get_profile_picture(self,instance) :
        profile_picture = None
        if instance.profile_picture :
            profile_picture = BASE_URL +"/"+"media/"+str(instance.profile_picture)
        return profile_picture    


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/')
'''
class UserPersonalInfoSerializer(serializers.ModelSerializer) :      
       
    class Meta :
        model = UserPersonalInfo
        fields =[
            'id',   
            'first_name',
            'last_name',   
            'profile_picture',
            'description'      
        ]

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/')
'''
class UserPersonalInfoMacroSerializer(serializers.ModelSerializer) :      
    class Meta :
        model = UserPersonalInfo
        fields =[
            'id',   
            'first_name',
            'last_name',
            'profile_picture'  
        ]

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/')
'''
class UserAddressMinSerializer(serializers.ModelSerializer) :    
       
    class Meta :
        model = UserAddress
        fields =[
            'id',   
            'address_nick_name',
            'landmark',   
            'city_id',                
        ]

'''
User Education Serializer is used to create User Education Records.
'''
class UserCertificationSerializer(serializers.ModelSerializer) :
    
    def setup_eager_loading(self, queryset) :
        return queryset   

    class Meta :
        model = UserCertification
        fields = [
            'id',
            'certification_name',
            'certification_from',
            'certification_year',
        ]

    
    def create(self,validated_data) :
        user_certification = UserCertification.objects.create(
            **validated_data,
            user = self.context['request'].user,
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name            
        )      
        return user_certification
        
    def update(self,user_certification,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(user_certification, attr, value)
            update_fields.append(attr)
        user_certification.modified_by = self.context['request'].user
        user_certification.modified_user = self.context['request'].user.user_name
        user_certification.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        user_certification.save(update_fields=update_fields)       
        return user_certification    



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/addresses')
'''
class UserAddressListSerializer(serializers.ModelSerializer) :    
    city_id = serializers.IntegerField(write_only=True)
    city = CitySerializer(read_only=True)   
    landmark = serializers.CharField(required=False)

    def setup_eager_loading(self, queryset) :
        return queryset   

    class Meta :
        model = UserAddress
        fields =[
            'id',
            'address_nick_name',
            'company_name',
            'address_line1',
            'address_line2',
            'landmark',
            'city_id',
            'city',
            'pincode',
            'is_default',
            'created_by',
            'created_user',
            'created_date'                    
        ] 
        read_only_fields = ['created_by','created_user','created_date']

    def create(self,validated_data) :
        user_address = UserAddress.objects.create(
            **validated_data,
            user = self.context['request'].user,
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name            
        )      
        return user_address



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user/bank-list')
'''
class UserBankListSerializer(serializers.ModelSerializer) :    
    
    def setup_eager_loading(self, queryset) :
        return queryset   

    class Meta :
        model = UserBank
        fields =[
            'id',
            'bank_account_name',
            'bank_account_no',            
            'bank_name',
            'branch_name',
            'bank_address',
            'bank_ifsc_code',
            'created_by',
            'created_user',
            'created_date'                    
        ] 
        read_only_fields = ['created_by','created_user','created_date']

    def create(self,validated_data) :
        user_address = UserBank.objects.create(
            **validated_data,
            user = self.context['request'].user,
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name            
        )      
        return user_address

    def update(self,user_bank,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(user_bank, attr, value)
            update_fields.append(attr)
        user_bank.modified_by = self.context['request'].user
        user_bank.modified_user = self.context['request'].user.user_name
        user_bank.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        user_bank.save(update_fields=update_fields)       
        return user_bank     

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/service_level/')
'''
class ServiceLevelListSerializer(serializers.ModelSerializer) :

    class Meta :
        model = Level
        fields = [
            'id',
            'seq_no',
            'level_name',
            'range_from',
            'range_to',
            'no_of_orders',
            'created_date'
        ]


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/service_level/seller-gigs')
'''
class SellerLevelGigsListSerializer(serializers.ModelSerializer) :
    user_count = serializers.SerializerMethodField()  

    def setup_eager_loading(self,queryset) :        
        return queryset

    class Meta :
        model = Level
        fields = [
            'id',
            'seq_no',
            'level_name',
            'user_count',
            'created_date'
        ]

    def get_user_count(self, instance) :
        search = self.context['request'].GET.get('search')       
        category_name = self.context['request'].GET.get('category_name')  
        sub_category_name = self.context['request'].GET.get('sub_category_name')  
        search_tags = self.context['request'].GET.get('search_tags')       
        user_count = 0
        if search :                 
            gig_ids = GigSearchTag.objects.filter(search_tags__icontains=search).values_list('gig_id')  
            gigs = Gig.objects.filter(Q(id__in=gig_ids) | Q(category__category_name__icontains=search) | Q(sub_category__sub_category_name__icontains=search))
            user_ids = gigs.values_list('created_by_id')        
            user_count = User.objects.filter(id__in=user_ids,level_id=instance.id).count()   
        if category_name :  
            gigs = Gig.objects.filter(Q(category__category_name__icontains=category_name))
            user_ids = gigs.values_list('created_by_id')        
            user_count = User.objects.filter(id__in=user_ids,lang_id=instance.id).count() 
        if sub_category_name :
            gigs = Gig.objects.filter(Q(sub_category__sub_category_name__icontains=sub_category_name))
            user_ids = gigs.values_list('created_by_id')        
            user_count = User.objects.filter(id__in=user_ids,lang_id=instance.id).count() 
        if search_tags :
            gig_ids = GigSearchTag.objects.filter(search_tags__icontains=search).values_list('gig_id')  
            user_ids = gigs.values_list('created_by_id') 
            user_count = User.objects.filter(id__in=user_ids,lang_id=instance.id).count()      
        return user_count

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/user_type/')
'''
class UserTypeListSerializer(serializers.ModelSerializer) :

    class Meta :
        model = UserType
        fields = [
            'id',
            'user_type_name',
            'created_by_id',
            'created_user',
            'created_date'
        ]        