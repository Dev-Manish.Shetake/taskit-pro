from django.urls import path
from accounts.views import ResetPasswordView,ForgotPasswordGenerateOTPView, ResetPasswordWithOTP, UserAccountDeactivateView

urlpatterns =[
    path('reset',ResetPasswordView.as_view(),name='reset-password'),
    path('forgot-generate-otp', ForgotPasswordGenerateOTPView.as_view(),name='forgot-password-generate-otp'),    
    path('forgot-verify-otp', ResetPasswordWithOTP.as_view(),name='forgot-password-verify-otp'),
    path('user-account-deactivate',UserAccountDeactivateView.as_view(),name='user-account-deactivate'),
]