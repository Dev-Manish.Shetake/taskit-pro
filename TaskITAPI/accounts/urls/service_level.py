from django.urls import path
from accounts.views import user_info

urlpatterns =[
    path('',user_info.ServiceLevelListView.as_view()),
    path('seller-gigs',user_info.SellerLevelGigsListView.as_view())    
]