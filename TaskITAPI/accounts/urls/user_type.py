from django.urls import path
from accounts.views import user_info

urlpatterns =[
    path('',user_info.UserTypeListView.as_view())   
]