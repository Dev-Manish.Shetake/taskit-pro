from configs.models import GenLedgerType,GenLedger
from configs.values import LEDGER_TYPE


# Generate One Time Password
def generate_otp() :
    from random import randint
    return randint(100000, 999999)

'''
When user created in system as soon as ledger entry created in ledger table
'''
def create_user_gen_ledger(self,user) :
    gen_ledger_type = GenLedgerType.objects.all().exclude(id=4)
    for ledger_type in gen_ledger_type :
        gen_ledger_name = user.user_name
        if ledger_type.id != LEDGER_TYPE['User'] :
            gen_ledger_name = str(gen_ledger_name) + '_' + str(ledger_type.gen_ledger_type_name)
        gen_ledger = GenLedger.objects.create(
            gen_ledger_type = ledger_type,
            user = user,
            gen_ledger_name = gen_ledger_name,
            created_by = user,
            created_user = user.user_name
        ) 