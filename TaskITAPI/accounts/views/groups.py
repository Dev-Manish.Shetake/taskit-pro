from django.contrib.auth.models import Group
from accounts.serializers import GroupSerializer
from rest_framework.generics import ListAPIView
from django.db.models import Q

# View for GET/POST request (endpoint = '/roles/)
class GroupListView(ListAPIView) :
    """
    Get group (role) masters

    Query parameter
    ---------------------------------------
    1. name = To filter on name column

    E.g.
        1.BackOffice Roles :- 127.0.0.1:8000/api/v1/group?group_name=BackOffice
       
    """
    serializer_class = GroupSerializer     

    def get_queryset(self) :        
        queryset = Group.objects.all()
        group_name = self.request.GET.get('group_name')      
        if group_name :
            queryset= queryset.filter(name__icontains=group_name)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)  
        return queryset