from rest_framework.generics import CreateAPIView,GenericAPIView
from accounts.models import User,ResetPassword,UserDeactivation
from rest_framework.response import Response
from rest_framework import status 
from accounts.serializers.password import ResetPasswordSerializers, PasswordOTPSerializer
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from notifications.tasks import notify_user
from django.contrib.auth import authenticate
from django.contrib.auth.hashers import make_password
from django.utils import timezone
from django.db import transaction 
from rest_framework import serializers
import random
import string
from accounts.utils import generate_otp
from notifications import events
from datetime import timedelta,datetime
from gigs.models import Gig
from orders.models import Order
from configs.values import GIG_STATUS,ORDER_STATUS,ORDER_CANCEL_REASON
from accounts.values import USER_TYPE
from django.db.models import Q

def update_password(user, password) :
    user.password = make_password(password) 
    user.modified_by = user
    user.modified_user = user.user_name
    # user.password_modified_date = timezone.now()    
    user.save(update_fields = ['password', 'modified_by', 'modified_user'])

'''
When user selects Reset password, user can Set new password.

'''
class ResetPasswordView(CreateAPIView) :
    serializer_class = ResetPasswordSerializers
    queryset = User.objects.none()

    def validate_current_password(self,request) :
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True) :
            email_address = request.user.email_address
            current_password = serializer.data['current_password']
            user = authenticate(request, email_address=email_address, password=current_password)
            return user

    def post(self,request,*args,**kwargs) :
        user = self.validate_current_password(request)
        if not user :
            raise ValidationError(_('Invalid Current Password.'), code='invalid_current_password')
        update_password(user,request.data['new_password'])
        response = {
            "code" : "password_changed",
            "message" : "Password Succesfully changed."
        }
        notify_user.delay(events.CHANGED_PASSWORD_SUCCESS, { 'user' : user })
        return Response(response,status=status.HTTP_200_OK)


'''
When user selects forgot password, a reset OTP will be sent to linked email.

'''
class ForgotPasswordGenerateOTPView(GenericAPIView) :
    class ForgotPasswordSerializer(serializers.Serializer) :
        email_address = serializers.EmailField()

    authentication_classes = []
    permission_classes = []
    serializer_class = ForgotPasswordSerializer    

    def post(self,request,*args,**kwargs) :
        email_address = request.data['email_address']
        user = User.objects.get(email_address=email_address)

        if not user :
            raise ValidationError(("User with email address does not exist."),code='user_not_found')

        if not user.is_verified_email_address :
            raise ValidationError(("User account is not verified."),code='user_not_verified')

        # token = ''.join(random.choices(string.ascii_lowercase +
        #                      string.digits, k = 16))
        otp = generate_otp()  
        ResetPassword.objects.create(user=user, otp=otp, email_address=email_address)         
        notify_user.delay(events.FORGOT_PASSWORD, { 'user' : user,  'otp' : otp })        
        return Response(status=status.HTTP_200_OK)



'''
When user selects forgot password, a reset OTP will be sent to linked email.
If OTP given in link matches with OTP provided in  POST request body
We will allow to reset password 
'''
class ResetPasswordWithOTP(CreateAPIView) :
    """
    Reset password with OTP    
    
    """
    queryset = ResetPassword.objects.none()
    serializer_class = PasswordOTPSerializer
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True) 
        otp = serializer.validated_data['otp']
        password = serializer.validated_data['password']        
        reset_obj = ResetPassword.objects.filter(otp=otp, created_date__gte = (timezone.now() - timedelta(minutes=30)))
        if len(reset_obj) == 0 :
            raise ValidationError("Invalid or expired OTP.",code='invalid_link')
        user = reset_obj[0].user
        update_password(user, password)
        reset_obj.delete()   
        # forcefully_log_out_user(user.id)
        response = {
            'code' : 'password_changed',
            'message' : 'Password changed successfully.'
        }
        notify_user.delay(events.RESET_PASSWORD_SUCCESS, { 'user' : user })
        return Response(response, status=status.HTTP_200_OK)




class UserAccountDeactivateView(GenericAPIView) :
    class UserAccountDeactivateSerializer(serializers.Serializer) :
        email_address = serializers.EmailField()
        user_deactivation_reason_id = serializers.IntegerField()
        other_reason = serializers.CharField(max_length=1000,allow_null=True)

        def validate_email_address(self,value) :
            if not User.objects.filter(email_address=value).exists() :
                raise ValidationError(_('Invalid email address recieved.'),code='invalid_input')
            return value
    
    serializer_class = UserAccountDeactivateSerializer    

    @transaction.atomic
    def post(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True) 
        validated_data = serializer.validated_data
        email_address = validated_data['email_address']
        user_deactivation_reason_id = validated_data['user_deactivation_reason_id']
        other_reason = validated_data['other_reason']

        user = User.objects.get(email_address=email_address)

        if not user.is_verified_email_address :
            raise ValidationError(("User account is not verified."),code='user_not_verified')     

        user_deactivation = UserDeactivation.objects.create(
            user = user,
            user_deactivation_reason_id = user_deactivation_reason_id,
            other_reason = other_reason
        )            
        if request.user.user_type_id == USER_TYPE['buyer'] :
            if Order.objects.filter(Q(buyer=request.user) & ~Q(order_status_id__in=(ORDER_STATUS['Cancelled'],ORDER_STATUS['Completed']))).exists() :
                raise ValidationError(_('You have active order,cant not dlete your account.'),code='can_not_delete_account')      
        elif  request.user.user_type_id == USER_TYPE['selller'] :
            if Order.objects.filter(Q(selller=request.user) & ~Q(order_status_id__in=(ORDER_STATUS['Cancelled'],ORDER_STATUS['Completed']))).exists() :
                raise ValidationError(_('You have active order,cant not dlete your account.'),code='can_not_delete_account')
        Gig.objects.filter(created_by=user).update(
            is_active=False,
            deleted_by=user,
            deleted_user=user.user_name,
            deleted_date=timezone.now()
            )
        user.is_active = False
        user.deleted_by = user
        user.deleted_user = user.user_name
        user.deleted_date = timezone.now()
        user.save()
        return Response(status=status.HTTP_200_OK)