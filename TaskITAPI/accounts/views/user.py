from django.shortcuts import render
from rest_framework import generics
from accounts.serializers.user import LoggedInUserSerailizer,UserListSerializer,UserProfileDetailsSerializer,\
    BackOfficeUserCreateSerializer
from accounts.models import User,UserVerificationCode,UserOTPVerification,UserEducation,UserPersonalInfo
from rest_framework import status
from rest_framework.response import Response
from rest_framework import serializers
from django.utils import timezone
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db import transaction
from jwtauth.utils import generate_otp
from accounts.values import USER_TYPE,GROUP
from django.db.models import Q
from django.contrib.auth.models import Group
from django.utils.translation import gettext as _
from rest_framework.exceptions import ValidationError,PermissionDenied
from notifications import events
from notifications.tasks import notify_user
from TaskITAPI.validators import validate_mobile_no
import string,random
from TaskITAPI.settings import RESET_LINK,VERIFICATION_LINK
from configs.values import GEN_LEDGER_TYPE,VOUCHER_TYPE
from orders.models import Order,OrderPayment,FinanceTransaction,FinanceTransactionDetails
from configs.models import GenLedger,GenLedgerType


# View for PUT request (endpoint = '/user/firebase-device-id)  
class UpdateFirebaseDeviceIDView(generics.GenericAPIView):
    class FireBaseIDUpdateSerializer(serializers.Serializer) :
        firebase_device_id = serializers.CharField(max_length=1000)

    serializer_class = FireBaseIDUpdateSerializer

    def put(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = request.user
        user.firebase_device_id = serializer.validated_data['firebase_device_id']
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()   
        user.save(update_fields=['firebase_device_id','modified_by','modified_user','modified_date'])
        return Response(status=status.HTTP_200_OK)

'''
Generate OTP to verify Mobile No of User
'''
# View for PUT request (endpoint = '/user/firebase-device-id')  
class MobileVerificationView(generics.GenericAPIView):
    class MobileVerificationSerializer(serializers.Serializer) :
        mobile_no = serializers.CharField(max_length=10)

        def validate_mobile_no(self,value) :
            validate_mobile_no(value)
            user = User.objects.filter(mobile_no=value)
            if len(user) > 0 :
                raise ValidationError(_('Mobile number should be unique.'),code='mobile_no_not_unique')
            return value
   
    serializer_class = MobileVerificationSerializer

    def user_otp_generate(self,user) :
        otp = generate_otp()
        user_verification = UserOTPVerification.objects.create(
            user=user,
            mobile_no = user.mobile_no,
            otp = otp
        )
        return user_verification
        
    @transaction.atomic    
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        mobile_no = serializer.validated_data['mobile_no']
        user = User.objects.get(id=request.user.id)
        user.mobile_no = mobile_no
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()   
        user.save()
        user_verification = self.user_otp_generate(user)
        notify_user.delay(events.MOBILE_VERIFICATION,{ 'user' : user, 'otp' : user_verification.otp, 'mobile_no' : user.mobile_no})
        return Response(status=status.HTTP_200_OK)        

'''
Generate Resend OTP to verify Mobile No of User
'''
class MobileVerificationResendOTPView(generics.GenericAPIView):

    def user_otp_generate(self,user) :
        otp = generate_otp()
        user_verification = UserOTPVerification.objects.create(
            user=user,
            mobile_no = user.mobile_no,
            otp = otp
        )
        return user_verification

    @transaction.atomic    
    def post(self,request) :      
        user = User.objects.get(id=request.user.id)       
        user_verification = self.user_otp_generate(user)
        notify_user.delay(events.MOBILE_VERIFICATION,{ 'user' : user, 'otp' : user_verification.otp, 'mobile_no' : user.mobile_no})
        return Response(status=status.HTTP_200_OK)    





'''
OTP to verify Mobile No of User
'''
# View for PUT request (endpoint = 'mobile-no-verify')  
class MobileVerifyView(generics.GenericAPIView):
    class MobileVerifySerializer(serializers.Serializer) :
        otp = serializers.CharField(max_length=10)

    serializer_class = MobileVerifySerializer
        
    @transaction.atomic    
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.get(id=request.user.id)
        otp = serializer.validated_data['otp']       
        user_verification = UserOTPVerification.objects.filter(otp=otp,user=user)     
        if len(user_verification) == 0 :
            raise ValidationError(_('User OTP not found.'),code='otp_not_found')
        user.is_verified_mobile_no = True
        user.verified_mobile_no_date = timezone.now()
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()
        user.save()
        return Response(status=status.HTTP_200_OK)  


# View for PUT request (endpoint = '/user/firebase-device-id)  
class SwitchUserView(generics.GenericAPIView):
    """
    Note: Pass value as buyer/seller in switch_user_in field
    """
    class SwitchUserSerializer(serializers.Serializer) :
        switch_user_in = serializers.CharField(max_length=10)

    serializer_class = SwitchUserSerializer
    
    @transaction.atomic    
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        is_seller = False
        # user = request.user
        user = User.objects.get(id=request.user.id)
        if serializer.validated_data['switch_user_in'].lower() == 'seller' :
            is_seller = True
            user.user_type_id = USER_TYPE['seller']
        else :            
            user.user_type_id = USER_TYPE['buyer']    
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()   
        user.save()
        if is_seller == True :
            group = Group.objects.get(id=GROUP['seller'])
        else :
            group = Group.objects.get(id=GROUP['buyer']) 
        user_group = User.groups.through.objects.get(user=user)
        user_group.group = group
        user_group.save()
        return Response(status=status.HTTP_200_OK)   

'''
View to receive data in specific format for GET method(endpoint = '/users/me')
'''
class LoggedInUserView(generics.GenericAPIView) :
    """
    Get loggedin user details
    
    Get loggedin user details
    """
    queryset = UserPersonalInfo.objects.none()
    serializer_class = LoggedInUserSerailizer

    def get(self,request,*args,**kwargs) :  
        try:
            user = UserPersonalInfo.objects.get(user=request.user)
        except UserPersonalInfo.DoesNotExist:
            user = None    
        Serializer = self.serializer_class(user)
        return Response(Serializer.data,status=status.HTTP_200_OK)


# View for GET/POST request (endpoint = '/user/backoffice-list')
class BackOfficeUserCreateView(generics.CreateAPIView) :
    serializer_class = BackOfficeUserCreateSerializer




# View for GET/POST request (endpoint = '/user/backoffice-list')
class UserListView(generics.ListAPIView) :
    """
    User List 
    
    Query parameters for GET method
    ---------------------------------------
    1. user_name = To filter by User Name   
    2. email_address = To filter by Email Address
    3. level_id = To filter by Service Level ID
    4. is_active = true/false   

    E.g. 127.0.0.1:8000/api/v1/user/backoffice-list?user_name=&email_address&level_id&is_active   

    """
    serializer_class = UserListSerializer
    pagination_class = StandardResultsSetPagination

    def apply_filters(self,queryset) :
        q_objects = Q()
        user_name = self.request.GET.get('user_name')
        email_address = self.request.GET.get('email_address')       
        level_id = self.request.GET.get('level_id')
        if user_name :
            q_objects.add(Q(user_name__icontains=user_name), Q.AND)  
        if email_address :
            q_objects.add(Q(email_address=email_address), Q.AND)     
        if level_id :
            q_objects.add(Q(level_id=level_id), Q.AND) 
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        if is_active == 'false' :
            queryset = User.all_objects.filter(is_active=False)
        else :    
            queryset = User.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


# View for GET/PUT/PATCH request (endpoint = '/user/register/:id)  
class UserDetailsView(generics.RetrieveUpdateAPIView):
    queryset = User.objects.all()
    serializer_class = UserListSerializer

    def get_queryset(self) :
        queryset = User.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        user = self.get_object()
        user.is_active = False
        user.deleted_by = request.user
        user.deleted_user = request.user.user_name
        user.deleted_date = timezone.now()
        user.save()
        return Response(status=status.HTTP_204_NO_CONTENT)        


# View for GET/PUT/PATCH request (endpoint = '/user/register/:id)  
class UserProfileDetailsView(generics.RetrieveUpdateAPIView) :
    queryset = User.objects.all()
    serializer_class = UserProfileDetailsSerializer

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        if is_active == 'false' :
            queryset = User.objects.filter(is_active=False)
        else :
            queryset = User.objects.all() 
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset



# View for Resend Registration Link ( POST method )
class ResendRegistrationLinkView(generics.CreateAPIView) :
    """
    Re-send registration link to User (Seller,Buyer)
  
    """
    class ResendRegistrationSerializer(serializers.Serializer) :
        user_id = serializers.IntegerField()
  
    permission_classes = []
    serializer_class = ResendRegistrationSerializer

    def post(self, request, *args, **kwargs):        
        user_id = request.data['user_id']
        user = User.objects.filter(id=user_id)       
      
        if len(user) == 0 :
            raise NotFound(_("User with user id does not exist."), code='user_not_found')
        user = user[0]   
       
        if user.is_verified_email_address :
            raise ValidationError(_('User account is verified.'),code='user_already_verified')
        
        token = ''.join(random.choices(string.ascii_lowercase +
                             string.digits, k = 16)) 
        UserVerificationCode.objects.create(
            user=user,
            created_date=timezone.now(),
            verification_code=token
        )
        notify_user.delay(events.REGISTER_USER, { 'user' : user,  'verification_link' : VERIFICATION_LINK + token })       
        return Response(status=status.HTTP_200_OK)


class UserWalletBalancetView(generics.GenericAPIView) :
    """
    User wallet balance
  
    """
    def get(self, request, *args, **kwargs) :
        if request.user.is_seller != True :
            raise PermissionDenied
        from django.db.models import Q,Sum
        gen_ledger = GenLedger.objects.filter(user=request.user,gen_ledger_type_id=GEN_LEDGER_TYPE['user_wallet'])
        fin_tran_dtl = FinanceTransactionDetails.objects.filter(
            gen_ledger_id = gen_ledger[0].id,
            fin_tran_id__in = FinanceTransaction.objects.filter(
                voucher_type_id__in=(
                    VOUCHER_TYPE['nodal_to_wallet'],
                    VOUCHER_TYPE['wallet_to_bank'],
                    VOUCHER_TYPE['wallet_to_nodal_partial_payment'],
                    VOUCHER_TYPE['payment_through_wallet']
                    )
                ).values_list('id')
        ).aggregate(total_balance_amount=Sum('amount'))    
        total_balance_amount = 0 if not fin_tran_dtl['total_balance_amount']  else fin_tran_dtl['total_balance_amount'] 
        data = {
            'total_balance_amount' : total_balance_amount
        }
        return Response(data, status=status.HTTP_200_OK)  

