from django.shortcuts import render
from rest_framework import generics
from accounts.serializers import UserEducationListSerializer,UserSocialAccountListSerializer,UserAccountVerifyDetailsSerializer,UserLanguageListSerializer,UserSkillListSerializer,\
    UserCertificationSerializer,UniversityListSerializer,UserAddressListSerializer,ServiceLevelListSerializer,UserTypeListSerializer,SellerLevelGigsListSerializer,\
        UserBankListSerializer
from accounts.models import User,Level,UserOTPVerification,UserEducation,UserSocialAccount,UserLanguage,UserSkill,UserCertification,UserAddress,UserType,UserBank
from rest_framework import status
from rest_framework.response import Response
from rest_framework import serializers
from django.utils import timezone
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db import transaction
from jwtauth.utils import generate_otp
from accounts.values import USER_TYPE
from django.db.models import Q
from django.db import connection



# View for GET/POST request (endpoint = '/user/educations)
class UserEducationList(generics.ListAPIView) :
    """
    Category List 
    
    Query parameters for GET method
    ---------------------------------------
    1. keyword = To filter by university_name      

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. 127.0.0.1:8000/api/v1/user/university-list/?keyword=vjti   

    """
    queryset = UserEducation.objects.none()
    serializer_class = UniversityListSerializer
   
    def get(self, request, *args, **kwargs) :       
        res = []    
        keyword=self.request.GET.get('keyword')
        if not keyword :
            keyword = ''
        with connection.cursor() as cursor:
            cursor.callproc('sp_get_university_list',[keyword])  
            from TaskITAPI.serializers import StoredProcedureSerializer         
            data = StoredProcedureSerializer(cursor).data                     
            if type(data) == dict and not data :
               result = [] 
            elif len(data) == 1 :
               res.append(data)  
            if len(data) > 1 :
                result = data
            else :
                result = res                 
        return Response(result, status=status.HTTP_200_OK)         


# View for GET/POST request (endpoint = '/user/social-accounts-linked')
class UserSocialAccountList(generics.ListAPIView) :
    """
    User Social Account List 
    
    Query parameters for GET method  

    E.g. 127.0.0.1:8000/api/v1/user/social-accounts-linked/1   

    """
    serializer_class = UserSocialAccountListSerializer
    permission_classes = []
    authentication_classes = []
    # pagination_class = StandardResultsSetPagination

    def get_queryset(self) :     
        user_id = self.kwargs['user_id']  
        queryset = UserSocialAccount.objects.filter(user_id=user_id)
        # queryset = self.get_serializer_class().setup_eager_loading(self,queryset)             
        return queryset


# View for GET/POST request (endpoint = '/user/social-accounts-verify-details')
class UserAccountVerifyDetails(generics.RetrieveAPIView) :
    """
    User Social Account List 
    
    Query parameters for GET method  

    E.g. 127.0.0.1:8000/api/v1/user/social-accounts-linked/1   

    """
    queryset = User.objects.all()
    serializer_class = UserAccountVerifyDetailsSerializer   

    def get_queryset(self) :
        queryset = User.objects.all()
        # queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


# View for GET/POST request (endpoint = '/user/languages')
class UserLanguageCreateView(generics.CreateAPIView) :
    serializer_class = UserLanguageListSerializer


# View for GET/POST request (endpoint = '/user/languages')
class UserLanguageDetailsView(generics.RetrieveUpdateAPIView) :
    queryset = UserLanguage.objects.all()
    serializer_class = UserLanguageListSerializer

    def get_queryset(self) :
        queryset = UserLanguage.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        user_language = self.get_object()
        user_language.is_active = False
        user_language.deleted_by = request.user
        user_language.deleted_user = request.user.user_name
        user_language.deleted_date = timezone.now()
        user_language.save()
        return Response(status=status.HTTP_204_NO_CONTENT)    


# View for GET/POST request (endpoint = '/user/skills')
class UserSkillsCreateView(generics.CreateAPIView) :
    serializer_class = UserSkillListSerializer


# View for GET/POST request (endpoint = '/user/skills')
class UserSkillsDetailsView(generics.RetrieveUpdateAPIView) :
    queryset = UserSkill.objects.all()
    serializer_class = UserSkillListSerializer

    def get_queryset(self) :
        queryset = UserSkill.objects.all()
        # queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        user_skill = self.get_object()
        user_skill.is_active = False
        user_skill.deleted_by = request.user
        user_skill.deleted_user = request.user.user_name
        user_skill.deleted_date = timezone.now()
        user_skill.save()
        return Response(status=status.HTTP_204_NO_CONTENT)    


# View for GET/POST request (endpoint = '/user/educations')
class UserEducationCreateView(generics.CreateAPIView) :
    serializer_class = UserEducationListSerializer


# View for GET/POST request (endpoint = '/user/educations')
class UserEducationDetailsView(generics.RetrieveUpdateAPIView) :
    queryset = UserEducation.objects.all()
    serializer_class = UserEducationListSerializer

    def get_queryset(self) :
        queryset = UserEducation.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        user_education = self.get_object()
        user_education.is_active = False
        user_education.deleted_by = request.user
        user_education.deleted_user = request.user.user_name
        user_education.deleted_date = timezone.now()
        user_education.save()
        return Response(status=status.HTTP_204_NO_CONTENT)       


# View for GET/POST request (endpoint = '/user/certifications')
class UserCertificationsCreateView(generics.CreateAPIView) :
    serializer_class = UserCertificationSerializer


# View for GET/POST request (endpoint = '/user/certifications')
class UserCertificationsDetailsView(generics.RetrieveUpdateAPIView) :
    queryset = UserCertification.objects.all()
    serializer_class = UserCertificationSerializer

    def get_queryset(self) :
        queryset = UserCertification.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        user_certification = self.get_object()
        user_certification.is_active = False
        user_certification.deleted_by = request.user
        user_certification.deleted_user = request.user.user_name
        user_certification.deleted_date = timezone.now()
        user_certification.save()
        return Response(status=status.HTTP_204_NO_CONTENT)           


# View for POST request (endpoint = '/user/addresses')
class UserAddressCreateView(generics.CreateAPIView) :
    serializer_class = UserAddressListSerializer


# View for GET request (endpoint = '/user/addresses-list')
class UserAddressListView(generics.ListAPIView) :
    queryset = UserAddress.objects.all()
    serializer_class = UserAddressListSerializer

    def get_user_data(self,queryset) :
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def get_queryset(self) :
        queryset = UserAddress.objects.all()
        queryset = self.get_user_data(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


# View for GET/POST request (endpoint = '/user/addresses/:id')
class UserAddressDetailsView(generics.RetrieveUpdateAPIView) :
    queryset = UserAddress.objects.all()
    serializer_class = UserAddressListSerializer

    def get_queryset(self) :
        queryset = UserAddress.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        user_address = self.get_object()
        user_address.is_active = False
        user_address.deleted_by = request.user
        user_address.deleted_user = request.user.user_name
        user_address.deleted_date = timezone.now()
        user_address.save()
        return Response(status=status.HTTP_204_NO_CONTENT) 


class UserBankCreateListView(generics.ListCreateAPIView) :
    queryset = UserBank.objects.all()
    serializer_class = UserBankListSerializer

    def get_user_data(self,queryset) :
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def get_queryset(self) :
        queryset = UserBank.objects.all()
        queryset = self.get_user_data(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


class UserBankDetailsView(generics.RetrieveUpdateAPIView) :
    queryset = UserBank.objects.all()
    serializer_class = UserBankListSerializer

    def get_queryset(self) :
        queryset = UserBank.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        user_bank = self.get_object()
        user_bank.is_active = False
        user_bank.deleted_by = request.user
        user_bank.deleted_user = request.user.user_name
        user_bank.deleted_date = timezone.now()
        user_bank.save()
        return Response(status=status.HTTP_204_NO_CONTENT) 


# View for GET/POST request (endpoint = '/service_level/)
class ServiceLevelListView(generics.ListAPIView) :
    """
    Service Level List 
    
    Query parameters for GET method
    ---------------------------------------
    1. level_name = To filter by level_name   
   
    E.g. 127.0.0.1:8000/api/v1/service_level/?level_name=level1   

    """
    serializer_class = ServiceLevelListSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self) :
        q_objects = Q()
        level_name =  self.request.GET.get('level_name')       
        queryset = Level.objects.all()
        if level_name :    
            q_objects.add(Q(level_name__icontains=level_name), Q.AND)        
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)       
        return queryset




# View for GET/POST request (endpoint = '/service_level/seller-gigs')
class SellerLevelGigsListView(generics.ListAPIView) :
    """
    Seller Level List 
    
    Query parameters for GET method
    ---------------------------------------
    1. level_name = To filter by level_name   
    2. search = To filter by Category/GigsTags/   
   
    E.g. 127.0.0.1:8000/api/v1/service_level/seller-gigs?level_name=level1   

    """
    serializer_class = SellerLevelGigsListSerializer
    authentication_classes = []
    permission_classes = []

    def get_queryset(self) :
        queryset = Level.objects.all()        
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)   
        return queryset



# View for GET/POST request (endpoint = '/user_type/)
class UserTypeListView(generics.ListAPIView) :
    """
    Service Level List 
    
    Query parameters for GET method
    ---------------------------------------
    1. user_type_name = To filter by user_type_name   
   
    E.g. 127.0.0.1:8000/api/v1/user_type/?user_type_name=seller   

    """
    serializer_class = UserTypeListSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self) :
        q_objects = Q()
        user_type_name =  self.request.GET.get('user_type_name')       
        queryset = UserType.objects.all()
        if user_type_name :    
            q_objects.add(Q(user_type_name__icontains=user_type_name), Q.AND)        
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)       
        return queryset

