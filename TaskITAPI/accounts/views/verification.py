from rest_framework.generics import CreateAPIView
from accounts.models import UserVerificationCode
from rest_framework import serializers
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.hashers import make_password
from rest_framework import generics
from django.db.models import Q
from accounts.models import User
from django.urls import reverse
from notifications import events
from notifications.tasks import notify_user



class VerificationSerializer(serializers.Serializer) :
    verification_code = serializers.CharField(max_length=16)    

class VerifyUserAccount(CreateAPIView) :
    """
    Verify code and set password
    
    Verify code and set password
    """
    queryset = UserVerificationCode.objects.none()
    serializer_class = VerificationSerializer
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True) 
        verification_code = serializer.validated_data['verification_code']
        is_verified = False
        user_id = None
        user_name = None
        user_type = None
        email_address = None
        is_verified_email_address = None
        verification = UserVerificationCode.objects.filter(verification_code=verification_code)
        if len(verification) > 0 :
            verification = verification[0]
            user = verification.user           
            user.is_verified_email_address = True
            user.verified_email_address_date = timezone.now()
            user.modified_date = user.modified_date
            user.password_modified_date = timezone.now()
            user.save()  
            is_verified = True
            user_id = user.id
            user_name = user.user_name
            user_type = {'user_type_id' : user.user_type_id,'user_type_name' : user.user_type.user_type_name}
            email_address = user.email_address
            is_verified_email_address = user.is_verified_email_address
            verification.delete()
            notify_user.delay(events.USER_ACCOUNT_ACTIVATED,{'user':user})
        response = {
            'is_verified' : is_verified,        
            'user_id' : user_id,
            'user_name' : user_name,
            'user_type' : user_type,
            'email_address' : email_address,
            'is_verified_email_address' : is_verified_email_address   
        }       
        return Response(response, status=status.HTTP_200_OK)



class UserEmailVerified(generics.GenericAPIView) :
    """
    Verify email and mobile duplicate or not.

    ---------------------------------------   
    """
    class VerifySerializer(serializers.Serializer) :
        email_address = serializers.EmailField(required=False)

    serializer_class = VerifySerializer
    permission_classes = []    
    authentication_classes = []

    def post(self, request, *args, **kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)       
        email_address = None     
        is_email_exist = False      

        if 'email_address' in serializer.validated_data :
            email_address = serializer.validated_data['email_address']
            
        if email_address :
                is_email_exist = User.objects.filter(email_address=email_address).exists()

        response = {
            'is_email_exist' : is_email_exist,           
        }
        return Response(response, status=status.HTTP_200_OK)
  
