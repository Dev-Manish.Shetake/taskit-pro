FROM python:3.7-slim

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN set -ex \
    && RUN_DEPS=" \
        libpcre3 \
        mime-support \
        postgresql-client \
    " \
    && seq 1 8 | xargs -I{} mkdir -p /usr/share/man/man{} \
    && apt-get update && apt-get install -y --no-install-recommends $RUN_DEPS \
    && rm -rf /var/lib/apt/lists/*

# Copy in your requirements file
COPY requirements.txt ./

RUN set -ex \
    && BUILD_DEPS=" \
        build-essential \
        libpcre3-dev \
        libpq-dev \
    " \
    && apt-get update && apt-get install -y --no-install-recommends $BUILD_DEPS \
    && apt-get install -y --no-install-recommends default-libmysqlclient-dev \
    && pip install -U pip \
    && pip install --no-cache-dir -r /requirements.txt 

# Copy your application code to the container (make sure you create a .dockerignore file if any large files or directories should be excluded)
# RUN mkdir /speed/
# WORKDIR /speed/
# COPY . /speed/

# uWSGI will listen on this port
EXPOSE 8000
