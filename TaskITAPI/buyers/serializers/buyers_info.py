from rest_framework import serializers
from accounts.models import User,UserPersonalInfo,UserLanguage,UserSocialAccount,UserSkill,UserEducation,UserCertification
from django.db import transaction
from rest_framework.exceptions import PermissionDenied
from sellers.serializers.seller_general import UserLanguageSerializer,UserLanguageMinSerializer
from accounts.serializers import UserMinSerializer,UserLanguageListSerializer,UserSocialAccountListSerializer,UserSkillListSerializer,UserEducationListSerializer,\
    UserCertificationSerializer
from django.db.models import Prefetch



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/buyers/info/:id)
'''
class BuyerPersonalInfoSerializer(serializers.ModelSerializer) :
       
    def setup_eager_loading(self,queryset) :
        return queryset

    class Meta :
        model = UserPersonalInfo
        fields = [
            'id', 
            'first_name',
            'last_name',
            'profile_picture',
            'description',  
        ]


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/buyers/info/:id)
'''
class BuyerInfoSerializer(serializers.ModelSerializer) :   
    personal_info = BuyerPersonalInfoSerializer()
    user_languages = UserLanguageListSerializer(many=True)
    accounts = UserSocialAccountListSerializer(many=True)
    skills = UserSkillListSerializer(many=True)
    educations = UserEducationListSerializer(many=True)
    certifications = UserCertificationSerializer(many=True)

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('userpersonalinfo',UserPersonalInfo.objects.all(),to_attr='personal_info'),
            Prefetch('userlanguage_set',UserLanguage.objects.all(),to_attr='user_languages'),
            Prefetch('usersocialaccount_set',UserSocialAccount.objects.all(),to_attr='accounts'),
            Prefetch('userskill_set',UserSkill.objects.all(),to_attr='skills'),
            Prefetch('usereducation_set',UserEducation.objects.all(),to_attr='educations'),
            Prefetch('usereducation_set',UserCertification.objects.all(),to_attr='certifications'),
        )       
        return queryset

    class Meta :
        model = User
        fields = [
            'id',            
            'user_name',
            'rated_no_of_records',
            'created_date',
            'rating',     
            'personal_info',
            'user_languages',
            'accounts',
            'skills',
            'educations',
            'certifications'      
        ]        



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/buyers/personal-info/:id)
'''
class BuyerPersonalInfoDetailsSerializer(serializers.ModelSerializer) :
    profile_picture = serializers.FileField(required=False)
    personal_website = serializers.CharField(required=False)
   
    def setup_eager_loading(self,queryset) :
        return queryset

    class Meta :
        model = UserPersonalInfo
        fields = [
            'id',
            'first_name',
            'last_name',
            'profile_picture',
            'description',
            'personal_website',           
        ]
    
   
    def update(self, user_personal_info, validated_data) :       
        user_update_fields = []
        for key, value in validated_data.items() :
            if getattr(user_personal_info, key) != value :
                user_update_fields.append(key)
                setattr(user_personal_info, key, value)        
        user_personal_info.save(update_fields = user_update_fields)
        return user_personal_info    
