from django.urls import path
from buyers.views import buyers_info

urlpatterns =[
    path('user-favourite-gigs',buyers_info.UserFavouriteGigsCreateView.as_view()),    
    path('info/<int:user_id>',buyers_info.BuyerInfoView.as_view()),
    path('personal-info/<int:pk>',buyers_info.BuyerPersonalInfoDetailsView.as_view()),
]