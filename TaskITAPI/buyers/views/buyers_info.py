from rest_framework import views
from rest_framework.generics import RetrieveAPIView,GenericAPIView,CreateAPIView,RetrieveUpdateAPIView
from django.utils import timezone
from django.db import transaction
from accounts.models import User,UserPersonalInfo
from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response
from gigs.models import UserFavouriteGigs
from buyers.serializers import BuyerInfoSerializer,BuyerPersonalInfoDetailsSerializer
from rest_framework.exceptions import PermissionDenied
from accounts.values import USER_TYPE


# View for PUT request (endpoint = '/user/firebase-device-id)  
class UserFavouriteGigsCreateView(generics.GenericAPIView):
    """
    Note: Pass value as true/false in 'is_favourite' field
        favourite Add pass is_favourite = true
        favourite remove pass is_favourite = true
    """
    class UserFavouriteGigsCreateSerializer(serializers.Serializer) :
        gig_id = serializers.IntegerField()

    serializer_class = UserFavouriteGigsCreateSerializer
    
    @transaction.atomic    
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        is_favourite = request.GET.get('is_favourite')
        user = request.user       
        if is_favourite.lower() == 'true' :
            UserFavouriteGigs.objects.create(
                gig_id = serializer.validated_data['gig_id'],
                created_by = user,
                created_user = user.user_name,
                created_date =  timezone.now()   
            )     
            res = {
                'code' : 'gig_favourite_added',
                'message' : 'Gigs favourite added'
            }
        elif is_favourite.lower() == 'false' :
            UserFavouriteGigs.objects.filter(gig_id=serializer.validated_data['gig_id'], created_by = user).delete()
            res = {
                'code' : 'gig_favourite_removed',
                'message' : 'Gigs favourite removed'
            }
        return Response(res,status=status.HTTP_200_OK)   


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/buyers/info')
'''
class BuyerInfoView(RetrieveAPIView) :
    queryset = User.objects.all()
    serializer_class = BuyerInfoSerializer  
    lookup_field = 'id'
    lookup_url_kwarg = 'user_id'   

    def get_queryset(self) :       
        queryset = User.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/buyers/personal-info/:id')
'''
class BuyerPersonalInfoDetailsView(RetrieveUpdateAPIView) :
    queryset = UserPersonalInfo.objects.all()
    serializer_class = BuyerPersonalInfoDetailsSerializer

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = UserPersonalInfo.all_objects.all().filter(is_active=False)
        else : 
            queryset = UserPersonalInfo.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

    def delete(self,request,*args,**kwargs) :
        user_personal_info = self.get_object()
        user_personal_info.is_active = False
        user_personal_info.deleted_by = request.user
        user_personal_info.deleted_user = request.user.user_name
        user_personal_info.deleted_date = timezone.now()
        user_personal_info.save()       
        return Response(status=status.HTTP_204_NO_CONTENT)        