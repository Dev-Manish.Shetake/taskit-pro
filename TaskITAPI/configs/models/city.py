from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from configs.models import State


class City(SoftDeleteModel) :
    state = models.ForeignKey(State, on_delete=models.CASCADE)
    code = models.CharField(max_length=10,default=None)
    city_name = models.CharField(max_length=100)
    seq_grp = models.BooleanField()
    seq_no = models.IntegerField()
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name = 'city_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='city_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True,null=True,blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='city_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    translated_fields = ['city_name']

    class Meta :
        db_table = 'mst_city'
        unique_together = [
            'city_name','state','code'
        ]
        default_permissions = ()
