from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User


class Country(SoftDeleteModel) :
    code = models.CharField(max_length=10)
    country_name = models.CharField(max_length=100)
    seq_grp = models.BooleanField(default=False)
    seq_no = models.IntegerField()
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name = 'country_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='country_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='country_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

   
    class Meta :
        db_table = 'mst_country'
        verbose_name = 'Country'
        verbose_name_plural  = 'Countries'
        unique_together = [
            'code','country_name'
        ]
        default_permissions = ()
