from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User


# Master Social Media related data stored in this table
class EducationTitle(SoftDeleteModel) :
    education_title_name = models.CharField(max_length=100, unique=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'education_title_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='education_title_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='education_title_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_education_title'
        default_permissions = ()


# Master Social Media related data stored in this table
class EducationMajor(SoftDeleteModel) :
    education_major_name = models.CharField(max_length=100)
    is_approved = models.BooleanField(null=True)
    approved_by = models.ForeignKey(User, on_delete=models.DO_NOTHING,null=True, related_name = 'education_major_approved_by')
    approved_user = models.CharField(max_length=50, null=True)
    approved_date = models.DateTimeField(null=True) 
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'education_major_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='education_major_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='education_major_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_education_major'
        default_permissions = ()