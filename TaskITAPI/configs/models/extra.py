from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User

# Master ExtraDays related data stored in this table
class ExtraDays(SoftDeleteModel) :
    seq_no = models.IntegerField()
    extra_days_name = models.CharField(max_length=100)
    extra_days_value = models.IntegerField()
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'extra_days_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='extra_days_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='extra_days_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_extra_days'
        default_permissions = ()


# Master ExtraPrice related data stored in this table
class ExtraPrice(SoftDeleteModel) :
    seq_no = models.IntegerField()
    extra_price_name = models.CharField(max_length=100)
    extra_price_value = models.IntegerField()
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'extra_price_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='extra_price_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='extra_price_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_extra_price'
        default_permissions = ()
