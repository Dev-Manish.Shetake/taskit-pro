from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User


# Master Langauges related data stored in this table
class Language(SoftDeleteModel) :
    code = models.CharField(max_length=10, unique=True)
    lang_name = models.CharField(max_length=100, unique=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'language_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='language_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='language_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_lang'
        default_permissions = ()



# Master Langauges related data stored in this table
class LanguageProficiency(SoftDeleteModel) :   
    lang_proficiency_name = models.CharField(max_length=100,unique=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'language_proficiency_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='language_proficiency_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='language_proficiency_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_lang_proficiency'
        default_permissions = ()


