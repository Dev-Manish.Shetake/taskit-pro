from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User


# Master Skills related data stored in this table
class Skill(SoftDeleteModel) :
    skill_name = models.CharField(max_length=100, unique=True)
    is_approved = models.BooleanField(null=True)
    approved_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'skill_approved_by')
    approved_user = models.CharField(max_length=50, null=True)
    approved_date = models.DateTimeField(null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'skill_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='skill_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='skill_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_skill'
        default_permissions = ()



# Master Skill Level related data stored in this table
class SkillLevel(SoftDeleteModel) :
    skill_level_name = models.CharField(max_length=100, unique=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'skill_level_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='skill_level_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='skill_level_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_skill_level'
        default_permissions = ()


# Master Skill Suggestion related data stored in this table
class SkillSuggestion(SoftDeleteModel) :
    skill_id = models.ForeignKey(Skill,on_delete=models.DO_NOTHING,related_name='skill_id')
    suggestion_skill = models.ForeignKey(Skill,on_delete=models.DO_NOTHING,related_name='skill_suggestion_id')
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'skill_suggestion_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='skill_suggestion_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='skill_suggestion_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_skill_suggestion'
        default_permissions = ()        