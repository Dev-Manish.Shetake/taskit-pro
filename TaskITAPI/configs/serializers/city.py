from rest_framework import serializers
from configs.models import City
from .state import StateSerializer

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/city/)
'''
class CitySerializer(serializers.ModelSerializer) :
    state = StateSerializer()

    class Meta :
        model = City
        fields = [
            'id',
            'city_name',
            'state'
        ]
