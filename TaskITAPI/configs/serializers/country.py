from rest_framework import serializers
from configs.models import Country

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/occupations/)
'''
class CountrySerializer(serializers.ModelSerializer) :

    class Meta :
        model = Country
        fields = [
            'id',
            'country_name'
        ]
