from rest_framework import serializers
from configs.models import UserDeactivationReason

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/deactivation_reason/)
'''
class UserDeactivationReasonSerializer(serializers.ModelSerializer) :

    class Meta :
        model = UserDeactivationReason
        fields = [
            'id',
            'head_name',
            'seq_no',
            'user_deactivate_reason_name',
            'created_date'
        ]
