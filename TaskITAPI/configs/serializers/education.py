from rest_framework import serializers
from configs.models import EducationTitle,EducationMajor
from django.db import transaction 
from django.utils import timezone
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/education/education-title')
'''
class EducationTitleSerializer(serializers.ModelSerializer) :

    class Meta :
        model = EducationTitle
        fields = [
        'id',
        'education_title_name'
        ]


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/education/education-major')
'''
class EducationMajorSerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self,queryset) :
        return queryset

    class Meta :
        model = EducationMajor
        fields = [
        'id',
        'education_major_name'
        ]
        
    @transaction.atomic
    def create(self,validated_data) :
        education_major_name = validated_data['education_major_name'].strip()
        education_major = EducationMajor.objects.filter(education_major_name=education_major_name)
        if len(education_major) :
            education_major = education_major[0]
        else :   
            education_major = EducationMajor.objects.create(
                **validated_data,
                created_by = self.context['request'].user,
                created_user = self.context['request'].user.user_name,
                created_date = timezone.now()
            )
        return education_major
       