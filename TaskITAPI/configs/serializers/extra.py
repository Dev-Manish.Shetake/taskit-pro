from rest_framework import serializers
from configs.models import ExtraDays,ExtraPrice

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/')
'''
class ExtraDaysSerializer(serializers.ModelSerializer) :

    class Meta :
        model = ExtraDays
        fields = [
            'id',
            'seq_no',
            'extra_days_name',
            'extra_days_value'
        ]

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/')
'''
class ExtraPriceSerializer(serializers.ModelSerializer) :

    class Meta :
        model = ExtraPrice
        fields = [
            'id',
            'seq_no',
            'extra_price_name',
            'extra_price_value'
        ]        