from rest_framework import serializers
from configs.models import GigGalleryType

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '')
'''
class GigGalleryTypeSerializer(serializers.ModelSerializer) :

    class Meta :
        model = GigGalleryType
        fields = [
            'id',
            'gig_gallery_type_name'
        ]