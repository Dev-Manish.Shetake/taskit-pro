from rest_framework import serializers
from configs.models import GigStatus

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/gig-status/)
'''
class GigStatusSerializer(serializers.ModelSerializer) :

    class Meta :
        model = GigStatus
        fields = [
        'id',
        'gig_status_name'
        ]