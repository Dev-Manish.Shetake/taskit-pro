from rest_framework import serializers
from configs.models import Language,LanguageProficiency
from gigs.models import Gig,GigSearchTag
from accounts.models import User
from django.db.models import Q

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/language/)
'''
class LanguageSerializer(serializers.ModelSerializer) :

    class Meta :
        model = Language
        fields = [
        'id',
        'code',
        'lang_name'
        ]

    def setup_eager_loading(self,queryset) :
        return queryset

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/language-proficiency)
'''
class LanguageProficiencySerializer(serializers.ModelSerializer) :

    class Meta :
        model = LanguageProficiency
        fields = [
        'id',
        'lang_proficiency_name'
        ]

    def setup_eager_loading(self,queryset) :
        return queryset        


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/language/seller-gigs)
'''
class SellerLanguageGigsSerializer(serializers.ModelSerializer) :
    user_count = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) :
        return queryset 

    class Meta :
        model = Language
        fields = [
        'id',
        'code',
        'lang_name',
        'user_count'
        ]

    def get_user_count(self, instance) :
        search = self.context['request'].GET.get('search')  
        category_name = self.context['request'].GET.get('category_name')  
        sub_category_name = self.context['request'].GET.get('sub_category_name')  
        search_tags = self.context['request'].GET.get('search_tags')       
        user_count = 0
        if search :                 
            gig_ids = GigSearchTag.objects.filter(search_tags__icontains=search).values_list('gig_id')  
            gigs = Gig.objects.filter(Q(id__in=gig_ids) | Q(category__category_name__icontains=search) | Q(sub_category__sub_category_name__icontains=search))
            user_ids = gigs.values_list('created_by_id')        
            user_count = User.objects.filter(id__in=user_ids,lang_id=instance.id).count() 
        if category_name :  
            gigs = Gig.objects.filter(Q(category__category_name__icontains=category_name))
            user_ids = gigs.values_list('created_by_id')        
            user_count = User.objects.filter(id__in=user_ids,lang_id=instance.id).count() 
        if sub_category_name :
            gigs = Gig.objects.filter(Q(sub_category__sub_category_name__icontains=sub_category_name))
            user_ids = gigs.values_list('created_by_id')        
            user_count = User.objects.filter(id__in=user_ids,lang_id=instance.id).count() 
        if search_tags :
            gig_ids = GigSearchTag.objects.filter(search_tags__icontains=search).values_list('gig_id')  
            user_ids = gigs.values_list('created_by_id') 
            user_count = User.objects.filter(id__in=user_ids,lang_id=instance.id).count()        
        return user_count    

         