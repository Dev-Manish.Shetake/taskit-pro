from rest_framework import serializers
from configs.models import Occupation

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/occupations/)
'''
class OccupationSerializer(serializers.ModelSerializer) :

    class Meta :
        model = Occupation
        fields = [
            'id',
            'occupation_name'
        ]
