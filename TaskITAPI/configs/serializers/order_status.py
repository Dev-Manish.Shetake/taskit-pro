from rest_framework import serializers
from configs.models import OrderStatus,OrderPaymentStatus,OrderCancelReason

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/order_status/)
'''
class OrderStatusSerializer(serializers.ModelSerializer) :

    class Meta :
        model = OrderStatus
        fields = [
        'id',
        'order_status_name',        
        ]

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/order_payment_status/)
'''
class OrderPaymentStatusSerializer(serializers.ModelSerializer) :

    class Meta :
        model = OrderPaymentStatus
        fields = [
        'id',
        'order_payment_status_name',                
        ]        

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/order_cancel_reason/)
'''
class OrderCancelReasonSerializer(serializers.ModelSerializer) :

    class Meta :
        model = OrderCancelReason
        fields = [
        'id',
        'order_cancel_reason_name',                
        ]                