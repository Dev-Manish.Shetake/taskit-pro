from rest_framework import serializers
from configs.models import PriceType, PriceScopeControlType
from masters.models import PriceExtraService
from django.db import transaction
from django.utils import timezone
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _ 

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/price_type/)
'''
class PriceTypeSerializer(serializers.ModelSerializer) :

    class Meta :
        model = PriceType
        fields = [
            'id',
            'price_type_name'
        ]

class PriceScopeControlTypeMinSerializer(serializers.ModelSerializer) :
    class Meta :
        model = PriceScopeControlType
        fields = [
            'id',
            'price_scope_control_type_name'            
        ]


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/price_type/price-scope=control-type')
'''
class PriceScopeControlTypeSerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    def validate_price_scope_control_type_name(self,value) :
        if PriceScopeControlType.objects.filter(price_scope_control_type_name=value).exists() :
            raise ValidationError(_('Price Scope Control Type name should be unique.'),code='not_unique')
        return value
    class Meta :
        model = PriceScopeControlType
        fields = [
            'id',
            'price_scope_control_type_name',
            'is_active',
            'created_by',
            'created_user',
            'created_date',
            'modified_date'
        ]
        read_only_fields = ['is_active','created_by','created_user','created_date','modified_date']

    @transaction.atomic
    def create(self,validated_data) :
        price_scope_control_type = PriceScopeControlType.all_objects.filter(
            price_scope_control_type_name = validated_data['price_scope_control_type_name'],
            is_active = False
        )
        if len(price_scope_control_type) > 0 :
            price_scope_control_type = price_scope_control_type[0]
            price_scope_control_type.is_active = True
            price_scope_control_type.modified_by = self.context['request'].user
            price_scope_control_type.modified_user = self.context['request'].user.user_name
            price_scope_control_type.modified_date = timezone.now()
            price_scope_control_type.deleted_by = None
            price_scope_control_type.deleted_user = None
            price_scope_control_type.deleted_date = None
        else : 
            price_scope_control_type = PriceScopeControlType.objects.create(
                **validated_data,
                created_by = self.context['request'].user,
                created_user = self.context['request'].user.user_name,           
            )
        return price_scope_control_type    


    def update(self,price_scope_control_type,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(price_scope_control_type, attr, value)
            update_fields.append(attr)
        price_scope_control_type.modified_by = self.context['request'].user
        price_scope_control_type.modified_user = self.context['request'].user.user_name
        price_scope_control_type.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        price_scope_control_type.save(update_fields=update_fields)
        return price_scope_control_type      


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/price_type/price-extra-service')
'''
class PriceExtraServiceSerializer(serializers.ModelSerializer) :

    class Meta :
        model = PriceExtraService
        fields = [
            'id',
            'title',
            'description',
            'is_price_type_applicable',
            'heading_1',
            'heading_1_master_name',
            'heading_2',
            'heading_2_master_name',
            'help'
        ]
