from rest_framework import serializers
from configs.models import QuestionForm

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/question_form/)
'''
class QuestionFormSerializer(serializers.ModelSerializer) :

    class Meta :
        model = QuestionForm
        fields = [
            'id',
            'question_form_name'
        ]