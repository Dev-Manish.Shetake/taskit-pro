from rest_framework import serializers
from configs.models import RefundStatus

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/refund_status/)
'''
class RefundStatusSerializer(serializers.ModelSerializer) :

    class Meta :
        model = RefundStatus
        fields = [
        'id',
        'refund_status_name',        
        ]