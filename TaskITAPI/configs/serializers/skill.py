from rest_framework import serializers
from configs.models import Skill,SkillLevel,SkillSuggestion

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/skills/)
'''
class SkillSerializer(serializers.ModelSerializer) :

    class Meta :
        model = Skill
        fields = [
            'id',
            'skill_name'
        ]


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/skills-level')
'''
class SkillLevelSerializer(serializers.ModelSerializer) :

    class Meta :
        model = SkillLevel
        fields = [
        'id',
        'skill_level_name'
        ]
        