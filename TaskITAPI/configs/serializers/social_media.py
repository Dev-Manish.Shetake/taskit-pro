from rest_framework import serializers
from configs.models import SocialMedia
'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/social_media/')
'''
class SocialMediaSerializer(serializers.ModelSerializer) :

    class Meta :
        model = SocialMedia
        fields = [
        'id',
        'social_media_name'
        ]
