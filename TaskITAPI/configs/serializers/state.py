from rest_framework import serializers
from configs.models import State
from .country import CountrySerializer

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/state/)
'''
class StateSerializer(serializers.ModelSerializer) :
    country = CountrySerializer()

    class Meta :
        model = State
        fields = [
            'id',
            'state_name',
            'country'
        ]
