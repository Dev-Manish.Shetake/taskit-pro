from rest_framework import serializers
from configs.models import VoucherType

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/voucher_type/)
'''
class VoucherTypeSerializer(serializers.ModelSerializer) :

    class Meta :
        model = VoucherType
        fields = [
        'id',
        'voucher_type_name',        
        ]
