from django.urls import path,include
from configs.views import CityListView

urlpatterns = [
    path('',CityListView.as_view()),
]
