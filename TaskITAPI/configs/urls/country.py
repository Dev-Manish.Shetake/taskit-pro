from django.urls import path,include
from configs.views import CountryListView

urlpatterns = [
    path('',CountryListView.as_view()),
]
