from django.urls import path,include
from configs.views import UserDeactivationReasonListView

urlpatterns = [
    path('',UserDeactivationReasonListView.as_view()),
]
