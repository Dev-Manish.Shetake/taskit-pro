from django.urls import path,include
from configs.views import EducationTitleListView, EducationMajorListView

urlpatterns = [
    path('education-title',EducationTitleListView.as_view()),
    path('education-major',EducationMajorListView.as_view())
]
