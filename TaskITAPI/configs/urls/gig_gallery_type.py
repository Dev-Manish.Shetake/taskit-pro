from django.urls import path,include
from configs.views import GigGalleryTypeListView

urlpatterns = [
    path('',GigGalleryTypeListView.as_view()),   
]
