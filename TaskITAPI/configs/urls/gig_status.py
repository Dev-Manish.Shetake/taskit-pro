from django.urls import path,include
from configs.views import GigStatusListView

urlpatterns = [
    path('',GigStatusListView.as_view()),
]