from django.urls import path,include
from configs.views import LanguageListView, LanguageProfeciencyListView, SellerLanguageGigsListView

urlpatterns = [
    path('',LanguageListView.as_view()),
    path('profeciency',LanguageProfeciencyListView.as_view()),
    path('seller-gigs',SellerLanguageGigsListView.as_view())
]
