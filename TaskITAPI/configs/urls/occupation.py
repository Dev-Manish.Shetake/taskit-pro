from django.urls import path,include
from configs.views import OccupationListView

urlpatterns = [
    path('',OccupationListView.as_view()),
]