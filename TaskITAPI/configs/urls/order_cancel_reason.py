from django.urls import path,include
from configs.views import OrderCancelReasonListView

urlpatterns = [
    path('',OrderCancelReasonListView.as_view()),
]