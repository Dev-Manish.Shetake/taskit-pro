from django.urls import path,include
from configs.views import OrderPaymentStatusListView

urlpatterns = [
    path('',OrderPaymentStatusListView.as_view()),
]