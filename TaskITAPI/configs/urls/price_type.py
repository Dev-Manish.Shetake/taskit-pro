from django.urls import path,include
from configs.views import PriceTypeListView,PriceScopeControlTypeListView,PriceScopeControlTypeDetailsView,PriceExtraServiceListView

urlpatterns = [
    path('',PriceTypeListView.as_view()),
    path('price-scope-control-type',PriceScopeControlTypeListView.as_view()),
    # path('price-scope-control-type/<int:pk>',PriceScopeControlTypeDetailsView.as_view()),
    path('price-extra-service',PriceExtraServiceListView.as_view()),
]