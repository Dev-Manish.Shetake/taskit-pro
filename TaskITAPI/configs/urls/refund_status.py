from django.urls import path,include
from configs.views import RefundStatusListView

urlpatterns = [
    path('',RefundStatusListView.as_view()),
]