from django.urls import path,include
from configs.views import SkillListView,SkillLevelListView

urlpatterns = [
    path('',SkillListView.as_view()),
    path('level',SkillLevelListView.as_view()),
]