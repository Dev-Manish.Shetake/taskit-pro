from django.urls import path,include
from configs.views import SocialMediaListView

urlpatterns = [
    path('',SocialMediaListView.as_view()),
]
