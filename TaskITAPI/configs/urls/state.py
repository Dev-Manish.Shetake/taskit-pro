from django.urls import path,include
from configs.views import StateListView

urlpatterns = [
    path('',StateListView.as_view()),
]
