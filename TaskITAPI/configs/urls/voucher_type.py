from django.urls import path,include
from configs.views import VoucherTypeListView

urlpatterns = [
    path('',VoucherTypeListView.as_view()),
]
