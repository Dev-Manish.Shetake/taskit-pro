GIG_STATUS = {
    'Draft' : 1,
    'Pending_for_Approval' : 2,
    'Requires_Modification' : 3,
    'Denied' : 4,
    'Active' : 5,
    'Paused' : 6
}

SOCIAL_MEDIA = {
    'google' : 1,
    'facebook' : 2,
    'tweeter' : 3,
    'linkedin' : 4
}

ORDER_STATUS = {
    'Draft' : 1,
    'Active' : 2,
    'In-Process' : 3,
    'Submit_Review' : 4,
    'Modification_Required' : 5,   
    'Completed' : 6,
    'Dispute' : 7,
    'Cancelled' : 8
}


PRICE_TYPE = {
    'Basic' : 1,
    'Standard' : 2,
    'Premium' : 3,    
}

GALLERY_TYPE = {
    'Photo' : 1,
    'Video' : 2,
    'Pdf' : 3,    
}

LEDGER_TYPE = {
    'User' : 1,
    'User_Wallet' : 2,
    'User_Bank' : 3,    
}

ORDER_CANCEL_REASON = {
    'User' : 1       
}

QUESTION_FORM = {
    'free_text' : 1,
    'multiple_choice' : 2,
    'attachment' : 3
}

VOUCHER_TYPE = {
    'buyer_to_nodal' : 1,
    'nodal_to_wallet' : 2,
    'wallet_to_bank' : 3,
    'notal_to_payment_gatewy_cancel_by_buyer' : 4,
    'notal_to_payment_gatewy_cancel_by_seller' : 5,
    'wallet_to_nodal_partial_payment' : 6 ,
    'payment_through_wallet' : 7  
}

ORDER_PAYMENT_STATUS = {
    'Inprocess' : 1,
    'Success' : 2,
    'Failure' : 3
}

GEN_LEDGER_TYPE = {
    'user' : 1,
    'user_wallet' : 2,
    'user_bank' : 3
}

REFUND_STATUS = {    
    'pending' : 1,
    'done' : 2
  }