from rest_framework import generics
from configs.models import City
from configs.serializers import CitySerializer
from django.db.models import Q 



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/city/)
'''
class CityListView(generics.ListAPIView) :
    """
    City List 

    Query parameters for GET method
    ---------------------------------------
    1. city_name = To filter by city_name   
    2. state_id = To filter by state id
    3. state_name = To filter by state name
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/city/?city_name=marketing&source=   

    """
    serializer_class = CitySerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        city_name = self.request.GET.get('city_name')
        state_id = self.request.GET.get('state_id')
        state_name = self.request.GET.get('state_name')
        if city_name :
            q_object.add(Q(city_name__icontains=city_name),Q.AND)
        if state_id :
            q_object.add(Q(state_id=state_id),Q.AND)
        if state_name :
            q_object.add(Q(state__state_name__icontains=state_name),Q.AND)        
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = City.all_objects.filter(is_active=False)
        else:
            queryset = City.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('city_name')  
        return queryset    
