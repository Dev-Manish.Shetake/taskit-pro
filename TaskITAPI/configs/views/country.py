from rest_framework import generics
from configs.models import Country
from configs.serializers import CountrySerializer
from django.db.models import Q 



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/country/)
'''
class CountryListView(generics.ListAPIView) :
    """
    Country List 

    Query parameters for GET method
    ---------------------------------------
    1. country_name = To filter by country_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/country/?country_name=marketing&source=   

    """
    serializer_class = CountrySerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        country_name = self.request.GET.get('country_name')
        if country_name :
            q_object.add(Q(country_name__icontains=country_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = Country.all_objects.filter(is_active=False)
        else:
            queryset = Country.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('country_name')  
        return queryset    
