from rest_framework import generics
from configs.models import UserDeactivationReason
from configs.serializers import UserDeactivationReasonSerializer
from django.db.models import Q 



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/deactivation_reason/)
'''
class UserDeactivationReasonListView(generics.ListAPIView) :
    """
    User Deactivation Reason List 

    Query parameters for GET method
    ---------------------------------------
    1. user_deactivate_reason_name = To filter by user_deactivate_reason_name   

    E.g. http://127.0.0.1:8000/api/v1/deactivation_reason/?user_deactivate_reason_name=

    """
    serializer_class = UserDeactivationReasonSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        user_deactivate_reason_name = self.request.GET.get('user_deactivate_reason_name')
        if user_deactivate_reason_name :
            q_object.add(Q(user_deactivate_reason_name__icontains=user_deactivate_reason_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset =  UserDeactivationReason.all_objects.filter(is_active=False)
        else:
            queryset =  UserDeactivationReason.objects.all()
        queryset = self.appy_filters(queryset)   
        queryset = queryset.order_by('seq_no')  
        return queryset    
