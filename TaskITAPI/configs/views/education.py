from rest_framework import generics
from configs.models import EducationTitle,EducationMajor
from configs.serializers import EducationTitleSerializer, EducationMajorSerializer
from django.db.models import Q 


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/education/education-title')
'''
class EducationTitleListView(generics.ListAPIView) :
    """
    Education Title List
    
    Query parameters for GET method
    ---------------------------------------
    1. education_title_name = To filter by education_title_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/education/education-title?education_title_name=English&source=   

    """
    serializer_class = EducationTitleSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        education_title_name = self.request.GET.get('education_title_name')
        if education_title_name :
            q_object.add(Q(education_title_name__icontains=education_title_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = EducationTitle.all_objects.filter(is_active=False)
        else:
            queryset = EducationTitle.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('education_title_name')  
        return queryset    



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/education/education-major')
'''
class EducationMajorListView(generics.ListCreateAPIView) :
    """
    Education Major List

    Query parameters for GET method
    ---------------------------------------
    1. education_major_name = To filter by education_major_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/education/education-title?education_major_name=string&source=   

    """
    serializer_class = EducationMajorSerializer

    def apply_filters(self,queryset) :
        q_object = Q()
        education_major_name = self.request.GET.get('education_major_name')
        if education_major_name :
            q_object.add(Q(education_major_name__icontains=education_major_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = EducationMajor.all_objects.filter(is_active=False)
        else:
            queryset = EducationMajor.objects.all()
        queryset = self.apply_filters(queryset)    
        queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('education_major_name')  
        return queryset 