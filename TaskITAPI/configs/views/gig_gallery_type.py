from rest_framework import generics
from configs.models import GigGalleryType
from configs.serializers import GigGalleryTypeSerializer
from django.db.models import Q 



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/gallery_type/')
'''
class GigGalleryTypeListView(generics.ListAPIView) :
    """
    Gig Gallery TypeList
    
    Query parameters for GET method
    ---------------------------------------
    1. gig_gallery_type_name = To filter by gig_gallery_type_name   

    E.g. http://127.0.0.1:8000/api/v1/gallery_type/?gig_gallery_type_name=image

    """
    serializer_class = GigGalleryTypeSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        gig_gallery_type_name = self.request.GET.get('gig_gallery_type_name')
        if gig_gallery_type_name :
            q_object.add(Q(gig_gallery_type_name__icontains=gig_gallery_type_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = GigGalleryType.all_objects.filter(is_active=False)
        else:
            queryset = GigGalleryType.objects.all()
        queryset = self.appy_filters(queryset) 
        return queryset    
