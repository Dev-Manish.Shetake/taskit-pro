from rest_framework import generics
from configs.models import GigStatus
from configs.serializers import GigStatusSerializer
from django.db.models import Q 


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/gig_status/)
'''
class GigStatusListView(generics.ListAPIView) :
    """
    Gig Status List 

    Query parameters for GET method
    ---------------------------------------
    1. gig_status_name = To filter by gig_status_name   
   

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/gig_status/?gig_status_name=active

    """
    serializer_class = GigStatusSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        gig_status_name = self.request.GET.get('gig_status_name')
        if gig_status_name :
            q_object.add(Q(gig_status_name__icontains=gig_status_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')       
        if is_active == 'false' :
            queryset = GigStatus.all_objects.filter(is_active=False)
        else:
            queryset = GigStatus.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)  
        return queryset    