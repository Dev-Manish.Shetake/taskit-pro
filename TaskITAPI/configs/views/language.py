from rest_framework import generics
from configs.models import Language,LanguageProficiency
from configs.serializers import LanguageSerializer, LanguageProficiencySerializer, SellerLanguageGigsSerializer
from django.db.models import Q 


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/language/)
'''
class LanguageListView(generics.ListAPIView) :
    """
    Language List
    
    Query parameters for GET method
    ---------------------------------------
    1. lang_name = To filter by lang_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/languages/?lang_name=English&source=   

    """
    serializer_class = LanguageSerializer
    authentication_classes = []
    permission_classes = []
    
    def appy_filters(self,queryset) :
        q_object = Q()
        lang_name = self.request.GET.get('lang_name')
        if lang_name :
            q_object.add(Q(lang_name__icontains=lang_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = Language.all_objects.filter(is_active=False)
        else:
            queryset = Language.objects.all()
        queryset = self.appy_filters(queryset) 
        queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('lang_name')  
        return queryset    



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/language-profeciency/)
'''
class LanguageProfeciencyListView(generics.ListAPIView) :
    """
    Language Profeciency List

    Query parameters for GET method
    ---------------------------------------
    1. lang_proficiency_name = To filter by lang_profeciency_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/languages/profeciency?lang_proficiency_name=string&source=   

    """
    serializer_class = LanguageProficiencySerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        lang_proficiency_name = self.request.GET.get('lang_proficiency_name')
        if lang_proficiency_name :
            q_object.add(Q(lang_proficiency_name__icontains=lang_proficiency_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = LanguageProficiency.all_objects.filter(is_active=False)
        else:
            queryset = LanguageProficiency.objects.all()
        queryset = self.appy_filters(queryset)    
        queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('lang_proficiency_name')  
        return queryset 


 
'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/language/seller-gigs/)
'''
class SellerLanguageGigsListView(generics.ListAPIView) :
    """
    Seller Language Gigs List

    Query parameters for GET method
    ---------------------------------------
    1. search = To filter by Category/GigsTags/   
    2. category_name = To filter by Category Name
    3. sub_category_name = To filter by Sub Category Name
    4. search_tags = Gigs Tags

    E.g. http://127.0.0.1:8000/api/v1/languages/seller-gigs?search=string   

    """
    serializer_class = SellerLanguageGigsSerializer
    authentication_classes = []
    permission_classes = []

    def appy_filters(self,queryset) :
        q_object = Q()
        lang_name = self.request.GET.get('lang_name')
        category_name = self.request.GET.get('category_name')
        sub_category_name = self.request.GET.get('sub_category_name')
        search_tags = self.request.GET.get('search_tags')
        if lang_name :
            q_object.add(Q(lang_name__icontains=lang_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')        
        queryset = Language.objects.all()
        queryset = self.appy_filters(queryset) 
        queryset = self.serializer_class.setup_eager_loading(self,queryset)
        return queryset       