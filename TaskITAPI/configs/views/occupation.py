from rest_framework import generics
from configs.models import Occupation
from configs.serializers import OccupationSerializer
from django.db.models import Q 



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/occupations/)
'''
class OccupationListView(generics.ListAPIView) :
    """
    Occupations List 

    Query parameters for GET method
    ---------------------------------------
    1. occupation_name = To filter by occupation_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/occupations/?occupation_name=marketing&source=   

    """
    serializer_class = OccupationSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        occupation_name = self.request.GET.get('occupation_name')
        if occupation_name :
            q_object.add(Q(occupation_name__icontains=occupation_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = Occupation.all_objects.filter(is_active=False)
        else:
            queryset = Occupation.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('occupation_name')  
        return queryset    
