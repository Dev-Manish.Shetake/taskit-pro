from rest_framework import generics
from configs.models import OrderStatus,OrderPaymentStatus,OrderCancelReason
from configs.serializers import OrderStatusSerializer,OrderPaymentStatusSerializer,OrderCancelReasonSerializer
from django.db.models import Q 



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/order_status/)
'''
class OrderStatusListView(generics.ListAPIView) :
    """
    Order Status List 

    Query parameters for GET method
    ---------------------------------------
    1. order_status_name = To filter by order_status_name   
   

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/order_status/?order_status_name=marketing

    """
    serializer_class = OrderStatusSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        order_status_name = self.request.GET.get('order_status_name')
        if order_status_name :
            q_object.add(Q(order_status_name__icontains=order_status_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')       
        if is_active == 'false' :
            queryset = OrderStatus.all_objects.filter(is_active=False)
        else:
            queryset = OrderStatus.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)  
        return queryset    


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/order_payment_status/)
'''
class OrderPaymentStatusListView(generics.ListAPIView) :
    """
    Order Status List 

    Query parameters for GET method
    ---------------------------------------
    1. order_payment_status_name = To filter by order_payment_status_name   

    E.g. http://127.0.0.1:8000/api/v1/order_payment_status/?order_payment_status_name=marketing

    """
    serializer_class = OrderPaymentStatusSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        order_payment_status_name = self.request.GET.get('order_payment_status_name')
        if order_payment_status_name :
            q_object.add(Q(order_payment_status_name__icontains=order_payment_status_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')       
        if is_active == 'false' :
            queryset = OrderPaymentStatus.all_objects.filter(is_active=False)
        else:
            queryset = OrderPaymentStatus.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)
        return queryset    


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/order_cancel_reason')
'''
class OrderCancelReasonListView(generics.ListAPIView) :
    """
    Order Status List 

    Query parameters for GET method
    ---------------------------------------
    1. order_cancel_reason_name = To filter by order_cancel_reason_name   

    E.g. http://127.0.0.1:8000/api/v1/order_cancel_reason/?order_cancel_reason_name=marketing

    """
    serializer_class = OrderCancelReasonSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        order_cancel_reason_name = self.request.GET.get('order_cancel_reason_name')
        if order_cancel_reason_name :
            q_object.add(Q(order_cancel_reason_name__icontains=order_cancel_reason_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')       
        if is_active == 'false' :
            queryset = OrderCancelReason.all_objects.filter(is_active=False)
        else:
            queryset = OrderCancelReason.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)
        return queryset    

