from rest_framework.generics import ListAPIView,ListCreateAPIView,RetrieveUpdateAPIView
from configs.models import PriceType, PriceScopeControlType
from configs.serializers import PriceTypeSerializer, PriceScopeControlTypeSerializer, PriceExtraServiceSerializer
from django.db.models import Q 
from masters.models import PriceExtraService
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/price_type/)
'''
class PriceTypeListView(ListAPIView) :
    """
    Price Type List 

    Query parameters for GET method
    ---------------------------------------
    1. price_type_name = To filter by price_type_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/price_type/?price_type_name=marketing&source=   

    """
    serializer_class = PriceTypeSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        price_type_name = self.request.GET.get('price_type_name')
        if price_type_name :
            q_object.add(Q(price_type_name__icontains=price_type_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = PriceType.all_objects.filter(is_active=False)
        else:
            queryset = PriceType.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('price_type_name')  
        return queryset    



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/price_type/price-scope-control-type')
'''
class PriceScopeControlTypeListView(ListCreateAPIView) :
    """
    PriceScopeControlType List

    Query parameters for GET method
    ---------------------------------------
    1. price_scope_control_type_name = To filter by price_scope_control_type_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/price_type/price-scope=control-type?price_scope_control_type_name=string&source=   

    """
    serializer_class = PriceScopeControlTypeSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        price_scope_control_type_name = self.request.GET.get('price_scope_control_type_name')
        if price_scope_control_type_name :
            q_object.add(Q(price_scope_control_type_name__icontains=price_scope_control_type_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = PriceScopeControlType.all_objects.filter(is_active=False)
        else:
            queryset = PriceScopeControlType.objects.all()
        queryset = self.appy_filters(queryset)    
        queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('price_scope_control_type_name')  
        return queryset 


'''
View to send/receive/delete data in specific format for GET/PUT/PATCH/DELETE method
(endpoint = '/metadata_type/:id')
'''
class PriceScopeControlTypeDetailsView(RetrieveUpdateAPIView) :

    queryset = PriceScopeControlType.objects.none()
    serializer_class = PriceScopeControlTypeSerializer
   
    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = PriceScopeControlType.all_objects.all().filter(is_active=False)
        else : 
            queryset = PriceScopeControlType.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 

    def delete(self,request,*args,**kwargs) :
        price_scope_control_type = self.get_object()
        price_scope_control_type.is_active = False
        price_scope_control_type.deleted_by = request.user
        price_scope_control_type.deleted_user = request.user.user_name
        price_scope_control_type.deleted_date = timezone.now()
        price_scope_control_type.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/price_type/price-extra-service')
'''
class PriceExtraServiceListView(ListAPIView) :
    """
    Price Extra Service List

    Query parameters for GET method
    ---------------------------------------
    1. title = To filter by title   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/price_type/price-extra-service?title=string&source=   

    """
    serializer_class = PriceExtraServiceSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        title = self.request.GET.get('title')
        if title :
            q_object.add(Q(title__icontains=title),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = PriceExtraService.all_objects.filter(is_active=False)
        else:
            queryset = PriceExtraService.objects.all()
        queryset = self.appy_filters(queryset)    
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('title')  
        return queryset 
