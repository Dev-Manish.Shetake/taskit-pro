from rest_framework import generics
from configs.models import RefundStatus
from configs.serializers import RefundStatusSerializer
from django.db.models import Q 


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/refund_status/)
'''
class RefundStatusListView(generics.ListAPIView) :
    """
    Refund Status List 

    Query parameters for GET method
    ---------------------------------------
    1. refund_status_name = To filter by refund_status_name     

    E.g. http://127.0.0.1:8000/api/v1/refund_status/?refund_status_name=done

    """
    serializer_class = RefundStatusSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        refund_status_name = self.request.GET.get('refund_status_name')
        if refund_status_name :
            q_object.add(Q(refund_status_name__icontains=refund_status_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')       
        if is_active == 'false' :
            queryset = RefundStatus.all_objects.filter(is_active=False)
        else:
            queryset = RefundStatus.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)  
        return queryset   