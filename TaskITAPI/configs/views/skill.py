from rest_framework import generics
from configs.models import Skill,SkillLevel,SkillSuggestion
from configs.serializers import SkillSerializer,SkillLevelSerializer
from django.db.models import Q 



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/skills/)
'''
class SkillListView(generics.ListAPIView) :
    """
    Skill List 

    Query parameters for GET method
    ---------------------------------------
    1. skill_name = To filter by skill_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/skillss/?skill_name=marketing&source=   

    """
    serializer_class = SkillSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        skill_name = self.request.GET.get('skill_name')
        if skill_name :
            q_object.add(Q(skill_name__icontains=skill_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = Skill.all_objects.filter(is_active=False)
        else:
            queryset = Skill.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('skill_name')  
        return queryset    



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/skills-level/')
'''
class SkillLevelListView(generics.ListAPIView) :
    """
    Skill Level List

    Query parameters for GET method
    ---------------------------------------
    1. skill_level_name = To filter by skill_level_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/skills/profeciency?skill_level_name=string&source=   

    """
    serializer_class = SkillLevelSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        skill_level_name = self.request.GET.get('skill_level_name')
        if skill_level_name :
            q_object.add(Q(skill_level_name__icontains=skill_level_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = SkillLevel.all_objects.filter(is_active=False)
        else:
            queryset = SkillLevel.objects.all()
        queryset = self.appy_filters(queryset)    
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('skill_level_name')  
        return queryset 