from rest_framework import generics
from configs.models import SocialMedia
from configs.serializers import SocialMediaSerializer
from django.db.models import Q 


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/social_media/')
'''
class SocialMediaListView(generics.ListAPIView) :
    """
    Social Media List
    
    Query parameters for GET method
    ---------------------------------------
    1. social_media_name = To filter by social_media_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/social_media/?social_media_name=google&source=   

    """
    serializer_class = SocialMediaSerializer
    permission_classes = []
    authentication_classes = []

    def appy_filters(self,queryset) :
        q_object = Q()
        social_media_name = self.request.GET.get('social_media_name')
        if social_media_name :
            q_object.add(Q(social_media_name__icontains=social_media_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = SocialMedia.all_objects.filter(is_active=False)
        else:
            queryset = SocialMedia.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('social_media_name')  
        return queryset    
