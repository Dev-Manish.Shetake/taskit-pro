from rest_framework import generics
from configs.models import State
from configs.serializers import StateSerializer
from django.db.models import Q 



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/state/)
'''
class StateListView(generics.ListAPIView) :
    """
    State List 

    Query parameters for GET method
    ---------------------------------------
    1. state_name = To filter by state_name   
    2. country_id = To filter by contry id
    3. country_name = To filter by contry name
    4. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/state/?state_name=mah&source=   

    """
    serializer_class = StateSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        state_name = self.request.GET.get('state_name')
        country_id = self.request.GET.get('country_id')
        country_name = self.request.GET.get('country_name')
        if state_name :
            q_object.add(Q(state_name__icontains=state_name),Q.AND)
        if country_id :
            q_object.add(Q(country_id=country_id),Q.AND)    
        if country_name :
            q_object.add(Q(country__country_name__icontains=country_name),Q.AND)    
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        source = self.request.GET.get('source')
        if is_active == 'false' :
            queryset = State.all_objects.filter(is_active=False)
        else:
            queryset = State.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)    
        if source :
            queryset = queryset.order_by('state_name')  
        return queryset    
