from rest_framework import generics
from configs.models import VoucherType
from configs.serializers import VoucherTypeSerializer
from django.db.models import Q 


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/voucher_type/)
'''
class VoucherTypeListView(generics.ListAPIView) :
    """
    Voucher Type List 

    Query parameters for GET method
    ---------------------------------------
    1. voucher_type_name = To filter by voucher_type_name   
   

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. http://127.0.0.1:8000/api/v1/order_status/?voucher_type_name=marketing

    """
    serializer_class = VoucherTypeSerializer

    def appy_filters(self,queryset) :
        q_object = Q()
        voucher_type_name = self.request.GET.get('voucher_type_name')
        if voucher_type_name :
            q_object.add(Q(voucher_type_name__icontains=voucher_type_name),Q.AND)
        if len(q_object) > 0:
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')       
        if is_active == 'false' :
            queryset = VoucherType.all_objects.filter(is_active=False)
        else:
            queryset = VoucherType.objects.all()
        queryset = self.appy_filters(queryset) 
        # queryset = self.serializer_class.setup_eager_loading(self,queryset)  
        return queryset  