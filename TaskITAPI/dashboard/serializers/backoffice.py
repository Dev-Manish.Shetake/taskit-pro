from rest_framework import serializers
from orders.models import Order,OrderGigPrice,OrderPriceExtraService,OrderRating,OrderComment
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.db import transaction
from django.db.models import Prefetch
from configs.serializers import OrderStatusSerializer
from gigs.models import GigGallery,Gig,GigPrice
from gigs.serializers import GigsGalleryMinSerializer,GigPriceSerializer
from configs.values import GALLERY_TYPE,PRICE_TYPE


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = 'dashboard/orders-list')
'''
class OrdersDashboardListSerializer(serializers.ModelSerializer) :  
    order_status = OrderStatusSerializer()
    # gig_gallery = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :        
        return queryset.order_by('-created_date')

    class Meta :
        model = Order
        fields = [
            'id',           
            'gig_id',
            'amount',
            'extra_service_amount',
            'extra_service_custom_amount',
            'service_fee',
            'total_amount',
            'order_status',
            'delivered_date',           
            'created_date'            
        ]

    # def get_gig_gallery(self,instance) :         
    #     gallery = GigGallery.objects.filter(gig=instance.gig,gig_gallery_type_id=GALLERY_TYPE['Photo'])
    #     print(gallery[0])
    #     gig_gallery = {
    #         'id' : gallery[0].id,
    #         'file_path' : gallery[0].file_path,
    #         'file_path_thumbnail' : gallery[0].file_path_thumbnail,
    #     }
    #     return gig_gallery    
       


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = 'dashboard/gigs-list/')
'''
class GigsDashboardListSerializer(serializers.ModelSerializer) :     
    gallery = GigsGalleryMinSerializer(many=True)
    gig_price = GigPriceSerializer(many=True)   
     
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(           
            Prefetch('giggallery_set',GigGallery.objects.filter(gig_gallery_type_id=GALLERY_TYPE['Photo']),to_attr='gallery'),
            Prefetch('gigprice_set',GigPrice.objects.filter(price_type_id=PRICE_TYPE['Basic']),to_attr='gig_price'),   
        )        
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = Gig
        fields = [
            'id',    
            'title',
            'gallery',
            'total_views',
            'rating',
            'gig_price',
            'is_active',
            'created_date',                    
        ]


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/dashboard/count')
'''
class DashboardCountSerializer(serializers.Serializer) :   
    total_gigs = serializers.IntegerField()
    total_user = serializers.IntegerField()
    total_orders = serializers.IntegerField()
    complete_orders = serializers.IntegerField()
    dispute_orders = serializers.IntegerField()
    tatal_seller = serializers.IntegerField()
    