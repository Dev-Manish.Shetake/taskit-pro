from django.urls import path, include
from dashboard.views import OrdersDashboardListView,GigsDashboardListView,DashboardCountView

urlpatterns = [
    path('orders-list',OrdersDashboardListView.as_view()),
    path('gigs-list',GigsDashboardListView.as_view()),
    path('count',DashboardCountView.as_view()),
]