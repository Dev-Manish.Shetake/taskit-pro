from rest_framework.generics import ListAPIView,ListCreateAPIView,CreateAPIView
from orders.models import Order,OrderComment
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db import transaction 
from orders.utils import get_user_data
from django.db.models import Q
from django.db import connection
from TaskITAPI.serializers import StoredProcedureSerializer
from dashboard.serializers import OrdersDashboardListSerializer,GigsDashboardListSerializer,DashboardCountSerializer
from gigs.models import Gig


'''
View to send/receive data in specific format for GET/POST method
(endpoint = 'dashboard/orders-list)
'''
class OrdersDashboardListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. order_status_id = To filter by Order Status
   
    E.g. http://127.0.0.1:8000/api/v1/dashboard/orders-list

    """
    serializer_class = OrdersDashboardListSerializer
    pagination_class = StandardResultsSetPagination
 

    def apply_filters(self,queryset) :
        q_objects = Q()        
        order_status_id = self.request.GET.get('order_status_id')
      
        if order_status_id :
            q_objects.add(Q(order_status_id=order_status_id), Q.AND)               
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL             
        if is_active == 'false' :    
            queryset = Order.all_objects.filter(is_active=False)
        else : 
            queryset = Order.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = get_user_data(self.request,queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset



'''
View to send/receive data in specific format for GET/POST method
(endpoint = 'dashboard/gigs-list)
'''
class GigsDashboardListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. gig_status_id = To filter by Gig Status
   
    E.g. http://127.0.0.1:8000/api/v1/dashboard/gigs-list

    """
    serializer_class = GigsDashboardListSerializer
    pagination_class = StandardResultsSetPagination
 

    def apply_filters(self,queryset) :
        q_objects = Q()        
        gig_status_id = self.request.GET.get('gig_status_id')
      
        if gig_status_id :
            q_objects.add(Q(gig_status_id=gig_status_id), Q.AND)               
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL             
        if is_active == 'false' :    
            queryset = Gig.all_objects.filter(is_active=False)
        else : 
            queryset = Gig.objects.all()
        queryset = self.apply_filters(queryset)      
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/dashboard/count')
'''
class DashboardCountView(ListAPIView) :
    """
    Get Seller Dashboard Count status stage wise(dashboard + menu)
    
    Query parameters for GET method
    ---------------------------------------    
  
    E.g  http://127.0.0.1:8000/api/v1/dashboard/count
    """

    serializer_class = DashboardCountSerializer
    # queryset = User.objects.none()
    result_set = []

    def get(self, request, *args, **kwargs) :       
        data = None    
        with connection.cursor() as cursor:
            cursor.callproc('sp_get_backoffice_dashboard_summary', [request.user.id])  
            from TaskITAPI.serializers import StoredProcedureSerializer         
            data = StoredProcedureSerializer(cursor, self.result_set).data
            if type(data) == dict and not data :
               data = []             
        return Response(data, status=status.HTTP_200_OK)         