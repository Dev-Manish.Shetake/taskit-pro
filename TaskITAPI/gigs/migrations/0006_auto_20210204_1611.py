# Generated by Django 2.2 on 2021-02-04 16:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gigs', '0005_auto_20210204_1601'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gigsubcategorypricedetails',
            name='value',
            field=models.CharField(blank=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='gigsubcategorypricedetailsdraft',
            name='value',
            field=models.CharField(blank=True, max_length=200),
        ),
    ]
