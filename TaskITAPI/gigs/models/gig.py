from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from masters.models import Category,SubCategory
from configs.models import GigStatus

# Master Gig related data stored in this table
class Gig(SoftDeleteModel) :
    title = models.CharField(max_length=100)
    category = models.ForeignKey(Category,on_delete=models.DO_NOTHING)
    sub_category = models.ForeignKey(SubCategory,on_delete=models.DO_NOTHING)
    is_approved = models.BooleanField(null=True)
    approved_by = models.ForeignKey(User, on_delete=models.DO_NOTHING,null=True, related_name = 'gig_approved_by')
    approved_user = models.CharField(max_length=50, null=True)
    approved_date = models.DateTimeField(null=True)
    is_price_package = models.BooleanField(default=False)
    description = models.CharField(max_length=2000)
    gig_status = models.ForeignKey(GigStatus,on_delete=models.DO_NOTHING)
    total_views = models.IntegerField(null=True)
    rating = models.IntegerField(null=True)
    rated_no_of_records = models.IntegerField(null=True)
    sum_of_rating = models.IntegerField(null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='gig_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='gig_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='gig_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_gig'
        default_permissions = ()



# Master Gig Last Visited related data stored in this table
class GigLastVisited(SoftDeleteModel) :
    user = models.ForeignKey(User,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    created_date = models.DateTimeField(auto_now_add=True) 

    class Meta :
        db_table = 'gig_last_visited'
        default_permissions = ()
    

 
# Master Gig related data stored in this table
class GigDraft(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    title = models.CharField(max_length=100)
    category = models.ForeignKey(Category,on_delete=models.DO_NOTHING)
    sub_category = models.ForeignKey(SubCategory,on_delete=models.DO_NOTHING)
    is_approved = models.BooleanField(null=True)
    approved_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True, related_name = 'gig_draft_approved_by')
    approved_user = models.CharField(max_length=50, null=True)
    approved_date = models.DateTimeField(null=True)
    is_price_package = models.BooleanField(default=False)
    description = models.CharField(max_length=2000)
    gig_status = models.ForeignKey(GigStatus,on_delete=models.DO_NOTHING)
    modification_required_comment = models.CharField(max_length=500, null=True)
    reject_comment = models.CharField(max_length=500, null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'gig_draft_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='gig_draft_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='gig_draft_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_gig_draft'
        default_permissions = ()

   
 
# Master Gig Log related data stored in this table
class GigLog(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING,null=True)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    gig_status = models.ForeignKey(GigStatus,on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
  
    class Meta :
        db_table = 'gig_log'
        default_permissions = ()
     

# Master Gig Search Last Visited related data stored in this table
class GigSearchTag(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    search_tags = models.CharField(max_length=2000,null=True)

    class Meta :
        db_table = 'mst_gig_search_tag'
        default_permissions = ()     


# Master GigSearchTagDraft Last Visited related data stored in this table
class GigSearchTagDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    search_tags = models.CharField(max_length=2000,null=True)

    class Meta :
        db_table = 'mst_gig_search_tag_draft'
        default_permissions = ()     


# Master GigFaq Last Visited related data stored in this table
class GigFaq(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    seq_no = models.IntegerField()
    question = models.CharField(max_length=200)
    answer = models.CharField(max_length=300)

    class Meta :
        db_table = 'mst_gig_faq'
        default_permissions = ()             


# Master GigFaqDraft Last Visited related data stored in this table
class GigFaqDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    seq_no = models.IntegerField()
    question = models.CharField(max_length=200)
    answer = models.CharField(max_length=300)

    class Meta :
        db_table = 'mst_gig_faq_draft'
        default_permissions = ()                     


# Master UserFavouriteGigs Last Visited related data stored in this table
class UserFavouriteGigs(models.Model) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)

    class Meta :
        db_table = 'user_favourite_gig'
        default_permissions = ()         



# Master Gigs List View
class VWGigsData(models.Model) :
    gig_id = models.IntegerField()   
    title = models.CharField(max_length=100)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.DO_NOTHING)
    sub_category_name = models.CharField(max_length=100)  
    category = models.ForeignKey(Category, on_delete=models.DO_NOTHING)
    category_name =  models.CharField(max_length=100)    
    gig_status = models.ForeignKey(GigStatus,on_delete=models.DO_NOTHING)
    gig_status_name = models.CharField(max_length=100)
    total_views = models.IntegerField(null=True)
    rating = models.IntegerField(null=True)
    gallery_id = models.IntegerField()
    gallery_file_path = models.CharField(max_length=100,null=True)
    gallery_file_path_thumbnail = models.CharField(max_length=100,null=True)
    orders_count =  models.IntegerField(null=True)
    cancelled_orders_count = models.IntegerField(null=True)
    is_active =  models.BooleanField()   
    created_by_id = models.IntegerField()
    created_date = models.DateTimeField()
    created_user = models.CharField(max_length=50)
    modified_date = models.DateTimeField()
    gig_price = models.IntegerField(null=True)   

    class Meta :
        managed = False    
        db_table = 'vw_gig_data'       
        default_permissions = ()          


class GigSearchKeyword(SoftDeleteModel) :    
    search_keyword = models.CharField(max_length=200)    

    class Meta :
        db_table = 'mst_gig_search_keyword'
        default_permissions = ()   