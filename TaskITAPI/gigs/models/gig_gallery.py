from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from gigs.models import Gig,GigDraft
from configs.models import GigGalleryType
from TaskITAPI.settings import MEDIA_ROOT 
import os
from django.db.models.signals import post_save

def gig_gallery_file_path(instance, filename):
    gigs = GigGallery.objects.last()  
    if gigs :
       count = gigs.id + 1
    else :
        count = 1 
    return 'Gig/Gallery/File_Path/{0}/{1}_{2}'.format(
        instance.gig_id,      
        count,      
        filename
        )

def gig_gallery_file_path_thumbnail(instance, filename):   
    gigs = GigGallery.objects.last()   
    if gigs :
       count = gigs.id + 1
    else :
        count = 1 
    return 'Gig/Gallery/File_Path_Thumbnail/{0}/{1}_{2}'.format(
        instance.gig_id,       
        count,      
        filename
        ) 

def gig_draft_gallery_file_path(instance, filename):    
    draft = GigGalleryDraft.objects.last()
    if draft :
        count = draft.id + 1
    else :
        count = 1    
    return 'GigDraft/Gallery/File_Path/{0}/{1}_{2}'.format(       
        instance.gig_draft_id,         
        count,     
        filename
        )

def gig_draft_gallery_file_path_thumbnail(instance, filename):     
    draft = GigGalleryDraft.objects.last()    
    if draft :
      count = draft.id + 1  
    else :
        count = 1 
    return 'GigDraft/Gallery/File_Path_Thumbnail/{0}/{1}_{2}'.format(        
        instance.gig_draft_id,  
        count,  
        filename
        ) 


# Master GigGallery related data stored in this table
class GigGallery(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    gig_gallery_type = models.ForeignKey(GigGalleryType,on_delete=models.DO_NOTHING)
    file_path = models.FileField(upload_to=gig_gallery_file_path)
    file_path_thumbnail = models.FileField(upload_to=gig_gallery_file_path_thumbnail)

    class Meta :
        db_table = 'mst_gig_gallery'
        default_permissions = ()


# Master GigGalleryDraft related data stored in this table
class GigGalleryDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    gig_gallery_type = models.ForeignKey(GigGalleryType,on_delete=models.DO_NOTHING)
    file_path = models.FileField(upload_to=gig_draft_gallery_file_path)
    file_path_thumbnail = models.FileField(upload_to=gig_draft_gallery_file_path_thumbnail)

    class Meta :
        db_table = 'mst_gig_gallery_draft'
        default_permissions = ()  
