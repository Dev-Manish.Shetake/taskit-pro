from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from masters.models import Category,SubCategory,MetaDataType,MetaDataTypeDetails,SubCategoryMetaDataType
from gigs.models import GigDraft,Gig



# Master GigSubCategoryMetaDataType related data stored in this table
class GigSubCategoryMetaDataType(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    sub_category = models.ForeignKey(SubCategory,on_delete=models.DO_NOTHING)
    metadata_type = models.ForeignKey(MetaDataType,on_delete=models.DO_NOTHING)
    is_single = models.BooleanField(null=True)
    is_mandatory = models.BooleanField(null=True)
    min_value = models.IntegerField(null=True)
    max_value = models.IntegerField(null=True)
    is_suggest_other = models.BooleanField(null=True)

    class Meta :
        db_table = 'mst_gig_sub_category_metadata_type'
        default_permissions = ()


# Master GigSubCategoryMetaDataTypeDetails related data stored in this table
class GigSubCategoryMetaDataTypeDetails(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    sub_category_metadata_type = models.ForeignKey(GigSubCategoryMetaDataType,on_delete=models.DO_NOTHING)
    metadata_type_dtl = models.ForeignKey(MetaDataTypeDetails,on_delete=models.DO_NOTHING)
    
    class Meta :
        db_table = 'mst_gig_sub_category_metadata_type_dtl'
        default_permissions = ()


# Master GigSubCategoryMetaDataTypeOther related data stored in this table
class GigSubCategoryMetaDataTypeOther(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    sub_category_metadata_type = models.ForeignKey(GigSubCategoryMetaDataType,on_delete=models.DO_NOTHING)
    other = models.CharField(max_length=100)
    
    class Meta :
        db_table = 'mst_gig_sub_category_metadata_type_other'
        default_permissions = ()        


# Master GigSubCategoryMetaDataTypeDraft related data stored in this table
class GigSubCategoryMetaDataTypeDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    sub_category = models.ForeignKey(SubCategory,on_delete=models.DO_NOTHING)
    metadata_type = models.ForeignKey(MetaDataType,on_delete=models.DO_NOTHING)
    is_single = models.BooleanField(null=True)
    is_mandatory = models.BooleanField(null=True)
    min_value = models.IntegerField(null=True)
    max_value = models.IntegerField(null=True)
    is_suggest_other = models.BooleanField(null=True)

    class Meta :
        db_table = 'mst_gig_sub_category_metadata_type_draft'
        default_permissions = ()


# Master GigSubCategoryMetaDataTypeDraftDetails related data stored in this table
class GigSubCategoryMetaDataTypeDraftDetails(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    gig_sub_category_metadata_type_draft = models.ForeignKey(GigSubCategoryMetaDataTypeDraft,on_delete=models.DO_NOTHING)
    metadata_type_dtl = models.ForeignKey(MetaDataTypeDetails,on_delete=models.DO_NOTHING)
    
    class Meta :
        db_table = 'mst_gig_sub_category_metadata_type_dtl_draft'
        default_permissions = ()        


# Master GigSubCategoryMetaDataTypeOtherDraft related data stored in this table
class GigSubCategoryMetaDataTypeOtherDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    gig_sub_category_metadata_type_draft = models.ForeignKey(GigSubCategoryMetaDataTypeDraft,on_delete=models.DO_NOTHING)
    other = models.CharField(max_length=100)

    class Meta :
        db_table = 'mst_gig_sub_category_metadata_type_other_draft'
        default_permissions = ()