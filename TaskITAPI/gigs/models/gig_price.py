from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from gigs.models import Gig,GigDraft
from configs.models import PriceType,ExtraDays
from masters.models import SubCategoryPriceScope,SubCategoryPriceScopeDetails,PriceExtraService


# Master Gig Price related data stored in this table
class GigPrice(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    price_type = models.ForeignKey(PriceType,on_delete=models.DO_NOTHING)
    price = models.DecimalField(decimal_places=2,max_digits=9,null=True)

    class Meta :
        db_table = 'mst_gig_price'
        default_permissions = ()


# Master GigSubCategoryPriceDetails Last Visited related data stored in this table
class GigSubCategoryPriceDetails(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    gig_price = models.ForeignKey(GigPrice,on_delete=models.DO_NOTHING)
    sub_category_price_scope = models.ForeignKey(SubCategoryPriceScope,on_delete=models.DO_NOTHING)
    sub_category_price_scope_dtl = models.ForeignKey(SubCategoryPriceScopeDetails,on_delete=models.DO_NOTHING,null=True)
    value = models.CharField(max_length=200,blank=True)    

    class Meta :
        db_table = 'mst_gig_sub_category_price_dtl'
        default_permissions = () 


# Master GigPriceExtraService related data stored in this table
class GigPriceExtraService(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    price_extra_service = models.ForeignKey(PriceExtraService,on_delete=models.DO_NOTHING)
    price_type = models.ForeignKey(PriceType,on_delete=models.DO_NOTHING,null=True)
    extra_price = models.DecimalField(max_digits=9,decimal_places=2,null=True) 
    extra_days = models.ForeignKey(ExtraDays,on_delete=models.DO_NOTHING,null=True)
    extra_day = models.IntegerField(null=True)

    class Meta :
        db_table = 'mst_gig_price_extra_service'
        default_permissions = ()



# Master GigPriceExtraServiceCustom related data stored in this table
class GigPriceExtraServiceCustom(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    extra_price = models.DecimalField(max_digits=9,decimal_places=2,null=True)
    extra_days =  models.ForeignKey(ExtraDays,on_delete=models.DO_NOTHING,null=True)
    extra_day = models.IntegerField(null=True)

    class Meta :
        db_table = 'mst_gig_price_extra_service_custom'
        default_permissions = ()


# Master GigPriceDraft related data stored in this table
class GigPriceDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    price_type = models.ForeignKey(PriceType,on_delete=models.DO_NOTHING)
    price = models.DecimalField(decimal_places=2,max_digits=9,null=True)

    class Meta :
        db_table = 'mst_gig_price_draft'
        default_permissions = ()        


# Master GigSubCategoryPriceDetailsDraft related data stored in this table
class GigSubCategoryPriceDetailsDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    gig_price_draft = models.ForeignKey(GigPriceDraft,on_delete=models.DO_NOTHING)
    sub_category_price_scope = models.ForeignKey(SubCategoryPriceScope,on_delete=models.DO_NOTHING)
    sub_category_price_scope_dtl = models.ForeignKey(SubCategoryPriceScopeDetails,on_delete=models.DO_NOTHING,null=True)
    value = models.CharField(max_length=200,blank=True)

    class Meta :
        db_table = 'mst_gig_sub_category_price_dtl_draft'
        default_permissions = ()             


# Master GigPriceExtraServiceDraft related data stored in this table
class GigPriceExtraServiceDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    price_extra_service = models.ForeignKey(PriceExtraService,on_delete=models.DO_NOTHING)
    price_type = models.ForeignKey(PriceType,on_delete=models.DO_NOTHING,null=True)
    extra_price = models.DecimalField(max_digits=9,decimal_places=2,null=True)
    extra_days = models.ForeignKey(ExtraDays,on_delete=models.DO_NOTHING,null=True)
    extra_day = models.IntegerField(null=True)


    class Meta :
        db_table = 'mst_gig_price_extra_service_draft'
        default_permissions = ()   


# Master GigPriceExtraServiceCustomDraft related data stored in this table
class GigPriceExtraServiceCustomDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)    
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    extra_price = models.DecimalField(max_digits=9,decimal_places=2,null=True)
    extra_days = models.ForeignKey(ExtraDays,on_delete=models.DO_NOTHING,null=True)
    extra_day = models.IntegerField(null=True)
   
    class Meta :
        db_table = 'mst_gig_price_extra_service_custom_draft'
        default_permissions = ()              