from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from masters.models import Category,SubCategory
from configs.models import GigStatus,QuestionForm
from gigs.models import GigDraft,Gig


# Master GigRequirement related data stored in this table
class GigRequirement(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    is_mandatory = models.BooleanField(default=False)
    question = models.CharField(max_length=4000)
    question_form = models.ForeignKey(QuestionForm,on_delete=models.DO_NOTHING) 
    is_multiselect = models.BooleanField(default=False)

    class Meta :
        db_table = 'mst_gig_requirement'
        default_permissions = ()


# Master GigRequirementDetails related data stored in this table
class GigRequirementDetails(SoftDeleteModel) :
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    gig_requirement = models.ForeignKey(GigRequirement,on_delete=models.DO_NOTHING) 
    description = models.CharField(max_length=1000)

    class Meta :
        db_table = 'mst_gig_requirement_dtl'
        default_permissions = ()


# Master GigRequirementDraft related data stored in this table
class GigRequirementDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    is_mandatory = models.BooleanField(default=False)
    question = models.CharField(max_length=4000)
    question_form = models.ForeignKey(QuestionForm,on_delete=models.DO_NOTHING) 
    is_multiselect = models.BooleanField(default=False)

    class Meta :
        db_table = 'mst_gig_requirement_draft'
        default_permissions = ()


# Master GigRequirementDetailsDraft related data stored in this table
class GigRequirementDetailsDraft(SoftDeleteModel) :
    gig_draft = models.ForeignKey(GigDraft,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING,null=True)
    gig_requirement_draft = models.ForeignKey(GigRequirementDraft,on_delete=models.DO_NOTHING)
    description = models.CharField(max_length=100)
   
    class Meta :
        db_table = 'mst_gig_requirement_dtl_draft'
        default_permissions = ()        