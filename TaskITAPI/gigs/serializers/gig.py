from rest_framework import serializers
from gigs.models import GigSearchTag, GigSearchTagDraft, Gig, GigDraft, GigLog, GigSubCategoryMetaDataTypeDraft, GigSubCategoryMetaDataTypeDraftDetails,\
    GigSubCategoryMetaDataTypeOtherDraft,GigSubCategoryMetaDataTypeOther,GigGalleryDraft, GigGallery, GigSubCategoryMetaDataType,GigSubCategoryMetaDataTypeDetails,\
        GigSubCategoryMetaDataTypeDraftDetails,GigSearchKeyword 
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from masters.serializers import CategoryMinSerializer,SubCategoryMinSerializer,MetaDataTypeSerializer,MetaDataTypeDetailsSerializer
from configs.serializers import GigStatusSerializer
from django.db import transaction
from django.urls import reverse
from configs.values import GIG_STATUS,GALLERY_TYPE
from django.db.models import Prefetch
from django.utils import timezone
from configs.models import GigGalleryType
from masters.models import MetaDataType,MetaDataTypeDetails,Category,SubCategory
from configs.serializers import GigGalleryTypeSerializer
from accounts.serializers import UserMinSerializer

def create_gig_tags(self,gig_draft,gig_tags) :
    tag_list = []
    for tag in gig_tags :
        t = GigSearchTagDraft(
            gig_draft = gig_draft,
            gig=gig_draft.gig,               
            search_tags = tag['search_tags']
        )
        tag_list.append(t)
    if len(tag_list) > 0 :
        GigSearchTagDraft.objects.bulk_create(tag_list)


def create_gig_log(self,gig_draft) :  
    gig_log = GigLog.objects.create(
        gig_draft = gig_draft,           
        gig = gig_draft.gig,
        gig_status = gig_draft.gig_status,
        created_by = self.context['request'].user,
        created_user = self.context['request'].user.user_name,
        created_date = timezone.now() 
    )
    return gig_log
   

def create_gig_suggest_other(self,meta,suggest_other) :
    other = GigSubCategoryMetaDataTypeOtherDraft.objects.create(
        gig_draft = meta.gig_draft,    
        gig = meta.gig_draft.gig,
        gig_sub_category_metadata_type_draft=meta,         
        **suggest_other
    )
    return other


def create_gig_metadata_type_draft_details(self,meta,metadata_type_draft_details,suggest_other) :     
    for metadata_dtl in metadata_type_draft_details :
        metadata_type_draft_dtl = GigSubCategoryMetaDataTypeDraftDetails(
            gig_draft = meta.gig_draft,  
            gig = meta.gig_draft.gig,
            gig_sub_category_metadata_type_draft=meta,             
            **metadata_dtl
        )
        metadata_type_draft_dtl.save()
    if meta.is_suggest_other and suggest_other:
        gig_suggest_other =  create_gig_suggest_other(self,meta,suggest_other)    
    return metadata_type_draft_details 


def create_gig_metadata(self,gig_draft,metadata_type_draft) :       
    for metadata in metadata_type_draft :     
        metadata_type_draft_details = metadata.pop('metadata_type_draft_details')
        suggest_other = metadata.pop('suggest_other')
        meta = GigSubCategoryMetaDataTypeDraft.objects.create(
            gig_draft = gig_draft, 
            gig = gig_draft.gig,             
            **metadata
        )
        gig_metadata_type_draft_details = create_gig_metadata_type_draft_details(self,meta,metadata_type_draft_details,suggest_other)
    return meta



class GigMinSerializer(serializers.ModelSerializer) :
    class Meta :
        model = Gig
        fields =[
            'id',
            'title',
            'is_approved',
            'description',
            'created_user'
        ]


class GigSearchTagMinSerializer(serializers.ModelSerializer) :    
    class Meta :
        model = GigSearchTag
        fields = [
            'id',
            'search_tags'
        ]
        read_only_fields = ['id']


class GigSearchTagDraftSerializer(serializers.ModelSerializer) :  

    class Meta :
        model = GigSearchTagDraft
        fields = [
            'id',
            'search_tags'
        ]
        read_only_fields = ['id']


class GigSearchTagDraftUpdateSerializer(serializers.ModelSerializer) :    
    id = serializers.IntegerField()

    class Meta :
        model = GigSearchTagDraft
        fields = [
            'id',
            'search_tags'
        ]
     
class GigsGalleryMinSerializer(serializers.ModelSerializer) :
    gig_gallery_type = GigGalleryTypeSerializer()

    class Meta :
        model = GigGallery
        fields = [
            'id',            
            'gig_gallery_type',
            'file_path',
            'file_path_thumbnail'
        ]


class GigsGalleryDraftMinSerializer(serializers.ModelSerializer) :
    gig_gallery_type = GigGalleryTypeSerializer()

    class Meta :
        model = GigGalleryDraft
        fields = [
            'id',       
            'gig_gallery_type',     
            'file_path',
            'file_path_thumbnail'
        ]


class GigOrderMinSerializer(serializers.ModelSerializer) :
    created_by = UserMinSerializer()
    gig_status = GigStatusSerializer()
    gallery = GigsGalleryMinSerializer(many=True)

    def setup_eager_loading(self,queryset,prefetch_prefix='') :
        queryset = queryset.prefetch_related(
            Prefetch(prefetch_prefix + 'giggallery_set',GigGallery.objects.filter(gig_gallery_type_id=GALLERY_TYPE['Photo']),to_attr='gallery')
        )
        return queryset

    class Meta :
        model = Gig
        fields =[
            'id',
            'title',           
            'description',
            'gig_status',
            'created_by',
            'gallery'
        ]


class GigSubCategoryMetaDataTypeSerializer(serializers.ModelSerializer) :
    sub_category = SubCategoryMinSerializer()
    metadata_type = MetaDataTypeSerializer()

    class Meta :
        model = GigSubCategoryMetaDataType
        fields = [
            'id',  
            'sub_category',          
            'metadata_type',
            'is_single',
            'is_mandatory',
            'min_value',
            'max_value',
            'is_suggest_other'             
        ]


class GigSubCategoryMetaDataTypeDetailsSerializer(serializers.ModelSerializer) :
    # sub_category_metadata_type = GigSubCategoryMetaDataTypeSerializer()
    metadata_type_dtl = MetaDataTypeDetailsSerializer()

    class Meta :
        model = GigSubCategoryMetaDataTypeDetails
        fields = [
            'id',                   
            # 'sub_category_metadata_type',
            'metadata_type_dtl'           
        ]


class GigSubCategoryMetaDataTypeDraftDetailsMinSerializer(serializers.ModelSerializer) :
    metadata_type_dtl = MetaDataTypeDetailsSerializer()

    class Meta :
        model = GigSubCategoryMetaDataTypeDraftDetails
        fields = [
            'id',                   
            'metadata_type_dtl'           
        ]


class GigSubCategoryMetadataTypeDraftDetailsSerializers(serializers.ModelSerializer) :
    metadata_type_dtl_id = serializers.IntegerField()

    def validate_metadata_type_dtl_id(self,value) :    
        if(not MetaDataTypeDetails.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for metadata type details id.'),code='invalid_input')
        return value

    class Meta :
        model = GigSubCategoryMetaDataTypeDraftDetails
        fields = [
            'id',
            'gig_draft',
            'gig',
            'metadata_type_dtl_id'                 
        ]
        read_only_fields = ['gig_draft','gig']


class GigSubCategoryMetadataTypeDraftDetailsUpdateSerializers(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    metadata_type_dtl_id = serializers.IntegerField()

    def validate_metadata_type_dtl_id(self,value) :    
        if(not MetaDataTypeDetails.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for metadata type details id.'),code='invalid_input')
        return value

    class Meta :
        model = GigSubCategoryMetaDataTypeDraftDetails
        fields = [
            'id',
            'gig_draft',
            'gig',
            'metadata_type_dtl_id'                 
        ]
        read_only_fields = ['gig_draft','gig']


class GigSubCategoryMetaDataTypeOtherDraftSerializers(serializers.ModelSerializer) :

    class Meta :
        model = GigSubCategoryMetaDataTypeOtherDraft
        fields = [
            'id',
            'gig_draft',
            'gig',
            'other',
        ]
        read_only_fields = ['gig_draft','gig']


class GigSubCategoryMetaDataTypeOtherDraftUpdateSerializers(serializers.ModelSerializer) :
    id = serializers.IntegerField()

    class Meta :
        model = GigSubCategoryMetaDataTypeOtherDraft
        fields = [
            'id',
            'gig_draft',
            'gig',
            'other',
        ]
        read_only_fields = ['gig_draft','gig']


class GigSubCategoryMetadataTypeDraftSerializers(serializers.ModelSerializer) :
    sub_category_id = serializers.IntegerField()
    metadata_type_id = serializers.IntegerField()
    metadata_type_draft_details = GigSubCategoryMetadataTypeDraftDetailsSerializers(many=True)
    suggest_other = GigSubCategoryMetaDataTypeOtherDraftSerializers(required=False,allow_null=True)

    
    def validate_sub_category_id(self,value) :    
        if(not SubCategory.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for sub category.'),code='invalid_input')
        return value

    def validate_metadata_type_id(self,value) :    
        if(not MetaDataType.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for metadata type.'),code='invalid_input')
        return value 

    class Meta :
        model = GigSubCategoryMetaDataTypeDraft
        fields = [
            'id',
            'gig_draft_id',
            'gig_id',
            'sub_category_id',
            'metadata_type_id',
            'is_single',
            'is_mandatory',
            'min_value',
            'max_value',
            'is_suggest_other',
            'metadata_type_draft_details',
            'suggest_other'           
        ]
        read_only_fields = ['gig_draft_id','gig_id']


class GigSubCategoryMetadataTypeDraftUpdateSerializers(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    sub_category_id = serializers.IntegerField()
    metadata_type_id = serializers.IntegerField()
    metadata_type_draft_details = GigSubCategoryMetadataTypeDraftDetailsUpdateSerializers(many=True)
    suggest_other = GigSubCategoryMetaDataTypeOtherDraftUpdateSerializers(allow_null=True,required=False)

    
    def validate_sub_category_id(self,value) :    
        if(not SubCategory.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for sub category.'),code='invalid_input')
        return value

    def validate_metadata_type_id(self,value) :    
        if(not MetaDataType.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for metadata type.'),code='invalid_input')
        return value 

    class Meta :
        model = GigSubCategoryMetaDataTypeDraft
        fields = [
            'id',
            'gig_draft_id',
            'gig_id',
            'sub_category_id',
            'metadata_type_id',
            'is_single',
            'is_mandatory',
            'min_value',
            'max_value',
            'is_suggest_other',
            'metadata_type_draft_details',
            'suggest_other'           
        ]
        read_only_fields = ['gig_draft_id','gig_id']


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/gigs/search-tags)
'''
class GigSearchTagSerializer(serializers.ModelSerializer) :
    gig = GigMinSerializer()

    def setup_eager_loading(self,queryset) :
        return queryset    

    class Meta :
        model = GigSearchTag
        fields = [
            'id',
            'gig_id',
            'gig',
            'search_tags'
        ]
        read_only_fields = ['gig']


'''
Serializer to receive data in specific format for GET method
(endpoint = '/gigs/search-extender)
'''
class SearchExtenderSerializer(serializers.Serializer) :
    search_id = serializers.IntegerField()
    search_name = serializers.SerializerMethodField()
    search_type = serializers.CharField()
   
    def get_search_name(self, instance) :
        return instance.search_name

    def validate_search_type(self, value) :
        if value not in ('category', 'sub_category', 'gigs_tag') :
            raise ValidationError(_('Invalid Search type.'),code='invalid_input')
        return value    



'''
Serializer to receive data in specific format for GET method
(endpoint = '/gigs/search-extender)
'''
class SearchKeywordSerializer(serializers.ModelSerializer) :

    class Meta :
        model = GigSearchKeyword
        fields = [
            'id',
            'search_keyword'
        ]
   
  

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/draft')
'''
class GigsDraftListSerializer(serializers.ModelSerializer) :   
    sub_category = SubCategoryMinSerializer(read_only=True)  
    category = CategoryMinSerializer(read_only=True)
    gig_status = GigStatusSerializer(read_only=True)
    gig_tags = GigSearchTagMinSerializer(many=True)
    gallery = GigsGalleryMinSerializer(many=True)   
   
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('gigsearchtagdraft_set',GigSearchTagDraft.objects.all(),to_attr='gig_tags'),
            Prefetch('giggallerydraft_set',GigGalleryDraft.objects.all(),to_attr='gallery'),
        )
        return queryset

    class Meta :
        model = GigDraft
        fields = [
            'id',
            'title',         
            'sub_category',            
            'category',
            'is_approved',
            'approved_by',
            'approved_user',
            'approved_date',
            'is_price_package',
            'description',            
            'gig_status',
            # 'total_views',
            # 'rating',
            'gig_tags',
            'gallery',                 
            'is_active',
            'created_by',
            'created_date',
            'created_user',
            'modified_by',
            'modified_user',
            'modified_date'
        ]



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/active-gig-update')
'''
class ActiveGigsDraftSerializer(serializers.ModelSerializer) :
    category_id = serializers.IntegerField(write_only=True)
    sub_category_id = serializers.IntegerField(write_only=True)
    is_price_package = serializers.BooleanField(required=False)    
    gig_tags = GigSearchTagDraftSerializer(many=True,write_only=True)    
    metadata_type_draft = GigSubCategoryMetadataTypeDraftSerializers(many=True,write_only=True)
    
    def validate_category_id(self,value) :    
        if(not Category.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for category.'),code='invalid_input')
        return value

    def validate_sub_category_id(self,value) :    
        if(not SubCategory.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for sub category.'),code='invalid_input')
        return value   

    class Meta :
        model = GigDraft
        fields = [
            'id',
            'title',
            'sub_category_id',
            'category_id',
            'is_approved',           
            'is_price_package',
            'description',   
            'gig_tags',
            'metadata_type_draft'    
        ]
          


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/:id')
'''
class GigsDraftSerializer(serializers.ModelSerializer) :
    sub_category_id = serializers.IntegerField(write_only=True)
    sub_category = SubCategoryMinSerializer(read_only=True)
    category_id = serializers.IntegerField(write_only=True)
    category = CategoryMinSerializer(read_only=True)
    is_price_package = serializers.BooleanField(required=False)    
    gig_status = GigStatusSerializer(read_only=True)
    gig_tags = GigSearchTagDraftSerializer(many=True)
    metadata_type_draft = GigSubCategoryMetadataTypeDraftSerializers(many=True,write_only=True)
    metadata_type_drafts = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('gigsearchtagdraft_set',GigSearchTagDraft.objects.all(),to_attr='gig_tags'),
        )
        return queryset

    def validate_category_id(self,value) :    
        if(not Category.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for category.'),code='invalid_input')
        return value

    def validate_sub_category_id(self,value) :    
        if(not SubCategory.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for sub category.'),code='invalid_input')
        return value    

    class Meta :
        model = GigDraft
        fields = [
            'id',
            'title',
            'sub_category_id',
            'sub_category',
            'category_id',
            'category',
            'is_approved',
            'approved_by',
            'approved_user',
            'approved_date',
            'is_price_package',
            'description',          
            'gig_status',
            'gig_tags',
            'metadata_type_draft', 
            'metadata_type_drafts',          
            'is_active',
            'created_by',
            'created_date',
            'created_user',
            'modified_by',
            'modified_user',
            'modified_date'
        ]
        read_only_fields = ['is_active','is_approved','approved_by','approved_user','approved_date',
            'created_by','created_date','created_user','modified_by','modified_user','modified_date'
        ]

    def get_metadata_type_drafts(self,instance) :
        metadata = []
        metadata_type_draft = GigSubCategoryMetaDataTypeDraft.objects.filter(gig_draft_id=instance.id)
        for meta_draft in metadata_type_draft :
            draft_dtl = None
            metadata_type_details = []
            metadata_type = MetaDataType.objects.get(id=meta_draft.metadata_type_id)
            metadata_draft_dtl = GigSubCategoryMetaDataTypeDraftDetails.objects.filter(gig_draft_id=instance.id,gig_sub_category_metadata_type_draft=meta_draft) 
            for dtl in metadata_draft_dtl :               
                metadata_type_dtl = MetaDataTypeDetails.objects.filter(id=dtl.metadata_type_dtl_id)
                for meta_dtl in metadata_type_dtl :
                    draft_dtl = {'id':meta_dtl.id,'metadata_type_name':meta_dtl.metadata_type_name}
                metadata_type_details.append(draft_dtl)
            suggest_other = GigSubCategoryMetaDataTypeOtherDraft.objects.filter(gig_draft=instance.id,gig_sub_category_metadata_type_draft=meta_draft)
            other = []
            for suggest in suggest_other :
                sug = {'id':suggest.id,'other':suggest.other}
                other.append(sug)
            meta = {
                'id' : meta_draft.id,
                'metadata_type' : {'id':metadata_type.id,'metadat_type_name' : metadata_type.metadata_type_name},
                'is_single' : meta_draft.is_single,
                'is_mandatory' : meta_draft.is_mandatory,
                'min_value' : meta_draft.min_value,
                'max_value' : meta_draft.max_value,
                'is_suggest_other': meta_draft.is_suggest_other,
                'metadata_type_draft_details' : metadata_type_details,
                'suggest_other' : other
            }
            metadata.append(meta)
        return metadata   

           
    @transaction.atomic
    def create(self,validated_data) :
        gig_tags = []
        gig_id = None       
        if 'gig_tags' in validated_data :
            gig_tags = validated_data.pop('gig_tags')
        metadata_type_draft = []
        if 'metadata_type_draft' in validated_data :
            metadata_type_draft = validated_data.pop('metadata_type_draft')

        gig_draft = GigDraft.objects.create(
            **validated_data,
            gig_id = gig_id,
            gig_status_id=GIG_STATUS['Draft'],
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name
            )       
     
        create_gig_tags(self,gig_draft, gig_tags)
        gig_log = create_gig_log(self,gig_draft)
        if len(metadata_type_draft) > 0 :
            gig_metadata = create_gig_metadata(self,gig_draft,metadata_type_draft)
        gig_draft.gig_tags = []
        gig_draft.metadata = []        
        return gig_draft


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/:id')
'''
class GigsDraftDetailsUpdateSerializer(serializers.ModelSerializer) :
    sub_category_id = serializers.IntegerField(write_only=True)   
    category_id = serializers.IntegerField(write_only=True)    
    is_price_package = serializers.BooleanField(required=False)       
    gig_tags = GigSearchTagDraftUpdateSerializer(many=True)
    metadata_type_draft = GigSubCategoryMetadataTypeDraftUpdateSerializers(many=True,write_only=True)
   
    def setup_eager_loading(self,queryset) :       
        return queryset

    def validate_category_id(self,value) :    
        if(not Category.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for category.'),code='invalid_input')
        return value

    def validate_sub_category_id(self,value) :    
        if(not SubCategory.objects.filter(id=value).exists()) :
            raise ValidationError(_('Invalid input received for sub category.'),code='invalid_input')
        return value    

    class Meta :
        model = GigDraft
        fields = [
            'id',
            'title',
            'sub_category_id',           
            'category_id',          
            'is_approved',
            'approved_by',
            'approved_user',
            'approved_date',
            'is_price_package',
            'description',          
            'gig_status',
            'gig_tags',
            'metadata_type_draft',                       
            'is_active',
            'created_by',
            'created_date',
            'created_user',
            'modified_by',
            'modified_user',
            'modified_date'
        ]
        read_only_fields = ['is_active','is_approved','approved_by','approved_user','approved_date',
            'created_by','created_date','created_user','modified_by','modified_user','modified_date'
        ]

    def update_gig_tags(self,gig_draft,gig_tags) :
        for tag in gig_tags :
            if tag['id'] != 0 :
                GigSearchTagDraft.objects.filter(gig=tag['id']).update(
                    search_tags=tag['search_tags']
                )
            else :
                GigSearchTagDraft.objects.create(
                    gig_draft=gig_draft,
                    search_tags=tag['search_tags']
                ) 

    def update_gig_metadata_type_draft_details(self,meta,metadata_type_draft_details) :
        for metadata_dtl in metadata_type_draft_details :
            if metadata_dtl['id'] != 0 :
                GigSubCategoryMetaDataTypeDraftDetails.objects.filter(id=metadata_dtl['id']).update(
                    **metadata_dtl
                )
            else :
                metadata_type_draft_dtl = GigSubCategoryMetaDataTypeDraftDetails.objects.create(
                    gig_draft = meta.gig_draft,  
                    gig = meta.gig,
                    gig_sub_category_metadata_type_draft=meta,             
                    metadata_type_dtl_id = metadata_dtl['metadata_type_dtl_id']
                )
                     
    def update_gig_suggest_other(self,meta,suggest_other) :
        if suggest_other and suggest_other['id'] != 0 :
            other = GigSubCategoryMetaDataTypeOtherDraft.objects.filter(id=suggest_other['id']).update(
                **suggest_other
            )
        else :
            other = GigSubCategoryMetaDataTypeOtherDraft.objects.create(
                gig_draft = meta.gig_draft,    
                gig = meta.gig_draft.gig,
                gig_sub_category_metadata_type_draft=meta,         
                other = suggest_other['other']
            )
       
    def update_metadata_type_draft(self,gig_draft,metadata_type_draft) :
        suggest_other = None
        metadata_type_draft_details = []
        for metadata_type in metadata_type_draft :   
            if 'metadata_type_draft_details' in metadata_type :
                metadata_type_draft_details = metadata_type.pop('metadata_type_draft_details')
            if 'suggest_other' in metadata_type :
                suggest_other = metadata_type.pop('suggest_other')
            if metadata_type['id'] != 0 :
                meta_data = GigSubCategoryMetaDataTypeDraft.objects.filter(id=metadata_type['id'])
                meta = meta_data[0]
                meta_drf = meta_data.update(
                    **metadata_type
                )               
                # meta = metadata_type_drf[0]
            else :
                meta = GigSubCategoryMetaDataTypeDraft.objects.create(
                    gig_draft = gig_draft, 
                    gig = gig_draft.gig,             
                    **metadata_type
                )
            self.update_gig_metadata_type_draft_details(meta,metadata_type_draft_details)
            if suggest_other :
                self.update_gig_suggest_other(meta,suggest_other)
              

    @transaction.atomic
    def update(self, gig_draft, validated_data) :
        gig_tags = []
        metadata_type_draft = []
        if 'gig_tags' in validated_data :
            gig_tags = validated_data.pop('gig_tags') 
        if 'metadata_type_draft' in validated_data :
            metadata_type_draft = validated_data.pop('metadata_type_draft')          
        gig_update_fields = []
        for key, value in validated_data.items() :
            if getattr(gig_draft, key) != value :
                gig_update_fields.append(key)
                setattr(gig_draft, key, value)
        gig_draft.modified_by = self.context['request'].user
        gig_draft.modified_user = self.context['request'].user.user_name
        gig_draft.modified_date = timezone.now()
        gig_update_fields = gig_update_fields + ['modified_by', 'modified_date', 'modified_user']
        gig_draft.save(update_fields = gig_update_fields)
        # Update gig tags list for gig
        if len(gig_tags) > 0 :
            self.update_gig_tags(gig_draft, gig_tags)
        # Update gig metadata type draft list for gig
        if len(metadata_type_draft) > 0 :
            self.update_metadata_type_draft(gig_draft, metadata_type_draft)    
        gig_draft.gig_tags = []
        gig_draft.metadata = []   
        return gig_draft    


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/:id')
'''
class GigsSerializer(serializers.ModelSerializer) :
    sub_category_id = serializers.IntegerField(write_only=True)
    sub_category = SubCategoryMinSerializer(read_only=True)
    category_id = serializers.IntegerField(write_only=True)
    category = CategoryMinSerializer(read_only=True)
    is_price_package = serializers.BooleanField(required=False)    
    gig_status = GigStatusSerializer(read_only=True)
    gig_tags = GigSearchTagMinSerializer(many=True)
    # gallery = GigsGalleryMinSerializer(many=True)
    metadata_type_draft = GigSubCategoryMetadataTypeDraftSerializers(many=True,write_only=True)
    metadata_type = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('gigsearchtag_set',GigSearchTag.objects.all(),to_attr='gig_tags'),
            # Prefetch('giggallerydraft_set',GigGalleryDraft.objects.all(),to_attr='gallery'),           
        )
        return queryset

    class Meta :
        model = Gig
        fields = [
            'id',
            'title',
            'sub_category_id',
            'sub_category',
            'category_id',
            'category',
            'is_approved',
            'approved_by',
            'approved_user',
            'approved_date',
            'is_price_package',
            'description',          
            'gig_status',
            'total_views',
            'rating',
            'gig_tags',
            # 'gallery',
            'metadata_type_draft',       
            'metadata_type',    
            'is_active',
            'created_by',
            'created_date',
            'created_user',
            'modified_by',
            'modified_user',
            'modified_date'
        ]

    def get_metadata_type(self,instance) :
        metadata = []
        metadata_type = GigSubCategoryMetaDataType.objects.filter(gig_id=instance.id)
        for meta in metadata_type :
            draft_dtl = None
            metadata_type_details = []
            metadata_type = MetaDataType.objects.get(id=meta.metadata_type_id)
            metadata_dtl = GigSubCategoryMetaDataTypeDetails.objects.filter(gig_id=instance.id,sub_category_metadata_type=meta) 
            for dtl in metadata_dtl :                
                metadata_type_dtl = MetaDataTypeDetails.objects.filter(id=dtl.metadata_type_dtl_id)    
                for d in metadata_type_dtl :                           
                    draft_dtl={'id':d.id,'metadata_type_name':d.metadata_type_name}
                metadata_type_details.append(draft_dtl)    
            suggest_other = GigSubCategoryMetaDataTypeOther.objects.filter(gig_id=instance.id)
            other = None
            for suggest in suggest_other :
                sug = {'id':suggest.id,'other':suggest.other}
                other = sug
            meta = {
                'metadata_type' : {'id':metadata_type.id,'metadat_type_name' : metadata_type.metadata_type_name},
                'is_single' : meta.is_single,
                'is_mandatory' : meta.is_mandatory,
                'min_value' : meta.min_value,
                'max_value' : meta.max_value,
                'is_suggest_other': meta.is_suggest_other,
                'metadata_type_details' : metadata_type_details,
                'suggest_other' : other
            }
            metadata.append(meta)
        return metadata   
    


'''
Serializer to send data in specific format for POST method
(endpoint = '/gigs/update:id')
'''
class GigsDraftUpdateSerializer(serializers.ModelSerializer) :
    sub_category_id = serializers.IntegerField(write_only=True)    
    category_id = serializers.IntegerField(write_only=True)    
    is_price_package = serializers.BooleanField(required=False)
    gig_tags = GigSearchTagMinSerializer(many=True)    
    metadata_type_draft = GigSubCategoryMetadataTypeDraftSerializers(many=True,write_only=True)
   

    class Meta :
        model = GigDraft
        fields = [
            'id',
            'title',
            'sub_category_id',            
            'category_id',
            'is_approved',
            'approved_by',
            'approved_user',
            'approved_date',
            'is_price_package',
            'description',    
            'gig_tags',
            'metadata_type_draft', 
            'is_active',
            'created_by',
            'created_date',
            'created_user',
            'modified_by',
            'modified_user',
            'modified_date'
        ]
        read_only_fields = ['is_active','is_approved','approved_by','approved_user','approved_date',
            'created_by','created_date','created_user','modified_by','modified_user','modified_date'
        ]

    @transaction.atomic
    def create(self,validated_data) :
        gig_tags = []       
        gig_id = self.kwargs['id']
        if 'gig_tags' in validated_data :
            gig_tags = validated_data.pop('gig_tags')
        metadata_type_draft = []
        if 'metadata_type_draft' in validated_data :
            metadata_type_draft = validated_data.pop('metadata_type_draft')

        gig_draft = GigDraft.objects.create(
            **validated_data,
            gig_id = gig_id,
            gig_status_id=GIG_STATUS['Draft'],
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name
            )       
     
        create_gig_tags(self,gig_draft, gig_tags)
        gig_log = create_gig_log(self,gig_draft)
        if len(metadata_type_draft) > 0 :
            gig_metadata = create_gig_metadata(self,gig_draft,metadata_type_draft)
        gig_draft.gig_tags = []
        gig_draft.metadata = []        
        return gig_draft    