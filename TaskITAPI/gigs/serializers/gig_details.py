from rest_framework import serializers
from gigs.models import Gig,GigPrice,GigSubCategoryPriceDetails,GigPriceExtraService,GigPriceExtraServiceDraft,GigGallery,GigSubCategoryMetaDataType,GigPriceExtraServiceCustom,GigPriceExtraServiceCustomDraft
from configs.models import PriceType,ExtraDays
from masters.models import PriceExtraService
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.db.models import Prefetch
from django.utils import timezone
from gigs.serializers import GigPriceSerializer,GigSubCategoryPriceDetailsMinSerializer,GigsGalleryMinSerializer,GigSubCategoryMetaDataTypeSerializer,GigPriceExtraServiceCustomDraftSerializer,GigPriceExtraServiceCustomMinSerializer
from masters.serializers import ExtraPriceServiceSerializer,ExtraPriceServiceMinSerializer
from configs.serializers import PriceTypeSerializer,ExtraDaysSerializer
from configs.values import GALLERY_TYPE


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gig-extra-services/<int:pk>')
'''
class GigPriceExtraServiceSerializer(serializers.ModelSerializer) :    
    price_extra_service = ExtraPriceServiceSerializer()
    price_type = PriceTypeSerializer()
    extra_days = ExtraDaysSerializer()

    def setup_eager_loading(self,queryset) :          
        return queryset

    class Meta :
        model = GigPriceExtraService
        fields = [
            'id', 
            'price_extra_service',           
            'price_type',    
            'extra_price',
            'extra_days',
            'extra_day'                      
        ]         


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gig-extra-services/<int:pk>')
'''
class GigPriceExtraServiceMinSerializer(serializers.ModelSerializer) :    
    price_extra_service = ExtraPriceServiceMinSerializer()
    price_type = PriceTypeSerializer()

    def setup_eager_loading(self,queryset) :          
        return queryset

    class Meta :
        model = GigPriceExtraService
        fields = [
            'id', 
            'price_extra_service',           
            'price_type',    
            'extra_price',
            'extra_days_id',
            'extra_day'                      
        ]

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gig-min-details/<int:pk>')
'''
class GigMinDetailsSerializer(serializers.ModelSerializer) : 
    gigs_pricing = serializers.SerializerMethodField()
    gallery = GigsGalleryMinSerializer(many=True)  
    extra_services = GigPriceExtraServiceMinSerializer(many=True)
    extra_custom_services = serializers.SerializerMethodField()


    def setup_eager_loading(self,queryset) :  
        price_type_id =  self.request.GET.get('price_type_id')
        queryset = queryset.prefetch_related(           
            Prefetch('giggallery_set',GigGallery.objects.filter(gig_gallery_type=GALLERY_TYPE['Photo']),to_attr='gallery'),
            Prefetch('gigpriceextraservice_set',GigPriceExtraService.objects.all(),to_attr='extra_services'),
        )      
        return queryset

    class Meta :
        model = Gig
        fields = [
            'id', 
            'title',           
            'description',    
            'total_views',
            'is_price_package',
            'rating',
            'rated_no_of_records',
            'gigs_pricing',
            'gallery', 
            'extra_services',
            'extra_custom_services',
            'created_by',
            'created_user'      
        ]    

    def get_gigs_pricing(self,instance) :
        gigs_pricing = None
        dtl = []
        price_type_id =  self.context['request'].GET.get('price_type_id')
        gig_price = GigPrice.objects.filter(gig_id=instance.id,price_type_id=price_type_id)
        for gig in gig_price :
            _gig_price = GigPriceSerializer(gig).data
            gig_price_details = GigSubCategoryPriceDetails.objects.filter(gig_price=gig)
            for price_dtl in gig_price_details :                
                _gig_price_dtl = GigSubCategoryPriceDetailsMinSerializer(price_dtl).data
                dtl.append(_gig_price_dtl)
            gigs_pricing = {
                'gig_price' : _gig_price,
                'gig_price_details' : dtl
            }    
        return gigs_pricing    

    def get_extra_custom_services(self,instance) :
        extra_custom_services = []
        custom_services = GigPriceExtraServiceCustom.objects.filter(gig_id=instance.id)
        for custom in custom_services :
            extra_custom_services.append(GigPriceExtraServiceCustomMinSerializer(custom).data)
        return extra_custom_services

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/:gig_draft_id')
'''
class GigDraftPriceExtraServiceSerializer(serializers.ModelSerializer) :    
    price_extra_service = ExtraPriceServiceMinSerializer()
    price_type = PriceTypeSerializer()
    extra_days = ExtraDaysSerializer()

    def setup_eager_loading(self,queryset) :          
        return queryset

    class Meta :
        model = GigPriceExtraServiceDraft
        fields = [
            'id', 
            'price_extra_service',           
            'price_type',    
            'extra_price',
            'extra_days',
            'extra_day',
        ]         
     