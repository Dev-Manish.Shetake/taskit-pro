from rest_framework import serializers
from django.db import transaction
from django.db.models import Prefetch
from django.utils import timezone
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from gigs.models import GigFaq,GigFaqDraft


class GigFaqSerializer(serializers.ModelSerializer) :

    class Meta :
        model = GigFaq
        fields =[
            'id',
            'seq_no',
            'question',        
            'answer'    
        ]


class GigFaqDraftSerializer(serializers.ModelSerializer) :

    class Meta :
        model = GigFaqDraft
        fields =[
            'id',
            'seq_no',
            'question',        
            'answer'    
        ]
