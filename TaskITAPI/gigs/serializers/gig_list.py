from rest_framework import serializers
from gigs.models import Gig,GigDraft,GigSearchTag,GigSearchTagDraft,GigGallery,GigGalleryDraft,GigPrice,GigPriceDraft,VWGigsData
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from masters.serializers import CategoryMinSerializer,SubCategoryMinSerializer,MetaDataTypeSerializer
from configs.serializers import GigStatusSerializer
from django.db import transaction
from configs.values import GIG_STATUS
from django.db.models import Prefetch,OuterRef,Subquery
from django.utils import timezone
from configs.models import GigGalleryType
from gigs.serializers import GigSearchTagMinSerializer,GigsGalleryMinSerializer,GigSubCategoryMetadataTypeDraftSerializers,GigPriceSerializer
from orders.models import Order
from configs.values import PRICE_TYPE,GALLERY_TYPE
from TaskITAPI.settings import BASE_URL

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/list')
'''
class GigListSerializer(serializers.ModelSerializer) :   
    sub_category = SubCategoryMinSerializer(read_only=True)    
    category = CategoryMinSerializer(read_only=True)    
    gig_status = GigStatusSerializer(read_only=True)    
    gallery = GigsGalleryMinSerializer(many=True)   
   
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(           
            Prefetch('giggallery_set',GigGallery.objects.filter(gig_gallery_type_id=1),to_attr='gallery')            
        )
        return queryset.order_by('-created_date')

    class Meta :
        model = Gig
        fields = [
            'id',
            'title',            
            'sub_category',           
            'category',
            'is_approved', 
            'description',           
            'gig_status',
            'total_views',
            'rating',           
            'gallery',                  
            'is_active',
            'created_by',
            'created_date',
            'created_user',
            'modified_by',
            'modified_user',
            'modified_date'
        ]
       



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/draft-backoffice-list')
'''
class BackOfficeDraftGigsListSerializer(serializers.ModelSerializer) :   
    sub_category = SubCategoryMinSerializer(read_only=True)    
    category = CategoryMinSerializer(read_only=True)    
    gig_status = GigStatusSerializer(read_only=True)    
    gallery = GigsGalleryMinSerializer(many=True)   
    gig_price = GigPriceSerializer(many=True)
    orders_count = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(           
            Prefetch('giggallerydraft_set',GigGalleryDraft.objects.filter(gig_gallery_type_id=GALLERY_TYPE['Photo']),to_attr='gallery'),
            Prefetch('gigpricedraft_set',GigPriceDraft.objects.filter(price_type_id=PRICE_TYPE['Basic']),to_attr='gig_price')             
        )        
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = GigDraft
        fields = [
            'id',
            'title',            
            'sub_category',           
            'category',
            'is_approved', 
            'description',           
            'gig_status',                  
            'gallery',    
            'gig_price',    
            'orders_count',          
            'is_active',
            'created_by',
            'created_date',
            'created_user',
            'modified_by',
            'modified_user',
            'modified_date'
        ]
       
    def get_orders_count(self , instance) :
        orders_count = Order.objects.filter(gig_id=instance.id).count()
        return orders_count  


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/backoffice-list')
'''
class BackOfficeGigsListSerializer(serializers.ModelSerializer) :   
    sub_category = SubCategoryMinSerializer(read_only=True)    
    category = CategoryMinSerializer(read_only=True)    
    gig_status = GigStatusSerializer(read_only=True)    
    gallery = GigsGalleryMinSerializer(many=True)   
    gig_price = GigPriceSerializer(many=True)
    orders_count = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(           
            Prefetch('giggallery_set',GigGallery.objects.filter(gig_gallery_type_id=GALLERY_TYPE['Photo']),to_attr='gallery'),
            Prefetch('gigprice_set',GigPrice.objects.filter(price_type_id=PRICE_TYPE['Basic']),to_attr='gig_price')             
        )        
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = Gig
        fields = [
            'id',
            'title',            
            'sub_category',           
            'category',
            'is_approved', 
            'description',           
            'gig_status',
            'total_views',
            'rating',           
            'gallery',    
            'gig_price',    
            'orders_count',          
            'is_active',
            'created_by',
            'created_date',
            'created_user',
            'modified_by',
            'modified_user',
            'modified_date'
        ]
       
    def get_orders_count(self , instance) :
        orders_count = Order.objects.filter(gig_id=instance.id).count()
        return orders_count  

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/backoffice-list')
'''
class BackOfficeVWGigsDataListSerializer(serializers.ModelSerializer) : 
    gallery_file_path = serializers.SerializerMethodField()
    gallery_file_path_thumbnail = serializers.SerializerMethodField()
    
    def setup_eager_loading(self,queryset) :       
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = VWGigsData
        fields = [       
            'id',    
            'gig_id',
            'title',            
            'sub_category_id',  
            'sub_category_name',         
            'category_id',
            'category_name',
            'gig_status_id',
            'gig_status_name',
            'total_views',
            'rating',
            'gallery_id',
            'gallery_file_path',
            'gallery_file_path_thumbnail', 
            'orders_count',      
            'cancelled_orders_count',     
            'is_active',
            'created_by_id',
            'created_date',
            'created_user',  
            'modified_date',
            'gig_price'
        ]

    def get_gallery_file_path(self,instance) :
        gallery_file_path = None
        if instance.gallery_file_path :
            gallery_file_path = BASE_URL +'/media/'+ instance.gallery_file_path
        return gallery_file_path    

    def get_gallery_file_path_thumbnail(self,instance) :
        gallery_file_path_thumbnail = None
        if instance.gallery_file_path_thumbnail :
            gallery_file_path_thumbnail = BASE_URL +'/media/'+ instance.gallery_file_path_thumbnail
        return gallery_file_path_thumbnail    
       

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/backoffice-list-count')
'''
class BackOfficeVWGigsDataListCountSerializer(serializers.Serializer) :
    total_gig = serializers.IntegerField()
    total_active_gig = serializers.IntegerField()
    total_pending_approval_gig = serializers.IntegerField()
    total_required_modification_gig = serializers.IntegerField()
    total_draft_gig = serializers.IntegerField()
    total_rejected_gig = serializers.IntegerField()
    total_paused_gig = serializers.IntegerField()

      