from rest_framework import serializers
from gigs.models import GigSubCategoryMetaDataType
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.db.models import Prefetch
from django.utils import timezone
from masters.serializers import SubCategoryMinSerializer,MetaDataTypeSerializer


