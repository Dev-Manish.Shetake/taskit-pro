from rest_framework import serializers
from gigs.models import GigSearchTag, GigSearchTagDraft, Gig, GigDraft, GigLog, GigSubCategoryMetaDataTypeDraft, GigSubCategoryMetaDataTypeDraftDetails,\
    GigSubCategoryMetaDataTypeOtherDraft,GigSubCategoryMetaDataTypeOther,GigGalleryDraft, GigGallery, GigSubCategoryMetaDataType,GigSubCategoryMetaDataTypeDetails,\
        GigSubCategoryMetaDataTypeDraftDetails,GigFaq,GigFaqDraft,GigRequirement,GigRequirementDetails,GigRequirementDraft,GigRequirementDetailsDraft,GigPrice,GigPriceDraft,\
            GigSubCategoryPriceDetails,GigSubCategoryPriceDetailsDraft,GigPriceExtraService,GigPriceExtraServiceDraft,GigPriceExtraServiceCustom,GigPriceExtraServiceCustomDraft
from gigs.views.gig_pricing import GigPriceDraftSerializer,GigPriceExtraServiceDraftSerializer,GigPriceExtraServiceCustomDraftSerializer
from gigs.views.gig_requirements import GigsDraftRequirementSerializer
from gigs.views.gig_gallery import ActiveGigGallerySerializer
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.db import transaction
from django.utils import timezone
from configs.values import GIG_STATUS
from gigs.serializers import ActiveGigsDraftSerializer,GigFaqDraftSerializer
from gigs.utils import create_gig_log
import os,shutil
from TaskITAPI.settings import MEDIA_ROOT


# Create Gigs Serch tags in draft table
def create_gig_draft_search_tags(self,gig_draft,gig_tags,gig_id) :
    tag_list = []
    if len(gig_tags) > 0:              
        for tag in gig_tags :
            t = GigSearchTagDraft(
                gig_draft = gig_draft,
                gig = gig_draft.gig,               
                search_tags = tag['search_tags']
            )
            tag_list.append(t)
        if len(tag_list) > 0 :
            GigSearchTagDraft.objects.bulk_create(tag_list)
    else :
        gig_search_tags = GigSearchTag.objects.filter(gig=gig_id)
        for tag in gig_search_tags :
            t = GigSearchTagDraft(
                gig_draft = gig_draft,
                gig = gig_draft.gig,               
                search_tags = tag.search_tags
            )
            tag_list.append(t)
        if len(tag_list) > 0 :
            GigSearchTagDraft.objects.bulk_create(tag_list)


# Create Gigs Gallery in gig gallery draft
def create_gig_gallery(self,gig_draft,gallery,gig_id) :    
    if len(gallery) > 0 :
        for gal in gallery :   
            if gal['id'] != 0 :
                gig_gallery_draft = GigGalleryDraft(
                    gig_draft = gig_draft,              
                    gig_gallery_type_id = gal['gig_gallery_type_id'],
                    file_path = gal['file_path'],
                    file_path_thumbnail = gal['file_path_thumbnail']
                )
                gig_gallery_draft.save()   
            else :
                gallery_list = GigGallery.objects.filter(id=gal['id'])
                for gallery in gallery_list :
                    source_file_path = os.path.join(MEDIA_ROOT,str(gig_gallery.file_path)).replace('\\', '/')
                    folder_file_path = "GigDraft/Gallery/File_Path/"+str(gig_draft.id)+"/"

                    source_file_path_thumbnail = os.path.join(MEDIA_ROOT,str(gig_gallery.file_path_thumbnail)).replace('\\', '/')
                    folder_file_path_thumbnail = "GigDraft/Gallery/File_Path_Thumbnail/"+str(gig_draft.id)+"/"

                    dest_file_path = os.path.join(MEDIA_ROOT,folder_file_path).replace('\\', '/') 
                    dest_file_path_thumbnail = os.path.join(MEDIA_ROOT,folder_file_path_thumbnail).replace('\\', '/') 

                    if not os.path.exists(dest_file_path):
                        os.makedirs(dest_file_path)

                    if not os.path.exists(dest_file_path_thumbnail):
                        os.makedirs(dest_file_path_thumbnail) 
                    if os.path.exists(source_file_path) and os.path.exists(source_file_path_thumbnail) :
                        shutil.copy(source_file_path,dest_file_path)
                        file_path_name = os.path.basename(source_file_path)  
                        os.rename(str(dest_file_path)+str(file_path_name),str(dest_file_path)+str(gig_draft.id)+"_"+str(file_path_name))

                        shutil.copy(source_file_path_thumbnail,dest_file_path_thumbnail)   
                        file_path_thumbnail_name = os.path.basename(source_file_path_thumbnail)  
                        os.rename(str(dest_file_path_thumbnail)+str(file_path_thumbnail_name),str(dest_file_path_thumbnail)+str(gig_draft.id)+"_"+str(file_path_thumbnail_name))
                    
                        gig_draft_gallery = GigGalleryDraft.objects.create(
                            gig_draft = gig_draft,
                            gig_gallery_type = gallery.gig_gallery_type,
                            file_path = (folder_file_path+str(gig_draft.id)+"_"+str(file_path_name)).replace('\\', '/'), ##draft.file_path,
                            file_path_thumbnail = (folder_file_path_thumbnail+str(gig_draft.id)+"_"+str(file_path_thumbnail_name)).replace('\\', '/') #draft.file_path_thumbnail
                        )      
    else :                
        gig_gallery_list = GigGallery.objects.filter(gig_id=gig_id)
        for gig_gallery in gig_gallery_list :
            source_file_path = os.path.join(MEDIA_ROOT,str(gig_gallery.file_path)).replace('\\', '/')
            folder_file_path = "GigDraft/Gallery/File_Path/"+str(gig_draft.id)+"/"

            source_file_path_thumbnail = os.path.join(MEDIA_ROOT,str(gig_gallery.file_path_thumbnail)).replace('\\', '/')
            folder_file_path_thumbnail = "GigDraft/Gallery/File_Path_Thumbnail/"+str(gig_draft.id)+"/"

            dest_file_path = os.path.join(MEDIA_ROOT,folder_file_path).replace('\\', '/') 
            dest_file_path_thumbnail = os.path.join(MEDIA_ROOT,folder_file_path_thumbnail).replace('\\', '/') 

            if not os.path.exists(dest_file_path):
                os.makedirs(dest_file_path)

            if not os.path.exists(dest_file_path_thumbnail):
                os.makedirs(dest_file_path_thumbnail) 
           
            if os.path.exists(source_file_path) and os.path.exists(source_file_path_thumbnail) :
                shutil.copy(source_file_path,dest_file_path)
                file_path_name = os.path.basename(source_file_path)  
                os.rename(str(dest_file_path)+str(file_path_name),str(dest_file_path)+str(gig_draft.id)+"_"+str(file_path_name))

                shutil.copy(source_file_path_thumbnail,dest_file_path_thumbnail)   
                file_path_thumbnail_name = os.path.basename(source_file_path_thumbnail)  
                os.rename(str(dest_file_path_thumbnail)+str(file_path_thumbnail_name),str(dest_file_path_thumbnail)+str(gig_draft.id)+"_"+str(file_path_thumbnail_name))
            
                gig_draft_gallery = GigGalleryDraft.objects.create(
                    gig_draft = gig_draft,
                    gig_gallery_type = gig_gallery.gig_gallery_type,
                    file_path = (folder_file_path+str(gig_draft.id)+"_"+str(file_path_name)).replace('\\', '/'), ##draft.file_path,
                    file_path_thumbnail = (folder_file_path_thumbnail+str(gig_draft.id)+"_"+str(file_path_thumbnail_name)).replace('\\', '/') #draft.file_path_thumbnail
                )                    
    

# Create gigs metadata other suggestion 
def create_gig_suggest_other(self,meta,suggest_other) :
    other = GigSubCategoryMetaDataTypeOtherDraft.objects.create(
        gig_draft = meta.gig_draft,    
        gig = meta.gig_draft.gig,
        gig_sub_category_metadata_type_draft=meta,         
        **suggest_other
    )
    return other

'''
This method is used,when no request to update the record.
Then dump records from gigs main table draft table
'''
def update_gig_suggest_other(self,meta,gig_id) :
    gig_others = GigSubCategoryMetaDataTypeOther.objects.filter(gig_id=gig_id)
    for other in gig_others :
        other = GigSubCategoryMetaDataTypeOtherDraft.objects.create(
            gig_draft = meta.gig_draft,    
            gig = meta.gig_draft.gig,
            gig_sub_category_metadata_type_draft=meta,         
            other = other.other
        )



# Create gig metadata type draft details in details table
def create_gig_metadata_type_draft_details(self,meta,metadata_type_draft_details,suggest_other) :     
    for metadata_dtl in metadata_type_draft_details :
        metadata_type_draft_dtl = GigSubCategoryMetaDataTypeDraftDetails(
            gig_draft = meta.gig_draft,  
            gig = meta.gig_draft.gig,
            gig_sub_category_metadata_type_draft=meta,             
            **metadata_dtl
        )
        metadata_type_draft_dtl.save()
    if meta.is_suggest_other :
        gig_suggest_other =  create_gig_suggest_other(self,meta,suggest_other)    
    return metadata_type_draft_details 

'''
This method is used,when no request to update the record.
Then dump records from gigs main table draft table
'''
def update_gig_metadata_type_draft_details(self,meta,gig_draft,gig_id) :     
    gig_metadata_type_details = GigSubCategoryMetaDataTypeDetails.objects.filter(gig_id=gig_id)
    for metadata_dtl in gig_metadata_type_details :
        metadata_type_draft_dtl = GigSubCategoryMetaDataTypeDraftDetails(
            gig_draft = meta.gig_draft,  
            gig = meta.gig_draft.gig,
            gig_sub_category_metadata_type_draft=meta,             
            metadata_type_dtl = metadata_dtl.metadata_type_dtl
        )
        metadata_type_draft_dtl.save()
        if meta.is_suggest_other :
            gig_suggest_other =  update_gig_suggest_other(self,meta,gig_id)  
    return True 



# Create gig metadata type draft
def create_metadata_type_draft(self,gig_draft,metadata_type_draft) :  
    for metadata in metadata_type_draft :     
        metadata_type_draft_details = metadata.pop('metadata_type_draft_details')
        suggest_other = metadata.pop('suggest_other')
        meta = GigSubCategoryMetaDataTypeDraft.objects.create(
            gig_draft = gig_draft, 
            gig = gig_draft.gig,             
            **metadata
        )
        gig_metadata_type_draft_details = create_gig_metadata_type_draft_details(self,meta,metadata_type_draft_details,suggest_other)

'''
This method is used,when no request to update the record.
Then dump records from gigs main table draft table
'''
def update_metadata_type_draft(self,gig_draft,gig_id) :    
    gig_sub_metadata_type = GigSubCategoryMetaDataType.objects.filter(gig_id=gig_id) 
    for metadata_type in gig_sub_metadata_type :
        meta = GigSubCategoryMetaDataTypeDraft.objects.create(
            gig_draft = gig_draft, 
            gig = gig_draft.gig,   
            sub_category = metadata_type.sub_category,
            metadata_type = metadata_type.metadata_type,
            is_single = metadata_type.is_single,
            is_mandatory = metadata_type.is_mandatory,
            min_value = metadata_type.min_value,
            max_value = metadata_type.max_value,
            is_suggest_other = metadata_type.is_suggest_other         
        )
        gig_metadata_type_draft_details = update_gig_metadata_type_draft_details(self,meta,gig_draft,gig_id)
    
'''
This method is used for both when no request to update the record.
Then dump records from gigs main table draft table else create new records
'''
def create_faq_draft(self,gig_draft,faq,gig_id) :
    if len(faq) > 0 :
        for fa in faq :                
            gig_draft_faq = GigFaqDraft(
                gig_draft=gig_draft,               
                **fa
            )    
            gig_draft_faq.save()  
    else :
        gig_faq = GigFaq.objects.filter(gig_id=gig_id)    
        for fa in gig_faq :                
            gig_draft_faq = GigFaqDraft(
                gig_draft=gig_draft,               
                seq_no = fa.seq_no,
                question = fa.question,
                answer = fa.answer
            )    
            gig_draft_faq.save()             


'''
This method is used for both when no request to update the record.
Then dump records from gigs main table draft table else create new records
'''
def create_req_draft(self,gig_draft,requirements,gig_id) :
    if len(requirements) > 0 :
         for req in requirements :    
            description = req.pop('description')       
            gig_req = GigRequirementDraft(
                gig_draft = gig_draft,               
                # is_mandatory = req['is_mandatory'],
                # question = req['question'],
                # question_form_id = req['question_form_id'],
                # is_multiselect = req['is_multiselect']
                **req
            )      
            gig_req.save()
            for desc in description :
                gig_req_dtl = GigRequirementDetailsDraft(
                    gig_draft = gig_draft,                   
                    gig_requirement_draft = gig_req,
                    **desc
                )  
                gig_req_dtl.save() 
    else :
        gig_req = GigRequirement.objects.filter(gig_id=gig_id)    
        for r in gig_req :                
            gig_draft_req = GigRequirementDraft(
                gig_draft=gig_draft,               
                is_mandatory = r.is_mandatory,
                question = r.question,
                question_form_id = r.question_form_id,
                is_multiselect = r.is_multiselect
            )    
            gig_draft_req.save()     
            gig_req_dtl = GigRequirementDetails.objects.filter(gig_id=gig_id,gig_requirement=r.id)
            for desc in gig_req_dtl :
                    gig_req_dtl = GigRequirementDetailsDraft(
                        gig_draft = gig_draft,                   
                        gig_requirement_draft = gig_draft_req,
                        description = desc.description
                    )  
                    gig_req_dtl.save()     

'''
This method is used for both when no request to update the record.
Then dump records from gigs main table draft table else create new records
'''
def create_gig_price(self,gigs_price_lst,gig_draft,gig_id) :
    if len(gigs_price_lst) > 0 :
        for price in gigs_price_lst :
            if 'price_type_details' in price :
                price_type_details_list = price.pop('price_type_details')
            gig_price_draft = GigPriceDraft(
                gig_draft = gig_draft,                
                price_type_id = price['price_type_id'],
                price = price['price']
            )
            gig_price_draft.save()
            for price_dtl in price_type_details_list :
                gig_price_details = GigSubCategoryPriceDetailsDraft(
                    gig_draft = gig_draft,                    
                    gig_price_draft = gig_price_draft,
                    sub_category_price_scope_id = price_dtl['sub_category_price_scope_id'],
                    sub_category_price_scope_dtl_id = price_dtl['sub_category_price_scope_dtl_id'],
                    value = price_dtl['value']
                )
                gig_price_details.save() 
    else :
        gig_price_list = GigPrice.objects.filter(gig_id=gig_id)
        for gig_price in gig_price_list :
            gig_price_draft = GigPriceDraft(
                    gig_draft = gig_draft,                
                    price_type = gig_price.price_type,
                    price = gig_price.price
                )
            gig_price_draft.save()
            gig_sub_category_price_dtl = GigSubCategoryPriceDetails.objects.filter(gig_id=gig_id,gig_price_id=gig_price.id)    
            for price_dtl in gig_sub_category_price_dtl :
                gig_price_details = GigSubCategoryPriceDetailsDraft(
                    gig_draft = gig_draft,                    
                    gig_price_draft = gig_price_draft,
                    sub_category_price_scope = price_dtl.sub_category_price_scope,
                    sub_category_price_scope_dtl = price_dtl.sub_category_price_scope_dtl,
                    value = price_dtl.value
                )
                gig_price_details.save()  

'''
This method is used for both when no request to update the record.
Then dump records from gigs main table draft table else create new records
'''
def create_gigs_extra_price_service(self,gigs_extra_price,gig_draft,gig_id) :
    if len(gigs_extra_price) > 0 :
        for extra_price in gigs_extra_price :
            price_extra_service = GigPriceExtraServiceDraft(
                gig_draft= gig_draft,              
                price_extra_service_id = extra_price['price_extra_service_id'],
                price_type_id = extra_price['price_type_id'],
                extra_price = extra_price['extra_price'],
                extra_days_id = extra_price['extra_days_id'],
                extra_day = extra_price['extra_day'],         
            )   
            price_extra_service.save()
    else :
        gig_price_extra_service_list = GigPriceExtraService.objects.filter(gig_id=gig_id)
        for price_extra_service in gig_price_extra_service_list :
            price_extra_service = GigPriceExtraServiceDraft(
                gig_draft= gig_draft,              
                price_extra_service = price_extra_service.price_extra_service,
                price_type = price_extra_service.price_type,
                extra_price = price_extra_service.extra_price,
                extra_days = price_extra_service.extra_days,
                extra_day = price_extra_service.extra_day,         
            )   
            price_extra_service.save() 

'''
This method is used for both when no request to update the record.
Then dump records from gigs main table draft table else create new records
'''
def create_gigs_extra_price_custom_service(self,extra_service_custom,gig_draft,gig_id) :
    if len(extra_service_custom) > 0 :
        for price_custom in extra_service_custom :
            price_service_custom = GigPriceExtraServiceCustomDraft(
                gig_draft= gig_draft,     
                **price_custom
            )   
            price_service_custom.save()
    else :
        gig_price_extra_service_custom_list = GigPriceExtraServiceCustom.objects.filter(gig_id=gig_id)       
        for extra_price_custom in gig_price_extra_service_custom_list :
            price_service_custom = GigPriceExtraServiceCustomDraft(
                gig_draft= gig_draft,     
                title = extra_price_custom.title,
                description = extra_price_custom.description,
                extra_price = extra_price_custom.extra_price,
                extra_days = extra_price_custom.extra_days,
                extra_day = extra_price_custom.extra_day
            )   
            price_service_custom.save()


class ActiveGigPriceUpdate(serializers.Serializer) :
    is_price_package = serializers.BooleanField()
    gigs_price = GigPriceDraftSerializer(many=True)

class ActiveGigUpdateSerializer(serializers.Serializer) :
    gig_id = serializers.IntegerField()
    gig_info = ActiveGigsDraftSerializer(required=False)
    gallery = ActiveGigGallerySerializer(many=True,required=False)
    faq = GigFaqDraftSerializer(many=True,required=False)
    req = GigsDraftRequirementSerializer(many=True,required=False)
    gigs_price = ActiveGigPriceUpdate(required=False)
    gigs_extra_price = GigPriceExtraServiceDraftSerializer(many=True,required=False)
    extra_service_custom = GigPriceExtraServiceCustomDraftSerializer(many=True,required=False)   

    # @transaction.atomic
    # def create(self,validated_data) :
    #     gig_info = None
    #     gallery = []
    #     gig_id = validated_data['gig_id']
    #     gig_tags = []
    #     metadata_type_draft = []
    #     faq = []
    #     req = []
    #     gigs_price = None
    #     is_price_package = False
    #     gigs_extra_price = []
    #     extra_service_custom = []

        # if 'gig_info' in validated_data :
        #     gig_info = validated_data.pop('gig_info')
        # if 'gallery' in validated_data :
        #     gallery = validated_data.pop('gallery')    
        # if 'faq' in validated_data :
        #     faq = validated_data.pop('faq')  
        # if 'req' in validated_data :
        #     req = validated_data.pop('req')     
        # if 'gigs_price' in validated_data :
        #     gigs_price = validated_data.pop('gigs_price')      
        #     if 'is_price_package' in gigs_price :
        #         is_price_package = gigs_price.pop('is_price_package') 
        # if 'gigs_extra_price' in validated_data :
        #     gigs_extra_price = validated_data.pop('gigs_extra_price')  
        # if 'extra_service_custom' in validated_data :
        #     extra_service_custom = validated_data.pop('extra_service_custom')      

        # if gig_info :
        #     if 'gig_tags' in gig_info :
        #         gig_tags = gig_info.pop('gig_tags')
        #     if 'metadata_type_draft' in gig_info :
        #         metadata_type_draft = gig_info.pop('metadata_type_draft')     
        #     gig_draft = GigDraft.objects.create(
        #         **gig_info,
        #         gig_id = None,
        #         gig_status_id = GIG_STATUS['Draft'],
        #         created_by = self.context['request'].user,
        #         created_user = self.context['request'].user.user_name
        #     )  
        #     gigs = None
        #     gig_log = create_gig_log(self.context['request'],gig_draft,gigs)
        #     create_gig_draft_search_tags(self,gig_draft,gig_tags)
        #     create_metadata_type_draft(self,gig_draft,metadata_type_draft)
        # else :
        #     gig = Gig.objects.get(id=gig_id)
        #     gig_draft = GigDraft.objects.create(
        #         title = gig.title,
        #         category = gig.category,
        #         sub_category = gig.sub_category,
        #         is_price_package = gig.is_price_package,
        #         description = gig.description,
        #         gig_status = GIG_STATUS['draft'],
        #         created_by = self.context['request'].user,
        #         created_user = self.context['request'].user.user_name,
        #         created_date = timezone.now() 
        #     )
        #     gigs = None
        #     gig_log = create_gig_log(self,gig_draft,gigs)
        #     create_gig_draft_search_tags(self,gig_draft,gig)
        #     update_metadata_type_draft(self,gig_draft,gig)       
        # create_faq_draft(self,gig_draft,faq,gig_id)    
        # create_req_draft(self,gig_draft,req,gig_id)          
        # create_gig_price(self,gigs_price,gig_draft,gig_id)
        # create_gigs_extra_price_service(self,gigs_extra_price,gig_draft,gig_id)
        # create_gigs_extra_price_custom_service(self,extra_service_custom,gig_draft,gig_id)
        # create_gig_gallery(self,gig_draft,gallery)
        # gig_draft.is_price_package = is_price_package
        # gig_draft.save()
        # return gig_draft    

