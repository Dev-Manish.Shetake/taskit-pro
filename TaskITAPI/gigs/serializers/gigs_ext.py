from rest_framework import serializers
from gigs.models import GigSearchTag, GigSearchTagDraft, Gig, GigDraft,GigFaqDraft ,GigLog, GigSubCategoryMetaDataType,GigSubCategoryMetaDataTypeDetails, GigSubCategoryMetaDataTypeDraftDetails,\
    GigSubCategoryMetaDataTypeOtherDraft, GigGallery,GigGalleryDraft,GigSubCategoryPriceDetails,GigSubCategoryPriceDetailsDraft ,GigPrice,GigFaq,GigPriceDraft,GigPriceExtraServiceCustomDraft,GigPriceExtraServiceCustom,\
        UserFavouriteGigs        
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from masters.serializers import CategoryMinSerializer,SubCategoryMinSerializer,SubCategoryPriceScopeDetailsSerializer
from accounts.serializers import UserMinSerializer,UserPersonalInfoMinSerializer,UserAddressMinSerializer,UserRatingSerializer,SellerInfoMinSerializer
from accounts.models import UserPersonalInfo,UserAddress
from masters.models import SubCategoryPriceScope,SubCategoryPriceScopeDetails,SubCategory
from configs.serializers import GigStatusSerializer,ExtraDaysSerializer,ExtraPriceSerializer
from django.db import transaction
from configs.values import GIG_STATUS
from django.db.models import Prefetch,OuterRef,Subquery
from django.utils import timezone
from configs.models import GigGalleryType,City,State,Country
from gigs.serializers import GigSearchTagMinSerializer,GigSearchTagDraftSerializer,GigsGalleryMinSerializer,GigsGalleryDraftMinSerializer,GigSubCategoryMetaDataType,GigSubCategoryMetadataTypeDraftSerializers,\
    GigSubCategoryMetaDataTypeSerializer,GigSubCategoryMetaDataTypeDetailsSerializer,GigSubCategoryMetaDataTypeDraftDetailsMinSerializer
from gigs.serializers.gigs_pricing import GigSubCategoryPriceDetailsDraftMinSerializer,GigSubCategoryPriceDetailsMinSerializer,GigSubCategoryPriceDetailsSerializer,GigPriceSerializer,GigPriceDraftMinSerializer
from gigs.serializers.gig_faq import GigFaqSerializer,GigFaqDraftSerializer
from configs.serializers import PriceTypeSerializer
from orders.models import OrderRating,OrderComment
from gigs import values
from configs.models import ExtraDays,ExtraPrice
from masters.serializers import SubCategoryPriceScopeMinSerializer,SubCategoryPriceScopeDetailsSerializer
from configs.values import PRICE_TYPE,GALLERY_TYPE
from _requests.models import ServiceRequest,RequestOffer


class OrderCommentMinSerializer(serializers.ModelSerializer) :
    created_by = UserRatingSerializer()

    class Meta :
        model = OrderComment
        fields =[
            'id',
            'comment',
            'created_user',
            'created_by'   
        ]


class OrderRatingMinSerializer(serializers.ModelSerializer) :
    
    class Meta :
        model = OrderRating
        fields =[
            'id',
            'seller_feedback',
            'buyer_feedback',        
            'buyer_rating',
            'seller_rating',
            'created_user',
            'created_date'    
        ]


class OrderRatingReviewsSerializer(serializers.ModelSerializer) :
    
    class Meta :
        model = OrderRating
        fields =[
            'id',           
            'buyer_feedback',        
            'buyer_rating',           
            'created_user',
            'created_date'    
        ]


class GigSubCategoryMetadataTypeSerializers(serializers.ModelSerializer) :     

    class Meta :
        model = GigSubCategoryMetaDataType
        fields = [
            'id',            
            'gig_id',
            'sub_category_id',
            'metadata_type_id',
            'is_single',
            'is_mandatory',
            'min_value',
            'max_value',
            'is_suggest_other'                      
        ]        


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gigs/')
'''
class GigsSearchResultSerializer(serializers.ModelSerializer) :
    sub_category_id = serializers.IntegerField(write_only=True)
    sub_category = SubCategoryMinSerializer(read_only=True)
    category_id = serializers.IntegerField(write_only=True)
    category = CategoryMinSerializer(read_only=True)
    is_price_package = serializers.BooleanField(required=False)   
    gig_status = GigStatusSerializer(read_only=True)
    tags = GigSearchTagMinSerializer(many=True)
    gallery = GigsGalleryMinSerializer(many=True)  
    gigs_pricing = GigPriceSerializer(many=True)
    created_by = UserMinSerializer()
    is_favourite = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('gigsearchtag_set',GigSearchTag.objects.all(),to_attr='tags'),
            Prefetch('giggallery_set',GigGallery.objects.all(),to_attr='gallery'),
            Prefetch('gigprice_set',GigPrice.objects.all(),to_attr='gigs_pricing'),
        )
        return queryset

    class Meta :
        model = Gig
        fields = [
            'id',
            'title',
            'sub_category_id',
            'sub_category',
            'category_id',
            'category',
            'is_approved',
            'approved_by',
            'approved_user',
            'approved_date',
            'is_price_package',
            'description',           
            'gig_status',
            'total_views',
            'rating',
            'tags',
            'gallery',      
            'gigs_pricing',     
            'is_favourite',
            'is_active',
            'created_by',
            'created_date',                        
        ]
        read_only_fields = ['is_active','is_approved','approved_by','approved_user','approved_date','total_views',
            'created_by','created_date','modified_by','modified_user','modified_date','rating'
        ]

    def get_is_favourite(self,instance) :
        is_favourite = False
        if self.context['request'].user.is_anonymous == False :
            if UserFavouriteGigs.objects.filter(gig=instance.id,created_by=self.context['request'].user).exists() :
                is_favourite = True
        return is_favourite

    
'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gig-details/<int:pk>')
'''
class GigsDetailsSerializer(serializers.ModelSerializer) :    
    sub_category = SubCategoryMinSerializer(read_only=True)    
    category = CategoryMinSerializer(read_only=True)
    is_price_package = serializers.BooleanField(required=False)   
    gig_status = GigStatusSerializer(read_only=True)
    gig_tags = GigSearchTagMinSerializer(many=True)
    gallery = GigsGalleryMinSerializer(many=True)   
    # gigs_pricing = GigPriceSerializer(many=True)
    faq = GigFaqSerializer(many=True)
    created_by = UserMinSerializer()
    order_rating = OrderRatingMinSerializer(many=True)
    order_comment = OrderCommentMinSerializer(many=True)
    # metadata = GigSubCategoryMetaDataTypeSerializer(many=True)
    metadata = GigSubCategoryMetaDataTypeDetailsSerializer(many=True)
    about_seller = serializers.SerializerMethodField()
    seller_info = serializers.SerializerMethodField()
    order_rating_reviews = OrderRatingReviewsSerializer(many=True)
    last_delivery_date = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('gigsearchtag_set',GigSearchTag.objects.all(),to_attr='gig_tags'),
            Prefetch('giggallery_set',GigGallery.objects.all(),to_attr='gallery'),
            # Prefetch('gigprice_set',GigPrice.objects.all(),to_attr='gigs_pricing'),
            Prefetch('gigfaq_set',GigFaq.objects.all(),to_attr='faq'),
            Prefetch('gigsubcategorymetadatatypedetails_set',GigSubCategoryMetaDataTypeDetails.objects.all(),to_attr='metadata'),
            Prefetch('orderrating_set',OrderRating.objects.all(),to_attr='order_rating'),
            Prefetch('ordercomment_set',OrderComment.objects.all(),to_attr='order_comment'),
            Prefetch('orderrating_set',OrderRating.objects.all(),to_attr='order_rating_reviews'),
        )      
        return queryset

    class Meta :
        model = Gig
        fields = [
            'id',
            'title',            
            'sub_category',            
            'category',
            'is_approved',
            'approved_by',
            'approved_user',
            'approved_date',
            'is_price_package',
            'description',           
            'gig_status',
            'total_views',
            'rating',
            'gig_tags',
            'gallery',      
            # 'gigs_pricing',     
            'faq',
            'is_active',
            'created_by',
            'order_rating',
            'order_comment',
            'metadata',
            'about_seller',
            'seller_info',
            'order_rating_reviews',
            'created_date',       
            'last_delivery_date'                 
        ]

    def get_about_seller(self, instance) :
        about_seller = None
        user_info = UserPersonalInfo.objects.filter(user=instance.created_by.id)
        if len(user_info) > 0 :
            about_seller = UserPersonalInfoMinSerializer(user_info[0]).data
        return about_seller  

    def get_seller_info(self, instance) :
        seller_info = None
        user_info = UserAddress.objects.filter(user=instance.created_by.id)
        if len(user_info) > 0 :
            country = Country.objects.filter(
                id=State.objects.filter(
                    id__in=City.objects.filter(
                        id=user_info[0].city_id).values_list('state_id')
                    ).values_list('country_id')
                )
            seller_info = {
                'from' : country.name,                
            }
        return seller_info      

    def get_last_delivery_date(self, instance) :
        last_delivery_date = None        
        return last_delivery_date       



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gig-draft-details/<int:pk>')
'''
class GigsDraftDetailsSerializer(serializers.ModelSerializer) :  
    sub_category = SubCategoryMinSerializer(read_only=True)    
    category = CategoryMinSerializer(read_only=True)
    is_price_package = serializers.BooleanField(required=False)   
    gig_status = GigStatusSerializer(read_only=True)
    gig_tags = GigSearchTagDraftSerializer(many=True)
    gallery = GigsGalleryDraftMinSerializer(many=True)   
    faq = GigFaqDraftSerializer(many=True)
    created_by = UserMinSerializer()
    # order_rating = OrderRatingMinSerializer(many=True)
    # order_comment = OrderCommentMinSerializer(many=True)
    metadata = GigSubCategoryMetaDataTypeDraftDetailsMinSerializer(many=True)
    about_seller = serializers.SerializerMethodField()
    seller_info = serializers.SerializerMethodField()
    # order_rating_reviews = OrderRatingReviewsSerializer(many=True)
    # last_delivery_date = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('gigsearchtagdraft_set',GigSearchTagDraft.objects.all(),to_attr='gig_tags'),
            Prefetch('giggallerydraft_set',GigGalleryDraft.objects.all(),to_attr='gallery'),
            Prefetch('gigfaqdraft_set',GigFaqDraft.objects.all(),to_attr='faq'),
            Prefetch('gigsubcategorymetadatatypedraftdetails_set',GigSubCategoryMetaDataTypeDraftDetails.objects.all(),to_attr='metadata'),
            # Prefetch('orderrating_set',OrderRating.objects.all(),to_attr='order_rating'),
            # Prefetch('ordercomment_set',OrderComment.objects.all(),to_attr='order_comment'),
            # Prefetch('orderrating_set',OrderRating.objects.all(),to_attr='order_rating_reviews'),
        )      
        return queryset

    class Meta :
        model = GigDraft
        fields = [
            'id',
            'title',    
            'sub_category',
            'category',        
            'is_approved',
            'approved_by',
            'approved_user',
            'approved_date',
            'is_price_package',
            'description',           
            'gig_status',
            # 'total_views',
            # 'rating',
            'gig_tags',
            'gallery',      
            'faq',
            'is_active',
            'created_by',
            # 'order_rating',
            # 'order_comment',
            'metadata',
            'about_seller',
            'seller_info',
            # 'order_rating_reviews',
            'created_date',       
            # 'last_delivery_date'                 
        ]

    def get_about_seller(self, instance) :
        about_seller = None
        user_info = UserPersonalInfo.objects.filter(user=instance.created_by.id)
        if len(user_info) > 0 :
            about_seller = UserPersonalInfoMinSerializer(user_info[0]).data
        return about_seller  

    def get_seller_info(self, instance) :
        seller_info = None
        user_info = UserAddress.objects.filter(user=instance.created_by.id)
        if len(user_info) > 0 :
            country = Country.objects.filter(
                id=State.objects.filter(
                    id__in=City.objects.filter(
                        id=user_info[0].city_id).values_list('state_id')
                    ).values_list('country_id')
                )
            seller_info = {
                'from' : country.name,                
            }
        return seller_info          

    # def get_last_delivery_date(self, instance) :
    #     last_delivery_date = None        
    #     return last_delivery_date           



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = 'gig-pricing-details/<int:gig_id>')
'''
class GigsPricingDetailsSerializer(serializers.ModelSerializer) :
    price_type = PriceTypeSerializer()
    # price_details = serializers.SerializerMethodField()
    price_details = GigSubCategoryPriceDetailsMinSerializer(many=True)

    def setup_eager_loading(self,queryset) :     
        queryset = queryset.prefetch_related(
            Prefetch('gigsubcategorypricedetails_set',GigSubCategoryPriceDetails.objects.all(),'price_details')
        )  
        return queryset

    class Meta :
        model = GigPrice
        fields = [
            'id',
            'gig',
            'price_type',    
            'price',        
            'price_details'
        ]

    
    # def get_price_details(self, instance) :
    #     price_details_lst = []
    #     scope = []        
    #     psd_lst = []
    #     gig_price_details = GigSubCategoryPriceDetails.objects.filter(gig_price=instance.id,gig=instance.gig)
    #     for gig_price in gig_price_details :           
    #         details = GigSubCategoryPriceDetailsSerializer(gig_price).data  
    #         price_scope = SubCategoryPriceScope.objects.filter(id=details['sub_category_price_scope_id']).only('id','seq_no','price_scope_name')
    #         ps = {'id' : price_scope[0].id,'seq_no' : price_scope[0].seq_no,'price_scope_name' : price_scope[0].price_scope_name}
    #         sub_category_price_scope_dtl = SubCategoryPriceScopeDetails.objects.filter(id=price_scope[0].id)
    #         for dtl in sub_category_price_scope_dtl :
    #             psd = {'id' : dtl.id,'seq_no' : dtl.seq_no,'value' : dtl.value}
    #             psd_lst.append(psd)
    #         ps['sub_category_price_scope_dtl'] = psd_lst
    #         details['sub_category_price_scope'] = ps
    #         price_details_lst.append(details)
    #     return price_details_lst


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = 'gigs/draft-pricing-details/:gig_draft_id>')
'''
class GigsDraftPricingDetailsSerializer(serializers.ModelSerializer) :
    price_type = PriceTypeSerializer()    
    price_details = GigSubCategoryPriceDetailsDraftMinSerializer(many=True)

    def setup_eager_loading(self,queryset) :     
        queryset = queryset.prefetch_related(
            Prefetch('gigsubcategorypricedetailsdraft_set',GigSubCategoryPriceDetailsDraft.objects.all(),'price_details')
        )  
        return queryset

    class Meta :
        model = GigPriceDraft
        fields = [
            'id',
            'gig',
            'gig_draft',
            'price_type',
            'price',            
            'price_details'
        ]
       

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = 'gigs/draft-pricing-details/:gig_draft_id>')
'''
class GigPriceExtraServiceCustomDraftSerializer(serializers.ModelSerializer) :
    extra_days = ExtraDaysSerializer()
    extra_master_price = serializers.SerializerMethodField()
    extra_master_days = serializers.SerializerMethodField()

    def setup_eager_loading(sel,queryset) :
        return queryset

    class Meta :
        model = GigPriceExtraServiceCustomDraft
        fields = [
            'id',           
            'title',
            'description',            
            'extra_price',
            'extra_days',
            'extra_day',
            'extra_master_price',
            'extra_master_days'
        ]
   
    def get_extra_master_price(self,instance) :
        prices = []
        extra_prices = ExtraPrice.objects.all()
        for ext_price in extra_prices :
            extra_price = ExtraPriceSerializer(ext_price).data
            prices.append(extra_price)
        return prices 

    def get_extra_master_days(self,instance) :
        days = []
        extra_days = ExtraDays.objects.all()
        for ext_day in extra_days :
            extra_day = ExtraDaysSerializer(ext_day).data
            days.append(extra_day)
        return days    

'''
Serializer to send/receive data in specific format for POST/GET method
'''
class GigPriceExtraServiceCustomMinSerializer(serializers.ModelSerializer) :

    class Meta :
        model = GigPriceExtraServiceCustom
        fields = [
            'id',           
            'title',
            'description',            
            'extra_price',
            'extra_days',
            'extra_day'            
        ]

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = 'gigs/draft-pricing-details/:gig_draft_id>')
'''
class GigPriceExtraServiceCustomSerializer(serializers.ModelSerializer) :
    extra_days = ExtraDaysSerializer()
    extra_master_price = serializers.SerializerMethodField()
    extra_master_days = serializers.SerializerMethodField()

    def setup_eager_loading(sel,queryset) :
        return queryset

    class Meta :
        model = GigPriceExtraServiceCustom
        fields = [
            'id',           
            'title',
            'description',            
            'extra_price',
            'extra_days',
            'extra_day',
            'extra_master_price',
            'extra_master_days'
        ]
   
    def get_extra_master_price(self,instance) :
        prices = []
        extra_prices = ExtraPrice.objects.all()
        for ext_price in extra_prices :
            extra_price = ExtraPriceSerializer(ext_price).data
            prices.append(extra_price)
        return prices 

    def get_extra_master_days(self,instance) :
        days = []
        extra_days = ExtraDays.objects.all()
        for ext_day in extra_days :
            extra_day = ExtraDaysSerializer(ext_day).data
            days.append(extra_day)
        return days 

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = 'gigs_extra/relevant-searches')
'''
class RelevantSearchesSerializer(serializers.Serializer) :
    relevant_id = serializers.IntegerField()
    relevant_name = serializers.SerializerMethodField()
    relevant_type = serializers.CharField()

   
    def get_relevant_name(self, instance) :       
        return instance.relevant_name

    def validate_relevant_type(self, value) :
        if value not in ('category', 'sub_category', 'gigs_tag') :
            raise ValidationError(_('Invalid relevant type.'),code='invalid_input')
        return value

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = 'gigs_extra/relevant-searches')
'''      
class PopularServicesSerializer(serializers.ModelSerializer) :

    class Meta :
        model = SubCategory
        fields = [
            'id',
            'seq_no',
            'sub_category_name',
            'sub_category_picture',
            'sub_category_thumb_picture',
            'tagline',
            'created_user',
            'created_date'
        ]



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gig-draft/seller-offer-list')
'''
class SellerOfferGigListSerializer(serializers.ModelSerializer) :
    # gigs_pricing = GigPriceSerializer(many=True)
    # gigs_pricing_details = GigSubCategoryPriceDetailsMinSerializer(many=True)
    gigs_pricing = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            # Prefetch('gigprice_set',GigPrice.objects.filter(price_type_id=PRICE_TYPE['Basic']),to_attr='gigs_pricing'),
            # Prefetch('gigsubcategorypricedetails_set',GigSubCategoryPriceDetails.objects.all(),to_attr='gigs_pricing_details')
        )
        return queryset
    class Meta :
        model = Gig
        fields = [
            'id',           
            'title',
            'description',
            'gigs_pricing',
            # 'gigs_pricing_details',
        ]

    def get_gigs_pricing(self,instance) :    
        price = None   
        data = []
        gig_price = GigPrice.objects.filter(gig_id=instance.id,price_type_id=PRICE_TYPE['Basic'])
        for gig in gig_price :
            price = GigPriceSerializer(gig).data
            gig_price_dtl = GigSubCategoryPriceDetails.objects.filter(gig_price=gig,sub_category_price_scope__price_scope_name__in=['Delivery Time','Revisions'])
            for dtl in gig_price_dtl :
                data.append(GigSubCategoryPriceDetailsMinSerializer(dtl).data)
        res = {
            'gig_price' : price,
            'gig_price_dtl' : data
        }        
        return res



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gig-draft/seller-request-offer')
'''
class SellerRequestOfferGigListSerializer(serializers.ModelSerializer) :
    created_by = SellerInfoMinSerializer()
    gallery = GigsGalleryMinSerializer(many=True)   
    gigs_pricing = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset,prefetch_prefix='') :
        queryset = queryset.prefetch_related(
            Prefetch(prefetch_prefix + 'giggallery_set',GigGallery.objects.filter(gig_gallery_type_id=GALLERY_TYPE['Photo']),to_attr='gallery'),
        )
        queryset = SellerInfoMinSerializer.setup_eager_loading(self,queryset,prefetch_prefix + 'created_by__')
        return queryset

    class Meta :
        model = Gig
        fields = [
            'id',           
            'title',
            'description',
            'total_views',
            'rating',
            'rated_no_of_records',
            'gallery',
            'gigs_pricing',
            'created_by',
        ]

    def get_gigs_pricing(self,instance) :    
        price = None   
        data = []        
        request_id = self.parent.instance[0].request_id
        request_offer = RequestOffer.objects.filter(request_id=request_id,gig_id=instance.id)
        gig_price = GigPrice.objects.filter(gig_id=instance.id,price_type_id=request_offer[0].price_type_id)
        for gig in gig_price :
            price = GigPriceSerializer(gig).data
            gig_price_dtl = GigSubCategoryPriceDetails.objects.filter(gig_price=gig)
            for dtl in gig_price_dtl :
                data.append(GigSubCategoryPriceDetailsMinSerializer(dtl).data)
        res = {
            'gig_price' : price,
            'gig_price_dtl' : data
        }        
        return res    


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/gig-draft/seller-request-offer/:id')
'''
class SellerRequestOfferDetailsGigSerializer(serializers.ModelSerializer) :
    created_by = SellerInfoMinSerializer()
    gallery = GigsGalleryMinSerializer(many=True)   
    gigs_pricing = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset,prefetch_prefix='') :
        queryset = queryset.prefetch_related(
            Prefetch(prefetch_prefix + 'giggallery_set',GigGallery.objects.filter(gig_gallery_type_id=GALLERY_TYPE['Photo']),to_attr='gallery'),
        )
        queryset = SellerInfoMinSerializer.setup_eager_loading(self,queryset,prefetch_prefix + 'created_by__')
        return queryset

    class Meta :
        model = Gig
        fields = [
            'id',           
            'title',
            'description',
            'total_views',
            'rating',
            'rated_no_of_records',
            'gallery',
            'gigs_pricing',
            'created_by',
        ]

    def get_gigs_pricing(self,instance) :    
        gigs_pricing = []   
        gig_price_details = []      
        # request_offer_id = self.parent.instance.pk       
        # request_offer = RequestOffer.objects.filter(id=request_offer_id,gig_id=instance.id)
        gig_price = GigPrice.objects.filter(gig_id=instance.id)
        for gig in gig_price :
            price = GigPriceSerializer(gig).data
            gig_price_dtl = GigSubCategoryPriceDetails.objects.filter(gig_price=gig,sub_category_price_scope__price_scope_name__in=['Delivery Time','Max Revisions Allowed'])
            for dtl in gig_price_dtl :
                gig_price_details.append(GigSubCategoryPriceDetailsMinSerializer(dtl).data)
            res = {
                'gig_price' : price,
                'gig_price_dtl' : gig_price_details
            }   
            gigs_pricing.append(res)
            gig_price_details = []
        return gigs_pricing          