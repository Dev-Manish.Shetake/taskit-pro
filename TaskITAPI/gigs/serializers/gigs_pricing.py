from rest_framework import serializers
from django.db import transaction
from configs.values import GIG_STATUS
from django.db.models import Prefetch
from django.utils import timezone
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from gigs.models import GigSubCategoryPriceDetails,GigPrice,GigPriceDraft,GigSubCategoryPriceDetailsDraft
from configs.serializers import PriceTypeSerializer
from masters.serializers import SubCategoryPriceScopeMinSerializer,SubCategoryPriceScopeDetailsSerializer


class GigPriceSerializer(serializers.ModelSerializer) :
    price_type = PriceTypeSerializer()

    class Meta :
        model = GigPrice
        fields =[
            'id',
            'price_type',
            'price'                      
        ]


class GigPriceDraftMinSerializer(serializers.ModelSerializer) :
    price_type = PriceTypeSerializer()

    class Meta :
        model = GigPriceDraft
        fields =[
            'id',
            'price_type'                      
        ]


class GigSubCategoryPriceDetailsSerializer(serializers.ModelSerializer) :
    # gig_price = GigPriceSerializer(many=True)
    # sub_category_price_scope_dtl = SubCategoryPriceScopeDetailsSerializer(many=True)

    class Meta :
        model = GigSubCategoryPriceDetails
        fields =[
            'id',
            'sub_category_price_scope_id',
            'sub_category_price_scope_dtl_id',     
            'value'       
        ]


class GigSubCategoryPriceDetailsMinSerializer(serializers.ModelSerializer) :
    sub_category_price_scope = SubCategoryPriceScopeMinSerializer()
    sub_category_price_scope_dtl = SubCategoryPriceScopeDetailsSerializer()

    class Meta :
        model = GigSubCategoryPriceDetails
        fields =[
            'id',
            'sub_category_price_scope',
            'sub_category_price_scope_dtl',     
            'value'       
        ]


class GigSubCategoryPriceDetailsDraftMinSerializer(serializers.ModelSerializer) :   
    sub_category_price_scope = SubCategoryPriceScopeMinSerializer()
    sub_category_price_scope_dtl = SubCategoryPriceScopeDetailsSerializer()

    class Meta :
        model = GigSubCategoryPriceDetailsDraft
        fields =[
            'id',            
            'sub_category_price_scope',
            'sub_category_price_scope_dtl',     
            'value'       
        ]
