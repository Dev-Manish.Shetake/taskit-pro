from django.urls import include,path
from gigs.views import gig,gigs_ext,gig_details,gig_gallery,gig_faq,gig_requirements,gig_pricing,gig_list,gig_status 

urlpatterns = [
    # Gigs Transactional
    path('draft',gig.GigsListView.as_view()),
    path('draft/<int:pk>',gig.GigsDraftListDetailView.as_view()),
    path('draft-update/<int:pk>',gig.GigsDraftUpdateView.as_view()),
    path('list',gig_list.GigListView.as_view()),
    path('<int:pk>',gig.GigsListDetailView.as_view()),
    path('draft-verify/<int:gig_id>',gig.GigsDraftVerifyView.as_view()),
    path('update/<int:pk>',gig.GigsDetailUpdateView.as_view()),
    # Gigs Gallery     
    path('draft-gallery',gig_gallery.GigsGalleryCreateView.as_view()),
    path('draft-gallery-list/<int:gig_draft_id>',gig_gallery.GigsDraftGalleryListView.as_view()), 
    path('draft-gallery-update/<int:gig_draft_id>',gig_gallery.GigsDraftGalleryUpdateView.as_view()), 
    path('draft-gallery-delete/<int:pk>',gig_gallery.GigsDraftGalleryDeleteView.as_view()), 
    path('gallery-list/<int:gig_id>',gig_gallery.GigsGalleryListView.as_view()),
    path('gallery-delete/<int:pk>',gig_gallery.GigsGalleryDeleteView.as_view()), 
    #Gigs Pricing   
    path('draft-pricing',gig_pricing.GigsPricingCreateView.as_view()),
    path('draft-pricing-details/<int:gig_draft_id>',gigs_ext.GigsDraftPricingDetailView.as_view()),  
    path('draft-pricing-update/<int:gig_draft_id>',gig_pricing.GigsDraftPricingsUpdateView.as_view()),  
    path('pricing-details/<int:gig_id>',gigs_ext.GigsPricingDetailView.as_view()),      
    path('draft-pricing-extra-services',gig_pricing.GigsExtraPricingServicesCreateView.as_view()),
    path('draft-pricing-extra-services/<int:gig_draft_id>',gig_details.GigDraftPriceExtraServiceView.as_view()),
    path('draft-pricing-extra-services-update/<int:gig_draft_id>',gig_pricing.GigsExtraPricingServicesUpdateView.as_view()),
    path('draft-pricing-extra-custom-services/<int:gig_draft_id>',gig_details.GigsDraftExtraPricingCustomServicesView.as_view()),
    path('pricing-extra-services/<int:gig_id>',gig_details.GigPriceExtraServiceView.as_view()),
    path('pricing-extra-custom-services/<int:gig_id>',gig_details.GigsExtraPricingCustomServicesView.as_view()),
    # Gigs FAQ   
    path('draft-faq',gig_faq.GigsFAQCreateView.as_view()),
    path('draft-faq-list/<int:gig_draft_id>',gig_faq.GigsDraftFAQListView.as_view()),
    path('draft-faq-update/<int:gig_draft_id>',gig_faq.GigsFaqUpdateView.as_view()),
    path('faq-list/<int:gig_id>',gig_faq.GigsFAQListView.as_view()),
    # Gigs Requirements
    path('draft-requirements',gig_requirements.GigsRequirementsCreateView.as_view()),
    path('draft-requirements-list/<int:gig_draft_id>',gig_requirements.GigsDraftRequirementsListView.as_view()),
    path('draft-requirements-update/<int:gig_draft_id>',gig_requirements.GigsDraftRequirementsUpdateView.as_view()),
    path('requirements-list/<int:id>',gig_requirements.GigsRequirementsListView.as_view()),
    # Publish
    path('pre-publish-verify/<int:gig_draft_id>',gig.GigsPrePublishVerifyView.as_view()),
    path('<int:gig_draft_id>/publish',gig.GigsPublishView.as_view()),  
    # Gigs Extenders
    path('search-tags',gig.GigSearchTagListView.as_view()),
    # path('search-extender',gig.SearchExtenderView.as_view()),  
    path('search-extender',gig.SearchKeywordExtenderView.as_view()), 
    # Others
    path('last-visited',gig.GigsLastvisitedView.as_view()),
    # Back-Office    
    path('profile-list',gig_list.ProfileGigsListView.as_view()),    
    path('backoffice-list',gig_list.BackOfficeVWGigsDataListView.as_view()),   
    path('backoffice-list-count',gig_list.BackOfficeVWGigsDataListCountView.as_view()), 
    path('approve/<int:id>',gig_status.GigApproveView.as_view()),
    path('modify/<int:id>',gig_status.GigModificationView.as_view()),
    path('reject/<int:id>',gig_status.GigRejectedView.as_view()),
    path('paused/<int:id>',gig_status.GigPausedView.as_view()),
    path('resume/<int:id>',gig_status.GigResumeView.as_view()),
]