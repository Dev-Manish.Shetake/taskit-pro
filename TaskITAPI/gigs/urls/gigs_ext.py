from django.urls import include,path
from gigs.views import gigs_ext,gig_details,gig_update

urlpatterns = [
    # Dashboard    
    path('active-gig-update',gig_update.ActiveGigUpdateView.as_view()),
    path('gig-details/<int:pk>',gigs_ext.GigsDetailView.as_view()),
    path('gig-draft-details/<int:pk>',gigs_ext.GigsDraftDetailView.as_view()),
    path('gig-pricing-details/<int:gig_id>',gigs_ext.GigsPricingDetailView.as_view()),    
    path('gig-draft-pricing-details/<int:gig_draft_id>',gigs_ext.GigsPricingDraftDetailView.as_view()),    
    path('gig-min-details/<int:pk>',gig_details.GigMinDetailView.as_view()),
    path('gig-extra-services/<int:gig_id>',gig_details.GigPriceExtraServiceView.as_view()),
    path('seller-offer-list',gigs_ext.SellerOfferGigListView.as_view()),
]