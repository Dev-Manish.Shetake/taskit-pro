from django.urls import include,path
from gigs.views import gigs_ext


urlpatterns = [
    # Dashboard
    path('search-result',gigs_ext.GigSearchResultListView.as_view()),
    path('active-search-result',gigs_ext.GigActiveSearchResultListView.as_view()),
    path('favourite-list',gigs_ext.GigSearchResultFavouriteListView.as_view()),
    path('best-sellers',gigs_ext.BestSellerListView.as_view()),
    path('browsing-history',gigs_ext.GigBrowsingSearchHistoryListView.as_view()),
    path('top-services',gigs_ext.TopServicesListView.as_view()),
    path('taskit-pics',gigs_ext.TaskITPicsListView.as_view()),   
    path('relevant-searches',gigs_ext.RelevantSearchesView.as_view()),
    path('popular-services',gigs_ext.PopularServicesListView.as_view()),
]