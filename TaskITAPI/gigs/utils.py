from gigs.models import Gig,GigDraft,GigLog
from django.utils import timezone
from accounts.values import USER_TYPE
from rest_framework.exceptions import PermissionDenied

def get_user_data(request, queryset) :
    if request.user.user_type_id == USER_TYPE['back_office'] :
        return queryset   
    elif request.user.user_type_id == USER_TYPE['buyer']:
        queryset = queryset.filter(created_by_id=request.user.id) 
    elif request.user.user_type_id == USER_TYPE['seller']:
        queryset = queryset.filter(created_by_id=request.user.id)        
    else :
        raise PermissionDenied
    return queryset


def create_gig_log(request,gig_draft,gig=None) :
    if gig != None :
        gig_log = GigLog.objects.create(
            gig_draft = gig_draft,           
            gig = gig,
            gig_status = gig.gig_status,
            created_by = request.user,
            created_user = request.user.user_name,
            created_date = timezone.now() 
        )
    else :  
        gig_log = GigLog.objects.create(
            gig_draft = gig_draft,           
            gig = gig_draft.gig,
            gig_status = gig_draft.gig_status,
            created_by = request.user,
            created_user = request.user.user_name,
            created_date = timezone.now() 
        )
    return gig_log


