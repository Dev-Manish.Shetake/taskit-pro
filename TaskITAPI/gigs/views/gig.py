from rest_framework.generics import ListAPIView,CreateAPIView,ListCreateAPIView,UpdateAPIView,RetrieveUpdateAPIView,RetrieveDestroyAPIView,RetrieveAPIView,DestroyAPIView,GenericAPIView
from gigs.models import Gig,GigDraft,GigSearchTag, GigLastVisited,GigSearchKeyword,GigSearchTagDraft,GigFaqDraft,GigRequirementDraft,GigRequirementDetailsDraft,\
    GigPriceDraft,GigSubCategoryPriceDetailsDraft,GigPriceExtraServiceDraft,GigPriceExtraServiceCustomDraft,GigSubCategoryMetaDataTypeDraft,\
        GigSubCategoryMetaDataTypeDraftDetails,GigSubCategoryMetaDataTypeOtherDraft,GigGalleryDraft
from gigs.serializers import GigSearchTagSerializer,SearchExtenderSerializer,SearchKeywordSerializer,GigsDraftSerializer,GigsDraftDetailsUpdateSerializer,GigsDraftUpdateSerializer,GigsSerializer, GigsDraftListSerializer
from django.db.models import Q,F,Value,CharField
from masters.models import Category,SubCategory
from gigs import values
from rest_framework import status
from rest_framework.response import Response 
from TaskITAPI.pagination import StandardResultsSetPagination
from configs.values import GIG_STATUS,ORDER_STATUS
from rest_framework import serializers
from django.db import transaction 
from django.utils import timezone
from rest_framework.exceptions import PermissionDenied
from gigs.utils import create_gig_log
from orders.models import Order
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from notifications.tasks import notify_user
from notifications import events

'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/gigs/search-tags/)
'''
class GigSearchTagListView(ListAPIView) :
    """
    Get Gig Tags List.

    Query parameters for GET method
    ---------------------------------------
    1. search_tags_name = To filter by search_tags_name
    2. gig_title = To filter by gig_title
    3. source = 'Master' or ''

    Note : For DropDown & Extender Do not Pass source parameter.
  
    E.g. http://127.0.0.1:8000/api/v1/gigs/search-tags?search_tags_name=string&gig_title=string   

    """

    serializer_class = GigSearchTagSerializer

    def get_queryset(self):
        q_objects = Q()
        queryset = GigSearchTag.objects.all()  

        search_tags_name = self.request.GET.get('search_tags_name')       
        source = self.request.GET.get('source')
        if search_tags_name :
            q_objects.add(Q(search_tags__icontains=search_tags_name), Q.AND)       
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        if not source :
            queryset = queryset.order_by('search_tags')        
        return queryset[:10]



'''
View to receive data in specific format for GET method
To get Gig Search Extender List 
(endpoint = '/gigs/search-tags/)
'''
class SearchKeywordExtenderView(ListAPIView) :
    """
    Get Gig masters.

    Query Parameter
    ---------------------------------------
    1.  search_keyword  = Name OF Gig (it will search on category/subcategory/gigs_tag)

    E.g. 127.0.0.1:8000/api/v1/gigs/search-extender?search_keyword =LogoDesign
    """
    queryset = GigSearchKeyword.objects.none()
    serializer_class = SearchKeywordSerializer
    permission_classes = []
    authentication_classes = []

    def get_queryset(self) :
        search_keyword  =  self.request.GET.get('search_keyword') 
        queryset = GigSearchKeyword.objects.all()
        if search_keyword :
            queryset = queryset.filter(search_keyword__icontains=search_keyword)
        queryset = queryset.order_by('search_keyword').distinct()
        return queryset


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/gigs/)
'''
class GigsListView(ListCreateAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. title = To filter by Title   
    2. category_id = To filter by Category ID
    3. category = To filter by Category Name
    4. sub_category_id = To filter by Sub Category ID
    5. sub_category = To filter by Sub Category Name
    6. search_tags = Gigs Tags
    6. status_id = To filter by Gig Status 

    E.g. 127.0.0.1:8000/api/v1/gigs/?title=string&source=   

    """
    serializer_class = GigsDraftSerializer 
    pagination_class = StandardResultsSetPagination
   

    def get_serializer_class(self) :
        if self.request.method == 'GET' :
            return GigsDraftListSerializer
        return self.serializer_class


    def apply_filters(self,queryset) :
        q_objects = Q()
        title = self.request.GET.get('title')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')
        sub_category_id = self.request.GET.get('sub_category_id')
        sub_category_name = self.request.GET.get('sub_category_name')
        search_tags = self.request.GET.get('search_tags')
        status_id = self.request.GET.get('status_id')

        if title :
            q_objects.add(Q(title__icontains=title), Q.AND)
        if category_id :
            q_objects.add(Q(category_id=category_id), Q.AND)  
        if category_name :
            q_objects.add(Q(category__category_name__icontains=category_name), Q.AND)       
        if sub_category_id :
            q_objects.add(Q(sub_category_id=sub_category_id), Q.AND)
        if sub_category_name :
            q_objects.add(Q(sub_category__sub_category_name__icontains=sub_category_name), Q.AND) 
        if search_tags :
            q_objects.add(Q(gig_tags__search_tags__icontains=search_tags), Q.AND) 
        if status_id :
            q_objects.add(Q(gig_status_id=status_id), Q.AND)   
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL           
        if is_active == 'false' :    
            queryset = GigDraft.all_objects.filter(is_active=False)
        else : 
            queryset = GigDraft.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to receive data in specific format for GET method
(endpoint = '/gigs/draft/:id')
'''
class GigsDraftListDetailView(RetrieveDestroyAPIView) :
    queryset = GigDraft.objects.all()
    serializer_class = GigsDraftSerializer

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = GigDraft.all_objects.all().filter(is_active=False)
        else : 
            queryset = GigDraft.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset) 
        return queryset

    def delete_gig_draft_search_tags(self,gig_draft) :
        GigSearchTagDraft.objects.filter(
            gig_draft=gig_draft
            ).delete

    def delete_gig_draft_gallery(self,gig_draft) :
        GigGalleryDraft.objects.filter(
            gig_draft=gig_draft
            ).delete        

    def delete_gig_draft_price(self,gig_draft) :
        gig_price_draft = GigPriceDraft.objects.filter(gig_draft=gig_draft)
        for price_draft in gig_price_draft :
            GigSubCategoryPriceDetailsDraft.objects.filter(
                gig_price_draft=price_draft
                ).delete
        gig_price_draft.delete        
        GigPriceExtraServiceDraft.objects.filter(
            gig_draft=gig_draft
        ).delete
        GigPriceExtraServiceCustomDraft.objects.filter(
            gig_draft=gig_draft
        ).delete

    def delete_gig_draft_faq(self,gig_draft) :
        GigFaqDraft.objects.filter(
            gig_draft=gig_draft
        ).delete

    def delete_gig_draft_req(self,gig_draft) :
        gig_draft_req = GigRequirementDraft.objects.filter(
            gig_draft=gig_draft
        )
        for req_draft in gig_draft_req :
            GigRequirementDetailsDraft.objects.filter(
                gig_requirement_draft=req_draft
            ).delete
        gig_draft_req.delete

    @transaction.atomic
    def delete(self,request,*args,**kwargs) :
        gig_draft = self.get_object()
        gig = gig_draft.gig
        gig_log = create_gig_log(self.request,gig_draft,gig)
        self.delete_gig_draft_search_tags(gig_draft)
        self.delete_gig_draft_gallery(gig_draft)
        self.delete_gig_draft_price(gig_draft)
        self.delete_gig_draft_faq(gig_draft)
        self.delete_gig_draft_req(gig_draft)
        gig_draft.is_active = False
        gig_draft.deleted_by = request.user
        gig_draft.deleted_user = request.user.user_name
        gig_draft.deleted_date = timezone.now()
        gig_draft.save()
        return Response(status=status.HTTP_204_NO_CONTENT)    
      

'''
View to receive data in specific format for GET method
(endpoint = '/gigs/draft-update/:id')
'''
class GigsDraftUpdateView(UpdateAPIView) :

    serializer_class = GigsDraftDetailsUpdateSerializer
    queryset = GigDraft.objects.all()
    

'''
View to receive data in specific format for GET method
(endpoint = '/gigs/draft/:id')
'''
class GigsListDetailView(RetrieveDestroyAPIView) :
    queryset = Gig.objects.all()
    serializer_class = GigsSerializer

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = Gig.all_objects.all().filter(is_active=False)
        else : 
            queryset = Gig.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


    def destroy(self,request,*args,**kwargs) :
        gig = self.get_object()
        if Order.objects.filter(order_status_id__gt=ORDER_STATUS['Draft']).exists() :       
            raise ValidationError(_('Gig involved in active orders.'),code='gig_cannot_delete')            
        gig.is_active = False
        gig.deleted_by = request.user
        gig.deleted_user = request.user.user_name
        gig.deleted_date = timezone.now()
        gig.save()
        return Response(status=status.HTTP_204_NO_CONTENT)           
    


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/gigs/draft-verify/:gig_id')
'''
class GigsDraftVerifyView(GenericAPIView) :
    queryset = GigDraft.objects.all()

    def get(self,request,*args,**kwargs) :
        gig_id = self.kwargs['gig_id']
        gig_draft = GigDraft.objects.filter(gig_id=gig_id,gig_status_id=GIG_STATUS['Draft'])
        if len(gig_draft) :
            data = {
                'gig_draft_id' : gig_draft[0].id,
                'code' : 'gig_draft_exist',
                'message' : 'Gig draft id already exist against this gig.'
            }
        else :
            data = {
                'gig_draft_id' : None,
                'code' : 'gig_draft_not_exist',
                'message' : 'Gig draft id not exist against this gig.'
            }  
        return Response(data,status=status.HTTP_200_OK)
   

'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/gigs/update:id')
'''
class GigsDetailUpdateView(CreateAPIView) :
    queryset = Gig.objects.all()
    serializer_class = GigsDraftUpdateSerializer


'''
View to receive data in specific format for GET method
(endpoint = '/gigs/:id/publish')
'''
class GigsPublishView(GenericAPIView) :
    """
    Gigs Publish 

    """
    class GigsPublishSerializer(serializers.Serializer) :
        pass

    # serializer_class = GigsPublishSerializer
    queryset = GigDraft.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'gig_draft_id'  
    
    @transaction.atomic
    def patch(self, request, gig_draft_id) :            
        gig_draft = self.get_object()  
        if gig_draft.gig_status_id not in (GIG_STATUS['Draft'],GIG_STATUS['Requires_Modification'],GIG_STATUS['Denied']) :
            raise PermissionDenied
        gig_draft.gig_status_id = GIG_STATUS['Pending_for_Approval']
        gig_draft.modified_by = self.request.user
        gig_draft.modified_user = self.request.user.user_name
        gig_draft.modified_date = timezone.now()
        gig_draft.save()        
        return Response(status=status.HTTP_200_OK)



'''
View to receive data in specific format for GET method
(endpoint = '/gigs/pre-publish/:id')
'''
class GigsPrePublishVerifyView(GenericAPIView) :
    """
    Gigs Pre-Publish Verify 

    """
    queryset = GigDraft.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'gig_draft_id' 

    @transaction.atomic
    def get(self, request, *args,**kwargs) :            
        gig_draft = self.get_object()  
        error = []
        if not GigSearchTagDraft.objects.filter(gig_draft=gig_draft).exists() :
            error.append(
                {'code':'search_tags_not_added','message':'Search tags not added.'}
            )
        if not GigGalleryDraft.objects.filter(gig_draft=gig_draft).exists() :
            error.append(
                {'code':'gallery_not_added','message':'Gallery not added.'}
            )    
        if not GigPriceDraft.objects.filter(gig_draft=gig_draft).exists() :
            error.append(
                {'code':'gig_price_not_added','message':'Gig Price not added.'}
            )        
        if not GigFaqDraft.objects.filter(gig_draft=gig_draft).exists() :
            error.append(
                {'code':'faq_not_added','message':'Gig FAQ not added.'}
            )                            
        return Response(error,status=status.HTTP_200_OK)


# Gigs Publish View Class
class GigsLastvisitedView(GenericAPIView) :
    """
    Gigs Last Visited 

    """
    class GigsLastVisitedSerializer(serializers.Serializer) :
        gig_id = serializers.IntegerField()

        def validate_gig_id(self,value) :
            if not Gig.objects.filter(id=value).exists() :
                raise ValidationError(_('Invalid input recieved for gig id.'),code='invalid_input')
            return value

    serializer_class = GigsLastVisitedSerializer
    
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        if 'gig_id' in validated_data :
            gig_id = validated_data.pop('gig_id')
        else :
            raise ValidationError(_('Gig id required.'), code='gig_id_required')                
        gig = Gig.objects.get(id=gig_id)
        if gig.total_views :
            gig.total_views =  gig.total_views + 1
        else :
            gig.total_views =  1 
        gig.modified_by = self.request.user
        gig.modified_user = self.request.user.user_name
        gig.modified_user = timezone.now()     
        gig.save()
        last_visited = GigLastVisited.objects.create(
                user = request.user,
                gig = gig,                
        )
        return Response(status=status.HTTP_200_OK)
