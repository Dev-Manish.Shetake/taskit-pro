from rest_framework.generics import ListAPIView,GenericAPIView,RetrieveAPIView
from rest_framework import serializers
from gigs.models import Gig,GigDraft,GigPriceExtraService,GigPriceExtraServiceDraft,GigPriceExtraServiceCustomDraft,GigPriceExtraServiceCustom
from gigs.serializers import GigsSearchResultSerializer,GigMinDetailsSerializer,GigPriceExtraServiceSerializer,GigDraftPriceExtraServiceSerializer,GigPriceExtraServiceCustomDraftSerializer,\
    GigPriceExtraServiceCustomSerializer
from rest_framework import status
from rest_framework.response import Response
from django.db import transaction 


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/gig-min-details/<int:pk>')
'''
class GigMinDetailView(RetrieveAPIView) :
    """
    
    Query parameters for GET method
    =====================================
    1. price_type_id = ID of Gig price type

    Note : price_type_id keyparamter must be pass
    """
    queryset = Gig.objects.all()
    serializer_class = GigMinDetailsSerializer
    permission_classes = []
    authentication_classes = []

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active')
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = Gig.all_objects.all().filter(is_active=False)
        else : 
            queryset = Gig.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/gig-min-details/<int:pk>')
'''
class GigPriceExtraServiceView(ListAPIView) :
    queryset = GigPriceExtraService.objects.all()
    serializer_class = GigPriceExtraServiceSerializer
    authentication_classes = []
    permission_classes = []


    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active')
        gig_id = self.kwargs['gig_id']
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = GigPriceExtraService.all_objects.all().filter(is_active=False)
        else : 
            queryset = GigPriceExtraService.objects.filter(gig_id=gig_id)
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        print(len(queryset))        
        return queryset

'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = 'gigs/extra-services-suctom/:gig_id')
'''
class GigsExtraPricingCustomServicesView(ListAPIView) :
    queryset = GigPriceExtraServiceCustom.objects.all()
    serializer_class = GigPriceExtraServiceCustomDraftSerializer
    authentication_classes = []
    permission_classes = []


    def get_queryset(self) :       
        gig_id = self.kwargs['gig_id']
        queryset = GigPriceExtraServiceCustom.objects.filter(gig_id=gig_id)
        # queryset = self.get_serializer_class().setup_eager_loading(self,queryset)            
        return queryset  

'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = 'gigs/draft-extra-services/:gig_draft_id')
'''
class GigDraftPriceExtraServiceView(ListAPIView) :
    queryset = GigPriceExtraServiceDraft.objects.all()
    serializer_class = GigDraftPriceExtraServiceSerializer

    def get_queryset(self) :       
        gig_draft_id = self.kwargs['gig_draft_id']
        queryset = GigPriceExtraServiceDraft.objects.filter(gig_draft_id=gig_draft_id)
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)            
        return queryset

     
'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = 'gigs/draft-extra-services/:gig_draft_id')
'''
class GigsDraftExtraPricingCustomServicesView(ListAPIView) :
    queryset = GigPriceExtraServiceCustomDraft.objects.all()
    serializer_class = GigPriceExtraServiceCustomDraftSerializer

    def get_queryset(self) :       
        gig_draft_id = self.kwargs['gig_draft_id']
        queryset = GigPriceExtraServiceCustomDraft.objects.filter(gig_draft_id=gig_draft_id)
        # queryset = self.get_serializer_class().setup_eager_loading(self,queryset)            
        return queryset   