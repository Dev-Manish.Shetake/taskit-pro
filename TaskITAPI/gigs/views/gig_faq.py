from rest_framework.generics import GenericAPIView,CreateAPIView,ListAPIView,ListCreateAPIView,RetrieveUpdateAPIView,RetrieveDestroyAPIView
from gigs.models import Gig,GigDraft,GigFaq,GigFaqDraft
from rest_framework import serializers
from gigs import values
from rest_framework import status
from rest_framework.response import Response 
from TaskITAPI.pagination import StandardResultsSetPagination
from rest_framework.exceptions import NotFound, ValidationError


class GigsDraftFAQSerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self, queryset) :
        queryset = queryset.order_by('seq_no')
        return queryset

    class Meta :
        model = GigFaqDraft
        fields = [
            'id',
            # 'gig_draft_id',
            'gig_id',
            'seq_no',
            'question',
            'answer'
        ]       
        read_only_fields = ['gig_id']


class GigsDraftFAQUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField(required=False)

    class Meta :
        model = GigFaqDraft
        fields = [
            'id',
            'seq_no',
            'question',
            'answer'
        ]  


class GigsFAQSerializer(serializers.ModelSerializer) :
    class Meta :
        model = GigFaq
        fields = [
            'id',           
            'gig_id',
            'seq_no',
            'question',
            'answer'
        ]

    def setup_eager_loading(self, queryset) :
        queryset = queryset.order_by('seq_no')
        return queryset


'''
View to send data in specific format for POST method
(endpoint = '/gigs/faq/)
'''
class GigsFAQCreateView(CreateAPIView) :
    """
    Gig FAQ POST API .

    """      
    class GigsFAQCreateSerializer(serializers.Serializer) :
        gig_draft_id =  serializers.IntegerField()
        faq = GigsDraftFAQSerializer(many=True)

    serializer_class = GigsFAQCreateSerializer
    
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        if 'gig_draft_id' in validated_data :
            gig_draft_id = validated_data.pop('gig_draft_id')
        else :
            raise ValidationError(_('Gig Draft id fuired.'), code='gig_draft_id_fuired') 
               
        try :              
            gig_draft = GigDraft.objects.get(id=gig_draft_id)
        except :
            raise NotFound
       
        faq = None
        if 'faq' in validated_data :
            faq = validated_data.pop('faq')
        for fa in faq :                
            gig_faq = GigFaqDraft(
                gig_draft=gig_draft,               
                **fa
            )    
            gig_faq.save()    
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for GET method
(endpoint = '/gigs/draft-faq/)
'''
class GigsDraftFAQListView(ListAPIView) :
    """
    Gig FAQ GET API .
   
    """    
    queryset = GigFaqDraft.objects.all()
    serializer_class = GigsDraftFAQSerializer

    # Override this function to build custom queryset
    def get_queryset(self) :
        gig_draft_id = self.kwargs['gig_draft_id']
        queryset = GigFaqDraft.objects.filter(gig_draft_id=gig_draft_id)            
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 



'''
View to send data in specific format for POST method
(endpoint = '/gigs/faq-update/:gig_draft_id)
'''
class GigsFaqUpdateView(GenericAPIView) :
    """
    Gig Draft FAQ PATCH API .

    """      
    class GigsDraftFaqUpdateSerializer(serializers.Serializer) :      
        faq = GigsDraftFAQUpdateSerializer(many=True)

    serializer_class = GigsDraftFaqUpdateSerializer

    def patch(self,request,gig_draft_id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        gig_draft_id = self.kwargs['gig_draft_id']
        if 'faq' in validated_data :
                faq = validated_data.pop('faq')  
        gig_faq_draft = GigFaqDraft.objects.filter(gig_draft_id=gig_draft_id)   
        for f in faq :  
            if f['id'] != 0 :
                GigFaqDraft.objects.filter(id=f['id']).update(
                    seq_no = f['seq_no'],
                    question = f['question'],
                    answer = f['answer']
                )
            else :
                GigFaqDraft.objects.create(
                    gig_draft_id = gig_draft_id,
                    seq_no = f['seq_no'],
                    question = f['question'],
                    answer = f['answer']
                )       
        return Response(status=status.HTTP_200_OK)   


'''
View to send data in specific format for GET method
(endpoint = '/gigs/faq/)
'''
class GigsFAQListView(ListAPIView) :
    """
    Gig FAQ GET API .
   
    """    
    queryset = GigFaq.objects.all()
    serializer_class = GigsFAQSerializer

    # Override this function to build custom queryset
    def get_queryset(self) :
        gig_id = self.kwargs['gig_id']
        queryset = GigFaq.objects.filter(gig_id=gig_id)            
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset         