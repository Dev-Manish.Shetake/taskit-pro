from rest_framework.generics import GenericAPIView,ListAPIView,CreateAPIView,DestroyAPIView
from gigs.models import Gig, GigDraft, GigGalleryDraft,GigGallery
from gigs.serializers import GigsSerializer
from rest_framework import serializers
from django.db import transaction
from rest_framework.exceptions import NotFound
from rest_framework import status
from rest_framework.response import Response
from configs.serializers import GigGalleryTypeSerializer
from TaskITAPI import settings
from django.utils import timezone

class GigsDraftGallerySerializer(serializers.ModelSerializer) :
    gig_gallery_type_id = serializers.IntegerField(write_only=True)
    gig_gallery_type = GigGalleryTypeSerializer(read_only=True)

    class Meta :
        model = GigGalleryDraft
        fields = [
            'id',            
            'gig_gallery_type_id',
            'gig_gallery_type',
            'file_path',
            'file_path_thumbnail'
        ]      

    def setup_eager_loading(self, queryset) :
        return queryset


class ActiveGigGallerySerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    gig_gallery_type_id = serializers.IntegerField(write_only=True,required=False,allow_null=True)
    file_path = serializers.FileField(required=False,allow_null=True)
    file_path_thumbnail = serializers.FileField(required=False,allow_null=True)

    class Meta :
        model = GigGalleryDraft
        fields = [
            'id',            
            'gig_gallery_type_id',
            'file_path',
            'file_path_thumbnail'
        ] 


class GigsDraftGalleryUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    gig_gallery_type_id = serializers.IntegerField()

    class Meta :
        model = GigGalleryDraft
        fields = [
            'id',           
            'gig_gallery_type_id',            
            'file_path',
            'file_path_thumbnail'
        ]


class GigsGallerySerializer(serializers.ModelSerializer) :
    gig_gallery_type = GigGalleryTypeSerializer()

    def setup_eager_loading(self, queryset) :
        return queryset

    class Meta :
        model = GigGallery
        fields = [
            'id',           
            'gig_gallery_type',
            'file_path',
            'file_path_thumbnail'
        ]        




'''
View to send data in specific format for POST method
(endpoint = '/gigs/gig-gallery/)
'''
class GigsGalleryCreateView(CreateAPIView) :
    """
    Gig Gallery POST API .
   
    """  
    class GigsGalleryCreateSerializer(serializers.Serializer) :
        gig_draft_id =  serializers.IntegerField()
        gallery = GigsDraftGallerySerializer(many=True)

    serializer_class = GigsGalleryCreateSerializer


    @transaction.atomic
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        if 'gig_draft_id' in validated_data :
            gig_draft_id = validated_data.pop('gig_draft_id')
       
        try :              
            gig_draft = GigDraft.objects.get(id=gig_draft_id)
        except :
            raise NotFound

        gallery = None
        if 'gallery' in validated_data :
            gallery = validated_data.pop('gallery')

        for gal in gallery :
            gig_gallery_draft = GigGalleryDraft(
                gig_draft=gig_draft,                
                **gal
            )
            gig_gallery_draft.save()      
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for GET method
(endpoint = '/gigs/draft-gallery/:gig_draft_id')
'''
class GigsDraftGalleryListView(ListAPIView) :
    """
    Gig Gallery GET API .
   
    """    
    queryset = GigGalleryDraft.objects.all()
    serializer_class = GigsDraftGallerySerializer

    # Override this function to build custom queryset
    def get_queryset(self) :
        gig_draft_id = self.kwargs['gig_draft_id']
        queryset = GigGalleryDraft.objects.filter(gig_draft_id=gig_draft_id)
        # queryset = self.apply_filters(queryset)       
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 


'''
View to send data in specific format for POST method
(endpoint = '/gigs/draft-gallery-update/:gig_draft_id)
'''
class GigsDraftGalleryUpdateView(GenericAPIView) :
    """
    Gig Draft Gallery PATCH API .

    """      
    class GigsGalleryDraftUpdateSerializer(serializers.Serializer) :      
        gallery = GigsDraftGalleryUpdateSerializer(many=True)

    serializer_class = GigsGalleryDraftUpdateSerializer


    def patch(self,request,gig_draft_id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        gig_draft_id = self.kwargs['gig_draft_id']
        if 'gallery' in validated_data :
                gallery = validated_data.pop('gallery')  
        # gig_gallery_draft = GigGalleryDraft.objects.filter(gig_draft_id=gig_draft_id)        
        for gal in gallery : 
            if gal['id'] == 0 :
                GigGalleryDraft.objects.create(
                    gig_draft_id = gig_draft_id,
                    gig_gallery_type_id = gal['gig_gallery_type_id'],
                    file_path = gal['file_path'],
                    file_path_thumbnail = gal['file_path_thumbnail']                    
                )          
        return Response(status=status.HTTP_200_OK)


'''
View to send data in specific format for GET method
(endpoint = '/gigs/gig-draft-gallery-delete/:id)
'''
class GigsDraftGalleryDeleteView(DestroyAPIView) :
    queryset = GigGalleryDraft.objects.all()

    def delete(self,request,*args,**kwargs) :
        gig_draft_gallery = self.get_object()
        gig_draft_gallery.is_active=False     
        gig_draft_gallery.save()  
        return Response(status=status.HTTP_204_NO_CONTENT)    


'''
View to send data in specific format for GET method
(endpoint = '/gigs/gig-gallery/)
'''
class GigsGalleryListView(ListAPIView) :
    """
    Gig Gallery GET API .

    """    
    queryset = GigGallery.objects.all()
    serializer_class = GigsGallerySerializer

    # Override this function to build custom queryset
    def get_queryset(self) :
        gig_id = self.kwargs['gig_id']
        queryset = GigGallery.objects.filter(gig_id=gig_id)          
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 


'''
View to send data in specific format for GET method
(endpoint = '/gigs/gig-gallery-delete/:id)
'''
class GigsGalleryDeleteView(DestroyAPIView) :
    queryset = GigGallery.objects.all()

    def delete(self,request,*args,**kwargs) :
        gig_gallery = self.get_object()
        gig_gallery.is_active=False    
        gig_gallery.save()   
        return Response(status=status.HTTP_204_NO_CONTENT) 