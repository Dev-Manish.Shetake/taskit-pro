from rest_framework.generics import ListAPIView,ListCreateAPIView,RetrieveUpdateAPIView,RetrieveDestroyAPIView,GenericAPIView
from gigs.models import Gig,GigDraft,GigSearchTag, GigLastVisited,VWGigsData
from gigs.serializers import GigsSearchResultSerializer,GigSearchTagSerializer,SearchExtenderSerializer, GigListSerializer,BackOfficeGigsListSerializer,BackOfficeDraftGigsListSerializer,BackOfficeVWGigsDataListSerializer,\
    BackOfficeVWGigsDataListCountSerializer
from django.db.models import Q,F,Value,CharField
from masters.models import Category,SubCategory
from accounts.models import User
from gigs import values
from rest_framework import status
from rest_framework.response import Response 
from TaskITAPI.pagination import StandardResultsSetPagination
from configs.values import GIG_STATUS
from rest_framework import serializers
from django.db import transaction 
from django.utils import timezone
from datetime import timedelta
from TaskITAPI.pagination import get_paginated_response
from gigs.utils import get_user_data
from django.db import connection 

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/gigs/list)
'''
class GigListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. status_id = To filter by Gig Status 
    2. title = Filter by Title
    3. duration = Filter by duration (Last 7 Days= 1 | Last 14 Days= 2 | Last 30 Days= 3 | Last 2 Month= 4)

    E.g. http://127.0.0.1:8000/api/v1/gigs/list?title=logo&status_id=1&duration=1   

    """
    serializer_class = GigListSerializer
    pagination_class = StandardResultsSetPagination
 

    def apply_filters(self,queryset) :
        q_objects = Q()
        title = self.request.GET.get('title')       
        status_id = self.request.GET.get('status_id')
        duration = self.request.GET.get('duration')

        if title :
            q_objects.add(Q(title__icontains=title), Q.AND)        
        if status_id :
            q_objects.add(Q(gig_status_id=status_id), Q.AND)   
        if duration :
            if duration == '1' :
                end_date = timezone.now() 
                start_date = end_date - timedelta(days=7)
                queryset = queryset.filter(created_date__range=[start_date,end_date])
            elif duration == '2' :
                end_date = timezone.now() 
                start_date = end_date - timedelta(days=14)
                queryset = queryset.filter(created_date__range=[start_date,end_date])
            elif duration == '3' :
                end_date = timezone.now() 
                start_date = end_date - timedelta(days=30)
                queryset = queryset.filter(created_date__range=[start_date,end_date])
            elif duration == '4' :
                end_date = timezone.now() 
                start_date = end_date - timedelta(days=60)
                queryset = queryset.filter(created_date__range=[start_date,end_date])    
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL        
        if is_active == 'false' :    
            queryset = Gig.all_objects.filter(is_active=False)
        else : 
            queryset = Gig.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/gigs/profile-list')
'''
class ProfileGigsListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------

    1. status_id = To filter by Gig Status 

    'Draft' : 1
    'Pending_for_Approval' : 2
    'Requires_Modification' : 3
    'Denied' : 4
    'Active' : 5
    'Paused' : 6

    E.g. http://127.0.0.1:8000/api/v1/gigs/profile-list?status_id=5

    """
    serializer_class = GigsSearchResultSerializer
    pagination_class = StandardResultsSetPagination
   
    def apply_filters(self,queryset) :
        q_objects = Q()       
        status_id = self.request.GET.get('status_id')              
                  
        if status_id :
            q_objects.add(Q(gig_status_id=status_id), Q.AND)            
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active')        
        if is_active == 'false' :    
            queryset = Gig.all_objects.filter(is_active=False)
        else : 
            queryset = Gig.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = get_user_data(self.request,queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/gigs/backoffice-list')
'''
class BackOfficeVWGigsDataListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------    

    1. gig = To filter by Gig Title
    2. category_id = To filter by Category ID
    3. seller = To filter by Seller Name
    4. status_id = To filter by Gig Status 

    E.g. http://127.0.0.1:8000/api/v1/gigs/profile-list?gig=&category_id=&seller=&status_id=   


    """
    serializer_class = BackOfficeVWGigsDataListSerializer
    pagination_class = StandardResultsSetPagination
 

    def apply_filters(self,queryset) :
        q_objects = Q()
        gig = self.request.GET.get('gig')  
        category_id = self.request.GET.get('category_id')  
        category_name = self.request.GET.get('category_name')  
        seller = self.request.GET.get('seller')       
        status_id = self.request.GET.get('status_id')      
        
        if gig :
            q_objects.add(Q(title__icontains=gig), Q.AND)     
        if category_id :
            q_objects.add(Q(category_id=category_id), Q.AND) 
        if category_name :
            q_objects.add(Q(category__category_name__icontains=category_name), Q.AND)          
        if seller :
            q_objects.add(Q(created_user__icontains=seller), Q.AND)                   
        if status_id :
            q_objects.add(Q(gig_status_id=status_id), Q.AND)            
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL        
        if is_active == 'false' :    
            queryset = VWGigsData.all_objects.filter(is_active=False)
        else : 
            queryset = VWGigsData.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = get_user_data(self.request,queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset
        


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/gigs/backoffice-list-count')
'''
class BackOfficeVWGigsDataListCountView(ListAPIView) :
    """
    Get Seller Dashboard Count status stage wise(dashboard + menu)
    
    Query parameters for GET method
    ---------------------------------------    
  
    E.g  http://127.0.0.1:8000/api/v1/gigs/backoffice-list-count
    """

    serializer_class = BackOfficeVWGigsDataListCountSerializer
    queryset = User.objects.none()
    result_set = []

    def get(self, request, *args, **kwargs) :       
        data = None    
        with connection.cursor() as cursor:
            cursor.callproc('sp_get_seller_gig_tab_count', [request.user.id])  
            from TaskITAPI.serializers import StoredProcedureSerializer         
            data = StoredProcedureSerializer(cursor, self.result_set).data
            if type(data) == dict and not data :
               data = []             
        return Response(data, status=status.HTTP_200_OK)  