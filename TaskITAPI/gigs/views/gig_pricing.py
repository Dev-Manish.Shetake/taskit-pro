from rest_framework.generics import GenericAPIView,ListAPIView,CreateAPIView
from gigs.models import Gig, GigDraft, GigPriceDraft,GigSubCategoryPriceDetailsDraft,GigPriceExtraServiceDraft,GigPriceExtraServiceCustomDraft
from gigs.serializers import GigsSerializer
from rest_framework import serializers
from django.db import transaction
from rest_framework.exceptions import NotFound
from rest_framework import status
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ValidationError



class GigSubCategoryPriceDetailsDraftSerializer(serializers.ModelSerializer) :
    # price_type_id = serializers.IntegerField()
    sub_category_price_scope_id = serializers.IntegerField()
    sub_category_price_scope_dtl_id = serializers.IntegerField(allow_null=True)
    value = serializers.CharField(allow_blank=True)
    
    class Meta :
        model = GigSubCategoryPriceDetailsDraft
        fields = [
            'id',
            # 'price_type_id',
            'sub_category_price_scope_id',
            'sub_category_price_scope_dtl_id',
            'value'            
        ]
       

class GigSubCategoryPriceDetailsDraftUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    sub_category_price_scope_id = serializers.IntegerField()
    sub_category_price_scope_dtl_id = serializers.IntegerField(allow_null=True)
    value = serializers.CharField(allow_blank=True)

    class Meta :
        model = GigSubCategoryPriceDetailsDraft
        fields = [
            'id',            
            'sub_category_price_scope_id',
            'sub_category_price_scope_dtl_id',
            'value'            
        ]


class GigPriceDraftSerializer(serializers.ModelSerializer) :
    price_type_id = serializers.IntegerField()
    price = serializers.DecimalField(max_digits=9,decimal_places=2,allow_null=True)
    price_type_details = GigSubCategoryPriceDetailsDraftSerializer(many=True,required=False)
    
    class Meta :
        model = GigPriceDraft
        fields = [
            'id',            
            'price_type_id',
            'price',
            'price_type_details'            
        ]


class GigPriceDraftUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    price_type_id = serializers.IntegerField()
    price = serializers.DecimalField(max_digits=9,decimal_places=2,required=False)
    price_type_details = GigSubCategoryPriceDetailsDraftUpdateSerializer(many=True)

    class Meta :
        model = GigPriceDraft
        fields = [
            'id',            
            'price_type_id',
            'price',
            'price_type_details'            
        ]

'''
View to send data in specific format for POST method
(endpoint = '/gigs/pricing/)
'''
class GigsPricingCreateView(CreateAPIView) :
    """
    Gig Pricing POST API .
   
    """  
    class GigsPricingCreateSerializer(serializers.Serializer) :
        gig_draft_id =  serializers.IntegerField()
        is_price_package = serializers.BooleanField()
        gigs_price = GigPriceDraftSerializer(many=True)

    serializer_class = GigsPricingCreateSerializer

    @transaction.atomic
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        if 'gig_draft_id' in validated_data :
            gig_draft_id = validated_data.pop('gig_draft_id')
               
        try :              
            gig_draft = GigDraft.objects.get(id=gig_draft_id)
        except :
            raise NotFound       
        
        gig_draft.is_price_package = validated_data['is_price_package']
        gig_draft.save()

        gigs_price = []
        price_type_details_list = []
        if 'gigs_price' in validated_data :
            gigs_price = validated_data.pop('gigs_price')
        if not gigs_price or len(gigs_price) < 0 :
            raise ValidationError(_('Gigs price details required'),code='gigs_price_details_required')
        for price in gigs_price :
            price_type_details_list = price.pop('price_type_details')
            gig_price_draft = GigPriceDraft(
                gig_draft = gig_draft,                
                price_type_id = price['price_type_id'],
                price = price['price']
            )
            gig_price_draft.save()
            for price_dtl in price_type_details_list :
                gig_price_details = GigSubCategoryPriceDetailsDraft(
                    gig_draft = gig_draft,                    
                    gig_price_draft = gig_price_draft,
                    **price_dtl
                )
                gig_price_details.save() 
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for POST method
(endpoint = '/gigs/pricing-update/:gig_draft_id)
'''
class GigsDraftPricingsUpdateView(GenericAPIView) :
    """
    Gig Draft Requirements PATCH API .

    """      
    class GigsDraftPricingUpdateSerializer(serializers.Serializer) :      
        pricings = GigPriceDraftUpdateSerializer(many=True)

    serializer_class = GigsDraftPricingUpdateSerializer

    def patch(self,request,gig_draft_id) : 
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        gig_draft_id = self.kwargs['gig_draft_id']
        if 'pricings' in validated_data :
                pricings = validated_data.pop('pricings')  
        for price in pricings :            
            if 'price_type_details' in price :
                price_type_details = price.pop('price_type_details')
            if price['id'] != 0 :
                gig_price_draft = GigPriceDraft.objects.filter(id=price['id']).update(
                    price_type_id = price['price_type_id'],
                    price = price['price']
                )    
            else :
                gig_price_draft = GigPriceDraft.objects.create(
                    gig_draft_id=gig_draft_id,
                    price_type_id = price['price_type_id'],
                    price = price['price']
                )                 
            for price_type in price_type_details :              
                if price_type['id'] != 0 :
                    GigSubCategoryPriceDetailsDraft.objects.filter(id=price_type['id']).update(
                        sub_category_price_scope_id = price_type['sub_category_price_scope_id'],
                        sub_category_price_scope_dtl_id = price_type['sub_category_price_scope_dtl_id'],
                        value = price_type['value']          
                    )
                else :     
                    GigSubCategoryPriceDetailsDraft.objects.create(
                        gig_draft_id=gig_draft_id,
                        gig_price_draft=gig_price_draft,
                        sub_category_price_scope_id = price_type['sub_category_price_scope_id'],
                        sub_category_price_scope_dtl_id = price_type['sub_category_price_scope_dtl_id'],
                        value = price_type['value']          
                    )                  
        return Response(status=status.HTTP_200_OK)   


class GigPriceExtraServiceCustomDraftSerializer(serializers.ModelSerializer) :
    
    class Meta :
        model = GigPriceExtraServiceCustomDraft
        fields = [
            'id',
            'title',
            'description',
            'extra_price',
            'extra_days', 
            'extra_day'                    
        ]


class GigPriceExtraServiceCustomDraftUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField() 

    class Meta :
        model = GigPriceExtraServiceCustomDraft
        fields = [
            'id',
            'title',
            'description',
            'extra_price',
            'extra_days', 
            'extra_day'                    
        ]


class GigPriceExtraServiceDraftSerializer(serializers.ModelSerializer) :   
    price_extra_service_id = serializers.IntegerField()
    price_type_id = serializers.IntegerField()
    extra_days_id = serializers.IntegerField()
   
    class Meta :
        model = GigPriceExtraServiceDraft
        fields = [
            'id',
            'price_extra_service_id',
            'price_type_id',
            'extra_price',
            'extra_days_id',
            'extra_day'            
        ]


class GigPriceExtraServiceDraftUpdateSerializer(serializers.ModelSerializer) :  
    id = serializers.IntegerField() 
    price_extra_service_id = serializers.IntegerField()

    class Meta :
        model = GigPriceExtraServiceDraft
        fields = [
            'id',
            'price_extra_service_id',
            'price_type',
            'extra_price',
            'extra_days',
            'extra_day',
        ]


'''
View to send data in specific format for POST method
(endpoint = '/gigs/pricing/)
'''
class GigsExtraPricingServicesCreateView(CreateAPIView) :
    """
    Gig Extra Pricing Services POST API .
   
    """  
    class GigsExtraPricingServicesCreateSerializer(serializers.Serializer) :
        gig_draft_id =  serializers.IntegerField()
        gigs_extra_price = GigPriceExtraServiceDraftSerializer(many=True)
        extra_service_custom = GigPriceExtraServiceCustomDraftSerializer(many=True)   

    serializer_class = GigsExtraPricingServicesCreateSerializer

    @transaction.atomic
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        if 'gig_draft_id' in validated_data :
            gig_draft_id = validated_data.pop('gig_draft_id')

        try :              
            gig_draft = GigDraft.objects.get(id=gig_draft_id)
        except :
            raise NotFound

        gigs_extra_price = []
        extra_service_custom_list = []
        if 'gigs_extra_price' in validated_data :
            gigs_extra_price = validated_data.pop('gigs_extra_price')
        if 'extra_service_custom' in validated_data :
            extra_service_custom_list = validated_data.pop('extra_service_custom')    

        for extra_price in gigs_extra_price :
            price_extra_service = GigPriceExtraServiceDraft(
                gig_draft= gig_draft,              
                price_extra_service_id = extra_price['price_extra_service_id'],
                price_type_id = extra_price['price_type_id'],
                extra_price = extra_price['extra_price'],
                extra_days_id = extra_price['extra_days_id'],
                extra_day = extra_price['extra_day'],         
            )   
            price_extra_service.save()
        for extra_price_custom in extra_service_custom_list :
            price_service_custom = GigPriceExtraServiceCustomDraft(
                gig_draft= gig_draft,     
                **extra_price_custom
            )   
            price_service_custom.save()
        return Response(status=status.HTTP_201_CREATED)    


'''
View to send data in specific format for POST method
(endpoint = 'draft-pricing-extra-services-update/:gig_draft_id')
'''
class GigsExtraPricingServicesUpdateView(GenericAPIView) :
    """
    Gig Draft Pricing PATCH API .

    """      
    class GigsExtraPricingServicesUpdateSerializer(serializers.Serializer) :      
        pricings = GigPriceExtraServiceDraftUpdateSerializer(many=True)
        extra_service_custom = GigPriceExtraServiceCustomDraftUpdateSerializer(many=True,required=False)   


    serializer_class = GigsExtraPricingServicesUpdateSerializer

    def patch(self,request,gig_draft_id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        gig_draft_id = self.kwargs['gig_draft_id']
        pricings = []
        extra_service_custom = []
        extra_service_ids = []
        custom_ids = []
        if 'pricings' in validated_data :
                pricings = validated_data.pop('pricings')  
        if 'extra_service_custom' in validated_data :
                extra_service_custom = validated_data.pop('extra_service_custom')         
        for price in pricings :       
            if price['id'] != 0 :
                extra_service_ids.append(price['id'])
                GigPriceExtraServiceDraft.objects.filter(id=price['id']).update(
                    price_extra_service_id = price['price_extra_service_id'],
                    price_type = price['price_type'],
                    extra_price = price['extra_price'],
                    extra_days = price['extra_days'], 
                    extra_day = price['extra_day']                    
                )       
            else :
                gig_price_extra_service = GigPriceExtraServiceDraft.objects.create(
                    gig_draft_id = gig_draft_id,
                    price_extra_service_id = price['price_extra_service_id'],
                    price_type = price['price_type'],
                    extra_price = price['extra_price'],
                    extra_days = price['extra_days'],      
                    extra_day = price['extra_day']              
                )  
                extra_service_ids.append(gig_price_extra_service.id)
        GigPriceExtraServiceDraft.objects.filter(gig_draft_id=gig_draft_id).exclude(id__in=extra_service_ids).delete()                    
        for custom in extra_service_custom :
            if custom['id'] != 0 :               
                custom_ids.append(custom['id'])
                GigPriceExtraServiceCustomDraft.objects.filter(id=custom['id']).update(
                    title = custom['title'],
                    description = custom['description'],
                    extra_price = custom['extra_price'],
                    extra_days = custom['extra_days'],
                    extra_day = custom['extra_day']
                )     
            else :
                gig_price_custom = GigPriceExtraServiceCustomDraft.objects.create(
                    gig_draft_id = gig_draft_id,
                    title = custom['title'],
                    description = custom['description'],
                    extra_price = custom['extra_price'],
                    extra_days = custom['extra_days'],
                    extra_day = custom['extra_day']
                )         
                custom_ids.append(gig_price_custom.id)
        GigPriceExtraServiceCustomDraft.objects.filter(gig_draft_id=gig_draft_id).exclude(id__in=custom_ids).delete()     
        return Response(status=status.HTTP_200_OK)   


class ActiveGigPricengUpdateSerializer(serializers.Serializer) :
    is_price_package = serializers.BooleanField()
    gigs_price = GigPriceDraftSerializer(many=True)