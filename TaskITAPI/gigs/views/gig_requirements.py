from rest_framework.generics import GenericAPIView,CreateAPIView,ListAPIView,ListCreateAPIView,RetrieveUpdateAPIView,RetrieveDestroyAPIView
from gigs.models import Gig,GigDraft,GigRequirement,GigRequirementDraft,GigRequirementDetails,GigRequirementDetailsDraft
from rest_framework import serializers
from configs.serializers import QuestionFormSerializer
from gigs import values
from django.db.models import Prefetch
from rest_framework import status
from rest_framework.response import Response 
from TaskITAPI.pagination import StandardResultsSetPagination
from rest_framework.exceptions import NotFound, ValidationError


class GigsRequirementDetailsDraftSerializer(serializers.ModelSerializer) :

    class Meta :
        model = GigRequirementDetailsDraft
        fields = [
            'id',
            'description'
        ]

class GigsRequirementDetailsDraftUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()

    class Meta :
        model = GigRequirementDetailsDraft
        fields = [
            'id',
            'description'
        ]

class GigsRequirementDetailsSerializer(serializers.ModelSerializer) :

    class Meta :
        model = GigRequirementDetailsDraft
        fields = [
            'id',
            'description'
        ]


class GigsDraftRequirementSerializer(serializers.ModelSerializer) :
    question_form_id = serializers.IntegerField(write_only=True)
    question_form = QuestionFormSerializer(read_only=True)
    is_mandatory = serializers.BooleanField(required=False)
    is_multiselect = serializers.BooleanField(required=False)
    description = GigsRequirementDetailsDraftSerializer(many=True,write_only=True)
    descriptions = GigsRequirementDetailsSerializer(many=True,read_only=True)

    def setup_eager_loading(self, queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('gigrequirementdetailsdraft_set',GigRequirementDetailsDraft.objects.all(),to_attr='descriptions')
        )
        return queryset

    class Meta :
        model = GigRequirementDraft
        fields = [
            'id',
            # 'gig_draft_id',
            'gig_id',
            'is_mandatory',
            'question',
            'question_form_id',
            'question_form',
            'is_multiselect',
            'description',
            'descriptions'
        ]
    read_only_fields = ['gig_id']   
       

class GigsDraftRequirementUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    question_form_id = serializers.IntegerField(write_only=True)   
    is_mandatory = serializers.BooleanField(required=False)
    is_multiselect = serializers.BooleanField(required=False)
    description = GigsRequirementDetailsDraftUpdateSerializer(many=True,write_only=True)    

    def setup_eager_loading(self, queryset) :        
        return queryset

    class Meta :
        model = GigRequirementDraft
        fields = [
            'id',
            'is_mandatory',
            'question',
            'question_form_id',           
            'is_multiselect',
            'description',            
        ]
       


class GigsRequirementSerializer(serializers.ModelSerializer) :
    # question_form_id = serializers.IntegerField(write_only=True)
    question_form = QuestionFormSerializer(read_only=True)
    is_mandatory = serializers.BooleanField(required=False)
    is_multiselect = serializers.BooleanField(required=False)
    # description = GigsRequirementDetailsDraftSerializer(many=True,write_only=True)
    descriptions = GigsRequirementDetailsSerializer(many=True,read_only=True)

    def setup_eager_loading(self, queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('gigrequirementdetails_set',GigRequirementDetails.objects.all(),to_attr='descriptions')
        )
        return queryset

    class Meta :
        model = GigRequirement
        fields = [
            'id',
            'gig_id',
            'is_mandatory',
            'question',
            # 'question_form_id',
            'question_form',
            'is_multiselect',
            # 'description',
            'descriptions'
        ]      


'''
View to send data in specific format for POST method
(endpoint = '/gigs/requirements/)
'''
class GigsRequirementsCreateView(CreateAPIView) :
    """
    Gig Requirements POST API .

    """      
    class GigsRequirementsCreateSerializer(serializers.Serializer) :
        gig_draft_id =  serializers.IntegerField()
        requirements = GigsDraftRequirementSerializer(many=True)

    serializer_class = GigsRequirementsCreateSerializer


    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data

        if 'gig_draft_id' in validated_data :
            gig_draft_id = validated_data.pop('gig_draft_id')
        else :
            raise ValidationError(_('Gig draft id required.'), code='gig_draft_id_required') 
               
        try :              
            gig_draft = GigDraft.objects.get(id=gig_draft_id)
        except :
            raise NotFound
       
        requirements = None
        description = None
        if 'requirements' in validated_data :
            requirements = validated_data.pop('requirements')  

        for req in requirements :           
            gig_req = GigRequirementDraft(
                gig_draft=gig_draft,               
                is_mandatory = req['is_mandatory'],
                question = req['question'],
                question_form_id = req['question_form_id'],
                is_multiselect = req['is_multiselect']
            )      
            gig_req.save()
            for desc in req['description'] :
                gig_req_dtl = GigRequirementDetailsDraft(
                    gig_draft = gig_draft,                   
                    gig_requirement_draft = gig_req,
                    **desc
                )  
                gig_req_dtl.save()
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for GET method
(endpoint = '/gigs/draft-faq/)
'''
class GigsDraftRequirementsListView(ListAPIView) :
    """
    Gig Requirements GET API .
   
    """    
    queryset = GigRequirementDraft.objects.all()
    serializer_class = GigsDraftRequirementSerializer

    # Override this function to build custom queryset
    def get_queryset(self) :
        gig_draft_id = self.kwargs['gig_draft_id']
        queryset = GigRequirementDraft.objects.filter(gig_draft_id=gig_draft_id)            
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 



'''
View to send data in specific format for POST method
(endpoint = '/gigs/requirements-update/:gig_draft_id)
'''
class GigsDraftRequirementsUpdateView(GenericAPIView) :
    """
    Gig Draft Requirements PATCH API .

    """      
    class GigsDraftRequirementsUpdateSerializer(serializers.Serializer) :      
        requirements = GigsDraftRequirementUpdateSerializer(many=True)

    serializer_class = GigsDraftRequirementsUpdateSerializer

    def patch(self,request,gig_draft_id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        gig_draft_id = self.kwargs['gig_draft_id']
        if 'requirements' in validated_data :
                requirements = validated_data.pop('requirements')  
        gig_requirements_draft = GigRequirementDraft.objects.filter(gig_draft_id=gig_draft_id)        
        # for req in requirements :            
        #     if 'description' in req :
        #         description = req.pop('description')
        #     for req_draft in gig_requirements_draft :
        #         if req_draft.id == req['id'] :
        #             req_draft.is_mandatory = req['is_mandatory']
        #             req_draft.question = req['question']
        #             req_draft.question_form_id = req['question_form_id']
        #             req_draft.is_multiselect = req['is_multiselect']
        #     req_draft.save(update_fields=['is_mandatory','question','question_form_id','is_multiselect'])  
        #     gig_requirements_draft_details = GigRequirementDetailsDraft.objects.filter(gig_requirement_draft=req_draft)          
        #     for desc in description :
        #         for req_dtl in gig_requirements_draft_details :
        #             if req_dtl.id == desc['id'] :
        #                 req_dtl.description = desc['description']  
        #                 req_dtl.save(update_fields=['description'])
        for req in requirements :
            if 'description' in req :
                description = req.pop('description')
            if req['id'] != 0 :                
                gig_req_draft = GigRequirementDraft.objects.filter(id=req['id']).update(
                    is_mandatory=req['is_mandatory'],
                    question=req['question'],
                    question_form_id=req['question_form_id'],
                    is_multiselect=req['is_multiselect']
                )    
                for desc in description :
                    if req['id'] != 0 :          
                        GigRequirementDetailsDraft.objects.filter(id=desc['id']).update(
                            description=desc['description']                   
                        )
                    else :
                        GigRequirementDetailsDraft.objects.create(
                            gig_draft_id=gig_draft_id,
                            gig_requirement_draft=gig_req_draft,
                            description=desc['description']                   
                        )  
            else :
                gig_req_draft = GigRequirementDraft.objects.create(
                    gig_draft_id=gig_draft_id,
                    is_mandatory=req['is_mandatory'],
                    question=req['question'],
                    question_form_id=req['question_form_id'],
                    is_multiselect=req['is_multiselect']
                )    
                for desc in description :                   
                    GigRequirementDetailsDraft.objects.create(
                        gig_draft_id=gig_draft_id,
                        gig_requirement_draft=gig_req_draft,
                        description=desc['description']                   
                    )  
        return Response(status=status.HTTP_200_OK)   


'''
View to send data in specific format for GET method
(endpoint = '/gigs/faq/)
'''
class GigsRequirementsListView(ListAPIView) :
    """
    Gig Requirements GET API .
   
    """    
    queryset = GigRequirement.objects.all()
    serializer_class = GigsRequirementSerializer

    # Override this function to build custom queryset
    def get_queryset(self) :
        id = self.kwargs['id']
        queryset = GigRequirement.objects.filter(gig_id=id)            
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset         