from rest_framework.generics import GenericAPIView
from rest_framework import status
from rest_framework.response import Response
from django.db import transaction
from rest_framework import serializers
from django.utils import timezone
from django.utils.translation import gettext as _
from rest_framework.exceptions import PermissionDenied, ValidationError
from gigs.models import Gig,GigDraft,GigSearchTag,GigSearchTagDraft,GigSubCategoryMetaDataTypeDraft,GigSubCategoryMetaDataType,GigSubCategoryMetaDataTypeDraftDetails,\
    GigSubCategoryMetaDataTypeDetails,GigSubCategoryMetaDataTypeOtherDraft,GigSubCategoryMetaDataTypeOther,GigGalleryDraft,GigGallery,GigPriceDraft,GigPrice,\
        GigSubCategoryPriceDetailsDraft,GigSubCategoryPriceDetails,GigPriceExtraServiceDraft,GigPriceExtraService,GigPriceExtraServiceCustomDraft,GigPriceExtraServiceCustom,\
            GigFaqDraft,GigFaq,GigRequirementDraft,GigRequirement,GigRequirementDetailsDraft,GigRequirementDetails
from configs.values import GIG_STATUS
from gigs.utils import create_gig_log
import os,shutil
from TaskITAPI.settings import MEDIA_ROOT
import os,shutil
from notifications.tasks import notify_user
from notifications import events

def update_approved_gig_tags(request,gig_draft,gig) :
    gigs_update_tags_fields = []
    gig_search_tags_draft = GigSearchTagDraft.objects.filter(gig_draft=gig_draft)
    if gig_draft.gig != None :
        gig_search_tag = GigSearchTag.objects.filter(gig=gig_draft.gig)
        for search_tag_draft in gig_search_tags_draft :
            for search_tag in gig_search_tag :
                if search_tag.id == search_tag_draft.id:
                    for key, value in search_tag_draft.items() :
                        if getattr(search_tag, key) != value :
                                gigs_update_tags_fields.append(key)
                                setattr(search_tag, key, value)
                    search_tag.save(update_fields = gigs_update_tags_fields)            
    else :            
        for tags in gig_search_tags_draft :
            gigs_tag = GigSearchTag.objects.create(
                gig = gig,
                search_tags = tags.search_tags
            )
    gig_search_tags_draft.update(gig=gig)


def update_gig_suggest_other(request,gig_draft,gig_sub_category_metadata_type) :
    others_update_fields = []
    if gig_draft.gig != None :
        metadata_others_draft = GigSubCategoryMetaDataTypeOtherDraft.objects.filter(gig_draft=gig_draft)
        metadata_others = GigSubCategoryMetaDataTypeOther.objects.filter(gig=gig_draft.gig)
        for others_draft in metadata_others_draft :
            for others in metadata_others :
                if others.id == others_draft.id :
                    for key, value in others_draft.items() :
                            if getattr(others, key) != value :
                                    others_update_fields.append(key)
                                    setattr(others, key, value)
                others.save(update_fields = others_update_fields) 
    else :
        metadata_others_draft = GigSubCategoryMetaDataTypeOtherDraft.objects.filter(gig_draft=gig_draft)
        for others_draft in metadata_others_draft :
            GigSubCategoryMetaDataTypeOther.objects.create(
                gig = gig_sub_category_metadata_type.gig,
                sub_category_metadata_type = gig_sub_category_metadata_type,
                other = others_draft.other
            )


def update_gig_metadata_type_draft_details(request,gig_draft,gig_sub_category_metadata_type) :
    metadata_dtl_update_fields = []
    if gig_draft.gig != None :
        metadata_type_draft_details = GigSubCategoryMetaDataTypeDraftDetails.objects.filter(gig_draft=gig_draft)
        metadata_type_details = GigSubCategoryMetaDataTypeDetails.objects.filter(gig=gig_draft.gig)
        for draft_details in metadata_type_draft_details :
            for metadata_dtl in metadata_type_details :
                if metadata_dtl.id == draft_details.id :
                    for key, value in draft_details.items() :
                            if getattr(metadata_dtl, key) != value :
                                    metadata_dtl_update_fields.append(key)
                                    setattr(metadata_dtl, key, value)
                metadata_dtl.save(update_fields = metadata_dtl_update_fields) 
                if metadata_dtl.is_suggest_other :
                    gig_suggest_other =  update_gig_suggest_other(request,gig_draft,metadata_dtl)  
    else :        
        metadata_type_draft_details = GigSubCategoryMetaDataTypeDraftDetails.objects.filter(gig_draft=gig_draft)
        for draft_details in metadata_type_draft_details :
            metadata_type_draft_dtl = GigSubCategoryMetaDataTypeDetails.objects.create(
                gig = gig_sub_category_metadata_type.gig,
                sub_category_metadata_type = gig_sub_category_metadata_type,
                metadata_type_dtl = draft_details.metadata_type_dtl
            )
        if gig_sub_category_metadata_type.is_suggest_other :
            gig_suggest_other =  update_gig_suggest_other(request,gig_draft,gig_sub_category_metadata_type)   
     

def update_approved_gig_metadata(request,gig_draft,gig) :
    metadata_update_fields = []
    if gig_draft.gig != None :
        gig_sub_category_metadata_draft = GigSubCategoryMetaDataTypeDraft.objects.filter(gig_draft=gig_draft)
        gig_sub_category_metadata_type = GigSubCategoryMetaDataType.objects.filter(gig=gig_draft.gig)
        for metadata_draft in gig_sub_category_metadata_draft :
            for metadata_type in gig_sub_category_metadata_type :
                if metadata_type.id == metadata_draft.id:
                    for key, value in metadata_draft.items() :
                        if getattr(metadata_type, key) != value :
                            metadata_update_fields.append(key)
                            setattr(metadata_type, key, value)
                    metadata_type.save(update_fields = metadata_update_fields)     
                    gig_metadata_type_draft_details = update_gig_metadata_type_draft_details(request,gig_draft,metadata_type)
    else :
        gig_sub_category_metadata = GigSubCategoryMetaDataTypeDraft.objects.filter(gig_draft=gig_draft)
        for metadata in gig_sub_category_metadata :
            gig_sub_category_metadata_type = GigSubCategoryMetaDataType.objects.create(
                gig = gig,
                sub_category = metadata.sub_category,
                metadata_type = metadata.metadata_type,
                is_single = metadata.is_single,
                is_mandatory = metadata.is_mandatory,
                min_value = metadata.min_value,
                max_value = metadata.max_value,
                is_suggest_other = metadata.is_suggest_other
            )
            gig_metadata_type_draft_details = update_gig_metadata_type_draft_details(request,gig_draft,gig_sub_category_metadata_type)
        gig_sub_category_metadata.update(gig=gig)    


def update_approved_gig_gallery(request,gig_draft,gig) :
    gallery_update_fields = []
    if gig_draft.gig != None :
        gallery_draft = GigGalleryDraft.objects.filter(gig_draft=gig_draft)
        gallery = GigGallery.objects.filter(gig=gig_draft.gig)
        for gal_draft in gallery_draft :
            for gal in gallery :
                if gal.id == gal_draft.id :
                    for key, value in gal_draft.items() :
                        if getattr(gal, key) != value :
                            gallery_update_fields.append(key)
                            setattr(gal, key, value)
                    gal.save(update_fields = gallery_update_fields)  
    else :
        gallery_draft = GigGalleryDraft.objects.filter(gig_draft=gig_draft)        
        for draft in gallery_draft :
            source_file_path = os.path.join(MEDIA_ROOT,str(draft.file_path)).replace('\\', '/')
            folder_file_path = "Gig/Gallery/File_Path/"+str(gig.id)+"/"

            source_file_path_thumbnail = os.path.join(MEDIA_ROOT,str(draft.file_path_thumbnail)).replace('\\', '/')
            folder_file_path_thumbnail = "Gig/Gallery/File_Path_Thumbnail/"+str(gig.id)+"/"

            dest_file_path = os.path.join(MEDIA_ROOT,folder_file_path).replace('\\', '/') 
            dest_file_path_thumbnail = os.path.join(MEDIA_ROOT,folder_file_path_thumbnail).replace('\\', '/') 

            if not os.path.exists(dest_file_path):
                os.makedirs(dest_file_path)

            if not os.path.exists(dest_file_path_thumbnail):
                os.makedirs(dest_file_path_thumbnail)  

            if os.path.exists(source_file_path) and os.path.exists(source_file_path_thumbnail) :
                shutil.copy(source_file_path,dest_file_path)
                file_path_name = os.path.basename(source_file_path)  
                os.rename(str(dest_file_path)+str(file_path_name),str(dest_file_path)+str(gig.id)+"_"+str(file_path_name))

                shutil.copy(source_file_path_thumbnail,dest_file_path_thumbnail)
                file_path_thumbnail_name = os.path.basename(source_file_path_thumbnail)  
                os.rename(str(dest_file_path_thumbnail)+str(file_path_thumbnail_name),str(dest_file_path_thumbnail)+str(gig.id)+"_"+str(file_path_thumbnail_name))

                gig_gallery = GigGallery.objects.create(
                    gig = gig,
                    gig_gallery_type = draft.gig_gallery_type,
                    file_path = (folder_file_path+str(gig.id)+"_"+str(file_path_name)).replace('\\', '/'), ##draft.file_path,
                    file_path_thumbnail = (folder_file_path_thumbnail+str(gig.id)+"_"+str(file_path_thumbnail_name)).replace('\\', '/') #draft.file_path_thumbnail
                )
        gallery_draft.update(gig=gig)    
            

def update_approved_gig_price(request,gig_draft,gig) :
    gig_price_update_fields = []
    gig_price_dtl_update_fields = []
    if gig_draft.gig != None :
        gig_price_draft = GigPriceDraft.objects.filter(gig_draft=gig_draft)
        gig_price = GigPrice.objects.filter(gig=gig_draft.gig)
        for price_draft in gig_price_draft :
            for price in gig_price :
                if price.id == price_draft.id:
                    for key, value in price_draft.items() :
                        if getattr(price, key) != value :
                            gig_price_update_fields.append(key)
                            setattr(price, key, value)
                    price.save(update_fields = gig_price_update_fields)  
        gig_price_draft_details = GigSubCategoryPriceDetailsDraft.objects.filter(gig_draft=gig_draft)
        gig_price_details = GigSubCategoryPriceDetails.objects.filter(gig=gig_draft.gig)
        for gig_price_draft in gig_price_draft_details :
            for gig_price in gig_price_details :
                if gig_price.id == gig_price_draft.id:
                    for key, value in gig_price_draft.items() :
                        if getattr(gig_price, key) != value :
                            gig_price_dtl_update_fields.append(key)
                            setattr(gig_price, key, value)
                    gig_price.save(update_fields = gig_price_dtl_update_fields)  
    else :        
        gig_price_draft = GigPriceDraft.objects.filter(gig_draft=gig_draft)
        for price_draft in gig_price_draft :
            gig_price = GigPrice.objects.create(
                gig = gig,
                price_type = price_draft.price_type,
                price = price_draft.price           
            )
            gig_price_draft_details = GigSubCategoryPriceDetailsDraft.objects.filter(gig_price_draft_id=price_draft.id,gig_draft=gig_draft)
            for gig_price_details  in gig_price_draft_details :
                gig_price_dtl = GigSubCategoryPriceDetails.objects.create(
                    gig = gig,
                    gig_price = gig_price,
                    sub_category_price_scope = gig_price_details.sub_category_price_scope,
                    sub_category_price_scope_dtl = gig_price_details.sub_category_price_scope_dtl,
                    value = gig_price_details.value
                )
        gig_price_draft.update(gig=gig)        
        gig_price_draft_details.update(gig=gig)


def update_approved_gig_extra_price(request,gig_draft,gig) : 
    price_update_fields = []
    price_services_update_fields = []
    if gig_draft.gig != None :
        gig_price_extra_service_draft = GigPriceExtraServiceDraft.objects.filter(gig_draft=gig_draft)
        gig_price_extra_service = GigPriceExtraService.objects.filter(gig=gig_draft.gig)
        for price_extra_service_draft in gig_price_extra_service_draft :
            for extra_service in gig_price_extra_service :
                if extra_service.id == price_extra_service_draft.id:
                    for key, value in price_extra_service_draft.items() :
                        if getattr(extra_service, key) != value :
                            price_update_fields.append(key)
                            setattr(extra_service, key, value)
                    extra_service.save(update_fields = price_update_fields)
        gig_price_extra_serice_custom_draft = GigPriceExtraServiceCustomDraft.objects.filter(gig_draft=gig_draft)
        gig_price_extra_serice_custom = GigPriceExtraServiceCustom.objects.filter(gig=gig_draft.gig)
        for extra_service_custom_draft in gig_price_extra_serice_custom_draft :
            for extra_service_custom in gig_price_extra_serice_custom :
                if extra_service_custom.id == extra_service_custom_draft.id:
                    for key, value in extra_service_custom_draft.items() :
                        if getattr(extra_service_custom, key) != value :
                            price_services_update_fields.append(key)
                            setattr(extra_service_custom, key, value)
                    extra_service_custom.save(update_fields = price_services_update_fields) 
    else :        
        gig_price_extra_service_draft = GigPriceExtraServiceDraft.objects.filter(gig_draft=gig_draft)
        for price_extra_service in gig_price_extra_service_draft :
            gig_price_extra_service = GigPriceExtraService.objects.create(
                gig = gig,
                price_extra_service = price_extra_service.price_extra_service,
                price_type = price_extra_service.price_type,
                extra_price = price_extra_service.extra_price,
                extra_days = price_extra_service.extra_days,
                extra_day = price_extra_service.extra_day
            )
        gig_price_extra_service_draft.update(gig=gig)    
        gig_price_extra_serice_custom_draft = GigPriceExtraServiceCustomDraft.objects.filter(gig_draft=gig_draft)
        for extra_service_custom in gig_price_extra_serice_custom_draft :
            gig_price_extra_service_custom = GigPriceExtraServiceCustom.objects.create(
                gig = gig,
                title = extra_service_custom.title,
                description = extra_service_custom.description,
                extra_price = extra_service_custom.extra_price,
                extra_days = extra_service_custom.extra_days,
                extra_day = extra_service_custom.extra_day
            )
        gig_price_extra_serice_custom_draft.update(gig=gig)

def update_approved_faq(request,gig_draft,gig) :
    faq_update_fields = []
    if gig_draft.gig != None :
        gig_faq_draft_list = GigFaqDraft.objects.filter(gig_draft=gig_draft)
        gig_faq_list = GigFaq.objects.filter(gig=gig_draft.gig)
        for gig_faq_draft in gig_faq_draft_list :
            for faq in gig_faq_list :
                if faq.id == gig_faq_draft.id:
                    for key, value in gig_faq_draft.items() :
                        if getattr(faq, key) != value :
                            faq_update_fields.append(key)
                            setattr(faq, key, value)
                    faq.save(update_fields = faq_update_fields) 
    else :
        gig_faq_draft = GigFaqDraft.objects.filter(gig_draft=gig_draft)
        for faq in gig_faq_draft :
            gig_faq = GigFaq.objects.create(
                gig = gig,
                seq_no = faq.seq_no,
                question = faq.question,
                answer = faq.answer
            )    
        gig_faq_draft.update(gig=gig)    


def update_approved_requirements(request,gig_draft,gig) :
    req_update_fields = []
    req_dtl_update_fields = []
    if gig_draft.gig != None :
        gig_req_draft_list = GigRequirementDraft.objects.filter(gig_draft=gig_draft)
        gig_req_list = GigRequirement.objects.filter(gig=gig_draft.gig)
        for gig_req_draft in gig_req_draft_list :
            for req in gig_req_list :
                if req.id == gig_req_draft.id:
                    for key, value in gig_req_draft.items() :
                        if getattr(req, key) != value :
                            req_update_fields.append(key)
                            setattr(req, key, value)
                    req.save(update_fields = req_update_fields)  
        gig_req_details_draft = GigRequirementDetailsDraft.objects.filter(gig_draft=gig_draft)
        gig_req_details = GigRequirementDetails.objects.filter(gig=gig_draft.gig)
        for gig_req_draft_dtl in gig_req_details_draft :
            for gig_req_dtl in gig_req_details :
                if gig_req_dtl.id == gig_req_draft_dtl.id:
                    for key, value in gig_req_draft_dtl.items() :
                        if getattr(gig_req_dtl, key) != value :
                            req_dtl_update_fields.append(key)
                            setattr(gig_req_dtl, key, value)
                    gig_req_dtl.save(update_fields = req_dtl_update_fields) 
    else :
        gig_req_draft = GigRequirementDraft.objects.filter(gig_draft=gig_draft)
        for req in gig_req_draft :
            gig_req = GigRequirement.objects.create(
                gig = gig,
                is_mandatory = req.is_mandatory,
                question = req.question,
                question_form = req.question_form,
                is_multiselect = req.is_multiselect          
            )
            gig_req_details_draft = GigRequirementDetailsDraft.objects.filter(gig_draft=gig_draft,gig_requirement_draft_id=req.id)
            for re_dtl in gig_req_details_draft :
                gig_req_dtl =  GigRequirementDetails.objects.create(
                    gig = gig_req.gig,
                    gig_requirement = gig_req,
                    description = re_dtl.description
                )   
            gig_req_details_draft.update(gig=gig)    
        gig_req_draft.update(gig=gig)        
        


'''
View to send data in specific format for POST method
(endpoint = '/gigs/approve')
'''
class GigApproveView(GenericAPIView) :
    """
    Approve Gig
    
    Approve Gig 
    """
    class GigApproveSerializer(serializers.Serializer) :
        pass

    queryset = GigDraft.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    # serializer_class = GigApproveSerializer   
    
    @transaction.atomic
    def patch(self, request, id) :  
        gig_draft = self.get_object()
        gig_update_fields = []
        gig = None
        if gig_draft.gig_status_id != GIG_STATUS['Pending_for_Approval'] :
            raise PermissionDenied
        if gig_draft.gig != None :
            gig = Gig.objects.get(id=gig_draft.gig)
            for key, value in gig_draft.items() :
                if getattr(gig, key) != value :
                    gig_update_fields.append(key)
                    setattr(gig, key, value)
            gig.modified_by = self.context['request'].user
            gig.modified_user = self.context['request'].user.user_name
            gig.modified_date = timezone.now()
            gig_update_fields = gig_update_fields + ['modified_by','modified_user','modified_date']
            gig.save(update_fields = gig_update_fields)        
        else :
            gig = Gig.objects.create(
                title = gig_draft.title,
                category = gig_draft.category,
                sub_category = gig_draft.sub_category,
                is_price_package = gig_draft.is_price_package,
                description = gig_draft.description,
                gig_status_id = GIG_STATUS['Active'],
                created_by = gig_draft.created_by,
                created_user = gig_draft.created_user,
                created_date = timezone.now()
            )           
        gig_log = create_gig_log(self.request,gig_draft,gig)
        gig_tags = update_approved_gig_tags(self.request,gig_draft,gig)
        gig_metadata = update_approved_gig_metadata(self.request,gig_draft,gig)
        gig_gallery = update_approved_gig_gallery(self.request,gig_draft,gig)
        gig_price = update_approved_gig_price(self.request,gig_draft,gig)
        gig_extra_price = update_approved_gig_extra_price(self.request,gig_draft,gig)
        gig_faq = update_approved_faq(self.request,gig_draft,gig)
        gig_req = update_approved_requirements(self.request,gig_draft,gig)
        gig_draft.gig = gig
        gig_draft.gig_status_id = GIG_STATUS['Active']
        gig_draft.is_approved = True
        gig_draft.approve_by = request.user
        gig_draft.approved_user = request.user.user_name
        gig_draft.approved_date = timezone.now()
        gig_draft.modified_by = request.user
        gig_draft.modified_user = request.user.user_name
        gig_draft.modified_date = timezone.now()
        gig_draft.save()
        notify_user.delay(events.GIG_PUBLISH_APPROVED,{'gig_draft':gig_draft})
        return Response(status=status.HTTP_200_OK)

'''
View to send data in specific format for POST method
(endpoint = '/gigs/modify')
'''
class GigModificationView(GenericAPIView) :
    """
    Gig Require Modification
    
    Gig Require Modification
    """
    class GigModificationSerializer(serializers.Serializer) :
        modification_required_comment = serializers.CharField(max_length=500,allow_null=True)

    queryset = GigDraft.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    serializer_class = GigModificationSerializer    

    
    @transaction.atomic
    def patch(self, request, id) :      
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        gig_draft = self.get_object()
        gig = None
        if gig_draft.gig_status_id != GIG_STATUS['Pending_for_Approval'] :
            raise PermissionDenied
        gig_draft.gig_status_id = GIG_STATUS['Requires_Modification']
        gig_draft.modification_required_comment = validated_data['modification_required_comment']
        gig_draft.modified_by = request.user
        gig_draft.modified_user = request.user.user_name
        gig_draft.modified_date = timezone.now()
        gig_draft.save()
        gig_log = create_gig_log(self.request,gig_draft,gig)
        notify_user.delay(events.GIG_MODIFICATION_REQUIRED,{'gig_draft' : gig_draft})
        return Response(status=status.HTTP_200_OK)

'''
View to send data in specific format for POST method
(endpoint = '/gigs/reject')
'''
class GigRejectedView(GenericAPIView) :
    """
    Reject Gig 
    
    Reject Gig 
    """
    class GigRejctedSerializer(serializers.Serializer) :
        reject_comment = serializers.CharField(max_length=500,allow_null=True)

    queryset = GigDraft.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    serializer_class = GigRejctedSerializer    

    
    @transaction.atomic
    def patch(self, request, id) :    
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data  
        gig_draft = self.get_object()
        gig = None
        if gig_draft.gig_status_id != GIG_STATUS['Pending_for_Approval'] :
            raise PermissionDenied
        gig_draft.gig_status_id = GIG_STATUS['Denied']
        gig_draft.reject_comment = validated_data['reject_comment']
        gig_draft.modified_by = request.user
        gig_draft.modified_user = request.user.user_name
        gig_draft.modified_date = timezone.now()
        gig_draft.save()
        gig_log = create_gig_log(self.request,gig_draft,gig)
        notify_user.delay(events.GIG_PUBLISH_REJECTED,{'gig_draft':gig_draft})
        return Response(status=status.HTTP_200_OK)        

'''
View to send data in specific format for POST method
(endpoint = '/gigs/paused')
'''
class GigPausedView(GenericAPIView) :
    """
    Gig Paused
    
    Gig Paused
    """
    class GigPausedSerializer(serializers.Serializer) :
        pass

    queryset = Gig.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    # serializer_class = GigPausedSerializer    

    
    @transaction.atomic
    def patch(self, request, id) :      
        gig = self.get_object()
        if gig.gig_status_id != GIG_STATUS['Active'] :
            raise PermissionDenied
        gig.gig_status_id = GIG_STATUS['Paused']
        gig.modified_by = request.user
        gig.modified_user = request.user.user_name
        gig.modified_date = timezone.now()
        gig.save()
        gig_draft = None
        gig_log = create_gig_log(self.request,gig_draft,gig)
        return Response(status=status.HTTP_200_OK)        

'''
View to send data in specific format for POST method
(endpoint = '/gigs/resume')
'''
class GigResumeView(GenericAPIView) :
    """
    Gig Resume
    
    Gig Resume
    """
    class GigResumeSerializer(serializers.Serializer) :
        pass

    queryset = Gig.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    # serializer_class = GigResumeSerializer    

    
    @transaction.atomic
    def patch(self, request, id) :      
        gig = self.get_object()
        if gig.gig_status_id != GIG_STATUS['Paused'] :
            raise PermissionDenied
        gig.gig_status_id = GIG_STATUS['Active']
        gig.modified_by = request.user
        gig.modified_user = request.user.user_name
        gig.modified_date = timezone.now()
        gig.save()
        gig_draft=None
        gig_log = create_gig_log(self.request,gig_draft,gig)
        return Response(status=status.HTTP_200_OK)            