from rest_framework.generics import CreateAPIView,GenericAPIView
from gigs.models import Gig,GigDraft
from gigs.serializers import ActiveGigUpdateSerializer,create_gig_draft_search_tags,create_metadata_type_draft,update_metadata_type_draft,create_faq_draft,\
    create_req_draft,create_gig_price,create_gigs_extra_price_service,create_gigs_extra_price_custom_service,create_gig_gallery
from django.db import transaction 
from configs.values import GIG_STATUS
from gigs.utils import create_gig_log
from rest_framework import status
from rest_framework.response import Response 


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/active-gigs-update')
'''
class ActiveGigUpdateView(GenericAPIView) :
    serializer_class = ActiveGigUpdateSerializer

    @transaction.atomic
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        gig_info = None
        gallery = []
        gig_id = validated_data['gig_id']
        gig_tags = []
        metadata_type_draft = []
        faq = []
        req = []
        gigs_price = None
        gigs_price_lst = []
        is_price_package = False
        gigs_extra_price = []
        extra_service_custom = []

        if 'gig_info' in validated_data :
            gig_info = validated_data.pop('gig_info')
        if 'gallery' in validated_data :
            gallery = validated_data.pop('gallery')    
        if 'faq' in validated_data :
            faq = validated_data.pop('faq')  
        if 'req' in validated_data :
            req = validated_data.pop('req')     
        if 'gigs_price' in validated_data :
            gigs_price = validated_data.pop('gigs_price')      
            if 'is_price_package' in gigs_price :
                is_price_package = gigs_price.pop('is_price_package')
            if 'gigs_price' in gigs_price :
                gigs_price_lst = gigs_price.pop('gigs_price')      
        if 'gigs_extra_price' in validated_data :
            gigs_extra_price = validated_data.pop('gigs_extra_price')  
        if 'extra_service_custom' in validated_data :
            extra_service_custom = validated_data.pop('extra_service_custom')     

        if gig_info :
            if 'gig_tags' in gig_info :
                gig_tags = gig_info.pop('gig_tags')
            if 'metadata_type_draft' in gig_info :
                metadata_type_draft = gig_info.pop('metadata_type_draft')     
            gig_draft = GigDraft.objects.create(
                **gig_info,
                gig_id = None,
                gig_status_id = GIG_STATUS['Draft'],               
                created_by = self.request.user,
                created_user = self.request.user.user_name
            )  
            gigs = None
            gig_log = create_gig_log(self.request,gig_draft,gigs)
            create_gig_draft_search_tags(self,gig_draft,gig_tags,gig_id)
            create_metadata_type_draft(self,gig_draft,metadata_type_draft)
        else :          
            gig = Gig.objects.get(id=gig_id)
            gig_draft = GigDraft.objects.create(
                title = gig.title,
                category = gig.category,
                sub_category = gig.sub_category,
                is_price_package = gig.is_price_package,
                description = gig.description,
                gig_status_id = GIG_STATUS['Draft'],             
                created_by = self.request.user,
                created_user = self.request.user.user_name               
            )
            gigs = None
            gig_log = create_gig_log(self.request,gig_draft,gigs)
            create_gig_draft_search_tags(self,gig_draft,gig_tags,gig_id)
            update_metadata_type_draft(self,gig_draft,gig_id)       
        create_faq_draft(self,gig_draft,faq,gig_id)    
        create_req_draft(self,gig_draft,req,gig_id)          
        create_gig_price(self,gigs_price_lst,gig_draft,gig_id)
        create_gigs_extra_price_service(self,gigs_extra_price,gig_draft,gig_id)
        create_gigs_extra_price_custom_service(self,extra_service_custom,gig_draft,gig_id)
        create_gig_gallery(self,gig_draft,gallery,gig_id)
        gig_draft.gig_id = gig_id 
        gig_draft.is_price_package = is_price_package
        gig_draft.save()
        res = {
            'gig_draft_id' : gig_draft.id
        }
        return Response(res,status=status.HTTP_200_OK)      

