from rest_framework.generics import ListAPIView,GenericAPIView,RetrieveAPIView
from gigs.models import Gig,GigDraft,GigSearchTag,GigPrice,GigSubCategoryPriceDetails,UserFavouriteGigs,GigPrice,GigPriceDraft,GigSubCategoryPriceDetailsDraft,GigLastVisited
from gigs.serializers import GigsSearchResultSerializer,GigsDetailsSerializer,GigsDraftDetailsSerializer,GigsPricingDetailsSerializer,GigsDraftPricingDetailsSerializer,\
    RelevantSearchesSerializer,PopularServicesSerializer,SellerOfferGigListSerializer
from django.db.models import Q,F,Value,CharField
from masters.models import Category,SubCategory
from gigs import values
from rest_framework import status
from rest_framework.response import Response 
from TaskITAPI.pagination import StandardResultsSetPagination
from configs.values import GIG_STATUS
from rest_framework import serializers
from django.db import transaction 
from configs.values import PRICE_TYPE
from settings.values import configs
from settings.models import SystemConfiguration

'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/home_screen/search-result/)
'''
class GigSearchResultListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    * search = Filter by Category,Sub Category and Search tags ( Index Page Filter) *
    
    1. title = To filter by Title   
    2. category_id = To filter by Category ID
    3. category_name = To filter by Category Name
    4. sub_category_id = To filter by Sub Category ID
    5. sub_category_name = To filter by Sub Category Name
    6. min_price = Min amount
    7. max_price = Ma amount
    8. delivery_time = Fiter by Delivery Time
    9. search_tags = Gigs Tags
    10. status_id = To filter by Gig Status 
    11. seller_level = Filter by ID of Level
    12. seller_lang_id = Filter by ID of Seller Language eg.(1/2/3..)
    13. sort_by = Filter by Soting ID ( Best Selling=1,Highest Rated = 2,Latest Arrivals=3)
    14. offer_delivery_time = Filter by offer delivery time
    15. offer_budget = Filter by offer budget
    

    E.g. 127.0.0.1:8000/api/v1/categories/?category_name=string&source=   

    """
    serializer_class = GigsSearchResultSerializer
    pagination_class = StandardResultsSetPagination
    authentication_classes = []
    permission_classes = []

    def apply_filters(self,queryset) :
        q_objects = Q()
        search = self.request.GET.get('search')
        title = self.request.GET.get('title')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')
        sub_category_id = self.request.GET.get('sub_category_id')
        sub_category_name = self.request.GET.get('sub_category_name')
        min_price = self.request.GET.get('min_price')
        max_price = self.request.GET.get('max_price')
        delivery_time = self.request.GET.get('delivery_time')
        search_tags = self.request.GET.get('search_tags')
        seller_level = self.request.GET.get('seller_level')
        seller_lang_id = self.request.GET.get('seller_lang_id')
        sort_by = self.request.GET.get('sort_by')
        status_id = self.request.GET.get('status_id')
        offer_delivery_time = self.request.GET.get('offer_delivery_time')
        offer_budget = self.request.GET.get('offer_budget')

        if search :
            gig_ids = GigSearchTag.objects.filter(search_tags__icontains=search)
            queryset = queryset.filter(Q(id__in=gig_ids) | Q(title__icontains=search) | Q(category__category_name__icontains=search) | Q(sub_category__sub_category_name__icontains=search))
        if title :
            q_objects.add(Q(title__icontains=title), Q.AND)
        if category_id :
            q_objects.add(Q(category_id=category_id), Q.AND)  
        if category_name :
            q_objects.add(Q(category__category_name__icontains=category_name), Q.AND)       
        if sub_category_id :
            q_objects.add(Q(sub_category_id=sub_category_id), Q.AND)
        if sub_category_name :
            q_objects.add(Q(sub_category__sub_category_name__icontains=sub_category_name), Q.AND) 
        if min_price and max_price :
            gig_ids = GigPrice.objects.filter(price__range=[min_price,max_price],price_type_id=PRICE_TYPE['Basic']).values_list('gig_id',flat=True)
            q_objects.add(Q(id__in=gig_ids), Q.AND) 
        if delivery_time :        
            actual_value = ''.join(delivery_time.split(' ')).split(',')      
            gig_ids = GigSubCategoryPriceDetails.objects.filter(sub_category_price_scope_dtl__actual_value__in=actual_value).values_list('gig_id',flat=True)  
            q_objects.add(Q(id__in=gig_ids), Q.AND)               
        if search_tags :
            gig_ids = GigSearchTag.objects.filter(search_tags__icontains=search_tags)
            q_objects.add(Q(id__in=gig_ids) , Q.AND) 
        if status_id :
            q_objects.add(Q(gig_status_id=status_id), Q.AND)         
        if seller_lang_id :
            lang_id = ''.join(seller_lang_id.split(' ')).split(',')      
            q_objects.add(Q(created_by__lang_id__in=lang_id), Q.AND)  
        if seller_level :        
            level_ids = ''.join(seller_level.split(' ')).split(',')                
            q_objects.add(Q(created_by__level_id__in=level_ids), Q.AND)   
        if offer_delivery_time :   
            delivery_time_sys = SystemConfiguration.objects.filter(code_name=configs.POST_REQUEST_DELIVERY_TIME_LOGIC)
            _to = int(offer_delivery_time) + int(delivery_time_sys[0].field_value)           
            gig_ids = GigSubCategoryPriceDetails.objects.filter(sub_category_price_scope_dtl__actual_value__in=[offer_delivery_time,_to]).values_list('gig_id',flat=True)  
            q_objects.add(Q(id__in=gig_ids), Q.AND)  
        if offer_budget :           
            budget_sys = SystemConfiguration.objects.filter(code_name=configs.POST_REQUEST_DELIVERY_TIME_LOGIC)
            _from = int(offer_budget)
            _to = _from + int((_from * int(budget_sys[0].field_value))/100)           
            gig_ids = GigPrice.objects.filter(price__range=[_from,_to]).values_list('gig_id',flat=True)  
            q_objects.add(Q(id__in=gig_ids), Q.AND)                           
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        if sort_by :
            if sort_by == '1' :
               pass
            elif sort_by == '2' :
                queryset = queryset.order_by('-rating')
            elif sort_by == '3' :
                queryset = queryset.order_by('-created_date') 
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL        
        if is_active == 'false' :    
            queryset = Gig.all_objects.filter(is_active=False)
        else : 
            queryset = Gig.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset



'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/home_screen/active-search-result/)
'''
class GigActiveSearchResultListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    * search = Filter by Category,Sub Category and Search tags ( Index Page Filter) *
    
    1. title = To filter by Title   
    2. category_id = To filter by Category ID
    3. category_name = To filter by Category Name
    4. sub_category_id = To filter by Sub Category ID
    5. sub_category_name = To filter by Sub Category Name
    6. min_price = Min amount
    7. max_price = Ma amount
    8. delivery_time = Fiter by Delivery Time
    9. search_tags = Gigs Tags
    10. status_id = To filter by Gig Status 
    11. seller_level = Filter by ID of Level
    12. seller_lang_id = Filter by ID of Seller Language eg.(1/2/3..)
    13. sort_by = Filter by Soting ID ( Best Selling=1,Highest Rated = 2,Latest Arrivals=3)
    14. offer_delivery_time = Filter by offer delivery time
    15. offer_budget = Filter by offer budget
    

    E.g. 127.0.0.1:8000/api/v1/categories/?category_name=string&source=   

    """
    serializer_class = GigsSearchResultSerializer
    pagination_class = StandardResultsSetPagination

    def apply_filters(self,queryset) :
        q_objects = Q()
        search = self.request.GET.get('search')
        title = self.request.GET.get('title')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')
        sub_category_id = self.request.GET.get('sub_category_id')
        sub_category_name = self.request.GET.get('sub_category_name')
        min_price = self.request.GET.get('min_price')
        max_price = self.request.GET.get('max_price')
        delivery_time = self.request.GET.get('delivery_time')
        search_tags = self.request.GET.get('search_tags')
        seller_level = self.request.GET.get('seller_level')
        seller_lang_id = self.request.GET.get('seller_lang_id')
        sort_by = self.request.GET.get('sort_by')
        status_id = self.request.GET.get('status_id')
        offer_delivery_time = self.request.GET.get('offer_delivery_time')
        offer_budget = self.request.GET.get('offer_budget')

        if search :
            gig_ids = GigSearchTag.objects.filter(search_tags__icontains=search)
            queryset = queryset.filter(Q(id__in=gig_ids) | Q(title__icontains=search) | Q(category__category_name__icontains=search) | Q(sub_category__sub_category_name__icontains=search))
        if title :
            q_objects.add(Q(title__icontains=title), Q.AND)
        if category_id :
            q_objects.add(Q(category_id=category_id), Q.AND)  
        if category_name :
            q_objects.add(Q(category__category_name__icontains=category_name), Q.AND)       
        if sub_category_id :
            q_objects.add(Q(sub_category_id=sub_category_id), Q.AND)
        if sub_category_name :
            q_objects.add(Q(sub_category__sub_category_name__icontains=sub_category_name), Q.AND) 
        if min_price and max_price :
            gig_ids = GigPrice.objects.filter(price__range=[min_price,max_price],price_type_id=PRICE_TYPE['Basic']).values_list('gig_id',flat=True)
            q_objects.add(Q(id__in=gig_ids), Q.AND) 
        if delivery_time :           
            actual_value = ''.join(delivery_time.split(' ')).split(',')      
            gig_ids = GigSubCategoryPriceDetails.objects.filter(sub_category_price_scope_dtl__actual_value__in=actual_value).values_list('gig_id',flat=True)  
            q_objects.add(Q(id__in=gig_ids), Q.AND)                            
        if search_tags :
            gig_ids = GigSearchTag.objects.filter(search_tags__icontains=search_tags)
            q_objects.add(Q(id__in=gig_ids) , Q.AND) 
        if status_id :
            q_objects.add(Q(gig_status_id=status_id), Q.AND)         
        if seller_lang_id :
            lang_id = ''.join(seller_lang_id.split(' ')).split(',')      
            q_objects.add(Q(created_by__lang_id__in=lang_id), Q.AND)  
        if seller_level :        
            level_ids = ''.join(seller_level.split(' ')).split(',')                
            q_objects.add(Q(created_by__level_id__in=level_ids), Q.AND)   
        if offer_delivery_time :   
            delivery_time_sys = SystemConfiguration.objects.filter(code_name=configs.POST_REQUEST_DELIVERY_TIME_LOGIC)
            _to = int(offer_delivery_time) + int(delivery_time_sys[0].field_value)           
            gig_ids = GigSubCategoryPriceDetails.objects.filter(sub_category_price_scope_dtl__actual_value__in=[offer_delivery_time,_to]).values_list('gig_id',flat=True)  
            q_objects.add(Q(id__in=gig_ids), Q.AND)  
        if offer_budget :           
            budget_sys = SystemConfiguration.objects.filter(code_name=configs.POST_REQUEST_DELIVERY_TIME_LOGIC)
            _from = int(offer_budget)
            _to = _from + int((_from * int(budget_sys[0].field_value))/100)           
            gig_ids = GigPrice.objects.filter(price__range=[_from,_to]).values_list('gig_id',flat=True)  
            q_objects.add(Q(id__in=gig_ids), Q.AND)                           
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        if sort_by :
            if sort_by == '1' :
               pass
            elif sort_by == '2' :
                queryset = queryset.order_by('-rating')
            elif sort_by == '3' :
                queryset = queryset.order_by('-created_date') 
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL        
        if is_active == 'false' :    
            queryset = Gig.all_objects.filter(is_active=False)
        else : 
            queryset = Gig.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset



'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/home_screen/favourite-list')
'''
class GigSearchResultFavouriteListView(ListAPIView) :
    """

    Query parameters for GET method
    --------------------------------------- 

    E.g. http://127.0.0.1:8000/api/v1/gigs_extra/favourite-list   

    """
    serializer_class = GigsSearchResultSerializer
    pagination_class = StandardResultsSetPagination
    

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        gig_ids = UserFavouriteGigs.objects.filter(created_by=self.request.user).values_list('gig_id')
        queryset = Gig.objects.filter(id__in=gig_ids)        
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset.order_by('-created_date')


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/home_screen/best-sellers/')
'''
class BestSellerListView(ListAPIView) :
    """
    
    Query parameters for GET method
    --------------------------------------- 
    1. sub_category_name = Sub Category Name
    2. sub_category_id = ID of Sub Category

    E.g. http://127.0.0.1:8000/api/v1/gigs_extra/best-sellers?sub_category_name=Logo Design   

    """
    serializer_class = GigsSearchResultSerializer
    pagination_class = StandardResultsSetPagination
    

    def get_queryset(self) :
        sub_category_id = self.request.GET.get('sub_category_id')
        sub_category_name =  self.request.GET.get('sub_category_name') # Get query parameter from URL        
        queryset = Gig.objects.all().order_by('-rating')
        if sub_category_id :
            queryset = queryset.filter(sub_category_id=sub_category_id)
        if sub_category_name :
            queryset = queryset.filter(sub_category__sub_category_name=sub_category_name)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/home_screen/top-services/')
'''
class TopServicesListView(ListAPIView) :
    """

    Query parameters for GET method
    ---------------------------------------   

    E.g. http://127.0.0.1:8000/api/v1/gigs_extra/top-services

    """
    serializer_class = GigsSearchResultSerializer
    pagination_class = StandardResultsSetPagination
    

    def get_queryset(self) :                
        queryset = Gig.objects.all().order_by('-rating')        
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/home_screen/taskit-picks/')
'''
class TaskITPicsListView(ListAPIView) :
    """

    Query parameters for GET method
    ---------------------------------------   

    E.g. http://127.0.0.1:8000/api/v1/gigs_extra/taskit-picks

    """
    serializer_class = GigsSearchResultSerializer
    pagination_class = StandardResultsSetPagination
    

    def get_queryset(self) :                
        queryset = Gig.objects.all().order_by('-rating')        
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/home_screen/favourite-list')
'''
class GigBrowsingSearchHistoryListView(ListAPIView) :
    """

    Query parameters for GET method
    --------------------------------------- 

    E.g. http://127.0.0.1:8000/api/v1/gigs_extra/favourite-list   

    """
    serializer_class = GigsSearchResultSerializer
    pagination_class = StandardResultsSetPagination
    

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        gig_ids = GigLastVisited.objects.filter(user=self.request.user).values_list('gig_id')
        queryset = Gig.objects.filter(id__in=gig_ids)        
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset.order_by('-created_date')


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/gig-details/<int:pk>')
'''
class GigsDetailView(RetrieveAPIView) :
    queryset = Gig.objects.all()
    serializer_class = GigsDetailsSerializer
    authentication_classes = []
    permission_classes = []

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active')
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = Gig.all_objects.all().filter(is_active=False)
        else : 
            queryset = Gig.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


'''
View to receive data in specific format for GET method
To get Gig Search Tags List 
(endpoint = '/gig-draft-details/<int:pk>')
'''
class GigsDraftDetailView(RetrieveAPIView) :
    queryset = GigDraft.objects.all()
    serializer_class = GigsDraftDetailsSerializer

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active')
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = GigDraft.all_objects.all().filter(is_active=False)
        else : 
            queryset = GigDraft.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset

'''
View to send data in specific format for GET method
(endpoint = 'gigs_extra/')
'''
class GigsPricingDetailView(ListAPIView) :
    """
    Gig Pricing Details GET API .
   
    """    
    queryset = GigPrice.objects.none()
    serializer_class = GigsPricingDetailsSerializer
    authentication_classes = []
    permission_classes = []


    # Override this function to build custom queryset
    def get_queryset(self) :
        gig_id = self.kwargs['gig_id']
        queryset = GigPrice.objects.filter(gig_id=gig_id)            
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 


'''
View to send data in specific format for GET method
(endpoint = 'gigs_extra/')
'''
class GigsPricingDraftDetailView(ListAPIView) :
    """
    Gig Pricing Draft Details GET API .
   
    """    
    queryset = GigPriceDraft.objects.none()

    serializer_class = GigsDraftPricingDetailsSerializer

    # Override this function to build custom queryset
    def get_queryset(self) :
        gig_draft_id = self.kwargs['gig_draft_id']
        queryset = GigPriceDraft.objects.filter(gig_draft_id=gig_draft_id)      
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send data in specific format for GET method
(endpoint = 'gigs/draft-pricing-details/:gig_draft_id')
'''
class GigsDraftPricingDetailView(ListAPIView) :
    """
    Gig Pricing Details GET API .
   
    """    
    queryset = GigPriceDraft.objects.none()
    serializer_class = GigsDraftPricingDetailsSerializer

    # Override this function to build custom queryset
    def get_queryset(self) :
        gig_draft_id = self.kwargs['gig_draft_id']
        queryset = GigPriceDraft.objects.filter(gig_draft_id=gig_draft_id)   
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 


'''
View to send data in specific format for GET method
(endpoint = '/home_screen/relevant-searches')
'''
class RelevantSearchesView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1.  search = Name OF Category/Sub Categroy/Search Tags

    E.g. http://127.0.0.1:8000/api/v1/home_screen/relevant-searches
    """

    queryset = Category.objects.none()
    serializer_class = RelevantSearchesSerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self) : 
        category = Category.objects.all().annotate(
            relevant_id = F('id'),
            relevant_name = F('category_name'),
            relevant_type = Value(values.category, CharField()),           
        ).only('id', 'category_name')

        sub_category = SubCategory.objects.all().annotate(
            relevant_id = F('id'),
            relevant_name = F('sub_category_name'),
            relevant_type = Value(values.sub_category, CharField()),           
        ).only('id', 'sub_category_name')

        search_tags = GigSearchTag.objects.all().annotate(
            relevant_id = F('id'),
            location_name = F('search_tags'),
            location_type = Value(values.gigs_tag, CharField()),          
        ).only('id', 'search_tags')

        search = self.request.GET.get('search')
        if search :
            category = category.filter(category_name__icontains=search)
            sub_category = sub_category.filter(sub_category_name__icontains=search)
            search_tags = search_tags.filter(search_tags__icontains=search)
        queryset = category.union(sub_category, search_tags)
        return queryset.order_by('relevant_name')



'''
View to send data in specific format for GET method
(endpoint = '/home_screen/popular-services')
'''
class PopularServicesListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------

    1.  sub_category_name = Sub Categroy/Search

    E.g. http://127.0.0.1:8000/api/v1/gigs_extra/popular-services
    """

    queryset = SubCategory.objects.none()
    serializer_class = PopularServicesSerializer
    pagination_class = StandardResultsSetPagination
    authentication_classes = []
    permission_classes = []

    def apply_filters(self,queryset) :
        q_object = Q()
        sub_category_name = self.request.GET.get('sub_category_name')
        if sub_category_name :
            q_objects.add(Q(sub_category_name__icontains=sub_category_name), Q.AND)
        if len(q_object) > 0 :
            queryset = queryset.filter(q_object)    
        return queryset

    def get_queryset(self) : 
        queryset = SubCategory.objects.all()
        queryset = self.apply_filters(queryset)
        return queryset



'''
View to send data in specific format for GET method
(endpoint = '/gigs_extra/seller-offer-list')
'''
class SellerOfferGigListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------

    1.  sub_category_id = Sub Categroy

    E.g. http://127.0.0.1:8000/api/v1/gigs_extra/seller-offer-list?sub_category_id=1
    """

    serializer_class = SellerOfferGigListSerializer
    pagination_class = StandardResultsSetPagination  

    def get_queryset(self) : 
        sub_category_id = self.request.GET.get('sub_category_id')
        if sub_category_id :
            queryset = Gig.objects.filter(sub_category_id=sub_category_id)
        else :
            queryset = Gig.objects.all()    
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset