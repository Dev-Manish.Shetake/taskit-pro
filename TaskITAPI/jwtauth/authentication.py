from django.utils.translation import ugettext_lazy as _
from rest_framework import HTTP_HEADER_ENCODING, authentication
from django.core.exceptions import ObjectDoesNotExist
from jwtauth.exception import AuthenticationFailed, InvalidToken, TokenError
from rest_framework import exceptions
import jwt
from jwtauth.settings import DEFAULTS
from accounts.models import User, UserActiveLoggedIn
from django.core.cache import cache
from datetime import datetime, timedelta
from django.utils import timezone
from django.contrib.auth.models import Group
from TaskITAPI.resources.cache import AUTH_USER_ID,AUTH_USER_ID_TIMEOUT,BLACKLISTED_TOKENS, IS_SYS_CONFIG_CHANGED
from jwtauth.utils import check_multiple_login_allowed, get_session_timeout



AUTH_HEADER_TYPES = ('Bearer', )

if not isinstance(AUTH_HEADER_TYPES, (list, tuple)):
    AUTH_HEADER_TYPES = (AUTH_HEADER_TYPES, )

AUTH_HEADER_TYPE_BYTES = set(
    h.encode(HTTP_HEADER_ENCODING) for h in AUTH_HEADER_TYPES)


# def check_sys_configurations_status(token) :
#     is_sys_config_changed = cache.get(IS_SYS_CONFIG_CHANGED)
#     if is_sys_config_changed :
#         blacklisted_tokens = cache.get(BLACKLISTED_TOKENS)
#         if blacklisted_tokens != None and token not in blacklisted_tokens :
#             blacklisted_tokens.append(token)
#         else :
#             blacklisted_tokens = [token]
#         cache.set(BLACKLISTED_TOKENS, blacklisted_tokens, get_session_timeout())
#         raise AuthenticationFailed(_('Session expired. Please log in again.'), code='session_expired')
# def check_token_in_blacklisted(token) :
#     blacklisted_tokens = cache.get(BLACKLISTED_TOKENS)  
#     if blacklisted_tokens != None and token in blacklisted_tokens :


def get_cached_user(user_id) :
    # check_sys_config_status(user_id)
    user = cache.get(AUTH_USER_ID % str(user_id))
    if (user == None):
        # if not check_multiple_login_allowed() :
        #     raise AuthenticationFailed(_('Session expired. Please log in again.'), code='session_expired')
        try : 
            user = User.objects.get(id=user_id)
            groups = user.groups.all()
            user.group_id = None
            if len(groups) > 0 :
                user.group_id = groups[0].id
            cache.set(AUTH_USER_ID % str(user_id), user, timeout=get_session_timeout())
        except ObjectDoesNotExist :
            raise AuthenticationFailed
    return user


def get_user_id_from_token(validated_token) :
    """
    Attempts to find and return a user using the given validated token.
    """
    try:
        user_id = validated_token['user_id']
    except KeyError:
        raise InvalidToken(
            _('Token contained no recognizable user identification'),code='not_found')
    return user_id


"""
Authentication is done through this class.
Request object will get info about logged in user
"""
class JWTAuthentication(authentication.BaseAuthentication):
    """
    An authentication plugin that authenticates requests through a JSON web
    token provided in a request header.
    """
    www_authenticate_realm = 'api'

    def authenticate_header(self, request):
        return '{0} realm="{1}"'.format(
            AUTH_HEADER_TYPES[0],
            self.www_authenticate_realm,
        )

    def authenticate(self, request):
        header = self.get_header(request)
        if header is None:
            return None

        raw_token = self.get_raw_token(header)
        if raw_token is None:
            return None

        validated_token = self.get_validated_token(raw_token)
        return self.get_user(request, validated_token), validated_token


    def get_header(self, request):
        header = request.META.get('HTTP_AUTHORIZATION')
        if isinstance(header, str):
            # Work around django test client oddness
            header = header.encode(HTTP_HEADER_ENCODING)
        return header


    def get_raw_token(self, header):
        """
        Extracts an unvalidated JSON web token from the given "Authorization"
        header value.
        """
        parts = header.split()
        if len(parts) == 0:
            # Empty AUTHORIZATION header sent
            return None
        if parts[0] not in AUTH_HEADER_TYPE_BYTES:
            # Assume the header does not contain a JSON web token
            return None
        if len(parts) != 2:
            raise AuthenticationFailed(
                _('Authorization header must contain two space-delimited values'
                  ),
                code='bad_authorization_header',
            )
        return parts[1]


    def get_validated_token(self, raw_token):
        """
        Validates an encoded JSON web token and returns a validated token
        wrapper object.
        """
        timeout = timezone.now() - timedelta(seconds=get_session_timeout())
        # if not UserActiveLoggedIn.objects.filter(access_token=raw_token, created_date__gt=timeout).exists() :
        #     UserActiveLoggedIn.objects.filter(access_token=raw_token).delete()
        #     raise exceptions.AuthenticationFailed(detail='Session expired. Please log in again.', code='session_expired')
        try:
            token = jwt.decode(raw_token,
                            DEFAULTS['SIGNING_KEY'],
                            algorithm=DEFAULTS['ALGORITHM'],
                            verify=False)
            UserActiveLoggedIn.objects.filter(access_token=raw_token).update(created_date=timezone.now())
            return token
        except Exception as e:
            raise exceptions.AuthenticationFailed(detail='Invalid user identification.', code='invalid_user_identification')


    def get_user(self, request, validated_token):
        user_id = get_user_id_from_token(validated_token)
        user = get_cached_user(user_id)
        if user == None :
            raise AuthenticationFailed(_('Invalid credentials'),
                                        code='invalid_credentials')
        lang = request.META.get('HTTP_ACCEPT_LANGUAGE')
        if lang != None :
            user.lang = lang
        return user


"""
In case we want both anonymous and logged user to access some APIs this
class will be used 
"""
class AuthOrAnonymousUser(JWTAuthentication):
    def get_user(self, validated_token):
        user_id = get_user_id_from_token(validated_token)
        user = get_cached_user(user_id)
        if user == None :
            from django.contrib.auth.models import AnonymousUser
            user = AnonymousUser()
        return user