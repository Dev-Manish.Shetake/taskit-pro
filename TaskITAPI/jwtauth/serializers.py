from rest_framework import serializers
from rest_framework.exceptions import NotFound
from django.core.exceptions import ObjectDoesNotExist
from jwtauth.exception import AuthenticationFailed
from accounts.models import User
from django.contrib.auth import authenticate
from jwtauth.utils import get_response_payload
from django.utils.translation import ugettext_lazy as _


class TokenObtainSerializer(serializers.Serializer):
    email_address = serializers.EmailField()
    password = serializers.CharField()
    
    def authenticate_user(self, attrs):
        authenticate_kwargs = {
            'email_address': attrs['email_address'].lower(),
            'password': attrs['password']
        }
        try:
            authenticate_kwargs['request'] = self.context['request']
        except KeyError:
            pass

        user_account = None
        try :
            user_account = User.objects.get(email_address=authenticate_kwargs['email_address'])
        except ObjectDoesNotExist :
            raise AuthenticationFailed(_('User with given email address does not exist.'),
                                        code='user_not_found')
        self.user = authenticate(**authenticate_kwargs)
        if self.user is None :
            raise AuthenticationFailed(_('Authentication failed.'),
                                        code='authentication_failed')                                   
        return self.user
         
    def validate(self, attrs):
        self.authenticate_user(attrs)        
        data = get_response_payload(self.user)
        return data
           