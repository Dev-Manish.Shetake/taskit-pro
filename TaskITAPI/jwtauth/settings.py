from datetime import timedelta
from django.conf import settings
# from speed_api.resources.cache import AUTH_USER_ID_TIMEOUT

DEFAULTS = {
    # 'ACCESS_TOKEN_LIFETIME': timedelta(seconds=AUTH_USER_ID_TIMEOUT),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    'ALGORITHM': 'HS256',
    'SIGNING_KEY': settings.SECRET_KEY,
    'VERIFYING_KEY': None,
    'AUTH_HEADER_TYPES': ['Bearer'],
}

OTP_VALID_TIME = timedelta(minutes=10)