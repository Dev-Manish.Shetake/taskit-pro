from django.urls import path
from jwtauth import views


urlpatterns = [
    path('login',views.TokenObtainPairView.as_view(), name='jwt-auth-login'),
    path('log-out',views.SignOutView.as_view(), name='jwt-auth-logout'),
    path('sign-up',views.SignUpView.as_view(),name='email-sign-up'),
    path('sign-up-register/<int:id>',views.SignUpRegisterView.as_view(),name='email-sign-up-register'),
    path('change-password',views.ChangePasswordView.as_view(), name='change-password'),
]