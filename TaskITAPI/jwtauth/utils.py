import pytz
from datetime import datetime, timedelta
from jwtauth.settings import DEFAULTS
import jwt
from django.core.cache import cache
from settings.models import SystemConfiguration
from TaskITAPI.resources.cache import (AUTH_USER_ID, AUTH_USER_ID_TIMEOUT, SESSION_TIME_CONFIG, SESSION_TIME_CONFIG_TIMEOUT,
    MULTIPLE_LOGIN_SWITCH_VALUE, MULTIPLE_LOGIN_SWITCH_TIMEOUT, BLACKLISTED_TOKENS)
from django.contrib.auth.models import Permission
from jwtauth.values import permissions_blacklist
from TaskITAPI.resources.cache import USER_META, USER_META_TIMEOUT, CONFIG_DEFAULT_PERMS, CONFIG_DEFAULT_PERMS_TIMEOUT
from jwtauth.values import permissions_blacklist
from accounts.models import UserActiveLoggedIn
from django.utils import timezone
from settings.utils import get_user_system_configuration

# Generate One Time Password
def generate_otp() :
    from random import randint
    return randint(100000, 999999)

def get_session_timeout() :
    sys_conf = cache.get(SESSION_TIME_CONFIG)
    if sys_conf == None :
        sys_config = SystemConfiguration.objects.filter(code_name='session_timeout')
        cache.set(SESSION_TIME_CONFIG, sys_conf, timeout=SESSION_TIME_CONFIG_TIMEOUT)
    if len(sys_config) == 0 :
        return AUTH_USER_ID_TIMEOUT
    return int(sys_config[0].field_value) * 60


def check_multiple_login_allowed() :
    sys_conf = cache.get(MULTIPLE_LOGIN_SWITCH_VALUE)
    if sys_conf == None :
        sys_conf = SystemConfiguration.objects.filter(code_name = 'multiple_login_switch')
        cache.set(MULTIPLE_LOGIN_SWITCH_VALUE, sys_conf, timeout=MULTIPLE_LOGIN_SWITCH_TIMEOUT)
    if len(sys_conf) == 0 :
        return False 
    return sys_conf[0].field_value == '1'    


# Get permissions of a user
def get_permissions(user) :
    permissions = user.get_all_permissions()
    default_permissions = cache.get(CONFIG_DEFAULT_PERMS)
    if(default_permissions == None):
        default_permissions = Permission.objects.exclude(codename__in=permissions_blacklist) \
            .values_list('codename', flat=True)
        cache.set(CONFIG_DEFAULT_PERMS, default_permissions, timeout=CONFIG_DEFAULT_PERMS_TIMEOUT) 

    user_permissions = dict.fromkeys(default_permissions, False)
    for permission in permissions:
        perm = permission.split('.', 1)[1]
        if perm in permissions_blacklist :
            continue
        user_permissions[perm] = True 
    return user_permissions


# Get access token for logged user
def get_token(user):
    time = datetime.now(pytz.timezone(user.time_zone))
    # timeout = get_session_timeout()
    access_payload = {
        'token_type': 'access',
        'user_id': user.id,
        'exp': time + timedelta(minutes=1)
    }
    refresh_payload = {
        'token_type': 'refresh',
        'user_id': user.id,
        'exp': time + DEFAULTS['REFRESH_TOKEN_LIFETIME'],
    }
    access_token = jwt.encode(access_payload,
                                DEFAULTS['SIGNING_KEY'],
                                algorithm=DEFAULTS['ALGORITHM'])
    refresh_token = jwt.encode(refresh_payload,
                                DEFAULTS['SIGNING_KEY'],
                                algorithm=DEFAULTS['ALGORITHM'])

    tokens = {'access': access_token, 'refresh': refresh_token}
    return tokens


# Get response payload
def get_response_payload(user) :
    data = get_token(user)
    # user_permissions = get_permissions(user)
    group_id = None
    group_name = None
    if user.groups.exists() :
        group_name = user.groups.first().name
        group_id = user.groups.first().id
        user.group_id = group_id
    else :
        user.group_id = 0
      
    logged_in_user = {
        'id': user.id,
        'user_name': user.user_name,
        'email_address': user.email_address,
        'is_verified_email_address' : user.is_verified_email_address,
        'is_verified_mobile_no' : user.is_verified_mobile_no,
        'group_name' : group_name,
        'group_id' : group_id,
        'is_seller' : user.is_seller,
        # 'permissions' : user_permissions
    }
    data['user'] = logged_in_user
    data['sys_config'] = get_user_system_configuration()

    UserActiveLoggedIn.objects.create(
        user = user,
        created_date = timezone.now(),
        access_token = data['access']
    )
    return data
