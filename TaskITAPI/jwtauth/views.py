from django.shortcuts import render
from jwtauth import serializers
from rest_framework.serializers import Serializer, CharField, IntegerField, EmailField
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework import status, filters
from accounts.models import User,UserVerificationCode
from accounts.values import USER_TYPE
from django.utils import timezone
from jwtauth.authentication import AUTH_HEADER_TYPES
import string,random
from TaskITAPI.settings import RESET_LINK,VERIFICATION_LINK
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
from django.contrib.auth.hashers import make_password 
from django.db import transaction
from notifications import events
from notifications.tasks import notify_user
from configs.models import GenLedgerType,GenLedger
from configs.values import LEDGER_TYPE
from accounts.utils import create_user_gen_ledger

class TokenBaseView(GenericAPIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = None

    www_authenticate_realm = 'api'

    def get_authenticate_header(self, request):
        return '{0} realm="{1}"'.format(
            AUTH_HEADER_TYPES[0],
            self.www_authenticate_realm,
        )
        
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)



class TokenObtainPairView(TokenBaseView):
    serializer_class = serializers.TokenObtainSerializer 


class SignOutView(GenericAPIView) :

    class SignOutSerializer(Serializer) :
        user_id = IntegerField()

    serializer_class = SignOutSerializer

    def post(self, request, *args, **kwargs) :  
        # forcefully_log_out_user(request.user.id)          
        # # cache.delete(AUTH_USER_ID % str(request.user.id))
        # cache.delete(USER_META % str(request.user.id))
        response = {
            'code' : 'sign_out_succeed',
            'message' : 'Successfully logged out of the system.'
        }
        return Response(response, status=status.HTTP_200_OK)


class SignUpView(GenericAPIView) :
    class SignUpSerializer(Serializer) :
        email_address = EmailField()

    authentication_classes = []
    permission_classes = []
    serializer_class = SignUpSerializer

    def post(self,request,*args,**kwargs) :
        email_address = request.data['email_address']
        _user = {
            'user_name' : 'TaskIT_USER',
            'email_address' : email_address,
            'user_type_id' : USER_TYPE['buyer'],
            'is_staff' : True,
            'lang_id' : 1,
            'created_by_id' : 1,
            'created_user' : 'System Admin',
            'created_date' : timezone.now()
        }
        user = User.objects.create(**_user)
        group = Group.objects.get(id=2)
        user.groups.add(group)
        user._groups = [group]
        # token = ''.join(random.choices(string.ascii_lowercase +
        #                      string.digits, k = 16)) 
        # UserVerificationCode.objects.create(
        #     user=user,
        #     created_date=timezone.now(),
        #     verification_code=token
        # )
        # notify_user.delay(events.REGISTER_USER, { 'user' : user,  'verification_link' : VERIFICATION_LINK + token })       
        response = {
            'user_id' : user.id,
            'code' : 'email_sent',
            'message' : 'Register link has sent to your email address.'
        }
        return Response(response, status=status.HTTP_200_OK) 




class SignUpRegisterView(GenericAPIView) :

    class SignUpRegisterSerializer(Serializer) :       
        user_name = CharField(max_length=100)
        password = CharField(max_length=50)

    serializer_class = SignUpRegisterSerializer
    queryset = User.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'  

    authentication_classes = []
    permission_classes = []

    def _validate_user_name(self,user_name) :     
        if User.objects.filter(user_name=user_name).exclude(id=self.kwargs['id']).exists() or GenLedger.objects.filter(gen_ledger_name=user_name).exclude(user_id=self.kwargs['id']).exists() :
            raise ValidationError(_('User name should be unique'),code='not_unique')
        return user_name

    @transaction.atomic
    def patch(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)      
        user = self.get_object()
        self._validate_user_name(serializer.validated_data['user_name'])
        user.user_name = serializer.validated_data['user_name']
        user.password = make_password(serializer.validated_data['password'])       
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()
        user.save(update_fields=['user_name','password','modified_by','modified_user','modified_date'])
        create_user_gen_ledger(self,user)
        
        token = ''.join(random.choices(string.ascii_lowercase +
                             string.digits, k = 16)) 
        UserVerificationCode.objects.create(
            user=user,
            created_date=timezone.now(),
            verification_code=token
        )
        notify_user.delay(events.REGISTER_USER, { 'user' : user,  'verification_link' : VERIFICATION_LINK + token })       
        return Response(status=status.HTTP_200_OK)



class ChangePasswordView(GenericAPIView) :
    class ChangePasswordSerializer(Serializer) :
        password = CharField()

    serializer_class = ChangePasswordSerializer

    def put(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        password = serializer.validated_data['password']
        user = request.user
        user.password = make_password(password)
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()  
        user.password_modified_date = timezone.now() 
        user.save(update_fields=['password','modified_by','modified_user','modified_date','password_modified_date'])
        # notify_user.delay(events.RESET_PASSWORD, { 'user' : user,  'reset_link' : RESET_LINK + token })       
        response = {
            "code" : "password_changed",
            "message" : "Password Succesfully changed."
        }
        return Response(response, status=status.HTTP_200_OK) 
