from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User


def upload_category_picture(instance, filename):
    category = Category.objects.last()
    if category :
        count = category.id + 1
    else :
        count = 1 
    return 'Category/CategoryPicture/{0}/{1}_{2}'.format(
        count,  
        instance.category_name.lower().replace(" ", "_"), 
        filename
    )

def upload_category_thumb_picture(instance, filename):
    category = Category.objects.last()
    if category :
        count = category.id + 1
    else :
        count = 1 
    return 'Category/CategoryThumbPicture/{0}/{1}_{2}'.format(
        count,  
        instance.category_name.lower().replace(" ", "_"),   
        filename
    )

# Master MetaDataType related data stored in this table
class Category(SoftDeleteModel) :
    category_name = models.CharField(max_length=100, unique=True)
    category_picture = models.FileField(null=True,upload_to=upload_category_picture)
    category_thumb_picture = models.FileField(null=True,upload_to=upload_category_thumb_picture)
    tagline = models.CharField(max_length=200,null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'category_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='category_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='category_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_category'
        default_permissions = ()
