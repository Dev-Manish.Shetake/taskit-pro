from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User

# Master PriceExtraService related data stored in this table
class PriceExtraService(SoftDeleteModel) :
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    is_price_type_applicable = models.BooleanField(default=False)
    heading_1 = models.CharField(max_length=100,null=True)
    heading_1_master_name = models.CharField(max_length=100,null=True)
    heading_2 = models.CharField(max_length=100,null=True)
    heading_2_master_name = models.CharField(max_length=100,null=True)
    help = models.CharField(max_length=1000,null=True)

    class Meta :
        db_table = 'mst_price_extra_service'
        default_permissions = ()

