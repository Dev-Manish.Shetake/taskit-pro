from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User


# Master MetaDataType related data stored in this table
class MetaDataType(SoftDeleteModel) :
    metadata_type_name = models.CharField(max_length=100, unique=True)
    is_single = models.BooleanField(default=False)
    is_mandatory = models.BooleanField(default=False)
    is_suggest_other = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'metadata_type_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='metadata_type_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='metadata_type_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_metadata_type'
        default_permissions = ()



# Master MetaDataTypeDetails related data stored in this table
class MetaDataTypeDetails(models.Model) :
    metadata_type = models.ForeignKey(MetaDataType,on_delete=models.DO_NOTHING)
    seq_no = models.IntegerField()
    metadata_type_name = models.CharField(max_length=100)
    
    class Meta :
        db_table = 'mst_metadata_type_dtl'
        default_permissions = ()

   