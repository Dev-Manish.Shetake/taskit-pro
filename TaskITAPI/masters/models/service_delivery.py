from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User


# Master ServiceDelivery related data stored in this table
class ServiceDelivery(SoftDeleteModel) :
    seq_no = models.IntegerField()
    service_delivery_name = models.CharField(max_length=100, unique=True)
    service_delivery_value = models.IntegerField()    
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'service_delivery_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='service_delivery_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='service_delivery_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_service_delivery'
        default_permissions = ()
