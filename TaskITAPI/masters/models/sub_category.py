from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from masters.models import Category,MetaDataType,MetaDataTypeDetails
from configs.models import PriceScopeControlType


def upload_sub_category_picture(instance, filename):
    sub_category = SubCategory.objects.last()
    if sub_category :
        count = sub_category.id + 1
    else :
        count = 1 
    return 'Sub_Category/SubCategoryPicture/{0}/{1}_{2}'.format(
        count,
        instance.sub_category_name.lower().replace(" ", "_"),    
        filename
    )


def upload_sub_category_thumb_picture(instance, filename):
    sub_category = SubCategory.objects.last()
    if sub_category :
        count = sub_category.id + 1
    else :
        count = 1 
    return 'category/SubCategoryThumbPicture/{0}/{1}_{2}'.format(
        count,
        instance.sub_category_name.lower().replace(" ", "_"),  
        filename
    )    

# Master SubCategory related data stored in this table
class SubCategory(SoftDeleteModel) :
    category = models.ForeignKey(Category,on_delete=models.DO_NOTHING)
    seq_no = models.IntegerField(null=True)
    sub_category_name = models.CharField(max_length=100)
    sub_category_picture = models.FileField(null=True,upload_to=upload_sub_category_picture)
    sub_category_thumb_picture = models.FileField(null=True,upload_to=upload_sub_category_thumb_picture)
    tagline = models.CharField(max_length=200,null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'sub_category_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='sub_category_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='sub_category_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_sub_category'
        default_permissions = ()



# Master SubCategoryMetaDataType related data stored in this table
class SubCategoryMetaDataType(SoftDeleteModel) :
    sub_category = models.ForeignKey(SubCategory,on_delete=models.DO_NOTHING)
    seq_no = models.IntegerField()
    metadata_type = models.ForeignKey(MetaDataType,on_delete=models.DO_NOTHING)
    is_single =  models.BooleanField(null=True)
    is_mandatory = models.BooleanField(null=True)
    min_value = models.IntegerField(null=True)
    max_value = models.IntegerField(null=True)
    is_suggest_other = models.BooleanField(null=True)
    is_allowed_on_request = models.BooleanField(null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'sub_category_metadata_type_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='sub_category_metadata_type_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='sub_category_metadata_type_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_sub_category_metadata_type'
        default_permissions = ()


# Master SubCategoryMetaDataTypeDetails related data stored in this table
class SubCategoryMetaDataTypeDetails(models.Model) :
    sub_category_metadata_type = models.ForeignKey(SubCategoryMetaDataType,on_delete=models.DO_NOTHING)
    seq_no = models.IntegerField()
    metadata_type_dtl = models.ForeignKey(MetaDataTypeDetails,on_delete=models.DO_NOTHING)

    class Meta :
        db_table = 'mst_sub_category_metadata_type_dtl'
        default_permissions = ()



# Master SubCategoryPriceScope related data stored in this table
class SubCategoryPriceScope(SoftDeleteModel) :
    sub_category = models.ForeignKey(SubCategory,on_delete=models.DO_NOTHING)
    seq_no = models.IntegerField()
    price_scope_name = models.CharField(max_length=100)
    is_heading_visible =  models.BooleanField(default=True)
    price_scope_control_type =  models.ForeignKey(PriceScopeControlType,on_delete=models.DO_NOTHING)
    watermark = models.CharField(max_length=100,null=True)
    default_value = models.CharField(max_length=100,null=True)
    is_mandatory = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'sub_category_price_scope_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='sub_category_price_scope_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='sub_category_price_scope_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'mst_sub_category_price_scope' 
        default_permissions = ()


# Master SubCategoryPriceScopeDetails related data stored in this table
class SubCategoryPriceScopeDetails(models.Model) :
    sub_category_price_scope = models.ForeignKey(SubCategoryPriceScope,on_delete=models.DO_NOTHING)
    seq_no = models.IntegerField()
    value = models.CharField(max_length=200)
    display_value = models.CharField(max_length=200,null=True)
    actual_value = models.IntegerField(null=True)
    class Meta :
        db_table = 'mst_sub_category_price_scope_dtl'
        default_permissions = ()