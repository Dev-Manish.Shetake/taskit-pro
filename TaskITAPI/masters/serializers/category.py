from rest_framework import serializers
from masters.models import Category,SubCategory
from django.db import transaction
from django.db.models import Prefetch,OuterRef,Subquery
from gigs.models import Gig
from django.utils import timezone


class CategoryMinSerializer(serializers.ModelSerializer) :

    class Meta :
        model = Category
        fields = [
            'id',
            'category_name',
            'is_active',
        ]  

class CategoryMacroSerializer(serializers.ModelSerializer) :

    class Meta :
        model = Category
        fields = [
            'id',
            'category_name',
        ]  

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/categories/list)
'''
class CategoryListSerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = Category
        fields = [
            'id',
            'category_name',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/categories/gigs-count-list)
'''
class CategoryGigsCountListSerializer(serializers.ModelSerializer) :
    gigs_count = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) :       
        return queryset.order_by('category_name')

    class Meta :
        model = Category
        fields = [
            'id',
            'category_name',
            'tagline',
            'gigs_count',
            'is_active',
            'created_date',
            'modified_date'
        ]           

    def get_gigs_count(self,instance) :     
        gigs = Gig.objects.filter(category_id=instance.id)
        count = gigs.count()
        return count

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/categories/)
'''
class CategorySerializer(serializers.ModelSerializer) :
    category_picture = serializers.FileField(required=False,allow_null=True)
    category_thumb_picture = serializers.FileField(required=False,allow_null=True)

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = Category
        fields = [
            'id',
            'category_name',
            'category_picture',
            'category_thumb_picture',
            'tagline',
            'is_active',
            'created_by',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_by','created_date','modified_date']

  
    def create(self,validated_data) :
        category = Category.all_objects.filter(
            category_name = validated_data['category_name'],
            is_active = False
        )
        if len(category) > 0 :
            category = category[0]
            category.is_active = True
            category.modified_by = self.context['request'].user
            category.modified_user = self.context['request'].user.user_name
            category.modified_date = timezone.now()
            category.deleted_by = None
            category.deleted_user = None
            category.deleted_date = None
        else : 
            category = Category(**validated_data)
            category.created_by = self.context['request'].user
            category.created_user = self.context['request'].user.user_name
        category.save()
        return category


'''
Serializer to receive/delete data in specific format for PUT/PATCH/DELETE method
(endpoint = '/categories/:id')
'''
class CategoryDetailSerializer(serializers.ModelSerializer):
    category_picture = serializers.FileField(required=False,allow_null=True)
    category_thumb_picture = serializers.FileField(required=False,allow_null=True)

    def setup_eager_loading(self, queryset) :
        return queryset

    class Meta:
        model = Category
        fields = [
            'id',
            'category_name',
            'category_picture',
            'category_thumb_picture',
            'tagline',
            'created_by',
            'created_date',            
            'created_user',
            'modified_date',
            'modified_user',
            'modified_by',
            'deleted_date',
            'deleted_user',
            'deleted_by',
            'is_active'
        ]
        read_only_fields = [
            'created_date','created_user','created_by', 'modified_date','modified_user',
            'modified_by', 'deleted_date', 'deleted_user','deleted_by', 'is_active']
       

    def update(self,category,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(category, attr, value)
            update_fields.append(attr)
        category.modified_by = self.context['request'].user
        category.modified_user = self.context['request'].user.user_name
        category.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        category.save(update_fields=update_fields)
        return category  


'''
Serializer to receive/delete data in specific format for PUT/PATCH/DELETE method
(endpoint = '/activities/:id')
'''
class CategoryDeleteSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Category
        fields = [
            'id'
        ]

class SubCategoryMinSerializer(serializers.ModelSerializer) :

    class Meta :
        model = SubCategory
        fields = [
            'id',
            'sub_category_name',
            'is_active',
        ] 

class SubCategoryMacroSerializer(serializers.ModelSerializer) :

    class Meta :
        model = SubCategory
        fields = [
            'id',
            'sub_category_name',
        ] 

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/category-subcategory-list/)
'''
class CategorySubCategoryListSerializer(serializers.ModelSerializer) :
    sub_category = SubCategoryMinSerializer(many=True)
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('subcategory_set', queryset=SubCategory.objects.all(), to_attr = 'sub_category')
        )
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = Category
        fields = [
            'id',
            'category_name',
            'sub_category',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']
