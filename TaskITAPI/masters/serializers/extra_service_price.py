from rest_framework import serializers
from masters.models import PriceExtraService
from configs.models import ExtraDays,ExtraPrice
from django.db import transaction
from django.db.models import Prefetch
from configs.serializers import ExtraDaysSerializer,ExtraPriceSerializer


'''
Serializer to receive data in specific format for GET method
(endpoint = '/extra_services/)
'''
class ExtraPriceServiceMinSerializer(serializers.ModelSerializer) :   

    def setup_eager_loading(self,queryset) :
        return queryset

    class Meta :
        model = PriceExtraService
        fields = [
            'id',
            'title',
            'description',
            'is_price_type_applicable',
            'heading_1',
            'heading_1_master_name',
            'heading_2',
            'heading_2_master_name',
            'help',            
            'is_active'
        ]  


'''
Serializer to receive data in specific format for GET method
(endpoint = '/extra_services/)
'''
class ExtraPriceServiceSerializer(serializers.ModelSerializer) :
    extra_days = serializers.SerializerMethodField()
    extra_price = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) :
        return queryset

    class Meta :
        model = PriceExtraService
        fields = [
            'id',
            'title',
            'description',
            'is_price_type_applicable',
            'heading_1',
            'heading_1_master_name',
            'heading_2',
            'heading_2_master_name',
            'help',
            'extra_days',
            'extra_price',
            'is_active'
        ]    

    def get_extra_days(self,instance) :
        days = []
        extra_days = ExtraDays.objects.all()
        for ext_day in extra_days :
            extra_day = ExtraDaysSerializer(ext_day).data
            days.append(extra_day)
        return days    

    def get_extra_price(self,instance) :
        prices = []
        extra_prices = ExtraPrice.objects.all()
        for ext_price in extra_prices :
            extra_price = ExtraPriceSerializer(ext_price).data
            prices.append(extra_price)
        return prices    
      