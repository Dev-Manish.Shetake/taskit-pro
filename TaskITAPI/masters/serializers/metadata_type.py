from rest_framework import serializers
from masters.models import MetaDataType,MetaDataTypeDetails
from django.db import transaction
from django.db.models import Prefetch
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _ 
from django.utils import timezone

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/)
'''
class MetaDataTypeSerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')
   
    def validate_metadata_type_name(self,value) :
        if MetaDataType.objects.filter(metadata_type_name=value).exists() :
            raise ValidationError(_('Metadata Type name should be unique.'),code='not_unique')
        return value
    class Meta :
        model = MetaDataType
        fields = [
            'id',
            'metadata_type_name',
            'is_single',
            'is_mandatory',
            'is_suggest_other',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']

    @transaction.atomic
    def create(self,validated_data) :
        metadata_type = MetaDataType.all_objects.filter(
            metadata_type_name = validated_data['metadata_type_name'],
            is_active = False
        )
        if len(metadata_type) > 0 :
            metadata_type = category[0]
            metadata_type.is_active = True
            metadata_type.modified_by = self.context['request'].user
            metadata_type.modified_user = self.context['request'].user.user_name
            metadata_type.modified_date = timezone.now()
            metadata_type.deleted_by = None
            metadata_type.deleted_user = None
            metadata_type.deleted_date = None
        else : 
            metadata_type = MetaDataType.objects.create(
                **validated_data,
                created_by = self.context['request'].user,
                created_user = self.context['request'].user.user_name,            
            )
        return metadata_type    


'''
Serializer to receive/delete data in specific format for PUT/PATCH/DELETE method
(endpoint = '/metadata_type/:id')
'''
class MetaDataTypeUpdateSerializer(serializers.ModelSerializer):
   
    def setup_eager_loading(self, queryset) :
        return queryset

    class Meta:
        model = MetaDataType
        fields = [
            'id',
            'metadata_type_name',
            'is_single',
            'is_mandatory',
            'is_suggest_other',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']


    def update(self,metadata_type,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(metadata_type, attr, value)
            update_fields.append(attr)
        metadata_type.modified_by = self.context['request'].user
        metadata_type.modified_user = self.context['request'].user.user_name
        metadata_type.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        metadata_type.save(update_fields=update_fields)
        return metadata_type  

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/details)
'''
class MetaDataTypeDetailsSerializer(serializers.ModelSerializer) :
    metadata_type = MetaDataTypeSerializer()

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('seq_no')

    class Meta :
        model = MetaDataTypeDetails
        fields = [
            'id',
            'metadata_type',
            'seq_no',
            'metadata_type_name'                   
        ]    
        read_only_fields = ['created_date','modified_date']

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/details-create')
'''
class MetaDataTypeDetailsMinSerializer(serializers.ModelSerializer) :

    class Meta :
        model = MetaDataTypeDetails
        fields = [
            'id',
            'seq_no',
            'metadata_type_name',                     
        ]  

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/details-update')
'''
class MetaDataTypeDetailsMinUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    class Meta :
        model = MetaDataTypeDetails
        fields = [
            'id',
            'seq_no',
            'metadata_type_name',                     
        ] 

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/details-create')
'''
class MetaDataTypeDetailsCreateSerializer(serializers.Serializer) :
    metadata_type_id = serializers.IntegerField()
    details = MetaDataTypeDetailsMinSerializer(many=True)

    def validate_metadata_type_id(self,value) :
        if not MetaDataType.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for metadata type id.'),code='invalid_input')
        return value
    
'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/details-create')
'''
class MetaDataTypeDetailsUpdateSerializer(serializers.Serializer) :
    metadata_type_id = serializers.IntegerField()
    details = MetaDataTypeDetailsMinUpdateSerializer(many=True)

    def validate_metadata_type_id(self,value) :
        if not MetaDataType.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for metadata type id.'),code='invalid_input')
        return value

class MetaDataTypeDetailsListSerializer(serializers.ModelSerializer) :   

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = MetaDataTypeDetails
        fields = [
            'id',         
            'seq_no',
            'metadata_type_name',
        ]    
        read_only_fields = ['created_date','modified_date']        