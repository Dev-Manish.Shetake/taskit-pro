from rest_framework import serializers
from masters.models import RequestStatus
from django.db import transaction
from django.utils import timezone

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/request_status/)
'''
class RequestStatusListSerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = RequestStatus
        fields = [
            'id',
            'request_status_name',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']



class RequestStatusMinSerializer(serializers.ModelSerializer) :
    
    class Meta :
        model = RequestStatus
        fields = [
            'id',
            'request_status_name'            
        ]    
