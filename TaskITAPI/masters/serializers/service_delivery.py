from rest_framework import serializers
from masters.models import ServiceDelivery
from django.db import transaction
from django.db.models import Prefetch



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/categories/)
'''
class ServiceDeliverySerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = ServiceDelivery
        fields = [
            'id',
            'seq_no',
            'service_delivery_name',
            'service_delivery_value',
            'created_date',
            'modified_date'
        ]           


class ServiceDeliveryMinSerializer(serializers.ModelSerializer) :
  
    class Meta :
        model = ServiceDelivery
        fields = [
            'id',            
            'service_delivery_name',
            'service_delivery_value',           
        ]         