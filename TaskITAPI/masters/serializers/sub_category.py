from rest_framework import serializers
from masters.models import SubCategory,SubCategoryMetaDataType, SubCategoryMetaDataTypeDetails, SubCategoryPriceScope, SubCategoryPriceScopeDetails
from configs.models.price_type import PriceScopeControlType
from django.db import transaction
from masters.serializers.category import CategoryMinSerializer
from masters.serializers.metadata_type import MetaDataTypeSerializer,MetaDataTypeDetailsSerializer,MetaDataTypeDetailsListSerializer
from configs.serializers import PriceScopeControlTypeSerializer
from django.utils import timezone
from django.db.models import Prefetch
from configs.serializers import PriceScopeControlTypeMinSerializer

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/list')
'''
class SubCategoryListSerializer(serializers.ModelSerializer) :
    category_id = serializers.IntegerField(write_only=True)
    category = CategoryMinSerializer()

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = SubCategory
        fields = [
            'id',
            'category_id',
            'category',
            'sub_category_name',
            'sub_category_picture',
            'sub_category_thumb_picture',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date','category']


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/)
'''
class SubCategorySerializer(serializers.ModelSerializer) :
    category_id = serializers.IntegerField(write_only=True)
    category = CategoryMinSerializer(read_only=True)
    seq_no = serializers.IntegerField(allow_null=True,required=False)
    sub_category_picture = serializers.FileField(required=False,allow_null=True)
    sub_category_thumb_picture = serializers.FileField(required=False,allow_null=True)

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = SubCategory
        fields = [
            'id',
            'category_id',           
            'category',
            'seq_no',
            'sub_category_name',
            'sub_category_picture',
            'sub_category_thumb_picture',
            'tagline',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date','category']

   
    def create(self,validated_data) :
        sub_category = SubCategory.all_objects.filter(
            sub_category_name = validated_data['sub_category_name'],
            is_active = False
        )
        if len(sub_category) > 0 :
            sub_category = sub_category[0]
            sub_category.is_active = True
            sub_category.modified_by = self.context['request'].user
            sub_category.modified_user = self.context['request'].user.user_name
            sub_category.modified_date = timezone.now()
            sub_category.deleted_by = None
            sub_category.deleted_user = None
            sub_category.deleted_date = None
        else : 
            sub_category = SubCategory(**validated_data)
            sub_category.created_by = self.context['request'].user
            sub_category.created_user = self.context['request'].user.user_name
        sub_category.save()
        return sub_category


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/list')
'''
class SubCategoryMinListSerializer(serializers.ModelSerializer) :
    category = CategoryMinSerializer()

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = SubCategory
        fields = [
            'id',
            'category',
            'sub_category_name'           
        ]    


'''
Serializer to receive/delete data in specific format for PUT/PATCH/DELETE method
(endpoint = '/subactivities/:id')
'''
class SubCategoryDetailSerializer(serializers.ModelSerializer):
    category_id = serializers.IntegerField(write_only=True)
    category = CategoryMinSerializer(read_only=True)
    sub_category_picture = serializers.FileField(required=False,allow_null=True)
    sub_category_thumb_picture = serializers.FileField(required=False,allow_null=True)

    def setup_eager_loading(self, queryset) :
        return queryset

    class Meta:
        model = SubCategory
        fields = [
            'id',
            'category_id',           
            'category',
            'seq_no',
            'sub_category_name',
            'sub_category_picture',
            'sub_category_thumb_picture',
            'tagline',
            'is_active',
            'created_date',  
            'created_by',         
            'created_user',
            'modified_date',
            'modified_user',
            'modified_by',
            'deleted_date',
            'deleted_user',
            'deleted_by',
            'is_active'
        ]
        read_only_fields = [
            'created_date','created_by','created_user','created_by', 'modified_date','modified_user',
            'modified_by', 'deleted_date', 'deleted_user','deleted_by', 'is_active']
       

    def update(self,subcategory,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(subcategory, attr, value)
            update_fields.append(attr)
        subcategory.modified_by = self.context['request'].user
        subcategory.modified_user = self.context['request'].user.user_name
        subcategory.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        subcategory.save(update_fields=update_fields)
        return subcategory  



'''
Serializer to receive/delete data in specific format for PUT/PATCH/DELETE method
(endpoint = '/subactivities/:id')
'''
class SubCategoryDeleteSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = SubCategory
        fields = [
            'id'
        ]



class SubCategoryMetaDataTypeDetailsListSerializer(serializers.ModelSerializer) :
    metadata_type_dtl = MetaDataTypeDetailsListSerializer(read_only=True)

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('seq_no')

    class Meta :
        model = SubCategoryMetaDataTypeDetails
        fields = [
            'id',   
            'seq_no',
            'metadata_type_dtl',           
        ]    
        read_only_fields = ['sub_category_metadata_type','seq_no','metadata_type_dtl']



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/metadata-type-details-list')
'''
class SubCategoryMetaDataTypeDetailsMasterSerializer(serializers.ModelSerializer) :
    metadata_type_dtl = MetaDataTypeDetailsSerializer(read_only=True)

    def setup_eager_loading(self,queryset) :
        return queryset.order_by('seq_no')

    class Meta :
        model = SubCategoryMetaDataTypeDetails
        fields = [
            'id',            
            'sub_category_metadata_type_id',
            'seq_no',
            'metadata_type_dtl',           
        ]    
        read_only_fields = ['sub_category_metadata_type_id','seq_no','metadata_type_dtl']



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/metadata-type-list')
'''
class SubCategoryMetaDataTypeListMasterSerializer(serializers.ModelSerializer) :
    sub_category = SubCategorySerializer(read_only=True)
    metadata_type = MetaDataTypeSerializer(read_only=True)
    metadata_type_details = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :        
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = SubCategoryMetaDataType
        fields = [
            'id',
            'sub_category',
            'seq_no',
            'metadata_type',
            'metadata_type_details',
            'is_single',
            'is_mandatory',
            'min_value',
            'max_value',
            'is_suggest_other',
            'is_allowed_on_request',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']


    def get_metadata_type_details(self,instance) :
        details = []
        metadata_type_dtl = SubCategoryMetaDataTypeDetails.objects.filter(sub_category_metadata_type=instance.id).order_by('seq_no')
        for metadata_type in metadata_type_dtl :
            dtl = SubCategoryMetaDataTypeDetailsListSerializer(metadata_type).data
            details.append(dtl)
        return details


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/price-scope-details')
'''
class SubCategoryPriceScopeDetailsSerializer(serializers.ModelSerializer) :    
   
    def setup_eager_loading(self,queryset) :
        return queryset.order_by('seq_no')

    class Meta :
        model = SubCategoryPriceScopeDetails
        fields = [
            'id',            
            'seq_no',
            'value',
            'display_value',
            'actual_value'
        ]    
       


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/price-scope')
'''
class SubCategoryPriceScopeMasterSerializer(serializers.ModelSerializer) :
    # sub_category = SubCategorySerializer(read_only=True)
    price_scope_control_type = PriceScopeControlTypeMinSerializer()
    sub_category_price_scope_details = SubCategoryPriceScopeDetailsSerializer(many=True)
   
    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('subcategorypricescopedetails_set',SubCategoryPriceScopeDetails.objects.all(),to_attr='sub_category_price_scope_details'),
        )
        return queryset.order_by('seq_no')

    class Meta :
        model = SubCategoryPriceScope
        fields = [
            'id',
            # 'sub_category',
            'seq_no',
            'price_scope_name',
            'is_heading_visible',         
            'price_scope_control_type',
            'watermark',
            'default_value',
            'sub_category_price_scope_details',
            'is_mandatory',
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']


 
'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/price-scope')
'''
class SubCategoryPriceScopeDetailsMasterSerializer(serializers.ModelSerializer) :
    # sub_category_price_scope = 
   
    def setup_eager_loading(self,queryset) :       
        return queryset
    class Meta :
        model = SubCategoryPriceScopeDetails
        fields = [
            'id',
            # 'sub_category_price_scope',
            'seq_no',
            'value',
            'display_value',
            'actual_value'
           
        ]    


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/price-scope')
'''
class SubCategoryPriceScopeMinSerializer(serializers.ModelSerializer) :
    price_scope_control_type = PriceScopeControlTypeMinSerializer()

    class Meta :
        model = SubCategoryPriceScope
        fields = [
            'id',
            'seq_no',
            'price_scope_name',
            'is_heading_visible',
            'watermark',
            'price_scope_control_type'
        ]