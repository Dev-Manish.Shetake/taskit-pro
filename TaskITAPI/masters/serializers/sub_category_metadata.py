from rest_framework import serializers
from masters.models import SubCategoryMetaDataType,SubCategoryMetaDataTypeDetails,MetaDataType,SubCategory
from masters.serializers.metadata_type import MetaDataTypeSerializer,MetaDataTypeDetailsListSerializer
from masters.serializers import CategoryMinSerializer,SubCategoryMinListSerializer
from django.utils import timezone
from django.db import transaction
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _ 
from django.db.models import Prefetch,OuterRef,Subquery

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/list')
'''
class SubCategoryMetaDataTypeDetailsCreateSerializer(serializers.ModelSerializer) :
    metadata_type_dtl_id = serializers.IntegerField()

    class Meta :
        model = SubCategoryMetaDataTypeDetails
        fields = [
            'id',           
            'seq_no',
            'metadata_type_dtl_id' 
        ]    

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/details')
'''
class SubCategoryMetaDataTypeDetailsUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    metadata_type_dtl_id = serializers.IntegerField(write_only=True)
    metadata_type_dtl = MetaDataTypeDetailsListSerializer(read_only=True)
    class Meta :
        model = SubCategoryMetaDataTypeDetails
        fields = [
            'id',           
            'seq_no',
            'metadata_type_dtl',
            'metadata_type_dtl_id' 
        ]   


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/')
'''
class SubCategoryMetaDataTypeSerializer(serializers.ModelSerializer) :
    # sub_category = SubCategoryMinListSerializer(read_only=True)
    metadata_type = MetaDataTypeSerializer(read_only=True)
   
    def setup_eager_loading(self,queryset) :        
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = SubCategoryMetaDataType
        fields = [
            'id',
            # 'sub_category',
            'seq_no',
            'metadata_type',           
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/')
'''
class SubCategoryMetaDataTypeListSerializer(serializers.ModelSerializer) :
    category = CategoryMinSerializer()
    sub_category_metadata_type = SubCategoryMetaDataTypeSerializer(many=True)
    updated_date = serializers.SerializerMethodField()
    

    def setup_eager_loading(self,queryset) : 
        queryset = queryset.prefetch_related(
            Prefetch('subcategorymetadatatype_set',SubCategoryMetaDataType.objects.all(),to_attr='sub_category_metadata_type')
        )       
        sub_category_metadata = SubCategoryMetaDataType.objects.filter(            
            sub_category_id=OuterRef('pk')
        ).order_by('-id')
        queryset = queryset.annotate(updated_date=Subquery(sub_category_metadata.values('created_date')[:1]))
        return queryset.order_by('-updated_date')

    class Meta :
        model = SubCategory
        fields = [
            'id',
            'category',
            'sub_category_name',
            'sub_category_metadata_type',
            'updated_date'
        ]

    def get_updated_date(self,instance) :        
        return instance.updated_date

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/create')
'''
class SubCategoryMetaDataTypeCustomCreateSerializer(serializers.ModelSerializer) :
    sub_category_id = serializers.IntegerField()
    metadata_type_id = serializers.IntegerField()
    details = SubCategoryMetaDataTypeDetailsCreateSerializer(required=False,many=True)

    def validate_sub_category_id(self,value) :
        if not SubCategory.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for sub category id.'),code='invalid_input')
        return value

    def validate_metadata_type_id(self,value) :
        if not MetaDataType.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for metadata type id.'),code='invalid_input')
        return value

    class Meta :
        model = SubCategoryMetaDataType
        fields = [
            'id',
            'sub_category_id',
            'seq_no',
            'metadata_type_id', 
            'is_single',
            'is_mandatory',
            # 'min_value',
            # 'max_value',
            'is_suggest_other',
            # 'is_allowed_on_request',
            'details'
        ]    


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/create')
'''
class SubCategoryMetaDataTypeCustomUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    sub_category_id = serializers.IntegerField()
    metadata_type_id = serializers.IntegerField()
    details = SubCategoryMetaDataTypeDetailsUpdateSerializer(required=False,many=True)

    def validate_sub_category_id(self,value) :
        if not SubCategory.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for sub category id.'),code='invalid_input')
        return value

    def validate_metadata_type_id(self,value) :
        if not MetaDataType.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for metadata type id.'),code='invalid_input')
        return value

    class Meta :
        model = SubCategoryMetaDataType
        fields = [
            'id',
            'sub_category_id',
            'seq_no',
            'metadata_type_id', 
            'is_single',
            'is_mandatory',
            # 'min_value',
            # 'max_value',
            'is_suggest_other',
            # 'is_allowed_on_request',
            'details'
        ]            

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/create')
'''
class SubCategoryMetaDataTypeCreateSerializer(serializers.Serializer) :
    sub_category_metadata_type = SubCategoryMetaDataTypeCustomCreateSerializer(many=True)

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/update')
'''
class SubCategoryMetaDataTypeUpdateSerializer(serializers.Serializer) :
    sub_category_metadata_type = SubCategoryMetaDataTypeCustomUpdateSerializer(many=True)

