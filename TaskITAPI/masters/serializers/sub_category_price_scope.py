from rest_framework import serializers
from masters.models import SubCategory,SubCategoryPriceScope,SubCategoryPriceScopeDetails
from configs.models import PriceScopeControlType
from configs.serializers import PriceScopeControlTypeMinSerializer
from masters.serializers import CategoryMinSerializer
from django.utils import timezone
from django.db import transaction
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _ 
from django.db.models import Prefetch,OuterRef,Subquery


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/create')
'''
class SubCategoryPriceScopeDetailsCreateSerializer(serializers.ModelSerializer) :

    class Meta :
        model = SubCategoryPriceScopeDetails
        fields = [
            'id',           
            'seq_no',
            'value',
            'display_value',
            'actual_value' 
        ]   



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/create')
'''
class SubCategoryPriceScopeDetailsUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    class Meta :
        model = SubCategoryPriceScopeDetails
        fields = [
            'id',           
            'seq_no',
            'value',
            'display_value',
            'actual_value' 
        ] 

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/list')
'''
class SubCategoryPriceScopeSerializer(serializers.ModelSerializer) :
    price_scope_control_type = PriceScopeControlTypeMinSerializer(read_only=True)
   
    def setup_eager_loading(self,queryset) :        
        return queryset.order_by('-modified_date','-created_date')

    class Meta :
        model = SubCategoryPriceScope
        fields = [
            'id',
            'seq_no',
            'price_scope_name',
            'price_scope_control_type',           
            'is_active',
            'created_date',
            'modified_date'
        ]    
        read_only_fields = ['is_active','created_date','modified_date']


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/list')
'''
class SubCategoryPriceScopeListSerializer(serializers.ModelSerializer) :
    category = CategoryMinSerializer()
    sub_category_price_scope = SubCategoryPriceScopeSerializer(many=True)
    updated_date = serializers.SerializerMethodField()    

    def setup_eager_loading(self,queryset) : 
        queryset = queryset.prefetch_related(
            Prefetch('subcategorypricescope_set',SubCategoryPriceScope.objects.all(),to_attr='sub_category_price_scope')
        )       
        sub_category_price_scope = SubCategoryPriceScope.objects.filter(            
            sub_category_id=OuterRef('pk')
        ).order_by('-id')
        queryset = queryset.annotate(updated_date=Subquery(sub_category_price_scope.values('created_date')[:1]))
        return queryset.order_by('-updated_date')

    class Meta :
        model = SubCategory
        fields = [
            'id',
            'category',
            'sub_category_name',
            'sub_category_price_scope',
            'updated_date'
        ]

    def get_updated_date(self,instance) :        
        return instance.updated_date


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/create')
'''
class SubCategoryPriceScopeCustomCreateSerializer(serializers.ModelSerializer) :
    sub_category_id = serializers.IntegerField()
    price_scope_control_type_id = serializers.IntegerField()
    details = SubCategoryPriceScopeDetailsCreateSerializer(required=False,many=True)

    def validate_sub_category_id(self,value) :
        if not SubCategory.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for sub category id.'),code='invalid_input')
        return value
    
    def validate_price_scope_control_type_id(self,value) :
        if not PriceScopeControlType.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for sub category id.'),code='invalid_input')
        return value

    class Meta :
        model = SubCategoryPriceScope
        fields = [
            'id',
            'sub_category_id',
            'seq_no',
            'price_scope_name', 
            'price_scope_control_type_id',
            'watermark',
            # 'default_value',
            'is_mandatory',
            'details'
        ]    


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/create')
'''
class SubCategoryPriceScopeCustomUpdateSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    sub_category_id = serializers.IntegerField()
    price_scope_control_type_id = serializers.IntegerField()
    watermark = serializers.CharField(max_length=100,allow_null=True,required=False)
    details = SubCategoryPriceScopeDetailsUpdateSerializer(required=False,many=True)

    def validate_sub_category_id(self,value) :
        if not SubCategory.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for sub category id.'),code='invalid_input')
        return value
    
    def validate_price_scope_control_type_id(self,value) :
        if not PriceScopeControlType.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for sub category id.'),code='invalid_input')
        return value

    class Meta :
        model = SubCategoryPriceScope
        fields = [
            'id',
            'sub_category_id',
            'seq_no',
            'price_scope_name', 
            'price_scope_control_type_id',
            'watermark',
            # 'default_value',           
            'is_mandatory',
            'details'
        ]    


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/create')
'''
class SubCategoryPriceScopeCreateSerializer(serializers.Serializer) :
    sub_category_price_scope = SubCategoryPriceScopeCustomCreateSerializer(many=True)


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/update')
'''
class SubCategoryPriceScopeUpdateSerializer(serializers.Serializer) :
    sub_category_price_scope = SubCategoryPriceScopeCustomUpdateSerializer(many=True)
