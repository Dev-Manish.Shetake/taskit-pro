from django.urls import path
from masters.views import CategoryListView,CategoryListCreateView,CategoryDetailView,CategoryDestroyView,CategorySubCategoryListView,\
    CategoryGigsCountListView

urlpatterns =[
    path('list',CategoryListView.as_view()),
    path('',CategoryListCreateView.as_view()),
    path('<int:pk>',CategoryDetailView.as_view()),   
    path('delete',CategoryDestroyView.as_view()),
    path('subcategory-list',CategorySubCategoryListView.as_view()),
    path('gigs-count-list',CategoryGigsCountListView.as_view()),
]