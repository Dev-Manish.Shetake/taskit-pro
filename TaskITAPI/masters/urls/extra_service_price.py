from django.urls import path
from masters.views import ExtraPriceServiceListView

urlpatterns =[
    path('',ExtraPriceServiceListView.as_view())
]