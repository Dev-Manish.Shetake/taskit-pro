from django.urls import path
from masters.views import MetaDataTypeListView,MetaDataTypeUpdateView,MetaDataTypeDetailsListView,MetaDataTypeDetailsCreateView,\
    MetaDataTypeDetailsUpdateView,MetaDataTypeDetailsDestroyView

urlpatterns =[
    path('',MetaDataTypeListView.as_view()),
    path('<int:pk>',MetaDataTypeUpdateView.as_view()),
    path('details',MetaDataTypeDetailsListView.as_view()),
    path('details/delete/<int:pk>',MetaDataTypeDetailsDestroyView.as_view()),
    path('details-create',MetaDataTypeDetailsCreateView.as_view()),
    path('details-update',MetaDataTypeDetailsUpdateView.as_view()),
]