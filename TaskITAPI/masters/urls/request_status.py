from django.urls import path
from masters.views import RequestStatusListView

urlpatterns =[   
    path('',RequestStatusListView.as_view()),   
]