from django.urls import path
from masters.views import SubCategoryListView,SubCategoryListCreateView,SubCategoryDetailView,SubCategoryDestroyView,\
    SubCategoryMetaDataTypeListView,SubCategoryMetaDataTypeDetailsListView, SubCategoryPriceScopeMasterListView,SubCategoryPriceScopeDetailsMasterListView

urlpatterns =[
    path('list',SubCategoryListView.as_view()),
    path('',SubCategoryListCreateView.as_view()),
    path('<int:pk>',SubCategoryDetailView.as_view()),
    path('delete',SubCategoryDestroyView.as_view()),
    path('metadata-type-list',SubCategoryMetaDataTypeListView.as_view()),
    path('metadata-type-details-list',SubCategoryMetaDataTypeDetailsListView.as_view()),
    path('price-scope',SubCategoryPriceScopeMasterListView.as_view()),
    path('price-scope-details-list',SubCategoryPriceScopeDetailsMasterListView.as_view()),
]