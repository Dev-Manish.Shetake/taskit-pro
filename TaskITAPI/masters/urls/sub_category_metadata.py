from django.urls import path
from masters.views import SubCategoryMetaDataListView,SubCategoryMetaDataCreateView,SubCategoryMetaDataUpdateView,SubCategoryMetaDataDestroyView


urlpatterns =[
    path('',SubCategoryMetaDataListView.as_view()),
    path('create',SubCategoryMetaDataCreateView.as_view()),
    path('delete/<int:pk>',SubCategoryMetaDataDestroyView.as_view()),
    path('update',SubCategoryMetaDataUpdateView.as_view())
    # path('details-delete/<int:pk>',SubCategoryMetaDataDetailsDestroyView.as_view()),
]