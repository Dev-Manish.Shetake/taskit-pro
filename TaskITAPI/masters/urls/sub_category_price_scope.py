from django.urls import path
from masters.views import SubCategoryPriceScopeListView,SubCategoryPriceScopeCreateView,SubCategoryPriceScopeUpdateView,SubCategoryPriceScopeDestroyView


urlpatterns =[
    path('list',SubCategoryPriceScopeListView.as_view()),
    path('create',SubCategoryPriceScopeCreateView.as_view()),
    path('update',SubCategoryPriceScopeUpdateView.as_view()),
    path('delete/<int:pk>',SubCategoryPriceScopeDestroyView.as_view()),
]