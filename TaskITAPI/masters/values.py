
REQUEST_STATUS = {
    'Pending' : 1,
    'Active' : 2,
    'Unapproved' : 3,
    'Deleted' : 4
}