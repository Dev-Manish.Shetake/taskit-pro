from rest_framework import views,generics,serializers
from rest_framework.generics import CreateAPIView,ListCreateAPIView,RetrieveUpdateAPIView,ListAPIView, DestroyAPIView
from masters.models import Category
from masters.serializers import CategoryListSerializer,CategoryGigsCountListSerializer,CategorySerializer,CategoryDetailSerializer,CategoryDeleteSerializer,CategorySubCategoryListSerializer
from django.utils import timezone
from django.db.models import Q
from rest_framework import status
from rest_framework.response import Response
from TaskITAPI.pagination import StandardResultsSetPagination

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/categories/list)
'''
class CategoryListView(ListAPIView) :
    """
    Category List 
    
    Query parameters for GET method
    ---------------------------------------
    1. category_name = To filter by category_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. 127.0.0.1:8000/api/v1/categories/list?category_name=string&source=   

    """
    serializer_class = CategoryListSerializer


    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        queryset = Category.all_objects.all()     
        if is_active == 'false' :    
            q_objects.add(Q(is_active=False), Q.AND)
        elif is_active == 'true' : 
            q_objects.add(Q(is_active=True), Q.AND)
               
        category_name = self.request.GET.get('category_name')
        source = self.request.GET.get('source')
       
        if category_name :
            q_objects.add(Q(category_name__icontains=category_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        # To get Record in Dropdown OR Extender by alphabetically
        if not source :
            queryset = queryset.order_by('category_name')
        return queryset


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/categories/)
'''
class CategoryListCreateView(ListCreateAPIView) :
    """
    Category List 
    
    Query parameters for GET method
    ---------------------------------------
    1. category_name = To filter by category_name   
    2. is_active = true/false
    3. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. 127.0.0.1:8000/api/v1/categories/?category_name=string&is_actiev=true   

    """
    serializer_class = CategorySerializer
    pagination_class = StandardResultsSetPagination
   
    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        queryset = Category.all_objects.all()     
        if is_active == 'false' :    
            q_objects.add(Q(is_active=False), Q.AND)
        elif is_active == 'true' : 
            q_objects.add(Q(is_active=True), Q.AND)
               
        category_name = self.request.GET.get('category_name')
        source = self.request.GET.get('source')
       
        if category_name :
            q_objects.add(Q(category_name__icontains=category_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        # To get Record in Dropdown OR Extender by alphabetically
        if not source :
            queryset = queryset.order_by('category_name')
        return queryset

'''
View to send/receive/delete data in specific format for GET/PUT/PATCH/DELETE method
(endpoint = '/categories/:id')
'''
class CategoryDetailView(RetrieveUpdateAPIView) :
    queryset = Category.objects.none()
    serializer_class = CategoryDetailSerializer
   
    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = Category.all_objects.all().filter(is_active=False)
        else : 
            queryset = Category.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 

    def delete(self,request,*args,**kwargs) :
        category = self.get_object()
        category.is_active = False
        category.deleted_by = request.user
        category.deleted_user = request.user.user_name
        category.deleted_date = timezone.now()
        category.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

'''
View to send/receive/delete data in specific format for GET/PUT/PATCH/DELETE method
(endpoint = '/subcategories/')
'''
class CategoryDestroyView(generics.GenericAPIView) :

    class CategoryDestroySerializers(serializers.Serializer) :
        ids = CategoryDeleteSerializer(many=True)

    queryset = Category.objects.none()
    serializer_class = CategoryDestroySerializers

    def patch(self,request) :
        ids = request.data['ids']       
        for id in ids :   
            category = Category.objects.filter(id=id['id'])
            category = category[0]
            category.is_active = False
            category.deleted_by = request.user
            category.deleted_user = request.user.user_name
            category.deleted_date = timezone.now()
            category.save()
        res = {
            'code' : 'category_deleted',
            'message' : 'Category Deleted Succesfully.'
        }    
        return Response(res,status=status.HTTP_204_NO_CONTENT)    



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/category-subcategory-list/)
'''
class CategorySubCategoryListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. category_name = To filter by category_name  
    2. sub_category_name = To filter by sub_category_name 
     
    E.g. 127.0.0.1:8000/api/v1/categories/?category_name=string&sub_category_name=string   

    """
    serializer_class = CategorySubCategoryListSerializer
    authentication_classes = []
    permission_classes = []

    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        queryset = Category.all_objects.all()     
        if is_active == 'false' :    
            q_objects.add(Q(is_active=False), Q.AND)
        elif is_active == 'true' : 
            q_objects.add(Q(is_active=True), Q.AND)
               
        category_name = self.request.GET.get('category_name')
        sub_category_name = self.request.GET.get('sub_category_name')
       
        if category_name :
            q_objects.add(Q(category_name__icontains=category_name), Q.AND)
        if sub_category_name :
            q_objects.add(Q(sub_category__sub_category_name__icontains=sub_category_name), Q.AND)    
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset.order_by('id')


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/categories/gigs-count-list/)
'''
class CategoryGigsCountListView(ListAPIView) :
    """
    Category Gigs Count List 
    
    Query parameters for GET method
    ---------------------------------------
    1. category_name = To filter by category_name  

    E.g. 127.0.0.1:8000/api/v1/categories/gigs-count-list?category_name=string

    """
    serializer_class = CategoryGigsCountListSerializer   
    authentication_classes = []
    permission_classes = []

    def get_queryset(self) :
        q_objects = Q()
        queryset = Category.objects.all()                       
        category_name = self.request.GET.get('category_name')
        if category_name :
            q_objects.add(Q(category_name__icontains=category_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)        
        return queryset