from rest_framework import views
from rest_framework.generics import ListCreateAPIView,ListAPIView
from masters.models import PriceExtraService
from masters.serializers import ExtraPriceServiceSerializer
from django.utils import timezone
from django.db.models import Q


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/extra_services/)
'''
class ExtraPriceServiceListView(ListAPIView) :
    """
    Extra Services List 
    
    Query parameters for GET method
    ---------------------------------------
    1. title = To filter by title name   
   
    E.g. 127.0.0.1:8000/api/v1/metadatatype/?metadata_type_name=string&source=   

    """
    serializer_class = ExtraPriceServiceSerializer
    
    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        queryset = PriceExtraService.all_objects.all()     
        if is_active == 'false' :    
            q_objects.add(Q(is_active=False), Q.AND)
        elif is_active == 'true' : 
            q_objects.add(Q(is_active=True), Q.AND)
               
        title = self.request.GET.get('title')
               
        if title :
            q_objects.add(Q(title__icontains=title), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset