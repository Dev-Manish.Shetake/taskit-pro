from rest_framework import views
from rest_framework.generics import GenericAPIView,CreateAPIView,ListCreateAPIView,RetrieveUpdateAPIView,ListAPIView,DestroyAPIView
from masters.models import MetaDataType, MetaDataTypeDetails, SubCategoryMetaDataTypeDetails
from masters.serializers import MetaDataTypeSerializer,MetaDataTypeUpdateSerializer,MetaDataTypeDetailsSerializer,MetaDataTypeDetailsCreateSerializer,MetaDataTypeDetailsUpdateSerializer 
from _requests.models import RequestDetails
from django.utils import timezone
from django.db.models import Q
from rest_framework import status
from rest_framework.response import Response
from django.db import transaction
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/)
'''
class MetaDataTypeListView(ListCreateAPIView) :
    """
    MetaDataType List 
    
    Query parameters for GET method
    ---------------------------------------
    1. metadata_type_name = To filter by metadata_type_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. 127.0.0.1:8000/api/v1/metadatatype/?metadata_type_name=string&source=   

    """
    serializer_class = MetaDataTypeSerializer

    def apply_filter(self,queryset) :
        q_objects = Q()
        metadata_type_name = self.request.GET.get('metadata_type_name')
        if metadata_type_name :
            q_objects.add(Q(metadata_type_name__icontains=metadata_type_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset
    
    def get_queryset(self) :       
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL      
        source = self.request.GET.get('source')       
        if is_active == 'false' :    
            queryset = MetaDataType.all_objects.filter(is_active=False)
        else : 
            queryset = MetaDataType.objects.all()               
        queryset = self.apply_filter(queryset)      
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        # To get Record in Dropdown OR Extender by alphabetically
        if not source :
            queryset = queryset.order_by('metadata_type_name')
        return queryset


'''
View to send/receive/delete data in specific format for GET/PUT/PATCH/DELETE method
(endpoint = '/metadata_type/:id')
'''
class MetaDataTypeUpdateView(RetrieveUpdateAPIView) :

    queryset = MetaDataType.objects.none()
    serializer_class = MetaDataTypeUpdateSerializer
   
    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = MetaDataType.all_objects.all().filter(is_active=False)
        else : 
            queryset = MetaDataType.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 

    def delete(self,request,*args,**kwargs) :
        metadata_type = self.get_object()
        metadata_type.is_active = False
        metadata_type.deleted_by = request.user
        metadata_type.deleted_user = request.user.user_name
        metadata_type.deleted_date = timezone.now()
        metadata_type.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/details)
'''
class MetaDataTypeDetailsListView(ListAPIView) :
    """
    MetaDataType Details List 
    
    Query parameters for GET method
    ---------------------------------------
    1. metadata_type_name = To filter by metadata_type_name 
    2. metadata_type_id = ID of Metadata type  
   
    E.g. 127.0.0.1:8000/api/v1/metadatatype/?metadata_type_name=string&source=   

    """
    serializer_class = MetaDataTypeDetailsSerializer

    def apply_filter(self,queryset) :
        q_objects = Q()
        metadata_type_id = self.request.GET.get('metadata_type_id')
        metadata_type_name = self.request.GET.get('metadata_type_name')
        if metadata_type_id :
            q_objects.add(Q(metadata_type_id=metadata_type_id), Q.AND)
        if metadata_type_name :
            q_objects.add(Q(metadata_type_name__icontains=metadata_type_name), Q.AND)    
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)     
      
        return queryset
    
    def get_queryset(self) :       
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        if is_active == 'false' :
            queryset = MetaDataTypeDetails.all_objects.filter(is_active=False)
        else :
            queryset = MetaDataTypeDetails.objects.all()
        source = self.request.GET.get('source')
        queryset = self.apply_filter(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset



class MetaDataTypeDetailsDestroyView(DestroyAPIView) :

    queryset = MetaDataTypeDetails.objects.all()

    def delete(self,request,*args,**kwargs) :
        metadata_type_dtl = self.get_object()
        if SubCategoryMetaDataTypeDetails.objects.filter(metadata_type_dtl=metadata_type_dtl).exists() or RequestDetails.objects.filter(metadata_type_dtl=metadata_type_dtl):
            raise ValidationError(_('Metadata detail record is linked to other subcategories metadata.'),code='can_not_delete')
        metadata_type_dtl.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/details-create')
'''
class MetaDataTypeDetailsCreateView(CreateAPIView) :
    """
    MetaDataType Details Create
    """
    serializer_class = MetaDataTypeDetailsCreateSerializer

    @transaction.atomic
    def create(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data  
        details = []
        if 'details'in validated_data :
            details = validated_data.pop('details')
        for dtl in details :
            metadata_type_dtl = MetaDataTypeDetails.objects.create(
                metadata_type_id = validated_data['metadata_type_id'],
                **dtl
            )
        return Response(status=status.HTTP_201_CREATED)



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/metadata_type/details-update')
'''
class MetaDataTypeDetailsUpdateView(GenericAPIView) :
    """
    MetaDataType Details Update
    """
    serializer_class = MetaDataTypeDetailsUpdateSerializer

    def patch(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data  
        details = []
        metadata_type_id = validated_data['metadata_type_id']

        if not MetaDataType.objects.filter(id=metadata_type_id).exists() :
            raise ValidationError(_('Invalid input recieved for metadata type id'),code='metadata_type_id_invalid_input')

        if 'details'in validated_data :
            details = validated_data.pop('details')

        for dtl in details :
            if dtl['id'] != 0 :
                MetaDataTypeDetails.objects.filter(id=dtl['id']).update(
                    seq_no=dtl['seq_no'],
                    metadata_type_name=dtl['metadata_type_name']
                )
            else :
                metadata_type_dtl = MetaDataTypeDetails.objects.create(
                    metadata_type_id = metadata_type_id,
                    seq_no=dtl['seq_no'],
                    metadata_type_name=dtl['metadata_type_name']
                ) 
        return Response(status=status.HTTP_201_CREATED)