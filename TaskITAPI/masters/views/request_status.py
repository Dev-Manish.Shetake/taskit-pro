from rest_framework import views,generics,serializers
from rest_framework.generics import ListAPIView
from masters.models import RequestStatus
from masters.serializers import RequestStatusListSerializer
from django.utils import timezone
from django.db.models import Q
from rest_framework import status
from rest_framework.response import Response
from TaskITAPI.pagination import StandardResultsSetPagination

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/categories/list)
'''
class RequestStatusListView(ListAPIView) :
    """
    Request Status List 
    
    Query parameters for GET method
    ---------------------------------------
    1. request_status_name = To filter by request_status_name   
    2. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. 127.0.0.1:8000/api/v1/request_status?request_status_name=string
    """

    serializer_class = RequestStatusListSerializer


    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if is_active == 'false' :    
           queryset = RequestStatus.all_objects.filter(is_active=False)    
        else :
            queryset = RequestStatus.objects.all()  
        request_status_name = self.request.GET.get('request_status_name')
        source = self.request.GET.get('source')
       
        if request_status_name :
            q_objects.add(Q(request_status_name__icontains=request_status_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        # To get Record in Dropdown OR Extender by alphabetically
        if not source :
            queryset = queryset.order_by('request_status_name')
        return queryset
