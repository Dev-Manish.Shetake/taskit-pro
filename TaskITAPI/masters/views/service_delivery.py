from rest_framework import views
from rest_framework.generics import ListAPIView
from masters.models import ServiceDelivery
from django.utils import timezone
from django.db.models import Q
from masters.serializers import ServiceDeliverySerializer


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/categories/)
'''
class ServiceDeliveryListView(ListAPIView) :
    """
    ServiceDelivery List 
    
    Query parameters for GET method
    ---------------------------------------     
    1. service_delivery_name = Filter by Service Delivery Name  

    E.g. 127.0.0.1:8000/api/v1/categories/?category_name=string&source=   

    """
    serializer_class = ServiceDeliverySerializer
    authentication_classes = []
    permission_classes = []


    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL            
        if is_active == 'false' :    
            queryset = ServiceDelivery.all_objects.filter(is_active=False)    
        else :
           queryset = ServiceDelivery.objects.all()               
        service_delivery_name = self.request.GET.get('service_delivery_name')        
        if service_delivery_name :
            q_objects.add(Q(service_delivery_name__icontains=service_delivery_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)        
        return queryset.order_by('seq_no')