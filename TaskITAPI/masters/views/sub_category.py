from rest_framework import views,serializers,generics
from rest_framework.generics import CreateAPIView,ListCreateAPIView,RetrieveUpdateAPIView, ListAPIView, DestroyAPIView
from masters.models import SubCategory, SubCategoryMetaDataType, SubCategoryMetaDataTypeDetails, SubCategoryPriceScope,SubCategoryPriceScopeDetails
from masters.serializers import SubCategoryListSerializer,SubCategorySerializer,SubCategoryDetailSerializer, SubCategoryMetaDataTypeListMasterSerializer,\
    SubCategoryMetaDataTypeDetailsMasterSerializer, SubCategoryPriceScopeMasterSerializer,SubCategoryPriceScopeDetailsMasterSerializer, SubCategoryPriceScopeDetailsSerializer,SubCategoryDeleteSerializer
from django.utils import timezone
from django.db.models import Q
from TaskITAPI.pagination import StandardResultsSetPagination
from rest_framework.response import Response
from rest_framework import status


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/list')
'''
class SubCategoryListView(ListAPIView) :
    """
    Sub Category List
    
    Query parameters for GET method
    ---------------------------------------
    1. sub_category_name = To filter by sub_category_name
    2. category_id = ID of Category
    3. category_name = To filter by category_name
    4. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. 127.0.0.1:8000/api/v1/categories/?sub_category_name=string   

    """
    serializer_class = SubCategoryListSerializer
    authentication_classes = []
    permission_classes = []

    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL             
        if is_active == 'false' :    
            queryset = SubCategory.all_objects.filter(is_active=False)
        else : 
            queryset = SubCategory.objects.all()
               
        sub_category_name = self.request.GET.get('sub_category_name')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')
        source = self.request.GET.get('source')
       
        if sub_category_name :
            q_objects.add(Q(sub_category_name__icontains=sub_category_name), Q.AND)
        if category_id :
            q_objects.add(Q(category_id=category_id), Q.AND)
        if category_name :
            q_objects.add(Q(category__category_name__icontains=category_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        # To get Record in Dropdown OR Extender by alphabetically
        if not source :
            queryset = queryset.order_by('sub_category_name')
        return queryset



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/')
'''
class SubCategoryListCreateView(ListCreateAPIView) :
    """
    Sub Category List
    
    Query parameters for GET method
    ---------------------------------------
    1. sub_category_name = To filter by sub_category_name
    2. category_id = ID of Category
    3. category_name = To filter by category_name
    4. is_active = true/false
    5. source = 'Master' OR ' ' 

    Note :  For DropDown & Extender Do not Pass source parameter.

    E.g. 127.0.0.1:8000/api/v1/categories/?sub_category_name=string&is_active   

    """
    serializer_class = SubCategorySerializer
    pagination_class = StandardResultsSetPagination

    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        queryset = SubCategory.all_objects.all()     
        if is_active == 'false' :    
            q_objects.add(Q(is_active=False), Q.AND)
        else : 
            q_objects.add(Q(is_active=True), Q.AND)
               
        sub_category_name = self.request.GET.get('sub_category_name')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')
        source = self.request.GET.get('source')
       
        if sub_category_name :
            q_objects.add(Q(sub_category_name__icontains=sub_category_name), Q.AND)
        if category_id :
            q_objects.add(Q(category_id=category_id), Q.AND)
        if category_name :
            q_objects.add(Q(category__category_name__icontains=category_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        # To get Record in Dropdown OR Extender by alphabetically
        if not source :
            queryset = queryset.order_by('sub_category_name')
        return queryset


'''
View to send/receive/delete data in specific format for GET/PUT/PATCH/DELETE method
(endpoint = '/subcategories/:id')
'''
class SubCategoryDetailView(RetrieveUpdateAPIView) :
    queryset = SubCategory.objects.all()
    serializer_class = SubCategoryDetailSerializer
    
    def get_queryset(self) :
        q_objects = Q()
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = SubCategory.all_objects.all().filter(is_active=False)
        else : 
            queryset = SubCategory.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset 

    def delete(self,request,*args,**kwargs) :
        sub_category = self.get_object()
        sub_category.is_active = False
        sub_category.deleted_by = request.user
        sub_category.deleted_user = request.user.user_name
        sub_category.deleted_date = timezone.now()
        sub_category.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

'''
View to send/receive/delete data in specific format for GET/PUT/PATCH/DELETE method
(endpoint = '/subcategories/:id')
'''
class SubCategoryDestroyView(generics.GenericAPIView) :

    class SubCategoryDestroySerializers(serializers.Serializer) :
        ids = SubCategoryDeleteSerializer(many=True)

    queryset = SubCategory.objects.none()
    serializer_class = SubCategoryDestroySerializers

    def patch(self,request,*args,**kwargs) :
        ids = request.data['ids']       
        for id in ids :       
            sub_category = SubCategory.objects.filter(id=id['id'])
            sub_category = sub_category[0]
            sub_category.is_active = False
            sub_category.deleted_by = request.user
            sub_category.deleted_user = request.user.user_name
            sub_category.deleted_date = timezone.now()
            sub_category.save()
        res = {
            'code' : 'sub_category_deleted',
            'message' : 'Sub Category Deleted Succesfully.'
        }      
        return Response(res,status=status.HTTP_204_NO_CONTENT)    




'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/metadata-type-list')
'''
class SubCategoryMetaDataTypeListView(ListAPIView) :
    """
    Sub-Category MetaData PriceType List
    
    Query parameters for GET method
    ---------------------------------------
    1. sub_category_id = To filter by sub category ID
    2. sub_category_name = To filter by sub_category_name
    3. category_id = To filter by category ID
    3. category_name = To filter by category_name
   

    E.g. http://127.0.0.1:8000/api/v1/subcategories/metadata-type-list?sub_category_name=string   

    """
    serializer_class = SubCategoryMetaDataTypeListMasterSerializer
    

    def get_queryset(self) :
        q_objects = Q()        
        queryset = SubCategoryMetaDataType.objects.all()

        sub_category_id = self.request.GET.get('sub_category_id')     
        sub_category_name = self.request.GET.get('sub_category_name')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')
        source = self.request.GET.get('source')

        if sub_category_id :
            q_objects.add(Q(sub_category_id=sub_category_id), Q.AND)
        if sub_category_name :
            q_objects.add(Q(sub_category__sub_category_name__icontains=sub_category_name), Q.AND)
        if category_id :
            q_objects.add(Q(sub_category__category__id=category_id), Q.AND)
        if category_name :
            q_objects.add(Q(sub_category__category__category_name__icontains=category_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
            
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        queryset = queryset.order_by('seq_no')
        return queryset




'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/metadata-type-details-list')
'''
class SubCategoryMetaDataTypeDetailsListView(ListAPIView) :
    """
    Sub-Category MetaData PriceType List
    
    Query parameters for GET method
    ---------------------------------------
    1. sub_category_metadata_type_id = To filter by sub category metadata type ID

    E.g. http://127.0.0.1:8000/api/v1/subcategories/metadata-type-details-list?sub_category_metadata_type_id=1 
    """
    serializer_class = SubCategoryMetaDataTypeDetailsMasterSerializer    

    def apply_filter(self,queryset) :
        q_objects = Q()
        sub_category_metadata_type_id = self.request.GET.get('sub_category_metadata_type_id')     
        if sub_category_metadata_type_id :
            q_objects.add(Q(sub_category_metadata_type_id=sub_category_metadata_type_id), Q.AND)       
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL 
        if is_active == 'false' :    
            queryset = SubCategoryMetaDataTypeDetails.all_objects.filter(is_active=False)     
        else : 
            queryset = SubCategoryMetaDataTypeDetails.objects.all()   
        queryset = self.apply_filter(queryset)              
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)        
        return queryset


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/price-scope')
'''
class SubCategoryPriceScopeMasterListView(ListAPIView) :
    """
    Sub Category Price Scope List
    
    Query parameters for GET method
    ---------------------------------------
    1. sub_category_id = To filter by sub category ID
    2. sub_category_name = To filter by sub_category_name
    3. category_id = To filter by category ID
    3. category_name = To filter by category_name    

    E.g. 127.0.0.1:8000/api/v1/subcategories/price-scope?sub_category_id=1&category_id=2   

    """
    serializer_class = SubCategoryPriceScopeMasterSerializer

    def apply_filter(self,queryset) :
        q_objects = Q()
        sub_category_id = self.request.GET.get('sub_category_id')     
        sub_category_name = self.request.GET.get('sub_category_name')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')
        price_scope_name = self.request.GET.get('price_scope_name')
      
        if sub_category_id :
            q_objects.add(Q(sub_category_id=sub_category_id), Q.AND)
        if sub_category_name :
            q_objects.add(Q(sub_category__sub_category_name__icontains=sub_category_name), Q.AND)
        if category_id :
            q_objects.add(Q(sub_category__category__id=category_id), Q.AND)
        if category_name :
            q_objects.add(Q(sub_category__category__category_name__icontains=category_name), Q.AND)
        if price_scope_name :
            q_objects.add(Q(price_scope_name__icontains=price_scope_name), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset

    def get_queryset(self) :  
        queryset = SubCategoryPriceScope.objects.all()     
        queryset = self.apply_filter(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)       
        return queryset        




'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories/price-scope')
'''
class SubCategoryPriceScopeDetailsMasterListView(ListAPIView) :
    """
    Sub Category Price Scope Details List
    
    Query parameters for GET method
    ---------------------------------------
    1. sub_category_price_scope_id = To filter by sub category price scope ID

    E.g. 127.0.0.1:8000/api/v1/subcategories/price-scope-details-list/?sub_category_price_scope_id=1   

    """
    serializer_class = SubCategoryPriceScopeDetailsMasterSerializer

    def apply_filter(self,queryset) :
        q_objects = Q()
        sub_category_price_scope_id = self.request.GET.get('sub_category_price_scope_id')   
      
        if sub_category_price_scope_id :
            q_objects.add(Q(sub_category_price_scope_id=sub_category_price_scope_id), Q.AND)      
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset

    def get_queryset(self) :       
        queryset = SubCategoryPriceScopeDetails.objects.all()             
        queryset = self.apply_filter(queryset)    
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)       
        return queryset 
