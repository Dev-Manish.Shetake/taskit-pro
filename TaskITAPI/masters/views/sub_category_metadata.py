from rest_framework.generics import GenericAPIView,ListAPIView,CreateAPIView,RetrieveUpdateAPIView,DestroyAPIView
from masters.models import SubCategory,SubCategoryMetaDataType,SubCategoryMetaDataTypeDetails
from masters.serializers import SubCategoryMetaDataTypeListSerializer,SubCategoryMetaDataTypeCreateSerializer,\
    SubCategoryMetaDataTypeUpdateSerializer    
from gigs.models import GigSubCategoryMetaDataTypeDetails
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from django.utils import timezone
from django.db import transaction
from gigs.models import GigSubCategoryMetaDataType,GigSubCategoryMetaDataTypeDetails 


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/list')
'''
class SubCategoryMetaDataListView(ListAPIView) :
    """
    Sub-Category MetaData List
    
    Query parameters for GET method
    ---------------------------------------
    1. sub_category_id = To filter by sub category ID
    2. sub_category_name = To filter by sub_category_name
    3. category_id = To filter by category ID
    3. category_name = To filter by category_name
    4. metadata_type_name = To filter by metadata type name
  
    E.g. http://127.0.0.1:8000/api/v1/subcategories/metadata-type-list?sub_category_name=string   

    """
    serializer_class = SubCategoryMetaDataTypeListSerializer
    pagination_class = StandardResultsSetPagination   
    
    def apply_filter(self,queryset) :
        q_objects = Q()
        sub_category_id = self.request.GET.get('sub_category_id')     
        sub_category_name = self.request.GET.get('sub_category_name')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')
        metadata_type_name = self.request.GET.get('metadata_type_name')

        if sub_category_id :
            q_objects.add(Q(id=sub_category_id), Q.AND)
        if sub_category_name :
            q_objects.add(Q(sub_category_name__icontains=sub_category_name), Q.AND)
        if category_id :
            q_objects.add(Q(category__id=category_id), Q.AND)
        if category_name :
            q_objects.add(Q(category__category_name__icontains=category_name), Q.AND)
        if metadata_type_name :
            sub_category_ids = SubCategoryMetaDataType.objects.filter(metadata_type__metadata_type_name__icontains=metadata_type_name).values_list('sub_category_id')
            q_objects.add(Q(id__in=sub_category_ids), Q.AND)
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)        
        return queryset

    def get_queryset(self) :    
        sub_categories_ids = SubCategoryMetaDataType.objects.all().values_list('sub_category_id')
        queryset = SubCategory.objects.filter(id__in=sub_categories_ids)  
        queryset = self.apply_filter(queryset)          
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/create')
'''
class SubCategoryMetaDataCreateView(CreateAPIView) :

    serializer_class = SubCategoryMetaDataTypeCreateSerializer

    
    @transaction.atomic
    def create(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data   
        sub_category_metadata_type = []
        details = []
        if 'sub_category_metadata_type' in validated_data :
            sub_category_metadata_type = validated_data.pop('sub_category_metadata_type')        
        for metadata_type in sub_category_metadata_type :     
            if 'details' in metadata_type :
                details = metadata_type.pop('details')
            sub_category_metadata_type = SubCategoryMetaDataType.objects.create(
                **metadata_type,
                created_by = self.request.user,
                created_user = self.request.user.user_name,
                created_date = timezone.now()
            )
            for dtl in details :
                SubCategoryMetaDataTypeDetails.objects.create(
                    sub_category_metadata_type=sub_category_metadata_type,
                    **dtl
            )
        return Response(status=status.HTTP_201_CREATED)


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/update/:id')
'''
class SubCategoryMetaDataUpdateView(GenericAPIView) :

    # queryset = SubCategoryMetaDataType.objects.all()
    serializer_class = SubCategoryMetaDataTypeUpdateSerializer    
    
    @transaction.atomic
    def patch(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data   
        sub_category_metadata_type = []
        details = []
        details_ids = []
        if 'sub_category_metadata_type' in validated_data :
            sub_category_metadata_type = validated_data.pop('sub_category_metadata_type')        
        for metadata_type in sub_category_metadata_type :     
            if 'details' in metadata_type :
                details = metadata_type.pop('details')
            if metadata_type['id'] != 0 :  
                sub_category_metadata_type = SubCategoryMetaDataType.objects.filter(id=metadata_type['id']).update(
                    sub_category_id = metadata_type['sub_category_id'],
                    seq_no = metadata_type['seq_no'],
                    metadata_type_id =  metadata_type['metadata_type_id'],
                    is_single = metadata_type['is_single'],
                    is_mandatory = metadata_type['is_mandatory'],
                    is_suggest_other = metadata_type['is_suggest_other'],
                    modified_by = self.request.user,
                    modified_user = self.request.user.user_name,
                    modified_date = timezone.now()
                )
                for dtl in details :
                    if dtl['id'] != 0 :
                        SubCategoryMetaDataTypeDetails.objects.filter(id=dtl['id']).update(
                            seq_no = dtl['seq_no'],
                            metadata_type_dtl_id = dtl['metadata_type_dtl_id']
                        )
                        details_ids.append(dtl['id'])    
                    else :
                        sub_category_metadatatype_details = SubCategoryMetaDataTypeDetails.objects.create(
                            sub_category_metadata_type_id = metadata_type['id'], 
                            seq_no = dtl['seq_no'],
                            metadata_type_dtl_id = dtl['metadata_type_dtl_id']
                        )    
                        details_ids.append(sub_category_metadatatype_details.id) 
                SubCategoryMetaDataTypeDetails.objects.filter(id=dtl['id']).exclude(id__in=details_ids).delete()  
            else :
                sub_category_metadata_type = SubCategoryMetaDataType.objects.create(
                    sub_category_id = metadata_type['sub_category_id'],
                    seq_no = metadata_type['seq_no'],
                    metadata_type_id =  metadata_type['metadata_type_id'],
                    is_single = metadata_type['is_single'],
                    is_mandatory = metadata_type['is_mandatory'],
                    is_suggest_other = metadata_type['is_suggest_other'],
                    created_by = self.request.user,
                    created_user = self.request.user.user_name,
                    created_date = timezone.now()
                )
                for dtl in details :                    
                    sub_category_metadatatype_details = SubCategoryMetaDataTypeDetails.objects.create(
                        sub_category_metadata_type = sub_category_metadata_type, 
                        seq_no = dtl['seq_no'],
                        metadata_type_dtl_id = dtl['metadata_type_dtl_id']
                    )
        return Response(status=status.HTTP_201_CREATED)


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_metadata/delete/:id')
'''
class SubCategoryMetaDataDestroyView(DestroyAPIView) :
    queryset = SubCategoryMetaDataType.objects.all()
    
    @transaction.atomic
    def delete(self,request,*args,**kwargs) :
        sub_category_metadata_type = self.get_object()      
        sub_category_metadata_type.is_active = False
        sub_category_metadata_type.deleted_by = request.user
        sub_category_metadata_type.deleted_user = request.user.user_name
        sub_category_metadata_type.deleted_date = timezone.now()
        sub_category_metadata_type.save()
        SubCategoryMetaDataTypeDetails.objects.filter(
            sub_category_metadata_type=sub_category_metadata_type
            ).delete()        
        return Response(status=status.HTTP_200_OK)

