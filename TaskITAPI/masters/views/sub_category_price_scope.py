from rest_framework.generics import GenericAPIView,ListAPIView,CreateAPIView,RetrieveUpdateAPIView,DestroyAPIView
from masters.models import SubCategory,SubCategoryPriceScope,SubCategoryPriceScopeDetails
from masters.serializers import SubCategoryPriceScopeListSerializer,SubCategoryPriceScopeCreateSerializer,SubCategoryPriceScopeUpdateSerializer
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db.models import Q
from rest_framework.response import Response
from rest_framework import status
from django.utils import timezone
from django.db import transaction
from gigs.models import GigSubCategoryPriceDetails,GigSubCategoryPriceDetailsDraft
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/list')
'''
class SubCategoryPriceScopeListView(ListAPIView) :
    """
    Sub-Category Price Scope List
    
    Query parameters for GET method
    ---------------------------------------
    1. sub_category_id = To filter by sub category ID   
    2. category_id = To filter by category ID   
  
    E.g. http://127.0.0.1:8000/api/v1/subcategories_price_scope/list?sub_category_id=1   

    """
    serializer_class = SubCategoryPriceScopeListSerializer
    pagination_class = StandardResultsSetPagination   
    
    def apply_filter(self,queryset) :
        q_objects = Q()
        sub_category_id = self.request.GET.get('sub_category_id')     
        sub_category_name = self.request.GET.get('sub_category_name')
        category_id = self.request.GET.get('category_id')
        category_name = self.request.GET.get('category_name')

        if sub_category_id :
            q_objects.add(Q(id=sub_category_id), Q.AND)
        if sub_category_name :
            q_objects.add(Q(sub_category_name__icontains=sub_category_name), Q.AND)
        if category_id :
            q_objects.add(Q(category__id=category_id), Q.AND)
        if category_name :
            q_objects.add(Q(category__category_name__icontains=category_name), Q.AND)       
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)        
        return queryset

    def get_queryset(self) :    
        sub_categories_ids = SubCategoryPriceScope.objects.all().values_list('sub_category_id')
        queryset = SubCategory.objects.filter(id__in=sub_categories_ids)  
        queryset = self.apply_filter(queryset)          
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/create')
'''
class SubCategoryPriceScopeCreateView(CreateAPIView) :

    serializer_class = SubCategoryPriceScopeCreateSerializer
    
    @transaction.atomic
    def create(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data   
        sub_category_price_scope_list = []
        details = []
        if 'sub_category_price_scope' in validated_data :
            sub_category_price_scope_list = validated_data.pop['sub_category_price_scope_list']        
        for price_scope in sub_category_price_scope_list :     
            if 'details' in price_scope :
                details = price_scope.pop['details']
            sub_category_price_scope = SubCategoryPriceScope.objects.create(
                **price_scope,
                created_by = self.request.user,
                created_user = self.request.user.user_name,
                created_date = timezone.now()
            )
            for dtl in details :
                SubCategoryPriceScopeDetails.objects.create(
                    sub_category_price_scope=sub_category_price_scope,
                    **dtl
            )
        return Response(status=status.HTTP_201_CREATED)    


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/update/')
'''
class SubCategoryPriceScopeUpdateView(GenericAPIView) :

    queryset = SubCategoryPriceScope.objects.all()
    serializer_class = SubCategoryPriceScopeUpdateSerializer    
    
    @transaction.atomic
    def patch(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data   
        sub_category_price_scope_list = []
        details = []
        details_ids = [] 
        if 'sub_category_price_scope' in validated_data :
            sub_category_price_scope_list = validated_data.pop('sub_category_price_scope')  
        for price_scope in sub_category_price_scope_list :  
            watermark = None   
            if 'details' in price_scope :
                details = price_scope.pop('details')
            if 'watermark' in price_scope :
                watermark = price_scope.pop('watermark')
            if price_scope['id'] != 0 :  
                sub_category_price_scope = SubCategoryPriceScope.objects.filter(id=price_scope['id']).update(
                    sub_category_id = price_scope['sub_category_id'],
                    seq_no = price_scope['seq_no'],
                    price_scope_name = price_scope['price_scope_name'],
                    price_scope_control_type_id =  price_scope['price_scope_control_type_id'],
                    watermark = watermark,
                    is_mandatory = price_scope['is_mandatory'],                  
                    modified_by = self.request.user,
                    modified_user = self.request.user.user_name,
                    modified_date = timezone.now()
                )
                for dtl in details :
                    if dtl['id'] != 0 :
                        SubCategoryPriceScopeDetails.objects.filter(id=dtl['id']).update(
                            seq_no = dtl['seq_no'],
                            value = dtl['value'],
                            display_value = dtl['display_value'],
                            actual_value = dtl['actual_value']
                        )
                    else :
                        sub_category_price_scope_details = SubCategoryPriceScopeDetails.objects.create(
                            sub_category_price_scope_id = price_scope['id'], 
                            seq_no = dtl['seq_no'],
                            value = dtl['value'],
                            display_value = dtl['display_value'],
                            actual_value = dtl['actual_value']
                        )    
            else :
                sub_category_price_scope = SubCategoryPriceScope.objects.create(
                    sub_category_id = price_scope['sub_category_id'],
                    seq_no = price_scope['seq_no'],
                    price_scope_name = price_scope['price_scope_name'],
                    price_scope_control_type_id =  price_scope['price_scope_control_type_id'],
                    watermark = watermark,
                    is_mandatory = price_scope['is_mandatory'],                  
                    created_by = self.request.user,
                    created_user = self.request.user.user_name,
                    created_date = timezone.now()
                )        
                for dtl in details :
                    sub_category_price_scope_details = SubCategoryPriceScopeDetails.objects.create(
                        sub_category_price_scope = sub_category_price_scope, 
                        seq_no = dtl['seq_no'],
                        value = dtl['value'],
                        display_value = dtl['display_value'],
                        actual_value = dtl['actual_value']
                    )     
        return Response(status=status.HTTP_201_CREATED)
         


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/subcategories_price_scope/delete/:id')
'''
class SubCategoryPriceScopeDestroyView(DestroyAPIView) :
    queryset = SubCategoryPriceScope.objects.all()
    
    @transaction.atomic
    def delete(self,request,*args,**kwargs) :
        sub_category_price_scope = self.get_object()
        sub_category_price_scope.is_active = False
        if GigSubCategoryPriceDetails.objects.filter(sub_category_price_scope=sub_category_price_scope).exists() or GigSubCategoryPriceDetails.objects.filter(sub_category_price_scope=sub_category_price_scope).exists() :
            raise ValidationError(_('Price Scope detail record is linked to other subcategories price scope.'),code='price_scope_can_not_delete')
        sub_category_price_scope.deleted_by = request.user
        sub_category_price_scope.deleted_user = request.user.user_name
        sub_category_price_scope.deleted_date = timezone.now()
        sub_category_price_scope.save()
        SubCategoryPriceScopeDetails.objects.filter(
            sub_category_price_scope=sub_category_price_scope
            ).delete()        
        return Response(status=status.HTTP_200_OK)
