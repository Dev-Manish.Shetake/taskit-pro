from notifications.tasks import send_email,send_message, generate_notification
from notifications.utils import get_email_gateway_info,get_specific_email_gateway_info, get_sms_gateway_info,get_in_app_gateway_info
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.template.loader import render_to_string
from django.utils import timezone
import os
from TaskITAPI.settings import DEFAULT_LANGUAGE
from django.core.cache import cache
from django.core.mail import get_connection
from django.core.mail.message import EmailMessage
from notifications.models import EmailGateway, EmailQueue, EmailTemplate, EmailTemplateLanguage
from notifications.models import SMSGateway, SMSQueue, SMSTemplate, SMSTemplateLanguage
from notifications.models import NotificationAppGateway, NotificationAppQueue, NotificationAppTemplate, NotificationAppTemplateLanguage
import requests
import json
from datetime import datetime, timedelta
import pytz
from urllib.parse import quote
from settings.models import SystemConfiguration
from notifications import events
from settings.values.configs import SMS_CC_SWITCH,SMS_CC_RECEPIENTS
from settings.values import defaults


class BaseNotification :

    def __init__(self, notification = None, context = None, lang = DEFAULT_LANGUAGE) :
        self.notification = notification
        self.context = context
        self.lang = lang

    def get_context_data(self) :      
        return self.context


    def get_langauge(self, context) :
        if 'user' in context :
            self.lang = context['user'].lang_id
        elif 'lang' in context :
            self.lang = context['lang']


    def send_notification(self) :
        context = self.get_context_data()
        mode = 'E' 
        if self.notification.is_scheduled :
            mode = 'S'
        self.get_langauge(context)            
        # if self.notification.event_code not in (events.BULK_NOTIFICATION,events.RESEND_BULK_NOTIFICATION) :           
        if self.notification.is_email_notification :
            send_email.delay(self.notification, context, self.lang, mode)
        if self.notification.is_sms_notification :
            send_message.delay(self.notification, context, self.lang, mode)  
        if self.notification.is_app_notification :
                generate_notification.delay(self.notification, context, self.lang, mode)   


'''
Base class for all email templates
'''
class BaseMail :

    def __init__(self, email_template = None, context = None, lang = DEFAULT_LANGUAGE, mode='E') :
        self.email_template = email_template
        self.context = context
        self.lang = lang
        self.mode = mode

    # Get template info according to preferred language
    def get_email_template_lang(self) :
        if len(self.email_template.email_template_lang) > 0 :
            email_template_lang = self.email_template.email_template_lang[0]
            return email_template_lang
        else :
            email_template_lang = EmailTemplateLanguage.objects \
                .filter(notification_email_template=self.email_template, lang=DEFAULT_LANGUAGE)
            if len(email_template_lang) > 0 :
                return email_template_lang[0]
        raise EmailTemplateLanguageNotAvailble

    # Get context to render in email template
    def get_context(self, **kwargs) :
        return self.context
    # Get email body.
    def get_email_body(self, email_template_lang, context) :
        temp_path = email_template_lang.template_path
        print("TEMPLATE PATH : " + temp_path)
        body = render_to_string(temp_path, context)
        return body 

    def get_recepients(self) :
        print(self.mode)
        if self.mode != 'B' :
            user = self.context['user']
            email_address = self.context['user']  
        else :
            email_address = self.context['email_address']  
        return (email_address,)
    
    def get_cc_recepients(self) :
        email_address = None
        if 'email_address' in self.context :
            email_address = self.context['email_address']
        return email_address

    def log_email_queue(self, gateway, recepients, template_lang, body, context) :
        email_queue = []        
        if self.mode != 'B' :
            for r in recepients :              
                email = EmailQueue.objects.create(
                    notification_email_template = self.email_template,
                    lang = self.lang,
                    from_email_id = gateway.user_name,
                    to_email_id = r,
                    cc_email_id = self.email_template.cc_email,
                    bcc_email_id = self.email_template.bcc_email,
                    email_subject = template_lang.email_subject,
                    body = body,
                    send_date = None,
                    mode = self.mode,      
                )
                email_queue.append(email)
        else :
            for r in recepients : 
                email = EmailQueue.objects.create(
                    notification_email_template = self.email_template,
                    lang = self.lang,
                    from_email_id = gateway.user_name,
                    to_email_id = r,
                    cc_email_id = context['cc_email'],
                    bcc_email_id = context['bcc_email'],
                    email_subject = context['subject'],
                    body = body,
                    send_date = None,
                    mode = self.mode,    
                )
                email_queue.append(email)
        return email_queue
    
    def log_email_sent(self, email_queue) :
        current_time = timezone.now()
        for email in email_queue :
            email.send_date = timezone.now()
        EmailQueue.objects.bulk_update(email_queue, ['send_date'])

    def send_email(self):
        print("send-mail mode:- "+ self.mode)
        if self.mode != 'B' :   
            gateway = get_email_gateway_info()
            template_lang = self.get_email_template_lang()
            context = self.get_context()
            email_subject = template_lang.email_subject            
            body = self.get_email_body(template_lang, context)
            recepients = self.get_recepients()
            _cc_recepients = self.get_cc_recepients()
            email_queue = self.log_email_queue(gateway, recepients, template_lang, body, context)        
        
            _cc = '' if not self.email_template.cc_email else self.email_template.cc_email            
            cc_recepients = '' if not _cc_recepients else _cc +','+ _cc_recepients

            bcc = None if not self.email_template.bcc_email else self.email_template.bcc_email.split(',')           
            cc = None if not cc_recepients else cc_recepients.split(',')
        with get_connection(
            host=gateway.host, 
            port=gateway.port, 
            username=gateway.user_name, 
            password=gateway.pass_word
        ) as connection:
            email = EmailMessage(
                email_subject, 
                body, 
                gateway.user_name, 
                recepients, 
                bcc=bcc, 
                cc=cc, 
                connection=connection
            )
            email.content_subtype = 'html'
            email.send()
        self.log_email_sent(email_queue)


'''
Base class for all sms templates
'''
class BaseSMS :

    def __init__(self, sms_template = None, context = None, lang = DEFAULT_LANGUAGE, mode='E') :
        self.sms_template = sms_template
        self.context = context
        self.lang = lang
        self.mode = mode

    # Get template info according to preferred language
    def get_sms_template_lang(self) :
        if len(self.sms_template.sms_template_lang) > 0 :
            sms_template_lang = self.sms_template.sms_template_lang[0]
            return sms_template_lang
        else :
            sms_template_lang = SMSTemplateLanguage.objects \
                .filter(notification_sms_template=self.sms_template, lang=DEFAULT_LANGUAGE)
            if len(sms_template_lang) > 0 :
                return sms_template_lang[0]
        raise SMSTemplateLanguageNotAvailble

    # Get email body.
    def get_sms_content(self, sms_template_lang, context) :
        raise NotImplementedError

    def get_recepients(self) :
        if self.mode != 'B':
            mobile_no = self.context['user'].mobile_no
            if mobile_no != None :
                return (mobile_no,)
        else :
            mobile_no = self.context['mobile_no']
            if mobile_no != None :
                return (mobile_no,)
        return []

    def get_cc_recepients(self, recepients) :
        sys_configs = SystemConfiguration.objects.filter(code_name__in=[SMS_CC_SWITCH, SMS_CC_RECEPIENTS])
        _cc_conf = None
        for config in sys_configs :
            if config.code_name == SMS_CC_SWITCH  :
                if int(config.field_value) != defaults.sms_cc_switch['ON'] :
                    return recepients
            if config.code_name == SMS_CC_RECEPIENTS :
                _cc_conf = config 
        
        if not hasattr(_cc_conf, 'field_value') or _cc_conf.field_value == None or _cc_conf.field_value == '' :
            return recepients
        return list(recepients) + _cc_conf.field_value.split(',')

    def relay_sms(self, gateway, recepient, content) :
        content = quote(content)
        
        sms_url = gateway.sms_url
        # if self.lang != DEFAULT_LANGUAGE :      
        #     sms_url = gateway.sms_url +"&type=U"       
       
        URL = sms_url + "&sender=" + str(gateway.user_name) + "&to=" + str(recepient) + \
            "&message=" + str(content)
        
        res = requests.get(url = URL) 
        response = res.json()
        message_id = None
        status = 0
        if response['status'] == 'OK' :
            status = 1
            message_id = response['data'][0]['id']
        return status, message_id

    def log_sms_queue(self, gateway, recepients, content) :
        sms_queue = []
        sms_url = gateway.sms_url  

        for r in recepients : 
            relay_status, message_id = self.relay_sms(gateway, r, content)
            # if self.lang != DEFAULT_LANGUAGE :      
            #     sms_url = gateway.sms_url +"&type=U" 
            sms = SMSQueue.objects.create(
                notification_sms_template = self.sms_template,
                lang = self.lang,
                content = content,
                mobile_no = r,
                sms_url = sms_url,
                relay_date = timezone.now(), 
                message_id = message_id,
                relay_status = relay_status, 
                mode = self.mode,
            )
            sms_queue.append(sms)           
        return sms_queue
   
    def send_sms(self):
        gateway = get_sms_gateway_info()
        template_lang = self.get_sms_template_lang()
        content = self.get_sms_content(template_lang, self.context)
        recepients = self.get_recepients()
        recepients = self.get_cc_recepients(recepients)
        sms_queue = self.log_sms_queue(gateway, recepients, content)


'''
Base class for all app templates
'''
class BaseInAppNotification :

    def __init__(self, in_app_template, context = None, lang = DEFAULT_LANGUAGE, mode='E') :
        self.in_app_template = in_app_template
        self.context = context
        self.lang = lang
        self.mode = mode

    # Get template info according to preferred language
    def get_in_app_template_lang(self) :
        if len(self.in_app_template.in_app_template_lang) > 0 :
            in_app_template_lang = self.in_app_template.in_app_template_lang[0]
            return in_app_template_lang
        else :
            in_app_template_lang = NotificationAppTemplateLanguage.objects \
                .filter(notification_app_template=self.in_app_template, lang=DEFAULT_LANGUAGE)
            if len(in_app_template_lang) > 0 :
                return in_app_template_lang[0]
        raise NotImplementedError

    # Get App Notification body.
    def get_notification_content(self, in_app_template_lang, context) :        
        raise NotImplementedError

    def get_recepient(self) :
        if self.mode != 'B':
            user = self.context['user']
            user_id = user.id
            user_name = user.user_name
        else :
            user_id = self.context['user_id']
            user_name = self.context['user_name']  
        return user_id, user_name

    def get_firebase_device_id(self, in_app_notification) :
        return in_app_notification.user.firebase_device_id

    """
    def push_notification(self, in_app_notification) :
        if self.mode != 'B':
            firebase_device_id = self.get_firebase_device_id(in_app_notification)           
        else :
            firebase_device_id = self.context['firebase_device_id']

        if firebase_device_id == None or firebase_device_id == '' :
            if self.mode == 'B':
                in_app_notification.relay_date = timezone.now()
                in_app_notification.save(update_fields=['relay_date'])
            return
        gateway = get_in_app_gateway_info()
        body = {
            'notification': {
                'title' : in_app_notification.title,
                'body' : in_app_notification.content
            },
            'data' : {
                'unread_count' : NotificationAppQueue.objects.filter(user=in_app_notification.user, is_read=False).count(),
                'timestamp' : str(datetime.now(pytz.timezone(in_app_notification.user.time_zone)))
            },
            'to' : firebase_device_id
        }
        _server_key = gateway.server_key
        if in_app_notification.notification_app_template_id == 78 :
            _server_key = 'AAAAtZYt13s:APA91bEhs2m04o7qWATx3oU528dxUZ27YdsycgikG-f3WfX3PXdrQtxv5IIw6Rj259R4CBSk_55p7fCXoPl5vEOYgtMpspl8_9F8Yds1W2y6O3MldTo_M0tNqghOu2WLYP9DL3LgxmBF'
        headers = {'content-type': 'application/json', 'Authorization' : 'key='+str(_server_key)}
        response = requests.post(gateway.url, data=json.dumps(body), headers=headers)
        if response.status_code == 200 or self.mode == 'B':
            in_app_notification.relay_date = timezone.now()
        in_app_notification.relay_status = response.status_code
        in_app_notification.save(update_fields=['relay_date', 'relay_status'])
    """

    def log_app_notification_queue(self, template_lang, content) :       
        recepient, user_name = self.get_recepient()
        if self.mode != 'B':
            title = template_lang.title
        else :
            title = self.context['subject']

        in_app_notification = NotificationAppQueue.objects.create(
            notification_app_template = self.in_app_template,
            lang = self.lang,
            title = title,
            content = content,
            user_id = recepient,
            is_read = False,
            created_by_id = recepient,
            created_user = user_name,
            mode = self.mode,
            relay_date = timezone.now()
        )
        # Relay date updated for this only,if push notification need to implement, then remove form here
        # self.push_notification(in_app_notification)
        return in_app_notification

    def generate_notification(self):
        # gateway = get_in_app_gateway_info()
        template_lang = self.get_in_app_template_lang()
        content = self.get_notification_content(template_lang, self.context)
        in_app_notification = self.log_app_notification_queue(template_lang, content)

     