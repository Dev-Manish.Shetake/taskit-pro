# BULK_NOTIFICATION = 'bulk_notification'
# RESEND_BULK_NOTIFICATION = 'resend_bulk_notification'
BACKOFFICE_REGISTER = 'registration_backoffice_user'
REGISTER_USER = 'register_user'
USER_ACCOUNT_ACTIVATED = 'user_account_activated'
FORGOT_PASSWORD = 'forgot_password'
RESET_PASSWORD_SUCCESS = 'reset_password_success'
CHANGED_PASSWORD_SUCCESS= 'changed_password_success'

MOBILE_VERIFICATION = 'mobile_verification'

GIG_PUBLISH_REJECTED = 'gig_publish_rejected'
GIG_PUBLISH_APPROVED = 'gig_publish_approved'
GIG_MODIFICATION_REQUIRED = 'gig_modification_required'

POST_REQUEST_SUBMIT = 'post_request_submit'
POST_REQUEST_APPROVED_TO_BUYER = 'post_request_approved_to_buyer'
POST_REQUEST_APPROVED_TO_SELLER = 'post_request_approved_to_seller'
POST_REQUEST_REJECTED = 'post_request_rejected'
REQUEST_OFFER_SUBMIT = 'request_offer_submit'
REQUEST_OFFER_REJECTED = 'request_offer_rejected'

ORDER_SUBMIT_TO_SELLER = 'order_submit_to_seller'
ORDER_SUBMIT_TO_BUYER = 'order_submit_to_buyer'
ORDER_ADDITIONAL_REQUIREMENT = 'order_additional_requirement'
ORDER_SUBMIT_REVIEW = 'order_submit_review'
ORDER_MODIFICATION_REQUIRED = 'order_modification_required'
ORDER_COMPLETED_TO_BUYER = 'order_completed_to_buyer'
ORDER_COMPLETED_TO_SELLER = 'order_completed_to_seller'
ORDER_PAYMENT_FAILED = 'order_payment_failed'
ORDER_CANCELLED_BY_BUYER_TO_BUYER = 'order_cancelled_by_buyer_to_buyer'
ORDER_CANCELLED_BY_BUYER_TO_SELLER = 'order_cancelled_by_buyer_to_seller'
ORDER_CANCELLED_BY_SELLER_TO_BUYER = 'order_cancelled_by_seller_to_buyer'
ORDER_CANCELLED_BY_SELLER_TO_SELLER = 'order_cancelled_by_seller_to_seller'



