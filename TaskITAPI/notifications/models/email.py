from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from django.contrib.auth.models import Group
from notifications.models import Notification
# from bulk_notifications.models import BulkNotificationRecepient


class EmailTemplate(models.Model) :
    notification = models.ForeignKey(Notification,on_delete=models.DO_NOTHING)
    cc_email = models.CharField(max_length=1000,null=True,blank=True)
    bcc_email = models.CharField(max_length=1000,null=True,blank=True)
    script = models.CharField(max_length=4000, null=True, blank=True)

    class Meta :
        db_table = 'notification_email_template'
        default_permissions = ()



class EmailTemplateLanguage(models.Model) :
    notification_email_template = models.ForeignKey(EmailTemplate,on_delete=models.DO_NOTHING)
    lang = models.CharField(max_length=10)
    email_subject = models.CharField(max_length=200)
    template_path = models.CharField(max_length=500)
     
    class Meta :
        db_table = 'notification_email_template_lang'
        unique_together = ['notification_email_template','lang'] 
        default_permissions = ()



class EmailQueue(SoftDeleteModel) :
    notification_email_template = models.ForeignKey(EmailTemplate,on_delete= models.DO_NOTHING)
    lang = models.CharField(max_length=10)
    mode = models.CharField(max_length=1,null=True,blank=True)
    from_email_id = models.CharField(max_length=100,null=True,blank=True)
    to_email_id = models.CharField(max_length=100,null=True,blank=True)
    cc_email_id = models.CharField(max_length=100,null=True,blank=True)
    bcc_email_id = models.CharField(max_length=100,null=True,blank=True)
    email_subject = models.CharField(max_length=200)
    body = models.TextField()
    relay_type = models.BooleanField(null=True,blank=True)
    send_date = models.DateTimeField(null=True,blank=True)
    read_date = models.DateTimeField(null=True,blank=True)
    request_offer_id = models.IntegerField(null=True)
    # bulk_notification_recipient = models.ForeignKey(BulkNotificationRecepient,on_delete=models.DO_NOTHING,null=True)

    class Meta :
        db_table = 'notification_email_queue'
        default_permissions = ()



class EmailGateway(models.Model) :
    host = models.CharField(max_length=50)
    port = models.CharField(max_length=50)
    user_name = models.CharField(max_length=50)
    pass_word = models.CharField(max_length=50)
    from_account = models.CharField(max_length=100)
    ssl = models.BooleanField()
    priority = models.SmallIntegerField(null=True)
    relay_count = models.IntegerField()
    created_by = models.ForeignKey(User,on_delete=models.DO_NOTHING, related_name = 'notification_email_gateway_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='notification_email_gateway_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='notification_email_gateway_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'notification_email_gateway'
        default_permissions = ()
