from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from django.contrib.auth.models import Group
from rest_framework.exceptions import ValidationError

def validate_email_day(value) :
    if value <= 0 or value >= 31 :
        raise ValidationError('Enter value between 1 to 31.',code='invalid')  

class Notification(SoftDeleteModel) :
    name = models.CharField(max_length=100)
    event_code = models.CharField(max_length=50)
    description = models.CharField(max_length=200)
    script = models.CharField(max_length=200)
    is_scheduled = models.BooleanField(default=False)
    occurance = models.CharField(max_length=10)
    week_days = models.CharField(max_length=100)
    email_day = models.SmallIntegerField(null=True, blank=True, validators = (validate_email_day,))
    email_custom = models.CharField(max_length=100)
    scheduled_time = models.CharField(max_length=5)
    is_app_notification = models.BooleanField()
    is_email_notification = models.BooleanField()
    is_sms_notification = models.BooleanField()
    seq_no = models.IntegerField(null=True)
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'Notification_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='Notification_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='Notification_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'notification'
        default_permissions = ()
 
