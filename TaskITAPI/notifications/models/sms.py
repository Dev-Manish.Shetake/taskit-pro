from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from django.contrib.auth.models import Group
from notifications.models import Notification
# from bulk_notifications.models import BulkNotificationRecepient


class SMSTemplate(models.Model) :
    notification = models.ForeignKey(Notification,on_delete=models.DO_NOTHING)
    script = models.CharField(max_length=4000, null=True)  

    class Meta :
        db_table = 'notification_sms_template'      
        default_permissions = ()



class SMSTemplateLanguage(models.Model) :
    notification_sms_template = models.ForeignKey(SMSTemplate,on_delete=models.DO_NOTHING)
    lang = models.CharField(max_length=10)
    content = models.TextField()

    class Meta :
        db_table = 'notification_sms_template_lang'
        unique_together = ['notification_sms_template','lang']
        default_permissions = ()



class SMSQueue(SoftDeleteModel) :
    notification_sms_template = models.ForeignKey(SMSTemplate,on_delete=models.DO_NOTHING)
    lang = models.CharField(max_length=10)
    mode = models.CharField(max_length=1,null=True,blank=True)
    content = models.TextField()
    mobile_no = models.CharField(max_length=50) 
    sms_url = models.CharField(max_length=255,null=True,blank=True)
    relay_date = models.DateTimeField(null=True,blank=True)
    relay_status = models.IntegerField(null=True,blank=True)
    message_id = models.CharField(max_length=200,null=True,blank=True)
    # bulk_notification_recipient = models.ForeignKey(BulkNotificationRecepient,on_delete=models.DO_NOTHING,null=True)
    # created_by = models.ForeignKey(User,on_delete=models.DO_NOTHING)      
    # created_user = models.CharField(max_length=50)
    # created_date = models.DateTimeField(auto_now_add=True) 

    class Meta :
        db_table = 'notification_sms_queue'
        default_permissions = ()



class SMSGateway(SoftDeleteModel) :
    sms_url = models.CharField(max_length=255,unique=True)
    user_name = models.CharField(max_length=50,null=True,blank=True)
    pass_word = models.CharField(max_length=50,null=True,blank=True)
    relay_count = models.IntegerField()
    created_by = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name = 'notification_sms_gateway_created_by')
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='notification_sms_gateway_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='notification_sms_gateway_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'notification_sms_gateway'
        default_permissions = ()
