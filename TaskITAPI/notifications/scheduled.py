from notifications import events
from django.db import connection, transaction
from TaskITAPI.serializers import StoredProcedureSerializer
from celery import shared_task, task
from django.utils.module_loading import import_string

################################################################################################

def run_stored_procedure_related_tasks(notification, sp_name):
    print("SP-Name:" + sp_name)
    with connection.cursor() as cursor:
        cursor.callproc(sp_name, [])
        dataset = StoredProcedureSerializer(cursor, many=True).data
        # dataset = get_precontract_scheduled_data(notification, dataset)
        for data in dataset:          
            CustomNotification = import_string(notification.script)
            CustomNotification(notification, data).send_notification()

################################################################################################

@shared_task
def get_notification_info(notification):
    print("Running Event:" + notification.event_code)
    if notification.event_code == events.POST_REQUEST_APPROVED_TO_SELLER:
        run_stored_procedure_related_tasks(
            notification, "sp_job_notifytoseller"
        )
