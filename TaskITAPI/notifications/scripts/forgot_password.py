from notifications.base import BaseNotification,BaseMail,BaseSMS
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data


class ForgotPasswordNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        user = self.context['user']
        # lang = self.context['user'].lang
        expiry_time = (datetime.now(pytz.timezone(user.time_zone)) + timedelta(minutes=15)).strftime('%d-%m-%Y %H:%M')

        context = {
            'user' : user,           
            'otp' : self.context['otp'],
            'expiry_time' : expiry_time,
        }
        context = get_web_related_data(context)  
        return context


class ForgotPasswordMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['user']
        return (user.email_address,)

