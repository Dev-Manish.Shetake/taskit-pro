from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from notifications.utils import get_web_related_data
from notifications import services
from notifications.models import EmailTemplateLanguage
from babel.numbers import format_currency,format_number
from notifications.models import EmailTemplateLanguage
from TaskITAPI.settings import BASE_URL

class GigPublishApprovedNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        gig_draft = self.context['gig_draft']
        click_here = BASE_URL + '/gigs_details?share_id=' + str(gig_draft.gig_id)

        context = {
            'gig_draft' : gig_draft,  
            'seller' : gig_draft.created_by,
            'click_here' : click_here                       
        }
        context = get_web_related_data(context)  
        return context
        

class GigPublishApprovedMail(BaseMail) :

    def get_email_template_lang(self) :
        if len(self.email_template.email_template_lang) > 0 :
            email_template_lang = self.email_template.email_template_lang[0]
            email_template_lang.email_subject = email_template_lang.email_subject.format(gig_title=self.context['gig_draft'].title)
            return email_template_lang
        else :
            email_template_lang = EmailTemplateLanguage.objects \
                .filter(notification_email_template=self.email_template, lang=DEFAULT_LANGUAGE)
            if len(email_template_lang) > 0 :
                email_template_lang = email_template_lang[0]
                email_template_lang.email_subject = email_template_lang.email_subject.format(gig_title=self.context['gig_draft'].title)
                return email_template_lang

    def get_recepients(self) :
        user = self.context['seller']
        return (user.email_address,)


class GigPublishApprovedAppNotification(BaseInAppNotification) :

    def get_in_app_template_lang(self) :
        if len(self.in_app_template.in_app_template_lang) > 0 :
            in_app_template_lang = self.in_app_template.in_app_template_lang[0]
            in_app_template_lang.title = in_app_template_lang.title.format(gig_title=self.context['gig_draft'].title)
            return in_app_template_lang
        else :
            in_app_template_lang = NotificationAppTemplateLanguage.objects \
                .filter(notification_app_template=self.in_app_template, lang=DEFAULT_LANGUAGE)
            if len(in_app_template_lang) > 0 :
                in_app_template_lang = in_app_template_lang[0]
                in_app_template_lang.title = in_app_template_lang.title.format(gig_title=self.context['gig_draft'].title)
                return in_app_template_lang
        raise NotImplementedError

    def get_notification_content(self, in_app_template_lang, context) :      
        content = in_app_template_lang.content.format(
            gig_title = context['gig_draft'].title,
            click_here = context['click_here']
        )        
        return content

    def get_recepient(self) :
        user_id = self.context['seller'].id
        user_name = self.context['seller'].user_name
        return user_id, user_name