from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from datetime import datetime, timedelta
import pytz
from notifications.models import EmailTemplateLanguage
from notifications.utils import get_web_related_data
from notifications import services
from TaskITAPI.settings import BASE_URL
from accounts.models import User
from accounts.values import USER_TYPE


# ######################## Notification To Buyer ###############################

class OrderAdditionalRequirementNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        order = self.context['order']
        click_here = BASE_URL + '/seller_orders'
        context = {
            'order' : order,    
            'buyer' : order.buyer,
            'seller' : order.seller,
            'click_here' : click_here                 
        }
        context = get_web_related_data(context)  
        return context


class OrderAdditionalRequirementMail(BaseMail) :

    def get_email_template_lang(self) :
        if len(self.email_template.email_template_lang) > 0 :
            email_template_lang = self.email_template.email_template_lang[0]
            email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
            return email_template_lang
        else :
            email_template_lang = EmailTemplateLanguage.objects \
                .filter(notification_email_template=self.email_template, lang=DEFAULT_LANGUAGE)
            if len(email_template_lang) > 0 :
                email_template_lang = email_template_lang[0]
                email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
                return email_template_lang

    def get_recepients(self) :
        user = self.context['seller']
        return (user.email_address,)
