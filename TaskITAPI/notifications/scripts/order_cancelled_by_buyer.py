from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from notifications.utils import get_web_related_data
from notifications import services
from notifications.models import EmailTemplateLanguage
from babel.numbers import format_currency,format_number
from settings.models import SystemConfiguration
from settings.values import configs


# ######################## Notification To Buyer ###############################

class OrderCancelledByBuyerToBuyerNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        order = self.context['order']
        sys_config = SystemConfiguration.objects.filter(code_name=configs.PAYMENT_REFUND_PERIOD)
      
        context = {
            'order' : order,   
            'buyer' : order.buyer,
            'seller' : order.seller,
            'amount' : format_number(int(order.amount),locale='en_IN'),
            'payment_days' : sys_config[0].field_value         
        }
        context = get_web_related_data(context)  
        return context

class OrderCancelledByBuyerToBuyerMail(BaseMail) :

    def get_email_template_lang(self) :
        if len(self.email_template.email_template_lang) > 0 :
            email_template_lang = self.email_template.email_template_lang[0]
            email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
            return email_template_lang
        else :
            email_template_lang = EmailTemplateLanguage.objects \
                .filter(notification_email_template=self.email_template, lang=DEFAULT_LANGUAGE)
            if len(email_template_lang) > 0 :
                email_template_lang = email_template_lang[0]
                email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
                return email_template_lang

    def get_recepients(self) :
        user = self.context['buyer']
        return (user.email_address,)


# ######################## Notification To Seller ###############################

class OrderCancelledByBuyerToSellerNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        order = self.context['order']
        sys_config = SystemConfiguration.objects.filter(code_name=configs.PAYMENT_REFUND_PERIOD)

        context = {
            'order' : order,   
            'buyer' : order.buyer,
            'seller' : order.seller,
            'amount' : format_number(int(order.amount),locale='en_IN'),
            'payment_days' : sys_config[0].field_value      
        }
        context = get_web_related_data(context)  
        return context


class OrderCancelledByBuyerToSellerMail(BaseMail) :

    def get_email_template_lang(self) :
        if len(self.email_template.email_template_lang) > 0 :
            email_template_lang = self.email_template.email_template_lang[0]
            email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
            return email_template_lang
        else :
            email_template_lang = EmailTemplateLanguage.objects \
                .filter(notification_email_template=self.email_template, lang=DEFAULT_LANGUAGE)
            if len(email_template_lang) > 0 :
                email_template_lang = email_template_lang[0]
                email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
                return email_template_lang

    def get_recepients(self) :
        user = self.context['seller']
        return (user.email_address,)        