from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data
from notifications.models import EmailTemplateLanguage
from notifications import services
from TaskITAPI.settings import BASE_URL
from accounts.models import User
from accounts.values import USER_TYPE
from gigs.models import GigGallery
from orders.models import OrderPriceExtraService,OrderSubCategoryPriceDetails
from configs.values import GALLERY_TYPE
from babel.numbers import format_currency,format_number


# ######################## Notification To Buyer ###############################

class OrderSubmitToBuyerNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        order = self.context['order']
        gig = order.gig

        gig_gallery = GigGallery.objects.filter(gig=gig,gig_gallery_type=GALLERY_TYPE['Photo'])    
        order_price_extra_services = OrderPriceExtraService.objects.filter(order=order)
        order_price_scope_dtl = OrderSubCategoryPriceDetails.objects.filter(order=order,sub_category_price_scope__price_scope_name__in=['Revisions'])
        revisions = 0
        if len(order_price_scope_dtl) > 0 :
            revisions =  order_price_scope_dtl[0].value
        context = {
            'order' : order,    
            'amount' : format_number(int(order.amount),locale='en_IN'),
            'extra_service_amount' : format_number(int(order.extra_service_amount),locale='en_IN'),
            'total_amount' : format_number(int(order.total_amount),locale='en_IN'),        
            'buyer' : order.buyer,
            'gig_title' : gig.title,
            'gig_profile_pic_url' : BASE_URL +'/media/'+ str(gig_gallery[0].file_path_thumbnail),
            'revisions' : revisions,
            'delivery_time' : order.delivery_time, 
            'order_price_extra_services_list' : list(order_price_extra_services)
        }
        context = get_web_related_data(context)  
        return context


class OrderSubmitToBuyerMail(BaseMail) :

    def get_email_template_lang(self) :
        if len(self.email_template.email_template_lang) > 0 :
            email_template_lang = self.email_template.email_template_lang[0]
            email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
            return email_template_lang
        else :
            email_template_lang = EmailTemplateLanguage.objects \
                .filter(notification_email_template=self.email_template, lang=DEFAULT_LANGUAGE)
            if len(email_template_lang) > 0 :
                email_template_lang = email_template_lang[0]
                email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
                return email_template_lang 

    def get_recepients(self) :
        user = self.context['buyer']
        return (user.email_address,)


# ######################## Notification To Seller ###############################


class OrderSubmitToSellerNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        order = self.context['order']
        gig = order.gig

        gig_gallery = GigGallery.objects.filter(gig=gig,gig_gallery_type=GALLERY_TYPE['Photo'])    
        order_price_extra_services = OrderPriceExtraService.objects.filter(order=order)
        order_price_scope_dtl = OrderSubCategoryPriceDetails.objects.filter(order=order,sub_category_price_scope__price_scope_name__in=['Revisions'])
        revisions = 0
        if len(order_price_scope_dtl) > 0 :
            revisions =  order_price_scope_dtl[0].value
        
        context = {
            'order' : order,   
            'amount' : order.amount,
            'extra_service_amount' : order.extra_service_amount,
            'total_amount' : order.total_amount, 
            'seller' : order.seller,          
            'buyer' : order.buyer,
            'gig_title' : gig.title,
            'gig_profile_pic_url' : BASE_URL +'/media/'+ str(gig_gallery[0].file_path_thumbnail),
            'revisions' : revisions,
            'delivery_time' : order.delivery_time,        
            'order_price_extra_services_list' : list(order_price_extra_services)
        }
        context = get_web_related_data(context)  
        return context


class OrderSubmitToSellerMail(BaseMail) :

    def get_email_template_lang(self) :
        if len(self.email_template.email_template_lang) > 0 :
            email_template_lang = self.email_template.email_template_lang[0]
            email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
            return email_template_lang
        else :
            email_template_lang = EmailTemplateLanguage.objects \
                .filter(notification_email_template=self.email_template, lang=DEFAULT_LANGUAGE)
            if len(email_template_lang) > 0 :
                email_template_lang = email_template_lang[0]
                email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
                return email_template_lang 

    def get_recepients(self) :
        user = self.context['seller']
        return (user.email_address,)