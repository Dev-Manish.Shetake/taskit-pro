from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from notifications.utils import get_web_related_data
from TaskITAPI.settings import BASE_URL
from notifications.models import EmailTemplateLanguage


class OrderSubmitReviewNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        order = self.context['order']
        click_here = BASE_URL + '/buyer_orders'

        context = {
            'order' : order, 
            'buyer' : order.buyer,
            'seller' : order.seller,
            'click_here' : click_here     
        }
        context = get_web_related_data(context)  
        return context



class OrderSubmitReviewMail(BaseMail) :

    def get_email_template_lang(self) :
        if len(self.email_template.email_template_lang) > 0 :
            email_template_lang = self.email_template.email_template_lang[0]
            email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
            return email_template_lang
        else :
            email_template_lang = EmailTemplateLanguage.objects \
                .filter(notification_email_template=self.email_template, lang=DEFAULT_LANGUAGE)
            if len(email_template_lang) > 0 :
                email_template_lang = email_template_lang[0]
                email_template_lang.email_subject = email_template_lang.email_subject.format(order_no=self.context['order'].order_no)
                return email_template_lang  

    def get_recepients(self) :
        user = self.context['buyer']
        return (user.email_address,)