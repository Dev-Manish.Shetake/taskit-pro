from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data
from notifications import services
from TaskITAPI.settings import BASE_URL

# ######################### Seller Notification ##########################
class PostRequestApprovedToSellerNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :              
        click_here = BASE_URL + '/seller_request'
        context = {
            'seller_id' : self.context['seller_id'],
            'seller_user_name' : self.context["seller_user_name"],
            'buyer_user_name' : self.context["buyer_user_name"],
            'email_address' : self.context['email_address'],
            'click_here' : click_here           
        }
        context = get_web_related_data(context) 
        return context


class PostRequestApprovedToSellerMail(BaseMail) :

    def get_recepients(self) :
        email_address = self.context["email_address"]
        return (email_address,)        


class PostRequestApprovedToSellerAppNotification(BaseInAppNotification) :
    
    def get_notification_content(self, in_app_template_lang, context) :      
        content = in_app_template_lang.content.format(
            seller_user_name = context["seller_user_name"],
            buyer_user_name = context["buyer_user_name"],
            click_here = context["click_here"]
        )        
        return content        

    def get_recepient(self) :
        user_id = self.context["seller_id"]
        user_name = self.context["seller_user_name"]
        return user_id, user_name


# ######################### Buyer Notification ##########################


class PostRequestApprovedToBuyerNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        _request = self.context['request']
        buyer = _request.created_by
        
        context = {
            'request' : _request,
            'buyer' : buyer                               
        }
        context = get_web_related_data(context)  
        return context


class PostRequestApprovedToBuyerMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['buyer']
        return (user.email_address,)        


class PostRequestApprovedToBuyerAppNotification(BaseInAppNotification) :
    
    def get_notification_content(self, in_app_template_lang, context) :      
        content = in_app_template_lang.content.format(
            buyer_user_name = context['buyer'].user_name,
            request_description = context['request'].description            
        )        
        return content

    def get_recepient(self) :
        user_id = self.context['buyer'].id
        user_name = self.context['buyer'].user_name
        return user_id, user_name