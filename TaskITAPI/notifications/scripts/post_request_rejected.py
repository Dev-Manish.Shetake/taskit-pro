from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data
from notifications import services


class PostRequestRejectedNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        _request = self.context['request']
        buyer = _request.created_by      
      
        context = {
            'buyer' : buyer,  
            'request' : _request,          
        }
        context = get_web_related_data(context)  
        return context


class PostRequestRejectedMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['buyer']
        return (user.email_address,)        


class PostRequestRejectedAppNotification(BaseInAppNotification) :
    
    def get_notification_content(self, in_app_template_lang, context) :      
        content = in_app_template_lang.content.format(
            buyer_user_name = context['buyer'].user_name,
            request_description = context['request'].description
        )        
        return content

    def get_recepient(self) :
        user_id = self.context['buyer'].id
        user_name = self.context['buyer'].user_name
        return user_id, user_name