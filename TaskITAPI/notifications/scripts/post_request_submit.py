from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data
from notifications import services
from TaskITAPI.settings import BASE_URL
from accounts.models import User
from accounts.values import USER_TYPE
from settings.models import SystemConfiguration
from settings.values.configs import DEFAULT_BO_USER

class PostRequestSubmitNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        request = self.context['request']
        sys = SystemConfiguration.objects.filter(code_name=DEFAULT_BO_USER)
        user = User.objects.get(id=sys[0].field_value)
        
        buyer = request.created_by 
        click_here = BASE_URL + "/admin_post_a_request"

        context = {
            'user_name' : buyer.user_name,
            'user' : user,
            'click_here' : click_here,                      
        }
        context = get_web_related_data(context)  
        return context


class PostRequestSubmitMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['user']
        return (user.email_address,)        


class PostRequestSubmitAppNotification(BaseInAppNotification) :
    
    def get_notification_content(self, in_app_template_lang, context) :      
        content = in_app_template_lang.content.format(
            user_name = context['user_name'],
            click_here = context['click_here']
        )        
        return content

    def get_recepient(self) :
        user_id = self.context['user'].id
        user_name = self.context['user'].user_name
        return user_id, user_name