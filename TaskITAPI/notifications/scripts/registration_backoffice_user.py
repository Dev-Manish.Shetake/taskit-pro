from notifications.base import BaseNotification,BaseMail,BaseSMS
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data
from notifications.utils import get_verification_link
from TaskITAPI.settings import VERIFICATION_LINK_EXPIRE_DAYS
from notifications import services


class BackofficeRegisterNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        user = self.context['user']
        expiry_time = (datetime.now(pytz.timezone(user.time_zone)) + timedelta(days=VERIFICATION_LINK_EXPIRE_DAYS)).strftime('%d-%m-%Y %H:%M')

        groups = user.groups.all()
        group = groups[0]
        group_name = ""
        if len(groups) > 0 :
            group_name = groups[0].name

        context = {            
            'user' : user,
            'group' : group_name,
            'user_type' :user.user_type,
            'expiry_time' : expiry_time,
            'verification_link' : get_verification_link(user)
        }
        context = get_web_related_data(context)  
        return context


class BackofficeRegisterMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['user']
        return (user.email_address,)


class BackofficeRegisterSMS(BaseSMS) :

    def get_sms_content(self, sms_template_lang, context) :
        content = sms_template_lang.content.format(
            user_name = context['user'].user_name,
            email_address = context['user'].email_address,
            mobile_no = context['user'].mobile_no,
            group_name = context['group'],
            # datetime.now(pytz.timezone(context['user'].time_zone)).strftime('%Y-%m-%d %H:%M'),
            support_email_address = context['support_email_address'],
            support_contact_number = context['support_contact_number'],
        )
        return content

    def get_recepients(self) :
        if self.context['user'].mobile_no :
            return (self.context['user'].mobile_no,)
        return ()