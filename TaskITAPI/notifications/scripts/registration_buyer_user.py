from notifications.base import BaseNotification,BaseMail,BaseSMS
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data
# from notifications.utils import get_verification_link
from TaskITAPI.settings import VERIFICATION_LINK_EXPIRE_DAYS
from notifications import services


class BuyerRegisterNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        user = self.context['user']
        # lang = self.context['user'].lang
        expiry_time = (datetime.now(pytz.timezone(user.time_zone)) + timedelta(days=VERIFICATION_LINK_EXPIRE_DAYS)).strftime('%d-%m-%Y %H:%M')

        groups = user.groups.all()
        group = groups[0]
        group_name = ""
        if len(groups) > 0 :
            group_name = groups[0].name

        context = {
            'user' : user,           
            'verification_link' : self.context['verification_link'],
            'expiry_time' : expiry_time,
        }
        context = get_web_related_data(context)  
        return context


class BuyerRegisterMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['user']
        return (user.email_address,)

