from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data
from notifications import services
from TaskITAPI.settings import BASE_URL
from accounts.models import User
from accounts.values import USER_TYPE


class RequestOfferRejectedNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        request_offer = self.context['request_offer']
        seller = request_offer.seller
        buyer = request_offer.buyer

        context = {
            'buyer' : buyer,
            'seller' : seller,
            'request_description' : request_offer.request.description,                      
        }
        context = get_web_related_data(context)  
        return context


class RequestOfferRejectedMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['seller']
        return (user.email_address,)        


# class RequestOfferRejectedAppNotification(BaseInAppNotification) :
    
#     def get_notification_content(self, in_app_template_lang, context) :      
#         content = in_app_template_lang.content.format(
#             buyer_user_name = context['buyer'].user_name,
#             seller_user_name = context['seller'].user_name,
#             request_description = context['request_description']
#         )        
#         return content

#     def get_recepient(self) :
#         user_id = self.context['seller'].id
#         user_name = self.context['seller'].user_name
#         return user_id, user_name