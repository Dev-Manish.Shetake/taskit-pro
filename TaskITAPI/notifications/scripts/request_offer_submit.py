from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from datetime import datetime, timedelta
import pytz
from notifications.utils import get_web_related_data
from notifications import services
from TaskITAPI.settings import BASE_URL
from accounts.models import User
from accounts.values import USER_TYPE


class RequestOfferSubmitNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        request_offer = self.context['request_offer']
        seller = request_offer.seller
        buyer = request_offer.buyer

        click_here = BASE_URL + "/seller_request"

        context = {
            'buyer' : buyer,
            'seller' : seller,
            'request_description' : request_offer.request.description,       
            'click_here' : click_here               
        }
        context = get_web_related_data(context)  
        return context


class RequestOffersSubmitMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['buyer']
        return (user.email_address,)        


class RequestOfferSubmitAppNotification(BaseInAppNotification) :
    
    def get_notification_content(self, in_app_template_lang, context) :      
        content = in_app_template_lang.content.format(
            buyer_user_name = context['buyer'].user_name,
            seller_user_name = context['seller'].user_name,
            request_description = context['request_description'],
            click_here = context['click_here']
        )        
        return content

    def get_recepient(self) :
        user_id = self.context['buyer'].id
        user_name = self.context['buyer'].user_name
        return user_id, user_name