from notifications.base import BaseNotification,BaseMail,BaseSMS
from notifications.utils import get_web_related_data


class ForgotPasswordSuccessNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        user = self.context['user']
        context = {
            'user' : user
        }
        context = get_web_related_data(context)  
        return context


class ForgotPasswordSuccessMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['user']
        return (user.email_address,)        

# ############################### Changed Password Successfully ########################################


class ChangedPasswordSuccessNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        user = self.context['user']
        context = {
            'user' : user
        }
        context = get_web_related_data(context)  
        return context


class ChangedPasswordSuccessMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['user']
        return (user.email_address,)  