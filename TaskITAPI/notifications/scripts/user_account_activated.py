from notifications.base import BaseNotification,BaseMail,BaseSMS,BaseInAppNotification
from notifications.utils import get_web_related_data



class BuyerAccountActivatedNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        user = self.context['user']
        context = {
            'user' : user          
        }
        context = get_web_related_data(context)  
        return context


class BuyerAccountActivatedMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['user']
        return (user.email_address,)



class BuyerAccountActivatedAppNotification(BaseInAppNotification) :
    
    def get_notification_content(self, in_app_template_lang, context) :      
        content = in_app_template_lang.content.format(
            email_address = context['user'].email_address,
        )        
        return content

    def get_recepient(self) :
        user_id = self.context['user'].id
        user_name = self.context['user'].user_name
        return user_id, user_name
