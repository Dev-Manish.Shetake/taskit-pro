from notifications.base import BaseNotification,BaseMail,BaseSMS
from notifications.utils import get_web_related_data


class MobileVerificationNotification(BaseNotification) :
    
    def get_context_data(self, **kwargs) :
        user = self.context['user']   
        otp = self.context['otp']     
        context = {
            'user' : user,
            'otp' : otp
        }
        context = get_web_related_data(context)  
        return context



class MobileVerificationMail(BaseMail) :

    def get_recepients(self) :
        user = self.context['user']
        return (user.email_address,)


class MobileVerificationOTPSMS(BaseSMS) :

    def get_sms_content(self, sms_template_lang, context) :
        content = sms_template_lang.content.format(
            user_name = context['user'].user_name,
            otp = context['otp'],
            support_email_address = context['support_email_address'],
            support_contact_number = context['support_contact_number'],
        )
        return content


    def get_recepients(self) :
        if self.context['user'] :
            return (self.context['user'].mobile_no,)
        return []