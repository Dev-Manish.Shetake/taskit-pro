from notifications.models import NotificationAppQueue
from rest_framework import serializers


class InAppNotificationsSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()

    def setup_eager_loading(self, queryset) :
        queryset = queryset.only('id', 'content', 'user_id', 'is_read', 'created_date')
        return queryset

    class Meta :
        model = NotificationAppQueue
        fields = [
            'id',
            'content',
            'user_id',
            'is_read',
            'created_date'
        ]   
        read_only_fields = ['content', 'user_id', 'created_date', 'is_read']



class InAppNotificationUpdateSerializer(serializers.Serializer) :
    notifications = InAppNotificationsSerializer(many=True)