from rest_framework import serializers
from notifications.models import Notification
from django.db.models import Prefetch
from django.utils import timezone
from accounts.serializers import GroupSerializer


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = 'notifications/)
'''
class NotificationListSerializer(serializers.ModelSerializer) :

    def setup_eager_loading(self, queryset) :        
        return queryset.order_by('seq_no')

    class Meta :
        model = Notification
        fields = [
            'id',
            'name',        
            'is_scheduled',
            'occurance',
            'week_days',
            'email_day',
            'scheduled_time',                     
            'is_app_notification',
            'is_email_notification',
            'is_sms_notification',
            'is_active',
            'seq_no'          
        ]



'''
Serializer to send data in specific format for GET method
(endpoint = 'notifications/:id')  
'''     
class NotificationDetailSerializer(serializers.ModelSerializer) :
    # email = EmailNotificationSerializer()
    # sms = SMSNotificationSerializer(read_only=True)
    # in_app_notification = AppNotificationSerializer(read_only=True)

    def setup_eager_loading(self, queryset) :              
        return queryset

    class Meta :
        model = Notification
        fields = [
            'id',
            'name',           
            'event_code',
            'description',
            'is_scheduled',
            'occurance',
            'email_day',
            'week_days',
            'is_scheduled',
            'scheduled_time',      
            'is_app_notification',
            'is_email_notification',
            'is_sms_notification',
            # 'email',
            # 'sms',
            # 'in_app_notification',
            'is_active'          
        ]
        read_only_fields = [
            'event_code','is_scheduled',           
        ]

    def update(self, notification, validated_data) :
        notication_allow_to = [] 
        if 'notication_allow_to' in validated_data :
            notication_allow_to = validated_data.pop('notication_allow_to')

        for key, value in validated_data.items() :            
            setattr(notification, key, value)
        notification.modified_by = self.context['request'].user
        notification.modified_user = self.context['request'].user.user_name
        notification.modified_date = timezone.now()    
        notification.save()        
        return notification