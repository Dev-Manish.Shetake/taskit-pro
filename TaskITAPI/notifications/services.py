from accounts.models import User,UserType
from django.contrib.auth.models import Group


def get_translated_group_object(group) :
    group = Group.objects.filter(id=group)
    return group

def get_user_object(user) :
    user = User.objects.filter(id=user)
    return user

def get_translated_user_type(user_type) :
    user_type = UserType.objects.filter(id=user_type)  
    return user_type
