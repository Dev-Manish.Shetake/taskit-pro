# Create your tasks here
from TaskITAPI.settings import DEFAULT_LANGUAGE
from celery import shared_task,task
import time
from django.core.exceptions import ObjectDoesNotExist
from django.utils.module_loading import import_string
from notifications.models import Notification
from notifications.models import EmailQueue, EmailTemplate, EmailTemplateLanguage, EmailGateway,SMSTemplate,SMSTemplateLanguage,SMSGateway
from notifications.models import NotificationAppTemplate, NotificationAppTemplateLanguage
from datetime import datetime
import pytz
from notifications.scheduled import get_notification_info
from notifications import events

@shared_task
def send_email(notification, context, lang=DEFAULT_LANGUAGE, mode='E') :
    print("send mail def:-"+mode)
    email_temp = EmailTemplate.objects.filter(notification=notification)[0]
    email_temp.email_template_lang = EmailTemplateLanguage.objects.filter(lang=lang, notification_email_template=email_temp)
    Email = import_string(email_temp.script)
    email = Email(email_temp, context, lang, mode)
    email.send_email()


@shared_task 
def send_message(notification, context, lang=DEFAULT_LANGUAGE, mode='E') :
    sms_temp = SMSTemplate.objects.filter(notification=notification)[0]
    sms_temp.sms_template_lang = SMSTemplateLanguage.objects.filter(lang=lang, notification_sms_template=sms_temp)
    SMS = import_string(sms_temp.script)
    sms = SMS(sms_temp, context, lang, mode)
    sms.send_sms()

@shared_task
def generate_notification(notification, context, lang=DEFAULT_LANGUAGE, mode='E') :
    app_temp = NotificationAppTemplate.objects.filter(notification=notification)[0]
    app_temp.in_app_template_lang = NotificationAppTemplateLanguage.objects.filter(lang=lang, notification_app_template=app_temp)
    AppNotification = import_string(app_temp.script)
    app_notification = AppNotification(app_temp, context, lang, mode)
    app_notification.generate_notification()

@shared_task
def notify_user(event_code, context, lang=DEFAULT_LANGUAGE) :
    print('Entered in notify user def')
    try :
        notification = Notification.objects.get(event_code=event_code) 
        CustomNotification = import_string(notification.script)
        CustomNotification(notification, context, lang).send_notification()
    except ObjectDoesNotExist :
        pass    


@task()
def scheduled_notifications():
    print("Scheduling Notification!!")
    current_time = datetime.now(pytz.timezone("Asia/Kolkata")).replace(tzinfo=None)
    current_weekday = current_time.strftime("%A").lower()
    current_day = current_time.strftime("%d")
    _notifications = Notification.objects.filter(is_scheduled=True)
    for n in _notifications:
        to_run = False
        if n.occurance == "Daily":
            to_run = True
        elif n.occurance == "Weekly":
            weekdays = [day.strip().lower() for day in n.week_days.split(",")]
            if current_weekday in weekdays:
                to_run = True
        elif n.occurance == "Monthly":
            if n.email_day == int(current_day):
                to_run = True
        if not to_run:
            return
        t = datetime.strptime(n.scheduled_time, "%H:%M").time()
        diff = (
            current_time - datetime.combine(current_time.date(), t)
        ).total_seconds()
        if diff < 60 and diff >= 0:
            get_notification_info.delay(n)


@task()
def approved_request_to_seller_scheduled_notifications():
    _notifications = Notification.objects.filter(is_scheduled=True,event_code=events.POST_REQUEST_APPROVED_TO_SELLER)
    for n in _notifications :
        get_notification_info.delay(n)
