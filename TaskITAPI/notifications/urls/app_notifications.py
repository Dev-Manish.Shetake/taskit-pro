from django.urls import path, include
from notifications.views import app_notifications

urlpatterns = [
    path('', app_notifications.AppNotificationListView.as_view()),
    # path('read', app_notifications.AppNotificationReadView.as_view()),
    # path('unread', app_notifications.AppNotificationUnreadView.as_view()),
    # path('clear', app_notifications.AppNotificationClearView.as_view()),
    # path('count', app_notifications.AppNotificationCountView.as_view()),
]
