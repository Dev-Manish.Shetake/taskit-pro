from django.urls import path, include
from notifications.views import notifications

urlpatterns = [
    path('', notifications.NotificationListView.as_view()),
    path('<int:pk>', notifications.NotificationUpdateView.as_view()),
   
]
