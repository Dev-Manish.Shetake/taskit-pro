from notifications.models import EmailGateway, SMSGateway, NotificationAppGateway
import random, string
from TaskITAPI.settings import SUPPORT_MAIL, SUPPORT_CALL,VERIFICATION_LINK
from accounts.models import UserVerificationCode
from django.utils import timezone 

def get_web_related_data(context) :
    context.update({
        'support_email_address' : SUPPORT_MAIL,
        'support_contact_number' : SUPPORT_CALL,       
    })
    return context


def get_verification_link(user) :
    verification_code = ''.join(random.choices(string.ascii_lowercase +
                        string.digits, k = 16))
    v = UserVerificationCode.objects.create(
        user = user,
        verification_code = verification_code,
        created_date = timezone.now()        
    )
    verification_link = VERIFICATION_LINK + v.verification_code 
    return verification_link


class EmailGatewayNotFound(Exception) :
   pass

class SMSGatewayNotFound(Exception) :
   pass

class NotificationAppGatewayNotFound(Exception) :
   pass


def get_email_gateway_info() :
    # gateway = cache.get(EMAIL_GATEWAY_INFO)
    # if gateway == None :
    gateway = EmailGateway.objects.all()
    if len(gateway) == 0 :
        raise EmailGatewayNotFound 
    gateway = gateway[0]
    # cache.set(EMAIL_GATEWAY_INFO, gateway, EMAIL_GATEWAY_INFO_TIMEOUT)
    return gateway


def get_specific_email_gateway_info(email_address) :
    # gateway = cache.get(EMAIL_GATEWAY_INFO)
    # if gateway == None :
    gateway = EmailGateway.objects.filter(from_account=email_address)
    if len(gateway) < 0 :
        raise EmailGatewayNotFound 
    gateway = gateway[0]
    # cache.set(EMAIL_GATEWAY_INFO, gateway, EMAIL_GATEWAY_INFO_TIMEOUT)    
    return gateway     


def get_sms_gateway_info() :
    # gateway = cache.get(SMS_GATEWAY_INFO)
    # if gateway == None :
    gateway = SMSGateway.objects.all()
    if len(gateway) == 0 :
        raise SMSGatewayNotFound 
    gateway = gateway[0]
    # cache.set(SMS_GATEWAY_INFO, gateway, SMS_GATEWAY_INFO_TIMEOUT)
    return gateway


def get_in_app_gateway_info() :
    # gateway = cache.get(IN_APP_GATEWAY_INFO)
    # if gateway == None :
    gateway = NotificationAppGateway.objects.all()
    if len(gateway) == 0 :
        raise NotificationAppGatewayNotFound 
    gateway = gateway[0]
    # cache.set(IN_APP_GATEWAY_INFO, gateway, IN_APP_GATEWAY_INFO_TIMEOUT)
    return gateway