from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView
from notifications.models import NotificationAppQueue
from notifications.serializers.app_notifications import InAppNotificationsSerializer, InAppNotificationUpdateSerializer
from rest_framework.response import Response
from rest_framework import status
from rest_framework import serializers
from django.utils import timezone
from datetime import datetime, timedelta
import pytz
from django.db.models import Count, Q



class AppNotificationListView(ListAPIView) :
    """
    Get list of app notifications

    Get Query Parameter
    ---------------------------------------
    1. is_read = true/false
    """

    serializer_class = InAppNotificationsSerializer

    def apply_filters(self, queryset) :
        is_read = self.request.GET.get('is_read')
        if is_read == 'true' :
            queryset = queryset.filter(is_read = True)
        elif is_read == 'false' :
            queryset = queryset.filter(is_read = False)
        return queryset

    def get_queryset(self) :
        queryset = NotificationAppQueue.objects.filter(user_id=self.request.user.id)
        queryset = self.apply_filters(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset.order_by('is_read','-created_date')



class AppNotificationReadView(CreateAPIView) :
    """
    Set read tag to particular app notification by ID
    
    To set read tag to app notification by IDs
    """
    serializer_class = InAppNotificationUpdateSerializer

    def create(self, request, *args, **kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        notification_ids = []
        for data in serializer.data['notifications'] :
            notification_ids.append(data['id'])
        
        notifications = NotificationAppQueue.objects.filter(id__in=notification_ids, is_read=False, user_id=self.request.user.id)
        for n in notifications :
            n.is_read = True
        
        notifications = NotificationAppQueue.objects.bulk_update(list(notifications), ['is_read'])

        data = NotificationAppQueue.objects.filter(user_id=self.request.user.id).aggregate(
            notification_count = Count('id', filter=Q(is_read=False)),
            read_count = Count('id', filter=Q(is_read=True))
        )

        data.update({
            'fetched_at' : datetime.now(pytz.timezone(request.user.time_zone))
        })
        return Response(data, status=status.HTTP_200_OK)



class AppNotificationClearView(CreateAPIView) :
    """
    Clear app notification by ID
    
    To clear app notification by ID
    """
    serializer_class = InAppNotificationUpdateSerializer

    def create(self, request, *args, **kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        notification_ids = []
        for data in serializer.data['notifications'] :
            notification_ids.append(data['id'])
        
        notifications = NotificationAppQueue.objects.filter(id__in=notification_ids, user_id=self.request.user.id)
        for n in notifications :
            n.is_active = False
            n.is_read = True
        
        notifications = NotificationAppQueue.objects.bulk_update(list(notifications), ['is_read', 'is_active'])
        
        data = NotificationAppQueue.objects.filter(user_id=self.request.user.id).aggregate(
            notification_count = Count('id', filter=Q(is_read=False)),
            read_count = Count('id', filter=Q(is_read=True))
        )
        data.update({
            'fetched_at' : datetime.now(pytz.timezone(request.user.time_zone))
        })
        return Response(data, status=status.HTTP_200_OK)



class AppNotificationUnreadView(CreateAPIView) :
    """
    Set un-read tag to app notification by ID
    
    To set un-read tag to app notification by ID
    """
    serializer_class = InAppNotificationUpdateSerializer

    def create(self, request, *args, **kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        notification_ids = []
        for data in serializer.data['notifications'] :
            notification_ids.append(data['id'])
        
        notifications = NotificationAppQueue.objects.filter(id__in=notification_ids, is_read=True, user_id=self.request.user.id)
        for n in notifications :
            n.is_read = False
        
        notifications = NotificationAppQueue.objects.bulk_update(list(notifications), ['is_read'])
        data = NotificationAppQueue.objects.filter(user_id=self.request.user.id).aggregate(
            notification_count = Count('id', filter=Q(is_read=False)),
            read_count = Count('id', filter=Q(is_read=True))
        )
        data.update({
            'fetched_at' : datetime.now(pytz.timezone(request.user.time_zone))
        })
        return Response(data, status=status.HTTP_200_OK)



class AppNotificationCountView(RetrieveAPIView) :
    """
    Get app notifications count
    
    To get app notifications count
    """
    class NotificationCountSerializer(serializers.Serializer):
        notification_count = serializers.IntegerField()
        fetched_at = serializers.DateTimeField()

    queryset = NotificationAppQueue.objects.none()
    serializer_class = NotificationCountSerializer

    def get(self, request, *args, **kwargs) :
        data = NotificationAppQueue.objects.filter(user_id=self.request.user.id).aggregate(
            notification_count = Count('id', filter=Q(is_read=False)),
            read_count = Count('id', filter=Q(is_read=True))
        )

        data.update({
            'fetched_at' : datetime.now(pytz.timezone(request.user.time_zone))
        })
        return Response(data, status=status.HTTP_200_OK)                