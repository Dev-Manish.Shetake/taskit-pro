from rest_framework.generics import ListAPIView,RetrieveUpdateAPIView
from notifications.models import Notification
from notifications.serializers import NotificationListSerializer,NotificationDetailSerializer
from django.utils import timezone

'''
Serializer to receive data in specific format for GET method
(endpoint = 'notifications/)
'''
class NotificationListView(ListAPIView) :
    """
    Get all notification list
    
    Get all notification list
    """
    serializer_class = NotificationListSerializer
    queryset = Notification.objects.none()
   
    def get_queryset(self) :
        queryset = Notification.all_objects.all()       
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
Serializer to send/receive data in specific format for GET/PUT/PATCH method
(endpoint = 'notifications/:id)
'''
class NotificationUpdateView(RetrieveUpdateAPIView) :
    """
    Get particular notification details by ID
    
    Get particular notification details by ID
    """
    serializer_class = NotificationDetailSerializer
    queryset = Notification.objects.none()   

            
    def get_queryset(self) :
        queryset = Notification.all_objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset
    
    def delete(self, request, *args, **kwargs):
        notification = self.get_object()
        notification.is_active = False
        notification.deleted_by = request.user
        notification.deleted_user = request.user.user_name
        notification.deleted_date = timezone.now()
        notification.save()
        return Response(status=status.HTTP_204_NO_CONTENT)    