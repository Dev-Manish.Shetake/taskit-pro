# Generated by Django 2.2 on 2021-03-06 16:01

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0011_auto_20210306_1426'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderrating',
            name='buyer_feedback',
            field=models.CharField(max_length=500, null=True),
        ),
        migrations.AlterField(
            model_name='orderrating',
            name='created_by',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, related_name='order_rating_created_by', to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='orderrating',
            name='seller_feedback',
            field=models.CharField(max_length=500, null=True),
        ),
    ]
