# Generated by Django 2.2 on 2021-03-06 16:59

from django.db import migrations, models
import django.db.models.deletion
import orders.models.order_chat


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0012_auto_20210306_1601'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderChat',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_active', models.BooleanField(default=True)),
                ('chat', models.CharField(blank=True, max_length=400, null=True)),
                ('file_path', models.FileField(null=True, upload_to=orders.models.order_chat.order_chat_file_path)),
                ('quantity', models.IntegerField()),
                ('amount', models.DecimalField(decimal_places=2, max_digits=9)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='orders.Order')),
            ],
            options={
                'db_table': 'order_chat',
                'default_permissions': (),
            },
        ),
    ]
