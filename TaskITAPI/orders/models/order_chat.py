from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from orders.models import Order

def order_chat_file_path(instance, filename):
    order_chat = OrderChat.objects.last()
    if order_chat :
        count = order_chat.id + 1
    else :
        count = 1 
    return 'Order/Chat/File_Path/{0}/{1}_{2}'.format(
        instance.order_id,      
        count,      
        filename
        )

# Master OrderChat related data stored in this table
class OrderChat(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)
    chat = models.CharField(max_length=400,null=True)
    file_path = models.FileField(upload_to=order_chat_file_path,null=True) 
    created_by = models.ForeignKey(User,on_delete=models.DO_NOTHING) 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)

    class Meta :
        db_table = 'order_chat'
        default_permissions = ()
        permissions = [
            ('view_order_chat', 'Can view order chat.'),
            ('add_order_chat', 'Can create a order chat.')            
        ]