from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from configs.models import OrderStatus,OrderCancelReason,City,PriceType,VoucherType
from orders.models import OrderPayment,Order
from masters.models import SubCategoryPriceScope,SubCategoryPriceScopeDetails,PriceExtraService
from gigs.models import Gig



# Master FinanceTransaction related data stored in this table
class FinanceTransaction(SoftDeleteModel) : 
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING,null=True)
    order_payment = models.ForeignKey(OrderPayment,on_delete=models.DO_NOTHING,null=True)
    voucher_no = models.CharField(max_length=30)
    voucher_date = models.DateField()
    voucher_type = models.ForeignKey(VoucherType,on_delete=models.DO_NOTHING)
    approved_by = models.ForeignKey(City,on_delete=models.DO_NOTHING,null=True,related_name='finance_transaction_approved_by')
    approved_date = models.DateField(null=True)
    approved_remark = models.CharField(max_length=500)
    RefID = models.IntegerField(null=True)
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='finance_transaction_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='finance_transaction_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='finance_transaction_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'fin_tran'
        default_permissions = ()



# Master FinanceTransactionDetails related data stored in this table
class FinanceTransactionDetails(SoftDeleteModel) :    
    fin_tran = models.ForeignKey(FinanceTransaction,on_delete=models.DO_NOTHING,null=True)
    seq_no = models.IntegerField()
    gen_ledger_id = models.IntegerField()
    gen_ledger_name = models.CharField(max_length=100)
    amount = models.DecimalField(max_digits=9,decimal_places=2)
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING,null=True)
    note = models.CharField(max_length=500,null=True,blank=True)
   
    class Meta :
        db_table = 'fin_tran_dtl'
        default_permissions = () 


class OrderRefundLog(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)
    refunded_amount = models.DecimalField(max_digits=9,decimal_places=2)
    remark = models.CharField(max_length=500,null=True,blank=True)

    class Meta :
        db_table = 'order_refund_log'
        default_permissions = ()
