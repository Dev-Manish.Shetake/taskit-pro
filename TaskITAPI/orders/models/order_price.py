from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from configs.models import OrderStatus,OrderCancelReason,City,PriceType
from orders.models import Order
from masters.models import SubCategoryPriceScope,SubCategoryPriceScopeDetails,PriceExtraService

# Master OrderGigPrice related data stored in this table
class OrderGigPrice(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)
    price_type = models.ForeignKey(PriceType,on_delete=models.DO_NOTHING)    
    price = models.DecimalField(max_digits=9,decimal_places=2)
    quantity =  models.IntegerField()
    amount = models.DecimalField(max_digits=9,decimal_places=2)

    class Meta :
        db_table = 'order_gig_price'
        default_permissions = ()


# Master OrderSubCategoryPriceDetails Last Visited related data stored in this table
class OrderSubCategoryPriceDetails(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)   
    sub_category_price_scope = models.ForeignKey(SubCategoryPriceScope,on_delete=models.DO_NOTHING)
    sub_category_price_scope_dtl = models.ForeignKey(SubCategoryPriceScopeDetails,on_delete=models.DO_NOTHING,null=True)
    value = models.CharField(max_length=200,blank=True) 

    class Meta :
        db_table = 'order_gig_sub_category_price_dtl'
        default_permissions = ()        


# Master OrderPriceExtraService related data stored in this table
class OrderPriceExtraService(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)
    price_extra_service = models.ForeignKey(PriceExtraService,on_delete=models.DO_NOTHING)      
    extra_days = models.IntegerField()
    price = models.DecimalField(max_digits=9,decimal_places=2) 
    quantity = models.IntegerField()
    amount = models.DecimalField(max_digits=9,decimal_places=2) 

    class Meta :
        db_table = 'order_gig_price_extra_service'
        default_permissions = ()


# Master OrderPriceExtraServiceCustom related data stored in this table
class OrderPriceExtraServiceCustom(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    extra_price = models.DecimalField(max_digits=9,decimal_places=2,null=True)    
    extra_days = models.IntegerField(null=True)
    quantity = models.IntegerField()
    amount = models.DecimalField(max_digits=9,decimal_places=2)   

    class Meta :
        db_table = 'order_gig_price_extra_service_custom'
        default_permissions = ()        