from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from configs.models import QuestionForm
from orders.models import Order
from masters.models import SubCategoryPriceScope,SubCategoryPriceScopeDetails,PriceExtraService
from gigs.models import GigRequirement


# Master Order Gig Requirements related data stored in this table
class OrderGigRequirements(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)
    is_mandatory = models.BooleanField()
    question = models.CharField(max_length=4000)
    question_form = models.ForeignKey(QuestionForm,on_delete=models.DO_NOTHING) 
    is_multiselect = models.BooleanField()
    gig_requirement = models.ForeignKey(GigRequirement,on_delete=models.DO_NOTHING)

    class Meta :
        db_table = 'order_gig_requirement'
        default_permissions = ()



def order_req_file_path(instance, filename):
    order_desc = OrderGigRequirementsDetails.objects.last()
    if order_desc :
        count = order_desc.id + 1
    else :
        count = 1 
    return 'Order/Requirements/File_Path/{0}/{1}_{2}'.format(
        instance.order_id,      
        count,      
        filename
        )

# Master Order Gig Requirements Details related data stored in this table
class OrderGigRequirementsDetails(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)
    order_gig_requirement = models.ForeignKey(OrderGigRequirements,on_delete=models.DO_NOTHING) 
    file_path = models.FileField(null=True,upload_to=order_req_file_path)
    description = models.CharField(max_length=100,null=True,blank=True)

    class Meta :
        db_table = 'order_gig_requirement_dtl'
        default_permissions = ()        