from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from configs.models import OrderStatus,OrderCancelReason,City,RefundStatus
from gigs.models import Gig

# Master Order related data stored in this table
class Order(SoftDeleteModel) :   
    order_no = models.CharField(max_length=30,unique=True)
    buyer = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='order_buyer_id')
    seller = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='order_seller_id')
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)
    amount = models.DecimalField(max_digits=9,decimal_places=2)
    extra_service_amount = models.DecimalField(max_digits=9,decimal_places=2)
    extra_service_custom_amount = models.DecimalField(max_digits=9,decimal_places=2)
    service_fee = models.DecimalField(max_digits=9,decimal_places=2)
    total_amount_without_service_fee = models.DecimalField(max_digits=9,decimal_places=2,default=0)
    total_amount = models.DecimalField(max_digits=9,decimal_places=2)
    delivery_time = models.IntegerField()
    billing_address_line1 = models.CharField(max_length=200,null=True)
    billing_address_line2 = models.CharField(max_length=200,null=True)
    billing_landmark = models.CharField(max_length=200,null=True)
    billing_city = models.ForeignKey(City,on_delete=models.DO_NOTHING,null=True,related_name='order_billing_city_id')
    billing_pincode = models.CharField(max_length=6,null=True)
    shipping_address_line1 = models.CharField(max_length=200,null=True)
    shipping_address_line2 = models.CharField(max_length=200,null=True)
    shipping_landmark = models.CharField(max_length=200,null=True)
    shipping_city = models.ForeignKey(City,on_delete=models.DO_NOTHING,null=True,related_name='order_shiping_city_id')
    shipping_pincode = models.CharField(max_length=6,null=True)
    order_status = models.ForeignKey(OrderStatus,on_delete=models.DO_NOTHING)
    order_cancel_reason =  models.ForeignKey(OrderCancelReason,on_delete=models.DO_NOTHING,null=True)
    order_cancel_reason_other = models.CharField(max_length=200,null=True)
    order_payment_status = models.CharField(max_length=200,null=True)
    delivered_date = models.DateTimeField(null=True)
    requirement_submit_date = models.DateTimeField(null=True)
    dispute_remark = models.CharField(max_length=200,null=True)
    refund_status = models.ForeignKey(RefundStatus,null=True,on_delete=models.DO_NOTHING)
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='order_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='order_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='order_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)

    class Meta :
        db_table = 'order'
        default_permissions = ()

