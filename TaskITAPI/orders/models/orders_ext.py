from django.db import models
from softdelete.models import SoftDeleteModel
from accounts.models import User
from configs.models import OrderStatus,OrderCancelReason,City,PriceType,OrderPaymentStatus
from orders.models import Order
from masters.models import SubCategoryPriceScope,SubCategoryPriceScopeDetails,PriceExtraService
from gigs.models import Gig


# Master OrderPayment related data stored in this table
class OrderPayment(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING,null=True)
    razorpay_order_id = models.CharField(max_length=200)
    from_user = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='order_payment_from_user')
    to_user = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='order_payment_to_user')    
    razorpay_payment_no = models.CharField(max_length=200,null=True)
    razorpay_signature = models.CharField(max_length=500,null=True)
    order_payment_status = models.ForeignKey(OrderPaymentStatus,on_delete=models.DO_NOTHING,null=True)  
    order_status = models.ForeignKey(OrderStatus,on_delete=models.DO_NOTHING,null=True)
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='order_payment_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)  

    class Meta :
        db_table = 'order_payment'
        default_permissions = ()


def order_digital_filename(instance, filename):
    order_digi = OrderDigitalDownload.objects.last()
    if order_digi :
        count = order_digi.id + 1
    else :
        count = 1 
    return 'Order/OrderDigitalDownload/File_Path/{0}/{1}_{2}'.format(
        instance.order_id,      
        count,      
        filename
        )

# Master OrderDigitalDownload related data stored in this table
class OrderDigitalDownload(SoftDeleteModel) :
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)   
    seller = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='order_digital_seller')
    buyer = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='order_digital_buyer')    
    filename = models.FileField(upload_to=order_digital_filename,null=True)
    buyer_download =  models.BooleanField(null=True)
    buyer_download_date = models.DateTimeField(null=True) 
    file_size = models.CharField(max_length=50)      
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='order_digital_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)  

    class Meta :
        db_table = 'order_digital_download'
        default_permissions = ()


# Master OrderComment related data stored in this table
class OrderComment(SoftDeleteModel) :
    order_digital_download = models.ForeignKey(OrderDigitalDownload,on_delete=models.DO_NOTHING,null=True)
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING,null=True)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)   
    comment_ref = models.ForeignKey('self', null=True, on_delete=models.DO_NOTHING) 
    comment = models.CharField(max_length=500,null=True)
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='order_comment_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)  

    class Meta :
        db_table = 'order_comment'
        default_permissions = ()


# Master OrderRating related data stored in this table
class OrderRating(SoftDeleteModel) :    
    order = models.ForeignKey(Order,on_delete=models.DO_NOTHING,null=True)
    gig = models.ForeignKey(Gig,on_delete=models.DO_NOTHING)   
    seller = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='order_rating_seller')
    buyer = models.ForeignKey(User,on_delete=models.DO_NOTHING,related_name='order_rating_buyer')  
    seller_feedback = models.CharField(max_length=500,null=True)
    buyer_feedback = models.CharField(max_length=500,null=True)
    buyer_rating = models.IntegerField(null=True)
    seller_rating = models.IntegerField(null=True)    
    created_by = models.ForeignKey(User,on_delete=models.DO_NOTHING, related_name='order_rating_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)  

    class Meta :
        db_table = 'order_rating'
        default_permissions = ()        