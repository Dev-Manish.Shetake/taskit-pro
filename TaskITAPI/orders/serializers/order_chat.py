from rest_framework import serializers
from orders.models import Order,OrderChat
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from accounts.serializers import UserMinSerializer
from django.utils import timezone
from django.db import transaction


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/orders_chat/')
'''
class OrdersChatListSerializer(serializers.ModelSerializer) :  
    order_id = serializers.IntegerField(write_only=True)
    created_by = UserMinSerializer(read_only=True) 

    def validate_order_id(self,value) :
        if not Order.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for order id.'),code='invalid_input')
        return value

    def setup_eager_loading(self,queryset) :  
        return queryset.order_by('created_date')

    class Meta :
        model = OrderChat
        fields = [
            'id',
            'order_id',
            'chat',
            'file_path', 
            'created_by',            
            'created_user',
            'created_date'   
        ]
        read_only_fields = ['created_by','created_user','created_date']

    @transaction.atomic
    def create(self,validated_data) :
        order_chat = OrderChat.objects.create(
            **validated_data,
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name           
        )
        return order_chat    