from rest_framework import serializers
from accounts.models import User
from orders.models import Order,OrderPriceExtraService,OrderPriceExtraServiceCustom,OrderSubCategoryPriceDetails
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from masters.models import PriceExtraService
from masters.serializers import ExtraPriceServiceMinSerializer

class OrderPriceExtraServiceSerializer(serializers.ModelSerializer) :
    price_extra_service_id = serializers.IntegerField(write_only=True)
    price_extra_service = ExtraPriceServiceMinSerializer(read_only=True)

    def validate_price_extra_service_id(self,value) :
        if not PriceExtraService.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for price extra service id.'),code='invalid_input')
        return value

    class Meta :
        model = OrderPriceExtraService
        fields =[
            'id',
            'price_extra_service_id',
            'price_extra_service',
            'extra_days',        
            'price',
            'quantity',
            'amount',           
        ]


class OrderPriceExtraServiceCustomSerializer(serializers.ModelSerializer) :
    class Meta :
        model = OrderPriceExtraServiceCustom
        fields =[
            'id',
            'title',
            'description',        
            'extra_price',
            'extra_days',
            'quantity',
            'amount',           
        ]


class OrderSubCategoryPriceDetailsSerializer(serializers.ModelSerializer) :
    sub_category_price_scope_id = serializers.IntegerField()
    sub_category_price_scope_dtl_id = serializers.IntegerField(allow_null=True)
    class Meta :
        model = OrderSubCategoryPriceDetails
        fields =[
            'id',
            'sub_category_price_scope_id',
            'sub_category_price_scope_dtl_id',        
            'value'                   
        ]