from rest_framework import serializers
from django.db import transaction
from django.db.models import Prefetch
from django.utils import timezone
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from orders.models import Order,OrderGigRequirements,OrderGigRequirementsDetails
from configs.serializers import QuestionFormSerializer
from gigs.models import GigRequirementDetails
from gigs.views.gig_requirements import GigsRequirementDetailsSerializer 


class OrderGigRequirementsDetailsSerializer(serializers.ModelSerializer) :  
    file_path = serializers.FileField(allow_null=True)
    class Meta :
        model = OrderGigRequirementsDetails
        fields = [
            'id',
            'file_path',
            'description'
        ]



class OrderGigRequirementsGetSerializer(serializers.ModelSerializer) :
    question_form_id = serializers.IntegerField(write_only=True)
    question_form = QuestionFormSerializer(read_only=True)   
    gig_description = serializers.SerializerMethodField()
   
    def setup_eager_loading(self, queryset) :       
        return queryset

    class Meta :
        model = OrderGigRequirements
        fields = [
            'id',            
            'order_id',
            'is_mandatory',
            'question',
            'question_form_id',
            'question_form',
            'is_multiselect',
            'gig_requirement_id',
            'gig_description',           
        ]       
        read_only_fields = ['question_form']

    def get_gig_description(self,instance) :
        gig_description = []
        gig_req_dtl = GigRequirementDetails.objects.filter(gig_requirement=instance.gig_requirement)
        for req in gig_req_dtl :
            gig_description.append(GigsRequirementDetailsSerializer(req).data)
        return gig_description    


class OrderGigRequirementsSerializer(serializers.ModelSerializer) :
    id = serializers.IntegerField()
    question_form_id = serializers.IntegerField(write_only=True)
    question_form = QuestionFormSerializer(read_only=True)   
    description = OrderGigRequirementsDetailsSerializer(many=True)
   
    def setup_eager_loading(self, queryset) :    
        queryset = queryset.prefetch_related(
            Prefetch('ordergigrequirementsdetails_set',OrderGigRequirementsDetails.objects.all(),to_attr='description')   
        )   
        return queryset

    class Meta :
        model = OrderGigRequirements
        fields = [
            'id',            
            'order_id',
            'is_mandatory',
            'question',
            'question_form_id',
            'question_form',
            'is_multiselect',
            'description',           
        ]       
