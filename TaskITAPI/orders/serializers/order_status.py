from rest_framework import serializers
from accounts.models import User
from orders.models import Order,OrderDigitalDownload,OrderComment,OrderRating
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from orders.serializers.orders_rating import OrderDigitalDownloadSerializer,OrderDigitalDownloadListSerializer,OrderCommentSerializer,OrderRatingCreateSerializer
from django.db.models import Prefetch
from accounts.serializers import SellerInfoMinSerializer,BuyerInfoMinSerializer
from django.utils import timezone
from configs.models import OrderCancelReason
from settings.models import SystemConfiguration
from settings.values import configs

'''
View to send data in specific format for POST method
(endpoint = '/orders/submit-review/')
'''
class SubmitReviewSerializer(serializers.Serializer) :
    order_id = serializers.IntegerField()
    digital_download = OrderDigitalDownloadSerializer(required=False)
    comment = OrderCommentSerializer()

    def validate_order_id(self,value) :
        if not Order.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for order id.'),code='invalid_input')
        return value

'''
View to send data in specific format for POST method
(endpoint = '/orders/submit-review-attachment/:id')
'''
class SubmitReviewAttachmentUpdateSerializer(serializers.Serializer) :
    filename = serializers.FileField()




'''
View to send data in specific format for POST method
(endpoint = '/orders/delivery-list/:order_id')
'''
class OrderDeliveryListSerializer(serializers.ModelSerializer) : 
    comment = OrderCommentSerializer(many=True)  
    order_digital_download = OrderDigitalDownloadListSerializer(many=True) 
    order_complete_days_limit = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('ordercomment_set',OrderComment.objects.all().order_by('-created_date'),to_attr='comment'),
            Prefetch('orderdigitaldownload_set',OrderDigitalDownload.objects.all(),to_attr='order_digital_download')
        )
        return queryset
    class Meta :
        model = Order
        fields = [
            'id',  
            'order_digital_download',
            'comment',
            'order_complete_days_limit'
        ]

    def get_order_complete_days_limit(self,instance) :
        sys_config = SystemConfiguration.objects.filter(code_name=configs.ORDER_COMPLETE_DAYS_LIMIT)
        order_complete_days_limit = sys_config[0].field_value
        return order_complete_days_limit    


'''
View to send data in specific format for POST method
(endpoint = '/orders/delivery-review-list/:order_id')
'''
class OrderDeliveryReviewListSerializer(serializers.ModelSerializer) :
    seller = SellerInfoMinSerializer()
    buyer = BuyerInfoMinSerializer()

    def setup_eager_loading(self,queryset) :
        queryset = SellerInfoMinSerializer.setup_eager_loading(self,queryset,'seller__')
        queryset = BuyerInfoMinSerializer.setup_eager_loading(self,queryset,'buyer__')
        return queryset

    class Meta :
        model = OrderRating
        fields = [
            'id',    
            'seller',
            'buyer',
            'buyer_feedback',
            'buyer_rating',  
            'seller_feedback',
            'seller_rating',         
            'created_date',                      
        ]


'''
View to send data in specific format for POST method
(endpoint = '/orders/modifcation-required/')
'''
class ModificationRequiredSerializer(serializers.Serializer) :   
    order_comment_id = serializers.IntegerField()
    comment = serializers.CharField(max_length=500)

    def validate_order_comment_id(self,value) :
        if not OrderComment.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input recieved for order comment id'),code='invalid_input')
        return value
   

'''
View to send data in specific format for POST method
(endpoint = '/orders/completed')
'''
class OrderCompletedSerializer(serializers.Serializer) :
    order_id = serializers.IntegerField()    
    rating = OrderRatingCreateSerializer()

    def validate_order_id(self,value) :
        if not Order.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for order id.'),code='invalid_input')
        return value   


'''
View to send data in specific format for POST method
(endpoint = '/orders/cancelled')
'''
class OrderCancelledSerializer(serializers.Serializer) :    
    order_cancel_reason_id = serializers.IntegerField()
    order_cancel_reason_other = serializers.CharField(max_length=200,allow_null=True,allow_blank=True)

    def validate_order_cancel_reason_id(self,value) :
        if not OrderCancelReason.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for order cancel reason id.'),code='invalid_input')
        return value        