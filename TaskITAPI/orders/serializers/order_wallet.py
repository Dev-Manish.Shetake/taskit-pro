from rest_framework import serializers
from accounts.models import User,UserBank
from orders.models import Order,OrderPayment,FinanceTransaction,FinanceTransactionDetails,OrderRefundLog
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.utils import timezone
from django.db.models import Prefetch,OuterRef,Subquery
from accounts.serializers import UserMinSerializer,UserBankMacroSerializer,UserBankMinSerializer,BuyerInfoMinSerializer
from configs.serializers import OrderStatusSerializer,VoucherTypeSerializer
from configs.values import VOUCHER_TYPE,GEN_LEDGER_TYPE
from configs.models import GenLedger,GenLedgerType
from gigs.serializers import GigMinSerializer


'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/finance_transaction_details')
'''
class OrderPaymentSerializer(serializers.ModelSerializer) : 
    from_user = UserMinSerializer()
    to_user = UserMinSerializer()
   
    def setup_eager_loading(self,queryset) :      
        return queryset

    class Meta :
        model = OrderPayment    
        fields = [
            'id',  
            'razorpay_order_id',
            'from_user',
            'to_user',
            'razorpay_payment_no',
            'order_payment_status',
            'order_status',
            'created_by',
            'created_date',
            'created_user'               
        ] 


'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/finance_transaction_details')
'''
class OrderPaymentMinSerializer(serializers.ModelSerializer) : 
    # to_user = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :    
        return queryset

    class Meta :
        model = OrderPayment    
        fields = [
            'id',  
            'razorpay_order_id',           
            'razorpay_payment_no',
            # 'to_user'            
        ] 


'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/finance_transaction_details')
'''
class FinanceTransactionDetailsListSerializer(serializers.ModelSerializer) : 
   
    def setup_eager_loading(self,queryset) :      
        return queryset

    class Meta :
        model = FinanceTransactionDetails
        fields = [
            'id',  
            'seq_no',
            'gen_ledger_id',
            'gen_ledger_name',
            'amount'                
        ]    

'''
'''
class FinanceTransactionDetailsMinSerializer(serializers.ModelSerializer) : 
    amount = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :            
        return queryset

    class Meta :
        model = FinanceTransactionDetails
        fields = [
            'id',             
            'amount',           
        ] 

    def get_amount(self,instance) :
        if instance.fin_tran.voucher_type_id in (VOUCHER_TYPE['wallet_to_nodal_partial_payment'],VOUCHER_TYPE['payment_through_wallet']) :
            amount = -1 * int(instance.amount)
        else :
            amount = instance.amount
        return amount

'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/finance_transaction_details')
'''
class FinanceTransactionSerializer(serializers.ModelSerializer) : 
   
    def setup_eager_loading(self,queryset) :      
        return queryset

    class Meta :
        model = FinanceTransaction
        fields = [
            'id', 
            'voucher_no',
            'voucher_date',
            'voucher_type',
        ]  

'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/incoming/')
'''
class OrderIncomingWalletListSerializer(serializers.ModelSerializer) : 
    buyer = UserMinSerializer()    
    order_status = OrderStatusSerializer()
    gig = GigMinSerializer()

    def setup_eager_loading(self,queryset) : 
        return queryset

    class Meta :
        model = Order
        fields = [
            'id',  
            'order_no',
            'buyer',            
            'gig',
            'total_amount_without_service_fee',
            'order_status',
            'created_date'           
        ]    



'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/')
'''
class FinanceTransactionWithdrawalListSerializer(serializers.ModelSerializer) : 
    order_no = serializers.SerializerMethodField()
    voucher_type = VoucherTypeSerializer()
    fin_trans_dtl = FinanceTransactionDetailsMinSerializer(many=True)   
    order_payment = OrderPaymentMinSerializer()
    to_user = serializers.SerializerMethodField()

    def setup_eager_loading(self,queryset) : 
        gen_ledger_ids = GenLedger.objects.filter(
            user=self.request.user,
            gen_ledger_type_id__in=(
                GEN_LEDGER_TYPE['user_wallet'],
                GEN_LEDGER_TYPE['user_bank']
                )).values_list('id')
        queryset = queryset.prefetch_related(
            Prefetch('financetransactiondetails_set',FinanceTransactionDetails.objects.filter(
                gen_ledger_id__in=gen_ledger_ids
                ),
                to_attr='fin_trans_dtl'
                )
            )         
        queryset = OrderPaymentMinSerializer.setup_eager_loading(self,queryset)    
        return queryset

    class Meta :
        model = FinanceTransaction
        fields = [
            'id',  
            'order_id',
            'order_no',        
            'order_payment',
            'voucher_type',
            'to_user',
            'created_by',
            'created_date',   
            'fin_trans_dtl'                   
        ] 

   
    def get_order_no(self,instance) :
        return instance.order.order_no   

    def get_to_user(self,instance) :
        to_user = None
        user_bank_info = None
        if instance.voucher_type_id in (
            VOUCHER_TYPE['wallet_to_nodal_partial_payment'],
            VOUCHER_TYPE['payment_through_wallet']) :
            user = User.objects.filter(id=instance.order_payment.from_user_id)
        else :
            user = User.objects.filter(id=instance.order_payment.to_user_id)
       
        if len(user) > 0 :
            user_bank = UserBank.objects.filter(user=user[0])
            if len(user_bank) > 0 :
                user_bank_info = {
                    'id' : user_bank[0].id,
                    'bank_account_name' : user_bank[0].bank_account_name,
                    'bank_account_no' : user_bank[0].bank_account_no
                }                
            to_user = {
                'user_id' : user[0].id,
                'user_name' : user[0].user_name,
                'user_bank_info' : user_bank_info
            }           
        return to_user  


class OrderRefundLogSerializer(serializers.ModelSerializer) :

    class Meta :
        model = OrderRefundLog
        fields = [
            'id',            
            'refunded_amount',
            'remark'
        ]
    