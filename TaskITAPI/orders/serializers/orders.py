from rest_framework import serializers
from accounts.models import User
from orders.models import Order,OrderGigPrice,OrderPriceExtraService,OrderPriceExtraServiceCustom,OrderRating,OrderComment,\
    OrderSubCategoryPriceDetails,OrderPayment,OrderRefundLog
from masters.models import PriceExtraService
from orders.serializers.order_price import OrderPriceExtraServiceSerializer,OrderPriceExtraServiceCustomSerializer,OrderSubCategoryPriceDetailsSerializer
from orders.serializers.order_wallet import OrderPaymentMinSerializer,OrderRefundLogSerializer
from gigs.models import Gig
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from django.db import transaction
from django.db.models import Prefetch
from accounts.serializers import UserMinSerializer,SellerInfoMinSerializer,BuyerInfoMinSerializer
from configs.serializers import OrderStatusSerializer,OrderPaymentStatusSerializer,OrderCancelReasonSerializer,RefundStatusSerializer 
from configs.values import ORDER_STATUS
from gigs.serializers import GigMinSerializer,GigOrderMinSerializer
from accounts.values import USER_TYPE
from datetime import datetime,timedelta
from notifications.tasks import notify_user
from notifications import events
import datetime



class GigTitleSearilizer(serializers.ModelSerializer) :
    class Meta :
        model = Gig
        fields =[      
            'id',
            'title'
        ] 


class OrderGigPriceSerializer(serializers.ModelSerializer) :
    price_type_id = serializers.IntegerField()

    class Meta :
        model = OrderGigPrice
        fields =[
            'id',
            'price_type_id',
            'price',        
            'quantity',            
            'amount',           
        ]

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/orders/')
'''
class OrdersMinSerializer(serializers.ModelSerializer) :
    class Meta :
        model = Order
        fields = [
            'id',
            'created_date',            
        ]



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/orders/')
'''
class OrdersListSerializer(serializers.ModelSerializer) :   
    buyer = BuyerInfoMinSerializer()    
    seller = SellerInfoMinSerializer()    
    gig = GigTitleSearilizer()
    order_status = OrderStatusSerializer()
    due_date = serializers.SerializerMethodField()
   
    def setup_eager_loading(self,queryset) :        
        queryset = queryset.select_related('gig').only(
            'id','amount', 'extra_service_amount','extra_service_custom_amount','service_fee','total_amount','delivery_time',
            'delivered_date','created_date','gig__title'
        )
        queryset = BuyerInfoMinSerializer.setup_eager_loading(self, queryset, 'buyer__')
        queryset = SellerInfoMinSerializer.setup_eager_loading(self, queryset, 'seller__')
        return queryset.order_by('-created_date')

    class Meta :
        model = Order
        fields = [
            'id',
            'order_no',
            'buyer',
            'seller',
            'gig',
            'amount',
            'extra_service_amount',
            'extra_service_custom_amount',
            'service_fee',
            'total_amount',
            'delivery_time',
            'order_status',
            'delivered_date',
            'created_date',
            'due_date'            
        ]

    def get_due_date(self,instance) :
        due_date = instance.created_date + timedelta(days=instance.delivery_time) 
        return due_date.date()   

'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/orders/')
'''
class OrdersCreateSerializer(serializers.ModelSerializer) :  
    order_no = serializers.CharField(required=False)   
    buyer_id = serializers.IntegerField()
    seller_id = serializers.IntegerField()
    gig_id = serializers.IntegerField()
    amount = serializers.DecimalField(max_digits=9,decimal_places=2)
    extra_service_amount = serializers.DecimalField(max_digits=9,decimal_places=2)
    extra_service_custom_amount = serializers.DecimalField(max_digits=9,decimal_places=2)
    service_fee = serializers.DecimalField(max_digits=9,decimal_places=2)
    total_amount = serializers.DecimalField(max_digits=9,decimal_places=2)
    order_gig_price = OrderGigPriceSerializer()
    extra_service = OrderPriceExtraServiceSerializer(many=True,required=False)
    custom_services = OrderPriceExtraServiceCustomSerializer(many=True,required=False)
    sub_category_price_details = OrderSubCategoryPriceDetailsSerializer(many=True,required=False)

    def validate_buyer_id(self,value) :
        if not User.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for buyer id.'),code='invalid_input')
        return value

    def validate_seller_id(self,value) :
        if not User.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for seller id.'),code='invalid_input')
        return value    

    def validate_gig_id(self,value) :
        if not Gig.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for gig id.'),code='invalid_input')
        return value      

    class Meta :
        model = Order
        fields = [
            'id',
            'order_no',
            'buyer_id',
            'seller_id',
            'gig_id',
            'amount',
            'extra_service_amount',
            'extra_service_custom_amount',
            'service_fee',
            'total_amount',
            'delivery_time',    
            'order_gig_price',
            'extra_service',
            'custom_services',
            'sub_category_price_details'                      
        ]
              
    @transaction.atomic          
    def create(self,validated_data) :
        order_gig_price = None
        if 'order_gig_price' in validated_data :
            order_gig_price = validated_data.pop('order_gig_price')
        extra_service = []
        if 'extra_service' in validated_data :
            extra_service = validated_data.pop('extra_service')
        custom_services = []
        if 'custom_services' in validated_data :
            custom_services = validated_data.pop('custom_services')    
        sub_category_price_details = []
        if 'sub_category_price_details' in validated_data :
            sub_category_price_details = validated_data.pop('sub_category_price_details') 

        order = Order.objects.create(
            **validated_data,
            order_no = datetime.datetime.now().strftime('%Y%m%d%H-%M-%S'),
            order_status_id=ORDER_STATUS['Draft'],
            created_by = self.context['request'].user,
            created_user = self.context['request'].user.user_name
            )     
        order.total_amount_without_service_fee = int(order.total_amount) - int(order.service_fee)
        order.save()      
        if order_gig_price :
            order_gig_price = OrderGigPrice.objects.create(
                order = order,
                **order_gig_price               
            ) 

        if len(extra_service) :
            extra_service_list = []
            for extra in extra_service :
                extra_ser = OrderPriceExtraService(
                   order=order,
                    **extra
                )
                extra_service_list.append(extra_ser)
            if len(extra_service_list) > 0 :
                OrderPriceExtraService.objects.bulk_create(extra_service_list)   

        if len(custom_services) :
            custom_service_list = []
            for custom in custom_services :
                custom_ser = OrderPriceExtraServiceCustom(
                   order=order,
                    **custom
                )
                custom_service_list.append(custom_ser)
            if len(custom_service_list) > 0 :
                OrderPriceExtraServiceCustom.objects.bulk_create(custom_service_list)

        if len(sub_category_price_details) > 0 :
            sub_category_price_details_list = [] 
            for price_dtl in sub_category_price_details :
                order_price_dtl = OrderSubCategoryPriceDetails(
                   order=order,
                    **price_dtl
                )
                sub_category_price_details_list.append(order_price_dtl)
            if len(sub_category_price_details_list) > 0 :
                OrderSubCategoryPriceDetails.objects.bulk_create(sub_category_price_details_list)
        order.order_gig_price = None
        order.extra_service = None        
        order.custom_services = None
        order.sub_category_price_details = None
        return order         
    
'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/orders/:id')
'''
class OrdersDetailsSerializer(serializers.ModelSerializer) : 
    gig = GigOrderMinSerializer()
    order_status = OrderStatusSerializer()
    order_gig_price = OrderGigPriceSerializer(many=True)
    extra_service = OrderPriceExtraServiceSerializer(many=True)
    extra_custom_service = OrderPriceExtraServiceCustomSerializer(many=True)
    buyer = UserMinSerializer()
    order_payment = OrderPaymentMinSerializer(many=True)

    def setup_eager_loading(self,queryset) :    
        queryset = queryset.prefetch_related(
            Prefetch('ordergigprice_set',OrderGigPrice.objects.all(),to_attr='order_gig_price'),
            Prefetch('orderpriceextraservice_set',OrderPriceExtraService.objects.all(),to_attr='extra_service'),
            Prefetch('orderpriceextraservicecustom_set',OrderPriceExtraServiceCustom.objects.all(),to_attr='extra_custom_service'),
            Prefetch('orderpayment_set',OrderPayment.objects.all(),to_attr='order_payment')
        )
        queryset = GigOrderMinSerializer.setup_eager_loading(self,queryset,'gig__')
        return queryset

    class Meta :
        model = Order
        fields = [
            'id',   
            'order_no',       
            'gig',
            'amount',
            'extra_service_amount',
            'extra_service_custom_amount',
            'service_fee',
            'total_amount',
            'delivery_time',
            'order_status',
            'delivered_date',
            'requirement_submit_date',
            'created_date',
            'order_gig_price',
            'extra_service',
            'extra_custom_service',
            'order_payment',
            'buyer'
        ]


'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/orders/backoffice-review-list')
'''
class OrdersReviewsListSerializer(serializers.ModelSerializer) : 
    gig = GigMinSerializer() 
    created_by = UserMinSerializer()

    def setup_eager_loading(self,queryset) :        
        return queryset.order_by('-created_date')

    class Meta :
        model = OrderComment
        fields = [
            'id',         
            'order',
            'gig',            
            'comment',            
            'created_by',
            'created_user',           
            'created_date'            
        ]

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/orders/counts')
'''
class OrdersCountSerializer(serializers.Serializer) :
    total_active = serializers.IntegerField()
    total_in_process = serializers.IntegerField() 
    total_completed = serializers.IntegerField()
    total_cancelled = serializers.IntegerField()
    total_dispute = serializers.IntegerField()      



'''
Serializer to send/receive data in specific format for POST/GET method
(endpoint = '/orders/refund-list/')
'''
class OrdersRefundListSerializer(serializers.ModelSerializer) :  
    buyer = BuyerInfoMinSerializer()    
    seller = SellerInfoMinSerializer()   
    order_status = OrderStatusSerializer()
    order_cancel_reason = OrderCancelReasonSerializer()
    refund_status = RefundStatusSerializer()
    order_payment = OrderPaymentMinSerializer(many=True)
    order_refund_log = OrderRefundLogSerializer(many=True)
   
    def setup_eager_loading(self,queryset) :    
        queryset = queryset.prefetch_related(
            Prefetch('orderpayment_set',OrderPayment.objects.all(),to_attr='order_payment'),
            Prefetch('orderrefundlog_set',OrderRefundLog.objects.all(),to_attr='order_refund_log')
        )
        queryset = BuyerInfoMinSerializer.setup_eager_loading(self, queryset, 'buyer__')
        queryset = SellerInfoMinSerializer.setup_eager_loading(self, queryset, 'seller__')    
        return queryset.order_by('-modified_date')

    class Meta :
        model = Order
        fields = [
            'id',
            'order_no',
            'buyer',
            'seller',
            'amount',
            'extra_service_amount',
            'extra_service_custom_amount',
            'service_fee',
            'total_amount',
            'order_status',
            'order_cancel_reason',
            'refund_status',       
            'order_payment',    
            'order_refund_log',
            'created_date',
            'modified_date'           
        ]    