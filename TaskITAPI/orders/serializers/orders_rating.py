from rest_framework import serializers
from django.db import transaction
from django.db.models import Prefetch
from django.utils import timezone
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _
from orders.models import OrderRating,OrderDigitalDownload,OrderComment
from accounts.models import User
from accounts.serializers import UserMacroSerializer


'''
Serializer to get data in specific format for GET method
'''
class OrderRatingMinSerializer(serializers.ModelSerializer) :
    class Meta :
        model = OrderRating
        fields =[
            'id',
            'seller_feedback',
            'buyer_feedback',        
            'buyer_rating',
            'seller_rating',
            'created_user',
            'created_date'    
        ]


'''
Serializer to get data in specific format for GET method
'''
class OrderRatingCreateSerializer(serializers.ModelSerializer) :
    seller_id = serializers.IntegerField()
    buyer_id = serializers.IntegerField()

    def validate_seller_id(self,value) :
        if not User.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for seller id.'),code='invalid_input')
        return value

    def validate_buyer_id(self,value) :
        if not User.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for buyer id.'),code='invalid_input')
        return value 
    class Meta :
        model = OrderRating
        fields =[
            'id',
            'seller_id',
            'buyer_id',
            'seller_feedback',
            'seller_rating',           
        ]

'''
Serializer to send data in specific format for POST method
(endpoint = '/orders/modifcation-required/')
'''
class OrderDigitalDownloadSerializer(serializers.ModelSerializer) :
    seller_id = serializers.IntegerField()
    buyer_id = serializers.IntegerField()
    # filename = serializers.FileField(allow_null=True,required=False)

    def validate_seller_id(self,value) :
        if not User.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for seller id.'),code='invalid_input')
        return value

    def validate_buyer_id(self,value) :
        if not User.objects.filter(id=value).exists() :
            raise ValidationError(_('Invalid input received for buyer id.'),code='invalid_input')
        return value 
        
    class Meta :
        model = OrderDigitalDownload
        fields = [
            'id',
            'seller_id',
            'buyer_id',
            # 'filename',
            'file_size'            
        ]



class OrderDigitalDownloadListSerializer(serializers.ModelSerializer) :

    class Meta :
        model = OrderDigitalDownload
        fields = [
            'id',    
            'seller',
            'filename',
            'file_size',
            'buyer_download',
            'created_date',             
        ]


'''
Serializer to send data in specific format for POST method
(endpoint = '/orders/modifcation-required/')
'''
class OrderCommentSerializer(serializers.ModelSerializer) :
    created_by = UserMacroSerializer(read_only=True) 

    class Meta :
        model = OrderComment
        fields = [
            'id',
            'comment',       
            'created_by',
            'created_date'              
        ]
        read_only_fields = ['created_by','created_date']
