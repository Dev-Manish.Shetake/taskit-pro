import requests,json
from requests.auth import HTTPBasicAuth
from payments.utils import RazorpayGatewayConfig

"""
Transfer payment froom wallet to bank account of seller user 
"""
def transfer_payment(self,user_bank,withdraw_amount) :
    razorpay_gateway = RazorpayGatewayConfig()
    base_url = 'https://api.razorpay.com/v1/transfers'
    data = {
    "account": user_bank[0].bank_account_no,
    "amount": int(withdraw_amount) * 100,
    "currency": "INR"
    }

    pay_res = requests.post(
        url = base_url, 
        auth=HTTPBasicAuth(razorpay_gateway.key_id,razorpay_gateway.key_secret),            
        data =json.dumps(data),
        headers={"Content-Type": "application/json"}, 
    )
    return pay_res

