from django.urls import include,path
from orders.views import OrderChatListView,OrderChatCreateView

urlpatterns = [
    path('list/<int:order_id>',OrderChatListView.as_view()),
    path('create',OrderChatCreateView.as_view()),
]