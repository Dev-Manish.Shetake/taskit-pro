from django.urls import include,path
from orders.views import FinanceTransactionWithdrawalListView,SellerWithdrawAmountView,OrderIncomingWalletListView,\
    OrderSummaryListView,WithdrawalCountView

urlpatterns = [
    path('incoming',OrderIncomingWalletListView.as_view()),
    path('withdrawal',FinanceTransactionWithdrawalListView.as_view()),
    path('summary',OrderSummaryListView.as_view()),
    path('withdrawal-count',WithdrawalCountView.as_view()),
    path('withdraw-amount',SellerWithdrawAmountView.as_view())
]