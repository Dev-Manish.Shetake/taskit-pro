from django.urls import include,path
from orders.views import OrderListView,OrderCreateView,OrderPaymentCreateView,OrderRatingListView,OrderGigRequirementsDetailsUpdateView,\
    OrderGigEmptyRequirementsCreateView,OrderGigRequirementsListView,OrderGigRequirementsDetailsView,OrderDetailsView,OrderSubmitReviewView,\
        OrderDeliveryDownloadView,OrderDeliveryListView,OrderDeliveryReviewListView,OrderModificationRequiredView,OrderCompletedView,RaiseDisputeView,\
            OrderCancelledView,OrderCountsView,OrderActiveView,OrderSubmitReviewAttachmentUpdateView,OrderRefundListView

urlpatterns = [
    path('',OrderListView.as_view()),
    path('counts',OrderCountsView.as_view()),
    path('<int:pk>',OrderDetailsView.as_view()),
    path('create',OrderCreateView.as_view()),    
    path('payment-create',OrderPaymentCreateView.as_view()),  
    path('empty-requirements-create',OrderGigEmptyRequirementsCreateView.as_view()), 
    path('requirement/<int:order_id>',OrderGigRequirementsListView.as_view()), 
    path('requirements-update/<int:order_id>',OrderGigRequirementsDetailsUpdateView.as_view()), 
    path('gig-requirement-details/<int:order_id>',OrderGigRequirementsDetailsView.as_view()), 
    # Order Status
    path('active/<int:id>',OrderActiveView.as_view()), 
    path('submit-review',OrderSubmitReviewView.as_view()), 
    path('submit-review-attchment/<int:id>',OrderSubmitReviewAttachmentUpdateView.as_view()), 
    path('delivery-list/<int:order_id>',OrderDeliveryListView.as_view()), 
    path('delivery-review-list/<int:order_id>',OrderDeliveryReviewListView.as_view()), 
    path('delivery-download/<int:id>',OrderDeliveryDownloadView.as_view()), 
    path('modification-required',OrderModificationRequiredView.as_view()),
    path('completed',OrderCompletedView.as_view()), 
    path('raise-dispute/<int:id>',RaiseDisputeView.as_view()), 
    path('cancelled/<int:id>',OrderCancelledView.as_view()), 
    # BackOffice
    path('backoffice-review-list',OrderRatingListView.as_view()),
    path('refund-list',OrderRefundListView.as_view())
]