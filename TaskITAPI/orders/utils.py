from rest_framework.exceptions import PermissionDenied
from accounts.values import USER_TYPE,GROUP
from accounts.models import User
from settings.models import SystemConfiguration
from settings.values import configs
from django.utils import timezone
from configs.values import VOUCHER_TYPE,GEN_LEDGER_TYPE
from orders.models import Order,OrderPayment,FinanceTransaction,FinanceTransactionDetails
from configs.models import GenLedger

def get_user_data(request, queryset) :
    _user = User.objects.get(id=request.user.id)
    if _user.user_type_id == GROUP['back_office'] :
        return queryset   
    elif _user.user_type_id == GROUP['buyer']:
        queryset = queryset.filter(buyer=_user)
    elif _user.user_type_id == GROUP['seller'] :
        queryset = queryset.filter(seller=_user)
    else :
        raise PermissionDenied
    return queryset

# -------------------------------------------------------------- #

'''
Buyer Account To Nodal Account Transfer
# Order Complete Payment trought Bank
'''
def buyer_to_nodal_complet_payment(self,order_payment) : 
    fin_tran = FinanceTransaction.objects.filter(voucher_type_id=VOUCHER_TYPE['buyer_to_nodal']).last()
    if fin_tran :
        _voucher_no = 1 if not fin_tran.voucher_no else int(fin_tran.voucher_no) + 1
    else :
        _voucher_no = 1    
    finance_transaction = FinanceTransaction.objects.create(
        order=order_payment.order,
        order_payment=order_payment,
        voucher_no = _voucher_no,
        voucher_date = timezone.now().date(),
        voucher_type_id = VOUCHER_TYPE['buyer_to_nodal'],
        created_by = order_payment.from_user,
        created_user = order_payment.from_user.user_name
    )
    # Amount debited from user bank account (Buyer Account)  
    gen_ledger = GenLedger.objects.filter(user=order_payment.from_user,gen_ledger_type_id=GEN_LEDGER_TYPE['user'])
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    if fin_tran_dtl :
        _seq_no = fin_tran_dtl.seq_no + 1
    else :
        _seq_no = 1    
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = 0 - int(finance_transaction.order.total_amount - finance_transaction.order.service_fee),
        order = finance_transaction.order
    )
    # Amount credited to nodal account (TaskIT Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_WALLET_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,        
        amount = int(finance_transaction.order.total_amount - finance_transaction.order.service_fee),
        order = finance_transaction.order
    )
    # Amount credited to Admin fee(service) charges account (TaskIT Admin Fee Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_FEE_ACC_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = finance_transaction.order.service_fee,
        order = finance_transaction.order
    )


'''
Buyer Account/Wallet To Nodal Account Transfer
# Order Partial Payment
'''
def buyer_to_nodal_partial_payment(self,order_payment,amount) : 
    fin_tran = FinanceTransaction.objects.filter(voucher_type_id=VOUCHER_TYPE['buyer_to_nodal']).last()
    if fin_tran :
        _voucher_no = 1 if not fin_tran.voucher_no else int(fin_tran.voucher_no) + 1
    else :
        _voucher_no = 1    
    finance_transaction = FinanceTransaction.objects.create(
        order=order_payment.order,
        order_payment=order_payment,
        voucher_no = _voucher_no,
        voucher_date = timezone.now().date(),
        voucher_type_id = VOUCHER_TYPE['buyer_to_nodal'],
        created_by = order_payment.from_user,
        created_user = order_payment.from_user.user_name
    )
    # Amount debited from user bank account (Buyer Account)  
    gen_ledger = GenLedger.objects.filter(user=order_payment.from_user,gen_ledger_type_id=GEN_LEDGER_TYPE['user'])
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    if fin_tran_dtl :
        _seq_no = fin_tran_dtl.seq_no + 1
    else :
        _seq_no = 1    
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = 0 - int(amount),
        order = finance_transaction.order
    )
    # Amount credited to nodal account (TaskIT Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_WALLET_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,        
        amount = int(finance_transaction.order.total_amount - finance_transaction.order.service_fee),
        order = finance_transaction.order
    )


'''
Nodal Account To Sellel Wallet Account Transfer
'''
def nodal_to_seller_wallet_payment(self,order) :
    sys_config = SystemConfiguration.objects.filter(code_name=configs.NODAL_ACCOUNT_USER)
    nodal_user = User.objects.filter(id=sys_config[0].field_value)
    order_payment = OrderPayment.objects.create(
        order_id = order.id,       
        from_user = nodal_user[0],
        to_user = order.gig.created_by,      
        created_by = self.request.user,
        created_user = self.request.user.user_name
    )
    fin_tran = FinanceTransaction.objects.filter(voucher_type_id=VOUCHER_TYPE['nodal_to_wallet']).last()
    if fin_tran :
        _voucher_no = 1 if not fin_tran.voucher_no else int(fin_tran.voucher_no) + 1
    else :
        _voucher_no = 1  
    finance_transaction = FinanceTransaction.objects.create(
        order=order_payment.order,
        order_payment=order_payment,
        voucher_no = _voucher_no,
        voucher_date = timezone.now().date(),
        voucher_type_id = VOUCHER_TYPE['nodal_to_wallet'],
        created_by = order_payment.from_user,
        created_user = order_payment.from_user.user_name
    )
    # Amount debited from nodal account (TaskIT Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_WALLET_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    if fin_tran_dtl :
        _seq_no = int(fin_tran_dtl.seq_no) + 1
    else :
        _seq_no = 1        
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount =  0 - int(finance_transaction.order.total_amount - finance_transaction.order.service_fee),
        order = finance_transaction.order
    )
    # Amount credited to user wallet(Seller Wallet)
    gen_ledger = GenLedger.objects.filter(user=order_payment.order.gig.created_by,gen_ledger_type_id=GEN_LEDGER_TYPE['user_wallet'])
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = finance_transaction.order.total_amount - finance_transaction.order.service_fee,
        order = finance_transaction.order
    )
    return True    


'''
Debited from seller wallet to credited in seller bank ccount
# Amount Withdrawal 
'''
def seller_wallet_to_seller_bank_payment(self,order_payment,withdraw_amount) :
    fin_tran = FinanceTransaction.objects.filter(voucher_type_id=VOUCHER_TYPE['wallet_to_bank']).last()
    _voucher_no = 1 if not fin_tran.voucher_no else fin_tran.voucher_no + 1
    finance_transaction = FinanceTransaction.objects.create(
        order=order_payment.order,
        order_payment=order_payment,
        voucher_no = _voucher_no,
        voucher_date = timezone.now().date(),
        voucher_type_id = VOUCHER_TYPE['wallet_to_bank'],
        created_by = order_payment.from_user,
        created_user = order_payment.from_user.user_name
    )    
    # Amount debited from user wallet(Seller Wallet)
    gen_ledger = GenLedger.objects.filter(user=order_payment.order.gig.created_by,gen_ledger_type_id=GEN_LEDGER_TYPE['user_wallet'])
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    if fin_tran_dtl :
        _seq_no = fin_tran_dtl.seq_no + 1
    else :
        _seq_no = 1  
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger.id,
        gen_ledger_name = gen_ledger.gen_ledger_name,
        amount = 0 - int(withdraw_amount),
        order = finance_transaction.order
    )
    # Amount credited to user(Seller) bank account
    gen_ledger = GenLedger.objects.filter(user=order_payment.order.gig.created_by,gen_ledger_type_id=GEN_LEDGER_TYPE['user_bank'])
    _seq_no = _seq_no + 1  
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger.id,
        gen_ledger_name = gen_ledger.gen_ledger_name,
        amount = withdraw_amount,
        order = finance_transaction.order
    )   


'''
Debited from nodal to credited in buyer bank acount
# Refund To buyer once order cancelled by Buyer.
'''
def nodal_to_buyer_cancelled_by_buyer(self,order) :
    order_payment = OrderPayment.objects.create(
        order_id = order.id,
        razorpay_order_id = 'ABC123',
        from_user = order.created_by,
        to_user = order.gig.created_by,
        razorpay_payment_no = 'AN123',
        order_payment_status_id = 1,
        order_status = order.order_status,
        created_by = 1,
        created_user = 'System Admin'
    )
    fin_tran = FinanceTransaction.objects.filter(voucher_type_id=VOUCHER_TYPE['notal_to_payment_gatewy_cancel_by_buyer']).last()
    _voucher_no = 1 if not fin_tran.voucher_no else fin_tran.voucher_no + 1
    finance_transaction = FinanceTransaction.objects.create(
        order=order_payment.order,
        order_payment=order_payment,
        voucher_no = _voucher_no,
        voucher_date = timezone.now().date(),
        voucher_type_id = VOUCHER_TYPE['notal_to_payment_gatewy_cancel_by_buyer'],
        created_by = order_payment.from_user,
        created_user = order_payment.from_user.user_name
    )    
    # Amount debited from nodal account (TaskIT Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_WALLET_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    if fin_tran_dtl :
        _seq_no = fin_tran_dtl.seq_no + 1
    else :
        _seq_no = 1        
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount =  0 - int(_finance_transaction.order.amount),
        order = finance_transaction.order
    )
    # Amount credited to user Account(Buyer Ban Account)
    gen_ledger = GenLedger.objects.filter(user=order_payment.order.created_by,gen_ledger_type_id=GEN_LEDGER_TYPE['user'])
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger.id,
        gen_ledger_name = gen_ledger.gen_ledger_name,
        amount = _finance_transaction.order.amount,
        order = finance_transaction.order
    )
    return True


'''
Debited from nodal to credited in buyer bank ccount
# Refund To buyer once order cancelled by Seller.
'''
def nodal_to_buyer_cancelled_by_seller(self,order) :
    order_payment = OrderPayment.objects.create(
        order_id = order.id,
        razorpay_order_id = 'ABC123',
        from_user = order.created_by,
        to_user = order.gig.created_by,
        razorpay_payment_no = 'AN123',
        order_payment_status_id = 1,
        order_status = order.order_status,
        created_by = 1,
        created_user = 'System Admin'
    )
    fin_tran = FinanceTransaction.objects.filter(voucher_type_id=VOUCHER_TYPE['notal_to_payment_gatewy_cancel_by_seller']).last()
    _voucher_no = 1 if not fin_tran.voucher_no else fin_tran.voucher_no + 1
    finance_transaction = FinanceTransaction.objects.create(
        order=order_payment.order,
        order_payment=order_payment,
        voucher_no = _voucher_no,
        voucher_date = timezone.now().date(),
        voucher_type_id = VOUCHER_TYPE['notal_to_payment_gatewy_cancel_by_seller'],
        created_by = order_payment.from_user,
        created_user = order_payment.from_user.user_name
    )    
    # Amount debited from nodal account (TaskIT Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_WALLET_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    if fin_tran_dtl :
        _seq_no = fin_tran_dtl.seq_no + 1
    else :
        _seq_no = 1        
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount =  0 - int(_finance_transaction.order.amount),
        order = finance_transaction.order
    )
    # Amount debited from user wallet(Seller Wallet)
    gen_ledger = GenLedger.objects.filter(user=order_payment.order.gig.created_by,gen_ledger_type_id=GEN_LEDGER_TYPE['user_wallet'])
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    _seq_no = _seq_no + 1  
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger.id,
        gen_ledger_name = gen_ledger.gen_ledger_name,
        amount = 0 - int(_finance_transaction.order.service_fee),
        order = finance_transaction.order
    )
    # Amount credited to user Account(Buyer Bank Account)
    gen_ledger = GenLedger.objects.filter(user=order_payment.order.created_by,gen_ledger_type_id=GEN_LEDGER_TYPE['user'])
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger.id,
        gen_ledger_name = gen_ledger.gen_ledger_name,
        amount = _finance_transaction.order.total_amount,
        order = finance_transaction.order
    )
    return True


'''
Debited from buyer to credited in nodal bank ccount
'''
def buyer_to_nodal_partial_wallet_payment(self,order_payment,wallet_amount) :
    fin_tran = FinanceTransaction.objects.filter(voucher_type_id=VOUCHER_TYPE['wallet_to_nodal_partial_payment']).last()
    if fin_tran :
        _voucher_no = 1 if not fin_tran.voucher_no else int(fin_tran.voucher_no) + 1
    else :
        _voucher_no = 1   
    finance_transaction = FinanceTransaction.objects.create(
        order=order_payment.order,
        order_payment=order_payment,
        voucher_no = _voucher_no,
        voucher_date = timezone.now().date(),
        voucher_type_id = VOUCHER_TYPE['wallet_to_nodal_partial_payment'],
        created_by = order_payment.from_user,
        created_user = order_payment.from_user.user_name
    )    
    # Amount debited from user wallet account (Buyer/Seller Wallet)  
    gen_ledger = GenLedger.objects.filter(user=order_payment.from_user,gen_ledger_type_id=GEN_LEDGER_TYPE['user_wallet'])
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    if fin_tran_dtl :
        _seq_no = int(fin_tran_dtl.seq_no) + 1
    else :
        _seq_no = 1    
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = 0 - int(wallet_amount),
        order = finance_transaction.order
    )
    # Amount credited to nodal account (TaskIT Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_WALLET_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = int(wallet_amount),
        order = finance_transaction.order
    )

    # Service Amount debited from nodal account (TaskIT Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_WALLET_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = 0 - int(finance_transaction.order.service_fee),
        order = finance_transaction.order
    )

    # Amount credited to Admin fee(service) charges account (TaskIT Admin Fee Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_FEE_ACC_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = finance_transaction.order.service_fee,
        order = finance_transaction.order
    )


'''
Debited from buyer wallet account
'''
def buyer_to_nodal_complete_wallet_payment(self,order_payment) :
    fin_tran = FinanceTransaction.objects.filter(voucher_type_id=VOUCHER_TYPE['payment_through_wallet']).last()
    if fin_tran :
        _voucher_no = 1 if not fin_tran.voucher_no else int(fin_tran.voucher_no) + 1
    else :
        _voucher_no = 1 
    finance_transaction = FinanceTransaction.objects.create(
        order=order_payment.order,
        order_payment=order_payment,
        voucher_no = _voucher_no,
        voucher_date = timezone.now().date(),
        voucher_type_id = VOUCHER_TYPE['payment_through_wallet'],
        created_by = order_payment.from_user,
        created_user = order_payment.from_user.user_name
    )    
    # Amount debited from user wallet account (Buyer/Seller Wallet)  
    gen_ledger = GenLedger.objects.filter(user=order_payment.from_user,gen_ledger_type_id=GEN_LEDGER_TYPE['user_wallet'])
    fin_tran_dtl= FinanceTransactionDetails.objects.last()
    if fin_tran_dtl :
        _seq_no = int(fin_tran_dtl.seq_no) + 1
    else :
        _seq_no = 1    
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = 0 - int(finance_transaction.order.total_amount),
        order = finance_transaction.order
    )
    # Amount credited to nodal account (TaskIT Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_WALLET_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = int(finance_transaction.order.total_amount - finance_transaction.order.service_fee),
        order = finance_transaction.order
    )
    # Amount credited to Admin fee(service) charges account (TaskIT Admin Fee Account)  
    sys_config = SystemConfiguration.objects.filter(code_name=configs.ADMIN_FEE_ACC_LEDGER_TYPE)
    gen_ledger = GenLedger.objects.filter(id=sys_config[0].field_value)
    _seq_no = _seq_no + 1
    FinanceTransactionDetails.objects.create(
        fin_tran = finance_transaction,
        seq_no = _seq_no,
        gen_ledger_id = gen_ledger[0].id,
        gen_ledger_name = gen_ledger[0].gen_ledger_name,
        amount = finance_transaction.order.service_fee,
        order = finance_transaction.order
    )
