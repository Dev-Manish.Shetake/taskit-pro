from rest_framework.generics import ListAPIView,CreateAPIView
from orders.models import OrderChat
from orders.serializers import OrdersChatListSerializer
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db.models import Q
from rest_framework.exceptions import PermissionDenied,ValidationError


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders_chat/)
'''
class OrderChatListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. search = Filter by chat

    E.g. http://127.0.0.1:8000/api/v1/orders_chat/?search=

    """
    serializer_class = OrdersChatListSerializer
    pagination_class = StandardResultsSetPagination

    def check_permissions(self, request):
        if not request.user.is_authenticated:
            raise PermissionDenied
        if(request.method == 'GET' and request.user.has_perm('orders.view_order_chat')):
            return                  
        raise PermissionDenied

    def apply_filters(self,queryset) :
        q_objects = Q()        
        search = self.request.GET.get('search')
        if search :
            q_objects.add(Q(chat__icontains=search), Q.AND) 
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset  

    def get_queryset(self) :        
        order_id = self.kwargs['order_id']
        queryset = OrderChat.objects.filter(order_id=order_id)
        queryset = self.apply_filters(queryset)       
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset
 
'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders_chat/create')
'''
class OrderChatCreateView(CreateAPIView) :

    serializer_class = OrdersChatListSerializer

    def check_permissions(self, request):
        if not request.user.is_authenticated:
            raise PermissionDenied
        if(request.method == 'POST' and request.user.has_perm('orders.add_order_chat')):
            return       
        raise PermissionDenied