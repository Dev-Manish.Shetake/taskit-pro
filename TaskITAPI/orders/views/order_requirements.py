from rest_framework.generics import GenericAPIView,ListAPIView,CreateAPIView,UpdateAPIView
from orders.models import Order,OrderGigRequirements,OrderGigRequirementsDetails
from gigs.models import GigRequirement,GigRequirementDetails
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response 
from TaskITAPI.pagination import StandardResultsSetPagination
from rest_framework.exceptions import NotFound, ValidationError
from configs.serializers import QuestionFormSerializer
from orders.serializers import OrderGigRequirementsSerializer,OrderGigRequirementsGetSerializer
from django.db import transaction
from configs.values import QUESTION_FORM
from django.utils.translation import gettext as _
from django.utils import timezone
from configs.values import ORDER_STATUS
from notifications.tasks import notify_user
from notifications import events

'''
View to send data in specific format for POST method
(endpoint = '/orders/empty-requirements-create/')
'''
class OrderGigEmptyRequirementsCreateView(GenericAPIView) :
    """
    Order Gig Requirements POST API .

    """      
    class EmptyRequirementsCreateSerializer(serializers.Serializer) :
        order_id =  serializers.IntegerField()

        def validate_order_id(self,value) :
            if not Order.objects.filter(id=value).exists() :
                raise ValidationError(_('Invalid input received for order id.'),code='invalid_input')
            return value

    serializer_class = EmptyRequirementsCreateSerializer    

    @transaction.atomic
    def post(self,request) :       
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        order = Order.objects.get(id=validated_data['order_id'])
        gig_req = GigRequirement.objects.filter(gig=order.gig)
        for req in gig_req :
            OrderGigRequirements.objects.create(
                order=order,
                is_mandatory = req.is_mandatory,
                question = req.question,
                question_form = req.question_form,
                is_multiselect = req.is_multiselect,
                gig_requirement_id = req.id
            )
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for POST method
(endpoint = '/orders/requirements/:order_id')
'''
class OrderGigRequirementsListView(ListAPIView) :
    """
    Order Gig Requirements GET API .

    """ 
    serializer_class = OrderGigRequirementsGetSerializer    

    def get_queryset(self) :
        order_id = self.kwargs['order_id']
        queryset = OrderGigRequirements.objects.filter(order_id=order_id)
        queryset = self.serializer_class.setup_eager_loading(self,queryset)
        return queryset

  

'''
View to send data in specific format for POST method
(endpoint = 'requirements-update')
'''
class OrderGigRequirementsDetailsUpdateView(GenericAPIView) :
    """
    Order Gig Requirements Details PATCH API.
    """
    class OrderGigRequirementUpdateSerializer(serializers.Serializer) :
        requirements = OrderGigRequirementsSerializer(many=True,required=False)

    serializer_class = OrderGigRequirementUpdateSerializer
   
    @transaction.atomic
    def patch(self, request, order_id) :    
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data   
        order_id = self.kwargs['order_id']        
        try :
            order = Order.objects.get(id=order_id)
        except :
            raise ValidationError(_('Invalid input for order id'),code='invalid_input')
        requirements = []
        description = []
        if 'requirements' in validated_data :
            requirements = validated_data.pop('requirements')

        for req in requirements :
            if 'description' in req :
                description = req.pop('description')
            order_req = OrderGigRequirements.objects.get(id=req['id'])    
            order_req.is_mandatory = req['is_mandatory']
            order_req.question = req['question']
            order_req.question_form_id = req['question_form_id']
            order_req.is_multiselect = req['is_multiselect']
            order_req.save()
            for desc in description :
                req_dtl = OrderGigRequirementsDetails.objects.create(
                    order=order,
                    order_gig_requirement = order_req,
                    file_path = desc['file_path'],
                    description = desc['description']
                )         
        order.order_status_id = ORDER_STATUS['In-Process']               
        order.requirement_submit_date = timezone.now()       
        order.save(update_fields=['order_status_id','requirement_submit_date'])  
        notify_user.delay(events.ORDER_ADDITIONAL_REQUIREMENT,{'order':order,})      
        return Response(status=status.HTTP_200_OK)


'''
View to send data in specific format for POST method
(endpoint = '/orders/gig-requirements-details/:order_id')
'''
class OrderGigRequirementsDetailsView(ListAPIView) :
    """
    Order Gig Requirements GET API .

    """ 
    serializer_class = OrderGigRequirementsSerializer    

    def get_queryset(self) :
        order_id = self.kwargs['order_id']
        queryset = OrderGigRequirements.objects.filter(order_id=order_id)
        queryset = self.serializer_class.setup_eager_loading(self,queryset)
        return queryset