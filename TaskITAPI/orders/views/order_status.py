from rest_framework.generics import GenericAPIView,ListAPIView,CreateAPIView,ListCreateAPIView
from orders.models import Order,OrderPayment,OrderDigitalDownload,OrderComment,OrderRating
from django.db import transaction 
from rest_framework import status
from rest_framework.response import Response 
from rest_framework import serializers
from django.utils import timezone
from rest_framework.exceptions import ValidationError,PermissionDenied
from django.utils.translation import gettext as _
from orders.serializers.order_status import SubmitReviewSerializer,SubmitReviewAttachmentUpdateSerializer,OrderDeliveryListSerializer,OrderDeliveryReviewListSerializer,ModificationRequiredSerializer,\
    OrderCompletedSerializer,OrderCancelledSerializer
from configs.values import ORDER_STATUS
from configs.models import OrderCancelReason
from notifications.tasks import notify_user
from notifications import events
from accounts.values import GROUP
from orders.utils import nodal_to_seller_wallet_payment

'''
View to send data in specific format for POST method
(endpoint = '/orders/active/:id')
'''
class OrderActiveView(GenericAPIView) :
    """
    Order ACtive.

    Query parameters for GET method
    ---------------------------------------
    1. status = (succes/fail)

    Note : pass status as url parameter

    """         
    queryset = Order.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'

    def patch(self, request, id) :      
        order = self.get_object()
        # source = self.request.GET.get('status')
        # if not source :
        #     raise ValidationError(_('Source is mandatory.'),code='source_is_mandatory')
        is_success = False
        if order.order_status_id != ORDER_STATUS['Draft'] :
            raise PermissionDenied  
        order.modified_by = request.user
        order.modified_user = request.user.user_name
        order.modified_date = timezone.now()           
        # if status.lower() == 'succes' :  
        #     is_success = True                 
        order.order_status_id = ORDER_STATUS['Active']
        order.save()
        # if is_success :
        #     pass
        #     # notify_user.delay(events.ORDER_SUBMIT_TO_SELLER,{'order':order})
        #     # notify_user.delay(events.ORDER_SUBMIT_TO_BUYER,{'order':order})
        # else :
        #     pass
        #     # notify_user.delay(events.ORDER_PAYMENT_FAILED,{'order':order})    
        return Response(status=status.HTTP_200_OK)





'''
View to send data in specific format for POST method
(endpoint = '/orders/submit-review/')
'''
class OrderSubmitReviewView(GenericAPIView) :
    """
    Order Submit Review POST API .

    """       
    serializer_class = SubmitReviewSerializer    

    @transaction.atomic
    def post(self,request) :       
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data    
        digital_download = None    
        order_digital_download = None
        order_digital_download_id = None
        # access to seller only
        
        order = Order.objects.get(id=validated_data['order_id'])
        if order.order_status_id not in (ORDER_STATUS['In-Process'],ORDER_STATUS['Modification_Required']) : 
            raise PermissionDenied

        if 'digital_download' in validated_data :
            digital_download = validated_data.pop('digital_download')
        if 'comment' in validated_data :
            comment = validated_data.pop('comment')    
       
        if digital_download :
            order_digital_download = OrderDigitalDownload.objects.create(
                order = order,
                gig = order.gig,
                seller_id = digital_download['seller_id'],
                buyer_id = digital_download['buyer_id'],                      
                file_size = digital_download['file_size'],
                created_by = request.user,
                created_user = request.user.user_name,
                created_date = timezone.now()
            )    
            order_digital_download_id = order_digital_download.id
        order_comment = OrderComment.objects.create(
            order_digital_download = order_digital_download,
            order  = order,
            gig = order.gig,
            comment = comment['comment'],
            created_by = request.user,
            created_user = request.user.user_name,
            created_date = timezone.now()
        )      
        order.order_status_id = ORDER_STATUS['Submit_Review']
        order.modified_by = request.user
        order.modified_user = request.user.user_name
        order.modified_date = timezone.now()
        order.save(update_fields=['order_status_id','modified_by','modified_user','modified_date'])
        res = {
            'order_digital_download_id' : order_digital_download_id
        }
        notify_user.delay(events.ORDER_SUBMIT_REVIEW,{'order':order})
        return Response(res,status=status.HTTP_201_CREATED)


'''
View to send data in specific format for POST method
(endpoint = '/orders/submit-review-attchment/:id')
'''
class OrderSubmitReviewAttachmentUpdateView(GenericAPIView) :

    queryset = OrderDigitalDownload.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    serializer_class = SubmitReviewAttachmentUpdateSerializer    

    @transaction.atomic
    def patch(self,request,id) :       
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data       
        order_digital_download = self.get_object() 
        order_digital_download.filename = validated_data['filename']
        order_digital_download.save()
        return Response(status=status.HTTP_201_CREATED)




'''
View to send data in specific format for POST method
(endpoint = '/orders/delivery-download/:id')
'''
class OrderDeliveryDownloadView(GenericAPIView) :
    """
    Order Delivery Download PATCH API    
    
    """
    queryset = OrderDigitalDownload.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'
    
    def patch(self, request, id) :      
        order_digital_download = self.get_object()
        order_digital_download.buyer_download = True
        order_digital_download.buyer_download_date = timezone.now()
        order_digital_download.save()
        return Response(status=status.HTTP_200_OK)


'''
View to send data in specific format for POST method
(endpoint = '/orders/delivery/:order_id')
'''
class OrderDeliveryListView(ListAPIView) :
    """
    Order Delivery List     
    
    """
    queryset = Order.objects.all()
    serializer_class = OrderDeliveryListSerializer

    def get_queryset(self) :
        order_id = self.kwargs['order_id']        
        queryset = Order.objects.filter(id=order_id)            
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send data in specific format for POST method
(endpoint = '/orders/delivery-review-list/:order_id')
'''
class OrderDeliveryReviewListView(ListAPIView) :
    """
    Order Delivery Review List     
    
    """
    serializer_class = OrderDeliveryReviewListSerializer

    def get_queryset(self) :
        order_id = self.kwargs['order_id']        
        queryset = OrderRating.objects.filter(order=order_id)            
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset        


'''
View to send data in specific format for POST method
(endpoint = '/orders/modifcation-required/')
'''
class OrderModificationRequiredView(CreateAPIView) :
    """
    Order Modification Required POST API .

    """       
    serializer_class = ModificationRequiredSerializer  

    @transaction.atomic
    def create(self,request,*args,**kwargs) :   
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data 
        # Access to Buyer only
        order_comment = OrderComment.objects.get(id=validated_data['order_comment_id'])
        order = order_comment.order
        if order.order_status_id != ORDER_STATUS['Submit_Review'] :
            raise PermissionDenied

        order_comment = OrderComment.objects.create(
            order_digital_download = order_comment.order_digital_download,
            order = order_comment.order,
            gig = order_comment.gig,
            comment_ref = order_comment,
            comment = validated_data['comment'],
            created_by = request.user,
            created_user = request.user.user_name,
            created_date = timezone.now()
        )       
     
        order.order_status_id = ORDER_STATUS['Modification_Required']
        order.modified_by = request.user
        order.modified_user = request.user.user_name
        order.modified_date = timezone.now()
        order.save(update_fields=['order_status_id','modified_by','modified_user','modified_date'])
        notify_user.delay(events.ORDER_MODIFICATION_REQUIRED,{'order':order,'order_comment':order_comment})
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for POST method
(endpoint = '/orders/completed/')
'''
class OrderCompletedView(CreateAPIView) :
    """
    Order Completed.

    """       
    serializer_class = OrderCompletedSerializer    

    @transaction.atomic
    def create(self,request,*args,**kwargs) :        
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data  
        # Access to buyer only   
        order = Order.objects.get(id=validated_data['order_id'])
        if order.order_status_id != ORDER_STATUS['Submit_Review'] :
            raise PermissionDenied
        if 'rating' in validated_data :
            rating = validated_data.pop('rating')
        order_rating = OrderRating.objects.create(
            order = order,
            gig = order.gig,
            seller_id = rating['seller_id'],
            buyer_id = rating['buyer_id'],
            seller_feedback = rating['seller_feedback'],           
            seller_rating = rating['seller_rating'],
            created_by = request.user,
            created_user = request.user.user_name,
            created_date = timezone.now()
        )    
        gig = order.gig        
        gig.rated_no_of_records = 1 if not gig.rated_no_of_records else gig.rated_no_of_records + 1  
        gig.sum_of_rating = order_rating.seller_rating if not gig.sum_of_rating else gig.sum_of_rating + order_rating.seller_rating  
        gig.rating =  round(gig.sum_of_rating / gig.rated_no_of_records) 
        gig.modified_by = request.user
        gig.modified_user = request.user.user_name
        gig.modified_date = timezone.now()
        gig.save()
        order.order_status_id = ORDER_STATUS['Completed']
        order.modified_by = request.user
        order.modified_user = request.user.user_name
        order.modified_date = timezone.now()
        order.save(update_fields=['order_status_id','modified_by','modified_user','modified_date'])
        nodal_to_seller_wallet_payment(self,order)
        notify_user.delay(events.ORDER_COMPLETED_TO_BUYER,{'order':order})
        notify_user.delay(events.ORDER_COMPLETED_TO_SELLER,{'order':order})
        return Response(status=status.HTTP_201_CREATED)


'''
View to send data in specific format for POST method
(endpoint = '/orders/raise-dispute')
'''
class RaiseDisputeView(GenericAPIView) :
    """
    Order Raise Dispute.

    """       
    class RaiseDisputeSerializer(serializers.Serializer) :        
        dispute_remark = serializers.CharField()

    serializer_class = RaiseDisputeSerializer
    queryset = Order.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'

    @transaction.atomic
    def patch(self,request,id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data    
        order = self.get_object()
        if order.order_status_id not in (ORDER_STATUS['Submit_Review'],ORDER_STATUS['In-Process'],ORDER_STATUS['Modification_Required']) : 
            raise PermissionDenied
        order.dispute_remark = validated_data['dispute_remark']
        order.order_status_id = ORDER_STATUS['Dispute']
        order.modified_by = request.user
        order.modified_user = request.user.user_name
        order.modified_date = timezone.now()
        order.save(update_fields=['dispute_remark','order_status_id','modified_by','modified_user','modified_date'])
        return Response(status=status.HTTP_201_CREATED)

'''
View to send data in specific format for POST method
(endpoint = '/orders/cancelled')
'''
class OrderCancelledView(GenericAPIView) :
    """
    Order Cancelled
    """          
    serializer_class = OrderCancelledSerializer
    queryset = Order.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'

    @transaction.atomic
    def patch(self,request,id) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data    
        order = self.get_object()
        if order.order_status_id in (ORDER_STATUS['Draft'],ORDER_STATUS['Active'],ORDER_STATUS['Dispute'],ORDER_STATUS['Cancelled'] ): 
            raise PermissionDenied
        order.order_cancel_reason_id = validated_data['order_cancel_reason_id']
        order.order_cancel_reason_other = validated_data['order_cancel_reason_other']
        order.order_status_id = ORDER_STATUS['Cancelled']
        order.refund_status_id = REFUND_STATUS['pending']  
        order.modified_by = request.user
        order.modified_user = request.user.user_name
        order.modified_date = timezone.now()
        order.save(update_fields=['order_cancel_reason_id','order_cancel_reason_other','order_status_id','refund_status_id','modified_by','modified_user','modified_date'])
        if self.request.user.group_id == GROUP['buyer'] :            
            nodal_to_buyer_cancelled_by_buyer(self,order)
            notify_user.delay(events.ORDER_CANCELLED_BY_BUYER_TO_BUYER,{'order':order})
            notify_user.delay(events.ORDER_CANCELLED_BY_BUYER_TO_SELLER,{'order':order})
        elif self.request.user.group_id == GROUP['seller'] :          
            nodal_to_buyer_cancelled_by_seller(self,order)
            notify_user.delay(events.ORDER_CANCELLED_BY_SELLER_TO_SELLER,{'order':order})
            notify_user.delay(events.ORDER_CANCELLED_BY_SELLER_TO_BUYER,{'order':order})
        else :
            pass
        return Response(status=status.HTTP_201_CREATED)        