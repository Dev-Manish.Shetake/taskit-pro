from rest_framework.generics import GenericAPIView,ListAPIView,CreateAPIView,ListCreateAPIView
from rest_framework import serializers
from orders.models import Order,OrderPayment,FinanceTransaction,FinanceTransactionDetails,OrderRefundLog
from configs.models import GenLedger,GenLedgerType
from configs.values import GEN_LEDGER_TYPE,VOUCHER_TYPE
from accounts.models import User,UserBank
from rest_framework.exceptions import ValidationError,PermissionDenied
from django.utils.translation import gettext as _
from configs.values import ORDER_STATUS
from orders.serializers import FinanceTransactionWithdrawalListSerializer,OrderIncomingWalletListSerializer
from django.db.models import Q,Sum
from rest_framework import status
from rest_framework.response import Response
from django.db import transaction
from django.utils import timezone
from settings.models import SystemConfiguration
from settings.values import configs
from orders.utils import seller_wallet_to_seller_bank_payment
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db import connection
from orders.services import transfer_payment
import datetime

'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/incoming/')
'''
class OrderIncomingWalletListView(ListAPIView) :
    """
    Finance Transaction List   

    Query parameters for GET method
    -------------------------------------------
    1. search = Filter by order_no,amount,Buyer Name.
    2. order_date = Filter by order date.
    3. order_no = Filter by order no.
    4. amount = Filter by amount.
    5. buyer_name = Filter by buyer name.

    E.g. http://127.0.0.1:8000/api/v1/orders_wallet/incoming?search=    
    """
    queryset = Order.objects.all()
    serializer_class = OrderIncomingWalletListSerializer
    pagination_class = StandardResultsSetPagination

    def apply_filter(self,queryset) :
        q_objects = Q()
        order_date = self.request.GET.get('date')
        order_no = self.request.GET.get('order_no')       
        amount = self.request.GET.get('amount')
        buyer_name = self.request.GET.get('buyer_name')
        search = self.request.GET.get('search')
       
        if search :   
            queryset = queryset.filter(
                Q(order_no=search) | 
                Q(buyer__user_name__icontains=search)                         
            )       
        if order_date :
            date_time_obj = datetime.datetime.strptime(order_date, '%Y-%m-%d')
            queryset = queryset.filter(created_date__year=date_time_obj.year,created_date__month=date_time_obj.month,created_date__day=date_time_obj.day)   
        if order_no :
            q_objects = q_objects.add(Q(order_no=order_no), Q.AND) 
        if buyer_name :
            q_objects = q_objects.add(Q(buyer__user_name__icontains=buyer_name), Q.AND)            
        if amount :
            q_objects = q_objects.add(Q(total_amount_without_service_fee=amount), Q.AND)              
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)    
        return queryset

    def get_queryset(self) :               
        queryset = Order.objects.filter(order_status_id=ORDER_STATUS['Completed'],seller_id=self.request.user)       
        queryset = self.apply_filter(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/withdrawal/')
'''
class FinanceTransactionWithdrawalListView(ListAPIView) :
    """
    Finance Transaction List   

    Query parameters for GET method
    -------------------------------------------
    1. search = Filter by Amount,Transaction_id 
    2. payable_date = Filter by Apayable_date
    3. amount = Filter by amount

    E.g. http://127.0.0.1:8000/api/v1/orders_wallet/withdrawal/?search=    
    """
    
    queryset = FinanceTransaction.objects.all()
    serializer_class = FinanceTransactionWithdrawalListSerializer
    pagination_class = StandardResultsSetPagination


    def apply_filter(self,queryset) :
        q_objects = Q()
        search = self.request.GET.get('search')
        payable_date = self.request.GET.get('payable_date')
        amount = self.request.GET.get('payable_date')
        if search :
            fin_tran_ids = FinanceTransactionDetails.objects.filter(amount=search).values_list('fin_tran_id')
            queryset = queryset.filter(Q(id__in=fin_tran_ids) | Q(order_payment__transaction_id=search))    
        if payable_date :
            date_time_obj = datetime.datetime.strptime(payable_date, '%Y-%m-%d')
            queryset = queryset.filter(created_date__year=date_time_obj.year,created_date__month=date_time_obj.month,created_date__day=date_time_obj.day)   
        if amount :
            fin_tran_ids = FinanceTransactionDetails.objects.filter(amount=amount).values_list('fin_tran_id')
            q_objects = q_objects.add(Q(id__in=fin_tran_ids), Q.AND) 
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)    
        return queryset

    def get_queryset(self) :       
        queryset = FinanceTransaction.objects.filter(
            voucher_type_id__in=(
                VOUCHER_TYPE['wallet_to_bank'],
                VOUCHER_TYPE['wallet_to_nodal_partial_payment'],
                VOUCHER_TYPE['payment_through_wallet']
            ),
            created_user=self.request.user
        )
        queryset = self.apply_filter(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/summary/')
'''
class OrderSummaryListView(GenericAPIView) :

    # serializer_class = OrdersCountSerializer
   
    def get(self, request, *args, **kwargs) :   
        with connection.cursor() as cursor:
            cursor.callproc('sp_get_order_withdrawal_summary',[request.user.id])  
            from TaskITAPI.serializers import StoredProcedureSerializer         
            data = StoredProcedureSerializer(cursor).data           
            if type(data) == dict and not data :
               data = []             
        return Response(data, status=status.HTTP_200_OK)  


'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/withdrawal-count/')
'''
class WithdrawalCountView(GenericAPIView) :

    # serializer_class = OrdersCountSerializer
   
    def get(self, request, *args, **kwargs) :   
        with connection.cursor() as cursor:
            cursor.callproc('sp_get_withdrawal_count',[request.user.id])  
            from TaskITAPI.serializers import StoredProcedureSerializer         
            data = StoredProcedureSerializer(cursor).data           
            if type(data) == dict and not data :
               data = []             
        return Response(data, status=status.HTTP_200_OK)  


'''
View to send data in specific format for POST method
(endpoint = '/orders_wallet/withdraw-amount/')
'''
class SellerWithdrawAmountView(GenericAPIView) :

    class SellerWithdrawAmountSerializer(serializers.Serializer) :
        amount = serializers.DecimalField(max_digits=9,decimal_places=2)

    serializer_class = SellerWithdrawAmountSerializer

    def _validate_withdraw_amount(self,withdraw_amount) :
        #  Seller total balance amount.
        gen_ledger = GenLedger.objects.filter(user=self.request.user,gen_ledger_type_id=GEN_LEDGER_TYPE['user_wallet'])
        fin_tran_dtl = FinanceTransactionDetails.objects.filter(
            gen_ledger_id = gen_ledger[0].id,
            fin_tran_id__in = FinanceTransaction.objects.filter(
                voucher_type_id__in=(
                    VOUCHER_TYPE['nodal_to_wallet'],
                    VOUCHER_TYPE['wallet_to_bank'],
                    VOUCHER_TYPE['wallet_to_nodal_partial_payment'],
                    VOUCHER_TYPE['payment_through_wallet']
                    )
                ).values_list('id')
        ).aggregate(total_balance_amount=Sum('amount'))  
        total_balance_amount = fin_tran_dtl['total_balance_amount']          
        if not total_balance_amount or int(total_balance_amount) < withdraw_amount :
            raise ValidationError(_('Invalid amount to withdraw'),code='invalid_amount_to_withdraw')
        return withdraw_amount
   

    def pay_withdraw_amount(self,withdraw_amount) :       
        user_bank = UserBank.objects.filter(user=self.request.user)
        if len(user_bank) < 0 :
            raise ValidationError(_('User bank details not found'),code='bank_details_not_found')
        sys_config = SystemConfiguration.objects.filter(code_name=configs.NODAL_ACCOUNT_USER)
        if len(sys_config) < 0:
            raise ValidationError(_('Nodal Account configuration in sys config not found!'),code='nodal_config_not_found')
        nodal_user = User.objects.filter(id=sys_config[0].field_value)
        if len(nodal_user) < 0:
            raise ValidationError(_('Nodal User Account NotFound!'),code='nodal_user_not_found')
        # Transfer Amount Def Calling
        pay_res = transfer_payment(self,user_bank,withdraw_amount)
        if pay_res['id'] :
            order_payment = OrderPayment.objects.create(
                from_user =  nodal_user[0],
                to_user = self.request.user,
                razorpay_payment_no = pay_res['id'],
                created_by = self.request.user,
                created_user = self.request.user.user_name                
            )
            seller_wallet_to_seller_bank_payment(self,order_payment,withdraw_amount)
            res = {
            'code' : 'success',
            'message' : 'withdraw success!'
            }
        else :
            res = {
                'code' : 'success',
                'message' : 'withdraw success!'
            }
        return res          

    @transaction.atomic
    def post(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        withdraw_amount = serializer.validated_data['amount']
        withdraw_amount = self._validate_withdraw_amount(withdraw_amount)
        res = self.pay_withdraw_amount(withdraw_amount)
        return Response(res,status=status.HTTP_200_OK)
