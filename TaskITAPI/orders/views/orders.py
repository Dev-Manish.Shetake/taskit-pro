from rest_framework.generics import GenericAPIView,ListAPIView,ListCreateAPIView,CreateAPIView,RetrieveAPIView
from orders.models import Order,OrderComment
from accounts.models import User,UserPersonalInfo
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db import transaction 
from rest_framework.response import Response
from rest_framework import status
from orders.utils import get_user_data
from orders.serializers import OrdersListSerializer,OrdersCreateSerializer,OrdersDetailsSerializer,OrdersReviewsListSerializer,OrdersCountSerializer,OrdersRefundListSerializer
from django.db.models import Q
from django.db import connection
from TaskITAPI.serializers import StoredProcedureSerializer
import datetime
from configs.values import ORDER_STATUS

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders/list)
'''
class OrderListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. gig_title = Filter by gig title
    2. order_date = Filter by Date of order
    3. seller_name = Filter by seller name
    4. buyer_name = Filter by buyer name
    5. order_status_id = To filter by Order Status
    6. order_no = Filter by order no.
    6. search = Filter by (Gig Title , Seller Name, Buyer Name)
   
    E.g. http://127.0.0.1:8000/api/v1/orders/?search=

    """
    serializer_class = OrdersListSerializer
    pagination_class = StandardResultsSetPagination
 

    def apply_filters(self,queryset) :
        q_objects = Q()        
        order_no = self.request.GET.get('order_no')
        order_date = self.request.GET.get('order_date')
        gig_title = self.request.GET.get('gig_title')
        seller_name = self.request.GET.get('seller_name')
        seller_id = self.request.GET.get('seller_id')
        buyer_name = self.request.GET.get('buyer_name')
        buyer_id = self.request.GET.get('buyer_id')
        order_status_id = self.request.GET.get('order_status_id')
        search = self.request.GET.get('search')

        if search :            
            queryset = queryset.filter(Q(gig__title__icontains=search) | Q(seller__user_name__icontains=search) | Q(buyer__user_name__icontains=search))
        if order_no :
            q_objects.add(Q(order_no=order_no), Q.AND) 
        if order_date :
            date_time_obj = datetime.datetime.strptime(order_date, '%Y-%m-%d')
            queryset = queryset.filter(created_date__year=date_time_obj.year,created_date__month=date_time_obj.month,created_date__day=date_time_obj.day)   
        if gig_title :
            q_objects.add(Q(gig__title__icontains=gig_title), Q.AND) 
        if seller_name :
            q_objects.add(Q(seller__user_name__icontains=seller_name), Q.AND) 
        if seller_id :
            q_objects.add(Q(seller_id=seller_id), Q.AND)                
        if buyer_name :
            q_objects.add(Q(buyer__user_name__icontains=buyer_name), Q.AND)  
        if buyer_id :
            q_objects.add(Q(buyer_id=buyer_id), Q.AND)             
        if order_status_id :
            if int(order_status_id) == ORDER_STATUS['In-Process'] :
                q_objects.add(Q(order_status_id__in=[ORDER_STATUS['In-Process'],ORDER_STATUS['Modification_Required']]), Q.AND)  
            else :
                q_objects.add(Q(order_status_id=order_status_id), Q.AND)               
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL             
        if is_active == 'false' :    
            queryset = Order.all_objects.filter(is_active=False)
        else : 
            queryset = Order.objects.all()
        queryset = self.apply_filters(queryset)
        queryset = get_user_data(self.request,queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders/counts')
'''
class OrderCountsView(GenericAPIView) :
    """
    Orders Counts 

    """
    serializer_class = OrdersCountSerializer
   
    def get(self, request, *args, **kwargs) :   
        with connection.cursor() as cursor:
            cursor.callproc('sp_get_order_tab_count',[request.user.id])  
            from TaskITAPI.serializers import StoredProcedureSerializer         
            data = StoredProcedureSerializer(cursor).data           
            if type(data) == dict and not data :
               data = []             
        return Response(data, status=status.HTTP_200_OK)  


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders/create)
'''
class OrderCreateView(CreateAPIView) :
    serializer_class = OrdersCreateSerializer


'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders/list)
'''
class OrderDetailsView(RetrieveAPIView) :

    serializer_class = OrdersDetailsSerializer
    queryset = Order.objects.all()

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL             
        if is_active == 'false' :    
            queryset = Order.all_objects.filter(is_active=False)
        else : 
            queryset = Order.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders/backoffice-review-list')
'''
class OrderRatingListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. search = To filter by Title/Seller Name and Review by
   
    E.g. http://127.0.0.1:8000/api/v1/orders/backoffice-review-list

    """
    serializer_class = OrdersReviewsListSerializer
    pagination_class = StandardResultsSetPagination
 

    def apply_filters(self,queryset) :
        q_objects = Q()
        search = self.request.GET.get('search')       
              
        if search :
            queryset = queryset.filter(
                Q(gig__title=search) |
                Q(gig__created_user=search) |               
                Q(created_user=search)            
            )         
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset    

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL             
        if is_active == 'false' :    
            queryset = OrderComment.all_objects.filter(is_active=False)
        else : 
            queryset = OrderComment.objects.all()
        queryset = self.apply_filters(queryset)       
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset



'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders/refund-list/)
'''
class OrderRefundListView(ListAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
   
    1. order_date = Filter by Date of order
    2. order_no = Filter by order no.
    3. cancel_reason_id = Filter by Cancel Reason ID
    4. refund_status_id = Filter by Refund Status ID  
      
    E.g. http://127.0.0.1:8000/api/v1/orders/refund-list/?order_date=&order_no=&cancel_reason_id=&refund_status_id=

    """
    serializer_class = OrdersRefundListSerializer
    pagination_class = StandardResultsSetPagination
 

    def apply_filters(self,queryset) :
        q_objects = Q()        
        order_no = self.request.GET.get('order_no')
        order_date = self.request.GET.get('order_date')
        order_cancel_reason_id = self.request.GET.get('cancel_reason_id')
        refund_status_id = self.request.GET.get('refund_status_id')
        search = self.request.GET.get('search')

        if order_no :
            q_objects.add(Q(order_no=order_no), Q.AND) 
        if order_date :
            date_time_obj = datetime.datetime.strptime(order_date, '%Y-%m-%d')
            queryset = queryset.filter(created_date__year=date_time_obj.year,created_date__month=date_time_obj.month,created_date__day=date_time_obj.day)   
        if order_cancel_reason_id :
            q_objects.add(Q(order_cancel_reason_id=order_cancel_reason_id), Q.AND) 
        if refund_status_id :
            q_objects.add(Q(refund_status_id=refund_status_id), Q.AND) 
        if len(q_objects) > 0 :
            queryset = queryset.filter(q_objects)
        return queryset   

    def get_queryset(self) :
        queryset = Order.objects.filter(order_status=ORDER_STATUS['Cancelled'])
        queryset = self.apply_filters(queryset)
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset