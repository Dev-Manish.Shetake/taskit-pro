from rest_framework.generics import ListAPIView,ListCreateAPIView,RetrieveUpdateAPIView,RetrieveDestroyAPIView,GenericAPIView
from orders.models import Order,OrderPayment
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db.models import Q
from TaskITAPI.pagination import StandardResultsSetPagination
from django.db import transaction 
from rest_framework import status
from rest_framework.response import Response 
from rest_framework import serializers

'''
View to send/receive data in specific format for GET/POST method
(endpoint = '/orders-payment-create')
'''
class OrderPaymentCreateView(GenericAPIView) :

    class  OrderPaymentSerializer(serializers.Serializer) :
       pass

    serializer_class = OrderPaymentSerializer
    
    @transaction.atomic    
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)       
        return Response(status=status.HTTP_200_OK)   
    