from django.db import models
from accounts.models import User

class RazorpayGateway(models.Model):
    key_id = models.CharField(max_length=50,verbose_name="Key ID")
    key_secret = models.CharField(max_length=50,verbose_name="key Secret")
    mode = models.CharField(max_length=50)
    created_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='razorpay_gateway_created_by') 
    created_date = models.DateTimeField(auto_now_add=True) 
    created_user = models.CharField(max_length=50, null=True)
    modified_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='razorpay_gateway_modified_by')
    modified_user = models.CharField(max_length=50, null=True)
    modified_date = models.DateTimeField(auto_now=True, null=True, blank=True)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.DO_NOTHING, related_name='razorpay_gateway_deleted_by')
    deleted_user = models.CharField(max_length=50, null=True)
    deleted_date = models.DateTimeField(null=True)
    
    class Meta :
        db_table = 'razorpay_gateway'
        default_permissions = ()

