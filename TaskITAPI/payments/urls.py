from django.urls import include,path
from payments.views import PaymentTransactionView,PaymentTransactionSuccessView,SellerWalletPaymentView,RefundPaymentView

urlpatterns = [
    path('pay',PaymentTransactionView.as_view()),
    path('success',PaymentTransactionSuccessView.as_view()),
    path('wallet-payment',SellerWalletPaymentView.as_view()),
    path('refund-payment',RefundPaymentView.as_view())      
]