from payments.models import RazorpayGateway


class RazorpayGatewayNotFound(Exception) :
   pass

"""
Razorpay Gateway Configuration
"""
def RazorpayGatewayConfig() :
    razorpay_gateway = RazorpayGateway.objects.all()
    if len(razorpay_gateway) == 0 :
        raise RazorpayGatewayNotFound 
    razorpay_gateway = razorpay_gateway[0]
    return razorpay_gateway 




