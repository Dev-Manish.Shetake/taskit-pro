from rest_framework.generics import GenericAPIView
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response 
from TaskITAPI.settings import RZP_CONFIG,BASE_URL,DEFAULT_CURRENCY
from accounts.models import User
from orders.models import Order,OrderPayment,FinanceTransaction,FinanceTransactionDetails,OrderRefundLog
from django.shortcuts import render, render_to_response
from django.http import HttpResponse,HttpResponseRedirect
from django.template.loader import get_template
from django.template import Context, Template,RequestContext
import datetime,hmac,hashlib,requests,json,uuid
from random import randint
from rest_framework.exceptions import ValidationError,PermissionDenied
from django.utils.translation import gettext as _
from configs.values import VOUCHER_TYPE,GEN_LEDGER_TYPE
from django.utils import timezone
from orders.utils import buyer_to_nodal_complet_payment,buyer_to_nodal_partial_payment,nodal_to_seller_wallet_payment,\
    buyer_to_nodal_partial_wallet_payment,buyer_to_nodal_complete_wallet_payment        
import razorpay
from payments.utils import RazorpayGatewayConfig
from settings.models import SystemConfiguration
from settings.values import configs
from configs.values import ORDER_PAYMENT_STATUS
from django.db import transaction 
from notifications.tasks import notify_user
from notifications import events



class PaymentTransactionView(GenericAPIView) :

    class TransactionSerializer(serializers.Serializer) :
       order_id = serializers.IntegerField()

       def validate_order_id(self,value) :
           if not Order.objects.filter(id=value).exists() :
               raise ValidationError(_('Invalid input received for order id'),code='order_id_inalid_input')
           return value

    serializer_class = TransactionSerializer

    def create_order_payment(self,order,client_order) :
        sys_config = SystemConfiguration.objects.filter(code_name=configs.NODAL_ACCOUNT_USER)      
        nodal_user = User.objects.filter(id=sys_config[0].field_value)
        if len(nodal_user) > 0 :
            nodal_user = nodal_user[0]
        order_payment = OrderPayment.objects.create(
            order = order,
            razorpay_order_id = client_order['id'],
            from_user = order.created_by,
            to_user = nodal_user,
            order_payment_status_id = ORDER_PAYMENT_STATUS['Inprocess'],
            created_by = self.request.user,
            created_user = self.request.user.user_name
        ) 
        return order_payment

    @transaction.atomic
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data       
        order = Order.objects.get(id=validated_data['order_id'])              
        razorpay_gateway = RazorpayGatewayConfig()
        key_id = razorpay_gateway.key_id      
        key_secret = razorpay_gateway.key_secret   
        client = razorpay.Client(auth=(key_id,key_secret))
        # Create razorpay order 
        _receipt = 'RCP-' + str(randint(1, 9999))
        data = {
            'amount' : int(order.total_amount) * 100,
            'currency' : DEFAULT_CURRENCY, 
            'receipt' : _receipt,
            'notes' : {
                'buyer_name' : order.created_user,
                'payment_for' : order.gig.title 
            }    
        } 
        client_order = client.order.create(data=data)
        print(client_order['id']) 
        order_payment = self.create_order_payment(order,client_order)  
        return Response(client_order,status=status.HTTP_201_CREATED)



class PaymentTransactionSuccessView(GenericAPIView) :

    class TransactionDoneSerializer(serializers.Serializer) :
        razorpay_order_id = serializers.CharField(max_length=100)
        razorpay_payment_id = serializers.CharField(max_length=100)
        razorpay_signature = serializers.CharField(max_length=100)
        amount = serializers.DecimalField(max_digits=9,decimal_places=2)

    serializer_class = TransactionDoneSerializer    

    @transaction.atomic
    def post(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data        
        client = razorpay.Client(auth=(RZP_CONFIG.get('key_id'),RZP_CONFIG.get('key_secret')))
        sig_basestring = str(validated_data['razorpay_order_id']) + "|" + str(validated_data['razorpay_payment_id'])
        generated_signature = hmac.new(RZP_CONFIG.get('key_secret').encode('utf-8'),sig_basestring.encode('utf-8'),digestmod=hashlib.sha256).hexdigest()
        order_payment = OrderPayment.objects.filter(razorpay_order_id=validated_data['razorpay_order_id'])
        order_payment = order_payment[0]
        if generated_signature == validated_data['razorpay_signature']:
            if order_payment.order.total_amount == validated_data['amount'] :
                buyer_to_nodal_complet_payment(self,order_payment)
                order_payment.order_payment_status_id=ORDER_PAYMENT_STATUS['Success']
                order_payment.save()
                order_payment.order.order_payment_status = ORDER_PAYMENT_STATUS['Success']
                order_payment.order.save()                
            else :
                buyer_to_nodal_partial_payment(self,order_payment,validated_data['amount'])        
            order_payment.razorpay_payment_no = validated_data['razorpay_payment_id']
            order_payment.razorpay_signature = generated_signature        
            order_payment.modified_by = request.user
            order_payment.modified_user = request.user.user_name
            order_payment.modified_date = timezone.now()
            order_payment.save()
            notify_user.delay(events.ORDER_SUBMIT_TO_SELLER,{'order':order_payment.order})
            notify_user.delay(events.ORDER_SUBMIT_TO_BUYER,{'order':order_payment.order})
            res = {
               'code' : 'payment_success',
               'message' : 'Payment has been succesfully done!'
            }
        else :
            order_payment.order_payment_status_id=ORDER_PAYMENT_STATUS['Failure']
            order_payment.modified_by = request.user
            order_payment.modified_user = request.user.user_name
            order_payment.modified_date = timezone.now()
            order_payment.save()
            notify_user.delay(events.ORDER_PAYMENT_FAILED,{'order':order_payment.order}) 
            res = {
               'code' : 'payment_failed',
               'message' : 'Payment failed!'
             }           
        return Response(res,status=status.HTTP_201_CREATED)



class SellerWalletPaymentView(GenericAPIView) :
    """
    Note : If payment is partial ie.Account+Wallet then is_complete_wallet_payment shoud be False else is_complete_wallet_payment shoud be True
    """
    class SellerWalletPaymentSerializer(serializers.Serializer) :
        order_id = serializers.IntegerField()
        razorpay_payment_id = serializers.CharField(max_length=100,allow_null=True)
        wallet_amount = serializers.CharField(max_length=100)
        is_complete_wallet_payment = serializers.BooleanField() 

        def validate_order_id(self,value) :
            if not Order.objects.filter(id=value).exists() :
                raise ValidationError(_('Order id not exists.'),code='invalid_input')
            return value    

        def validate_razorpay_payment_id(self,value) :
            if value :
                if not OrderPayment.objects.filter(razorpay_payment_no=value).exists() :
                    raise ValidationError(_('Razorpay Payment ID'),code='invalid_input')
            return value            
        
      
    serializer_class = SellerWalletPaymentSerializer 
    
    @transaction.atomic
    def post(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data   
        from accounts.values import USER_TYPE
        user = User.objects.get(id=request.user.id)
        if user.user_type_id != USER_TYPE['buyer'] :
            raise PermissionDenied
        wallet_amount = validated_data['wallet_amount']
        is_complete_wallet_payment = validated_data['is_complete_wallet_payment']
        if is_complete_wallet_payment :
            sys_config = SystemConfiguration.objects.filter(code_name=configs.NODAL_ACCOUNT_USER)      
            nodal_user = User.objects.filter(id=sys_config[0].field_value)
            if len(nodal_user) > 0 :
                nodal_user = nodal_user[0]
            order_payment = OrderPayment.objects.create(
                order_id = validated_data['order_id'],               
                from_user = self.request.user,
                to_user = nodal_user,
                order_payment_status_id = ORDER_PAYMENT_STATUS['Inprocess'],
                created_by = self.request.user,
                created_user = self.request.user.user_name
            ) 
            buyer_to_nodal_complete_wallet_payment(self,order_payment)  
        else :
            order_payment = OrderPayment.objects.filter(razorpay_payment_no=validated_data['razorpay_payment_id'])
            order_payment = order_payment[0]
            buyer_to_nodal_partial_wallet_payment(self,order_payment,wallet_amount)  
            order_payment.order_payment_status_id=ORDER_PAYMENT_STATUS['Success']
            order_payment.save()
            order_payment.order.order_payment_status = ORDER_PAYMENT_STATUS['Success']
            order_payment.order.save()  
        return Response(status=status.HTTP_201_CREATED)


"""
Refund payment to seller account when order get cancelled by Seller/Buyer 

View to send data in specific format for POST method
(endpoint = '/payments/refund-payment/')

"""
class RefundPaymentView(GenericAPIView) :

    class RefundPaymentSerializer(serializers.Serializer) :
        razorpay_payment_id = serializers.CharField(max_length=100,allow_null=True)
        payment_amount = serializers.CharField(max_length=100)
        remark = serializers.CharField(max_length=500)

        def validate_payment_amount(self,value) :
            if not OrderPayment.objects.filter(razorpay_payment_no=value).exists() :
                raise ValidationError(_('Razorpay payment id not exists.'),code='not_exists')
            return value

    serializer_class = RefundPaymentSerializer 

    @transaction.atomic
    def post(self,request,*args,**kwargs) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data        
        import razorpay
        razorpay_gateway = RazorpayGatewayConfig()
        client = razorpay.Client(auth=(razorpay_gateway.key_id, razorpay_gateway.key_secret))
        # from order_payment
        payment_id = validated_data['razorpay_payment_id']
        remark = validated_data['remark']
        payment_amount = int(validated_data['payment_amount']) * 100
        res = client.payment.refund(payment_id, payment_amount)
        if res['id'] not in res :
            res = {
                'message' : 'Refund payment failed.',
                'code' : 'refund_failed'
            }
        else :        
            order_payment = OrderPayment.objects.filter(razorpay_payment_no=payment_id)
            order = order_payment[0].order
            from configs.values import REFUND_STATUS
            if res['status'] == 'processed' :            
                order.refund_status_id = REFUND_STATUS['done'] 
            else :         
                order.refund_status_id = REFUND_STATUS['pending'] 
            order.modified_by = request.user
            order.modified_user = request.user.user_name
            order.modified_date = timezone.now()    
            order.save(update_fields=['refund_status_id','modified_by','modified_user','modified_date'])    
            OrderRefundLog.objects.create(
                order=order,
                refunded_amount=payment_amount,
                remark=remark
            )
        return Response(res,status=status.HTTP_201_CREATED)