from rest_framework import serializers
from accounts.models import User,UserPersonalInfo,UserLanguage,UserSocialAccount,UserSkill,UserEducation,UserCertification
from accounts.serializers import UserSkillListSerializer,UserEducationListSerializer,UserCertificationSerializer,UserPersonalInfoSerializer
from django.db import transaction
from rest_framework.exceptions import PermissionDenied
from sellers.serializers.seller_general import UserLanguageSerializer,UserLanguageMinSerializer,UserLanguageUpdateSerializer
from accounts.values import USER_TYPE
from django.utils import timezone
from django.db.models import Prefetch



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/personal-info)
'''
class SellerPersonalInfoCreateSerializer(serializers.ModelSerializer) :
    profile_picture = serializers.FileField(required=False)
    description = serializers.CharField(max_length=300,required=False)
    # personal_website = serializers.CharField(required=False)
    languages = UserLanguageMinSerializer(many=True)

    class Meta :
        model = UserPersonalInfo
        fields = [
            'id',
            'first_name',
            'last_name',
            'profile_picture',
            'description',
            # 'personal_website',
            'languages'
        ]

    def create_user_languages(self,user,languages) :
        language_list = []
        for language in languages :
            t = UserLanguage(
                user = user,
                lang_id = language['lang_id'],
                lang_proficiency_id = language['lang_proficiency_id'],
                created_by = user,
                created_user = user.user_name,
                created_date = timezone.now()
            )
            language_list.append(t)
        if len(language_list) > 0 :
            UserLanguage.objects.bulk_create(language_list)

    @transaction.atomic
    def create(self,validated_data) :
        # user_id = self.context['request'].parser_context['kwargs']['user_id'] 
        user = self.context['request'].user       
        languages = []
        if 'languages' in validated_data :
            languages = validated_data.pop('languages')
        user = User.objects.get(id=user.id)  
        # if user.user_type_id == USER_TYPE['seller'] :
        #     raise PermissionDenied
        user_personal_info = UserPersonalInfo.objects.create(**validated_data,user=user)
        self.create_user_languages(user, languages)
        user.is_seller = False
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()
        user.save()
        user_personal_info.languages = []
        return user_personal_info


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/personal-info/:id)
'''
class SellerPersonalInfoDetailsSerializer(serializers.ModelSerializer) :
    profile_picture = serializers.FileField(required=False)
    personal_website = serializers.CharField(allow_blank=True,required=False)
    languages = UserLanguageUpdateSerializer(many=True,write_only=True)

    def setup_eager_loading(self,queryset) :
        return queryset

    class Meta :
        model = UserPersonalInfo
        fields = [
            'id',
            'first_name',
            'last_name',
            'profile_picture',
            'description',
            'personal_website',
            'languages'
        ]
    
    @transaction.atomic
    def update(self, user_personal_info, validated_data) :       
        user_update_fields = []
        languages = []
        if 'languages' in validated_data :
            languages = validated_data.pop('languages')
        for key, value in validated_data.items() :
            if getattr(user_personal_info, key) != value :
                user_update_fields.append(key)
                setattr(user_personal_info, key, value)        
        user_personal_info.save(update_fields = user_update_fields)
        for lang in languages :    
            user_language = UserLanguage.objects.filter(id=lang['id']).update(
                lang_id=lang['lang_id'],
                lang_proficiency_id=lang['lang_proficiency_id']
            )                 
        return user_personal_info    


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/user-personal-info/:id)
'''
class SellerUserPersonalInfoDetailsSerializer(serializers.ModelSerializer) :
    personal_info = UserPersonalInfoSerializer()
    languages = UserLanguageUpdateSerializer(many=True)

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('userpersonalinfo',UserPersonalInfo.objects.all(),to_attr='personal_info'),   
            Prefetch('userlanguage_set',UserLanguage.objects.all(),to_attr='languages')        
        )
        return queryset

    class Meta :
        model = User
        fields = [
            'id',
            'personal_info',
            'languages'                    
        ]


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/professional-info/:id)
'''
class SellerProfesionalInfoDetailsSerializer(serializers.ModelSerializer) :
    skills = UserSkillListSerializer(many=True)
    educations = UserEducationListSerializer(many=True)
    certifications = UserCertificationSerializer(many=True)
    personal_website = serializers.SerializerMethodField()  

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('userskill_set',UserSkill.objects.all(),to_attr='skills'),
            Prefetch('usereducation_set',UserEducation.objects.all(),to_attr='educations'),
            Prefetch('usercertification_set',UserCertification.objects.all(),to_attr='certifications'),
        )
        return queryset

    class Meta :
        model = User
        fields = [
            'id',
            'skills',
            'educations',
            'certifications',
            'personal_website'           
        ]

    def get_personal_website(self,instance) :
        personal_website = None
        personal_info = UserPersonalInfo.objects.filter(user_id=instance.id)
        if len(personal_info) > 0 :
            personal_website = personal_info[0].personal_website
        return personal_website    