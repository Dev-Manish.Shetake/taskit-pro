from rest_framework import serializers
from accounts.models import User,UserPersonalInfo,UserLanguage,UserSocialAccount,UserLanguage,UserSkill,UserEducation,UserCertification
from accounts.serializers import UserLanguageListSerializer,UserSocialAccountListSerializer,UserSkillListSerializer,UserEducationListSerializer,\
    UserCertificationSerializer
from django.db import transaction
from rest_framework.exceptions import PermissionDenied
from sellers.serializers.seller_general import UserLanguageSerializer,UserLanguageMinSerializer
from accounts.serializers import UserMinSerializer
from django.db.models import Prefetch


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/info/:id)
'''
class SellerPersonalInfoSerializer(serializers.ModelSerializer) :
       
    def setup_eager_loading(self,queryset) :
        return queryset

    class Meta :
        model = UserPersonalInfo
        fields = [
            'id', 
            'first_name',
            'last_name',
            'profile_picture',
            'description',  
        ]

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/info/:id)
'''
class SellerInfoSerializer(serializers.ModelSerializer) :   
    personal_info = SellerPersonalInfoSerializer()
    languages = UserLanguageListSerializer(many=True)
    accounts = UserSocialAccountListSerializer(many=True)
    skills = UserSkillListSerializer(many=True)
    educations = UserEducationListSerializer(many=True)
    certifications = UserCertificationSerializer(many=True)

    def setup_eager_loading(self,queryset) :
        queryset = queryset.prefetch_related(
            Prefetch('userpersonalinfo',UserPersonalInfo.objects.all(),to_attr='personal_info'),
            Prefetch('userlanguage_set',UserLanguage.objects.all(),to_attr='languages'),
            Prefetch('usersocialaccount_set',UserSocialAccount.objects.all(),to_attr='accounts'),
            Prefetch('userskill_set',UserSkill.objects.all(),to_attr='skills'),
            Prefetch('usereducation_set',UserEducation.objects.all(),to_attr='educations'),
            Prefetch('usereducation_set',UserCertification.objects.all(),to_attr='certifications'),
        )       
        return queryset

    class Meta :
        model = User
        fields = [
            'id',            
            'user_name',
            'rated_no_of_records',
            'rating',     
            'personal_info',
            'languages',
            'accounts',
            'skills',
            'educations',
            'certifications',
            'is_active',
            'created_date',
            'verified_email_address_date',
            'is_out_of_office'      
        ]        


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/info/:id)
'''
class SellerDashboardCountSerializer(serializers.Serializer) :   
    inprocess_orders = serializers.IntegerField()
    completed_orders = serializers.IntegerField()
    cancelled_orders = serializers.IntegerField()
    dispute_orders = serializers.IntegerField()
    total_orders = serializers.IntegerField()
    
