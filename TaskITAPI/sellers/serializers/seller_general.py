from rest_framework import serializers
from accounts.models import User,UserLanguage
from django.db import transaction
from rest_framework.exceptions import PermissionDenied


class UserLanguageMinSerializer(serializers.Serializer) :
    lang_id = serializers.IntegerField()
    lang_proficiency_id = serializers.IntegerField()
   

class UserLanguageUpdateSerializer(serializers.Serializer) :
    id = serializers.IntegerField()
    lang_id = serializers.IntegerField()
    lang_proficiency_id = serializers.IntegerField()


class UserLanguageSerializer(serializers.ModelSerializer) :
   
    class Meta :
        model = UserLanguage
        fields = [
            'id',
            'lang',
            'lang_proficiency',
        ]
