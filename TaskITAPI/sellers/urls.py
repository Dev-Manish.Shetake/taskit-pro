from django.urls import path,include
from sellers.views import SellerPersonalInfoCreate,SellerPersonalInfoDetailsView,SellerPersonalInfoDeleteView,SellerUserPersonalInfoDetailsView,\
    LinkedAccountsCreateView,SellerProfessionalInfoCreateView,SellerUserProfessionalInfoDetailsView,SellerInfoView,SellerDashboardCountView,\
        SecurityFinishCreateView,SellerGigsListView,UserAvailableStatusView

urlpatterns = [
    path('personal-info',SellerPersonalInfoCreate.as_view(),name='create-user-personal-info'),
    path('personal-info/<int:pk>',SellerPersonalInfoDetailsView.as_view(),name='update-user-personal-info'),
    path('delete-personal-info/<int:id>',SellerPersonalInfoDeleteView.as_view(),name='delete-user-personal-info'),
    path('user-personal-info/<int:pk>',SellerUserPersonalInfoDetailsView.as_view(),name='details-user-personal-info'),
    path('professional-info',SellerProfessionalInfoCreateView.as_view(),name='create-user-perofessional-info'),
    path('user-professional-info/<int:pk>',SellerUserProfessionalInfoDetailsView.as_view(),name='details-user-profesional-info'),
    path('linked-accounts',LinkedAccountsCreateView.as_view(),name='create-user-linked-accounts'),
    path('security-finish',SecurityFinishCreateView.as_view()),
    path('out-of-office',UserAvailableStatusView.as_view()),
    path('info/<int:user_id>',SellerInfoView.as_view()),
    path('gigs-list/<int:user_id>',SellerGigsListView.as_view()),
    path('dashboard-count',SellerDashboardCountView.as_view()),
]