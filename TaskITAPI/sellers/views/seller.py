from rest_framework import views
from rest_framework.generics import RetrieveAPIView,RetrieveUpdateAPIView,GenericAPIView,CreateAPIView,UpdateAPIView,DestroyAPIView
from sellers.serializers import SellerPersonalInfoCreateSerializer,SellerUserPersonalInfoDetailsSerializer,SellerPersonalInfoDetailsSerializer,SellerUserPersonalInfoDetailsSerializer,SellerProfesionalInfoDetailsSerializer
from accounts.models import User,UserPersonalInfo,UserSocialAccount,UserSkill,UserEducation,UserCertification
from rest_framework import serializers
from django.utils import timezone
from configs.values import SOCIAL_MEDIA
from rest_framework import status
from rest_framework.response import Response
from accounts.serializers import UserSkillSerializer,UserEducationSerializer,UserCertificationSerializer
from django.db import transaction
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/personal-info')
'''
class SellerPersonalInfoCreate(CreateAPIView) :
    serializer_class = SellerPersonalInfoCreateSerializer
   

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/personal-info')
'''
class SellerPersonalInfoDetailsView(UpdateAPIView) :
    queryset = UserPersonalInfo.objects.all()
    serializer_class = SellerPersonalInfoDetailsSerializer

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = UserPersonalInfo.all_objects.all().filter(is_active=False)
        else : 
            queryset = UserPersonalInfo.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/personal-info')
'''
class SellerPersonalInfoDeleteView(DestroyAPIView) :
    queryset = UserPersonalInfo.objects.all()
    lookup_field = 'id'
    lookup_url_kwarg = 'id'

    def delete(self,request,*args,**kwargs) :
        user_personal_info = self.get_object()
        user_personal_info.delete()       
        return Response(status=status.HTTP_204_NO_CONTENT)
   

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/user-personal-info')
'''
class SellerUserPersonalInfoDetailsView(RetrieveUpdateAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. id = User ID
    """  
    queryset = User.objects.all()
    serializer_class = SellerUserPersonalInfoDetailsSerializer

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = User.all_objects.all().filter(is_active=False)
        else : 
            queryset = User.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/professional-info')
'''
class SellerProfessionalInfoCreateView(CreateAPIView) :

    class SellerPerofessionalInfoCreateSerializer(serializers.Serializer) :
        skills = UserSkillSerializer(many=True)
        educations = UserEducationSerializer(many=True)
        certifications = UserCertificationSerializer(many=True)
        personal_website = serializers.CharField(max_length=200,allow_blank=True,required=False)

    serializer_class = SellerPerofessionalInfoCreateSerializer

    # Create Skills against Logged in User
    def create_user_skills(self,skills) :
        skill_list = []
        for skill in skills :
            s = UserSkill(
                user = self.request.user,
                skill_id = skill['skill_id'],
                skill_level_id = skill['skill_level_id'],
                created_by = self.request.user,
                created_user = self.request.user.user_name,
                created_date = timezone.now()
            )
            skill_list.append(s)
        if len(skill_list) > 0 :
            UserSkill.objects.bulk_create(skill_list)        

    # Create Education against Logged in User
    def create_user_educations(self,educations) :
        educations_list = []
        for education in educations :
            s = UserEducation(
                user = self.request.user,
                country_id = education['country_id'],
                university_name = education['university_name'],
                education_title_id = education['education_title_id'],
                education_major_id = education['education_major_id'],
                education_year = education['education_year'],
                created_by = self.request.user,
                created_user = self.request.user.user_name,
                created_date = timezone.now()
            )
            educations_list.append(s)
        if len(educations_list) > 0 :
            UserEducation.objects.bulk_create(educations_list)

    # Create Certification against Logged in User
    def create_user_certifications(self,certifications) :
        certifications_list = []
        for certification in certifications :
            s = UserCertification(
                user = self.request.user,
                certification_name = certification['certification_name'],
                certification_from = certification['certification_from'],
                certification_year = certification['certification_year'],
                created_by = self.request.user,
                created_user = self.request.user.user_name,
                created_date = timezone.now()
            )
            certifications_list.append(s)
        if len(certifications_list) > 0 :
            UserCertification.objects.bulk_create(certifications_list)        

    @transaction.atomic
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        skills = []
        educations = []
        certifications = []
        if 'skills' in validated_data :
            skills = validated_data.pop('skills')
        if 'educations' in validated_data :
            educations = validated_data.pop('educations')
        if 'certifications' in validated_data :
            certifications = validated_data.pop('certifications')      
        if 'personal_website' in validated_data :
            user_info = UserPersonalInfo.objects.get(user=request.user)
            user_info.personal_website = validated_data['personal_website']
            user_info.save()
        if len(skills) > 0 :
            self.create_user_skills(skills) 
        if len(educations) > 0 :
            self.create_user_educations(educations) 
        if len(certifications) > 0 :
            self.create_user_certifications(certifications)
        return Response(status=status.HTTP_201_CREATED)


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/user-professional-info')
'''
class SellerUserProfessionalInfoDetailsView(RetrieveAPIView) :
    """
    Query parameters for GET method
    ---------------------------------------
    1. id = User ID
    """  
    queryset = User.objects.all()
    serializer_class = SellerProfesionalInfoDetailsSerializer

    def get_queryset(self) :
        is_active =  self.request.GET.get('is_active') # Get query parameter from URL
        if self.request.method == 'GET' and is_active == 'false' :    
            queryset = User.all_objects.all().filter(is_active=False)
        else : 
            queryset = User.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset




'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/linked-accounts)
'''
class LinkedAccountsCreateView(GenericAPIView) :
    class LinkedAccountsSerializer(serializers.Serializer) :
        social_account_id = serializers.EmailField()
        provider = serializers.CharField()

    serializer_class = LinkedAccountsSerializer

    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        UserSocialAccount.objects.create(
            user = request.user,
            social_media_id = SOCIAL_MEDIA[serializer.validated_data['provider'].lower()],
            social_account_id = serializer.validated_data['social_account_id'],
            created_by = request.user,
            created_user = request.user.user_name,
            created_date = timezone.now()   
        )
        return Response(status=status.HTTP_201_CREATED)
      
'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/linked-accounts)
'''
class SecurityFinishCreateView(GenericAPIView) :
   
    def patch(self,request) :       
        user = request.user
        user.is_seller = True
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()   
        user.save(update_fields=['is_seller','modified_by','modified_user','modified_date'])
        return Response(status=status.HTTP_200_OK)



# View for PUT request (endpoint = '/user/firebase-device-id)  
class UserAvailableStatusView(GenericAPIView):
    """
    Note: Pass value as true/false in is_out_of_office field
    """
    class UserAvailableStatusSerializer(serializers.Serializer) :
        is_out_of_office = serializers.CharField(max_length=5)

    serializer_class = UserAvailableStatusSerializer
    
    @transaction.atomic    
    def post(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)       
        user = request.user
        if serializer.validated_data['is_out_of_office'].lower() == 'true' :
            is_out_of_office = True           
        elif serializer.validated_data['is_out_of_office'].lower() == 'false' :           
           is_out_of_office = False  
        else :
            raise ValidationError(_('Invalid Input received for is_out_of_office'),code='invalid_input')    
        user.is_out_of_office = is_out_of_office
        user.modified_by = user
        user.modified_user = user.user_name
        user.modified_date = timezone.now()   
        user.save()       
        return Response(status=status.HTTP_200_OK)   
