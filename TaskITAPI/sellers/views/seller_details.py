from rest_framework import views
from rest_framework.generics import RetrieveAPIView,GenericAPIView,CreateAPIView,ListAPIView
from rest_framework import serializers
from accounts.models import User,UserPersonalInfo
from gigs.models import Gig
from gigs.serializers import GigsSearchResultSerializer
from sellers.serializers import SellerInfoSerializer,SellerDashboardCountSerializer
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from django.db import connection
from configs.values import GIG_STATUS
from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext as _

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/info')
'''
class SellerInfoView(RetrieveAPIView) :
    queryset = User.objects.all()
    serializer_class = SellerInfoSerializer  
    lookup_field = 'id'
    lookup_url_kwarg = 'user_id'   
    permission_classes = []
    authentication_classes = []

    def get_queryset(self) :       
        queryset = User.objects.all()
        queryset = self.get_serializer_class().setup_eager_loading(self,queryset)
        return queryset


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/dashboard-count')
'''
class SellerDashboardCountView(ListAPIView) :
    """
    Get Seller Dashboard Count status stage wise(dashboard + menu)
    
    Query parameters for GET method
    ---------------------------------------    
  
    E.g  http://127.0.0.1:8000/api/v1/sellers/dashboard-count
    """

    serializer_class = SellerDashboardCountSerializer
    queryset = User.objects.none()
    result_set = []

    def get(self, request, *args, **kwargs) :       
        data = None    
        with connection.cursor() as cursor:
            cursor.callproc('sp_get_seller_dashboard_summary', [request.user.id])  
            from TaskITAPI.serializers import StoredProcedureSerializer         
            data = StoredProcedureSerializer(cursor, self.result_set).data
            if type(data) == dict and not data :
               data = []             
        return Response(data, status=status.HTTP_200_OK)         


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/sellers/gigs-list/:user_id')
'''
class SellerGigsListView(ListAPIView) :
    queryset = Gig.objects.all()
    serializer_class = GigsSearchResultSerializer      
    permission_classes = []
    authentication_classes = []

    def get_queryset(self) :
        created_by_id = self.kwargs['user_id']
        if not created_by_id :
            ValidationError(_('User Id required.'),code='user_id_required')
        queryset = Gig.objects.filter(created_by_id=created_by_id,gig_status_id=GIG_STATUS['Active'])
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset      