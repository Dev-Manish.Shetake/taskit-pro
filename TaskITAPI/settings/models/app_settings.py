from django.db import models
from accounts.models import User
from django.contrib.auth.models import Group


class SystemConfiguration(models.Model) :
    group = models.ForeignKey(Group,on_delete=models.DO_NOTHING, null=True, blank=True)
    module = models.CharField(max_length=50)
    display_name = models.CharField(max_length=100)
    code_name = models.CharField(max_length=100)
    field_value = models.CharField(max_length=200,null=True,blank=True)
    data_type = models.CharField(max_length=20)
    data_length = models.DecimalField(max_digits=9,decimal_places=2)
    data_control = models.CharField(max_length=20)
    data_option = models.CharField(max_length=200)
    is_system_config = models.BooleanField(default=False)

    class Meta :
        db_table = 'sys_config'
        default_permissions = ()
