from django.db import models
from softdelete.models import SoftDeleteModel
from django.conf import settings
USER = settings.AUTH_USER_MODEL


class ExceptionLog(SoftDeleteModel):
    user_id = models.IntegerField(null=True)
    request_method = models.CharField(max_length=10)
    request_path = models.CharField(max_length=500)
    traceback = models.TextField(null=True)
    remote_address = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True, blank=True)
    exception = models.TextField()

    class Meta:
        verbose_name = 'Exception Log'
        verbose_name_plural = 'Exception Logs'
        db_table = 'sys_exception_log'

    def __str__(self):
        return self.request_path



class ErrorCodeLog(models.Model) :
    code_name = models.CharField(max_length=100)
    message = models.CharField(max_length=500)
    source = models.CharField(max_length=10)
    api_path = models.CharField(max_length=200, null=True)
    created_date = models.DateTimeField(auto_now_add=True)

    class Meta :
        db_table = 'error_code_log'