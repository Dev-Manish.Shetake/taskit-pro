from rest_framework import serializers
from settings.models import ErrorCodeLog



class ErrorCodeLogSerializer(serializers.ModelSerializer) :

    class Meta :
        model = ErrorCodeLog
        fields = [
            'id',
            'code_name',
            'message',
            'source',
            'api_path',
            'created_date'
        ]
        read_only_fields = [
            'created_date'
        ]