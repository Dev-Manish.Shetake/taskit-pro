from rest_framework import serializers
from notifications.models import EmailGateway,SMSGateway
from django.utils import timezone
from accounts.models import Level
from settings.models import SystemConfiguration


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/open-sys-config/')
'''
class SystemConfigurationSerializer(serializers.ModelSerializer) :

    class Meta :
        model = SystemConfiguration
        fields = [
            'id',
            'group',
            'module',
            'display_name',
            'code_name',
            'field_value',
            'is_system_config'
        ]
    



'''
Serializer to send/receive data in specific format for GET/POST/PUT/PATCH method
(endpoint = '/settings/email' & '/settings/email:id')
'''
class EmailGatewaySerializer(serializers.ModelSerializer) :

    class Meta :
        model = EmailGateway
        fields = [
            'id',
            'host',
            'port',
            'user_name',
            'pass_word',
            'from_account',
            'ssl',
            'relay_count',
            'created_by',
            'created_user',
            'created_date'
        ]
        read_only_fields = [
            'created_by','created_user','created_date'
        ]

    def create(self,validated_data) :
        email_gateway = EmailGateway.objects.create_backoffice_user(
            **validated_data,
            created_by_id = self.context['request'].user,
            created_user = self.context['request'].user.user_name,
            created_date = timezone.now()
        )        
        return email_gateway

    def update(self,email_gateway,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(email_gateway, attr, value)
            update_fields.append(attr)
        email_gateway.modified_by = self.context['request'].user
        email_gateway.modified_user = self.context['request'].user.user_name
        email_gateway.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        email_gateway.save(update_fields=update_fields)       
        return email_gateway        


'''
Serializer to send/receive data in specific format for GET/POST/PUT/PATCH method
(endpoint = '/settings/sms' & '/settings/sms:id')
'''
class SMSGatewaySerializer(serializers.ModelSerializer) :

    class Meta :
        model = SMSGateway
        fields = [
            'id',
            'sms_url',
            'user_name',
            'pass_word',
            'relay_count',            
            'created_by',
            'created_user',
            'created_date'
        ]
        read_only_fields = [
            'created_by','created_user','created_date'
        ]

    def create(self,validated_data) :
        sms_gateway = SMSGateway.objects.create_backoffice_user(
            **validated_data,
            created_by_id = self.context['request'].user,
            created_user = self.context['request'].user.user_name,
            created_date = timezone.now()
        )        
        return sms_gateway

    def update(self,sms_gateway,validated_data) :
        update_fields = []
        for attr, value in validated_data.items():
            setattr(sms_gateway, attr, value)
            update_fields.append(attr)
        sms_gateway.modified_by = self.context['request'].user
        sms_gateway.modified_user = self.context['request'].user.user_name
        sms_gateway.modified_date = timezone.now()
        update_fields = update_fields + ['modified_by','modified_user','modified_date']
        sms_gateway.save(update_fields=update_fields)       
        return sms_gateway        



 
'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = 'settings/service-level')
'''

class ServiceLevelSerializer(serializers.ModelSerializer) :    

    def setup_eager_loading(self, queryset) :       
        return queryset

    class Meta :
        model = Level
        fields = [
            'id',
            'seq_no',
            'level_name',
            'range_from',
            'range_to',
            'no_of_orders',
            'created_by_id',           
            'created_user',
            'created_date'
        ]
        read_only_fields = ['created_by_id', 'created_user','created_date','is_active']
       