from django.urls import path, include
from settings.views import error_code_log,settings

urlpatterns = [
    path('open-sys-config/', settings.SystemConfigurationListView.as_view()),
    path('error-code-logs/', error_code_log.ErrorCodeLogView.as_view()),
    path('email/', settings.EmailGatewayListView.as_view()),
    path('email/<int:pk>', settings.EmailGatewayDetailsView.as_view()),
    path('sms/', settings.SMSGatewayListView.as_view()),
    path('sms/<int:pk>', settings.SMSGatewayDetailsView.as_view()),
    path('service-level', settings.ServiceLevelListView.as_view()),
    path('service-level-update', settings.ServiceLevelUpdateView.as_view()),
]

