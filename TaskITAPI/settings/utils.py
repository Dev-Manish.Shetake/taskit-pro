from settings.models import SystemConfiguration
from TaskITAPI.resources.cache import USER_SYS_CONFIGURATIONS, USER_SYS_CONFIGURATIONS_TIMEOUT, \
    SYS_CONFIGURATIONS, SYS_CONFIGURATIONS_TIMEOUT
from django.core.cache import cache




# Get default system configuration (user preferred)
def get_user_system_configuration() :
    # configs = cache.get(USER_SYS_CONFIGURATIONS)
    # if configs == None :
    # configurations = SystemConfiguration.objects.filter(is_system_config=True) 
    configurations = SystemConfiguration.objects.all()
    configs = {}
    for config in configurations :
        configs.update({
            config.code_name : config.field_value
        })
        # SysConfigurationItemSerializer(config).data)
    # cache.set(USER_SYS_CONFIGURATIONS, configs, timeout=USER_SYS_CONFIGURATIONS_TIMEOUT) 
    return configs



# Get default system configuration (System defined)
def get_system_configuration() :
    # configs = cache.get(SYS_CONFIGURATIONS)
    # if configs == None :
    # configurations = SystemConfiguration.objects.filter(is_system_config=True) 
    configurations = SystemConfiguration.objects.all()
    configs = {}
    for config in configurations :
        configs.update({
            config.code_name : config.field_value
        })
        # SysConfigurationItemSerializer(config).data)
    # cache.set(SYS_CONFIGURATIONS, configs, timeout=SYS_CONFIGURATIONS_TIMEOUT) 
    return configs