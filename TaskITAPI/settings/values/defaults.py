labour_movement_switch = {
    'ON' : 1,
    'OFF' : 2
}

gen_contractor_rating_switch = {
    'ON' : 1,
    'OFF' : 2
}

lab_contractor_rating_switch = {
    'ON' : 1,
    'OFF' : 2
}

kharchi_initiation_method = {
    'deployment_date' : 1,
    'last_date_of_month' : 2
}

sms_cc_switch = {
    'ON' : 1,
    'OFF' : 2
}

multiple_login_switch = {
    'ON' : 1,
    'OFF' : 2
}
