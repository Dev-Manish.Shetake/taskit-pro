from rest_framework.generics import ListCreateAPIView
from settings.serializers.error_code_log import ErrorCodeLogSerializer
from settings.models import ErrorCodeLog

class ErrorCodeLogView(ListCreateAPIView) :
    """
    Get + Add error code log 
    
    Get + Add error code log
    """
    serializer_class = ErrorCodeLogSerializer
    queryset = ErrorCodeLog.objects.all()
