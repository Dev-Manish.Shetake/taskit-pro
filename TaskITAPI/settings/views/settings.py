from rest_framework.generics import GenericAPIView,ListCreateAPIView,RetrieveUpdateAPIView,ListAPIView
from rest_framework import serializers
from settings.serializers import EmailGatewaySerializer,SMSGatewaySerializer,ServiceLevelSerializer,SystemConfigurationSerializer
from notifications.models import EmailGateway,SMSGateway
from settings.models import SystemConfiguration
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from accounts.models import Level

class LevelSerializer(serializers.Serializer) :
    seq_no = serializers.IntegerField()
    level_name = serializers.CharField(max_length=100)
    range_from = serializers.IntegerField()
    range_to = serializers.IntegerField()
    no_of_orders = serializers.IntegerField()


'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/open-sys-config/')
'''
class SystemConfigurationListView(ListAPIView) :
    """
   
    """
    serializer_class = SystemConfigurationSerializer
    permission_classes = []
    authentication_classes = []

    def get_queryset(self) :
        queryset = SystemConfiguration.objects.all()        
        return queryset

'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/settings/email')
'''
class EmailGatewayListView(ListCreateAPIView) :
    """
   
    """
    serializer_class = EmailGatewaySerializer

    def get_queryset(self) :
        queryset = EmailGateway.objects.all()        
        return queryset

'''
Serializer to send/receive data in specific format for GET/PUT/PATCH method
(endpoint = '/settings/email:id')
'''
class EmailGatewayDetailsView(RetrieveUpdateAPIView) :
    queryset = EmailGateway.objects.all()
    serializer_class = EmailGatewaySerializer

    def get_queryset(self) :
        queryset = EmailGateway.objects.all()         
        return queryset

    def delete(self,request,*args,**kwargs) :
        email_gateway = self.get_object()
        email_gateway.deleted_by = request.user
        email_gateway.deleted_user = request.user.user_name
        email_gateway.deleted_date = timezone.now()
        email_gateway.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



'''
Serializer to send/receive data in specific format for GET/POST method
(endpoint = '/settings/sms')
'''
class SMSGatewayListView(ListCreateAPIView) :
    """
   
    """
    serializer_class = SMSGatewaySerializer

    def get_queryset(self) :
        queryset = SMSGateway.objects.all()        
        return queryset

'''
Serializer to send/receive data in specific format for GET/PUT/PATCH method
(endpoint = '/settings/sms:id')
'''
class SMSGatewayDetailsView(RetrieveUpdateAPIView) :
    queryset = SMSGateway.objects.all()
    serializer_class = SMSGatewaySerializer

    def get_queryset(self) :
        queryset = SMSGateway.objects.all()         
        return queryset

    def delete(self,request,*args,**kwargs) :
        sms_gateway = self.get_object()
        sms_gateway.deleted_by = request.user
        sms_gateway.deleted_user = request.user.user_name
        sms_gateway.deleted_date = timezone.now()
        sms_gateway.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)



'''
Serializer to send/receive data in specific format for GET/PUT/PATCH method
(endpoint = '/settings/service-level')
'''
class ServiceLevelListView(ListAPIView) :
    """
    Get + Add service level details.     
   
    """
    serializer_class = ServiceLevelSerializer

    def get_queryset(self) :
        is_active = self.request.GET.get('is_active')
        if is_active == 'false':
            queryset = Level.all_objects.filter(is_active=False)
        else :
            queryset = Level.objects.all()       
        queryset = self.get_serializer_class().setup_eager_loading(self, queryset)
        return queryset


'''
Serializer to send/receive data in specific format for GET/PUT/PATCH method
(endpoint = '/settings/service-level-update')
'''
class ServiceLevelUpdateView(GenericAPIView):
    """
    Note: Pass value as buyer/seller in switch_user_in field
    """
    class ServiceLevelupdateSerializer(serializers.Serializer) :
        level = LevelSerializer(many=True)

    serializer_class = ServiceLevelupdateSerializer

    def patch(self,request) :
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        if 'level' in validated_data :
            service_level = validated_data.pop('level')
        level = Level.objects.all()      
        updated_rows = []
        for lev in level :
            for serv in service_level :  
                if lev.seq_no == serv['seq_no'] :
                    lev.range_from = serv['range_from']
                    lev.range_to = serv['range_to']
                    lev.no_of_orders = serv['no_of_orders']    
                    lev.modified_by_id = 1
                    lev.modified_user = self.request.user.user_name
                    lev.modified_date = timezone.now()
                    updated_rows.append(lev) 
        Level.objects.bulk_update(updated_rows, ['range_from','range_to','no_of_orders','modified_by_id','modified_user','modified_date'])            
        return Response(status=status.HTTP_200_OK)   

    