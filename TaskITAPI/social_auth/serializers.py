from rest_framework import serializers
from . import google
from rest_framework.exceptions import AuthenticationFailed
from accounts.models import User,UserSocialAccount
import os
from django.contrib.auth import authenticate
from accounts.values import USER_TYPE
from django.utils import timezone
from jwtauth.utils import get_response_payload
from django.contrib.auth.models import Group
from TaskITAPI.settings import DEFAULT_PASSWORD
from configs.values import SOCIAL_MEDIA
from django.db import transaction
from accounts.utils import create_user_gen_ledger
 

class SocialAuthSerializer(serializers.Serializer):
    email_address = serializers.EmailField()
    user_name = serializers.CharField(max_length=200)
    provider = serializers.CharField(max_length=10)

    def update_social_account(self,user,social_media_id,social_account_id) :
        if not UserSocialAccount.objects.filter(user=user,social_account_id=social_account_id,social_media_id=social_media_id).exists() :
            UserSocialAccount.objects.create(
                user = user,
                social_media_id = social_media_id,
                social_account_id = social_account_id,
                created_by = user,
                created_user = user.user_name,
                created_date = timezone.now()
            )
    
    def register_social_user(self,validated_data):
        filtered_user_by_email_address = User.objects.filter(email_address=validated_data['email_address'])
        if validated_data['provider'].lower() == 'google' :
                social_media_id = SOCIAL_MEDIA['google']
        elif validated_data['provider'].lower() == 'facebook' :
                social_media_id = SOCIAL_MEDIA['facebook']
        elif validated_data['provider'].lower() == 'linkdin':
                social_media_id = SOCIAL_MEDIA['linkdin']

        if filtered_user_by_email_address.exists():
            user = filtered_user_by_email_address[0]
            if user.auth_provider != validated_data['provider'] :
                self.update_social_account(user,social_media_id,validated_data['email_address'])
            user = filtered_user_by_email_address[0]
            user.auth_provider = validated_data['provider']
            user.save()
            self.user = user
        else:
            user = {
                'user_name':  validated_data['user_name'],
                'email_address':  validated_data['email_address'],
                'password': DEFAULT_PASSWORD,
                'user_type_id' : USER_TYPE['buyer'],
                'lang_id' : '1',
                'is_staff' : True,
                'created_by_id' : '1',
                'created_user' : 'System Admin',
                'created_date' : timezone.now()
            }
            user = User.objects.create_user(**user)
            user.is_verified_email_address = True
            user.auth_provider =  validated_data['provider']
            user.save()
            create_user_gen_ledger(self,user)
            group = Group.objects.get(id=2)
            user.groups.add(group)
            user._groups = [group]
            self.update_social_account(user,social_media_id,validated_data['email_address'])
            self.user = authenticate(
                email_address=validated_data['email_address'], password=DEFAULT_PASSWORD)
            return self.user

    # @transaction.atomic
    def validate(self, validated_data):
        self.register_social_user(validated_data)  
        data = get_response_payload(self.user)  
        return data