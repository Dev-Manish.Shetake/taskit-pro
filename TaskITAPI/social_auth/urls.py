from django.urls import path
from .views import SocialAuthView

urlpatterns = [
    path('login', SocialAuthView.as_view()),
]