from django.shortcuts import render
from rest_framework.generics import GenericAPIView,CreateAPIView
from social_auth.serializers import SocialAuthSerializer
from rest_framework.response import Response
from rest_framework import status
 

class TokenBaseSocialAuthView(GenericAPIView):
    authentication_classes = []
    permission_classes = []
    serializer_class = None

    www_authenticate_realm = 'api'

    def get_authenticate_header(self, request):
        return '{0} realm="{1}"'.format(
            AUTH_HEADER_TYPES[0],
            self.www_authenticate_realm,
        )
        
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.validated_data, status=status.HTTP_200_OK)


class SocialAuthView(TokenBaseSocialAuthView):
    serializer_class = SocialAuthSerializer 
