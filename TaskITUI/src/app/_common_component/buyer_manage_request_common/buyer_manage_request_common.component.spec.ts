/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Buyer_manage_request_commonComponent } from './buyer_manage_request_common.component';

describe('Buyer_manage_request_commonComponent', () => {
  let component: Buyer_manage_request_commonComponent;
  let fixture: ComponentFixture<Buyer_manage_request_commonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Buyer_manage_request_commonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Buyer_manage_request_commonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
