import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State, process, toODataString } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

declare var toastr: any;
declare var jQuery: any;

@Component({
  selector: 'app-buyer_manage_request_common',
  templateUrl: './buyer_manage_request_common.component.html',
  styleUrls: ['./buyer_manage_request_common.component.css']
})
export class Buyer_manage_request_commonComponent implements OnInit {
  @Input() requestStatusId: any;
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip: 0,
    take: 10
  }
  public pageNumber: any;
  public gridHeight: any;
  public buyer_requestList: any;
  public buyerRequest_gridData: GridDataResult | undefined;
  //public requestStatusId:number;
  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.getBuyer_request(this.requestStatusId);
  }

  //To populate Total Orders in Kendo Grid
  getBuyer_request(requestStatusId) {
    this.progress.show();
    var url = "";
    this.requestStatusId = requestStatusId;
    if (requestStatusId > 0) {
      url = "requests/list?status_id=" + requestStatusId + "&page=1&page_size=" + this.pageSize;
    }
    else {
      url = "requests/list?page=1&page_size=" + this.pageSize;
    }
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.buyer_requestList = data.results;
        if (data.results.length > 5) {
          this.gridHeight = "auto";
        }
        else {
          this.gridHeight = "270";
        }
        this.loadbuyer_request(data.count);
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }
  openSeller_Offer(request_id: any) {
    let passQueryParam = { request_id: request_id };
    sessionStorage.setItem("queryParamsRequest", JSON.stringify(passQueryParam));
    this.router.navigate(['/seller_offer'], {});
  }

  private loadbuyer_request(recordCount: number): void {
    if (this.buyer_requestList.length > 0) {
      this.buyerRequest_gridData = {
        data: orderBy(this.buyer_requestList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.buyerRequest_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortbuyer_request(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadbuyer_request(this.totalRecordCount);
  }


  public buyer_requestDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.buyerRequest_gridData = process(this.buyer_requestList, this.state);
  }

  //Page Change
  pageChange(event: PageChangeEvent): void {
    this.progress.show();
    var url = "";
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    if (this.requestStatusId > 0) {
      url = "requests/list?status_id=" + this.requestStatusId + "&page=1&page_size=" + this.pageSize;
    }
    else {
      //url = "requests/list?page=1&page_size=" + this.pageSize;
      url = "requests/list?page=" + this.pageNumber + "&page_size=" + this.pageSize
    }

    // url = "orders/?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    // if (this.searchSeller != null && this.searchSeller != undefined && this.searchSeller != '') {
    //   url = url + '&seller_name=' + this.searchSeller
    // }
    // if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
    //   url = url + '&buyer_name=' + this.searchBuyer
    // }
    // if (this.getStatus.id != null && this.getStatus.id != undefined && this.getStatus.id != 0) {
    //   url = url + '&order_status_id=' + this.getStatus.id
    // }
    // if (this.searchOrderdate != null && this.searchOrderdate != undefined && this.searchOrderdate != '') {
    //   url = url + '&order_date=' + this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    // }
    //var url = "orders/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.buyer_requestList = data.results;
        this.loadbuyer_request(data.count);
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  public request_id: number;
  onDeleteRequest(request_id: number) {
    this.request_id = request_id;
    jQuery('#delete_record').modal({ backdrop: 'static', keyboard: false });
  }


  onDeletePopBtn() {
    this.progress.show();
    this.httpServices.request("delete", "requests/" + this.request_id, "", "", null).subscribe((data) => {
      toastr.success("Request Deleted Successfully");
      jQuery('#delete_record').modal('hide');
      this.getBuyer_request(this.requestStatusId)
      this.progress.hide();
    }, error => {
      console.log(error);
      jQuery('#delete_record').modal('hide');
      this.progress.hide();
    });
  }

  onTaskIT_Offersbtn(category_id, subCategory_id, delivery_time_value, budget) {
    let passQueryParam = {
      category_id: category_id,
      subCategory_id: subCategory_id,
      delivery_time_value: delivery_time_value,
      budget: budget
    };
    sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gig_offers'], {});
  }

  editClick(request_id: any, request_status_id: any) {
    if (request_status_id == 3) {
      let passQueryParam = { request_id: request_id };
      sessionStorage.setItem("post_request_queryParams", JSON.stringify(passQueryParam));
      this.router.navigate(['/post_a_request'], {});
    }
  }
}
