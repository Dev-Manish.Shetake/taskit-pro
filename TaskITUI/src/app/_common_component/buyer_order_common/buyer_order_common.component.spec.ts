/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Buyer_order_commonComponent } from './buyer_order_common.component';

describe('Buyer_order_commonComponent', () => {
  let component: Buyer_order_commonComponent;
  let fixture: ComponentFixture<Buyer_order_commonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Buyer_order_commonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Buyer_order_commonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
