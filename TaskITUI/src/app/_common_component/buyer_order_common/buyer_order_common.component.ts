import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { GridDataResult, PageChangeEvent, GridComponent, DataStateChangeEvent, CellClickEvent, SelectableSettings, SelectAllCheckboxState } from '@progress/kendo-angular-grid';
import { DataSourceRequestState, DataResult, SortDescriptor, orderBy, State, process, filterBy, FilterDescriptor, CompositeFilterDescriptor, } from '@progress/kendo-data-query';
import { getLocaleDateFormat } from '@angular/common';
import { Router } from '@angular/router';
import { EncryptDecryptService } from '../../_common_services/encrypt-decrypt.service';


@Component({
  selector: 'app-buyer_order_common',
  templateUrl: './buyer_order_common.component.html',
  styleUrls: ['./buyer_order_common.component.scss']
})
export class Buyer_order_commonComponent implements OnInit {

  @Input() orderStatusId: any;
  // @Input() searchContainer:any;
  @Output() refreshCount: any = new EventEmitter<any>();
  // public orderStatusId:any;
  public gridHeight:any;
  public result: any;
  public pageNumber: any;
  public griddata: GridDataResult | undefined;
  public state: State = {
    skip: 0,
    take: 10,
    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public skip = 0;
  public pageSize = 10;
  public sort: SortDescriptor[] = [];
  public totalRecordCount: any;
  public order_Search: any;
  public userAuthInfo: any;

  constructor(private httpServices: HttpRequestService, private encrypt_decrypt: EncryptDecryptService, private progress: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.getResult(this.orderStatusId);
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
  }
  // ===========================================
  // get data 
  // ==========================================
  getResult(orderStatusId: any) {
    let url = "";
    this.orderStatusId = orderStatusId;
    if (orderStatusId > 0) {
      url = "orders/?order_status_id=" + orderStatusId + "&page=1&page_size=" + this.pageSize;
    }
    else {
      url = "orders/?page=1&page_size=" + this.pageSize;
    }
    this.progress.show();
    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      this.progress.hide();
      this.result = data.results;
      if (data.results.length > 5) {
        this.gridHeight = "auto";
      }
      else {
        this.gridHeight = "270";
      }
      this.totalRecordCount = data.count;
      this.loadData(data.count);
      this.manipulateStatus();
      this.refreshCount.emit({ orderStatusID: orderStatusId, count: data.count })
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // ====================================
  // Manipulate status 
  // ===================================
  manipulateStatus() {
    if (this.result.length > 0) {
      for (let i = 0; i < this.result.length; i++) {
        if (this.result[i].order_status.id == 2) {
          this.result[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.result[i].order_status.id == 6) {
          this.result[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.result[i].order_status.id == 7) {
          this.result[i].property = "badge badge-primary custom_badge_css";
        }
        else if (this.result[i].order_status.id == 8) {
          this.result[i].property = "badge badge-danger custom_badge_css";
        }
        else if (this.result[i].order_status.id == 3) {
          this.result[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.result[i].order_status.id == 4) {
          this.result[i].property = "badge badge-warning custom_badge_css";
        }
        else if (this.result[i].order_status.id == 5) {
          this.result[i].property = "badge badge-success custom_badge_css";
        }
      }
    }
  }
  //==========================================================
  //  kendo grid bind
  //==========================================================
  private loadData(recordCount: number): void {
    if (this.result.length > 0) {
      this.griddata = {
        data: orderBy(this.result, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount //this.labourList.results.length
      };
    }
    else {
      this.griddata = undefined;
    }

  }
  //==========================================================
  //  kendo grid - sorting
  //==========================================================
  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadData(this.totalRecordCount);
  }
  //==========================================================
  //  kendo grid - data state change
  //==========================================================
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.griddata = process(this.result, this.state);
  }
  //==========================================================
  //  kendo grid - page change
  //==========================================================
  public pageChange(event: PageChangeEvent): void {
    this.progress.show();
    let url = "";
    if (this.orderStatusId > 0) {
      url = "orders/?order_status_id=" + this.orderStatusId + "&page=1&page_size=" + this.pageSize;
    }
    else {
      url = "orders/?page=1&page_size=" + this.pageSize;
    }

    this.httpServices.request("get", url, null, null, null).subscribe((data) => {
      this.result = data.results;
      this.totalRecordCount = data.count;
      this.loadData(this.totalRecordCount);
      this.manipulateStatus();
      this.progress.hide();
    });
  }

  // ============================
  // Seller Name to Seller Profile
  // ============================
  onSellerClick(sellerId) {
    // let passQueryParam = { userId: sellerId };
    // sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));

    //This code comes from an encrypted service file where we have declared buyer or seller ID and Mode 
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
  openOrderDetail(order_id: any) {
    let passQueryParam = { id: order_id };
    sessionStorage.setItem("order_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/order_details'], {});
  }
  // ============================
  // Search Button
  // ============================
  onSearchBtnClick() {
    this.progress.show;
    var url = "orders/?order_status_id=" + this.orderStatusId + "&page=1&page_size=" + this.pageSize;
    //var url = "orders/?page=1&page_size=" + this.pageSize;
    if (this.order_Search != null && this.order_Search != '' && this.order_Search != undefined) {
      url = url + "&search=" + this.order_Search;
    }

    this.httpServices.request("get", url, "", "", null).subscribe((data) => {
      this.result = data.results;
      this.totalRecordCount = data.count;
      this.loadData(this.totalRecordCount);
      this.manipulateStatus();
      this.progress.hide();
    });
  }
}
