import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { Socialusers } from './../../_models/socialusers'
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { EncryptDecryptService } from '../../_common_services/encrypt-decrypt.service';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import * as CryptoJS from 'crypto-js';
import { NgForm } from '@angular/forms';
import { PopupCloseEvent } from '@progress/kendo-angular-grid';
declare var jQuery: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  response;
  social_users = new Socialusers();
  public after_login: boolean = false;
  public userAuthInfo: any;
  public key = CryptoJS.enc.Utf8.parse('SOMERANDOMKEY123');
  public emailAddressForRegister: any;
  public userIdAfterRegister: any;
  public userName_addyourdetails: any;
  public password_addyourdetails: any;
  public confirmPassword_addyourdetails: any;
  public emailAddress_forgotPasswd: any;
  public otp_forgotPasswdOTP: any;
  public password_forgotPasswdOTP: any;
  public checkValidation_continue_register: boolean = false;
  public checkValidation_addyourdetails: boolean = false;
  public checkValidation_forgotPasswd: boolean = false;
  public checkValidation_verifyOTP: boolean = false;
  public userInfo: any = { currentUser: AuthInfo };
  public group_id: Number = 0;
  constructor(private router: Router, private progress: NgxSpinnerService, private httpServices: HttpRequestService,
    private encrypt_decrypt: EncryptDecryptService, private authService: SocialAuthService) { }

  //==========================================================
  //  page initialisation method
  //==========================================================
  ngOnInit() {

    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    if (this.userAuthInfo.currentUser.user != undefined) {
      this.group_id = this.userAuthInfo.currentUser.user.group_id;
    }
    if (this.userAuthInfo.currentUser.user == undefined || this.userAuthInfo.currentUser.project_code != this.encrypt_decrypt.projectCode) {
      this.router.navigate(['/start_page'], {});
      this.after_login = false;
      localStorage.removeItem("currentUser");
      localStorage.removeItem("userAccess");
      localStorage.removeItem("Token");
    }
    else {
      this.router.navigate(['/home'], {});
      this.after_login = true;
    }
  }

  //==========================================================
  //  Call social login POP (Google or Facebook)
  //==========================================================
  public socialSignIn(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.authService.signIn(socialPlatformProvider).then(social_users => {
      this.saves_response(social_users);
    });
  }

  //==========================================================
  //  Register or Get authenticate in system after social login verification
  //==========================================================
  saves_response(social_users: Socialusers) {
    console.log(social_users);
    this.progress.show();
    let login_info = { email_address: social_users.email, user_name: social_users.name, provider: social_users.provider };
    this.httpServices.request('post', 'social_auth/login', null, null, login_info).subscribe((data) => {
      if (data) {
        console.log(data);
        if (this.encrypt_decrypt.encryptionMode == "encrypt") {
          this.userAuthInfo.access = data.access;
          this.key = CryptoJS.enc.Utf8.parse(data.access.split('.')[1].substring(0, 16));
          data.project_code = this.encrypt_decrypt.projectCode;
          this.userAuthInfo.currentUser = this.encrypt(data, this.key);
        }
        else {
          data.project_code = this.encrypt_decrypt.projectCode;
          this.userAuthInfo.currentUser = data;
        }
        if (this.userAuthInfo.currentUser.user != undefined) {
          this.group_id = this.userAuthInfo.currentUser.user.group_id;
        }
        if (this.userAuthInfo.currentUser.user.group_id != 1) {
          this.router.navigate(['/home'], {});
        } else {
          this.router.navigate(['/admin_dashboard'], {});
        }

        this.after_login = true;
        jQuery('#SignIn').modal('hide');
      }
      else {
        jQuery('#SignIn').modal('hide');
      }
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }

  register_click() {
    //jQuery('#SignIn').modal({ backdrop: 'static', keyboard: false });
    jQuery('#SignIn').modal('hide');
    jQuery('#Verify_OTP_modal_pop_modal').modal('hide');
    jQuery('#after_register_modal_pop_modal').modal('hide');
    jQuery('#after_register_modal_pop_modal').modal('hide');
    jQuery('#Register').modal({});
  }

  signIN_click() {
    this.checkvalidation = false;
    jQuery('#SignIn').modal({});
    document.getElementById('Forgot_Password_div').style.display = 'none';
    document.getElementById('SignIn_div').style.display = 'block';
    jQuery('#Register').modal('hide');
  }
  // =======================================================
  // On Click of forgot password link of 'Sign in to TASKIT' popup 
  // =======================================================

  forgot_password_click() {
    jQuery('#SignIn').modal({});
    document.getElementById('Forgot_Password_div').style.display = 'block';
    document.getElementById('SignIn_div').style.display = 'none';
    jQuery('#Register').modal('hide');
  }
  // ===============================================
  // On Click of Get OTP on 'Forgot pAsswd' popup 
  // ===============================================
  getOTP_ForgotPasswd() {

    this.progress.show();
    this.checkValidation_forgotPasswd = true;
    if (this.emailAddress_forgotPasswd != null && this.emailAddress_forgotPasswd != undefined && this.emailAddress_forgotPasswd != '') {
      let login_info = { email_address: this.emailAddress_forgotPasswd }
      this.httpServices.request('post', 'password/forgot-generate-otp', null, null, login_info).subscribe((data) => {
        jQuery('#Verify_OTP_modal_pop_modal').modal({});
        this.progress.hide();
        this.checkValidation_forgotPasswd = false;
      }, (error) => {
        this.progress.hide();
        this.checkValidation_forgotPasswd = false;
      });
    }
    else {
      this.progress.hide();
    }
  }
  // ===============================================
  // On Click of verify OTP on 'verify OTP' popup 
  // ===============================================
  verifyOTP_forgotPasswd() {
    this.progress.show();
    this.checkValidation_verifyOTP = true;
    if (this.otp_forgotPasswdOTP != null && this.otp_forgotPasswdOTP != undefined && this.otp_forgotPasswdOTP != ''
      && this.password_forgotPasswdOTP != null && this.password_forgotPasswdOTP != undefined && this.password_forgotPasswdOTP != '') {
      let login_info = { otp: this.otp_forgotPasswdOTP, password: this.password_forgotPasswdOTP }
      this.httpServices.request('post', 'password/forgot-verify-otp', null, null, login_info).subscribe((data) => {
        jQuery('#Verify_OTP_modal_pop_modal').modal('hide');
        this.progress.hide();
        this.checkValidation_verifyOTP = false;
      }, (error) => {
        this.progress.hide();
        this.checkValidation_verifyOTP = false;
      });
    }
    else {
      this.progress.hide();
    }
  }
  // ============================================
  // on Click of Register btn on Register popup 
  // ===========================================
  register_with_email_click() {
    this.progress.show();
    this.checkValidation_continue_register = true;
    if (this.emailAddressForRegister != null && this.emailAddressForRegister != undefined && this.emailAddressForRegister != '') {
      let login_info = { email_address: this.emailAddressForRegister }
      this.httpServices.request('post', 'auth/sign-up', null, null, login_info).subscribe((data) => {

        jQuery('#after_register_modal_pop_modal').modal({});
        jQuery('#Register').modal('hide');
        this.progress.hide();
        this.checkValidation_continue_register = false;
      }, (error) => {
        this.checkValidation_continue_register = false;
        this.progress.hide();
      });
    }
    else {
      this.progress.hide();
    }
  }
  // ========================================
  // On Submit click of Add your details 
  // ======================================
  onSubmit_addyourdetails() {
    this.progress.show();
    this.checkValidation_addyourdetails = true;
    if (this.userName_addyourdetails != null && this.userName_addyourdetails != undefined && this.userName_addyourdetails != ''
      && this.password_addyourdetails != null && this.password_addyourdetails != undefined && this.password_addyourdetails != ''
      && this.confirmPassword_addyourdetails != null && this.confirmPassword_addyourdetails != undefined && this.confirmPassword_addyourdetails != '') {

      if (this.password_addyourdetails == this.confirmPassword_addyourdetails) {
        let login_info = { user_name: this.userName_addyourdetails, password: this.confirmPassword_addyourdetails }
        this.httpServices.request('patch', 'auth/sign-up-register/' + this.userIdAfterRegister, null, null, login_info).subscribe((data) => {
          jQuery('#after_register_modal_pop_modal').modal('hide');
          this.progress.hide();
          this.checkValidation_addyourdetails = false;
        }, (error) => {
          this.progress.hide();
          this.checkValidation_addyourdetails = false;
        });
      }
      else {
        this.progress.hide();
      }
    }
    else {
      this.progress.hide();
    }

  }

  public checkvalidation: boolean = false;

  // =======================================================
  // On Click of continue btn of 'Sign in to TASKIT' popup 
  // =======================================================
  login(user: any, form: NgForm) {
    this.checkvalidation = true;

    let login_info = { email_address: user.email_address, password: user.password };
    if (form.valid) {
      this.progress.show();
      this.httpServices.request('post', 'auth/login', null, null, login_info).subscribe((data) => {
        if (data) {
          console.log(data);
          if (this.encrypt_decrypt.encryptionMode == "encrypt") {
            this.userAuthInfo.access = data.access;
            this.key = CryptoJS.enc.Utf8.parse(data.access.split('.')[1].substring(0, 16));
            data.project_code = this.encrypt_decrypt.projectCode;
            this.userAuthInfo.currentUser = this.encrypt(data, this.key);
          }
          else {
            data.project_code = this.encrypt_decrypt.projectCode;
            this.userAuthInfo.currentUser = data;
          }
          if (this.userAuthInfo.currentUser.user != undefined) {
            this.group_id = this.userAuthInfo.currentUser.user.group_id;
          }
          if (this.userAuthInfo.currentUser.user.group_id != 1) {
            this.router.navigate(['/home'], {});
          } else {
            this.router.navigate(['/admin_dashboard'], {});
          }

          this.after_login = true;
          jQuery('#SignIn').modal('hide');
        }
        else {
          jQuery('#SignIn').modal('hide');
        }
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
  }
  // =====================================
  // On user menu click 
  // ======================================
  onUserMenuClick(menuName: string) {
    if (menuName == 'become_a_seller') {
      this.router.navigate(['/become_seller'], {});
    }
    else if (menuName == 'log_out') {
      this.progress.show();
      setTimeout(() => { }, 600);
      let user: any = { user_id: this.userInfo.user.id }
      this.httpServices.request("post", "auth/log-out", null, null, user).subscribe((data) => {
        this.router.navigate(['/logout'], {});
        this.after_login = false;
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
    else if (menuName == 'my_profile') {
      this.router.navigate(['/new_gig'], {});
    }
    else if(menuName=='orders')
    {
      //this.router.navigate(['/seller_orders'], {});
      this.router.navigate(['/buyer_orders'], {});
    }
  }
  encrypt(msgString, key) {
    // msgString is expected to be Utf8 encoded
    var iv = CryptoJS.lib.WordArray.random(16);
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify(msgString), key, {
      iv: iv
    });
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
  }

  decrypt(ciphertextStr, key) {
    var ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);

    // split IV and ciphertext
    var iv = ciphertext.clone();
    iv.sigBytes = 16;
    iv.clamp();
    ciphertext.words.splice(0, 4); // delete 4 words = 16 bytes
    ciphertext.sigBytes -= 16;

    // decryption
    var decrypted = CryptoJS.AES.decrypt({ ciphertext: ciphertext }, key, {
      iv: iv
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

}
