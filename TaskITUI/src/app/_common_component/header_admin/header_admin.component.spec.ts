/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Header_adminComponent } from './header_admin.component';

describe('Header_adminComponent', () => {
  let component: Header_adminComponent;
  let fixture: ComponentFixture<Header_adminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Header_adminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Header_adminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
