import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { HttpRequestService } from '../../_common_services/httprequest.service';
import { AutoCompleteModel, LookupModel } from '../../_models/auto_complete';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { AutoCompleteModule } from '@progress/kendo-angular-dropdowns';
import { toODataString } from '@progress/kendo-data-query';


declare var $: any;
declare var toastr: any;

@Component({
  selector: 'app-multi_autocomplete',
  templateUrl: './multi_autocomplete.component.html',
  styleUrls: ['./multi_autocomplete.component.scss']
})
export class Multi_autocompleteComponent implements OnInit {
  @Input() autocompleteFor: string = "";
  @Input() getMethodName: string;
  @Input() placeHolder: string = "Enter Name";
  @Input() selctedValue: any[] = [];
  @Input() attrId: string = 'txtautoComplete';
  @Input() textField: string;
  @Input() valueField: string;
  @Input() fieldFrom: string;
  @Output() onAutoCompleteChange: any = new EventEmitter<any[]>();
  //@Input() autocompleteBlank:boolean=false;
  @Input()
  set autocompleteBlank(refresh: boolean) {
    if (refresh) {
      this.autocompeleteText = '';
    }
  }
  constructor(private httpServices: HttpRequestService) { }
  searchTerm: FormControl = new FormControl();
  ngOnInit() {


    this.searchTerm.valueChanges.subscribe(
      (term: string) => {
        if (term.length >= 1) {
          this.searchAuto(term).subscribe(
            data => {
              if (data) {
                this.autoSearchList = data; //[{code: '1', description:"mumbai"},{code: '2', description:"pune"}] //data;
              }
            })
        }
        else {
          //this.autoSearchList = [<AutoCompleteModel>{ code: "", description: "", colName: "" }];
          this.autoSearchList = [];
          this.flag = false;
        }
      })
    if (this.selctedValue != null) {
      this.selctedValue.forEach(a => {
        this.autoSelectedItem.push(a);
      });
    }
  }
  onKey(event: any) {
    this.autocompleteBlank = true;
    this.kyTab.emit(event);
    document.getElementById('txtautoCompletegigSearch').focus();
  }
  @Input() autocompeleteText = '';
  @Output() autocompeleteTextChange = new EventEmitter<any>();
  @Input() attrDisabled: boolean = false;
  @Output() kyTab = new EventEmitter<any>();

  onBlur() {
    // this.autocompeleteText="";
    this.autoSelectedItem = [];
  }
  change(newValue: any) {

    this.autocompeleteTextChange.emit(newValue);
  }
  public flag: boolean = true;
  // @Output() textChange: any = new EventEmitter<object>();
  // Push a search term into the observable stream.  
  searchClient(term: string): void {
    this.flag = true;
    // this.searchTerms.next(term);
  }
  // onTextBlur(autocompeleteText:any)
  // {

  //   this.textChange.emit(autocompeleteText);
  // }
  onSelect(Obj: any) {

    //if (Obj.code != "") {
    if (Obj[this.valueField] != "") {
      this.autoSelectedItemfinal = [];
      if (this.fieldFrom == 'gig') {
        if (this.autoSelectedItem == null) {
          // this.autoSelectedItem = [{ code: "", description: "" }];
          this.autoSelectedItem = [];
        }
        //this.autoSelectedItem.push({ code: Obj.code, description: Obj.description });
        const found = this.autoSelectedItem.some(el => el.id === Obj.id || el.search_keyword.toLowerCase() === Obj.search_keyword.toLowerCase());
        if (!found) {
          this.autoSelectedItem.push(Obj);
        }
        else {
          toastr.error("Already selected.");
        }
        //;
        this.flag = false;
        this.autocompeleteText = "";

        //document.getElementById('txtautoComplete').focus();
        document.getElementById(this.attrId).focus();
      }
      else if (this.fieldFrom == 'activity' || this.fieldFrom == 'subactivity') {
        if (this.autoSelectedItem == null) {
          // this.autoSelectedItem = [{ code: "", description: "" }];
          this.autoSelectedItem = [];
        }

        //this.autoSelectedItem.push({ code: Obj.code, description: Obj.description });
        const found = this.autoSelectedItem.some(el => el.id === Obj.id);
        if (!found) {
          this.autoSelectedItem.push(Obj);
        }
        else {
          toastr.error("Already selected.");
        }
        this.autoSelectedItem = this.autoSelectedItem.filter(x => x[this.valueField] != '');
        this.flag = false;
        this.autocompeleteText = "";

        //document.getElementById('txtautoComplete').focus();
        document.getElementById(this.attrId).focus();
      }
      else {
        if (this.autoSelectedItem == null) {
          // this.autoSelectedItem = [{ code: "", description: "" }];
          this.autoSelectedItem = [];
        }

        //this.autoSelectedItem.push({ code: Obj.code, description: Obj.description });
        const found = this.autoSelectedItem.some(el => el.location_id === Obj.location_id);
        if (!found) {
          this.autoSelectedItem.push(Obj);
        }
        else {
          toastr.error("Already selected.");
        }
        //this.autoSelectedItem = this.autoSelectedItem.filter(x => x[this.valueField] != '');
        this.flag = false;
        this.autocompeleteText = "";

        //document.getElementById('txtautoComplete').focus();
        document.getElementById(this.attrId).focus();
      }
    }
    else {
      return false;
    }

    this.onAutoCompleteChange.emit(this.autoSelectedItem);
  }

  @Input() autoSelectedItem: any[] = [];

  autoSearchList: any[] = [];

  autoSelectedItemfinal: LookupModel[] = [];

  searchAuto(terms: string): Observable<any[]> {
    if (terms != null && terms != undefined && (terms.length > 0)) {
      this.flag = true;
      if (this.fieldFrom == 'subactivity') {
        return this.httpServices.request('getauto', this.getMethodName + "&" + this.textField + "=" + terms, '', '', null);
      }
      else {
        return this.httpServices.request('getauto', this.getMethodName + "?" + this.textField + "=" + terms, '', '', null);
      }
    }
    else {
      //return Observable.create({ code: "", description: "" })
      return Observable.create({});
    }
  }
  removeAutoItem(item: any) {

    if (!this.attrDisabled) {
      if (this.fieldFrom == 'gig') {
        this.autoSelectedItem = this.autoSelectedItem.filter(x => x[this.textField] != item[this.textField]);
      }
      else {
        this.autoSelectedItem = this.autoSelectedItem.filter(x => x[this.valueField] != item[this.valueField] && x[this.textField] != item[this.textField]);
      }


      this.onAutoCompleteChange.emit(this.autoSelectedItem);
    }
  }


  setautoSelectedItem(data: any) {
    this.autoSelectedItem = data;
  }
  onblur() {

    this.flag = false;
  }
}

/** look up model interface */
interface ILookUpModelList {
  list: LookupModel[];
}
