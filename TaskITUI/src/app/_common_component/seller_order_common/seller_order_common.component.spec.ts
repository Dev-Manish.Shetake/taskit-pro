/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Seller_order_commonComponent } from './seller_order_common.component';

describe('Seller_order_commonComponent', () => {
  let component: Seller_order_commonComponent;
  let fixture: ComponentFixture<Seller_order_commonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Seller_order_commonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Seller_order_commonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
