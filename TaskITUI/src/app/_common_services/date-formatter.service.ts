import { Injectable } from '@angular/core';
import { anyChanged } from '@progress/kendo-angular-common';

@Injectable({
  providedIn: 'root'
})
export class DateFormatterService {

constructor() { }
format(date: Date, format: any = 'yyyy-MM-dd') {
  let year:any; let month :any; let day:any;let hr:any;let mm:any;let ss:any;
  year = date.getFullYear();
  month = 1 + date.getMonth();
  day = date.getDate();
  month = month.toString().length == 1 ? "0" + month : month;
  day = day.toString().length == 1 ? "0" + day : day;
  hr=date.getHours();
  mm=date.getMinutes();
  ss=date.getSeconds();
  let formattedDate = "";
  const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
   ];
  if (format == 'dd/MM/yyyy') {
    formattedDate = day + '/' + month + '/' + year;
  }
  else if (format == 'yyyy/MM/dd') {
    formattedDate = year + '/' + month + '/' + day;
  }
  else if (format == 'yyyy-MM-dd') {
    formattedDate = year + '-' + month + '-' + day;
  }
  else if(format=='yyyy')
  {
    formattedDate=year;
  }
  else if (format == 'yyyy-MM-dd hr:mm:ss') {
    formattedDate = year + '-' + month + '-' + day+' '+hr+':'+mm+':'+ss;
  }
  else if (format == 'dd MMM yyyy hr:mm') {
   // let newDate=new Date(date);
    const d = date;
    month=date.getMonth();
    formattedDate = day + ' ' + monthNames[Number(month)] + ' ' + year+' '+hr+':'+mm;
  }
  return formattedDate;
}
}
