import { Injectable } from '@angular/core';
import { UserAuthInfoService } from '../_component/start_page/user-auth-info.service';
import * as CryptoJS from 'crypto-js';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EncryptDecryptService {
  public projectCode: string = '1q2w3e4r5t6y7u8i9o0ptaskitp0o9i8u7y6t5r4e3w2q1';
  public encryptionMode: string = 'decrypt'; //encrypt  decrypt
  buyer_profile_id: any;
  buyer_profile_mode: any;
  seller_profile_id: any;
  seller_profile_mode: any;

  // private data_buyer_profile = new BehaviorSubject(false);
  // data$ = this.data_buyer_profile.asObservable();

  private data_cart_login = new BehaviorSubject({is_login: false,groupId:0,servicePercent:0});
  data_cart_login$ = this.data_cart_login.asObservable();

  changeCart_groupId(data) {
    this.data_cart_login.next(data)
  }
  
  // changeSellerProfileData(data: boolean) {
  //   this.data_seller_profile.next(data)
  // }

  private data = new BehaviorSubject(false);
  data$ = this.data.asObservable();

  private favData = new BehaviorSubject({ flag: false, location: "", is_Favourite: false, gig_Data: null });
  favData$ = this.favData.asObservable();

  private tagFav = new BehaviorSubject({ is_login: false, is_Favourite: false, gig_Data: null });
  tagFav$ = this.tagFav.asObservable();

  private cartData = new BehaviorSubject({ flag: false });
  cartData$ = this.cartData.asObservable();

  private cartDataLogin = new BehaviorSubject({ is_login: false });
  cartDataLogin$ = this.cartDataLogin.asObservable();


  private switchSelling = new BehaviorSubject({ flag: false, location: "" })
  switchSelling$ = this.switchSelling.asObservable();

  private gigSearch = new BehaviorSubject({ flag: false, location: "" })
  gigSearch$ = this.gigSearch.asObservable();

  changeData(data: boolean) {
    this.data.next(data)
  }
  changeDataFavourite(data) {
    this.favData.next(data)
  }
  changeDataTagFav(data) {
    this.tagFav.next(data)
  }
  changeDataCart(data) {
    this.cartData.next(data)
  }
  changeDataTagCartLogin(data) {
    this.cartDataLogin.next(data)
  }

  changeDataSwitchSelling(data) {
    this.switchSelling.next(data)
  }
  changeDataGigSearch(data) {
    this.gigSearch.next(data)
  }

  public key: string;
  public userInfo: any = { access: Token, currentUser: AuthInfo };
  constructor(private userAuthInfo: UserAuthInfoService) { }
  decryptUserInfo() {
    if (this.encryptionMode == 'encrypt') {
      if (this.userAuthInfo.currentUser.toString() != '[object Object]') {
        let key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.toString().split('.')[1].substring(0, 16));
        let jesonData = this.decrypt(this.userAuthInfo.currentUser, key);
        let currentUser = JSON.parse(jesonData);
        this.userInfo.currentUser = currentUser;
        this.userInfo.access = this.userAuthInfo.access;
        return this.userInfo;
      }
      else {
        return this.userAuthInfo;
      }
    }
    else {
      return this.userAuthInfo;
    }
  }
  encryptUserInfo() {
    if (this.encryptionMode == 'encrypt') {
      if (this.userAuthInfo.currentUser.user != undefined) {
        let key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.toString().split('.')[1].substring(0, 16));
        this.userAuthInfo.currentUser = this.encrypt(this.userAuthInfo.currentUser, key);
      }
      else {
        this.userAuthInfo;
      }
    }
    else {
      this.userAuthInfo;
    }
  }

  encrypt(msgString, key) {
    // msgString is expected to be Utf8 encoded
    var iv = CryptoJS.lib.WordArray.random(16);
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify(msgString), key, {
      iv: iv
    });
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
  }

  decrypt(ciphertextStr, key) {
    var ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);

    // split IV and ciphertext
    var iv = ciphertext.clone();
    iv.sigBytes = 16;
    iv.clamp();
    ciphertext.words.splice(0, 4); // delete 4 words = 16 bytes
    ciphertext.sigBytes -= 16;

    // decryption
    var decrypted = CryptoJS.AES.decrypt({ ciphertext: ciphertext }, key, {
      iv: iv
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }
}
