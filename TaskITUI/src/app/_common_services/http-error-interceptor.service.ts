import { Injectable } from '@angular/core';
import {
	HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse,
	HttpErrorResponse
} from '@angular/common/http';
import { catchError, finalize, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpMessageService } from '../_common_services/http-message.service';
import { EncryptDecryptService } from '../_common_services/encrypt-decrypt.service';
import { HttpRequestService } from './httprequest.service';
import * as CryptoJS from 'crypto-js';
import { NgxSpinnerService } from "ngx-spinner";

declare var toastr: any;
declare var jQuery: any;

@Injectable()
export class HttpErrorInterceptorService implements HttpInterceptor {
	constructor(private httpMessage: HttpMessageService, private encrypt_decrypt: EncryptDecryptService,
		private httpServices: HttpRequestService, private progress: NgxSpinnerService) { }
	public showToastr: boolean = true;
	public userAuthInfo: any;
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
		request = request.clone({
			withCredentials: true
		});
		return next.handle(request)
			.pipe(
				map(event => {
					return event;
				}),
				catchError((errorResponse: HttpErrorResponse) => {
					let errMsg: string;
					this.getErrorResponse(errorResponse, errMsg);
					this.progress.hide();
					return throwError(errorResponse);

				}), finalize(() => {
				})
			);
	}

	//new method for appropriate tostar message
	getErrorResponse(errorResponse: HttpErrorResponse, errMsg: any) {
		if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
			let key;
			let error_response;
			this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
			if (this.userAuthInfo.currentUser.access == undefined) {
				key = CryptoJS.enc.Utf8.parse(this.encrypt_decrypt.key);
			} else {
				key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.currentUser.access.toString().split('.')[1].substring(0, 16));
			}

			if (errorResponse.error.indexOf("code") > 0 && errorResponse.error.indexOf("message") > 0) {
				error_response = JSON.parse(errorResponse.error);
			}
			else {
				let str: any;
				str = this.encrypt_decrypt.decrypt(errorResponse.error, key);
				error_response = JSON.parse(this.encrypt_decrypt.decrypt(errorResponse.error, key));
			}
			if (errorResponse.status == 401 || errorResponse.status == 440) {
				this.progress.hide();
				let errorMessage = "";
				let session_Expired = '';
				if (error_response.errors != undefined) {
					for (var i = 0; i < error_response.errors.length; i++) {
						if (error_response.errors[i].code.toLowerCase() == 'authentication_failed') {
							errorMessage = errorMessage + ("<li>" + error_response.errors[i].message + "</li>");
						}
						else if (this.httpMessage.errorMessage_EN[error_response.errors[i].code] == undefined
							&& error_response.errors.length > 0) {
							// let tempErrorMessage =
							// {
							// 	code_name: error_response.errors[i].code,
							// 	message: error_response.errors[i].message,
							// 	source: "web",
							// 	api_path: errorResponse.url
							// }
							// this.httpServices.request("post", "settings/error-code-logs/", null, null, tempErrorMessage).subscribe((data) => {
							// 	this.progress.hide();
							// }, (error) => {
							// 	this.progress.hide();
							// });
						}
						else {
							if (error_response.errors[i].code == "authentication_failed" //|| error_response.errors[i].code == "user_not_found"
								|| error_response.errors[i].code == "account_temporarily_locked") {
								this.showToastr = false;
							}
							else {
								this.showToastr = true;
							}
							errorMessage = errorMessage + ("<li>" + this.httpMessage.errorMessage_EN[error_response.errors[i].code] + "</li>");

						}
						if (error_response.errors[i].code == 'session_expired') {
							session_Expired = 'session_expired';
						}
					}
				}
				if (this.showToastr == true) {
					if (errorMessage != '')
						toastr.error(errorMessage);
				}
				if (errorResponse.statusText != 'Unauthorized' || session_Expired == 'session_expired') {
					localStorage.removeItem("currentUser");
					localStorage.removeItem("userAccess");
					localStorage.removeItem("Token");
					if (window.location.pathname != '/start_page')
						window.location.href = "start_page";
				}
			}
			else {
				this.progress.hide();
				let errorMessage = "";
				if (error_response.errors != undefined) {
					for (var i = 0; i < error_response.errors.length; i++) {
						if (this.httpMessage.errorMessage_EN[error_response.errors[i].code] == undefined
							&& error_response.errors.length > 0) {
							// let tempErrorMessage =
							// {
							// 	code_name: error_response.errors[i].code,
							// 	message: error_response.errors[i].message,
							// 	source: "web",
							// 	api_path: errorResponse.url
							// }
							// this.httpServices.request("post", "settings/error-code-logs/", null, null, tempErrorMessage).subscribe((data) => {
							// 	this.progress.hide();
							// }, (error) => {
							// 	this.progress.hide();
							// });
						}
						else {
							errorMessage = errorMessage + ("<li>" + this.httpMessage.errorMessage_EN[error_response.errors[i].code] + "</li>");
						}
					}
				}
				if (errorMessage == '') {
					toastr.error('System is unable to process your request. Please contact to Admin.');
				}
				else {
					if (errorMessage != '')
						toastr.error(errorMessage);
				}
			}
		}
		else {
			if (errorResponse.status == 401 || errorResponse.status == 440) {
				this.progress.hide();
				let errorMessage = "";
				let session_Expired = '';
				if (errorResponse.error.errors != undefined) {
					for (var i = 0; i < errorResponse.error.errors.length; i++) {
						if (errorResponse.error.errors[i].code.toLowerCase() == 'authentication_failed') {
							errorMessage = errorMessage + ("<li>" + errorResponse.error.errors[i].message + "</li>");
						}
						else if (this.httpMessage.errorMessage_EN[errorResponse.error.errors[i].code] == undefined
							&& errorResponse.error.errors.length > 0) {
							// let tempErrorMessage =
							// {
							// 	code_name: errorResponse.error.errors[i].code,
							// 	message: errorResponse.error.errors[i].message,
							// 	source: "web",
							// 	api_path: errorResponse.url
							// }
							// this.httpServices.request("post", "settings/error-code-logs/", null, null, tempErrorMessage).subscribe((data) => {
							// 	this.progress.hide();
							// }, (error) => {
							// 	this.progress.hide();
							// });
						}
						else {

							if (errorResponse.error.errors[i].code == "authentication_failed" //|| errorResponse.error.errors[i].code == "user_not_found"
								|| errorResponse.error.errors[i].code == "account_temporarily_locked") {
								this.showToastr = false;
							}
							else {
								this.showToastr = true;
							}
							errorMessage = errorMessage + ("<li>" + this.httpMessage.errorMessage_EN[errorResponse.error.errors[i].code] + "</li>");

						}
						if (errorResponse.error.errors[i].code == 'session_expired') {
							session_Expired = 'session_expired';
						}
					}
				}
				if (this.showToastr == true) {
					if (errorMessage != '')
						toastr.error(errorMessage);
				}

				if (errorResponse.statusText != 'Unauthorized' || session_Expired == 'session_expired') {
					localStorage.removeItem("currentUser");
					localStorage.removeItem("userAccess");
					localStorage.removeItem("Token");
					if (window.location.pathname != '/start_page')
						window.location.href = "start_page";
				}
			}
			else {
				let errorMessage = "";
				if (errorResponse.error.errors != undefined) {
					for (var i = 0; i < errorResponse.error.errors.length; i++) {
						if (this.httpMessage.errorMessage_EN[errorResponse.error.errors[i].code] == undefined
							&& errorResponse.error.errors.length > 0) {
							// let tempErrorMessage =
							// {
							// 	code_name: errorResponse.error.errors[i].code,
							// 	message: errorResponse.error.errors[i].message,
							// 	source: "web",
							// 	api_path: errorResponse.url
							// }
							// this.httpServices.request("post", "settings/error-code-logs/", null, null, tempErrorMessage).subscribe((data) => {
							// 	this.progress.hide();
							// }, (error) => {
							// 	this.progress.hide();
							// });
						}
						else {
							errorMessage = errorMessage + ("<li>" + this.httpMessage.errorMessage_EN[errorResponse.error.errors[i].code] + "</li>");

						}
					}
				}
				if (errorMessage == '') {
					toastr.error('System is unable to process your request. Please contact to Admin.');
				}
				else {
					if (errorMessage != '')
						toastr.error(errorMessage);
				}
			}
		}
	}
}
