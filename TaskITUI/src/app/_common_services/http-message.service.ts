import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class HttpMessageService {

	constructor() { }

	public errorMessage_EN: any = {
		group_id_not_exist: 'Role does not exist.',
		mobile_no_not_unique: 'This mobile number already exists.',
		email_address_not_unique: 'This email address already exists.',
		invalid_credentials: 'Invalid password.',
		password_changed: 'Password successfully changed.',
		user_no_verified: 'User account is not verified.',
		invalid_link: 'Invalid or link has been expired.',
		link_sent: 'Verification link is sent on registered email address.',
		account_verified: 'Account succesfully verified. ',
		user_verified: 'User is already verified.',
		not_exist: 'User does not exist.',
		password_expired: 'Password is expired, Please reset your password to log in.',
		session_expired: 'Session expired. Please log in again.',
		token_not_valid: 'Token is invalid or expired',
		not_found: 'User with given mobile number does not exist.',
		sign_out_succeed: 'Successfully logged out of the system.',
		user_name_required: 'User Name is required.',
		email_address_required: 'Email address is required.',
		email_address_invalid: 'Enter a valid email address.',
		group_id_required: 'Role is required.',
		user_not_found:'Email address or password not matched.',
		mobile_no_mobile_no_not_unique:'This mobile number already exists.',
		user_name_not_unique:'User name should be unique.',
		can_not_delete:'Metadata detail record is lined to other subcategories metadata.',
		gallery_not_added:'Please add atleast one media in gallery tab.',
		search_tags_not_added:'Please add atleast one search tag.',
		gig_price_not_added:'Please add gig price.',
		faq_not_added:'Please add atleast one FAQ',
		gig_cannot_delete:'Gig involved in active orders.',
		not_unique:'User name should be unique.'
	};
}








