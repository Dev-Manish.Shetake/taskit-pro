import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, } from '@angular/common/http';
import { map, debounceTime } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EncryptDecryptService } from '../_common_services/encrypt-decrypt.service';
import { WithoutEncryptedUrlsService } from '../_common_services/without-encrypted-urls.service';
import * as CryptoJS from 'crypto-js';

@Injectable()
export class HttpRequestService {
    private baseUrl: string;
    private headers: HttpHeaders;
    private options: any;
    public key: any;
    public randomNumber: any;
    public userAuthInfo: any;

    constructor(private _http: HttpClient, private encrypt_decrypt: EncryptDecryptService,
        private without_encrypted_urls: WithoutEncryptedUrlsService, @Inject('BASE_URL') baseUrl: string) {
        this.baseUrl = baseUrl == 'http://localhost:4200/' ? 'http://127.0.0.1:8000/api/v1/' : baseUrl + 'api/v1/';
    }

    request(type: string, url: string, requestoption: string, requestparam: string, requestbody: any): Observable<any> {
        if (this.without_encrypted_urls.without_encrypted_urls_list[url] == undefined) {
            this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
        }

        let formData = new FormData();
        //==========================================================
        // Encrypted Mode
        //==========================================================

        if (this.encrypt_decrypt.encryptionMode == "encrypt") {
            let header;
            this.randomNumber = (Math.random().toString(36).substring(2) + Math.random().toString(36).substring(2)).substring(0, 16);
            if (this.userAuthInfo == undefined) {
                header = {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json"
                };
            }
            else if (requestbody != null || (type == 'patch' && requestbody == undefined)) {
                if (type == 'patch' && requestbody == undefined) {
                    header = {
                        "X-Requested-With": "XMLHttpRequest",
                        "Authorization": "Bearer " + this.userAuthInfo.currentUser.access,
                    };
                }
                else if (requestbody.toString() == "[object FormData]") {
                    header = {
                        "X-Requested-With": "XMLHttpRequest",
                        "Authorization": "Bearer " + this.userAuthInfo.currentUser.access,
                    };
                }
                else {
                    header = {
                        "X-Requested-With": "XMLHttpRequest",
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + this.userAuthInfo.currentUser.access,
                    };
                }
            }
            else {
                header = {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + this.userAuthInfo.currentUser.access,
                };
            }
            this.headers = new HttpHeaders(header);

            this.options = { headers: this.headers, responseType: 'text' };
            let encryptedData;
            if (this.without_encrypted_urls.without_encrypted_urls_list[url] != undefined) {
                this.encrypt_decrypt.key = this.randomNumber;
                this.key = CryptoJS.enc.Utf8.parse(this.randomNumber);
                encryptedData = this.randomNumber + this.encrypt(requestbody, this.key);
            }
            else {
                if (requestbody != null) {
                    if (requestbody.toString() == "[object FormData]") {
                        encryptedData = requestbody;
                    }
                    else {
                        this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.currentUser.access.toString().split('.')[1].substring(0, 16));
                        encryptedData = this.encrypt(requestbody, this.key)
                    }
                }
                else {
                    this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.currentUser.access.toString().split('.')[1].substring(0, 16));
                    encryptedData = this.encrypt(requestbody, this.key)
                }
            }

            if (type.toLowerCase() == "get") {
                if (this.without_encrypted_urls.without_encrypted_urls_list[url] != undefined) {
                    return this._http.get((this.baseUrl + url+'?str_code='+this.encrypt_decrypt.key) + (requestparam ? requestparam : ''), this.options).pipe(map(
                        (data) => {
                            let jesonData;
                            jesonData = this.decrypt(data, this.key);
                            jesonData = jesonData.trim('0')
                            if (jesonData == '') {
                                return '';
                            }
                            else {
                                return JSON.parse(jesonData);
                            }
                        }
                    ));
                }
                else {
                    return this._http.get((this.baseUrl + url) + (requestparam ? requestparam : ''), this.options).pipe(map(
                        (data) => {
                            let jesonData;
                            jesonData = this.decrypt(data, this.key);
                            jesonData = jesonData.trim('0')
                            if (jesonData == '') {
                                return '';
                            }
                            else {
                                return JSON.parse(jesonData);
                            }
                        }
                    ));
                }
            }
            else if (type.toLowerCase() == "post") {
                return this._http.post((this.baseUrl + url), encryptedData, this.options).pipe(map(
                    (data) => {
                        let jesonData = this.decrypt(data, this.key);
                        if (jesonData != "") {
                            return JSON.parse(jesonData);
                        }
                        else {
                            return "";
                        }
                    }
                ));
            }
            else if (type.toLowerCase() == "put") {
                return this._http.put((this.baseUrl + url), encryptedData, this.options).pipe(map(
                    (data) => {
                        let jesonData = this.decrypt(data, this.key);
                        return JSON.parse(jesonData);
                    }
                ));
            }
            else if (type.toLowerCase() == "patch") {
                return this._http.patch((this.baseUrl + url), requestbody == undefined ? formData : encryptedData, this.options).pipe(map(
                    (data) => {
                        let jesonData: any;
                        if (this.without_encrypted_urls.form_data_urls[url] != undefined) {
                            jesonData = this.decrypt(data, this.key);
                            if (jesonData == '') {
                                return null;
                            }
                            else {
                                return JSON.parse(jesonData);
                            }
                        }
                        else {
                            jesonData = data;
                        }
                        return jesonData;
                    }
                ));
            }
            else if (type.toLowerCase() == 'delete') {
                return this._http.delete((this.baseUrl + url) + (requestparam ? requestparam : ''), this.options).pipe(map(
                    (data) => {
                        //let jesonData = this.decrypt(data, this.key);
                        //return JSON.parse(jesonData);
                        return data;
                    }
                ));
            }
            else if (type.toLowerCase() == "getauto") {
                return this._http.get((this.baseUrl + url) + (requestparam ? requestparam : ''), this.options).pipe(debounceTime(500), map(
                    (data) => {
                        let jesonData = this.decrypt(data, this.key);
                        return JSON.parse(jesonData);
                    }
                ));
            }

            else {
                return Observable.create("");
            }
        }
        //==========================================================
        // Plain Mode
        //==========================================================
        else {
            let header;
            if (this.userAuthInfo == undefined) {
                header = {
                    "X-Requested-With": "XMLHttpRequest",
                    "Content-Type": "application/json"
                    //'Access-Control-Allow-Origin': 'http://127.0.0.1:8000',
                    //'Access-Control-Allow-Credentials': 'true'
                };
            }
            else {
                header = {
                    "X-Requested-With": "XMLHttpRequest",
                    // "Content-Type": "application/json",
                    "Authorization": "Bearer " + this.userAuthInfo.currentUser.access,
                    // "Accept-Language": this.userAuthInfo.currentUser.user == undefined ? (this.userAuthInfo.language.language == undefined ? "en-US" : this.userAuthInfo.language.language) : this.userAuthInfo.currentUser.user.language
                };
            }

            this.headers = new HttpHeaders(header);
            //this.headers.append("X-Requested-With", "XMLHttpRequest");
            //this.headers.append('Access-Control-Allow-Origin', 'http://localhost:4200');
            //this.headers.append('Access-Control-Allow-Credentials', 'true');
            this.options = { headers: this.headers };
            if (type.toLowerCase() == "get") {
                return this._http.get((this.baseUrl + url) + (requestparam ? requestparam : ''), this.options);
            }
            else if (type.toLowerCase() == "post") {
                return this._http.post((this.baseUrl + url), requestbody, this.options);
            }
            else if (type.toLowerCase() == "put") {
                return this._http.put((this.baseUrl + url), requestbody, this.options);
            }
            else if (type.toLowerCase() == "patch") {
                return this._http.patch((this.baseUrl + url), requestbody, this.options);
            }
            else if (type.toLowerCase() == 'delete') {
                return this._http.delete((this.baseUrl + url) + (requestparam ? requestparam : ''), this.options)
            }
            else if (type.toLowerCase() == "getauto") {
                return this._http.get((this.baseUrl + url) + (requestparam ? requestparam : ''), this.options).pipe(debounceTime(500), map(
                    (data) => {
                        return data;
                    }
                ));
            }
            else {
                return Observable.create("");
            }
        }
    }

    private errorHandler(err: HttpErrorResponse) {
        if (err.status == 401) {

        }
    }

    encrypt(msgString, key) {
        // msgString is expected to be Utf8 encoded
        var iv = CryptoJS.lib.WordArray.random(16);
        var encrypted = CryptoJS.AES.encrypt(JSON.stringify(msgString), key, {
            iv: iv
        });
        return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
    }

    decrypt(ciphertextStr, key) {
        var ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);

        // split IV and ciphertext
        var iv = ciphertext.clone();
        iv.sigBytes = 16;
        iv.clamp();
        ciphertext.words.splice(0, 4); // delete 4 words = 16 bytes
        ciphertext.sigBytes -= 16;

        // decryption
        var decrypted = CryptoJS.AES.decrypt({ ciphertext: ciphertext }, key, {
            iv: iv
        });
        return decrypted.toString(CryptoJS.enc.Utf8);

    }
}
