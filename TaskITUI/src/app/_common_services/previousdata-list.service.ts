import { Injectable } from "@angular/core";
import { Router} from '@angular/router';
import { Location } from '@angular/common';

@Injectable()

export class GetListPageDataService {
    

    constructor(private router: Router, private location: Location) {}

    // create page session..
    createSession(sessionObj, kendoPage){
        let prevURL = this.router.url;
		if(prevURL.charAt(0) == '/'){
            prevURL = prevURL.substr(1);
        }
        sessionObj.kendoPagination = {skip: kendoPage, take: 10};
        sessionObj.isBack = false;
        sessionObj.prevURL = prevURL;
        
        // create session..
        sessionStorage.setItem("listPageState", JSON.stringify(sessionObj));
    }

    // back button flag set up in session..
    setBackButtonFlagInSession(){        
        let sessionObj = JSON.parse(sessionStorage.getItem("listPageState"));
        if(sessionObj !== null){
            sessionObj.isBack = true;
            sessionStorage.setItem("listPageState", JSON.stringify(sessionObj));
            this.router.navigate([sessionObj.prevURL]);            
        } else {
            this.location.back();
        }
    }

    // send session data on page load id back button flag is true..
    sendSeassionObj(){
        return JSON.parse(sessionStorage.getItem("listPageState"));
    }

    // clear session.. 
    clearlistPageSession(sessionName){
        sessionStorage.removeItem(sessionName);
    }

    //replace property names....
    replacePropName(oldProp, newProp, listObj){
        let propVal = new RegExp(oldProp, 'g');
        let strData = JSON.stringify(listObj).replace(propVal, newProp);
        let parseData = JSON.parse(strData);

        return parseData;
    }

    // filter search data to list page..
    masterPaginationData(filedValue, listObj, propName){
        // changes in property value..
        let parseData = this.replacePropName(propName, 'name', listObj);
            
        // finding match in array obj..
        let regVal = new RegExp(filedValue, 'i');
        let filterData = parseData.filter(item => item.name.match(regVal));

        // changing back to again..
        return this.replacePropName('name', propName, filterData);
    }
}