import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WithoutEncryptedUrlsService {

  constructor() { }

  public without_encrypted_urls_list: any = {
    'auth/login': 'auth/login',
    'auth/sign-up': 'auth/sign-up',
    'auth/sign-up-register': 'auth/sign-up-register',
    'social_auth/login': 'social_auth/login',
    'users/verify': 'users/verify',
    'password/forgot-generate-otp': 'password/forgot-generate-otp',
    'password/forgot-verify-otp': 'password/forgot-verify-otp',
    'categories/subcategory-list': 'categories/subcategory-list',
    'categories': 'categories',
    'subcategories': 'subcategories',
    'gigs': 'gigs'
  }

  public form_data_urls: any = {
    'labours/enc/import/verify': 'labours/enc/import/verify'
  }
}
