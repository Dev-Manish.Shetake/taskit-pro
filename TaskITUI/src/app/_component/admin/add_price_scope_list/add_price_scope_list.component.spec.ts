/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Add_price_scope_listComponent } from './add_price_scope_list.component';

describe('Add_price_scope_listComponent', () => {
  let component: Add_price_scope_listComponent;
  let fixture: ComponentFixture<Add_price_scope_listComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Add_price_scope_listComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Add_price_scope_listComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
