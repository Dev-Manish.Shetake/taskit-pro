import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

declare var jQuery;
declare var toastr;
@Component({
  selector: 'app-add_price_scope_list',
  templateUrl: './add_price_scope_list.component.html',
  styleUrls: ['./add_price_scope_list.component.css']
})
export class Add_price_scope_listComponent implements OnInit {

  public getCategory: any = { id: 0, category_name: "" };
  public getsubCategory: any = { id: 0, sub_category_name: "" };
  public categoryList: any;
  public subCategoryList: any;
  public priceScope_control_type:any;
  public controlType:any;
  public txt_price_scope_name:string;
  public showMasterOptions:boolean=false;

  public txt_watermark:any;
  public is_Mandatory:boolean=false;
  public getPricescope_controlType: any = { id: 0, price_scope_control_type_name: "" };

  public priceScope_name: priceScope={
    sub_category_price_scope: [
      {
        sub_category_id: 0,
        seq_no: 0,
        price_scope_name: "",
        price_scope_control_type_id: 0,
        watermark: "",
        is_mandatory: true,
        details: [
          {
            seq_no: 0,
            value: "",
            display_value: "",
            actual_value: 0
          }
        ]
      }
    ]
  };

  public subcategories_Pricescope:subCategory_priceScope={
    result:[{
      row_id:0,
      id: 0,
      seq_no: 0,
      price_scope_name:"",
      is_heading_visible: true,
      price_scope_control_type: {
        id: 0,
        price_scope_control_type_name: ""
      },
      watermark: "",
      default_value: null,
      sub_category_price_scope_details: [
        {
          id: 0,
          seq_no: 0,
          value: "",
          display_value: "",
          actual_value: 0,
          txt_option:"",
          showTextBox:false,
          is_new:false
        }
      ],
      is_mandatory: true,
      is_active: true,
      created_date: "",
      modified_date: "",
      is_new:false
    }]
  };
  public queryParam: any;
  public _queryParams: any;
  public editMode:boolean=false;
  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService) { }

  ngOnInit() {
    if (this.queryParam == null) {
      this._queryParams = (sessionStorage.getItem("queryParamsPriceScope") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      if( this.queryParam !=null &&  this.queryParam !=undefined && this._queryParams!="{}")
      {
        this.editMode=this.queryParam.subCategoryid>0?true:false;
        if(this.editMode==true)
        {
          this.getCategory_dropdown();
        }
        else{
          this.getCategory_dropdown();
        }
      }
      else{
        this.getCategory_dropdown();
      }
    }
    else{
    this.getCategory_dropdown();
    }
    this.getPrice_scope_control_type();
    //this.getSubcategories_priceScope();
  }

  //**********************************
  // Category List
  //**********************************
  getCategory_dropdown() {
    this.httpServices.request("get", "categories/list", "", "", null).subscribe((data) => {
      this.categoryList = data;
      if(this.editMode==true)
      {
        this.getCategory.id=this.queryParam.categoryId;
        this.getsubCategory_dropdown(this.getCategory.id);
      }
    }, error => {
      console.log(error);
    });
  }

  //**********************************
  // SubCategory List
  //**********************************
  getsubCategory_dropdown(categoryID: number) {
    //this.progress.show();
    if (this.getCategory.id > 0) {
      this.progress.show();
      this.httpServices.request('get', 'subcategories/list?category_id=' + categoryID, null, null, null)
        .subscribe(data => {
          if (data.length > 0) {
            this.subCategoryList = data;
            if(this.editMode==true)
            {
              this.getsubCategory.id=this.queryParam.subCategoryid;
              this.getSubcategories_priceScope();
            }
          }
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    }
  }

  //**********************************
  //To get Control type dropdown
  //**********************************
  getPrice_scope_control_type() {
    this.httpServices.request("get", "price_type/price-scope-control-type", "", "", null).subscribe((data) => {
      this.priceScope_control_type = data;
    }, error => {
      console.log(error);
    });
  }

  //*****************************************
  //Edit pencil btn edit Price scope name
  //*****************************************
  public priceScopeId:any;
  public seqNo:number;
  onEdit_priceScopeName(price_ScopeId){
    this.priceScopeId=price_ScopeId;
       this.subcategories_Pricescope.result.forEach(x=>{
      if(x.id==price_ScopeId){
         this.txt_price_scope_name=x.price_scope_name;
      }
    });
    jQuery('#Add_Price_scope').modal({ backdrop: 'static', keyboard: false });
  }

  //*****************************************
  //Edit pencil btn to view value in Textbox
  //*****************************************
  onOpenPencilbtn(itemId:any,master_Id:number){
    this.subcategories_Pricescope.result.forEach(x=>{
          if(x.id==itemId)
          {
            x.sub_category_price_scope_details.forEach(y=>{
              if(y.id==master_Id)
              {
                y.showTextBox=true;
                //y.display_value = y.txt_option;
                y.txt_option = y.display_value;
              }
              else{
                y.showTextBox = false;
              }
            })
        }
    })
  }

public showDuplicateMasterOption:boolean=false;
  onEditoptionList_metadata(itemId: any, master_Id: number, optionName: any) {
    this.subcategories_Pricescope.result.forEach(x => {
      if (x.id == itemId) {
       // x.sub_category_price_scope_details.forEach(y => {
         for(let k=0;k<x.sub_category_price_scope_details.length;k++)
         {
          if(x.sub_category_price_scope_details.filter(element=>element.display_value==optionName && element.id!=master_Id).length>0)
          {
            toastr.error("This option is already exist against this price scope.");
            break;
          }
          else{
          if (x.sub_category_price_scope_details[k].id == master_Id) {
            x.sub_category_price_scope_details[k].showTextBox = true;
            //y.display_value = y.txt_option;
            x.sub_category_price_scope_details[k].display_value = x.sub_category_price_scope_details[k].txt_option;
            x.sub_category_price_scope_details[k].showTextBox = false;
          }
          else {
            x.sub_category_price_scope_details[k].showTextBox = false;
          }
        }
        }
      }
    })
  }

  //*****************************************
  //On Delete Master Option
  //*****************************************
  public removeMaster_optionId:any;
  onDeleteBtn(master_option_Id:number){
    this.removeMaster_optionId = master_option_Id;
    jQuery('#delete_record').modal({ backdrop: 'static', keyboard: false });
  }

  onDeletePopBtn_masterOptions(){

    if(this.removeMaster_optionId>0){
      this.subcategories_Pricescope.result.forEach(x=>{
        if(x.row_id==this.tabId)
        {
        x.sub_category_price_scope_details.splice(x.sub_category_price_scope_details.findIndex(y=>y.id==this.removeMaster_optionId),1)
        for (let i = 0; i < x.sub_category_price_scope_details.length; i++) {
          if (x.sub_category_price_scope_details[i].seq_no < this.removeMaster_optionId) {
  
          }
          else if (x.sub_category_price_scope_details[i].seq_no > this.removeMaster_optionId) {
            x.sub_category_price_scope_details[i].seq_no = x.sub_category_price_scope_details[i].seq_no - 1;
          }
        }
      }
      })
    
      jQuery('#delete_record').modal('hide');	
    }
  }

  //*****************************************
  //On Undo Master Option
  //*****************************************
  onClickUndo(itemId: any, master_option_Id: any) {
    this.subcategories_Pricescope.result.forEach(x => {
      if (x.id == itemId) {
        x.sub_category_price_scope_details.forEach(y => {
          if (y.id == master_option_Id) {
            y.showTextBox = false;
            //y.display_value = y.txt_option;
            y.txt_option = "";
          } else {
            y.showTextBox = false;
          }
        })
      }
    })
  }
  
  //*****************************************
  //On Get tab data that is price Scope name
  //*****************************************
  getSubcategories_priceScope() {
    this.progress.show();
    this.httpServices.request("get", "subcategories/price-scope?sub_category_id="+this.getsubCategory.id, "", "", null).subscribe((data) => {
      this.subcategories_Pricescope.result = data;
      if(this.subcategories_Pricescope.result.length>0)
      {
      this.subcategories_Pricescope.result.forEach(x => {
        x.row_id=x.id;
        x.is_new=false;
        if(x.sub_category_price_scope_details.length>0)
        {
        x.sub_category_price_scope_details.forEach(y => {
          y.showTextBox = false;
          y.is_new=false;
        })
      }
        this.tabId=this.subcategories_Pricescope.result[0].id;
      })
    }
   
      this.progress.hide();
    },error=>{
      console.log(error);
      this.progress.hide();
    });
  }

  //*****************************************
  //On Get tab data that is price Scope name
  //*****************************************
  public txt_new_master_option:any;
  public tabId:number;
  public showDuplicacyMsg:boolean=false;
  onTabClick(masterId:number){
    this.tabId=masterId;
  }

  Addnew_masterOption(){
  this.subcategories_Pricescope.result.forEach(x => {
     if (x.row_id == this.tabId) {
      let seq_no: number;
      let id:number;
      let flag:boolean=true;
      if(x.sub_category_price_scope_details.length>0){
        if(x.sub_category_price_scope_details.filter(element=>element.display_value==this.txt_new_master_option).length>0)
        {
          flag=false;
          toastr.error("This option is already exist.");
        }
        else{
        id=Math.max.apply(Math, x.sub_category_price_scope_details.map(function (o) { return o.id; }));
        seq_no=Math.max.apply(Math, x.sub_category_price_scope_details.map(function (o) { return o.seq_no; }));
        }
      }else{
        seq_no=0;
        id=0;
      }
      if(flag==true)
      {
      x.sub_category_price_scope_details.push({id:id+1,seq_no:seq_no+1,value:this.txt_new_master_option,display_value:this.txt_new_master_option,actual_value:this.txt_new_master_option,txt_option:"",showTextBox:false,is_new:true});
      }
    }
    })
    this.txt_new_master_option="";
  }

  //*****************************************
  //On Delete tab data that is price Scope name
  //*****************************************
  public remove_priceScope_id:number;
  public is_new_main:boolean;
  onDeleteBtn_priceScopeName(priceScope_id:number,is_new:boolean){
   this.remove_priceScope_id=priceScope_id;
   this.is_new_main=is_new;
   jQuery('#delete_record_tab_pricescope').modal({ backdrop: 'static', keyboard: false });
  }

  onDeleteTab_priceScopeName(){
    if(this.is_new_main==true)
    {
      this.subcategories_Pricescope.result.splice(this.subcategories_Pricescope.result.findIndex(x=>x.row_id==this.remove_priceScope_id),1);
      for (let i = 0; i < this.subcategories_Pricescope.result.length; i++) {
        if (this.subcategories_Pricescope.result[i].seq_no < this.remove_priceScope_id) {

        }
        else if (this.subcategories_Pricescope.result[i].seq_no > this.remove_priceScope_id) {
          this.subcategories_Pricescope.result[i].seq_no = this.subcategories_Pricescope.result[i].seq_no - 1;
        }
      }
      jQuery('#delete_record_tab_pricescope').modal('hide');	
      toastr.success("Record Deleted Successfully");
    }
    else{
      this.progress.show();
    this.httpServices.request("delete","subcategories_price_scope/delete/"+this.remove_priceScope_id,"","",null).subscribe((data)=>{
      this.subcategories_Pricescope.result.splice(this.subcategories_Pricescope.result.findIndex(x=>x.row_id==this.remove_priceScope_id),1);
      for (let i = 0; i < this.subcategories_Pricescope.result.length; i++) {
        if (this.subcategories_Pricescope.result[i].seq_no < this.remove_priceScope_id) {

        }
        else if (this.subcategories_Pricescope.result[i].seq_no > this.remove_priceScope_id) {
          this.subcategories_Pricescope.result[i].seq_no = this.subcategories_Pricescope.result[i].seq_no - 1;
        }
      }
      this.progress.hide();
      jQuery('#delete_record_tab_pricescope').modal('hide');	
      toastr.success("Record Deleted Successfully");
    },error=>{
      this.progress.hide();
      console.log(error);
    });
  }
  }

  //*****************************************
  //On Plus Price scope btn
  //*****************************************
  onPlus_priceScope(){
    jQuery('#Add_Price_scope').modal({ backdrop: 'static', keyboard: false });
    this.txt_price_scope_name="";
  }

  onPlusbtn_priceScope() {
    let seq_no=0;
    let row_temp_id=0;
    let flag:boolean=true;
    if(this.subcategories_Pricescope.result.length>0)
    {
      if(this.subcategories_Pricescope.result.filter(element=>element.price_scope_name==this.txt_price_scope_name).length>0)
      {
        flag=false;
        toastr.error("This price scope is already exist.");
      }
      else{
      seq_no=Math.max.apply(Math, this.subcategories_Pricescope.result.map(function (o) { return o.seq_no; }));
      row_temp_id=Math.max.apply(Math, this.subcategories_Pricescope.result.map(function (o) { return o.id; }));
      }
    }
    else{
      seq_no=0;
      row_temp_id=0;
    }
    
if(flag==true)
{
    let  price_scope_control_type_detail:any= {
      id: 0,
      price_scope_control_type_name: ""
    }

     let sub_category_price_scope_details_info:any= [
        {
          id: 0,
          seq_no: 0,
          value: "",
          display_value: "",
          actual_value: 0
        }
      ]

    if (this.priceScopeId > 0) {
      this.subcategories_Pricescope.result.forEach(x => {
        if (x.id == this.priceScopeId) {
          x.price_scope_name = this.txt_price_scope_name;
        }
        jQuery('#Add_Price_scope').modal('hide');
        this.txt_price_scope_name="";
        this.priceScopeId=0;
      });
    } else {
      sub_category_price_scope_details_info.splice(sub_category_price_scope_details_info.findIndex(element=>element.value.toString().trim()),1);
      this.subcategories_Pricescope.result.push({
        row_id:row_temp_id + 1, id: row_temp_id + 1, seq_no: seq_no + 1, price_scope_name: this.txt_price_scope_name, is_heading_visible: true, price_scope_control_type: price_scope_control_type_detail, watermark: "", default_value: null,
        sub_category_price_scope_details: sub_category_price_scope_details_info, is_mandatory: false, is_active: true, created_date: "", modified_date: null,is_new:true
      });
     // details_info.splice(details_info.findIndex(element=>element.display_value==""),1)
      //sub_category_price_scope_details_info.splice(sub_category_price_scope_details_info.findIndex(x=>x.display_value==""),1);
     if(this.tabId==undefined || this.tabId==null || this.tabId==0)
     {
       this.tabId=1;
     }
      jQuery('#Add_Price_scope').modal('hide');
      this.txt_price_scope_name="";
    }
  }
  }

  //*****************************************
  //Update Master
  //*****************************************
  public showDuplicate_priceScope:boolean=false;
  AddBtn_priceScope(){
    let  sub_category_price_scope_body:any= {
    sub_category_price_scope:[
        {
          id: 0,
          sub_category_id: 0,
          seq_no: 0,
          price_scope_name: "",
          price_scope_control_type_id: 0,
          watermark: "",
          is_mandatory: true,
          details: [
            {
              id: 0,
              seq_no: 0,
              value: "",
              display_value: "",
              actual_value: 0,
              is_new:false
            }
          ]
        }]
      }
 
      
     // sub_category_price_scope.sub_category_id=this.getsubCategory.id;
     sub_category_price_scope_body.sub_category_price_scope.splice(sub_category_price_scope_body.sub_category_price_scope.findIndex(x=>x.sub_category_id==0),1);
      this.subcategories_Pricescope.result.forEach(x=>{
        let details_info:any= [
          {
            id: 0,
            seq_no: 0,
            value: "",
            display_value: "",
            actual_value: 0,
            is_new:false
          }
        ]
        details_info.splice(details_info.findIndex(element=>element.value.toString().trim()==""),1);
        if(x.price_scope_control_type.id==2)
        {
        x.sub_category_price_scope_details.forEach(y=>{
          let id=0;
          if(x.is_new==true)
          {
            id=0;
          }
          else if(x.is_new==false && y.is_new==true)
          {
            id=0;
          }
          else if(x.is_new==false && y.is_new==false)
          {
            id=y.id;
          }
          details_info.push({id:id,seq_no:y.seq_no,value:y.value,display_value:y.display_value,actual_value:null,txt_option:this.txt_new_master_option,showTextBox:false})
        })
      }
        
        //splice for details
        //  details_info.splice(details_info.findIndex(element=>element.value.toString().trim()==""),1);

          let main_id=0;
          if(x.is_new==true)
          {
            main_id=0;
          }
          else{
            main_id=x.id;
          }
        sub_category_price_scope_body.sub_category_price_scope.push({id:main_id,sub_category_id:this.getsubCategory.id,seq_no:x.seq_no,
          price_scope_name:x.price_scope_name,price_scope_control_type_id:x.price_scope_control_type.id,watermark:(x.watermark!=null && x.watermark!=undefined && x.watermark!="")?x.watermark:null,
          is_mandatory:x.is_mandatory,details:details_info})
          
      })
      //sub_category_price_scope_body.sub_category_price_scope.splice(sub_category_price_scope_body.sub_category_price_scope.findIndex(x=>x.sub_category_id==0),1);
//splice for subcategory
 
  
  this.progress.show();
   this.httpServices.request("patch","subcategories_price_scope/update","","",sub_category_price_scope_body).subscribe((data)=>{
    toastr.success("Record Updated Successfully");
    this.progress.hide();
   },error=>{
     console.log(error)
     this.progress.hide();
   });
  }

}

interface subCategory_priceScope{
  result:[{
    row_id:number,
    id: number,
    seq_no: number,
    price_scope_name:string,
    is_heading_visible: true,
    price_scope_control_type: {
      id: number,
      price_scope_control_type_name: string
    },
    watermark: string,
    default_value: null,
    sub_category_price_scope_details: [
      {
        id: number,
        seq_no: number,
        value: string,
        display_value: string,
        actual_value: number,
        txt_option:string,
        showTextBox:boolean,
        is_new:boolean
      }
    ],
    is_mandatory: boolean,
    is_active: boolean,
    created_date: string,
    modified_date: string,
    is_new:boolean
  }]
}

interface priceScope{
    sub_category_price_scope: [
      {
        sub_category_id: number,
        seq_no: number,
        price_scope_name: string,
        price_scope_control_type_id: number,
        watermark: string,
        is_mandatory: boolean,
        details: [
          {
            seq_no: number,
            value: string,
            display_value: string,
            actual_value: number
          }
        ]
      }
    ]
}
