import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State, process } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
declare var toastr: any;
@Component({
  selector: 'app-admin_Post_a_request',
  templateUrl: './admin_Post_a_request.component.html',
  styleUrls: ['./admin_Post_a_request.component.css']
})
export class Admin_Post_a_requestComponent implements OnInit {
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip: 0,
    take: 10
  }
  public pageNumber: any;
  public admin_PostrequestList: any;
  public postRequest_gridData: GridDataResult | undefined;
  public userAuthInfo: any;
  public requestStatus_list: any;
  public buyer: string;
  public budget: any;
  public delivery_time: any;
  public searchStatus: any = { id: 0, request_status_name: "" };
  constructor(private httpServices: HttpRequestService, private encrypt_decrypt: EncryptDecryptService, private progress: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.getRequest_List();
    this.getRequestStatus();
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
  }


  getRequestStatus() {
    this.httpServices.request("get", "request_status/", "", "", null).subscribe((data) => {
      this.requestStatus_list = data;
    }, error => {
      console.log(error);
    });
  }

  getRequest_List() {
    this.progress.show();
    this.httpServices.request("get", "requests/list", "", "", null).subscribe((data) => {
      this.admin_PostrequestList = data.results;
      this.loadRequest_List(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }


  private loadRequest_List(recordCount: number): void {
    if (this.admin_PostrequestList.length > 0) {
      this.postRequest_gridData = {
        data: orderBy(this.admin_PostrequestList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.postRequest_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortPost_a_requestChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadRequest_List(this.totalRecordCount);
  }


  public DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.postRequest_gridData = process(this.admin_PostrequestList, this.state);
  }

  //Page Change
  pageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    if (this.buyer != null && this.buyer != '' && this.buyer != undefined) {
      url = url + "&buyer_name=" + this.buyer;
    }
    if (this.delivery_time != null && this.delivery_time != '' && this.delivery_time != undefined) {
      url = url + "&service_delivery_name=" + this.delivery_time;
    }
    if (this.budget != null && this.budget != '' && this.budget != undefined) {
      url = url + "&budget=" + this.budget;
    }
    if (this.searchStatus.id != null && this.searchStatus.id != 0 && this.searchStatus.id != undefined) {
      url = url + "&status_id=" + this.searchStatus.id;
    }
    var url = "requests/list?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.admin_PostrequestList = data.results;
        this.loadRequest_List(data.count);
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }


  onSearchBtnClick() {
    this.progress.show;
    var url = "requests/list?page=1&page_size=" + this.pageSize;
    if (this.buyer != null && this.buyer != '' && this.buyer != undefined) {
      url = url + "&buyer_name=" + this.buyer;
    }
    if (this.delivery_time != null && this.delivery_time != '' && this.delivery_time != undefined) {
      url = url + "&service_delivery_name=" + this.delivery_time;
    }
    if (this.budget != null && this.budget != '' && this.budget != undefined) {
      url = url + "&budget=" + this.budget;
    }
    if (this.searchStatus.id != null && this.searchStatus.id != 0 && this.searchStatus.id != undefined) {
      if (this.searchStatus.id == 4) {
        url = url + "&is_active=false&status_id=" + this.searchStatus.id;
      }
      else {
        url = url + "&status_id=" + this.searchStatus.id;
      }
    }
    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      //this.progress.hide();
      this.admin_PostrequestList = data.results;
      this.loadRequest_List(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  onPost_requestEditPencilBtn(request_id: number, request_status_id: number) {
    let request: any = { request_id: request_id, request_status_id: request_status_id };
    sessionStorage.setItem("request_queryParam", JSON.stringify(request));
    this.router.navigate(['/edit_post_request'], {});
  }

  onBuyer_nameClick(buyer_id) {
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == buyer_id) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.buyer_profile_id = buyer_id;
    this.encrypt_decrypt.buyer_profile_mode = profile_mode;
    this.router.navigate(['/buyer_profile'], {});
  }
}
