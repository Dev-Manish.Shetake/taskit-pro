import { Component, OnInit } from '@angular/core';
import { SocialAuthService } from 'angularx-social-login';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-dashboard_admin',
  templateUrl: './dashboard_admin.component.html',
  styleUrls: ['./dashboard_admin.component.css']
})
export class Dashboard_adminComponent implements OnInit {

  constructor(private progress: NgxSpinnerService, private httpServices: HttpRequestService, private authService: SocialAuthService) {
    //this.loadScripts();
  }

 // Method to dynamically load JavaScript 
  loadScripts() {
    // This array contains all the files/CDNs 
    const dynamicScripts = [
      'assets/js/jquery.meanmenu.js',
      'assets/js/metisMenu.min.js',
      'assets/js/metisMenu-active.js',
      'assets/js/jquery.sticky.js',
      'assets/js/main.js'
    ];

    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  ngOnInit() {
  }

}
