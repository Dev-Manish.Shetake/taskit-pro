import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { toODataString } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

declare var jQuery: any;
declare var toastr: any;
@Component({
  selector: 'app-edit_Post_a_request',
  templateUrl: './edit_Post_a_request.component.html',
  styleUrls: ['./edit_Post_a_request.component.css']
})
export class Edit_Post_a_requestComponent implements OnInit {
  public queryParam: any;
  public requestId: any;
  public request_status_id: any;
  public _queryParams: any;
  public admin_PostrequestList: any;
  //public requestApprove:any;
  //public requestReject:any;
  public date: any;
  public description: string;
  public category: string;
  public subCategory: string;
  public buyer_name: string;
  public delivery_time: any;
  public budget: string;
  public status: string;
  public statusId: number;
  public attachments: any;
  public reject_comment: any;
  public check_reject_comment: boolean = false;

  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    if (this.queryParam == null || this.queryParam == undefined) {
      this._queryParams = (sessionStorage.getItem("request_queryParam") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      this.requestId = this.queryParam.request_id;
      this.request_status_id = this.queryParam.request_status_id;
      this.getRequest_list();
    }
    //this.getRequest_list();  
  }

  
  // ========================
  // View Attached Imgae 
  // =========================
  public image_ViewUrl: any;
  openImage(item: any) {
    this.image_ViewUrl = item;
    jQuery('#imagemodal').modal({ backdrop: 'static', keyboard: false });
  }
  
  //this.requestId=30;
  getRequest_list() {
    this.progress.show();
    let url: any;
    if (this.request_status_id == 4) {
      url = "requests/" + this.requestId + "?is_active=false"
    }
    else {
      url = "requests/" + this.requestId
    }
    this.httpServices.request("get", "requests/" + this.requestId, "", "", null).subscribe((data) => {
      this.admin_PostrequestList = data;

      this.requestId = data.id;
      this.description = data.description;
      this.date = data.created_date;
      this.category = data.category.category_name;
      this.subCategory = data.sub_category.sub_category_name;
      this.buyer_name = data.created_user;
      this.delivery_time = data.service_delivery == null ? data.service_delivery_other : data.service_delivery.service_delivery_name;
      this.budget = data.budget.replace(".00", "");
      this.status = data.request_status.request_status_name;
      this.statusId = data.request_status.id;
      //this.attachments=data.attachments.file_path;

      this.admin_PostrequestList.attachments.forEach(element => {
        if (this.requestId > 0) {
          this.attachments = element.file_path;
        }
      });

      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  onApproveBtn() {
    //On Approve btn status will turn to Active
    this.progress.show();
    this.httpServices.request("patch", "requests/approve/" + this.requestId, null, null, null)
      .subscribe((data) => {
        toastr.success("Request Approved");
        this.router.navigate(['/admin_post_a_request']);
        this.progress.hide();
      });
  }

  onRejectBtn() {
    //On Request btn status will turn to UnApproved
    // this.progress.show();
    // this.httpServices.request("patch", "requests/reject/" + this.requestId, null, null, null)
    //   .subscribe((data) => {
    //     toastr.success("Request Rejected");
    //     this.router.navigate(['/admin_post_a_request']);
    //     this.progress.hide();
    //   });
    this.check_reject_comment = false;
    this.reject_comment = '';
    jQuery('#reject_comment').modal({ backdrop: 'static', keyboard: false });
  }

  //========================================
  // Reject Submit
  //========================================
  onClickRejectCommentSubmit() {
    this.check_reject_comment = true;
    if (this.reject_comment != null && this.reject_comment != undefined && this.reject_comment.toString().trim() != "") {
      let reject_comment_body: any = {
        reject_comment: ""
      }
      reject_comment_body.reject_comment = this.reject_comment;
      this.progress.show();
      this.httpServices.request("patch", "requests/reject/" + this.requestId, null, null, reject_comment_body).subscribe((data) => {
        toastr.success("This request has been rejected successfully.");
        jQuery('#reject_comment').modal("hide");
        this.router.navigate(['/admin_post_a_request'], {});
        this.progress.hide();

      }, (error) => {
        this.progress.hide();
      });
    }
  }

}
