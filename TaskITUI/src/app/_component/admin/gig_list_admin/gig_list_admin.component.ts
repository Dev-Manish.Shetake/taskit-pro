import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { orderBy, SortDescriptor, State, process } from '@progress/kendo-data-query';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { Router } from '@angular/router';
import { GetListPageDataService } from '../../../_common_services/previousdata-list.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
declare var toastr: any;
@Component({
  selector: 'app-gig_list_admin',
  templateUrl: './gig_list_admin.component.html',
  styleUrls: ['./gig_list_admin.component.scss']
})
export class Gig_list_adminComponent implements OnInit {

  constructor(private previousPageSessionData: GetListPageDataService, private encrypt_decrypt: EncryptDecryptService, private httpServices: HttpRequestService, private progress: NgxSpinnerService, private router: Router) {
    this.loadScripts();
  }
  loadScripts() {
    // This array contains all the files/CDNs 
    const dynamicScripts = [
      'assets/js/jquery.meanmenu.js',
      'assets/js/metisMenu.min.js',
      'assets/js/metisMenu-active.js',
      'assets/js/jquery.sticky.js',
      'assets/js/main.js'
    ];

    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      document.getElementsByTagName('body')[0].appendChild(node);
    }
  }

  public pageSize: any = 10;
  public admin_gig_list: any;
  public gig_name: any;
  public categoryList: any;
  public getCategory: any = { id: 0, category_name: '' };
  public gigStatusList: any;
  public getGigStatus: any = { id: 0, gig_status_name: '' };
  public seller: any;
  public sort: SortDescriptor[] = [];
  public totalRecordCount: number = 0;
  public skip = 0;
  public grid_data: GridDataResult | undefined;
  public state: State = {
    skip: 0,
    take: 10,

    // Initial filter descriptor
    filter: {
      logic: 'and',
      filters: []
    }
  };
  public pageNumber: any = 1;
  public gig_list: any;
  public userAuthInfo: any;
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.get_gig_list();
    this.getGigCategoryList();
    this.getGigStatusList();
  }
  // ===================================
  // get gig list 
  // ===================================
  get_gig_list() {
    this.progress.show();
    this.httpServices.request("get", "gigs/backoffice-list?page=1&page_size=" + this.pageSize, null, null, null).subscribe((data) => {
      this.gig_list = data.results;
      this.loadGig(data.count);
      this.maniulateData();
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }
  // ===========================
  // Manippulate data 
  // =============================
  maniulateData() {
    for (let i = 0; i < this.gig_list.length; i++) {

      if (this.gig_list[i].gig_status_id == 1) {
        this.gig_list[i].statusColour = "badge badge-success Status_Draft";
        this.gig_list[i].CHECKBOX = false;
      }
      else if (this.gig_list[i].gig_status_id == 2) {
        this.gig_list[i].statusColour = "badge badge-success status_pending_for_approval";
        this.gig_list[i].CHECKBOX = false;
      }
      else if (this.gig_list[i].gig_status_id == 3) {
        this.gig_list[i].statusColour = "badge badge-success Requires_Modification";
        this.gig_list[i].CHECKBOX = false;
      }
      else if (this.gig_list[i].gig_status_id == 4) {
        this.gig_list[i].statusColour = "badge badge-success Status_Denied";
        this.gig_list[i].CHECKBOX = false;
      }
      else if (this.gig_list[i].gig_status_id == 5) {
        this.gig_list[i].statusColour = "badge badge-success status_active";
        this.gig_list[i].CHECKBOX = true;
      }
      else if (this.gig_list[i].gig_status_id == 6) {
        this.gig_list[i].statusColour = "badge badge-success Status_Paused";
        this.gig_list[i].CHECKBOX = false;
      }
    }
  }
  // =================================
  // on Action click 
  // =================================
  onActionClick(item: any) {
    this.progress.show();
    if (item.CHECKBOX == true) {
      this.httpServices.request("patch", "gigs/paused/" + item.gig_id, null, null, null).subscribe((data) => {
        toastr.success("Gig paused successfully.");
        this.get_gig_list();
        this.progress.hide();
      }, error => {
        item.CHECKBOX = true;
        console.log(error);
        this.progress.hide();
      });
    }
    else {
      this.httpServices.request("patch", "gigs/resume/" + item.gig_id, null, null, null).subscribe((data) => {
        toastr.success("Gig resumed successfully.");
        this.get_gig_list();
        this.progress.hide();
      }, error => {
        item.CHECKBOX = false;
        console.log(error);
        this.progress.hide();
      });
    }
  }
  // =============================
  // get gig category list 
  // ==============================
  getGigCategoryList() {
    this.httpServices.request("get", "categories/?page=1&page_size=1000", null, null, null).subscribe((data) => {
      this.categoryList = data.results;
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }
  // =================================
  // get gig status list 
  // ================================
  getGigStatusList() {
    this.httpServices.request("get", "gig_status/", null, null, null).subscribe((data) => {
      this.gigStatusList = data;
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }
  // ====================================
  // on click of clear all 
  // ====================================
  onClearAllClick() {
    this.gig_name = "";
    this.getCategory.id = 0;
    this.seller = "";
    this.getGigStatus.id = 0;
    this.get_gig_list();
  }
  // =================================
  // on click of search 
  // =================================
  onSearchClick() {
    this.progress.show();
    var url = "gigs/backoffice-list?page=1&page_size=" + this.pageSize;
    if (this.gig_name != null && this.gig_name != undefined && this.gig_name != "") {
      url = url + "&gig=" + this.gig_name;
    }
    if (this.getCategory.id != 0 && this.getCategory.id != null && this.getCategory.id != undefined) {
      url = url + "&category_id=" + this.getCategory.id;
    }
    if (this.seller != null && this.seller != "" && this.seller != undefined) {
      url = url + "&seller=" + this.seller;
    }
    if (this.getGigStatus.id != 0 && this.getGigStatus.id != null && this.getGigStatus.id != undefined) {
      url = url + "&status_id=" + this.getGigStatus.id;
    }
    this.httpServices.request("get", url, null, null, null).subscribe((data) => {
      this.gig_list = data.results
      this.maniulateData();
      this.loadGig(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }
  pageChange(event: PageChangeEvent): void {
    var url = "";
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    url = "gigs/backoffice-list?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    if (this.gig_name != null && this.gig_name != undefined && this.gig_name != "") {
      url = url + "&gig=" + this.gig_name;
    }
    if (this.getCategory.id != 0 && this.getCategory.id != null && this.getCategory.id != undefined) {
      url = url + "&category_id=" + this.getCategory.id;
    }
    if (this.seller != null && this.seller != "" && this.seller != undefined) {
      url = url + "&seller=" + this.seller;
    }
    if (this.getGigStatus.id != 0 && this.getGigStatus.id != null && this.getGigStatus.id != undefined) {
      url = url + "&status_id=" + this.getGigStatus.id;
    }
    this.httpServices.request("get", url, null, null, null).subscribe((data) => {
      this.gig_list = data.results;
      this.maniulateData();
      this.loadGig(data.count);

    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadGig(this.gig_list.length);
  }
  private loadGig(recordCount: number): void {
    if (this.gig_list.length > 0) {
      this.grid_data = {
        data: orderBy(this.gig_list, this.sort).slice(this.skip, this.skip + this.pageSize),
        //In case if gig list page has 40 record....SLICE to split record to next position as Record to be shown for Page is 10.
        //So remaining records are sliced to skip btn press.
        total: recordCount
      };
    } else {
      this.grid_data = undefined;
    }
  }
  public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.grid_data = process(this.gig_list, this.state);
  }
  openGigDetails(gig_id: any, gig_status_id: any) {
    let passQueryParam = { id: gig_id, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], { /*queryParams: passQueryParam /*, skipLocationChange: true*/ });
    //window.open("gigs_details?id=" + gig_id, "");
    this.setListPageDataInSession();
  }
  setListPageDataInSession() {
    /* Create Page Data.. */
    let pageData = {
      searchValues: {
        gig: this.gig_name ? this.gig_name : '',
        category_id: this.getCategory.id ? this.getCategory.id : 0,
        seller: this.seller ? this.seller : '',
        status_id: this.getGigStatus.id ? this.getGigStatus.id : '',
      }
    }
    // Page Data, Kendo Page, Search Click
    this.previousPageSessionData.createSession(pageData, this.state.skip);
  }

  // =================================
  // on Seller Name to Seller Profile 
  // =================================
  OnSellernameClick(sellerId) {
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }

  // ==================================
  // Display rating 
  // ==================================
  public printStar: any;
  getStar(value: any) {
    this.printStar = '';
    if (value == null) {
      value = 0;
    }
    if (value > 5) {
      value = 5;
    }
    let printStar = '';
    for (let i = 0; i < value; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star" style="color: #febf10;"></i>' : printStar + "" + '<i class="fa fa-star" style="color: #febf10;"></i>');
    }
    if (value > 1 && value < 5) {
      for (let j = 0; j < (5 - value); j++) {
        printStar = printStar + "" + '<i class="fa fa-star"></i>';
      }
    }
    this.printStar = printStar;
  }
}


