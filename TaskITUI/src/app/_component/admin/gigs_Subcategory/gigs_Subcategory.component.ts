import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent, SelectableSettings, SelectAllCheckboxState } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State, process } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

declare var toastr: any;
declare var jQuery: any;
@Component({
  selector: 'app-gigs_Subcategory',
  templateUrl: './gigs_Subcategory.component.html',
  styleUrls: ['./gigs_Subcategory.component.css']
})
export class Gigs_SubcategoryComponent implements OnInit {

  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService) { }

  public gigs_subCategory_List: any;
  public gigs_subCategoryGriddata: GridDataResult | undefined;
  public pageSize: number = 10;
  public sort: SortDescriptor[] = [];
  public skip = 0;
  public state: State = {
    skip: 0,
    take: 10,
  }
  public gigsCategory_list: any;
  public searchCategories: any = { id: 0, category_name: '' };
  public search_subCategories: any = { id: 0, sub_category_name: '' };
  public txt_searchSubcategory: string;
  public searchStatus: any = { id: 2, status_level: '' };
  public getCategories: any = { id: 0, category_name: '' };
  public tagLine: string;
  public status: any = { id: 2, status_level: '' };
  public pageNumber: any = 1;
  public addClick: boolean = false;
  public subCategory: string;
  public subCategory_Id: any;
  public openshowDeletepop: boolean = false;
  public deleteSubcategory_Id: any;
  public urls: any = [];
  public filename: string = "";
  public fileErrorMsgArr: any[] = [];
  public campFileSpan: any;
  public meta_Details: any;
  public block = "block";
  public none = "none";
  public onShowAddpop: boolean = false;
  public subCategory_edit: any;
  public checkValidation: boolean = false;
  public selectableSettings: SelectableSettings;
  selectAllState: SelectAllCheckboxState = 'unchecked';
  public keyfieldName: any;
  public gridModel: any;
  public selectedRowCount: any;
  public activeClass: any;
  public subCategory_image: any = undefined;
  public subCategoryimage_dataSource: any;
  public photoCopy: boolean = false;
  public photoScan: any;
  public searchsubCatgory_list:any;


  ngOnInit() {
    this.getSub_categoryList();
    this.getCategoryList();
  }


  //To populate Dropdown for Search 
  getCategoryList() {
    this.httpServices.request('get', 'categories/list', '', '', null).subscribe((data) => {
      this.gigsCategory_list = data;
      //toastr.success("Data Fetched Successfully");
    }, error => {
      console.log(error);
    })
  }

  getSearch_subCategory(categoryID: number) {
    if (this.searchCategories.id > 0) {
      this.progress.show();
      this.httpServices.request('get', 'subcategories/list?category_id=' + categoryID, null, null, null)
        .subscribe(data => {
          if (data.length > 0) {
            this.searchsubCatgory_list = data;
            this.progress.hide();
          }
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    }
  }

  //To update single  Subcategory record
  getSubcategory_forEdit(id: number) {
    this.progress.show();
    this.subCategory_Id = id;
    this.httpServices.request('get', 'subcategories/' + id, '', '', null).subscribe((data) => {
      this.subCategory_edit = data;
     // toastr.success("Data Fetched Successfully");

      this.getCategories.id = data.category.id;
      this.getCategories.category_name = data.category.category_name
      this.subCategory = data.sub_category_name;
      this.tagLine = data.tagline;
      this.subCategory_image = data.sub_category_picture;
      this.subCategoryimage_dataSource = data.sub_category_picture;
      //this.status.id=data.is_active

      if (data.is_active == true) {
        this.status.id = 1;
      }
      else {
        this.status.id = 0;
      }
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //To display Category List in Kendo Grid
  getSub_categoryList() {
    this.progress.show();
    this.httpServices.request('get', 'subcategories?page=1&page_size=' + this.pageSize, '', '', null).subscribe((data) => {
      this.gigs_subCategory_List = data.results;
      this.loadGig_subCategoryData(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //Kendo Grid Load Data Method
  private loadGig_subCategoryData(recordCount: number) {

    if (this.gigs_subCategory_List.length > 0) {
      this.gigs_subCategoryGriddata = {
        data: orderBy(this.gigs_subCategory_List, this.sort).slice(this.skip, this.skip + this.pageSize),
        //In case if gig list page has 40 record....SLICE to split record to next position as Record to be shown for Page is 10.
        //So remaining records are sliced to skip btn press.
        total: recordCount
      };
    } else {
      this.gigs_subCategoryGriddata = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortGigs_subCategoryChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadGig_subCategoryData(this.gigs_subCategory_List.count);
  }

  // Kendo Grid - data state change For All Gig List
  public gigs_categoryDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gigs_subCategoryGriddata = process(this.gigs_subCategory_List, this.state);
  }


  onSearchBtnClick() {

    this.progress.show;
    var url = "subcategories/?page=1&page_size=" + this.pageSize;

    //this.txt_searchSubcategory.trim.toString();
    // if (this.search_subCategories.id != null && this.search_subCategories.id != 0 && this.search_subCategories.id != undefined) {
    //   url = url + "&sub_category_name=" + this.search_subCategories.id;
    // }
    if (this.txt_searchSubcategory != null && this.txt_searchSubcategory != '' && this.txt_searchSubcategory != undefined) {
      url = url + "&sub_category_name=" + this.txt_searchSubcategory;
    }
    if (this.searchCategories.id != null && this.searchCategories.id != 0 && this.searchCategories.id != undefined) {
      url = url + "&category_id=" + this.searchCategories.id;
    }
    if (this.searchStatus.id != null && this.searchStatus.id != 2 && this.searchStatus.id != undefined) {
      //url = url + "&is_active=" + this.searchStatus.id;

      if (this.searchStatus.id == 1) {
        url = url + "&is_active=true";
      }
      else {
        url = url + "&is_active=false";
      }
    }

    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.gigs_subCategory_List = data.results;
      this.loadGig_subCategoryData(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }


  pageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "subcategories/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    // if (this.search_subCategories.id != null && this.search_subCategories.id != 0 && this.search_subCategories.id != undefined) {
    //   url = url + "&sub_category_name =" + this.search_subCategories.id;
    // }
    if (this.txt_searchSubcategory != null && this.txt_searchSubcategory != '' && this.txt_searchSubcategory != undefined) {
      url = url + "&sub_category_name =" + this.txt_searchSubcategory;
    }
    if (this.searchCategories.id != null && this.searchCategories.id != 0 && this.searchCategories.id != undefined) {
      url = url + "&category_id=" + this.searchCategories.id;
    }
    if (this.searchStatus.id != null && this.searchStatus.id != 2 && this.searchStatus.id != undefined) {
      //url = url + "&is_active=" + this.searchStatus.id;

      if (this.searchStatus.id == 1) {
        url = url + "&is_active=true";
      }
      else {
        url = url + "&is_active=false";
      }
    }
    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.gigs_subCategory_List = data.results;
      this.loadGig_subCategoryData(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  onAdd_subCategoryPop() {
    jQuery('#Add_Category').modal({ backdrop: 'static', keyboard: false });
    this.onShowAddpop = true;
    this.checkValidation = false;
    this.addClick = true;
    this.getCategories.id = 0;
    this.subCategory = '';
    this.tagLine = '';
    this.status.id = 2
    this.subCategory_image = '';
    this.subCategoryimage_dataSource="";
    this.subCategory_Id=0;
  }

  onUpdate_SubcategoryPencilBtn(id: number) {
    jQuery('#Add_Category').modal({ backdrop: 'static', keyboard: false });
    this.onShowAddpop = true;
    this.addClick=false;
    this.subCategory_Id = id;
    this.getSubcategory_forEdit(id);
  }

  onCancel_addPop() {
    this.onShowAddpop = false;
  }

  onCategoryChange() {
    // this.getCategories.category_name=this.gigsCategory_list.filter(x=>x.id==this.getCategories.id).category_name;
    this.gigsCategory_list.forEach(element => {
      if (element.id == this.getCategories.id) {
        this.getCategories.category_name = element.category_name;
      }
    });
  }

  onAdd_subCategory() {
   
    this.checkValidation = true;
    let addSub_category: any = {
      // id: 0,
      // category: {
      //   category_id: 0,
      //   category_name: "",
      //   is_active: false
      // },
      // seq_no: 0,
      // sub_category_name: "",
      // tagline: "",

        category_id: 0,
        seq_no: 0,
        sub_category_name: "",
        tagline: ""
      
    }

    //addSub_category.id = this.subCategory_Id;
    addSub_category.category_id = this.getCategories.id;
    //addSub_category.category.category_name = this.getCategories.category_name;
    //addSub_category.category.is_active = this.status.id;
    addSub_category.seq_no = null;
    addSub_category.sub_category_name = this.subCategory;
    addSub_category.tagline = this.tagLine;

    const formData = new FormData();
    formData.append("sub_category_picture", this.subCategory_image[0]);

if(addSub_category.category_id!=null && addSub_category.category_id!=0 &&
  addSub_category.sub_category_name!=null && addSub_category.sub_category_name!=undefined &&
  addSub_category.tagline!=null && addSub_category.tagline!=undefined && addSub_category.tagline!="" &&
  addSub_category.seq_no==null){
    this.progress.show();

    if (this.subCategory_Id == 0 || this.subCategory_Id==undefined) {
      this.httpServices.request('post', 'subcategories/', '', '', addSub_category).subscribe((data) => {
        this.httpServices.request('patch', 'subcategories/' + data.id, '', '', formData).subscribe((data) => {
          toastr.success("Sub Categories Inserted Successfully");
          //this.onShowAddpop = false;
          jQuery('#Add_Category').modal('hide');
          this.progress.hide();
        });
      }, error => {
        console.log(error);
        this.progress.hide();
      });

    } else {
      this.httpServices.request('patch', 'subcategories/' + this.subCategory_Id, '', '', addSub_category).subscribe((data) => {
        this.httpServices.request('patch', 'subcategories/' + data.id, '', '', formData).subscribe((data) => {
          toastr.success("Sub Category Updated Successfully");
          //this.onShowAddpop = false;
          jQuery('#Add_Category').modal('hide');
          this.getSub_categoryList();
        });
      }, error => {
        console.log(error);
      });
    }
  }
}


  //On Delete Bin Button
  onDelete_singleSubcategory(id: number) {
    //this.openshowDeletepop = true;
    this.deleteSubcategory_Id = id;
    jQuery('#delete_record').modal({ backdrop: 'static', keyboard: false });
  }

  //Delete Confirm Box Click
  onDeletepopBtn() {
    this.progress.show();
    this.httpServices.request('delete', 'subcategories/' + this.deleteSubcategory_Id, '', '', null).subscribe((data) => {
      toastr.success("Sub Category Deleted Successfully");
      //this.openshowDeletepop = false;
      this.getSub_categoryList();
      jQuery('#delete_record').modal('hide');
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
      jQuery('#delete_record').modal('hide');
    });
  }

  //On Delete Multiple checkbox 
  onDelete_multipleCheckbox() {

    this.httpServices.request('patch', 'categories/delete', '', '', null).subscribe((data) => {
      toastr.success("Records Deleted");
    }, error => {
      console.log(error);
    });
  }

  // ******************************************************************
  // Method for Upload Image Button with Change and Delete Actions 
  // ****************************************************************** 
  fileChangedPhoto(event: any) {
    
    // this.progress.show();
    if (event.target.files.length == 0) {
      this.photoCopy = false;
      this.subCategory_image = undefined;
      this.photoScan = this.block;
      // this.progress.hide();
      return;
    }
    this.photoScan = this.none;
    //To obtaine a File reference

    var files = event.target.files;
    const reader = new FileReader();
    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    if (files[0].type == "image/jpeg" || files[0].type == "image/png" || files[0].type == "image/jpg" || files[0].type == "image/bmp" || files[0].type == "image/gif") {

      fileType = "";
    }
    else {

      fileType = ("<li>" + "The file does not match file type." + "</li>");
    }

    //To check file Size according to upload conditions

    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "Your file does not match the upload conditions, Your file size is:" + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + "MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    // if (files[0].size > 5000000) {

    //   fileSize = "";
    // }
    // else {

    //   fileSize = ("<li>" + "Your file does not match the upload conditions. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    // }

    // if (files[0].size < 30000) {

    //   fileSize = "";
    // }
    // else {

    //   fileSize = ("<li>" + "Your file does not match the upload conditions. The maximum file size for uploads should not be less than 1 MB." + "</li>");
    // }
  
    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      // this.progress.hide();
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {

      this.subCategory_image = event.target.files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.subCategoryimage_dataSource = reader.result;
      }
      //this.buyer_data_source=event.target.result;
      if (this.subCategory_image != "" || this.subCategory_image != undefined) {
        formData.append("sub_category_picture", this.subCategory_image[0]);
        this.photoCopy = true;
        this.photoScan = this.none;
      }
      // this.progress.hide();
    }
    else {
      //this.resetFileInput(this.file_image);
      this.subCategory_image = "";
      this.subCategory_image = undefined;
      this.photoCopy = false;
      this.photoScan = this.block;
      this.progress.hide();
    }
  }

  // ================================
  // Remove attached image 
  // =================================
  // deleteImage(event: any) {
  //   this.urls.splice(this.urls.findIndex(x => x.file_name == event.file_name), 1);
  // }


  // ================================
  // Check All checkbox for Delete
  // =================================

  public setSelectableSettings(): void {

    this.selectableSettings = {
      checkboxOnly: true,//this.checkboxOnly,
      mode: 'multiple'
    };
  }


  onSelectAllChange(checkedState: SelectAllCheckboxState, item: any) {

    let checked = true;
    if (checkedState === 'checked') {
      this.selectAllState = 'checked';
    }
    else {
      this.selectAllState = 'unchecked';
      checked = false;
    }

    this.keyfieldName = this.gridModel.gridSetting.filter(x => x.keyField == true)[0].fieldName;

    this.gridModel.gridResult.slice(this.skip, this.skip + this.gridModel.gridPageSize).forEach(x => {
      this.gridModel.gridResult.filter(f => f[this.keyfieldName] == x[this.keyfieldName]).forEach(w => {
        w[item] = checked;
      })
    });
    this.selectionChanged();

  }
  selectionChanged() {
    let count: any;
    count = this.gridModel.gridResult.filter(f => f['CHECKBOX'] == true).length;
    this.selectedRowCount = count;
    if (this.activeClass == "blink_me") {
      this.activeClass = "blink_me1";
    }
    else {
      this.activeClass = "blink_me";
    }
    //alert(count);
  }

  // ====================================
  // on click of clear all 
  // ====================================
  onClearAllClick() {
    this.searchStatus.id = 2;
    this.searchCategories.id = 0;
    //this.search_subCategories.id=0;
    this.txt_searchSubcategory="";
    this.getSub_categoryList();
  }
  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
     // !(code == 39) && //apostrophe
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }
}

