import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State, process } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

declare var toastr: any;
declare var jQuery: any;

@Component({
  selector: 'app-gigs_category',
  templateUrl: './gigs_category.component.html',
  styleUrls: ['./gigs_category.component.css']
})
export class Gigs_categoryComponent implements OnInit {

  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService) { }


  public gigs_categoryList: any;
  public gigs_categoryGriddata: GridDataResult;
  public state: State = {
    skip: 0,
    take: 10,
  }
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public getStatus: any = { id: 2, status_level: '' };
  public categoryList: any;
  public getCategory: any = { id: 0, category_name: '' };
  public categoryEdit: any;
  public category_Name: string;
  public tagLine: string;
  public status: any = { id: 2, status_level: '' };
  public editUserId: number;
  public deleteCategoryId: number;
  public openshowDeletepop: boolean = false;
  public openshowEditpop: boolean = false;
  public searchCategory_name: string;
  public category_Id: number;
  public checkvalidation: boolean = false;
  public pageNumber: any = 1;

  //addClick to show Status textfield in EditPop but not in Add Pop
  public addClick:boolean=false;
  ngOnInit() {
    this.getGigs_category();
  }


  //To get Gigs_Category List
  getGigs_category() {
    this.progress.show();
    this.httpServices.request('get', 'categories?page=1&page_size=' + this.pageSize, '', '', null).subscribe((data) => {
      this.gigs_categoryList = data.results;  
      for (let i = 0; i < this.gigs_categoryList.length; i++) {
        this.gigs_categoryList.CHECKBOX = false;
      }
      this.loadGig_categoryData(data.count);
      this.progress.hide();
      //toastr.success("Data Fetched Successfully");
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }


  // onCheckallClick(event: any) {

  //   let categoryIds = this.gigs_categoryList.filter(x => x["CHECKBOX"] == true);
  //   let projectExistCount = 0;
  //   let projectNoExistCount = 0;
  //   let flagAvailable: boolean = false;
  //   let flagAllocated: boolean = false;
  //   let toastrmsg: string = "";
  //   if (categoryIds.length > 0) {
  //     var labourstemp: any[] = []
  //     categoryIds.forEach(x => {
  //       let projectIDTemp: any;
  //       if (x.project != null && x.project != undefined && x.project != "") {
  //         projectIDTemp = x.project.id;
  //         projectExistCount = projectExistCount + 1;
  //       }
  //       else {
  //         projectIDTemp = null;
  //         projectNoExistCount = projectNoExistCount + 1;
  //       }
  //       if (x.labour_status.id == 5) {
  //         flagAvailable = true;
  //       }
  //       if (x.labour_status.id == 4) {
  //         flagAllocated = true;
  //       }
  //       labourstemp.push({ labour_id: x.id, lab_contractor_id: x.lab_contractor.id, project_id: projectIDTemp, sub_activity_id: x.sub_activity.id });

  //     });
  //     let laboursBig: any = { labours: labourstemp };
  //     this.progress.show();
  //     this.httpServices.request("post", "labours/make-as-available", "", "", laboursBig).subscribe((data) => {
  //     }, error => {
  //       this.progress.hide();
  //       console.log(error);
  //     });

  //   }
  //   else {
  //     toastr.error("Please Select Atleast One Record to Delete.");
  //   }
  // }

  //To check all checkbox and delete all
  // onCheckAll()
  // {
  //   for(let i=0;i< this.gigs_categoryList.length;i++)
  //   {
  //     this.gigs_categoryList.CHECKBOX=true;
  //   }
  // }

  //Kendo Grid Load Data Method
  private loadGig_categoryData(recordCount: number) {
    
    if (this.gigs_categoryList.length > 0) {
      this.gigs_categoryGriddata = {
        data: orderBy(this.gigs_categoryList, this.sort).slice(this.skip, this.skip + this.pageSize),
        //In case if gig list page has 40 record....SLICE to split record to next position as Record to be shown for Page is 10.
        //So remaining records are sliced to skip btn press.
        total: recordCount
      };
    } else {
      this.gigs_categoryGriddata = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortGigs_categoryChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadGig_categoryData(this.gigs_categoryList.count);
  }

  // Kendo Grid - data state change For All Gig List
  public gigs_categoryDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.gigs_categoryGriddata = process(this.gigs_categoryList, this.state);
  }


  //To get Particular Category On Basis os ID
  getCategory_onEdit(id: number) {
    this.category_Id = id;
    this.httpServices.request('get', 'categories/' + id, '', '', null).subscribe((data) => {
      this.categoryEdit = data;
      //this.loadGig_categoryData(data.length);
      //toastr.success("Data Fetched Successfully");

      this.category_Name = data.category_name;
      this.tagLine = data.tagline;
      //this.status=data.is_active;

      if (data.is_active == true) {
        this.status = 1;
      }
      else {
        this.status = 0;
      }
    }, error => {
      console.log(error);
    });
  }

  //On Search Btn 
  onSearchBtn() {
    this.progress.show();
    //this.searchCategory_name.trim.toString();
    var url = "categories/?page=1&page_size=" + this.pageSize;
    if (this.searchCategory_name != null && this.searchCategory_name != '' && this.searchCategory_name != undefined &&
      this.getStatus.id != null && this.getStatus.id != 2 && this.getStatus.id != undefined) {
      url = url + "&category_name=" + this.searchCategory_name + "&" + "is_active=" + this.getStatus.id;
    }
    else if (this.searchCategory_name != null && this.searchCategory_name != '' && this.searchCategory_name != undefined &&
      (this.getStatus.id == null || this.getStatus.id == 2 || this.getStatus.id == undefined)) {
      url = url + "&category_name=" + this.searchCategory_name;
    }
    else if (this.getStatus.id != null && this.getStatus.id != 2 && this.getStatus.id != undefined &&
      (this.searchCategory_name == null || this.searchCategory_name == '' || this.searchCategory_name == undefined)) {
      if (this.getStatus.id == 1) {
        url = url + "&is_active=true";
      }
      else {
        url = url + "&is_active=false";
      }

    }
    this.httpServices.request('get', url, '', '', null).subscribe((data) => {
      this.gigs_categoryList = data.results;
      this.loadGig_categoryData(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }


  
  pageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    //var url = "categories/?page=1&page_size=" + this.pageSize;
    var url = "categories/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    if (this.searchCategory_name != null && this.searchCategory_name != '' && this.searchCategory_name != undefined &&
      this.getStatus.id != null && this.getStatus.id != 2 && this.getStatus.id != undefined) {
      url = url + "&category_name=" + this.searchCategory_name + "&" + "is_active=" + this.getStatus.id;
    }
    else if (this.searchCategory_name != null && this.searchCategory_name != '' && this.searchCategory_name != undefined &&
      (this.getStatus.id == null || this.getStatus.id == 2 || this.getStatus.id == undefined)) {
      url = url + "&category_name=" + this.searchCategory_name;
    }
    else if (this.getStatus.id != null && this.getStatus.id != 2 && this.getStatus.id != undefined &&
      (this.searchCategory_name == null || this.searchCategory_name == '' || this.searchCategory_name == undefined)) {
      if (this.getStatus.id == 1) {
        url = url + "&is_active=true";
      }
      else {
        url = url + "&is_active=false";
      }     
    }
    
    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.gigs_categoryList = data.results;
      this.loadGig_categoryData(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }


  AddcategoryPopoup() {
    jQuery('#Add_Category').modal({ backdrop: 'static', keyboard: false });
    this.checkvalidation = false;
    this.addClick=true;
    this.openshowEditpop = true;
    this.category_Name = '';
    this.tagLine = '';
  }


  onAdd_categoryBtn() {
    this.checkvalidation = true;
    let add_Category: any = {

      category_name: '',
      tagline: ''
    }

    add_Category.category_name = this.category_Name;
    add_Category.tagline = this.tagLine;

    if (add_Category.category_name != null && add_Category.category_name != '' && add_Category.category_name != undefined
      && add_Category.tagline != null && add_Category.tagline != '' && add_Category.tagline != undefined) {
      if (this.category_Id == 0 || this.category_Id == undefined) {
        this.progress.show();
    
        this.httpServices.request('post', 'categories/', '', '', add_Category).subscribe((data) => {
          //this.openshowEditpop = false;
          toastr.success("Category Added Successfully");
          jQuery('#Add_Category').modal('hide');
          this.getGigs_category();
          this.progress.hide();
        }, error => {
          console.log(error);
          jQuery('#Add_Category').modal('hide');
          this.progress.hide();
        });

      } else {
        //this.progress.show();
        this.httpServices.request("patch", 'categories/' + this.category_Id, '', '', add_Category).subscribe((data) => {
          //this.openshowEditpop = false;
          toastr.success("Category Updated Successfully");
          jQuery('#Add_Category').modal('hide');
          this.getGigs_category();
          this.progress.hide();
        }, error => {
          console.log(error);
          jQuery('#Add_Category').modal('hide');
          this.progress.hide();
        });
      }
    }
  }



  //On Update For Add Category Popup  
  onUpdate_categoryPencilBtn(id: number) {
    jQuery('#Add_Category').modal({ backdrop: 'static', keyboard: false });
    this.addClick=false;
    this.openshowEditpop = true;
    this.getCategory_onEdit(id);
  }

  //After Popup to Update Action
  // onEdit_Category(){
  //    
  // let category_Update:any={ 
  //     category_name: '',
  //     tagline: ''   
  // }  

  // category_Update.category_name=this.category_Name;
  // category_Update.tagline=this.tagLine;

  //   this.httpServices.request("patch",'categories/'+ this.editUserId,'','',category_Update).subscribe((data)=>{
  //     toastr.success("Category Updated Successfully");
  //     this.openshowEditpop=false;
  //     //this.getGigs_category();
  //   },error=>{
  //     console.log(error);
  //   });
  // }


  onCancel_categoryPop() {
    this.openshowEditpop = false;
  }

  //On Delete Bin Button
  onDelete_singleCategory(id: number) {
    this.openshowDeletepop = true;
    this.deleteCategoryId = id;
  }

  //Delete Confirm Box Click
  onDeletepopBtn() {
    this.progress.show();
    this.httpServices.request('delete', 'categories/' + this.deleteCategoryId, '', '', null).subscribe((data) => {
      toastr.success("Category Deleted Successfully");
      jQuery('#delete_record').modal('hide');
      this.openshowDeletepop = false;
      this.getGigs_category();
      this.progress.hide();
    }, error => {
      console.log(error);
      jQuery('#delete_record').modal('hide');
      this.progress.hide();
    });
  }


  // ====================================
  // on click of clear all 
  // ====================================
  onClearAllClick() {
    this.getStatus.id = 2;
    this.searchCategory_name = "";
    this.getGigs_category();
  }

  //On Multple Select of Checkbox Delete Function
  // onDelete_multipleCheckbox(){
    
  //   this.httpServices.request('patch','categories/delete','','',null).subscribe((data)=>{
  //     toastr.success("Records Deleted");
  //   },error=>{
  //     console.log(error);
  //   });
  // }

  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  
  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  // onlyNumbers(event: any) {
  //   var charCode = (event.which) ? event.which : event.keyCode
  //   if ((charCode >= 48 && charCode <= 57))
  //     return true;
  //   return false;
  // }
}
