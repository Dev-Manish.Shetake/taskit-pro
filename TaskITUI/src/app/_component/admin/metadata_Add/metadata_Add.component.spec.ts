/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Metadata_AddComponent } from './metadata_Add.component';

describe('Metadata_AddComponent', () => {
  let component: Metadata_AddComponent;
  let fixture: ComponentFixture<Metadata_AddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Metadata_AddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Metadata_AddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
