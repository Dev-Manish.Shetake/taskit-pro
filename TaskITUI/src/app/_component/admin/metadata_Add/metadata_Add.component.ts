import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { element } from 'protractor';
import { ICON_REGISTRY_PROVIDER } from '@angular/material/icon';
import { Router } from '@angular/router';
declare var toastr: any;
declare var jQuery: any;
@Component({
  selector: 'app-metadata_Add',
  templateUrl: './metadata_Add.component.html',
  styleUrls: ['./metadata_Add.component.css']
})
export class Metadata_AddComponent implements OnInit {
  public getCategory: any = { id: 0, category_name: "" }
  public getMetadataStyle: any = { id: 0, metadata_type_name: "" }
  public getMetadataStyle_onPop: any = { id: 0, metadata_type_name: "" }
  public checkcuscheckMetadataTypoeNewtomAdd: boolean = false;
  public catgory_list: any;
  public addMetadata_toList: any;
  public subCatgory_list: any;
  public metaDataTypeList: any;
  public editMode: boolean = false;
  public getsubCategory: any = { id: 0, sub_category_name: "" }
  public metadata_Style_list: metadata_Style_list = {
    result: [{
      id: 0,
      metadata_type: {
        id: 0,
        metadata_type_name: "",
        is_single: false,
        is_mandatory: false,
        is_suggest_other: false,
        is_active: false,
        created_date: "",
        modified_date: ""
      },
      seq_no: 0,
      metadata_type_name: "",
      checked_value: false
    }]
  };
  public metadata_type_forPop: any;
  public metaData_Id: any;
  public metadata_Style_list_forPop: metadataTypelist = {
    result: [

      {
        id: 0,
        metadata_type: {
          id: 0,
          metadata_type_name: "",
          is_single: false,
          is_mandatory: false,
          is_suggest_other: false,
          is_active: false,
          created_date: "",
          modified_date: ""
        },
        seq_no: 0,
        metadata_type_name: "",
        is_active: false,
        showTextBox: false,
        txt_metadata_type_name: "",
        is_new:false
      },

    ]
  }
  public meta_data_name: string;
  public metadata_Subtype_list: metaDataSubTypeList = {
    mataData:
      [{
        metaData_draft_id: 0,
        created_date: '',
        modified_date: '',
        id: 0,
        is_active: true,
        is_allowed_on_request: true,
        is_mandatory: false,
        is_single: false,
        is_suggest_other: false,
        is_suggest_other_checked_value: false,
        is_suggest_other_id: 0,
        is_suggest_other_text: '',
        max_value: 0,
        min_value: 0,
        seq_no: 0,
        metadata_type: {
          created_date: '',
          id: 0,
          is_active: true,
          is_mandatory: false,
          is_single: false,
          is_suggest_other: false,
          metadata_type_name: '',
          modified_date: ''
        },
        metadata_type_details: [{
          id: 0,
          metadata_type_dtl: {
            id: 0,
            is_active: true,
            metadata_type_name: '',
            seq_no: 0
          },
          seq_no: 0,
          checked_value: false
        }],
        sub_category: {
          category: {
            id: 0,
            category_name: '',
            is_active: true
          },
          id: 0,
          is_active: true,
          modified_date: '',
          seq_no: 0,
          sub_category_name: '',
          sub_category_picture: '',
          sub_category_thumb_picture: '',
          tagline: ''
        },
        is_new:false
      }]
  };
  public queryParam: any;
  public _queryParams: any;
  public subCategoryId:any;
  constructor(private router: Router,private httpServices: HttpRequestService, private progress: NgxSpinnerService) { }

  ngOnInit() {
    if (this.queryParam == null) {
      this._queryParams = (sessionStorage.getItem("queryParamsMetadata") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      if( this.queryParam !=null &&  this.queryParam !=undefined && this._queryParams!="{}")
      {
        this.editMode=this.queryParam.subCategoryId>0?true:false;
        if(this.editMode==true)
        {
        this.subCategoryId = this.queryParam.subCategoryId;
        this.getCategory_list();
        }
        else{
          this.getCategory_list();
        }
      }
      else{
        this.getCategory_list();
    }
    }
    else{
        this.getCategory_list();
    }
    //this.onMetadata_AddPlusBtn();
  }

  //*******************************
  //Category List for Search.
  //*******************************
  getCategory_list() {
    this.httpServices.request("get", "categories/list", "", "", null).subscribe((data) => {
      this.catgory_list = data;
      if(this.editMode==true)
      {
        this.getCategory.id=this.queryParam.categoryId;
        this.getCategory.category_name=this.queryParam.category_Name;
        this.getsubCategory_list(this.getCategory.id);
      }
    }, error => {
      console.log(error);
    });
  }

  //*******************************
  //SubCategory List for Search.
  //*******************************
  getsubCategory_list(categoryID: number) {
    if (this.getCategory.id > 0) {
      this.progress.show();
      this.catgory_list.forEach(element => {
        if(element.id==this.getCategory.id)
        {
          this.getCategory.category_name=element.category_name;
        }
      });
      this.httpServices.request('get', 'subcategories/list?category_id=' + categoryID, null, null, null)
        .subscribe(data => {
          if (data.length > 0) {
            this.subCatgory_list = data;
            if(this.editMode==true)
            {
              this.getsubCategory.id=this.queryParam.subCategoryId;
              this.onSubCategoryChange();
            }
          }
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });
    }
  }
  // =========================================
  // get metadatatype list 
  // ==========================================
  getMetaDataTypeList() {
    this.httpServices.request('get', 'metadata_types', null, null, null)
      .subscribe(data => {
        this.metaDataTypeList = data;
      });
  }
  public allowMultiple: boolean = false;
  public is_mandatory: boolean = false;
  public allow_suggest: boolean = false;
  onMetaDataTypeChange() {
    if (this.getMetadataStyle.id > 0) {
      this.progress.show();
      this.httpServices.request('get', 'metadata_types/details?metadata_type_id=' + this.getMetadataStyle.id, null, null, null)
        .subscribe(data => {
          if (data.length > 0) {
            this.metadata_Style_list.result = data;
            this.showMetaDataDetails = true;
            for (let i = 0; i < this.metadata_Style_list.result.length; i++) {
              this.metadata_Style_list.result[i].checked_value = false;
              if (this.metadata_Style_list.result[i].metadata_type.is_single == true) {
                this.allowMultiple = false;
              }
              else {
                this.allowMultiple = true;
              }
              if (this.metadata_Style_list.result[i].metadata_type.is_mandatory == true) {
                this.is_mandatory = true;
              }
              else {
                this.is_mandatory = false;
              }
              if (this.metadata_Style_list.result[i].metadata_type.is_suggest_other == true) {
                this.allow_suggest = true;
              }
              else {
                this.allow_suggest = false;
              }
            }
          }
          this.progress.hide();
        });
    }
  }
  // *************************************
  // On sub category change 
  // **************************************
  public styleExist: boolean = false;
  public showMetaDataDetails: boolean = false;
  // onSubCategoryChange()
  // {
  //   if(this.getsubCategory.id > 0)
  //   {
  //   this.getMetaDataTypeList();
  //   }
  // }
  public emptyMetadataType:boolean=false;
  onSubCategoryChange() {

    if (this.getsubCategory.id > 0) {
      this.metadata_Subtype_list.mataData = null;
      this.progress.show();
      this.subCatgory_list.forEach(element => {
        if(element.id==this.getsubCategory.id)
        {
          this.getsubCategory.sub_category_name=element.sub_category_name;
        }
      });
      this.httpServices.request('get', 'subcategories/metadata-type-list?sub_category_id=' + this.getsubCategory.id, null, null, null)
        .subscribe(data => {
          sessionStorage.setItem("queryParams", null);
          this.metadata_Subtype_list.mataData = data;
          if (this.metadata_Subtype_list.mataData != null && this.metadata_Subtype_list.mataData != undefined && this.metadata_Subtype_list.mataData.length > 0) {
            this.showMetaDataDetails = true;
            this.metadata_Subtype_list.mataData.forEach(x => {
              x.is_new=false;
              x.metadata_type_details.forEach(element => {
                element.checked_value = false;
              });
            });
            this.getMetadataStyle.id = this.metadata_Subtype_list.mataData[0].metadata_type.id;
            this.onMetaDataTypeChange();
            this.emptyMetadataType=false;
          }
          else {
            this.showMetaDataDetails = false;
            this.emptyMetadataType=true;
          }
          this.progress.hide();
         
        }, error => {
          this.progress.hide();
          console.log(error);
        });
    }
  }
  public active_metadata_type_id: number = 0;
  onMetaTypeClick(metadataTypeId: any) {
    this.active_metadata_type_id = metadataTypeId;
  }
  //Calling metadata types list on Add Metadata Btn
  public addplusButton: boolean = false;
  public metadataType_newlyadded: any;
  onMetadata_AddPlusBtn() {
    this.addplusButton == true;
    this.getMetadataStyle_onPop.id=0;
    this.httpServices.request("get", "metadata_types/", "", "", null).subscribe((data) => {
      this.metadata_type_forPop = data;
      jQuery('#add_metadata_pop').modal({ backdrop: 'static', keyboard: false });
    }, error => {
      console.log(error);
    });
  }
  onEditMetadataTypeClick(metadataTypeId: any) {
    this.httpServices.request("get", "metadata_types/", "", "", null).subscribe((data) => {
      this.metadata_type_forPop = data;
      this.getMetadataStyle_onPop.id = metadataTypeId;
      this.onChangeMetadata_Type();
      jQuery('#add_metadata_pop').modal({ backdrop: 'static', keyboard: false });
    }, error => {
      console.log(error);
    });
  }
  onCloseAddMetadataPop()
  {
    this.PopallowMultiple=false;
    this.PopIsmandatory=false;
    this.PopIssuggest=false;
    this.metadata_Style_list_forPop.result=null;
  }
  public removeMetadataTypeId: any;
  onRemoveMetadataTypeClick(SubCategorymetadataTypeId: any) {
    this.removeMetadataTypeId = SubCategorymetadataTypeId;
    jQuery('#delete_record_MetadataType').modal({ backdrop: 'static', keyboard: false });
  }
  onConfirmRemoveMetadataTypeClick() {
    this.progress.show();
    this.httpServices.request("delete", "subcategories_metadata/delete/" + this.removeMetadataTypeId, null, null, null).subscribe((data) => {
      toastr.success("Deleted Successfully.");
      jQuery('#delete_record_MetadataType').modal("hide");
      this.onSubCategoryChange();
      this.progress.hide();
    }, error => {
      console.log(error);
    });
  }
  //To open Style list value in Textbox for Editing it.
  onPencilBtn_openStyleTextbox(metadata_id: number) {
    this.metadata_Style_list_forPop.result.forEach(element => {
      if (element.id == metadata_id) {
        element.showTextBox = true;
        element.txt_metadata_type_name = element.metadata_type_name;
      }
      else {
        element.showTextBox = false;
      }
    });
  }
  public txtNew_metadata_type_name: any;
  public showDuplicacyMsg: boolean = false;
  addNewsubType() {
    if (this.metadata_Style_list_forPop.result.filter(element => element.metadata_type_name == this.txtNew_metadata_type_name).length > 0) {
      this.showDuplicacyMsg = true;
    }
    else {
      this.showDuplicacyMsg = false;

      let metadata_type: any = {
        id: 0,
        metadata_type_name: "",
        is_single: false,
        is_mandatory: false,
        is_suggest_other: false,
        is_active: false,
        created_date: "",
        modified_date: ""
      }
      if (this.metadata_Style_list_forPop.result.length > 0) {
        let seq_No: number;
        seq_No = Math.max.apply(Math, this.metadata_Style_list_forPop.result.map(function (o) { return o.seq_no; }));
        metadata_type.id = this.metadata_Style_list_forPop.result[0].metadata_type.id;
        metadata_type.metadata_type_name = this.metadata_Style_list_forPop.result[0].metadata_type_name;
        metadata_type.is_single = this.metadata_Style_list_forPop.result[0].metadata_type.is_single;
        metadata_type.is_mandatory = this.metadata_Style_list_forPop.result[0].metadata_type.is_mandatory;
        metadata_type.is_suggest_other = this.metadata_Style_list_forPop.result[0].metadata_type.is_suggest_other;
        metadata_type.is_active = true;
        metadata_type.created_date = "";
        metadata_type.modified_date = "";
        this.metadata_Style_list_forPop.result.push({
          id: 0, metadata_type: metadata_type, seq_no: seq_No + 1, metadata_type_name: this.txtNew_metadata_type_name,
          is_active: true, showTextBox: false, txt_metadata_type_name: "",is_new:true
        });
        this.txtNew_metadata_type_name = "";
      }
      else {
        metadata_type.id = this.getMetadataStyle_onPop.id;
        metadata_type.metadata_type_name = this.getMetadataStyle_onPop.metadata_type_name;
        metadata_type.is_single = this.PopallowMultiple == true ? false : true;
        metadata_type.is_mandatory = this.PopIsmandatory == true ? true : false;
        metadata_type.is_suggest_other = this.PopIssuggest == true ? true : false;
        metadata_type.is_active = true;
        metadata_type.created_date = "";
        metadata_type.modified_date = "";
        this.metadata_Style_list_forPop.result.push({
          id: 0, metadata_type: metadata_type, seq_no: 1, metadata_type_name: this.txtNew_metadata_type_name,
          is_active: true, showTextBox: false, txt_metadata_type_name: "",is_new:true
        });
        this.txtNew_metadata_type_name = "";
      }
    }
  }
  //To  correctOption list value which appears in textbox below for edit
  public showDuplicacymsgEdit: boolean = false;
  onEditoptionList_metadata(metadata_id: any, updatedName: any) {
    if (this.metadata_Style_list_forPop.result.filter(element => element.metadata_type_name == updatedName).length > 0) {
      this.showDuplicacymsgEdit = true;
    }
    else {
      this.showDuplicacymsgEdit = false;
      this.metadata_Style_list_forPop.result.forEach(element => {
        if (element.id == metadata_id) {
          element.showTextBox = false;
          element.metadata_type_name = element.txt_metadata_type_name;
        }
      });
    }
  }
  onClickUndo(metadata_id: any) {
    this.metadata_Style_list_forPop.result.forEach(element => {
      if (element.id == metadata_id) {
        element.showTextBox = false;
        element.txt_metadata_type_name = "";
      }
    });
  }
  public metadataTypeDtlId: any;
  public metadataTypeDtlSeqNo: any;
  onDeleteMetaType(metadataTypeDtlId: any, seq_no: any) {
    this.metadataTypeDtlId = metadataTypeDtlId;
    this.metadataTypeDtlSeqNo = seq_no;
    jQuery('#delete_record').modal({ backdrop: 'static', keyboard: false });
  }
  onConfirmDeleteMetaType() {
    jQuery('#delete_record').modal("hide");
    if (this.metadataTypeDtlId > 0) {
      this.httpServices.request("delete", "metadata_types/details/delete/"+this.metadataTypeDtlId, null, null, null).subscribe((data) => {
      this.metadata_Style_list_forPop.result.splice(this.metadata_Style_list_forPop.result.findIndex(element => element.id == this.metadataTypeDtlId), 1);
      for (let i = 0; i < this.metadata_Style_list_forPop.result.length; i++) {
        if (this.metadata_Style_list_forPop.result[i].seq_no < this.metadataTypeDtlSeqNo) {

        }
        else if (this.metadata_Style_list_forPop.result[i].seq_no > this.metadataTypeDtlSeqNo) {
          this.metadata_Style_list_forPop.result[i].seq_no = this.metadata_Style_list_forPop.result[i].seq_no - 1;
        }
      }
      }, error => {
        this.progress.hide();
        console.log(error);
      });
    }
    else if (this.metadataTypeDtlSeqNo > 0) {
      this.metadata_Style_list_forPop.result.splice(this.metadata_Style_list_forPop.result.findIndex(element => element.seq_no == this.metadataTypeDtlSeqNo), 1);
      for (let i = 0; i < this.metadata_Style_list_forPop.result.length; i++) {
        if (this.metadata_Style_list_forPop.result[i].seq_no < this.metadataTypeDtlSeqNo) {

        }
        else if (this.metadata_Style_list_forPop.result[i].seq_no > this.metadataTypeDtlSeqNo) {
          this.metadata_Style_list_forPop.result[i].seq_no = this.metadata_Style_list_forPop.result[i].seq_no - 1;
        }
      }
    }
  }
  //Method works onchange on Metadata Type dropdown below options list gets populated
  public PopallowMultiple: boolean = false;
  public PopIsmandatory: boolean = false;
  public PopIssuggest: boolean = false;
  onChangeMetadata_Type() {
    if (this.getMetadataStyle_onPop.id > 0) {
      this.progress.show();
      this.metadata_type_forPop.forEach(element => {
        if (element.id == this.getMetadataStyle_onPop.id) {
          this.getMetadataStyle_onPop.metadata_type_name = element.metadata_type_name;
          if (element.is_single == true) {
            this.PopallowMultiple = false;
          }
          else {
            this.PopallowMultiple = true;
          }
          if (element.is_mandatory == true) {
            this.PopIsmandatory = true;
          }
          else {
            this.PopIsmandatory = false;
          }
          if (element.is_suggest_other == true) {
            this.PopIssuggest = true;
          }
          else {
            this.PopIssuggest = false;
          }
        }

      });
      this.httpServices.request("get", "metadata_types/details?metadata_type_id=" +
        this.getMetadataStyle_onPop.id, "", "", null).subscribe((data) => {
          this.metadata_Style_list_forPop.result = data;
          this.metadata_Style_list_forPop.result.forEach(element => {
            element.showTextBox = false;
            element.is_new=false;
            element.txt_metadata_type_name = element.metadata_type_name;
           
          });
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });
    }
  }
  public metadata_type_newlyadded: any;
  public checkMetadataTypoeNew: boolean = false;
  public allowMultiple_addMetadata: boolean = false;
  public ismandatory_addmetadata: boolean = false;
  public issuggest_addmetadata: boolean = false;
  public showDuplicacyAddMetadataType: boolean = false;
  openAddMetadataPop() {
    this.metadata_type_newlyadded = "";
    jQuery('#addEdit_metadata_new').modal({ backdrop: 'static', keyboard: false });
  }
  openEditMetadataPop() {
    this.metadata_type_newlyadded = this.getMetadataStyle_onPop.metadata_type_name;
    jQuery('#addEdit_metadata_new').modal({ backdrop: 'static', keyboard: false });
  }
  cancelnewmetadataTypePop() {
    jQuery('#addEdit_metadata_new').modal("hide");
  }
  addmetadatatypeClick() {
    this.checkMetadataTypoeNew = true;
    if (this.metadata_type_newlyadded != null && this.metadata_type_newlyadded != undefined && this.metadata_type_newlyadded.toString().trim() != "") {
      if (this.metadata_type_forPop.filter(element => element.metadata_type_name == this.metadata_type_newlyadded).length > 0) {
        this.showDuplicacyAddMetadataType = true;
      }
      else {
        this.showDuplicacyAddMetadataType = false;
        let addmetadataBody: any = {
          metadata_type_name: "",
          is_single: true,
          is_mandatory: true,
          is_suggest_other: true
        }
        addmetadataBody.metadata_type_name = this.metadata_type_newlyadded;
        addmetadataBody.is_single = this.allowMultiple_addMetadata == true ? false : true;
        addmetadataBody.is_mandatory = this.ismandatory_addmetadata == true ? true : false;
        addmetadataBody.is_suggest_other = this.issuggest_addmetadata == true ? true : false;
        this.progress.show();
        this.httpServices.request("post", "metadata_types/", null, null, addmetadataBody).subscribe((response) => {
          this.httpServices.request("get", "metadata_types/", "", "", null).subscribe((data) => {
            this.metadata_type_forPop = data;
            this.checkMetadataTypoeNew = false;
            jQuery('#addEdit_metadata_new').modal("hide");
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }, error => {
          console.log(error);
          this.progress.hide();
        });
      }
    }
  }
  updatemetadatatypeClick() {
    this.checkMetadataTypoeNew = true;
    if (this.metadata_type_newlyadded != null && this.metadata_type_newlyadded != undefined && this.metadata_type_newlyadded.toString().trim() != "") {
      if (this.metadata_type_forPop.filter(element => element.metadata_type_name == this.metadata_type_newlyadded).length > 0) {
        this.showDuplicacyAddMetadataType = true;
      }
      else {
        this.showDuplicacyAddMetadataType = false;
        let addmetadataBody: any = {
          metadata_type_name: "",
          is_single: true,
          is_mandatory: true,
          is_suggest_other: true
        }
        addmetadataBody.metadata_type_name = this.metadata_type_newlyadded;
        addmetadataBody.is_single = this.allowMultiple_addMetadata == true ? false : true;
        addmetadataBody.is_mandatory = this.ismandatory_addmetadata == true ? true : false;
        addmetadataBody.is_suggest_other = this.issuggest_addmetadata == true ? true : false;
        this.progress.show();
        this.httpServices.request("patch", "metadata_types/" + this.getMetadataStyle_onPop.id, null, null, addmetadataBody).subscribe((response) => {
          this.httpServices.request("get", "metadata_types/", "", "", null).subscribe((data) => {
            this.metadata_type_forPop = data;
            this.checkMetadataTypoeNew = false;
            jQuery('#addEdit_metadata_new').modal("hide");
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }, error => {
          console.log(error);
          this.progress.hide();
        });
      }
    }
  }
  submitAndUpdateMasterClick() {
    let metadataUpdateMasterBody: any =
    {
      metadata_type_id: 0,
      details: [
        {
          id: 0,
          seq_no: 0,
          metadata_type_name: ""
        }
      ]
    }
    let detailsBody: any = [
      {
        id: 0,
        seq_no: 0,
        metadata_type_name: ""
      }
    ]
    this.metadata_Style_list_forPop.result.forEach(element => {
      detailsBody.push({ id: element.id, seq_no: element.seq_no, metadata_type_name: element.metadata_type_name });
    });
    detailsBody.splice(detailsBody.findIndex(element=>element.metadata_type_name==""),1);
    metadataUpdateMasterBody.metadata_type_id = this.getMetadataStyle_onPop.id;
    metadataUpdateMasterBody.details = detailsBody;
    this.progress.show();
    this.httpServices.request("patch", "metadata_types/details-update", null, null, metadataUpdateMasterBody).subscribe((data) => {
      toastr.success("Master updated successfully.");
      jQuery('#add_metadata_pop').modal("hide");
      this.httpServices.request("get", "metadata_types/details?metadata_type_id=" +
        this.getMetadataStyle_onPop.id, "", "", null).subscribe((response) => {
          this.metadata_Style_list_forPop.result = response;
     if( this.metadata_Subtype_list.mataData.length>0)
     {
       if(this.metadata_Subtype_list.mataData.filter(element=>element.metadata_type.id==this.getMetadataStyle_onPop.id).length>0)
       {
        //  for(let i=0;i<this.metadata_Subtype_list.mataData.length;i++)
        //  {
        //    if(this.metadata_Subtype_list.mataData[i].metadata_type.id==this.getMetadataStyle_onPop.id)
        //    {
        //     this.metadata_Subtype_list.mataData[i].is_mandatory=this.PopIsmandatory==true?true:false;
        //     this.metadata_Subtype_list.mataData[i].is_single=this.PopallowMultiple==true?false:true;
        //     this.metadata_Subtype_list.mataData[i].is_suggest_other=this.PopIssuggest==true?true:false;
        //     this.metadata_Subtype_list.mataData[i].metadata_type.metadata_type_name=this.getMetadataStyle_onPop.metadata_type_name;
        //     for(let k=0;k<this.metadata_Subtype_list.mataData[i].metadata_type_details.length;k++)
        //     {
        //       if(this.metadata_Style_list_forPop.result.filter(element=>element.id==this.metadata_Subtype_list.mataData[i].metadata_type_details[k].metadata_type_dtl.id).length>0)
        //       {
        //         this.metadata_Style_list_forPop.result.forEach(x=>{
        //           if(x.id==this.metadata_Subtype_list.mataData[i].metadata_type_details[k].metadata_type_dtl.id)
        //           {
        //             this.metadata_Subtype_list.mataData[i].metadata_type_details[k].metadata_type_dtl.metadata_type_name=x.metadata_type_name;
        //             this.metadata_Subtype_list.mataData[i].metadata_type_details[k].metadata_type_dtl.seq_no=x.seq_no;
        //           }
        //         })
        //       }
        //       else if(this.metadata_Style_list_forPop.result.filter(element=>element.is_new==true).length>0)
        //       {
        //         data.details.forEach(element => {
        //           let metadata_type_dtl:any={
        //             id: element.id,
        //             seq_no: element.seq_no,
        //             metadata_type_name: element.metadata_type_name
        //           }
        //         this.metadata_Subtype_list.mataData[i].metadata_type_details.push({id:0,metadata_type_dtl:metadata_type_dtl,seq_no:0,checked_value:true})
        //         });
        //       }
        //       else {
        //         this.metadata_Subtype_list.mataData[i].metadata_type_details.splice(this.metadata_Subtype_list.mataData[i].metadata_type_details.findIndex(element=>element.metadata_type_dtl.id==this.metadata_Subtype_list.mataData[i].metadata_type_details[k].metadata_type_dtl.id),1) ;
        //       }
        //     }
        //    }
        //  }
        let seqNOData=this.metadata_Subtype_list.mataData.filter(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id);
        let seq_no=seqNOData[0].seq_no;
        //this.metadata_Subtype_list.mataData.splice(this.metadata_Subtype_list.mataData.findIndex(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id),1);
        let popUpArray=this.metadata_Style_list_forPop.result;
        for(let i=0;i<popUpArray.length;i++)
        {
          if(seqNOData[0].metadata_type_details.filter(element=>element.metadata_type_dtl.id==popUpArray[i].id).length>0)
          {
            this.metadata_Subtype_list.mataData.filter(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id)[0].metadata_type_details.forEach(element => {
              if(element.metadata_type_dtl.id==popUpArray[i].id)
              {
                element.metadata_type_dtl.metadata_type_name=popUpArray[i].metadata_type_name;
              }
            });
          }
          else{
            let metadata_type_dtl:any={
              id:popUpArray[i].id,
              metadata_type_name:popUpArray[i].metadata_type_name,
              seq_no:popUpArray[i].seq_no
            }
            this.metadata_Subtype_list.mataData.filter(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id)[0].metadata_type_details.push({
              id:0,metadata_type_dtl:metadata_type_dtl,seq_no:popUpArray[i].seq_no,checked_value:false
            })
          }
        }
        let IdsForSplice:any=[{Id:0}];
        for(let k=0;k<this.metadata_Subtype_list.mataData.filter(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id)[0].metadata_type_details.length;k++)
        {
          if(popUpArray.filter(element=>element.id==this.metadata_Subtype_list.mataData.filter(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id)[0].metadata_type_details[k].metadata_type_dtl.id).length>0)
          {

          }
          else{
            IdsForSplice.push({Id:this.metadata_Subtype_list.mataData.filter(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id)[0].metadata_type_details[k].metadata_type_dtl.id});
          }
        }
        IdsForSplice.splice(IdsForSplice.findIndex(x=>x.Id==0),1);
        for(let j=0;j<IdsForSplice.length;j++)
        {
          this.metadata_Subtype_list.mataData.filter(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id)[0].metadata_type_details.splice(this.metadata_Subtype_list.mataData.filter(x=>x.metadata_type.id==this.getMetadataStyle_onPop.id)[0].metadata_type_details.findIndex(x=>x.metadata_type_dtl.id==IdsForSplice[j].Id),1);
        }
        this.getMetadataStyle.id=this.getMetadataStyle_onPop.id;
        this.onMetaDataTypeChange();
      //   let category:any={
      //     id: this.getCategory.id,
      //     category_name: this.getCategory.category_name,
      //     is_active: true
      //   }
      //   let subCategory:any={
      //       category: category,
      //       id: this.getsubCategory.id,
      //       is_active: true,
      //       modified_date: "",
      //       seq_no: 0,
      //       sub_category_name: this.getsubCategory.category_name,
      //       sub_category_picture: "",
      //       sub_category_thumb_picture: "",
      //       tagline: ""
      //   }
      //   let metadata_type:any= {
      //     created_date: "",
      //     id: this.getMetadataStyle_onPop.id,
      //     is_active: true,
      //     is_mandatory: this.PopIsmandatory==true?true:false,
      //     is_single: this.PopallowMultiple==true?false:true,
      //     is_suggest_other: this.PopIssuggest==true?true:false,
      //     metadata_type_name: this.getMetadataStyle_onPop.metadata_type_name,
      //     modified_date: null
      //   }
      //   let metadata_type_details:any=[{
      //   id: 0,
      //   metadata_type_dtl: {
      //     id: 0,
      //     seq_no: 0,
      //     metadata_type_name: "",
      //     is_active:true
      //   }
      // }]
      // for(let i=0;i<this.metadata_Style_list_forPop.result.length;i++)
      // {
      //   let metadata_type_dtl_Temp:any={
      //     id:this.metadata_Style_list_forPop.result[i].id,
      //     seq_no: this.metadata_Style_list_forPop.result[i].seq_no,
      //     metadata_type_name:this.metadata_Style_list_forPop.result[i].metadata_type_name
      //   }
      
      //   metadata_type_details.push({id:0,seq_no:this.metadata_Style_list_forPop.result[i].seq_no,metadata_type_dtl:metadata_type_dtl_Temp});
      // }
      // metadata_type_details.splice(metadata_type_details.findIndex(element=>element.metadata_type_dtl.metadata_type_name==""),1);
      //   this.metadata_Subtype_list.mataData.push({metaData_draft_id:0,
      //     created_date:"",
      //     modified_date:"",
      //     id:this.getMetadataStyle_onPop.id, 
      //     is_active:true,
      //     is_allowed_on_request:false,
      //   is_mandatory:this.PopIsmandatory==true?true:false,
      //   is_single:this.PopallowMultiple==true?false:true,
      //   is_suggest_other:this.PopIssuggest==true?true:false,
      //   is_suggest_other_checked_value:true,
      //   is_suggest_other_id:0,
      //   is_suggest_other_text:"",
      //   max_value:null,
      //   min_value:null,
      //   seq_no:seq_no+1,
      //   metadata_type:metadata_type,
      //   metadata_type_details:metadata_type_details, 
      //   sub_category:subCategory,is_new:false});
       }
        //this.metadata_Subtype_list.mataData.splice(this.metadata_Subtype_list.mataData.findIndex(x=>x.id==this.getMetadataStyle_onPop.id));
        else{
        let category:any={
          id: this.getCategory.id,
          category_name: this.getCategory.category_name,
          is_active: true
        }
        let subCategory:any={
            category: category,
            id: this.getsubCategory.id,
            is_active: true,
            modified_date: "",
            seq_no: 0,
            sub_category_name: this.getsubCategory.category_name,
            sub_category_picture: "",
            sub_category_thumb_picture: "",
            tagline: ""
        }
        let metadata_type:any= {
          created_date: "",
          id: this.getMetadataStyle_onPop.id,
          is_active: true,
          is_mandatory: this.PopIsmandatory==true?true:false,
          is_single: this.PopallowMultiple==true?false:true,
          is_suggest_other: this.PopIssuggest==true?true:false,
          metadata_type_name: this.getMetadataStyle_onPop.metadata_type_name,
          modified_date: null
        }
        let metadata_type_details:any=[{
        id: 0,
        seq_no:0,
        metadata_type_dtl: {
          id: 0,
          seq_no: 0,
          metadata_type_name: "",
          is_active:true
        }
      }]
      for(let i=0;i<this.metadata_Style_list_forPop.result.length;i++)
      {
        let metadata_type_dtl_Temp:any={
          id:this.metadata_Style_list_forPop.result[i].id,
          seq_no: this.metadata_Style_list_forPop.result[i].seq_no,
          metadata_type_name:this.metadata_Style_list_forPop.result[i].metadata_type_name
        }
        // metadata_type_dtl_Temp.id=this.metadata_Style_list_forPop.result[i].id;
        // metadata_type_dtl_Temp.seq_no=this.metadata_Style_list_forPop.result[i].seq_no;
        // metadata_type_dtl_Temp.metadata_type_name=this.metadata_Style_list_forPop.result[i].metadata_type_name;
        metadata_type_details.push({id:0,seq_no:this.metadata_Style_list_forPop.result[i].seq_no,metadata_type_dtl:metadata_type_dtl_Temp});
      }
      metadata_type_details.splice(metadata_type_details.findIndex(element=>element.metadata_type_dtl.metadata_type_name==""),1);
      let seqNO=Math.max.apply(Math, this.metadata_Subtype_list.mataData.map(function (o) { return o.seq_no; }));
        this.metadata_Subtype_list.mataData.push({metaData_draft_id:0,
          created_date:"",
          modified_date:"",
          id:this.getMetadataStyle_onPop.id, 
          is_active:true,
          is_allowed_on_request:false,
        is_mandatory:this.PopIsmandatory==true?true:false,
        is_single:this.PopallowMultiple==true?false:true,
        is_suggest_other:this.PopIssuggest==true?true:false,
        is_suggest_other_checked_value:true,
        is_suggest_other_id:0,
        is_suggest_other_text:"",
        max_value:null,
        min_value:null,
        seq_no:seqNO+1,
        metadata_type:metadata_type,
        metadata_type_details:metadata_type_details, 
        sub_category:subCategory,is_new:true});
        this.getMetadataStyle.id=this.getMetadataStyle_onPop.id;
        this.onMetaDataTypeChange();
       }
      }
      else{
        let category:any={
          id: this.getCategory.id,
          category_name: this.getCategory.category_name,
          is_active: true
        }
        let subCategory:any={
            category: category,
            id: this.getsubCategory.id,
            is_active: true,
            modified_date: "",
            seq_no: 0,
            sub_category_name: this.getsubCategory.category_name,
            sub_category_picture: "",
            sub_category_thumb_picture: "",
            tagline: ""
        }
        let metadata_type:any= {
          created_date: "",
          id: this.getMetadataStyle_onPop.id,
          is_active: true,
          is_mandatory: this.PopIsmandatory==true?true:false,
          is_single: this.PopallowMultiple==true?false:true,
          is_suggest_other: this.PopIssuggest==true?true:false,
          metadata_type_name: this.getMetadataStyle_onPop.metadata_type_name,
          modified_date: null
        }
        let metadata_type_details:any=[{
        id: 0,
        seq_no:0,
        metadata_type_dtl: {
          id: 0,
          seq_no: 0,
          metadata_type_name: "",
          is_active:true
        }
      }]
      for(let i=0;i<this.metadata_Style_list_forPop.result.length;i++)
      {
        let metadata_type_dtl_Temp:any={
          id:this.metadata_Style_list_forPop.result[i].id,
          seq_no: this.metadata_Style_list_forPop.result[i].seq_no,
          metadata_type_name:this.metadata_Style_list_forPop.result[i].metadata_type_name
        }
        // metadata_type_dtl_Temp.id=this.metadata_Style_list_forPop.result[i].id;
        // metadata_type_dtl_Temp.seq_no=this.metadata_Style_list_forPop.result[i].seq_no;
        // metadata_type_dtl_Temp.metadata_type_name=this.metadata_Style_list_forPop.result[i].metadata_type_name;
        metadata_type_details.push({id:0,seq_no:this.metadata_Style_list_forPop.result[i].seq_no,metadata_type_dtl:metadata_type_dtl_Temp});
      }
      metadata_type_details.splice(metadata_type_details.findIndex(element=>element.metadata_type_dtl.metadata_type_name==""),1);
        this.metadata_Subtype_list.mataData.push({metaData_draft_id:0,
          created_date:"",
          modified_date:"",
          id:this.getMetadataStyle_onPop.id, 
          is_active:true,
          is_allowed_on_request:false,
        is_mandatory:this.PopIsmandatory==true?true:false,
        is_single:this.PopallowMultiple==true?false:true,
        is_suggest_other:this.PopIssuggest==true?true:false,
        is_suggest_other_checked_value:true,
        is_suggest_other_id:0,
        is_suggest_other_text:"",
        max_value:null,
        min_value:null,
        seq_no:1,
        metadata_type:metadata_type,
        metadata_type_details:metadata_type_details, 
        sub_category:subCategory,is_new:true});
       }
       this.getMetadataStyle.id=this.getMetadataStyle_onPop.id;
       this.onMetaDataTypeChange();
        this.progress.hide();
      }, error => {
        console.log(error);
      });
    }, error => {
      console.log(error);
    });

  }
  addMetadata() {
    let subCategoryMetadataBody: any =
    {
      sub_category_metadata_type: [
        {
          id: 0,
          sub_category_id: 0,
          seq_no: 0,
          metadata_type_id: 0,
          is_single: true,
          is_mandatory: true,
          is_suggest_other: true,
          details: [
            {
              id: 0,
              seq_no: 0,
              metadata_type_dtl_id: 0
            }
          ]
        }
      ]
    }
    if (this.metadata_Subtype_list.mataData != null && this.metadata_Subtype_list.mataData != undefined) {
      // let style_info = this.metadata_types_list.filter(element => element.id == 1);
      this.metadata_Subtype_list.mataData.forEach(x => {
        if (x.metadata_type_details != null && x.metadata_type_details != undefined) {
          // if (x.is_single) {
          // if (x.metadata_type_details.filter(y => y.checked_value != true && y.checked_value != false).length > 0) {
          let str_metadata_type_draft_details: any[] = [];
          x.metadata_type_details.forEach(element => {
            // if (element.checked_value != true && element.checked_value != false) {
            str_metadata_type_draft_details.push({
              id: element.id,
              seq_no: element.seq_no,
              metadata_type_dtl_id: element.metadata_type_dtl.id
            })
            //}
          });
          subCategoryMetadataBody.sub_category_metadata_type.push({
            id: x.id,
            sub_category_id: this.getsubCategory.id,
            seq_no: x.seq_no,
            metadata_type_id: x.metadata_type.id,
            is_single: this.allowMultiple == true ? false : true,
            is_mandatory: this.is_mandatory == true ? true : false,
            is_suggest_other: this.allow_suggest == true ? true : false,
            details: str_metadata_type_draft_details,
          });
          //}
          // }

        }
      });
    }
    subCategoryMetadataBody.sub_category_metadata_type.splice(subCategoryMetadataBody.sub_category_metadata_type.findIndex(element => element.sub_category_id == 0 || element.sub_category_id == "0"), 1);
    this.progress.show();
    this.httpServices.request("patch", "subcategories_metadata/update", null, null, subCategoryMetadataBody).subscribe((data) => {
      toastr.success("data saved successfully.");
      this.progress.hide();
    }, error => {
      console.log(error);
    });
  }
  addEdit_Metadata_Type() {
    if (this.metaData_Id == 0) {
      this.httpServices.request("post", "", "", "", null).subscribe((data) => {
        //toastr.success("Metadata Added Successfully");
      }, error => {
        console.log(error);
      });
    } else {
      this.httpServices.request("patch", "", "", "", null).subscribe((data) => {
        //toastr.success("Metadata Updated Successfully");
      }, error => {
        console.log(error);
      });
    }
  }
  update_Subcategory_Metadata()
  {
    // if(this.emptyMetadataType==false)
    // {
    let updateBody:any={
      sub_category_metadata_type: [
        {
          id: 0,
          sub_category_id: 0,
          seq_no: 0,
          metadata_type_id: 0,
          is_single: true,
          is_mandatory: true,
          is_suggest_other: true,
          details: [
            {
              id: 0,
              seq_no: 0,
              metadata_type_dtl_id: 0
            }
          ]
        }
      ]
    }

    for(let i=0;i<this.metadata_Subtype_list.mataData.length;i++)
    {
      let details_temp:any=[{
        id: 0,
        seq_no: 0,
        metadata_type_dtl_id: 0
      }]
      this.metadata_Subtype_list.mataData[i].metadata_type_details.forEach(element=>
        details_temp.push({id:element.id,seq_no:element.seq_no,metadata_type_dtl_id:element.metadata_type_dtl.id})
        )
        details_temp.splice(details_temp.findIndex(element=>element.metadata_type_dtl_id==0),1);

        updateBody.sub_category_metadata_type.push({id:this.metadata_Subtype_list.mataData[i].is_new==false?this.metadata_Subtype_list.mataData[i].id:0,
          sub_category_id:Number(this.getsubCategory.id),
          seq_no:this.metadata_Subtype_list.mataData[i].seq_no,
          metadata_type_id:this.metadata_Subtype_list.mataData[i].metadata_type.id,
          is_single:this.allowMultiple==true?false:true,
          is_mandatory:this.is_mandatory==true?true:false,
          is_suggest_other:this.allow_suggest==true?true:false,
          details:details_temp
        })
    }
    updateBody.sub_category_metadata_type.splice(updateBody.sub_category_metadata_type.findIndex(x=>x.sub_category_id==0),1);
    this.progress.show();
    this.httpServices.request("patch", "subcategories_metadata/update", null,null, updateBody).subscribe((data) => {
      toastr.success("Data updated successfully");
      this.router.navigate(['/metadata_list'], {});
      this.progress.hide();
    }, error => {
      console.log(error);
    });
  }
  // else{
  //   let updateBody:any={
  //     sub_category_metadata_type: [
  //       {
  //         sub_category_id: 0,
  //         seq_no: 0,
  //         metadata_type_id: 0,
  //         is_single: true,
  //         is_mandatory: true,
  //         is_suggest_other: true,
  //         details: [
  //           {
  //             seq_no: 0,
  //             metadata_type_dtl_id: 0
  //           }
  //         ]
  //       }
  //     ]
  //   }

  //   for(let i=0;i<this.metadata_Subtype_list.mataData.length;i++)
  //   {
  //     let details_temp:any=[{
  //       seq_no: 0,
  //       metadata_type_dtl_id: 0
  //     }]
  //     this.metadata_Subtype_list.mataData[i].metadata_type_details.forEach(element=>
  //       details_temp.push({seq_no:element.seq_no,metadata_type_dtl_id:element.metadata_type_dtl.id})
  //       )
  //       details_temp.splice(details_temp.findIndex(element=>element.metadata_type_dtl_id==0),1);

  //       updateBody.sub_category_metadata_type.push({
  //         sub_category_id:Number(this.getsubCategory.id),
  //         seq_no:this.metadata_Subtype_list.mataData[i].seq_no,
  //         metadata_type_id:this.metadata_Subtype_list.mataData[i].metadata_type.id,
  //         is_single:this.allowMultiple==true?false:true,
  //         is_mandatory:this.is_mandatory==true?true:false,
  //         is_suggest_other:this.allow_suggest==true?true:false,
  //         details:details_temp
  //       })
  //   }
  //   updateBody.sub_category_metadata_type.splice(updateBody.sub_category_metadata_type.findIndex(x=>x.sub_category_id==0),1);
  //   this.progress.show();
  //   this.httpServices.request("post", "subcategories_metadata/create", null,null, updateBody).subscribe((data) => {
  //     toastr.success("Data updated successfully");
  //     this.router.navigate(['/metadata_list'], {});
  //     this.progress.hide();
  //   }, error => {
  //     console.log(error);
  //   });
  // }
  // }
}

interface metadataTypelist {
  result: [

    {
      id: number,
      metadata_type: {
        id: number,
        metadata_type_name: string,
        is_single: boolean,
        is_mandatory: boolean,
        is_suggest_other: boolean,
        is_active: boolean,
        created_date: string,
        modified_date: string
      },
      seq_no: number,
      metadata_type_name: string,
      is_active: boolean,
      showTextBox: boolean,
      txt_metadata_type_name: string,
      is_new:boolean
    },

  ]
}
interface metaDataSubTypeList {
  mataData:
  [{
    metaData_draft_id: number,
    created_date: string,
    modified_date: string,
    id: number,
    is_active: boolean,
    is_allowed_on_request: boolean,
    is_mandatory: boolean,
    is_single: boolean,
    is_suggest_other: boolean,
    is_suggest_other_checked_value: boolean,
    is_suggest_other_id: number,
    is_suggest_other_text: string,
    max_value: number,
    min_value: number,
    seq_no: number,
    metadata_type: {
      created_date: string,
      id: number,
      is_active: boolean,
      is_mandatory: boolean,
      is_single: boolean,
      is_suggest_other: boolean,
      metadata_type_name: string,
      modified_date: string
    },
    metadata_type_details: [{
      id: number,
      metadata_type_dtl: {
        id: number,
        is_active: boolean,
        metadata_type_name: string,
        seq_no: number
      },
      seq_no: number,
      checked_value: boolean
    }],
    sub_category: {
      category: {
        id: number,
        category_name: string,
        is_active: boolean
      },
      id: number,
      is_active: boolean,
      modified_date: string,
      seq_no: number,
      sub_category_name: string,
      sub_category_picture: string,
      sub_category_thumb_picture: string,
      tagline: string
    },
    is_new:boolean
  }]
}
interface metadata_Style_list {
  result: [{
    id: number,
    metadata_type: {
      id: number,
      metadata_type_name: string,
      is_single: boolean,
      is_mandatory: boolean,
      is_suggest_other: boolean,
      is_active: boolean,
      created_date: string,
      modified_date: string
    },
    seq_no: number,
    metadata_type_name: string,
    checked_value: boolean
  }]

}