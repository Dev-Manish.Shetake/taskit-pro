/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Metadata_listComponent } from './metadata_list.component';

describe('Metadata_listComponent', () => {
  let component: Metadata_listComponent;
  let fixture: ComponentFixture<Metadata_listComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Metadata_listComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Metadata_listComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
