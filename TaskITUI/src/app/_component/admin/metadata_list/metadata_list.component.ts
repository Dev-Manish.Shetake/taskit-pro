import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State, process } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { Router } from '@angular/router';
declare var jQuery: any;
declare var toastr: any;

@Component({
  selector: 'app-metadata_list',
  templateUrl: './metadata_list.component.html',
  styleUrls: ['./metadata_list.component.css']
})
export class Metadata_listComponent implements OnInit {
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip: 0,
    take: 10
  }
  public pageNumber: any;

  public metaData_gridData: GridDataResult | undefined;
  //public category_id :any;
  //public subcategory_id:any;
  public metaData_type: any;
  public metadata_type_Id: number;
  public searchCatgory_list: any;
  public searchsubCatgory_list: any;
  public searchMetadata_type_name: any;
  public getCategory: any = { id: 0, category_name: "" };
  public getsubCategory: any = { id: 0, sub_category_name: "" };
  public metaData_List: metadata_list = {
    results: [
      {
        id: 0,
        category: {
          id: 0,
          category_name: "",
          is_active: true
        },
        sub_category_name: "",
        sub_category_metadata_type: [
          {
            id: 0,
            seq_no: 0,
            metadata_type: {
              id: 0,
              metadata_type_name: "",
              is_single: true,
              is_mandatory: true,
              is_suggest_other: true,
              is_active: true,
              created_date: "",
              modified_date: ""
            },
            is_active: true,
            created_date: "",
            modified_date: ""
          }
        ],
        updated_date: "",
        metadata_type_name_new: ""
      }
    ]
  };
  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.getMetadata_List();
    this.getSearch_category();
    //this.getSearch_subCategory();
  }

  //*******************************
  //Category List for Search.
  //*******************************
  getSearch_category() {
    this.httpServices.request("get", "categories/list", "", "", null).subscribe((data) => {
      this.searchCatgory_list = data;
    }, error => {
      console.log(error);
    });
  }
  // openaddEditMetadata(){
  //   // sessionStorage.setItem("queryParams", null);
  //   this.router.navigate(['/metadata_add'],{});
  // }
  editSubCategoryMetadata(subCategoryId: any, categoryId: any, category_Name: any) {
    let passQueryParam = { subCategoryId: subCategoryId, categoryId: categoryId, category_Name: category_Name };
    sessionStorage.setItem("queryParamsMetadata", JSON.stringify(passQueryParam));
    this.router.navigate(['/metadata_add'], {});
  }
  //*******************************
  //SubCategory List for Search.
  //*******************************
  getSearch_subCategory(categoryID: number) {
    if (this.getCategory.id > 0) {
      this.httpServices.request('get', 'subcategories/list?category_id=' + categoryID, null, null, null)
        .subscribe(data => {
          if (data.length > 0) {
            this.searchsubCatgory_list = data;
          }
        }, error => {
          console.log(error);
        });
    }
  }

  //*******************************
  //Kendo Grid List.
  //******************************

  getMetadata_List() {
    this.progress.show();
    this.httpServices.request("get", "subcategories_metadata/", "", "", null).subscribe((data) => {
      this.metaData_List.results = data.results;
      for (let i = 0; i < this.metaData_List.results.length; i++) {
        for (let k = 0; k < this.metaData_List.results[i].sub_category_metadata_type.length; k++) {
          if (k == 0) {
            this.metaData_List.results[i].metadata_type_name_new = this.metaData_List.results[i].sub_category_metadata_type[k].
              metadata_type.metadata_type_name;
          }
          else if (k > 0) {
            this.metaData_List.results[i].metadata_type_name_new = this.metaData_List.results[i].metadata_type_name_new + ", " +
              this.metaData_List.results[i].sub_category_metadata_type[k].metadata_type.metadata_type_name;
          }
        }
      }

      this.loadMetadata_List(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }


  private loadMetadata_List(recordCount: number): void {
    if (this.metaData_List.results.length > 0) {
      this.metaData_gridData = {
        data: orderBy(this.metaData_List.results, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.metaData_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortMetadataChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadMetadata_List(this.totalRecordCount);
  }


  public metaData_StateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.metaData_gridData = process(this.metaData_List.results, this.state);
  }

  //******************************
  // Page Change.
  //*******************************
  pageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "subcategories_metadata/?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.getCategory.id != null && this.getCategory.id != undefined && this.getCategory.id != '') {
      url = url + '&category_id=' + this.getCategory.id
    }
    if (this.getsubCategory.id != null && this.getsubCategory.id != undefined && this.getsubCategory.id != '') {
      url = url + '&sub_category_id=' + this.getsubCategory.id
    }
    if (this.searchMetadata_type_name != null && this.searchMetadata_type_name != undefined && this.searchMetadata_type_name != 0) {
      url = url + '&metadata_type_name=' + this.searchMetadata_type_name
    }

    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.metaData_List.results = data.results;
        for (let i = 0; i < this.metaData_List.results.length; i++) {
          for (let k = 0; k < this.metaData_List.results[i].sub_category_metadata_type.length; k++) {
            if (k == 0) {
              this.metaData_List.results[i].metadata_type_name_new = this.metaData_List.results[i].sub_category_metadata_type[k].
                metadata_type.metadata_type_name;
            }
            else if (k > 0) {
              this.metaData_List.results[i].metadata_type_name_new = this.metaData_List.results[i].metadata_type_name_new + ", " +
                this.metaData_List.results[i].sub_category_metadata_type[k].metadata_type.metadata_type_name;
            }
          }
        }
        this.loadMetadata_List(data.count);
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  //******************************
  // On Search Btn.
  //*******************************
  onSearchBtn() {
    this.progress.show();
    //if (this.getCategory.id > 0 || this.getsubCategory.id > 0 || this.searchMetadata_type_name != "") {
      var url = "subcategories_metadata/?page=1&page_size=" + this.pageSize;

      if (this.getCategory.id != null && this.getCategory.id != undefined && this.getCategory.id != '') {
        url = url + '&category_id=' + this.getCategory.id
      }
      if (this.getsubCategory.id != null && this.getsubCategory.id != undefined && this.getsubCategory.id != '') {
        url = url + '&sub_category_id=' + this.getsubCategory.id
      }
      if (this.searchMetadata_type_name != null && this.searchMetadata_type_name != undefined && this.searchMetadata_type_name != 0) {
        url = url + '&metadata_type_name=' + this.searchMetadata_type_name
      }

      this.httpServices.request("get", url, null, null, null)
        .subscribe((data) => {
          this.metaData_List.results = data.results;
          for (let i = 0; i < this.metaData_List.results.length; i++) {
            for (let k = 0; k < this.metaData_List.results[i].sub_category_metadata_type.length; k++) {
              if (k == 0) {
                this.metaData_List.results[i].metadata_type_name_new = this.metaData_List.results[i].sub_category_metadata_type[k].
                  metadata_type.metadata_type_name;
              }
              else if (k > 0) {
                this.metaData_List.results[i].metadata_type_name_new = this.metaData_List.results[i].metadata_type_name_new + ", " +
                  this.metaData_List.results[i].sub_category_metadata_type[k].metadata_type.metadata_type_name;
              }
            }
          }
          this.loadMetadata_List(data.count);
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    //} 
    // else {
    //   toastr.error("Please Enter a Search Parameter");
    //   this.progress.hide()
    // }
  }


  onDelete_trashBtn(metadata_type_Id: number) {
    this.metadata_type_Id = metadata_type_Id;
    jQuery('#delete_record').modal({ backdrop: 'static', keyboard: false });
  }

  onDeletemetadata_Type() {
    this.progress.show();
    this.httpServices.request("delete", "subcategories_metadata/" + this.metadata_type_Id, "", "", null).subscribe((data) => {
      jQuery('#delete_record').modal('hide');
      toastr.success("Record Deleted Successfully");
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  // ====================================
  // on click of clear all 
  // ====================================
  onClearAllClick() {
    this.getsubCategory.id = 0;
    this.getCategory.id = 0;
    this.searchMetadata_type_name = "";
    this.getMetadata_List();
  }
}



interface metadata_list {

  results: [
    {
      id: number,
      category: {
        id: number,
        category_name: string,
        is_active: true
      },
      sub_category_name: string,
      sub_category_metadata_type: [
        {
          id: number,
          seq_no: number,
          metadata_type: {
            id: number,
            metadata_type_name: string,
            is_single: true,
            is_mandatory: true,
            is_suggest_other: true,
            is_active: true,
            created_date: string,
            modified_date: string
          },
          is_active: true,
          created_date: string,
          modified_date: string
        }
      ],
      updated_date: string,
      metadata_type_name_new: string
    }
  ]
}
