import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State,process } from '@progress/kendo-data-query';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cancelled_Orders',
  templateUrl: './cancelled_Orders.component.html',
  styleUrls: ['./cancelled_Orders.component.css']
})
export class Cancelled_OrdersComponent implements OnInit {

  constructor(private httpServices: HttpRequestService,private router: Router,private dateFormatter: DateFormatterService,
    private progress: NgxSpinnerService,private encrypt_decrypt: EncryptDecryptService) { }
  public focusedDate: Date;
  public maxDate: Date;
  public getStatus:any={id:0, order_status_name:''};
  public cancelledOrders_list:any;
  public cancelledOrders_gridData:GridDataResult | undefined;
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip:0,
    take:10
  }
  public pageNumber:any;
  public searchSeller:string;
  public searchBuyer:string;
  public searchOrderdate:string;
  public userAuthInfo: any;

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.getCancelledorders();
  }

  getCancelledorders() {
    this.httpServices.request('get', 'orders/?order_status_id=8', '', '', null).subscribe((data) => {
      this.cancelledOrders_list = data.results;
      this.loadCancelled_order(data.count);
    }, error => {
      console.log(error);
    });
  }

  private loadCancelled_order(recordCount: number): void {
    if (this.cancelledOrders_list.length > 0) {
      this.cancelledOrders_gridData = {
        data: orderBy(this.cancelledOrders_list, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.cancelledOrders_gridData = undefined;
    }
  }

  public sortCancelled_orderChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadCancelled_order(this.totalRecordCount);
  }

  public cancelled_OrderDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.cancelledOrders_gridData = process(this.cancelledOrders_list, this.state);
  }

  //Page Change
  pageChange(event: PageChangeEvent): void{
    
    this.pageNumber=(event.skip + this.pageSize )/this.pageSize;
    var url = "orders/?order_status_id=8&page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if(this.searchSeller!=null && this.searchSeller!=undefined && this.searchSeller!=''){
      url=url+'&seller_name='+ this.searchSeller
    }
    if(this.searchBuyer!=null && this.searchBuyer!=undefined && this.searchBuyer!=''){
      url=url+'&buyer_name='+ this.searchBuyer
    }
    // if(this.getStatus.id!=null && this.getStatus.id!=undefined && this.getStatus.id!=0){
    //   url=url+'&order_status_id='+ this.getStatus.id
    // }
    if(this.searchOrderdate!=null && this.searchOrderdate!=undefined && this.searchOrderdate!=''){
      url=url+'&order_date='+ this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    }
    //var url = "orders/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    this.httpServices.request("get", url, null, null, null)
    .subscribe((data) => {
      this.cancelledOrders_list = data.results;
      this.loadCancelled_order(data.count);
    },error=>{
      console.log(error);
    });
  }

//On Search Btn Click
  onSearchBtn(){
    var url = "orders/?order_status_id=8&page=1&page_size=" + this.pageSize;

    if(this.searchSeller!=null && this.searchSeller!=undefined && this.searchSeller!=''){
      url=url+'&seller_name='+ this.searchSeller
    }
    if(this.searchBuyer!=null && this.searchBuyer!=undefined && this.searchBuyer!=''){
      url=url+'&buyer_name='+ this.searchBuyer
    }
    // if(this.getStatus.id!=null && this.getStatus.id!=undefined && this.getStatus.id!=0){
    //   url=url+'&order_status_id='+ this.getStatus.id
    // }
    if(this.searchOrderdate!=null && this.searchOrderdate!=undefined && this.searchOrderdate!=''){
      url=url+'&order_date='+ this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    }
    this.httpServices.request("get", url, null, null, null)
    .subscribe((data) => {
      this.cancelledOrders_list = data.results;
      this.loadCancelled_order(data.count);
    },error=>{
      console.log(error);
    });
  }

  // =====================================
  // On Seller Name to Seller Profile 
  // ======================================

  OnSellernameClick(sellerId){
    
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
  OnbuyernameClick(buyerId){
    
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == buyerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.buyer_profile_id = buyerId
    this.encrypt_decrypt.buyer_profile_mode = profile_mode;
    this.router.navigate(['/buyer_profile'], {});
  }
}
