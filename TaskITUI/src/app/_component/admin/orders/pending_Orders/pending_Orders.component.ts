import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State,process } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-pending_Orders',
  templateUrl: './pending_Orders.component.html',
  styleUrls: ['./pending_Orders.component.css']
})
export class Pending_OrdersComponent implements OnInit {

  constructor(private httpServices: HttpRequestService,private progress: NgxSpinnerService,private router: Router,private encrypt_decrypt: EncryptDecryptService,private dateFormatter: DateFormatterService) { }
  public focusedDate: Date;
  public maxDate: Date;
  public pendingOrderlist:any;
  public pending_Griddata:GridDataResult | undefined;
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip:0,
    take:10
  }
  public pageNumber:any;
  public searchSeller:string;
  public searchBuyer:string;
  public searchOrderdate:string;
  public getStatus:any={id:0, order_status_name:''};
  public userAuthInfo: any;

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.getPendingOrders();
  }

  getPendingOrders(){
    this.progress.show();
    this.httpServices.request("get","orders/?order_status_id=3","","",null).subscribe((data)=>{
      this.pendingOrderlist=data.results;
      this.loadPending_orderData(data.count);
      this.progress.hide();
    },error=>{
      console.log(error);
      this.progress.hide();
    });
  }


  // private loadPending_orderData(recordCount:number){
  //   if(this.pendingOrderlist.length>0){
  //     this.pending_Griddata={
  //       data: orderBy(this.pendingOrderlist,this.sort).slice(this.skip,this.skip+this.pageSize),
  //       total:recordCount
  //     };
  //   }else{
  //     this.pending_Griddata=undefined;
  //   }
  // }

  // public sortPending_orderChange(sort:SortDescriptor[]):void{
  //   this.sort=sort;
  //   this.loadPending_orderData(this.totalRecordCount);
  // }

  // public pending_OrderDataStateChange(state:DataStateChangeEvent){
  //   this.state=state;
  //   this.pending_Griddata = process(this.pendingOrderlist, this.state);
  // }

  
  private loadPending_orderData(recordCount: number): void {
    if (this.pendingOrderlist.length > 0) {
      this.pending_Griddata = {
        data: orderBy(this.pendingOrderlist, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.pending_Griddata = undefined;
    }
  }

  public sortPending_orderChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadPending_orderData(this.totalRecordCount);
  }

  public pending_OrderDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.pending_Griddata = process(this.pendingOrderlist, this.state);
  }

  pageChange(event:PageChangeEvent):void{
    this.progress.show();
    this.pageNumber=(event.skip + this.pageSize )/this.pageSize;
     var url = "orders/?order_status_id=3&page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if(this.searchSeller!=null && this.searchSeller!=undefined && this.searchSeller!=''){
      url=url+'&seller_name='+ this.searchSeller
    }
    if(this.searchBuyer!=null && this.searchBuyer!=undefined && this.searchBuyer!=''){
      url=url+'&buyer_name='+ this.searchBuyer
    }
    // if(this.getStatus.id!=null && this.getStatus.id!=undefined && this.getStatus.id!=0){
    //   url=url+'&order_status_id='+ this.getStatus.id
    // }
    if(this.searchOrderdate!=null && this.searchOrderdate!=undefined && this.searchOrderdate!=''){
      url=url+'&order_date='+ this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    }
    //var url = "orders/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    this.httpServices.request("get", url, null, null, null)
    .subscribe((data) => {
      this.pendingOrderlist=data.results;
      this.loadPending_orderData(data.count);
      this.progress.hide();
    },error=>{
      console.log(error);
      this.progress.hide();
    });
  }

  //On Search Btn Click
  onSearchBtn(){
    this.progress.show();
    var url = "orders/?order_status_id=3&page=1&page_size=" + this.pageSize;

    if(this.searchSeller!=null && this.searchSeller!=undefined && this.searchSeller!=''){
      url=url+'&seller_name='+ this.searchSeller
    }
    if(this.searchBuyer!=null && this.searchBuyer!=undefined && this.searchBuyer!=''){
      url=url+'&buyer_name='+ this.searchBuyer
    }
    // if(this.getStatus.id!=null && this.getStatus.id!=undefined && this.getStatus.id!=0){
    //   url=url+'&order_status_id='+ this.getStatus.id
    // }
    if(this.searchOrderdate!=null && this.searchOrderdate!=undefined && this.searchOrderdate!=''){
      url=url+'&order_date='+ this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    }
    this.httpServices.request("get", url, null, null, null)
    .subscribe((data) => {
      this.pendingOrderlist=data.results;
      this.loadPending_orderData(data.count);
      this.progress.hide();
    },error=>{
      console.log(error);
      this.progress.hide();
    });
  }

  // =====================================
  // On Seller Name to Seller Profile 
  // ======================================

  OnSellernameClick(sellerId){
    
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
  OnbuyernameClick(buyerId){
    
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == buyerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.buyer_profile_id = buyerId
    this.encrypt_decrypt.buyer_profile_mode = profile_mode;
    this.router.navigate(['/buyer_profile'], {});
  }

}
