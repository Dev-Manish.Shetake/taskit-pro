import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State, process } from '@progress/kendo-data-query';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-total_Orders',
  templateUrl: './total_Orders.component.html',
  styleUrls: ['./total_Orders.component.css']
})
export class Total_OrdersComponent implements OnInit {

  constructor(private httpServices: HttpRequestService,private progress: NgxSpinnerService,private router: Router,private encrypt_decrypt: EncryptDecryptService, private dateFormatter: DateFormatterService) { }
  public focusedDate: Date;
  public maxDate: Date;
  public orderStatusList: any;
  public totalOrderList: any;
  public getStatus: any = { id: 0, order_status_name: '' };
  public totalOrders_gridData: GridDataResult | undefined;

  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip: 0,
    take: 10
  }
  public pageNumber: any;
  public searchSeller: string;
  public searchBuyer: string;
  public searchOrderdate: string;
  public userAuthInfo: any;

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.getOrderstatus();
    this.getTotalorders();
  }

  //Search Order Status Dropdown
  getOrderstatus() {
    this.httpServices.request('get', 'order_status/', '', '', null).subscribe((data) => {
      this.orderStatusList = data;
    }, error => {
      console.log(error);
    });
  }

  //To populate Total Orders in Kendo Grid
  getTotalorders() {
    this.progress.show();
    this.httpServices.request("get", "orders/?page=1&page_size=" + this.pageSize, null, null, null)
      .subscribe((data) => {
        this.totalOrderList = data.results;
        this.loadTotal_orderData(data.count);
        this.manipulateStatus();
        this.progress.hide();
      },error=>{
        console.log(error);
        this.progress.hide();
      });
  }


  private loadTotal_orderData(recordCount: number): void {
    if (this.totalOrderList.length > 0) {
      this.totalOrders_gridData = {
        data: orderBy(this.totalOrderList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.totalOrders_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortTotal_orderChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadTotal_orderData(this.totalRecordCount);
  }


  public total_OrderDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.totalOrders_gridData = process(this.totalOrderList, this.state);
  }

  //Page Change
  pageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    url = "orders/?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.searchSeller != null && this.searchSeller != undefined && this.searchSeller != '') {
      url = url + '&seller_name=' + this.searchSeller
    }
    if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
      url = url + '&buyer_name=' + this.searchBuyer
    }
    if (this.getStatus.id != null && this.getStatus.id != undefined && this.getStatus.id != 0) {
      url = url + '&order_status_id=' + this.getStatus.id
    }
    if (this.searchOrderdate != null && this.searchOrderdate != undefined && this.searchOrderdate != '') {
      url = url + '&order_date=' + this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    }
    var url = url = "orders/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.totalOrderList = data.results;
        this.loadTotal_orderData(data.count);
        this.manipulateStatus();
        this.progress.hide();
      },error=>{
        console.log(error);
        this.progress.hide();
      });
  }

  //On Search Btn Click
  onSearchBtn() {
    this.progress.show();
    var url = "orders/?page=1&page_size=" + this.pageSize;

    if (this.searchSeller != null && this.searchSeller != undefined && this.searchSeller != '') {
      url = url + '&seller_name=' + this.searchSeller
    }
    if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
      url = url + '&buyer_name=' + this.searchBuyer
    }
    if (this.getStatus.id != null && this.getStatus.id != undefined && this.getStatus.id != 0) {
      url = url + '&order_status_id=' + this.getStatus.id
    }
    if (this.searchOrderdate != null && this.searchOrderdate != undefined && this.searchOrderdate != '') {
      url = url + '&order_date=' + this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    }
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.totalOrderList = data.results;
        this.loadTotal_orderData(data.count);
        this.manipulateStatus();
        this.progress.hide();
      },error=>{
        console.log(error);
        this.progress.hide();
      });
  }

  // =====================================
  // On Seller Name to Seller Profile 
  // ======================================

  OnSellernameClick(sellerId){
    
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
  OnbuyernameClick(buyerId){
    
    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == buyerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.buyer_profile_id = buyerId
    this.encrypt_decrypt.buyer_profile_mode = profile_mode;
    this.router.navigate(['/buyer_profile'], {});
  }

  // ====================================
  // Manipulate status 
  // ===================================
  manipulateStatus() {
    if (this.totalOrderList.length > 0) {
      for (let i = 0; i < this.totalOrderList.length; i++) {
        if (this.totalOrderList[i].order_status.id == 2) {
          this.totalOrderList[i].property = "badge badge-success custom_badge_css";
        }else if (this.totalOrderList[i].order_status.id == 1) {
          this.totalOrderList[i].property = "badge badge-secondary custom_badge_css";
        }
        else if (this.totalOrderList[i].order_status.id == 6) {
          this.totalOrderList[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.totalOrderList[i].order_status.id == 7) {
          this.totalOrderList[i].property = "badge badge-primary custom_badge_css";
        }
        else if (this.totalOrderList[i].order_status.id == 8) {
          this.totalOrderList[i].property = "badge badge-danger custom_badge_css";
        }
        else if (this.totalOrderList[i].order_status.id == 3) {
          this.totalOrderList[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.totalOrderList[i].order_status.id == 4) {
          this.totalOrderList[i].property = "badge badge-warning custom_badge_css";
        }
        else if (this.totalOrderList[i].order_status.id == 5) {
          this.totalOrderList[i].property = "badge badge-success custom_badge_css";
        }
      }
    }
  }

  onClearAllClick()
  {
    this.searchSeller="";
    this.getStatus.id=0;
    this.searchBuyer="";
    this.searchOrderdate="";
    this.getTotalorders();
  }
}
