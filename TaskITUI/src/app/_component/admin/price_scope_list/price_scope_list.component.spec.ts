/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Price_scope_listComponent } from './price_scope_list.component';

describe('Price_scope_listComponent', () => {
  let component: Price_scope_listComponent;
  let fixture: ComponentFixture<Price_scope_listComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Price_scope_listComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Price_scope_listComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
