import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, process, State } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { Router } from '@angular/router';
declare var toastr: any;
@Component({
  selector: 'app-price_scope_list',
  templateUrl: './price_scope_list.component.html',
  styleUrls: ['./price_scope_list.component.css']
})
export class Price_scope_listComponent implements OnInit {

  public getCategory: any = { id: 0, category_name: "" };
  public getsubCategory: any = { id: 0, sub_category_name: "" };
  public categoryList: any;
  public subcategoryList: any;
  public priceScope_gridData: GridDataResult | undefined;

  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip: 0,
    take: 10
  }
  public pageNumber: any;

  public priceScopeList: priceScope = {
    results: [
      {
        id: 0,
        category: {
          id: 0,
          category_name: "",
          is_active: true
        },
        sub_category_name: "",
        sub_category_price_scope: [
          {
            id: 0,
            seq_no: 0,
            price_scope_name: "",
            price_scope_control_type: {
              id: 0,
              price_scope_control_type_name: ""
            },
            is_active: true,
            created_date: "",
            modified_date: ""
          }
        ],
        updated_date: "",
        new_price_scope_name: ""
      }
    ]
  };

  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService, private router: Router) { }

  ngOnInit() {
    this.getcategory_forSearch();
    this.getPrice_scopeList();
  }

  //******************************
  // Get category for Search
  //******************************
  getcategory_forSearch() {
    this.httpServices.request("get", "categories/list", "", "", null).subscribe((data) => {
      this.categoryList = data;
    }, error => {
      console.log(error);
    });
  }


  //******************************
  // Get Subcategory for Search
  //******************************
  getSubcategory_forSearch(category_Id: number) {
    this.progress.show();
    if (this.getCategory.id > 0) {
      this.httpServices.request("get", "subcategories/list?category_id=" + category_Id, "", "", null)
        .subscribe((data) => {
          if (data.length > 0) {
            this.subcategoryList = data;
          }
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    }
  }

  //**********************************
  // Get Price Scope List for Search
  //**********************************
  getPrice_scopeList() {
    this.progress.show();
    this.httpServices.request("get", "subcategories_price_scope/list", "", "", null).subscribe((data) => {
      this.priceScopeList.results = data.results;
      for (let i = 0; i < this.priceScopeList.results.length; i++) {
        for (let k = 0; k < this.priceScopeList.results[i].sub_category_price_scope.length; k++) {
          if (k == 0) {
            this.priceScopeList.results[i].new_price_scope_name = this.priceScopeList.results[i].sub_category_price_scope[k].
              price_scope_name;
          } else if (k > 0) {
            this.priceScopeList.results[i].new_price_scope_name = this.priceScopeList.results[i].new_price_scope_name + ", " +
              this.priceScopeList.results[i].sub_category_price_scope[k].price_scope_name;
          }
        }
      }

      this.loadpriceScope_List(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }


  private loadpriceScope_List(recordCount: number): void {
    if (this.priceScopeList.results.length > 0) {
      this.priceScope_gridData = {
        data: orderBy(this.priceScopeList.results, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.priceScope_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortPrice_scopeChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadpriceScope_List(this.totalRecordCount);
  }

  //Data State Change
  public priceScope_StateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.priceScope_gridData = process(this.priceScopeList.results, this.state);
  }


  //**********************************
  // Page Change Event
  //**********************************
  pageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "subcategories_price_scope/list?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.getCategory.id != null && this.getCategory.id != undefined && this.getCategory.id != '') {
      url = url + '&category_id=' + this.getCategory.id
    }
    if (this.getsubCategory.id != null && this.getsubCategory.id != undefined && this.getsubCategory.id != '') {
      url = url + '&sub_category_id=' + this.getsubCategory.id
    }

    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.priceScopeList.results = data.results;
        for (let i = 0; i < this.priceScopeList.results.length; i++) {
          for (let k = 0; k < this.priceScopeList.results[i].sub_category_price_scope.length; k++) {
            if (k == 0) {
              this.priceScopeList.results[i].new_price_scope_name = this.priceScopeList.results[i].sub_category_price_scope[k].
                price_scope_name;
            } else if (k > 0) {
              this.priceScopeList.results[i].new_price_scope_name = this.priceScopeList.results[i].new_price_scope_name + ", " +
                this.priceScopeList.results[i].sub_category_price_scope[k].price_scope_name;
            }
          }
        }
        this.loadpriceScope_List(data.count);
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }
  onEditPriceScope(id: any, categoryId: any) {
    let passQueryParam = { subCategoryid: id, categoryId: categoryId };
    sessionStorage.setItem("queryParamsPriceScope", JSON.stringify(passQueryParam));
    this.router.navigate(['/add_price_scope_list'], {});
  }
  //**********************************
  // Search Btn Method
  //**********************************
  onSearchBtn() {
    this.progress.show();
    if (this.getCategory.id > 0 || this.getsubCategory.id > 0) {
      var url = "subcategories_price_scope/list?page=1&page_size=" + this.pageSize;

      if (this.getCategory.id != null && this.getCategory.id != undefined && this.getCategory.id != '') {
        url = url + '&category_id=' + this.getCategory.id
      }
      if (this.getsubCategory.id != null && this.getsubCategory.id != undefined && this.getsubCategory.id != '') {
        url = url + '&sub_category_id=' + this.getsubCategory.id
      }

      this.httpServices.request("get", url, null, null, null)
        .subscribe((data) => {
          this.priceScopeList.results = data.results;

          for (let i = 0; i < this.priceScopeList.results.length; i++) {
            for (let k = 0; k < this.priceScopeList.results[i].sub_category_price_scope.length; k++) {
              if (k == 0) {
                this.priceScopeList.results[i].new_price_scope_name = this.priceScopeList.results[i].sub_category_price_scope[k].
                  price_scope_name;
              } else if (k > 0) {
                this.priceScopeList.results[i].new_price_scope_name = this.priceScopeList.results[i].new_price_scope_name + ", " +
                  this.priceScopeList.results[i].sub_category_price_scope[k].price_scope_name;
              }
            }
          }
          this.loadpriceScope_List(data.count);
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    } else {
      toastr.error("Please Enter a Search Parameter");
      this.progress.hide()
    }
  }

  //**********************************
  // Delete Btn Method
  //**********************************
  onDelete_priceScope(recordId: number) {
    //this.progress.show();
    this.httpServices.request("delete", "subcategories_price_scope/delete" + recordId, "", "", null).subscribe((data) => {
      toastr.error("Record Deleted Successfully");
      //this.progress.hide();
    }, error => {
      console.log(error);
      //this.progress.hide();
    });
  }

  // ====================================
  // on click of clear all 
  // ====================================
  onClearAllClick() {
    this.getsubCategory.id = 0;
    this.getCategory.id = 0;
    this.getPrice_scopeList();
  }
}

interface priceScope {

  results: [
    {
      id: number,
      category: {
        id: number,
        category_name: string,
        is_active: true
      },
      sub_category_name: string,
      sub_category_price_scope: [
        {
          id: number,
          seq_no: number,
          price_scope_name: string,
          price_scope_control_type: {
            id: number,
            price_scope_control_type_name: string
          },
          is_active: true,
          created_date: string,
          modified_date: string
        }
      ],
      updated_date: string,
      new_price_scope_name: string
    }
  ]
}