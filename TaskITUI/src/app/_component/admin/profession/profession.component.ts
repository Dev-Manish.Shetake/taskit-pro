import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State,process } from '@progress/kendo-data-query';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-profession',
  templateUrl: './profession.component.html',
  styleUrls: ['./profession.component.css']
})
export class ProfessionComponent implements OnInit {

  constructor(private httpServices: HttpRequestService) { }

public professionList:any;
public pageSize=10;
public profession_Griddata:GridDataResult| undefined;
public sort: SortDescriptor[] = [];
public skip = 0;
public totalRecordCount: number = 0;
public state: State = {
  skip:0,
  take:10
}
public searchProfession:string;
public getStatus:any={id:0,status_name:''};
  ngOnInit() {
    //this.getProfession();
  }

    getProfession(){
      
      this.httpServices.request("get", "profession/?page=1&page_size="+ this.pageSize, null, null, null)
      .subscribe((data) => {
        this.professionList = data.results;
        this.loadProfession_listData(data.count);
      });
    }

    private loadProfession_listData(recordCount:number){
      if(this.professionList.length>0){
        this.profession_Griddata={
          data: orderBy(this.professionList, this.sort).slice(this.skip, this.skip + this.pageSize),
          total: recordCount
        }
      }else{
        this.profession_Griddata=undefined;
      }
    }

    // To handle sorting on KENDO COLUMNS 
   public sortProfession_Change(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadProfession_listData(this.totalRecordCount);
  }

  public profession_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.profession_Griddata = process(this.professionList, this.state);
  }

  pageChange(event: PageChangeEvent): void{
    
    // this.pageNumber=(event.skip + this.pageSize )/this.pageSize;
    // url = "orders/?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    // if(this.searchSeller!=null && this.searchSeller!=undefined && this.searchSeller!=''){
    //   url=url+'&seller_name='+ this.searchSeller
    // }
    // if(this.searchBuyer!=null && this.searchBuyer!=undefined && this.searchBuyer!=''){
    //   url=url+'&buyer_name='+ this.searchBuyer
    // }
    // if(this.getStatus.id!=null && this.getStatus.id!=undefined && this.getStatus.id!=0){
    //   url=url+'&order_status_id='+ this.getStatus.id
    // }
    // if(this.searchOrderdate!=null && this.searchOrderdate!=undefined && this.searchOrderdate!=''){
    //   url=url+'&order_date='+ this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    // }
    // var url = url = "orders/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    // this.httpServices.request("get", url, null, null, null)
    // .subscribe((data) => {
    //   this.totalOrderList = data.results;
    //   this.loadTotal_orderData(data.count);
    // })
  }

  //On Search Btn Click
  onSearchBtn(){
    
    // var url = "orders/?page=1&page_size=" + this.pageSize;

    // if(this.searchSeller!=null && this.searchSeller!=undefined && this.searchSeller!=''){
    //   url=url+'&seller_name='+ this.searchSeller
    // }
    // if(this.searchBuyer!=null && this.searchBuyer!=undefined && this.searchBuyer!=''){
    //   url=url+'&buyer_name='+ this.searchBuyer
    // }
    // if(this.getStatus.id!=null && this.getStatus.id!=undefined && this.getStatus.id!=0){
    //   url=url+'&order_status_id='+ this.getStatus.id
    // }
    // if(this.searchOrderdate!=null && this.searchOrderdate!=undefined && this.searchOrderdate!=''){
    //   url=url+'&order_date='+ this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    // }
    // this.httpServices.request("get", url, null, null, null)
    // .subscribe((data) => {
    //   this.totalOrderList = data.results;
    //   this.loadTotal_orderData(data.count);
    // })
  }
}
