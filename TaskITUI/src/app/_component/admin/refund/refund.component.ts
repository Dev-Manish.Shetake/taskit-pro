import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State, process } from '@progress/kendo-data-query';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-refund',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.scss']
})
export class RefundComponent implements OnInit {

  constructor(private httpServices: HttpRequestService, private router: Router, private dateFormatter: DateFormatterService,
    private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService) { }

  public focusedDate: Date;
  public maxDate: Date;
  public getRefund_status: any = { id: 0, refund_status_name: '' };
  public getCancel_reason: any = { id: 0, cancel_reason_name: '' };
  public refund_list: any;
  public refund_gridData: GridDataResult | undefined;
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip: 0,
    take: 10
  }
  public pageNumber: number = 1;
  public searchSeller: string;
  public searchOrderNo: string;
  public searchBuyer: string;
  public searchOrderdate: string;
  public userAuthInfo: any;
  public refund_statusList: any;
  public cancel_reasonList: any;

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.getRefundStatusList();
    this.getCancelReasonList();
    this.getRefundData();
  }

  getRefundStatusList() {
    this.httpServices.request('get', 'refund_status/', '', '', null).subscribe((data) => {
      this.refund_statusList = data;
    }, error => {
      console.log(error);
    });
  }

  getCancelReasonList() {
    this.httpServices.request('get', 'order_cancel_reason/', '', '', null).subscribe((data) => {
      this.cancel_reasonList = data;
    }, error => {
      console.log(error);
    });
  }

  getRefundData() {
    this.httpServices.request('get', 'orders/refund-list', '', '', null).subscribe((data) => {
      this.refund_list = data.results;
      if (this.refund_list.length > 0) {
        this.refund_list.forEach(element => {
          let total_amount: number = 0;
          element.order_refund_log.forEach(refund_log => {
            total_amount = total_amount + refund_log.refunded_amount;
          });
          element.total_refund_amount = total_amount;
        });
      }
      this.loadGridData(data.count);
    }, error => {
      console.log(error);
    });
  }

  onRefundAction(order_id: any, cancel_reason: any) {
    let passQueryParam = { id: order_id, cancel_reason: cancel_reason };
    sessionStorage.setItem("order_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/order_details'], {});
  }

  private loadGridData(recordCount: number): void {
    if (this.refund_list.length > 0) {
      this.refund_gridData = {
        data: orderBy(this.refund_list, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.refund_gridData = undefined;
    }
  }

  public sortRefundChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadGridData(this.totalRecordCount);
  }

  public refund_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.refund_gridData = process(this.refund_list, this.state);
  }

  //Page Change
  pageChange(event: PageChangeEvent): void {

    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "orders/refund-list?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.searchSeller != null && this.searchSeller != undefined && this.searchSeller != '') {
      url = url + '&seller_name=' + this.searchSeller
    }
    if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
      url = url + '&buyer_name=' + this.searchBuyer
    }
    if (this.searchOrderNo != null && this.searchOrderNo != undefined && this.searchOrderNo != '') {
      url = url + '&order_no=' + this.searchOrderNo
    }
    if (this.searchOrderdate != null && this.searchOrderdate != undefined && this.searchOrderdate != '') {
      url = url + '&order_date=' + this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    }
    if (this.getCancel_reason.id != null && this.getCancel_reason.id != undefined && this.getCancel_reason.id != 0) {
      url = url + '&cancel_reason_id=' + this.getCancel_reason.id;
    }
    if (this.getRefund_status.id != null && this.getRefund_status.id != undefined && this.getRefund_status.id != 0) {
      url = url + '&refund_status_id=' + this.getRefund_status.id;
    }

    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.refund_list = data.results;
        this.loadGridData(data.count);
      }, error => {
        console.log(error);
      });
  }


  //On Search Btn Click
  onSearchBtn() {
    var url = "orders/refund-list?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.searchSeller != null && this.searchSeller != undefined && this.searchSeller != '') {
      url = url + '&seller_name=' + this.searchSeller
    }
    if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
      url = url + '&buyer_name=' + this.searchBuyer
    }
    if (this.searchOrderNo != null && this.searchOrderNo != undefined && this.searchOrderNo != '') {
      url = url + '&order_no=' + this.searchOrderNo
    }
    if (this.searchOrderdate != null && this.searchOrderdate != undefined && this.searchOrderdate != '') {
      url = url + '&order_date=' + this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    }
    if (this.getCancel_reason.id != null && this.getCancel_reason.id != undefined && this.getCancel_reason.id != 0) {
      url = url + '&cancel_reason_id=' + this.getCancel_reason.id;
    }
    if (this.getRefund_status.id != null && this.getRefund_status.id != undefined && this.getRefund_status.id != 0) {
      url = url + '&refund_status_id=' + this.getRefund_status.id;
    }
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.refund_list = data.results;
        this.loadGridData(data.count);
      }, error => {
        console.log(error);
      });
  }
}
