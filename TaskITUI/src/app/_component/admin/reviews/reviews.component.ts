import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { SortDescriptor,process,State, orderBy, } from '@progress/kendo-data-query';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.css']
})
export class ReviewsComponent implements OnInit {

  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;

  public reviewList:any;
  public reviewGriddata: GridDataResult | undefined;

  public state: State = {
    //skip:0,
    //take:10,
  }
  constructor(private httpServices: HttpRequestService) { }

  ngOnInit() {
    this.getReviewgridData();
  }

  //**********************************  
  // Review Grid Data
  //**********************************

  getReviewgridData() {
    //change api name
    this.httpServices.request("get", "reviews/", null, null, null)
      .subscribe((data) => {
        this.reviewList = data.results;
        this.loadReviewData(data.results.length);
      })
  }

  //A particular method that is always to be followed when using Kendo GRID
  private loadReviewData(recordCount: number): void {
    if (this.reviewList.length > 0) {
      this.reviewGriddata = {
        data: orderBy(this.reviewList, this.sort).slice(this.skip, this.skip + this.pageSize),
        //In case if gig list page has 40 record....SLICE to split record to next position as Record to be shown for Page is 10.
        //So remaining records are sliced to skip btn press.
        total: recordCount
      };
    } else {
      this.reviewGriddata = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortReviewChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadReviewData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For All Gig List
  public reviewDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.reviewGriddata = process(this.reviewList, this.state);
  }


}
