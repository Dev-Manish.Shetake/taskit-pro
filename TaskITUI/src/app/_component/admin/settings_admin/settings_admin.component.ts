import { Component, OnInit } from '@angular/core';
import { GridDataResult, PageChangeEvent, GridComponent, DataStateChangeEvent, CellClickEvent, SelectableSettings, SelectAllCheckboxState } from '@progress/kendo-angular-grid';
import { DataSourceRequestState, DataResult, SortDescriptor, orderBy, State, process, filterBy, FilterDescriptor, CompositeFilterDescriptor, toODataString, } from '@progress/kendo-data-query';
import { NgxSpinnerService } from "ngx-spinner";
import { EncryptDecryptService } from '../../../_common_services/encrypt-decrypt.service';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
declare var toastr: any;
@Component({
  selector: 'app-settings_admin',
  templateUrl: './settings_admin.component.html',
  styleUrls: ['./settings_admin.component.scss']
})
export class Settings_adminComponent implements OnInit {

  constructor(private progress: NgxSpinnerService,private encrypt_decrypt: EncryptDecryptService,private httpServices: HttpRequestService,) { }
public emailFromAdress:any;
public emailFromName:any;
public smtpHost:any;
public portNumber:any;
public password:any;
public sslEmail:any;
public smsSettings:any;
public griddata: GridDataResult | undefined;
public notificationsList:any;
public totalRecordCount:any;
public activeTab:any='general';
public state: State = {
  skip: 0,
  take: 10,
  // Initial filter descriptor
  filter: {
    logic: 'and',
    filters: []
  }
};
public skip = 0;
public pageSize = 10;
public sort: SortDescriptor[] = [];
public supportEmailAddress:any;
public supportContactNumber:any;
public editMode:boolean=false;
public userAuthInfo: any;
public userInfo: any = { currentUser: AuthInfo };
public userName:any;
public queryParam: any;
public _queryParams: any;
public emailDetails:any;
public smsDetails:any;
public serviceLevelDetails:any;
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    this.userName=this.userAuthInfo.currentUser.user.user_name
  }
  // ===========================================
  // get settings values 
  // ===========================================
  openEmailSettings()
  {
    this.progress.show();
    this.httpServices.request('get', 'settings/email/', null, null,null).subscribe((data) => {
      this.emailDetails=data[0];
      this.emailFromAdress=this.emailDetails.from_account;
      this.emailFromName=this.emailDetails.user_name;
      this.smtpHost=this.emailDetails.host;
      this.portNumber=this.emailDetails.port;
      this.password=this.emailDetails.pass_word;
      if(this.emailDetails.ssl==true)
      {
        this.sslEmail="Yes";
      }
      else{
        this.sslEmail="No";
      }
      this.progress.hide();
    }, (error) => {
    this.progress.hide();
  });
  }
  // ======================================
  // Update Email settings 
  // ======================================
  saveEmailSettings()
  {
    this.progress.show();
    let emailDetails={
      host: "",
      port: "",
      user_name: "",
      pass_word: "",
      from_account: "",
      ssl: true,
      relay_count: 0
    }
    emailDetails.host=this.smtpHost;
    emailDetails.port=this.portNumber;
    emailDetails.user_name=this.emailFromName;
    emailDetails.pass_word=this.password;
    emailDetails.from_account=this.emailFromAdress;
    if(this.sslEmail=="Yes")
    {
      emailDetails.ssl=true;
    }
    else{
      emailDetails.ssl=false;
    }
    emailDetails.relay_count=this.emailDetails.relay_count;
    this.httpServices.request('patch', 'settings/email/'+this.emailDetails.id, null, null,emailDetails).subscribe((data) => {
      toastr.success("Record updated successfully");
      this.progress.hide();
    }, (error) => {
    this.progress.hide();
  });

  }

  // ===================================
  // get SMS Details 
  // =====================================
  getSMSDetails()
  {
    this.progress.show();
    this.httpServices.request('get', 'settings/sms/', null, null,null).subscribe((data) => {
      this.smsDetails=data[0];
      this.smsSettings=this.smsDetails.sms_url;
      this.progress.hide();
    }, (error) => {
    this.progress.hide();
  });
  }
  // ===========================
  // Update SMS Details 
  // ===========================
  saveSMSSettings()
  {
    this.progress.show();
    let smsDetails={
      sms_url: "",
      user_name: "",
      pass_word: "",
      relay_count: 0
    }
    smsDetails.sms_url=this.smsSettings;
    smsDetails.user_name=this.smsDetails.user_name;
    smsDetails.pass_word=this.smsDetails.pass_word;
    smsDetails.relay_count=this.smsDetails.relay_count;
    this.httpServices.request('patch', 'settings/sms/'+this.smsDetails.id, null, null,smsDetails).subscribe((data) => {
      toastr.success("Record updated successfully");
      this.progress.hide();
    }, (error) => {
    this.progress.hide();
  });
  }
  // ==========================================
  // get service level details 
  // ==========================================
  getServiceLevelDetails()
  {
    this.progress.show();
    this.httpServices.request('get', 'settings/service-level', null, null,null).subscribe((data) => {
      this.serviceLevelDetails=data;
      this.progress.hide();
    }, (error) => {
    this.progress.hide();
  });
  }
  // ===========================================
  // update Service level Details 
  // ===========================================
  saveServiceLevelDetails()
  {
    let serviceLevelDetails=
    {
      level: [
        {
          seq_no: 0,
          level_name: "",
          range_from: 0,
          range_to: 0,
          no_of_orders: 0
        }
      ]
    }
    for(let i=0;i<this.serviceLevelDetails.length;i++)
    {
      serviceLevelDetails.level.push({seq_no:this.serviceLevelDetails[i].seq_no,level_name:this.serviceLevelDetails[i].level_name,
        range_from:this.serviceLevelDetails[i].range_from,range_to:this.serviceLevelDetails[i].range_to,no_of_orders:this.serviceLevelDetails[i].no_of_orders});
    }
    if(serviceLevelDetails.level.length>0)
    {
      if(serviceLevelDetails.level.filter(x=>x.seq_no==0).length>0)
      {
        serviceLevelDetails.level.splice(serviceLevelDetails.level.findIndex(y=>y.seq_no==0),1);
      }
    }
    this.progress.show();
    this.httpServices.request("patch","settings/service-level-update",null,null,serviceLevelDetails).subscribe((data)=>{
      toastr.success("Records updated successfully");
      this.progress.hide();
    }, (error) => {
    this.progress.hide();
  });
  }
  // ==========================================
  // validation for range from and range to 
  // ==========================================
  range_from_blur(item:any)
  {
    if(Number(item.range_from)>Number(item.range_to))
    {
      toastr.error("From Range should be less than To Range");
      item.range_from="";
    }
  }
  range_to_blur(item:any)
  {
    if(Number(item.range_from)>Number(item.range_to))
    {
      toastr.error("To Range should be greater than From Range");
      item.range_to="";
    }
  }
//==========================================================
   //  kendo grid bind
   //==========================================================
   private loadData(recordCount: number): void {
    if (this.notificationsList.length > 0) {
      this.griddata = {
        data: orderBy(this.notificationsList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount //this.labourList.results.length
      };
    }
    else {
      this.griddata = undefined;
    }
  }
  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }
  //==========================================================
   //  kendo grid - sorting
   //==========================================================
   public sortChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadData(this.totalRecordCount);
  }
  //==========================================================
   //  kendo grid - data state change
   //==========================================================
   public dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.griddata = process(this.notificationsList, this.state);
  }
   //==========================================================
  //  kendo grid - page change
  //==========================================================
  public pageChange(event: PageChangeEvent): void {
   
    this.progress.show();
    let url="";
// if(this.orderStatusId>0)
// {
//   url="orders/order_status_id="+this.orderStatusId+"&page=1&page_size="+this.pageSize;
// }
// else{
//   url="orders/&page=1&page_size="+this.pageSize;
// }
    // this.httpServices.request("get", url, null, null, null).subscribe((data) => {
    //   this.result = data.results;
    //   this.totalRecordCount = data.count;
    //   this.loadData(this.totalRecordCount);
    //   this.progress.hide();
    // });
  }

}
