import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { orderBy, SortDescriptor, process, State } from '@progress/kendo-data-query';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { Router } from '@angular/router';

declare var toastr: any;
declare var jQuery:any;
@Component({
  selector: 'app-users_list',
  templateUrl: './users_list.component.html',
  styleUrls: ['./users_list.component.css']
})
export class Users_listComponent implements OnInit {

  public backoffice_userList: any;
  public backoffice_user: any;
  public serviceLevel_list: any = { id: 0, level_name: "" };
  public status: any;
  public backoffice_UserId: number;
  public checkvalidation: boolean = false;
  public openEditModal: boolean = false;
  public getService: any = { id: 0, service_level_name: '' };
  public getStatus: any = { id: 2, status_name: '' };
  public search_userName: string;
  public searchemail_Address: string;
  public userListGriddata: GridDataResult | undefined;
  public editUserId: any;
  public state: State = {
    skip: 0,
    take: 10,
  }
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public pageNumber: any = 1;
  public service_Level: any;
  public user_type: any;
  public isVerifiedEmail: any;
  public userStatus: any;
  public email_address: any;
  public user_Name: any;
  public userAuthInfo: any;
  public is_Seller:any;

  constructor(private httpServices: HttpRequestService, private router: Router, private encrypt_decrypt: EncryptDecryptService, private progress: NgxSpinnerService) { }

  ngOnInit() {
    //this.getStatus.id=2;
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.getUsersList();
    this.getServiceLevel();
  }


  getServiceLevel() {
    this.httpServices.request("get", "service_level/", null, null, null).subscribe((data) => {
      this.serviceLevel_list = data.results;
      //toastr.success("Service Level Loaded");
    }, error => {
      console.log(error);
    });
  }

public is_seller:boolean=false;
  getUsersList() {
    this.progress.show();
    this.httpServices.request("get", 'user/backoffice-list?page=1&page_size=' + this.pageSize, '', '', null).subscribe((data) => {
      this.progress.hide();
      this.backoffice_userList = data.results;     
      this.loadUsersData(data.count);

      // this.backoffice_userList.forEach(x => {
      //   this.user_type=x.user_type.id;
      // });
      //this.user_type = data.results.user_type.id;
     // toastr.success("Users Fetched Successfully");
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }


  getBackofficeUser(id: number) {
    this.progress.show();
    this.editUserId = id;
    this.httpServices.request("get", 'user/backoffice-list/' + id, '', '', null).subscribe((data) => {
       this.backoffice_user=data;         
      //toastr.success("User Fetched Successfully");
      //this.loadUsersData(data);
      //this.is_seller=data.is_seller;
      this.user_Name = data.user_name;
      this.email_address = data.email_address;
      if (data.level != null && data.level != undefined && data.level != "") {
        this.service_Level = data.level.id;
      }
      else {
        this.service_Level = 0;
      }
      this.user_type = data.user_type.id;
      this.isVerifiedEmail = data.is_verified_email_address;
      // if(data.is_verified_email_address==true){
      //   this.isVerifiedEmail="Yes"
      // }else{
      //   this.isVerifiedEmail="No"
      // }

      if (data.is_active == true) {
        this.userStatus = 1;
      }
      else {
        this.userStatus = 0;
      }
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  private loadUsersData(recordCount: number) {
    if (this.backoffice_userList.length > 0) {
      this.userListGriddata = {
        data: orderBy(this.backoffice_userList, this.sort).slice(this.skip, this.skip + this.pageSize),
        //In case if gig list page has 40 record....SLICE to split record to next position as Record to be shown for Page is 10.
        //So remaining records are sliced to skip btn press.
        total: recordCount
      };
    } else {
      this.userListGriddata = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortUserlistChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadUsersData(this.totalRecordCount);
  }

  // Kendo Grid - data state change 
  public userListDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.userListGriddata = process(this.backoffice_userList, this.state);
  }


  onSearchBtnClick() {
    this.progress.show;
    var url = "user/backoffice-list?page=1&page_size=" + this.pageSize;
    if (this.search_userName != null && this.search_userName != '' && this.search_userName != undefined) {
      url = url + "&user_name=" + this.search_userName;
    }
    if (this.searchemail_Address != null && this.searchemail_Address != '' && this.searchemail_Address != undefined) {
      url = url + "&email_address=" + this.searchemail_Address;
    }
    if (this.getService.id != null && this.getService.id != 0 && this.getService.id != undefined) {
      url = url + "&level_id=" + this.getService.id;
    }
    if (this.getStatus.id != null && this.getStatus.id != 2 && this.getStatus.id != undefined) {
      //url = url + "&is_active=" + this.getStatus.id;

      if (this.getStatus.id == 1) {
        url = url + "&is_active=true";
      }
      else {
        url = url + "&is_active=false";
      }
    }

    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.backoffice_userList = data.results;
      this.loadUsersData(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  pageChange(event: PageChangeEvent): void {
    this.progress.show;
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "user/backoffice-list?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.search_userName != null && this.search_userName != '' && this.search_userName != undefined) {
      url = url + "&user_name=" + this.search_userName;
    }
    if (this.searchemail_Address != null && this.searchemail_Address != '' && this.searchemail_Address != undefined) {
      url = url + "&email_address=" + this.searchemail_Address;
    }
    if (this.getService.id != null && this.getService.id != 0 && this.getService.id != undefined) {
      url = url + "&level_id=" + this.getService.id;
    }

    if (this.getStatus.id != null && this.getStatus.id != 2 && this.getStatus.id != undefined) {
      //url = url + "&is_active=" + this.getStatus.id;

      if (this.getStatus.id == 1) {
        url = url + "&is_active=true";
      }
      else {
        url = url + "&is_active=false";
      }
    }
    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.backoffice_userList = data.results;
      this.loadUsersData(data.count);
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //public editMode: boolean = false;
  onUserslist_Update(Id: number, userType_id: number) {
    if (Id > 0) {
      jQuery('#Edit_User').modal({ backdrop: 'static', keyboard: false });
      //this.openEditModal = true;
      this.getBackofficeUser(Id);
    }
  }

  onAdd_userPop(){
    jQuery('#New_User').modal({ backdrop: 'static', keyboard: false });
    this.user_Name="";
    this.email_address="";
    this.user_type=1;
    //this.editMode=false;
  }

  onAddUser_Savebtn(){
    this.checkvalidation = true;
  let newUser: any = {

    user_name:'',
    user_type_id: 0,
    email_address:'',
    //level_id: 0,
    //is_active: true
  }

    newUser.user_name=this.user_Name;
    newUser.user_type_id=this.user_type;
    newUser.email_address=this.email_address;
    //newUser.is_active=true;

    if(this.user_Name!=null && this.user_Name!=undefined && this.user_Name!="" &&
    this.email_address!=null && this.email_address!=undefined && this.email_address!=""&&
    this.user_type!=0 && this.user_type!=null && this.user_type!=undefined){

    this.progress.show();
    this.httpServices.request("post","user/backoffice-user-create","","",newUser).subscribe((data)=>{
      toastr.success("User created Successfully");
      jQuery('#New_User').modal('hide');
      this.getUsersList();
      this.checkvalidation = false;
      this.progress.hide();
    },error=>{
      console.log(error);
      this.progress.hide();
    });
  }
}


  onUpdate_User() {

    this.getServiceLevel();
    this.checkvalidation = true;
    let update_backOffice_user: any = {

      // user_name:'',
      // user_type_id: 0,
      // email_address:'',
      level_id: 0,
      is_active: false
    }

    // update_backOffice_user.user_name=this.user_Name;
    // update_backOffice_user.user_type_id=this.user_type;
    // update_backOffice_user.email_address=this.email_address;
    update_backOffice_user.level_id = this.service_Level;
    update_backOffice_user.is_active = this.userStatus;

    this.progress.show();
    if (update_backOffice_user.level_id != 0 && update_backOffice_user.level_id != null && update_backOffice_user.level_id != undefined
      && update_backOffice_user.is_active != 0 && update_backOffice_user.is_active != null && update_backOffice_user.is_active != undefined) {
      this.httpServices.request('patch', 'user/backoffice-list/' + this.editUserId, '', '', update_backOffice_user).subscribe((data) => {
        toastr.success("User Record Updated Successfully");
        this.getUsersList();
        jQuery('#Edit_User').modal('hide');
        //this.openEditModal = false;
        //this.editMode=false;
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    }
  }

  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code == 39) &&
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }

  // =================================
  // on Seller Name to Seller Profile 
  // =================================
  OnUsernameClick(userId,user_type) {
    if (user_type == true) {
      let profile_mode: string = '';
      if (this.userAuthInfo.currentUser.user != undefined) {
        if (this.userAuthInfo.currentUser.user.id == userId) {
          profile_mode = 'self';
        }
        else {
          profile_mode = 'other';
        }
      }
      else {
        profile_mode = 'other';
      }
      this.encrypt_decrypt.seller_profile_id = userId
      this.encrypt_decrypt.seller_profile_mode = profile_mode;
      this.router.navigate(['/seller_profile'], {});
    } else {

      let profile_mode: string = '';
      if (this.userAuthInfo.currentUser.user != undefined) {
        if (this.userAuthInfo.currentUser.user.id == userId) {
          profile_mode = 'self';
        }
        else {
          profile_mode = 'other';
        }
      }
      else {
        profile_mode = 'other';
      }
      this.encrypt_decrypt.buyer_profile_id = userId
      this.encrypt_decrypt.buyer_profile_mode = profile_mode;
      this.router.navigate(['/buyer_profile'], {});
    }
  }

}
