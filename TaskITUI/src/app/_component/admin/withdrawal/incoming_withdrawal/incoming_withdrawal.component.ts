import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State,process } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-incoming_withdrawal',
  templateUrl: './incoming_withdrawal.component.html',
  styleUrls: ['./incoming_withdrawal.component.css']
})
export class Incoming_withdrawalComponent implements OnInit {

  constructor(private httpServices: HttpRequestService,private router: Router, private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService, private dateFormatter: DateFormatterService) { }
  public incoming_WithdrawalList:any;
  public incomingWithdrawal_gridData:GridDataResult|undefined;
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip:0,
    take:10
  }
  public searchAmount:any;
  public searchBuyer:any;
  public order_Date:any;
  public order_No:any;
  public pageNumber:any;
  public userAuthInfo: any;
  public getStatus: any = { id: 0, order_status_name: '' };
  public orderStatusList: any;

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.getIncoming_Withdrawals();
    this.getOrderstatus();
  }
  //Search Order Status Dropdown
  getOrderstatus() {
    this.httpServices.request('get', 'order_status/', '', '', null).subscribe((data) => {
      this.orderStatusList = data;
    }, error => {
      console.log(error);
    });
  }
  //*********************************
  // INCOMING WITHDRAWAL PART.
  //*********************************
  //To populate INCOMING  WITHDRAWAL in Kendo Grid
  getIncoming_Withdrawals() {
    this.progress.show();
    this.httpServices.request("get", "orders_wallet/incoming?page=1&page_size=" + this.pageSize, null, null, null)
      .subscribe((data) => {
        this.incoming_WithdrawalList = data.results;
        this.loadIncoming_withdrawalData(data.count);
        this.manipulateStatus();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }


  private loadIncoming_withdrawalData(recordCount: number): void {
    if (this.incoming_WithdrawalList.length > 0) {
      this.incomingWithdrawal_gridData = {
        data: orderBy(this.incoming_WithdrawalList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.incomingWithdrawal_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortIncoming_withdrawal(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadIncoming_withdrawalData(this.totalRecordCount);
  }


  public incoming_withdrawalDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.incomingWithdrawal_gridData = process(this.incoming_WithdrawalList, this.state);
  }

//PAGE CHANGE EVENT
  incoming_withdrawal_pageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "orders_wallet/incoming?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if(this.searchAmount!=null && this.searchAmount!=undefined && this.searchAmount!=''){
      url=url+'&amount='+ this.searchAmount
    }
    if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
      url = url + '&buyer_name=' + this.searchBuyer
    }
    if (this.order_Date != null && this.order_Date != undefined && this.order_Date != 0) {
      url = url + '&order_date=' + this.order_Date
    }
    if (this.order_No != null && this.order_No != undefined && this.order_No != 0) {
      url = url + '&order_no=' + this.order_No
    }
    //var url = url = "/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.incoming_WithdrawalList = data.results;
        this.loadIncoming_withdrawalData(data.count);
        this.manipulateStatus();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

//On Search Btn Click
  onSearchBtn(){
    //debugger
     var url = "orders_wallet/incoming?page=1&page_size=" + this.pageSize;

     if(this.searchAmount!=null && this.searchAmount!=undefined && this.searchAmount!=''){
      url=url+'&amount='+ this.searchAmount
    }
    if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
      url = url + '&buyer_name=' + this.searchBuyer
    }
    if (this.order_Date != null && this.order_Date != undefined && this.order_Date != 0) {
      url = url + '&order_date=' + this.order_Date
    }
    if (this.order_No != null && this.order_No != undefined && this.order_No != 0) {
      url = url + '&order_no=' + this.order_No
    }
    this.httpServices.request("get", url, null, null, null)
    .subscribe((data) => {
      this.incoming_WithdrawalList = data.results;
      this.loadIncoming_withdrawalData(data.count);
      this.manipulateStatus();
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  // ====================================
  // Manipulate status 
  // ===================================
  manipulateStatus() {
    if (this.incoming_WithdrawalList.length > 0) {
      for (let i = 0; i < this.incoming_WithdrawalList.length; i++) {
        if (this.incoming_WithdrawalList[i].order_status.id == 2) {
          this.incoming_WithdrawalList[i].property = "badge badge-success custom_badge_css";
        } else if (this.incoming_WithdrawalList[i].order_status.id == 1) {
          this.incoming_WithdrawalList[i].property = "badge badge-secondary custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 6) {
          this.incoming_WithdrawalList[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 7) {
          this.incoming_WithdrawalList[i].property = "badge badge-primary custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 8) {
          this.incoming_WithdrawalList[i].property = "badge badge-danger custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 3) {
          this.incoming_WithdrawalList[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 4) {
          this.incoming_WithdrawalList[i].property = "badge badge-warning custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 5) {
          this.incoming_WithdrawalList[i].property = "badge badge-success custom_badge_css";
        }
      }
    }
  }

  // =====================================
  // On Buyer Name to buyer Profile 
  // ======================================
  OnbuyernameClick(buyerId) {

    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == buyerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.buyer_profile_id = buyerId
    this.encrypt_decrypt.buyer_profile_mode = profile_mode;
    this.router.navigate(['/buyer_profile'], {});
  }

}
