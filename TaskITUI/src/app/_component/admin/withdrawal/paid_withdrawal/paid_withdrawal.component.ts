import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State,process } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-paid_withdrawal',
  templateUrl: './paid_withdrawal.component.html',
  styleUrls: ['./paid_withdrawal.component.css']
})
export class Paid_withdrawalComponent implements OnInit {

  constructor(private httpServices: HttpRequestService, private router: Router, private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService, private dateFormatter: DateFormatterService) { }

  public withdrawalList:any;
  public withdrawal_gridData:GridDataResult | undefined;
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip:0,
    take:10
  }
  public userAuthInfo: any;
  public pageNumber:any;
  public searchAmount:any;
  public payable_date:any
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.getWithdrawals();
  }

  getWithdrawals() {
    
    this.progress.show();
    this.httpServices.request("get", "orders_wallet/withdrawal?page=1&page_size=" + this.pageSize, null, null, null)
      .subscribe((data) => {
        this.withdrawalList = data.results;
        this.loadwithdrawalData(data.count);
        //this.manipulateStatus();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }


  private loadwithdrawalData(recordCount: number): void {
    if (this.withdrawalList.length > 0) {
      this.withdrawal_gridData = {
        data: orderBy(this.withdrawalList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.withdrawal_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortwithdrawal(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadwithdrawalData(this.totalRecordCount);
  }


  public withdrawalDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.withdrawal_gridData = process(this.withdrawalList, this.state);
  }

  withdrawalpageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "orders_wallet/withdrawal?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.payable_date != null && this.payable_date != undefined && this.payable_date != '') {
      url = url + '&payable_date=' + this.payable_date
    }
    if (this.searchAmount != null && this.searchAmount != undefined && this.searchAmount != '') {
      url = url + '&amount=' + this.searchAmount
    }
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.withdrawalList = data.results;
        this.loadwithdrawalData(data.count);
        //this.manipulateStatus();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }



  //On Search Btn Click
  onSearchBtn(){
    //debugger
     var url = "orders_wallet/withdrawal?page=1&page_size=" + this.pageSize;

      if (this.payable_date != null && this.payable_date != undefined && this.payable_date != '') {
      url = url + '&order_date=' + this.payable_date
    }
    if (this.searchAmount != null && this.searchAmount != undefined && this.searchAmount != '') {
      url = url + '&amount=' + this.searchAmount
    }
    // if(this.getStatus.id!=null && this.getStatus.id!=undefined && this.getStatus.id!=0){
    //   url=url+'&order_status_id='+ this.getStatus.id
    // }
    // if(this.searchOrderdate!=null && this.searchOrderdate!=undefined && this.searchOrderdate!=''){
    //   url=url+'&order_date='+ this.dateFormatter.format(new Date(this.searchOrderdate), 'yyyy-MM-dd');
    // }
    this.httpServices.request("get", url, null, null, null)
    .subscribe((data) => {
      this.withdrawalList = data.results;
      this.loadwithdrawalData(data.count);
      //this.manipulateStatus();
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }
}
