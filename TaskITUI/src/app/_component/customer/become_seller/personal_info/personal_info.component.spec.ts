/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Personal_infoComponent } from './personal_info.component';

describe('Personal_infoComponent', () => {
  let component: Personal_infoComponent;
  let fixture: ComponentFixture<Personal_infoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Personal_infoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Personal_infoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
