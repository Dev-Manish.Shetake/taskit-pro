import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import '@angular/localize/init'
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { SocialAuthService } from "angularx-social-login";
import { Socialusers } from './../../../../_models/socialusers'
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
declare var toastr: any;
declare var jQuery: any;

@Component({
  selector: 'app-personal_info',
  templateUrl: './personal_info.component.html',
  styleUrls: ['./personal_info.component.css']
})
export class Personal_infoComponent implements OnInit {

  constructor(private router: Router, private dateFormatter: DateFormatterService, private encrypt_decrypt: EncryptDecryptService, private progress: NgxSpinnerService, private httpServices: HttpRequestService, private authService: SocialAuthService) {
    // this.loadScripts();
  }

  //Method to dynamically load JavaScript 
  // loadScripts() {
  //   // This array contains all the files/CDNs 
  //   const dynamicScripts = [
  //     'assets/js/jquery.validate.min.js',
  //     'assets/js/jquery.steps.min.js',
  //     'assets/js/stepper_main.js'
  //   ];

  //   for (let i = 0; i < dynamicScripts.length; i++) {
  //     const node = document.createElement('script');
  //     node.src = dynamicScripts[i];
  //     node.type = 'text/javascript';
  //     node.async = false;
  //     document.getElementsByTagName('head')[0].appendChild(node);
  //   }
  // }
  public fullName: any;
  public lastName: any;
  public description: any;
  public photoCopy: boolean = false;
  public user_photo_data: any = undefined;
  public user_data_source: any;
  public photoScan: any;
  public block = "block";
  public none = "none";
  public showPhoto: boolean = false;
  public checkvalidation_personalInfo: boolean = false;
  public checkValidation_skillInfo: boolean = false;
  public languageProfeciency: any;
  public occupationList: any;
  public occupationListAnother: any;
  public countryList: any;
  public languageList: any;
  public titleList: any;
  public major: any;
  public userLanguage: any;
  public checkvalidation_languagepop: boolean = false;
  public getLanguage: any = { id: 0, lang_proficiency_name: '' };
  public getLangName: any = { id: 0, lang_name: '' };
  public getSkillLevel: any = { id: 0, skill_level_name: '' };
  public getSkill: any = { id: 0, skill_name: '' };
  public getOccupation: any = { id: 0, occupation_name: '' };
  public getOccupationAnother: any = { id: 0, occupation_name: '' };
  public getCountry: any = { id: 0, country_name: '' };
  public getTitle: any = { id: 0, education_title_name: '' };
  public getYear: any = { id: 0, year: 0 };
  public getYear_year: Date = null;
  public getYearCertification: any = { id: 0, year: 0 };
  public languagedata: any;
  public autoCompLangugeListItem: Array<{ id: string, code: string, lang_name: string }> = [];
  public autoCompUniversityListItem: Array<{ id: number, code: string, university_name: string }> = [];
  public autoCompMajorListItem: Array<{ id: string, education_major_name: string }> = [];
  public autoCompSkillItem: Array<{ id: string, skill_name: string }> = [];
  public langugeDetails: any = { id: 0, lang_name: '' };
  public yearList: Array<{ year_id: Number, year: Number }> = [];
  public yearListEducation: Array<{ year_id: Number, year: Number }> = [];
  public majorDetails: any = { id: 0, education_major_name: '' };
  public universityDetails: any = { university_name: '' };
  public skillDetails: any = { id: 0, skill_name: '' };
  public languageProficiencyDetails: Array<{ id: 0, list_id: Number, lang_id: Number, lang_name: string, lang_proficiency_id: Number, lang_proficiency_name: string }> = [];
  //public languageProficiencyDetails:any;
  public skillAndSkillLevelDetails: Array<{ id: 0, list_id: Number, skill_id: Number, skill_name: string, skill_level_id: Number, skill_level_name: string }> = [];
  public educationDetails: Array<{ id: Number, list_id: Number, country_id: Number, country_name: string, university_id: Number, university_name: string, title_id: Number, title_name: string, major_id: Number, major_name: string, year_id: Number, year_name: string }> = [];
  public certificationDetails: Array<{ id: Number, list_id: Number, certificateOrAward: string, certifiedFrom: string, year_id: Number, year_name: string }> = [];
  public list_id: any;
  public language_id: any;
  public list_id_skill: any;
  public id_skill: any;
  public list_id_certification: any = 0;
  public id_certification: any = 0;
  public editMode: boolean = false;
  public showAnotherOccupationList: boolean = false;
  public skillsLevelList: any;
  public skillsList: any;
  public showEducationTable: boolean = false;
  public showCertificationTable: boolean = false;
  public certificateOrAwards: any;
  public certifiedFrom: any;
  public personalWebsite: any;
  public charactersLeftForDescription: number = 0;
  public showMinCharValidation: boolean = false;
  public showLimitInfo: boolean = false;
  public emailAddress_acc_security: any;
  public phone_acc_security: any;
  public list_id_education: any = 0;
  public id_education: any = 0;
  social_users = new Socialusers();
  public showPersonalInfo: boolean = true;
  public showProfessionalInfo: boolean = false;
  public showLinkedAccounts: boolean = false;
  public showAccountsSecurity: boolean = false;
  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userId: any;
  public is_verified_mobile_no: boolean = false;
  public is_seller: any;
  public personal_info_id: any;
  public professional_info_id: any;
  public languageIds_alreadyExist: Array<{ lang_id: number }> = [];
  public educationIds_alreadyExist: Array<{ education_id: number }> = [];
  public certificationIds_alreadyExist: Array<{ certification_id: number }> = [];
  public skillIds_alreadyExist: Array<{ skill_id: number }> = [];
  public validateStatus: boolean = false;
  ngOnInit() {
    jQuery('body').scrollTop(0);
    jQuery(document).scrollTop(0);
    window.scrollTo(0, 0);
    // this.showLinkedAccounts=true;
    // this.showAccountsSecurity=true;
    // this.showProfessionalInfo = true;
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    this.userId = this.userAuthInfo.currentUser.user.id;
    this.is_seller = this.userAuthInfo.currentUser.user.is_seller;
    this.maxDate = new Date();
    if (this.is_seller == 0) {
      this.editMode = true;
      this.getPersonalInfoDetails();

    }
    else if (this.is_seller == null) {
      this.editMode = false;
      this.languageProficiencyDetails.push({ id: 0, list_id: 1, lang_id: 1, lang_name: 'English', lang_proficiency_id: 2, lang_proficiency_name: 'Basic' });
    }

    // this.getAccountSecurityDetails();
    this.showPersonalInfo = true;
  }
  // ======================================
  // get Personal Info details 
  // =======================================
  getPersonalInfoDetails() {
    this.progress.show();
    this.httpServices.request('get', 'sellers/info/' + this.userId, null, null, null).subscribe((data) => {
      this.fullName = data.personal_info.first_name;
      this.lastName = data.personal_info.last_name;
      this.user_data_source = data.personal_info.profile_picture;
      this.description = data.personal_info.description;
      this.personal_info_id = data.personal_info.id;
      if (this.languageProficiencyDetails.length > 0) {
        this.languageProficiencyDetails.splice(0, this.languageProficiencyDetails.length);
      }
      data.languages.forEach(element => {
        this.languageProficiencyDetails.push({
          id: element.id, list_id: element.id, lang_id: element.lang.id, lang_name: element.lang.lang_name,
          lang_proficiency_id: element.lang_proficiency.id, lang_proficiency_name: element.lang_proficiency.lang_proficiency_name
        });
        this.languageIds_alreadyExist.push({ lang_id: element.id });
      });
      this.progress.hide();
    });

  }
  // ========================================
  // get professional info details 
  // =========================================
  public maxDate: any;
  public focusedDate: any;
  getProfessionalInfoDetails() {
    this.progress.show();
    this.httpServices.request('get', 'sellers/user-professional-info/' + this.userId, null, null, null).subscribe((data) => {
      if (data != null) {
        this.professional_info_id = data.id;
        let currentYear = new Date().getFullYear();

        this.focusedDate = new Date(currentYear - 1, 11, 31);
        if (data.skills != null && data.skills != undefined && data.skills != "") {
          if (this.skillAndSkillLevelDetails.length > 0) {
            this.skillAndSkillLevelDetails.splice(0, this.skillAndSkillLevelDetails.length);
          }
          data.skills.forEach(element => {
            this.skillAndSkillLevelDetails.push({
              id: element.id, list_id: element.id, skill_id: element.skill.id, skill_name: element.skill.skill_name,
              skill_level_id: element.skill_level.id, skill_level_name: element.skill_level.skill_level_name
            });
            this.skillIds_alreadyExist.push({ skill_id: element.id });
          });
        }
        if (data.educations != null && data.educations != undefined && data.educations != "") {
          if (this.educationDetails.length > 0) {
            this.educationDetails.splice(0, this.educationDetails.length);
          }
          data.educations.forEach(element => {
            this.educationDetails.push({
              id: element.id,
              list_id: element.id, country_id: element.country.id, country_name: element.country.country_name,
              university_id: 0, university_name: element.university_name, title_id: element.education_title.id, title_name: element.education_title.education_title_name,
              major_id: element.education_major.id, major_name: element.education_major.education_major_name, year_id: 0, year_name: this.dateFormatter.format(new Date(Number(element.education_year), 1, 1), 'yyyy')
            });
            this.educationIds_alreadyExist.push({ education_id: element.id });
          });
          this.showEducationTable = true;
        }
        if (data.certifications != null && data.certifications != "" && data.certifications != undefined) {
          if (this.certificationDetails.length > 0) {
            this.certificationDetails.splice(0, this.certificationDetails.length);
          }
          data.certifications.forEach(element => {
            this.certificationDetails.push({
              id: element.id, list_id: element.id, certificateOrAward: element.certification_name, certifiedFrom: element.certification_from,
              year_id: 0, year_name: this.dateFormatter.format(new Date(Number(element.certification_year), 1, 1), 'yyyy')
            });
            this.certificationIds_alreadyExist.push({ certification_id: element.id });
          });
          this.showCertificationTable = true;
        }
        this.personalWebsite = data.personal_website;
      }
      this.getCountryList();
      this.getTitleList();
      this.getYearList();
      this.getSkillLevelList();
      this.progress.hide();
    });
  }
  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  onlyCharacters_withApostrophe(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code == 39) && // apostrophe
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }
  onlyCharactersAndNumbersWithAmper(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code == 38) &&  // ampersand
      !(code == 39) && // apostrophe
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123) && // lower alpha (a-z)
      !(code > 47 && code < 58)) { 
      event.preventDefault();
    }
  }
  // ===============================================
  // Linked Accounts: Google, Facebook
  // ================================================
  linkedAccounts_connect_click(socialProvider: string) {

    let socialPlatformProvider;
    if (socialProvider === 'Facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'Google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    //this.saves_response(socialProvider);
    this.authService.signIn(socialPlatformProvider).then(social_users => {
      this.saves_response(social_users);
    });
  }
  saves_response(social_users: Socialusers) {
    //console.log(social_users);
    this.progress.show();
    let login_info = { social_account_id: social_users.email, provider: social_users.provider };
    this.httpServices.request('post', 'sellers/linked-accounts', null, null, login_info).subscribe((data) => {
      if (data) {
        console.log(data);
      }
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }
  // ==========================================================
  // Accounts Security tab: on click of Add Email Address
  // ==========================================================
  open_pop_email_acc_security() {
    jQuery('#Add_email_address').modal({ backdrop: 'static', keyboard: false });
  }
  // ==========================================================
  // Accounts Security tab: on click of Phone Number
  // ==========================================================
  open_pop_phone_acc_security() {
    jQuery('#Add_Phone_Number').modal({ backdrop: 'static', keyboard: false });
    this.phone_acc_security = "";
  }
  // ===================================================
  // Accounts Security tab:on click of verify by Email
  // ====================================================
  verifyByEmailClick() {

  }
  // ===================================================
  // Accounts Security tab:on click of verify by SMS
  // ====================================================
  public showPhoneValidation: boolean = false;
  public phone_OTP_acc_security: any;
  verifyBySMSClick() {
    if (this.phone_acc_security != null && this.phone_acc_security != "" && this.phone_acc_security != undefined && this.phone_acc_security.length == 10) {
      let mob_number: any = {
        mobile_no: ""
      }
      this.progress.show();
      mob_number.mobile_no = this.phone_acc_security;
      this.httpServices.request('post', 'user/mobile-no-verification-otp', null, null, mob_number).subscribe((data) => {
        toastr.success("OTP has been sent successfully.");
        jQuery('#Add_Phone_Number').modal("hide");
        jQuery('#verify_OTP_Phone_Number').modal({ backdrop: 'static', keyboard: false });
        this.phone_OTP_acc_security = "";
        this.progress.hide();
      });

      this.showPhoneValidation = false;
    }
    else {
      this.showPhoneValidation = true;
    }
  }
  resendOTP()
  {
    let mob_number: any = {
      mobile_no: ""
    }
    this.progress.show();
    mob_number.mobile_no = this.phone_acc_security;
    this.httpServices.request('post', 'user/mobile-no-verification-resend-otp', null, null, null).subscribe((data) => {
      this.progress.hide();
      toastr.success("OTP has been sent successfully.");
      jQuery('#Add_Phone_Number').modal("hide");
        jQuery('#verify_OTP_Phone_Number').modal({ backdrop: 'static', keyboard: false });
        this.phone_OTP_acc_security = "";
    });
  }
  // ======================================
  // on Click of Verify OTP : Phone number 
  // =======================================
  public checkOTPValidation: boolean = false;
  verifyphoneOTP() {
    let mob_number: any = {
      otp: ""
    }
    mob_number.otp = this.phone_OTP_acc_security;
    this.checkOTPValidation = true;
    if (this.phone_OTP_acc_security != null && this.phone_OTP_acc_security != undefined && this.phone_OTP_acc_security.toString().trim() != '') {
      this.progress.show();
      this.httpServices.request('post', 'user/mobile-no-verify', null, null, mob_number).subscribe((data) => {
        toastr.success("Mobile No Verified");
        jQuery('#verify_OTP_Phone_Number').modal('hide');
        this.is_verified_mobile_no = true;
        this.checkOTPValidation = false;
        this.progress.hide();
      });
    }
  }
  // ============================
  // On description keypress 
  // ============================
  onDescriptionBlur() {
    if (this.description.length > 1 && this.description.length < 150) {
      this.showMinCharValidation = true;
      this.showLimitInfo = false;
    }
    else if (this.description.length == 150) {
      this.showLimitInfo = true;
      this.showMinCharValidation = false;
    }
    else {
      this.showLimitInfo = false;
      this.showMinCharValidation = false;
    }
    this.charactersLeftForDescription = 600 - this.description.length;
  }
  // =====================
  // handle language filter 
  // =======================
  // handleLanguageFilter(value: string) {
  //   this.autoCompLangugeListItem = undefined;
  //   this.langugeDetails.id = 0;
  //   this.langugeDetails.lang_name = "";
  //   if (value.length > 1) {
  //     this.httpServices.request('get', 'languages/?lang_name=' + value.replace('&', '~'), '', '', null)
  //       .subscribe(data => {
  //         if (data.length > 0) {
  //           this.autoCompLangugeListItem = data;
  //         }
  //       });
  //   }
  // }
  getLanguageList() {
    this.httpServices.request('get', 'languages/', '', '', null).subscribe(data => {
      if (data.length > 0) {
        this.languageList = data;
      }
    });
  }



  //=============================================
  // On Selecting Language 
  //=============================================
  onSpanLanguageSelected(dataitem: any) {
    this.langugeDetails.id = dataitem.id;
    this.langugeDetails.lang_name = dataitem.lang_name;
  }
  // ==============================
  //   On Change of user photo 
  // ==============================

  // abc($event){
  //   alert();
  //   jQuery("#imageUpload_custom").change(function(data){
  //     var imageFile = data.target.files[0];
  //     var reader = new FileReader();
  //     reader.readAsDataURL(imageFile);

  //     reader.onload = function(evt){
  //       jQuery('#imagePreview').attr('src', evt.target.result);
  //       jQuery('#imagePreview').hide();
  //       jQuery('#imagePreview').fadeIn(650);
  //     }
  //   });
  // }



  fileChangedPhoto(event: any) {
    this.progress.show();
    if (event.target.files.length == 0) {
      this.photoCopy = false;
      this.user_photo_data = undefined;
      this.photoScan = this.block;
      this.progress.hide();
      return;
    }
    this.photoScan = this.none;
    //To obtaine a File reference

    var files = event.target.files;
    const reader = new FileReader();
    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    if (files[0].type == "image/jpeg" || files[0].type == "image/png" || files[0].type == "image/jpg" || files[0].type == "image/bmp" || files[0].type == "image/gif") {

      fileType = "";
    }
    else {

      fileType = ("<li>" + "The file does not match file type." + "</li>");
    }

    //To check file Size according to upload conditions

    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "Your file does not match the upload conditions, Your file size is:" + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + "MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      this.progress.hide();
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {

      this.user_photo_data = event.target.files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.user_data_source = reader.result;
      }
      //this.user_data_source=event.target.result;
      if (this.user_photo_data.fileName != "" || this.user_photo_data.fileName != undefined) {
        formData.append("profile_picture", this.user_photo_data[0]);
        this.photoCopy = true;
        this.photoScan = this.none;

      }
      this.progress.hide();
    }
    else {
      //this.resetFileInput(this.file_image);
      this.user_photo_data = "";
      this.user_photo_data = undefined;
      this.photoCopy = false;
      this.photoScan = this.block;
      this.progress.hide();

    }
  }
  deleteImage() {
    //this.logistic_plan_urls.splice(this.logistic_plan_urls.findIndex(x => x.file_name == event.file_name), 1);
    this.user_photo_data = "";
    this.user_photo_data = undefined;
    this.photoCopy = false;
    this.photoScan = this.block;
    this.user_data_source = null;
  }

  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }
  // =============================
  // On Click of add language 
  // =============================
  onAddLanguageClick() {
    this.progress.show();
    this.userLanguage = '';
    this.getLanguage.id = 0;
    this.getLanguage.lang_proficiency_name = '';
    this.getLangName.id = 0;
    this.getLangName.lang_name = '';
    this.list_id = 0;
    this.language_id = 0;
    this.langugeDetails.id = 0;
    this.langugeDetails.lang_name = '';
    this.checkvalidation_languagepop = false;
    this.getLanguageList();
    this.getLanguageProficiency();
    jQuery('#Add_Language').modal({ backdrop: 'static', keyboard: false });
  }
  // =========================================
  // get language proficiency dropdown list 
  // ==========================================
  // get_Language() {
  //   this.progress.show();
  //   this.httpServices.request('get', 'languages/', null, null, null).subscribe((data) => {
  //     this.languageProfeciency = data;
  //     this.progress.hide();
  //   }, error => {
  //     this.progress.hide();
  //     console.log(error);
  //   });
  // }
  // =========================================
  // get language proficiency dropdown list 
  // ==========================================
  getLanguageProficiency() {
    this.progress.show();
    this.httpServices.request('get', 'languages/profeciency', null, null, null).subscribe((data) => {
      this.languageProfeciency = data;
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // =========================
  // on Click of Add language 
  // =========================
  addLanguage() {
    this.checkvalidation_languagepop = true;
    if (this.languageProficiencyDetails.filter(element => element.lang_id == this.getLangName.id).length > 0) {
      toastr.error("This language is already on your list.");
    }
    else {
      // if (this.langugeDetails.id != 0 && this.langugeDetails.id != undefined && this.langugeDetails.id != null && this.getLanguage.id != null && this.getLanguage.id != 0 && this.getLanguage.id != undefined) {
        if (this.getLangName.id != 0 && this.getLangName.id != undefined && this.getLangName.id != null && this.getLanguage.id != null && this.getLanguage.id != 0 && this.getLanguage.id != undefined) {
        let ListID = 0;
        if (this.languageProficiencyDetails.length > 0) {
          ListID = Math.max.apply(Math, this.languageProficiencyDetails.map(function (o) { return o.list_id; }));
        }
        this.languageProficiencyDetails.push({ id: 0, list_id: ListID + 1, lang_id: this.getLangName.id, lang_name: this.getLangName.lang_name, lang_proficiency_id: this.getLanguage.id, lang_proficiency_name: this.getLanguage.lang_proficiency_name });
        jQuery("#Add_Language").modal("hide");
        this.checkvalidation_languagepop = false;
      }

    }
  }
  // =======================================
  // on languge Proficiency Change 
  // ========================================
  onSelection() {
    this.languageProfeciency.forEach(element => {
      if (element.id == this.getLanguage.id) {
        this.getLanguage.lang_proficiency_name = element.lang_proficiency_name;
      }
    });
  }

  // =======================================
  // on languge Change 
  // ========================================
  onLanguageSelection() {
    this.languageList.forEach(element => {
      if (element.id == this.getLangName.id) {
        this.getLangName.lang_name = element.lang_name;
      }
    });
  }

  // ============================
  // Update language 
  // ============================
  updateLanguage(list_id: any, id: any) {
    this.checkvalidation_languagepop = true;
    if (this.languageProficiencyDetails.filter(element => element.lang_id == this.getLangName.id && element.list_id != list_id).length > 0) {
      toastr.error("This language is already on your list.");
    }
    else {
      if (this.getLangName.id != 0 && this.getLangName.id != undefined && this.getLangName.id != null && this.getLanguage.id != null && this.getLanguage.id != 0 && this.getLanguage.id != undefined) {
        this.languageProficiencyDetails.splice(this.languageProficiencyDetails.findIndex(x => x.list_id == list_id), 1);
        this.languageProficiencyDetails.push({ id: id, list_id: list_id, lang_id: this.getLangName.id, lang_name: this.getLangName.lang_name, lang_proficiency_id: this.getLanguage.id, lang_proficiency_name: this.getLanguage.lang_proficiency_name });
        jQuery("#Add_Language").modal("hide");
        this.checkvalidation_languagepop = false;
      }
    }
  }
  public languageId: any;
  public languageDeleteId: any;
  onclickDeleteLanguage(id: any, list_id: any) {
    this.languageId = list_id;
    this.languageDeleteId = id;
    jQuery('#delete_language_modal').modal({ backdrop: 'static', keyboard: false });
  }
  // =================================================
  // Remove Language from Language proficiency table
  // =================================================
  removeLanguage() {
    if (this.languageProficiencyDetails.length > 1) {
      if (this.languageDeleteId > 0) {
        this.httpServices.request('delete', 'user/languages/' + this.languageDeleteId, null, null, null).subscribe((data) => {
          this.languageProficiencyDetails.splice(this.languageProficiencyDetails.findIndex(x => x.id == this.languageDeleteId), 1);
          jQuery("#delete_language_modal").modal("hide");
          this.languageId = 0;
          this.languageDeleteId = 0;
        });
      }
      else {
        this.languageProficiencyDetails.splice(this.languageProficiencyDetails.findIndex(x => x.list_id == this.languageId), 1);
        jQuery("#delete_language_modal").modal("hide");
        this.languageId = 0;
        this.languageDeleteId = 0;
      }

    }
    else {
      toastr.error("Atleast one language is mandatory.");
      jQuery("#delete_language_modal").modal("hide");
    }
  }
  public deleteEducationId: any;
  public deleteEducationListId: any;
  ondeleteEducation(id: any, list_id) {
    this.deleteEducationId = id;
    this.deleteEducationListId = list_id;
    jQuery('#delete_education_modal').modal({ backdrop: 'static', keyboard: false });
  }
  // =======================================
  // Remove education from education table 
  // =======================================
  removeEducation() {
    if (this.educationDetails.length > 1) {
      if (this.deleteEducationId > 0) {
        this.httpServices.request('delete', 'user/educations/' + this.deleteEducationId, null, null, null).subscribe((data) => {
          this.educationDetails.splice(this.educationDetails.findIndex(x => x.id == this.deleteEducationId), 1);
          jQuery("#delete_education_modal").modal("hide");
          this.deleteEducationId = 0;
          this.deleteEducationListId = 0;
        });
      }
      else {
        this.educationDetails.splice(this.educationDetails.findIndex(x => x.list_id == this.deleteEducationListId), 1);
        jQuery("#delete_education_modal").modal("hide");
        this.deleteEducationId = 0;
        this.deleteEducationListId = 0;
      }

    }
    else {
      toastr.error("Atleast one Education is mandatory.");
      jQuery("#delete_education_modal").modal("hide");
    }
  }
  public deleteCertificationId: any;
  public deleteCertificationListId: any;
  onClickDeleteCertification(id: any, list_id: any) {
    this.deleteCertificationId = id;
    this.deleteCertificationListId = list_id;
    jQuery('#delete_certification_modal').modal({ backdrop: 'static', keyboard: false });
  }
  // ===============================================
  // Remove Certification from certification table 
  // ===============================================
  removeCertification() {
    if (this.certificationDetails.length > 1) {
      if (this.deleteCertificationId > 0) {
        this.httpServices.request('delete', 'user/certifications/' + this.deleteCertificationId, null, null, null).subscribe((data) => {
          this.certificationDetails.splice(this.certificationDetails.findIndex(x => x.id == this.deleteCertificationId), 1);
          jQuery("#delete_certification_modal").modal("hide");
          this.deleteCertificationId = 0;
          this.deleteCertificationListId = 0;
        });
      }
      else {
        this.certificationDetails.splice(this.certificationDetails.findIndex(x => x.list_id == this.deleteCertificationListId), 1);
        jQuery("#delete_certification_modal").modal("hide");
        this.deleteCertificationId = 0;
        this.deleteCertificationListId = 0;
      }

    }
    else {
      toastr.error("Atleast one certification is mandatory.");
      jQuery("#delete_education_modal").modal("hide");
    }
  }
  // ==================================
  // On Country Change 
  // ===================================
  onCountryChange() {
    this.countryList.forEach(element => {
      if (element.id == this.getCountry.id) {
        this.getCountry.country_name = element.country_name;
      }
    });

  }
  // =================================
  // On Title Change 
  // =================================
  onTitleChange() {
    this.titleList.forEach(element => {
      if (element.id == this.getTitle.id) {
        this.getTitle.education_title_name = element.education_title_name;
      }
    });

  }
  // =====================================
  // On year Change in Education section 
  // ======================================
  onEducationYearChange() {
    //
    if (this.getYear_year != null) {
      this.yearListEducation.forEach(element => {
        if (element.year == this.getYear_year.getFullYear()) {
          this.getYear.id = element.year_id;
          this.getYear.year = element.year;
        }
      });

    }
  }
  // =================================
  // get year list 
  // ================================
  getYearList() {
    //
    var year = new Date().getFullYear();


    for (var i = 1; i < 43; i++) {
      this.yearList.push({ year_id: i, year: year - 42 + i });
      this.yearListEducation.push({ year_id: i, year: year - 42 + i });
    }
  }
  // ==================================
  // Open Language pop up on Edit icon 
  // ==================================
  openLangugeDetailPop(list_id: any) {

    this.progress.show();
    if (this.languageProfeciency == null || this.languageProfeciency == undefined || this.languageProfeciency == '') {
      this.httpServices.request('get', 'languages/profeciency', null, null, null).subscribe((data) => {
        this.languageProfeciency = data;
        this.httpServices.request('get', 'languages/', '', '', null).subscribe(data_lang => {
          if (data_lang.length > 0) {
            this.languageList = data_lang;
          }
        });
        let filterData = this.languageProficiencyDetails.filter(x => x.list_id == list_id);
        if (filterData.length > 0) {
          this.getLanguage.id = filterData[0].lang_proficiency_id;
          this.getLanguage.lang_proficiency_name = filterData[0].lang_proficiency_name;
          this.getLangName.id = filterData[0].lang_id;
          this.getLangName.lang_name = filterData[0].lang_name;
          this.list_id = filterData[0].list_id;
          this.language_id = filterData[0].id;
          jQuery('#Add_Language').modal({ backdrop: 'static', keyboard: false });
        }
        this.progress.hide();
      }, error => {
        this.progress.hide();
        console.log(error);
      });
    }
    else {
      let filterData = this.languageProficiencyDetails.filter(x => x.list_id == list_id);
      if (filterData.length > 0) {
        this.getLanguage.id = filterData[0].lang_proficiency_id;
        this.getLanguage.lang_proficiency_name = filterData[0].lang_proficiency_name;
        this.getLangName.id = filterData[0].lang_id;
        this.getLangName.lang_name = filterData[0].lang_name;
        this.list_id = filterData[0].list_id;
        this.language_id = filterData[0].id;
        jQuery('#Add_Language').modal({ backdrop: 'static', keyboard: false });
      }
      this.progress.hide();
    }

  }
  // ===================================
  // handle University filter 
  // ===================================
  handleUniversityFilter(value: string) {
    this.autoCompUniversityListItem = undefined;
    this.universityDetails.university_name = "";
   // this.universityDetails.id = 0;
    if (value.length > 1) {
      this.httpServices.request('get', 'user/university-list?keyword=' + value.replace('&', '~'), '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompUniversityListItem = data;
          }
          else {
            this.universityDetails.university_name = value;
          }
        });
    }
    this.universityDetails.university_name = value;
  }
  // =======================================
  // on Selecting University 
  // =======================================
  onSpanUniversitySelected(dataItem: any) {
    //this.universityDetails.id = dataItem.id;
    this.universityDetails.university_name = dataItem.university_name;
  }

  // =====================
  // handle language filter 
  // =======================
  handleMajorFilter(value: string) {
    this.autoCompMajorListItem = undefined;
    this.majorDetails.id = 0;
    this.majorDetails.education_major_name = "";
    if (value.length > 1) {
      this.httpServices.request('get', 'education/education-major?education_major_name=' + value.replace('&', '~'), '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompMajorListItem = data;
          }
        });
    }
    this.majorDetails.education_major_name = value;
  }
  //=============================================
  // On Selecting Language 
  //=============================================
  onSpanMajorSelected(dataitem: any) {
    this.majorDetails.id = dataitem.id;
    this.majorDetails.education_major_name = dataitem.education_major_name;
  }
  // ==================================
  // cancel btn click of language popup 
  // ===================================
  cancelLanguagepopup() {
    jQuery("#Add_Language").modal("hide");
  }
  // ===========================================
  // cancel btn click of certification section 
  // ===========================================
  cancelCertificationSection() {
    this.showCertificationTable = true;
  }
  // =======================================
  // Cancel btn click of education section 
  // ========================================
  cancelEducationSection() {
    this.showEducationTable = true;
  }

  public activetab: any = "personal_info";
  onClickPersonalInfo() {

    this.activetab = "personal_info";
  }
  onClickProfessionalInfo() {
    if (this.editMode == true) {
      this.activetab = "professional_info";
      this.showProfessionalInfo = true;
      this.getProfessionalInfoDetails();
    }
    else {
      if (this.showProfessionalInfo == true) {
        this.activetab = "professional_info";
        this.getProfessionalInfoDetails();
      }
    }

  }
  onClickLinkedAccounts() {
    if (this.showLinkedAccounts == true) {
      this.activetab = "linked_accounts";
      this.getLinkedAccountsData();
    }
  }
  onClickAccntSec() {
    if (this.showAccountsSecurity == true) {
      this.activetab = "accounts_security";
      this.getAccountSecurityDetails();
    }
  }
  onProfessionalPrevious() {
    jQuery('body').scrollTop(0);
    jQuery(document).scrollTop(0);
    window.scrollTo(0, 0);
    this.activetab = "personal_info";
  }
  // ======================================
  // ON Click of Next on Personal Info tab 
  // ======================================
  public key = CryptoJS.enc.Utf8.parse('SOMERANDOMKEY123');
  onClickofNext_personalInfo() {
    jQuery('body').scrollTop(0);
    jQuery(document).scrollTop(0);
    window.scrollTo(0, 0);
    this.checkvalidation_personalInfo = true;
    if (this.fullName != null && this.fullName.trim() != '' && this.fullName != undefined
      && this.lastName != null && this.lastName.trim() != '' && this.lastName != undefined
      && this.description != null && this.description != '' && this.description != undefined
      && this.languageProficiencyDetails.length > 0 && (this.user_photo_data != undefined || this.user_data_source != undefined)) {
      this.progress.show();

      this.userInfo = this.userAuthInfo.currentUser;
      if (this.personal_info_id == null || this.personal_info_id == undefined || this.personal_info_id == 0) {
        let sellerData: any = {
          first_name: '',
          last_name: '',
          description: '',
          languages: [{
            lang_id: 0,
            lang_proficiency_id: 0
          }]
        }

        sellerData.first_name = this.fullName,
          sellerData.last_name = this.lastName,
          sellerData.description = this.description,
          //sellerData.profile_picture = this.user_photo_data[0];
          this.languageProficiencyDetails.forEach(element => {
            sellerData.languages.push({ lang_id: element.lang_id, lang_proficiency_id: element.lang_proficiency_id })
          });
        let formData = new FormData();
        if (this.user_photo_data != null && this.user_photo_data != undefined && this.user_photo_data != "") {
          formData.append("profile_picture", this.user_photo_data[0]);
        }
        sellerData.languages.splice(sellerData.languages.findIndex(x => x.lang_id == 0), 1);
        this.httpServices.request('post', 'sellers/personal-info', null, null, sellerData).subscribe((data) => {
          this.personal_info_id = data.id;
          this.httpServices.request('patch', 'sellers/personal-info/' + data.id, null, null, formData).subscribe((data) => {
            toastr.success("Personal Information has been saved successfully.");

            this.activetab = "professional_info";
            this.showProfessionalInfo = true;
            this.showPersonalInfo = true;
            this.userInfo.user.is_seller = 0;
            this.getCountryList();
            this.getTitleList();
            this.getYearList();
            this.getSkillLevelList();
            this.getSkillList();
            if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
              this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
              localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
            }
            else {
              localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
            }
            this.progress.hide();
          }, error => {
            this.progress.hide();
            console.log(error);
          });
        }, error => {
          this.progress.hide();
          console.log(error);
        });
      }
      else {  //patch personal info
        let sellerData: any = {
          first_name: '',
          last_name: '',
          description: '',
          languages: [{
            id: 0,
            lang_id: 0,
            lang_proficiency_id: 0
          }]
        }

        sellerData.first_name = this.fullName;
        sellerData.last_name = this.lastName;
        sellerData.description = this.description;
        //sellerData.profile_picture = this.user_photo_data[0];
        this.languageProficiencyDetails.forEach(element => {
          sellerData.languages.push({ id: element.list_id, lang_id: element.lang_id, lang_proficiency_id: element.lang_proficiency_id })
        });
        sellerData.languages.splice(sellerData.languages.findIndex(x => x.lang_id == 0), 1);
        let formData = new FormData();
        if (this.user_photo_data != null && this.user_photo_data != undefined && this.user_photo_data != "") {
          formData.append("profile_picture", this.user_photo_data[0]);
        }
        this.httpServices.request('patch', 'sellers/personal-info/' + this.personal_info_id, null, null, sellerData).subscribe((data) => {
          // this.httpServices.request('patch', 'sellers/personal-info/' + data.id, null, null, formData).subscribe((data) => {
          this.updateLanguageData();
          toastr.success("Personal Information has been updated successfully.");

          this.activetab = "professional_info";
          this.showProfessionalInfo = true;
          this.showPersonalInfo = true;
          this.getCountryList();
          this.getTitleList();
          this.getYearList();
          this.getSkillLevelList();
          this.getSkillList();
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });
        // }, error => {
        //   this.progress.hide();
        //   console.log(error);
        // });
      }

    }
  }
  // ==================================================
  // Update languages 
  // ==================================================
  updateLanguageData() {
    if (this.languageProficiencyDetails.length > 0) {
      for (let i = 0; i < this.languageProficiencyDetails.length; i++) {
        let lang_body: any = {
          lang_id: 0,
          lang_proficiency_id: 0
        }
        // if (this.languageIds_alreadyExist.filter(x => x.lang_id == this.languageProficiencyDetails[i].list_id).length > 0) {
        if (this.languageProficiencyDetails[i].id > 0) {
          lang_body.lang_id = this.languageProficiencyDetails[i].lang_id;
          lang_body.lang_proficiency_id = this.languageProficiencyDetails[i].lang_proficiency_id;
          this.httpServices.request('patch', 'user/languages/' + this.languageProficiencyDetails[i].list_id, null, null, lang_body).subscribe((data) => {
          });
        }
        else {
          lang_body.lang_id = this.languageProficiencyDetails[i].lang_id;
          lang_body.lang_proficiency_id = this.languageProficiencyDetails[i].lang_proficiency_id;
          this.httpServices.request('post', 'user/languages', null, null, lang_body).subscribe((data) => {
          })
        }
      }
    }
    this.getProfessionalInfoDetails();
  }
  encrypt(msgString, key) {
    // msgString is expected to be Utf8 encoded
    var iv = CryptoJS.lib.WordArray.random(16);
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify(msgString), key, {
      iv: iv
    });
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
  }
  // ===========================
  // Get occupation list 
  // ===========================
  getOccupationList() {
    this.progress.show();
    this.httpServices.request('get', 'occupations/', null, null, null).subscribe((data) => {
      this.occupationList = data;
      if (this.editMode == false) {
        this.getOccupation.id = 0;
      }
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // ==================================
  // Click on Add New occupation 
  // ==================================
  clickonAddMoreOccupation() {

    this.httpServices.request('get', 'occupations/', null, null, null).subscribe((data) => {
      this.occupationListAnother = data;
      this.getOccupationAnother.id = 0;
      this.showAnotherOccupationList = true;
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // ==================================
  // Click on Trash occupation 
  // ==================================
  trashOccupation() {
    this.showAnotherOccupationList = false;
    this.getOccupationAnother.id = 0;
    this.getOccupationAnother.occupation_name = '';
    this.getOccupationAnother = [];
  }
  // ==================================
  // On change of occupation another 
  // ==================================
  onOccupationAnotherChange() {

    if (this.getOccupation.id == this.getOccupationAnother.id) {
      toastr.error("You have already selected this occupation, please select any other.");
      this.getOccupationAnother.id = 0;
      this.clickonAddMoreOccupation();
    }
  }
  // ================================
  // On Change of occuption 
  // ================================
  onOccupationChange() {
    if (this.getOccupation.id == this.getOccupationAnother.id) {
      toastr.error("You have already selected this occupation, please select any other.");
      this.getOccupation.id = 0;
    }
  }
  getSkillLevelList() {
    this.httpServices.request('get', 'skills/level', null, null, null).subscribe((data) => {
      this.skillsLevelList = data;
      //this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }

  
  getSkillList() {
    this.httpServices.request('get', 'skills/', null, null, null).subscribe((data) => {
      this.skillsList = data;
      //this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }

  // ===========================
  // Open Add Skill Popup 
  // ============================
  openAddSkillPop() {
    this.progress.show();
    this.getSkillLevel.id = 0;
    this.getSkillLevel.skill_level_name = '';
    this.getSkill.id = 0;
    this.getSkill.skill_name = '';
    this.skillDetails.id = 0;
    this.list_id_skill = 0;
    this.id_skill = 0;
    this.skillDetails.skill_name = '';
    this.checkValidation_skillInfo = false;
    this.httpServices.request('get', 'skills/level', null, null, null).subscribe((data) => {
      this.skillsLevelList = data;
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
    this.httpServices.request('get', 'skills', null, null, null).subscribe((data_skill) => {
      this.skillsList = data_skill;
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // =====================
  // handle Skill filter 
  // =======================
  handleSkillFilter(value: string) {
    this.autoCompSkillItem = undefined;
    this.skillDetails.id = 0;
    this.skillDetails.skill_name = "";
    if (value.length > 1) {
      this.httpServices.request('get', 'skills/?skill_name=' + value.replace('&', '~'), '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompSkillItem = data;
          }
        });
    }

  }
  //=============================================
  // On Selecting Skill 
  //=============================================
  onSpanSkillSelected(dataitem: any) {
    this.skillDetails.id = dataitem.id;
    this.skillDetails.skill_name = dataitem.skill_name;
  }
  // ===============================
  // On Change of skill level 
  // =================================
  onskillLevelChange() {

    this.skillsLevelList.forEach(element => {
      if (element.id == this.getSkillLevel.id) {
        this.getSkillLevel.skill_level_name = element.skill_level_name;
      }
    });

  }

  // ===============================
  // On Change of skill 
  // =================================
  onskillChange() {
    this.skillsList.forEach(element => {
      if (element.id == this.getSkill.id) {
        this.getSkill.skill_name = element.skill_name;
      }
    });
  }

  // =========================
  // on Click of Add skill
  // =========================
  addSkill() {

    this.checkValidation_skillInfo = true;
    if (this.skillAndSkillLevelDetails.filter(element => element.skill_id == this.getSkill.id).length > 0) {
      toastr.error("This skill is already added in your list.");
    }
    else {
      if (this.getSkill.id != 0 && this.getSkillLevel.id != 0) {
        let listID = 0;
        if (this.skillAndSkillLevelDetails.length > 0) {
          listID = Math.max.apply(Math, this.skillAndSkillLevelDetails.map(function (o) { return o.list_id; }));
        }
        this.skillAndSkillLevelDetails.push({ id: 0, list_id: listID + 1, skill_id: this.getSkill.id, skill_name: this.getSkill.skill_name, skill_level_id: this.getSkillLevel.id, skill_level_name: this.getSkillLevel.skill_level_name });
        jQuery('#Add_Skills').modal('hide');
        this.checkValidation_skillInfo = false;
      }
    }
  }
  // ============================
  // Update skill
  // ============================
  updateSkill(list_id: any, id: any) {

    this.checkValidation_skillInfo = true;
    if (this.skillAndSkillLevelDetails.filter(element => element.skill_id == this.getSkill.id && element.list_id != list_id).length > 0) {
      toastr.error("This skill is already added in your list.");
    }
    else {
      if (this.getSkill.id != 0 && this.getSkillLevel.id != 0) {
        this.skillAndSkillLevelDetails.splice(this.skillAndSkillLevelDetails.findIndex(x => x.list_id == list_id), 1);
        this.skillAndSkillLevelDetails.push({ id: id, list_id: list_id, skill_id: this.getSkill.id, skill_name: this.getSkill.skill_name, skill_level_id: this.getSkillLevel.id, skill_level_name: this.getSkillLevel.skill_level_name });
        jQuery('#Add_Skills').modal('hide');
        this.checkValidation_skillInfo = false;
      }
    }
  }
  // ==================================
  // Open Skill pop up on Edit icon
  // ==================================
  openSkillDetailPop(list_id: any) {

    let filterData = this.skillAndSkillLevelDetails.filter(x => x.list_id == list_id);
    if (filterData.length > 0) {
      this.getSkillLevel.id = filterData[0].skill_level_id;
      this.getSkillLevel.skill_level_name = filterData[0].skill_level_name;
      this.getSkill.id = filterData[0].skill_id;
      this.getSkill.skill_name = filterData[0].skill_name;
      this.list_id_skill = filterData[0].list_id;
      this.id_skill = filterData[0].id;
      jQuery('#Add_Skills').modal({});
    }
  }
  public skillDeleteId: any;
  public skillDeleteListId: any;
  ondeleteSkill(id: any, list_id: any) {
    this.skillDeleteId = id;
    this.skillDeleteListId = list_id;
    jQuery('#delete_skill_modal').modal({ backdrop: 'static', keyboard: false });
  }
  // =================================================
  // Remove Skill from Skills table
  // =================================================
  removeSkill() {
    if (this.skillAndSkillLevelDetails.length > 1) {
      if (this.skillDeleteId > 0) {
        this.httpServices.request('delete', 'user/skills/' + this.skillDeleteId, null, null, null).subscribe((data) => {
          this.skillAndSkillLevelDetails.splice(this.skillAndSkillLevelDetails.findIndex(x => x.id == this.skillDeleteId), 1);
          jQuery('#delete_skill_modal').modal('hide');
          this.skillDeleteId = 0;
          this.skillDeleteListId = 0;
        });
      }
      else {
        this.skillAndSkillLevelDetails.splice(this.skillAndSkillLevelDetails.findIndex(x => x.list_id == this.skillDeleteListId), 1);
        jQuery('#delete_skill_modal').modal('hide');
        this.skillDeleteId = 0;
        this.skillDeleteListId = 0;
      }

    }
    else {
      toastr.error("Atleast one skill is mandatory.");
      jQuery('#delete_skill_modal').modal('hide');
    }
  }
  // =========================
  // get countryList
  // =========================
  getCountryList() {
    this.httpServices.request('get', 'country/', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.countryList = data;
        }
      });
  }
  // =============================
  // get title list
  // =============================
  getTitleList() {
    this.httpServices.request('get', 'education/education-title', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.titleList = data;
        }
      });
  }
  // ==============================================
  // on Click of Add Education details -- add btn
  // ===============================================
  public checkEducation: boolean = false;
  public duplicateEducationFlag: boolean = false;
  addEducationDetails() {

    this.checkEducation = true;
    this.duplicateEducationFlag = false;
    if (this.getCountry.id != null && this.getCountry.id != 0 && this.getCountry.id != undefined &&
      this.universityDetails.university_name != null && this.universityDetails.university_name.trim() != "" && this.universityDetails.university_name != undefined && this.getTitle.id != null && this.getTitle.id != 0 && this.getTitle.id != undefined
      && ((this.majorDetails.id == 0 && this.majorDetails.education_major_name.trim() != '') || (this.majorDetails.id != 0)) && this.getYear.year != "" && this.getYear.year != undefined && this.getYear.year != null) {


      this.educationDetails.forEach(element => {
        if (element.country_id == this.getCountry.id && element.university_name == this.universityDetails.university_name &&
          element.title_id == this.getTitle.id && element.major_name == this.majorDetails.education_major_name && element.year_name == this.getYear.year) {
          this.duplicateEducationFlag = true;
        }
      });
      if (this.duplicateEducationFlag == false) {
        let listID = 0;
        if (this.educationDetails.length > 0) {
          listID = Math.max.apply(Math, this.educationDetails.map(function (o) { return o.list_id; }));
        }
        this.educationDetails.push({
          id: 0,
          list_id: listID + 1
          , country_id: this.getCountry.id
          , country_name: this.getCountry.country_name
          , university_id: 0
          , university_name: this.universityDetails.university_name
          , title_id: this.getTitle.id
          , title_name: this.getTitle.education_title_name
          , major_id: this.majorDetails.id
          , major_name: this.majorDetails.education_major_name
          , year_id: this.getYear.id
          , year_name: this.getYear.year
        });
        this.showEducationTable = true;
        this.checkEducation = false;
      }
      else {
        toastr.error('This education record is already in your list.');
      }
    }
  }
  // ============================
  // Update Education --update btn
  // ============================
  updateEducationDetails(list_id: any, id: any) {

    this.checkEducation = true;
    this.duplicateEducationFlag = false;
    if (this.getCountry.id != null && this.getCountry.id != 0 && this.getCountry.id != undefined && this.universityDetails.university_name.trim() != "" &&
      this.universityDetails.university_name != null && this.universityDetails.university_name != undefined && this.getTitle.id != null && this.getTitle.id != 0 && this.getTitle.id != undefined
      && ((this.majorDetails.id == 0 && this.majorDetails.education_major_name.trim() != '') || (this.majorDetails.id != 0)) && this.majorDetails.id != null && this.majorDetails.id != undefined && this.getYear.year != "" && this.getYear.year != undefined && this.getYear.year != null) {

      this.educationDetails.forEach(element => {
        if (element.country_id == this.getCountry.id && element.university_name == this.universityDetails.university_name &&
          element.title_id == this.getTitle.id && element.major_name == this.majorDetails.education_major_name && element.year_name == this.getYear.year && element.list_id!=list_id) {
          this.duplicateEducationFlag = true;
        }
      });
      if (this.duplicateEducationFlag == false) {
        this.educationDetails.splice(this.educationDetails.findIndex(x => x.list_id == list_id), 1);
        this.educationDetails.push({
          id: id,
          list_id: list_id,
          country_id: this.getCountry.id,
          country_name: this.getCountry.country_name,
          university_id: 0,
          university_name: this.universityDetails.university_name,
          title_id: this.getTitle.id,
          title_name: this.getTitle.education_title_name,
          major_id: this.majorDetails.id,
          major_name: this.majorDetails.education_major_name,
          year_id: this.getYear.id
          , year_name: this.getYear.year
        });
        this.showEducationTable = true;
        this.checkEducation = false;
      }
      else {
        toastr.error('This education record is already in your list.');
      }
    }
  }
  // ==================================
  // Open Education section on Edit icon
  // ==================================
  openEducationDetailSection(list_id: any) {
    let filterData = this.educationDetails.filter(x => x.list_id == list_id);
    if (filterData.length > 0) {
      this.getCountry.id = filterData[0].country_id;
      this.getCountry.country_name = filterData[0].country_name;
      this.getTitle.id = filterData[0].title_id;
      this.getTitle.title_name = filterData[0].title_name;
      this.list_id_education = filterData[0].list_id;
      this.id_education = filterData[0].id;
      this.majorDetails.id = filterData[0].major_id;
      this.majorDetails.education_major_name = filterData[0].major_name;
      this.getYear.id = filterData[0].year_id;
      this.getYear.year = filterData[0].year_name;
      this.getYear_year = new Date(Number(filterData[0].year_name), 1, 1);
      this.universityDetails.university_name = filterData[0].university_name;
     // this.universityDetails.university_id = filterData[0].university_id;
      this.showEducationTable = false;
    }
  }
  // ==================================
  // On Click of Add New education
  // ==================================
  onAddNewEducation() {
    this.showEducationTable = false;
    this.getCountry.id = 0;
    this.getCountry.country_name = '';
    this.getTitle.id = 0;
    this.getTitle.title_name = '';
    this.list_id_education = 0;
    this.id_education = 0;
    this.majorDetails.id = 0;
    this.majorDetails.education_major_name = '';
    this.getYear.id = 0;
    this.getYear.year = "";
    this.getYear_year = null;
    //this.universityDetails.university_id = 0;
    this.universityDetails.university_name = '';
    this.checkEducation = false;

  }
  // ====================================
  // On click of add new certification
  // ====================================
  onAddNewCerification() {
    this.showCertificationTable = false;
    this.list_id_certification = 0;
    this.id_certification = 0;
    this.certificateOrAwards = '';
    this.certifiedFrom = '';
    this.getYearCertification.id = 0;
    this.getYearCertification.year = 0;
    this.getCertificationYear = null;
    this.checkCertification = false;
  }
  // ==============================================
  // on Click of Add Certification details -- add btn
  // ===============================================
  public checkCertification: boolean = false;
  addCertificationDetails() {

    this.checkCertification = true;
    this.duplicateCertificateFlag = false;
    if (this.certificateOrAwards != null && this.certificateOrAwards != undefined && this.certificateOrAwards.trim() != "" && this.certifiedFrom != null && this.certifiedFrom.trim() != "" && this.certifiedFrom != undefined
      && this.getYearCertification.year != null && this.getYearCertification.year != undefined && this.getYearCertification.year != "") {

      this.certificationDetails.forEach(element => {
        if (element.certificateOrAward == this.certificateOrAwards && element.certifiedFrom == this.certifiedFrom &&
          element.year_id == this.getYearCertification.id && element.year_name == this.getYearCertification.year) {
          this.duplicateCertificateFlag = true;
        }
      });

      if (this.duplicateCertificateFlag == false) {
        let listID = 0;
        if (this.certificationDetails.length > 0) {
          listID = Math.max.apply(Math, this.certificationDetails.map(function (o) { return o.list_id; }));
        }

        this.certificationDetails.push({
          id: 0, list_id: listID + 1, certificateOrAward: this.certificateOrAwards
          , certifiedFrom: this.certifiedFrom, year_id: this.getYearCertification.id
          , year_name: this.getYearCertification.year
        });
        this.showCertificationTable = true;
        this.checkCertification = false;
      } else {
        toastr.error("This certificate record is already in your list.");
      }
    }

  }
  // ============================
  // Update Certification --update btn
  // ============================
  public duplicateCertificateFlag: boolean = false;
  updateCertificationDetails(list_id: any, id: any) {

    this.checkCertification = true;
    this.duplicateCertificateFlag = false;
    if (this.certificateOrAwards != null && this.certificateOrAwards != undefined && this.certificateOrAwards.trim() != "" && this.certifiedFrom != null && this.certifiedFrom.trim() != "" && this.certifiedFrom != undefined
      && this.getYearCertification.year != null && this.getYearCertification.year != undefined && this.getYearCertification.year != "") {

      this.certificationDetails.forEach(element => {
        if (element.certificateOrAward == this.certificateOrAwards && element.certifiedFrom == this.certifiedFrom &&
          element.year_id == this.getYearCertification.id && element.year_name == this.getYearCertification.year && element.list_id!=list_id) {
          this.duplicateCertificateFlag = true;
        }
      });

      if (this.duplicateCertificateFlag == false) {
        this.certificationDetails.splice(this.certificationDetails.findIndex(x => x.list_id == list_id), 1);
        this.certificationDetails.push({ id: id, list_id: list_id, certificateOrAward: this.certificateOrAwards, certifiedFrom: this.certifiedFrom, year_id: this.getYearCertification.id, year_name: this.getYearCertification.year });
        this.showCertificationTable = true;
        this.checkCertification = false;
      } else {
        toastr.error("This certifcate record is already in your list.");
      }
    }
  }
  // ====================================
  // On Certification year change
  // ====================================
  public getCertificationYear: Date = null;
  onCertific_yearChange() {
    if (this.getCertificationYear != null) {
      this.yearList.forEach(element => {
        if (element.year == this.getCertificationYear.getFullYear()) {
          this.getYearCertification.year = element.year;
          this.getYearCertification.id = element.year_id;
        }
      });
    }
  }
  // onEducationYearChange() {
  //   //
  //   if (this.getYear_year != null) {
  //     this.yearListEducation.forEach(element => {
  //       if (element.year == this.getYear_year.getFullYear()) {
  //         this.getYear.id = element.year_id;
  //         this.getYear.year = element.year;
  //       }
  //     });

  //   }
  // }

  // ==================================
  // Open Education section on Edit icon
  // ==================================
  openCertificationDetailSection(list_id: any) {
    let filterData = this.certificationDetails.filter(x => x.list_id == list_id);
    if (filterData.length > 0) {
      this.certificateOrAwards = filterData[0].certificateOrAward;
      this.certifiedFrom = filterData[0].certifiedFrom;
      this.getYearCertification.year_id = filterData[0].year_id;
      this.getYearCertification.year_name = filterData[0].year_name;
      this.getCertificationYear = new Date(Number(filterData[0].year_name), 1, 1);
      this.list_id_certification = filterData[0].list_id;
      this.id_certification = filterData[0].id;
      this.showCertificationTable = false;
    }
  }
  onLinkedAccountPrevious() {
    jQuery('body').scrollTop(0);
    jQuery(document).scrollTop(0);
    window.scrollTo(0, 0);
    this.activetab = "professional_info";
    this.getProfessionalInfoDetails();
  }
  onLinkedAccountSave() {
    jQuery('body').scrollTop(0);
    jQuery(document).scrollTop(0);
    window.scrollTo(0, 0);
    this.activetab = "accounts_security";
    this.showAccountsSecurity = true;
    this.showLinkedAccounts = true;
    this.getAccountSecurityDetails();
  }
  getAccountSecurityDetails() {
    this.progress.show();
    this.httpServices.request('get', 'user/user-accounts-verify-details/' + this.userId, null, null, null).subscribe((data) => {
      this.is_verified_mobile_no = data.is_verified_mobile_no;
      this.progress.hide();
    });
  }
  onPreviousAccntSec() {
    jQuery('body').scrollTop(0);
    jQuery(document).scrollTop(0);
    window.scrollTo(0, 0);
    this.activetab = "linked_accounts";
  }
  onClickFinish() {
    this.progress.show();
    this.httpServices.request('patch', 'sellers/security-finish', null, null, null).subscribe((data) => {
      toastr.success("Registration Successfull");
      jQuery('#All_done').modal({ backdrop: 'static', keyboard: false });
      this.userInfo.user.is_seller = 1;
      if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
        this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
        localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
      }
      else {
        localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
      }
      this.progress.hide();
    });

  }
  onEditProfileClick() {
    this.router.navigate(['/gigs_details'], {});
  }
  public errmsg: string = "";
  validateWebsite(): string {
    this.errmsg = "";
    this.validateStatus = true;
    let check: boolean = true;


    if (this.personalWebsite != null && this.personalWebsite != undefined && this.personalWebsite.trim() != "" && !this.is_url(this.personalWebsite)) {
      check = false;
      this.errmsg = "Invalid Website";
    }

    // if ((this.personalWebsite.trim() == "" || this.personalWebsite == undefined || this.personalWebsite == null) && !this.is_url(this.personalWebsite)) {
    //   check = false;
    //   this.errmsg = this.errmsg + "<li>" + "Enter Personal Website";
    // }
    if (!check) {
      return this.errmsg;
    }

    return "";
  }
  // validateWebsite(event: any, item: any) {
  //   
  //   this.validate = true;
  //   let check: boolean = true;
  //   if (item.value != "" && !this.is_url(item.value)) {
  //     check = false;
  //   }
  //   if (!check) {
  //     toastr.warning(this.pleaseEnterValidWebSite);
  //     item.value = '';
  //   }
  // }
  // is_email(str) {
  //   //let regexp = new RegExp(/[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/);
  //   let regexp = new RegExp(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
  //   // if (regexp.test(str)) {
  //   //   return true;
  //   // }
  //   // else {
  //   //   return false;
  //   // }
  //   return true;
  // }
  is_url(str) {
    //let regexp = new RegExp(/^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/);
    // if (regexp.test(str)) {
    //   return true;
    // }
    // else {
    //   return false;
    // }
    let regexp = /^((ftp|http|https):\/\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+((\/)[\w#]+)*(\/\w+\?[a-zA-Z0-9_]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/gm;
    if (regexp.exec(str) !== null) {
      return true;
    }
    else {
      return false;
    }
  }
  // =========================================
  // On click of professional Info
  // ===========================================
  onClickofNext_professionalInfo() {
    jQuery('body').scrollTop(0);
    jQuery(document).scrollTop(0);
    window.scrollTo(0, 0);
    this.progress.show();
    if (this.validateWebsite() != "") {
      this.progress.hide();
      toastr.error(this.errmsg);

    }
    else {
      if (this.professional_info_id == null || this.professional_info_id == "" || this.professional_info_id == 0 || this.professional_info_id == undefined) {
        let sellerData_professional: any = {
          skills: [{
            skill_id: 0,
            skill_level_id: 0
          }],
          educations: [
            {
              country_id: 0,
              university_name: '',
              education_title_id: 0,
              education_major_id: 0,
              education_year: 0
            }
          ],
          certifications: [
            {
              certification_name: '',
              certification_from: '',
              certification_year: 0
            }
          ],
          personal_website: ''
        }

        sellerData_professional.personal_website = this.personalWebsite == null ? "" : this.personalWebsite;
        this.skillAndSkillLevelDetails.forEach(element => {
          sellerData_professional.skills.push({ skill_id: element.skill_id, skill_level_id: element.skill_level_id })
        });
        // this.educationDetails.forEach(element => {
        //   sellerData_professional.educations.push({ country_id: element.country_id, university_name: this.universityDetails.university_name, education_title_id: element.title_id, education_major_id: this.majorDetails.id, education_year: this.getYear.year });
        // });
        this.certificationDetails.forEach(element => {
          sellerData_professional.certifications.push({ certification_name: element.certificateOrAward, certification_from: element.certifiedFrom, certification_year: this.dateFormatter.format(new Date(this.getYearCertification.year), 'yyyy') });
        })
        if (sellerData_professional.skills.length > 0) {
          sellerData_professional.skills.splice(sellerData_professional.skills.findIndex(x => x.skill_id == 0), 1);
        }
        if (sellerData_professional.educations.length > 0) {
          sellerData_professional.educations.splice(sellerData_professional.educations.findIndex(x => x.country_id == 0), 1);
        }
        if (sellerData_professional.certifications.length > 0) {
          sellerData_professional.certifications.splice(sellerData_professional.certifications.findIndex(x => x.certification_name == ''), 1);
        }
        if (sellerData_professional.skills.length > 0) {
          if (this.educationDetails.length > 0) {
            for (let i = 0; i < this.educationDetails.length; i++) {
              //
              let education_body: any = {
                country_id: 0,
                university_name: '',
                education_title_id: 0,
                education_major_id: 0,
                education_year: ''
              }

              // if (this.majorDetails.id == 0) {
              if (this.educationDetails[i].major_id == 0) {
                let educationMajor: any = {
                  education_major_name: ""
                }
                educationMajor.education_major_name = this.educationDetails[i].major_name;
                this.httpServices.request("post", "education/education-major", "", "", educationMajor).subscribe((data) => {
                  sellerData_professional.educations.push({ country_id: this.educationDetails[i].country_id, university_name: this.educationDetails[i].university_name, education_title_id: this.educationDetails[i].title_id, education_major_id: data.id, education_year: this.educationDetails[i].year_name });
                  if (i == this.educationDetails.length - 1) {
                    this.httpServices.request('post', 'sellers/professional-info', null, null, sellerData_professional).subscribe((data) => {
                      toastr.success("Professional Information has been saved successfully.");
                      this.activetab = "linked_accounts";
                      this.showProfessionalInfo = true;
                      this.showPersonalInfo = true;
                      this.showLinkedAccounts = true;
                      this.getLinkedAccountsData();
                      this.progress.hide();
                    }, error => {
                      this.progress.hide();
                      console.log(error);
                    });
                  }
                });
              }
              else {
                sellerData_professional.educations.push({ country_id: this.educationDetails[i].country_id, university_name: this.educationDetails[i].university_name, education_title_id: this.educationDetails[i].title_id, education_major_id: this.educationDetails[i].major_id, education_year: this.educationDetails[i].year_name });
                if (i == this.educationDetails.length - 1) {
                  this.httpServices.request('post', 'sellers/professional-info', null, null, sellerData_professional).subscribe((data) => {
                    toastr.success("Professional Information has been saved successfully.");
                    this.activetab = "linked_accounts";
                    this.showProfessionalInfo = true;
                    this.showPersonalInfo = true;
                    this.showLinkedAccounts = true;
                    this.getLinkedAccountsData();
                    this.progress.hide();
                  }, error => {
                    this.progress.hide();
                    console.log(error);
                  });
                }
              }
            }
          }


        }
        else {
          toastr.error("Please add atleast one skill");
          this.progress.hide();
        }
      }
      else {
        if (this.skillAndSkillLevelDetails.length > 0) {
          this.updateEducationData();
          this.updateSkillsData();
          this.updateCertificationData();
          let sellerData_professional: any = {
            personal_website: ""
          }
          sellerData_professional.personal_website = this.personalWebsite == null ? "" : this.personalWebsite;
          this.httpServices.request('patch', 'sellers/personal-info/' + this.personal_info_id, null, null, sellerData_professional).subscribe((data) => {
            toastr.success("Professional Information has been saved successfully.");
          }, error => {
            this.progress.hide();
            console.log(error);
          });

          this.activetab = "linked_accounts";
          this.showProfessionalInfo = true;
          this.showPersonalInfo = true;
          this.showLinkedAccounts = true;
          this.getLinkedAccountsData();
        }
        else {
          toastr.error("Please add atleast one skill");
          this.progress.hide();
        }
      }
    }
  }
  updateEducationData() {
    if (this.educationDetails.length > 0) {
      for (let i = 0; i < this.educationDetails.length; i++) {
        //
        let education_body: any = {
          country_id: 0,
          university_name: '',
          education_title_id: 0,
          education_major_id: 0,
          education_year: ''
        }

        // if (this.majorDetails.id == 0) {
        if (this.educationDetails[i].major_id == 0) {
          let educationMajor: any = {
            education_major_name: ""
          }
          educationMajor.education_major_name = this.educationDetails[i].major_name;
          this.httpServices.request("post", "education/education-major", "", "", educationMajor).subscribe((data) => {
            this.educationDetails[i].major_id = data.id;
            education_body.country_id = this.educationDetails[i].country_id;
            education_body.university_name = this.educationDetails[i].university_name;
            education_body.education_title_id = this.educationDetails[i].title_id;
            education_body.education_major_id = this.educationDetails[i].major_id;
            // education_body.education_year=Number(this.educationDetails[i].year_name);
            education_body.education_year = this.dateFormatter.format(new Date(Number(this.educationDetails[i].year_name), 1, 1), 'yyyy');
            // if (this.educationIds_alreadyExist.filter(x => x.education_id == this.educationDetails[i].list_id).length > 0) {
            if (this.educationDetails[i].id > 0) {
              this.httpServices.request('patch', 'user/educations/' + this.educationDetails[i].list_id, null, null, education_body).subscribe((data) => {
                this.progress.hide();
              });
            }
            else {
              this.httpServices.request('post', 'user/educations', null, null, education_body).subscribe((data) => {
                this.progress.hide();
                education_body.country_id = this.educationDetails[i].country_id;
                education_body.university_name = this.educationDetails[i].university_name;
                education_body.education_title_id = this.educationDetails[i].title_id;
                education_body.education_major_id = this.educationDetails[i].major_id;
                // education_body.education_year=Number(this.educationDetails[i].year_name);
                education_body.education_year = this.dateFormatter.format(new Date(Number(this.educationDetails[i].year_name), 1, 1), 'yyyy');
                // if (this.educationIds_alreadyExist.filter(x => x.education_id == this.educationDetails[i].list_id).length > 0) {
                //   if (this.educationDetails[i].id > 0) {
                //   this.httpServices.request('patch', 'user/educations/' + this.educationDetails[i].list_id, null, null, education_body).subscribe((data) => {
                //     this.progress.hide();
                //   });
                // }
                // else {
                //   this.httpServices.request('post', 'user/educations', null, null, education_body).subscribe((data) => {
                //     this.progress.hide();
                //   })
                // }
              })
            }
            //this.addEducationDetails();
          });
        } else {
          //this.educationDetails[i].major_id;
          education_body.country_id = this.educationDetails[i].country_id;
          education_body.university_name = this.educationDetails[i].university_name;
          education_body.education_title_id = this.educationDetails[i].title_id;
          education_body.education_major_id = this.educationDetails[i].major_id;
          // education_body.education_year=Number(this.educationDetails[i].year_name);
          education_body.education_year = this.dateFormatter.format(new Date(Number(this.educationDetails[i].year_name), 1, 1), 'yyyy');
          // if (this.educationIds_alreadyExist.filter(x => x.education_id == this.educationDetails[i].list_id).length > 0) {
          if (this.educationDetails[i].id > 0) {
            this.httpServices.request('patch', 'user/educations/' + this.educationDetails[i].list_id, null, null, education_body).subscribe((data) => {
              this.progress.hide();
            });
          }
          else {
            this.httpServices.request('post', 'user/educations', null, null, education_body).subscribe((data) => {
              this.progress.hide();
              education_body.country_id = this.educationDetails[i].country_id;
              education_body.university_name = this.educationDetails[i].university_name;
              education_body.education_title_id = this.educationDetails[i].title_id;
              education_body.education_major_id = this.educationDetails[i].major_id;
              // education_body.education_year=Number(this.educationDetails[i].year_name);
              education_body.education_year = this.dateFormatter.format(new Date(Number(this.educationDetails[i].year_name), 1, 1), 'yyyy');
              // if (this.educationIds_alreadyExist.filter(x => x.education_id == this.educationDetails[i].list_id).length > 0) {
              //   if (this.educationDetails[i].id > 0) {
              //   this.httpServices.request('patch', 'user/educations/' + this.educationDetails[i].list_id, null, null, education_body).subscribe((data) => {
              //     this.progress.hide();
              //   });
              // }
              // else {
              //   this.httpServices.request('post', 'user/educations', null, null, education_body).subscribe((data) => {
              //     this.progress.hide();
              //   })
              // }
            })
          }
          //this.addEducationDetails();
        }

      }
    }
  }
  updateSkillsData() {
    if (this.skillAndSkillLevelDetails.length > 0) {
      for (let i = 0; i < this.skillAndSkillLevelDetails.length; i++) {
        let skill_body: any = {
          skill_id: 0,
          skill_level_id: 0,
        }
        skill_body.skill_id = this.skillAndSkillLevelDetails[i].skill_id;
        skill_body.skill_level_id = this.skillAndSkillLevelDetails[i].skill_level_id;
        // if (this.skillIds_alreadyExist.filter(x => x.skill_id == this.skillAndSkillLevelDetails[i].list_id).length > 0) {
        if (this.skillAndSkillLevelDetails[i].id > 0) {
          this.httpServices.request('patch', 'user/skills/' + this.skillAndSkillLevelDetails[i].list_id, null, null, skill_body).subscribe((data) => {
            this.progress.hide();
          });
        }
        else {
          this.httpServices.request('post', 'user/skills', null, null, skill_body).subscribe((data) => {
            this.progress.hide();
          })
        }
      }
    }
  }
  updateCertificationData() {
    if (this.certificationDetails.length > 0) {
      for (let i = 0; i < this.certificationDetails.length; i++) {
        let certification_body: any = {
          certification_name: '',
          certification_from: '',
          certification_year: '',
        }
        certification_body.certification_name = this.certificationDetails[i].certificateOrAward;
        certification_body.certification_from = this.certificationDetails[i].certifiedFrom;
        // certification_body.certification_year=Number(this.certificationDetails[i].year_name);
        certification_body.certification_year = this.dateFormatter.format(new Date(Number(this.certificationDetails[i].year_name), 1, 1), 'yyyy');
        // if (this.certificationIds_alreadyExist.filter(x => x.certification_id == this.certificationDetails[i].list_id).length > 0) {
        if (this.certificationDetails[i].id > 0) {
          this.httpServices.request('patch', 'user/certifications/' + this.certificationDetails[i].list_id, null, null, certification_body).subscribe((data) => {
            this.progress.hide();
          });
        }
        else {
          this.httpServices.request('post', 'user/certifications', null, null, certification_body).subscribe((data) => {
            this.progress.hide();
          })
        }
      }
    }
  }
  // ===========================================
  // get linked accounts details 
  // ===========================================
  public social_mediaList: social_media = {
    social_media_array: [
      {
        id: 0,
        social_media_name: "",
        is_verified: false
      }
    ]
  }
  getLinkedAccountsData() {
    this.social_mediaList.social_media_array.splice(0, this.social_mediaList.social_media_array.length);
    this.httpServices.request('get', 'social_media/', null, null, null).subscribe((data) => {
      this.httpServices.request('get', 'user/social-accounts-linked/' + this.userId, null, null, null).subscribe((response) => {
        if (response != null) {
          for (let i = 0; i < data.length; i++) {
            if (response.filter(x => x.social_media.id == data[i].id).length > 0) {
              this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: true });
            }
            else {
              this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: false });
            }
          }
        }
        else {
          for (let i = 0; i < data.length; i++) {
            this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: false });
          }
        }
        //this.social_mediaList.social_media_array.splice(this.social_mediaList.social_media_array.findIndex(x => x.id == 0), 1);
      });
    });
  }
  // =========================================
  // All done pop up :on click of continue 
  // ==========================================
  onAllDoneContinuePop() {
    jQuery('#All_done').modal('hide');
    this.encrypt_decrypt.changeDataSwitchSelling({ flag: true, location: "AllDone" });
  }
  // =======================================
  // on click of create new gig 
  // =======================================
  onCreateNewGig() {
    jQuery('#All_done').modal('hide');
    sessionStorage.setItem("queryParams", null);
    this.encrypt_decrypt.changeDataSwitchSelling({ flag: true, location: "become_a_seller" });
  }
}
interface social_media {
  social_media_array: [
    {
      id: number,
      social_media_name: string,
      is_verified: boolean
    }
  ]
}
