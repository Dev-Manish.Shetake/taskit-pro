/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Become_buyerComponent } from './become_buyer.component';

describe('Become_buyerComponent', () => {
  let component: Become_buyerComponent;
  let fixture: ComponentFixture<Become_buyerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Become_buyerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Become_buyerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
