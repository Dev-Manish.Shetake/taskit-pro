/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Buyer_ordersComponent } from './buyer_orders.component';

describe('Buyer_ordersComponent', () => {
  let component: Buyer_ordersComponent;
  let fixture: ComponentFixture<Buyer_ordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Buyer_ordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Buyer_ordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
