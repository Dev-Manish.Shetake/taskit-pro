import { Component, OnInit, ViewChild } from '@angular/core';
import { Buyer_order_commonComponent } from '../../../../_common_component/buyer_order_common/buyer_order_common.component';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-buyer_orders',
  templateUrl: './buyer_orders.component.html',
  styleUrls: ['./buyer_orders.component.scss']
})
export class Buyer_ordersComponent implements OnInit {

  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService) { }
  @ViewChild(Buyer_order_commonComponent)
  childCommon_Orders: Buyer_order_commonComponent;
  public totalRecords: any;
  public orderStatusId: any;
  public pageSize = 10;
  public result: any;
  public allCount: any = 0;
  public activeCount: any = 0;
  public inProcessCount: any = 0;
  public deliveredCount: any = 0;
  public completedCount: any = 0;
  public cancelledCount: any = 0;
  public disputeCount: any = 0;
  public activeTab: string;
  public queryParam: any;
  public _queryParams: any;
  public _queryParamsOrderDetails: any;
  public fromFlag: any;
  ngOnInit() {
    //this.totalRecords= this.childCommon_Orders.totalRecordCount;
    //this.childCommon_Orders.getResult();
    //this.getAllOrdersData(1);
    if (this.queryParam == null) {
      this._queryParamsOrderDetails = (sessionStorage.getItem("queryParamsFromOrderDetails") || '{}');
      if (this._queryParamsOrderDetails != null && this._queryParamsOrderDetails != undefined) {
        this.queryParam = JSON.parse(this._queryParamsOrderDetails);
        this.fromFlag = this.queryParam.activeTab;
        //this.activeTab = this.fromFlag;
        this.onTabChange(0, this.fromFlag);
        sessionStorage.removeItem('queryParamsFromOrderDetails');
      }
    }
    this.getOrdersCountData();
  }
  getOrdersCountData() {
    this.httpServices.request('get', 'orders/counts', null, null, null).subscribe((data) => {
      this.result = data;
      if (data) {
        this.allCount = data.count;
        this.activeCount = this.result.total_active == null ? 0 : this.result.total_active;
        this.inProcessCount = this.result.total_in_process == null ? 0 : this.result.total_in_process;
        this.deliveredCount = this.result.total_delivered == null ? 0 : this.result.total_delivered;
        this.completedCount = this.result.total_completed == null ? 0 : this.result.total_completed;
        this.cancelledCount = this.result.total_cancelled == null ? 0 : this.result.total_cancelled;
        this.disputeCount = this.result.total_dispute == null ? 0 : this.result.total_dispute;

        if (this.activeTab == '' || this.activeTab == undefined) {
          if (this.activeCount > 0) {
            this.onTabChange(0, 'active_orders');
          }
          else if (this.inProcessCount > 0) {
            this.onTabChange(0, 'in_process');
          }
          else if (this.deliveredCount > 0) {
            this.onTabChange(0, 'delivered');
          }
          else if (this.completedCount > 0) {
            this.onTabChange(0, 'completed');
          }
          else if (this.cancelledCount > 0) {
            this.onTabChange(0, 'cancelled');
          }
          else if (this.disputeCount > 0) {
            this.onTabChange(0, 'dispute');
          }
          else {
            this.onTabChange(0, 'active_orders');
          }
        }
      }
    }, error => {
      console.log(error);
    });
  }
  onTabChange(orderStatusId, activetab) {
    //this.activeTab = 'active_orders';
    if (activetab != undefined)
      this.activeTab = activetab;
    //his.childCommon_Orders.getResult(orderStatusId);
  }
  refreshCount(event: any) {
    if (event.orderStatusID == 2) {
      this.activeCount = event.count;
    }
    else if (event.orderStatusID == 3) {
      this.inProcessCount = event.count;
    }
    else if (event.orderStatusID == 4) {
      this.deliveredCount = event.count;
    }
    else if (event.orderStatusID == 6) {
      this.completedCount = event.count;
    }
    else if (event.orderStatusID == 8) {
      this.cancelledCount = event.count;
    }
    else if (event.orderStatusID == 7) {
      this.disputeCount = event.count;
    }
  }
}
