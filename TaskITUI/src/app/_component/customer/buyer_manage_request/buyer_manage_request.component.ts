import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-buyer_manage_request',
  templateUrl: './buyer_manage_request.component.html',
  styleUrls: ['./buyer_manage_request.component.css']
})
export class Buyer_manage_requestComponent implements OnInit {
  public activeTab: string = 'active_orders';
  public requestList:any;
  public requestCount:number;
  public activeCount:number;
  public pendingCount:number;
  public unapprovedCount:number;

  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService) { }

  ngOnInit() {
    this.getrequestCount();
  }

  getrequestCount(){
    //request Count for Buyer
    this.httpServices.request("get","requests/counts","","",null).subscribe((data)=>{
      this.requestCount=data;

      this.activeCount=data.total_active;
      this.pendingCount=data.total_pending;
      this.unapprovedCount=data.total_unapproved;

    },error=>{
      console.log(error);
    });
  }

  onTabChange(orderStatusId, activetab) {
    //this.childCommon_Orders.getResult(orderStatusId);
    //this.getrequestCount();
    this.activeTab = activetab;
  }
}
