import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { Socialusers } from 'src/app/_models/socialusers';
import { element } from 'protractor';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';

declare var toastr: any;
declare var jQuery:any;
// declare var jQuery: any;
// declare var $: any;

@Component({
  selector: 'app-buyer_profile',
  templateUrl: './buyer_profile.component.html',
  styleUrls: ['./buyer_profile.component.css']
})
export class Buyer_profileComponent implements OnInit {

  constructor(private httpServices: HttpRequestService, private dateFormatter: DateFormatterService, private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService, private authService: SocialAuthService) { }

  public autoCompLangugeListItem: Array<{ id: string, code: string, lang_name: string }> = [];
  public languageDetails: any = { id: 0, lang_name: '' };
  public languageProficiencyDetails: any = { list_id: 0, lang_id: 0, lang_name: '', lang_proficiency_id: 0, lang_proficiency_name: '' };
  public getLanguageProfeciency: any = { id: 0, lang_proficiency_name: '' };
  public getYear: any = { id: 0, year: 0 };
  public getCertificationYear: any = { id: 0, year: 0 };
  public loggedIn_ProfileDetails: any;
  public yearList: Array<{ year_id: Number, year: Number }> = [];
  public yearListForEducation: Array<{ year_id: Number, year: Number }> = [];
  public languageProfeciencyList: any;
  public languagedata: any;
  public list_id: any;
  public autoCompSkillItem: Array<{ id: string, code: string, skill_name: string }> = [];
  public autoCompUniversityListItem: Array<{ id: number, code: string, university_name: string }> = [];
  public autoCompMajorListItem: Array<{ id: string, education_major_name: string }> = [];
  public universityDetails: any = { id: 0, university_name: '' };
  public skillDetails: any = { id: 0, skill_name: '' };
  public getSkillLevel: any = { id: 0, skill_level_name: '' };
  public skillsLevelList: any;
  public countryList: any;
  public getCountry: any = { id: 0, country_name: '' };
  public buyer_profile_photo: any = undefined;
  public photoCopy: boolean = false;
  public photoScan: any;
  public block = "block";
  public none = "none";
  public openProfileModal: boolean = false;
  public openEditDescriptionModal: boolean = false;
  public openLanguageModal: boolean = false;
  public openSkillModal: boolean = false;
  public openEducationModal: boolean = false;
  public openCertificationModal: boolean = false;
  public buyer_data_source: any;
  public certificateOrAward: string;
  public certificateList: any;
  public certificateFrom: string;
  public educationList: any;
  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userId: any;
  public skillList: any;
  public languageList: any;
  public educationDegree: string;
  public titleList: any;
  public getTitle: any = { id: 0, title_name: '' };
  public majorDetails: any = { id: 0, education_major_name: '' };
  public buyer_languageDetailsList: Array<{ list_id: any; lang_id: Number, lang_name: string, lang_proficiency_id: Number, lang_proficiency_name: string }> = [];
  public buyer_CertificationDetailsList: Array<{ list_id: any; certificate_Award: string, certified_from: string, certification_year: any, year_id: Number }> = [];
  public buyer_skillDetailsList: Array<{ list_id: any, skill_id: Number, skill_name: string, skill_proficiency_id: Number, skill_proficiency_name: string }> = [];
  public buyer_educationlDetailsList: Array<{ list_id: any; country_id: Number; country_name: string, university_name: string, education_title_id: Number, education_title_name: string, education_major_Id: Number, education_major_degree: string, education_year: string }> = [];
  public language_id: number = 0;
  public certificate_id: number = 0;
  public skillDetails_id: number = 0;
  public educationDetails_id: number = 0;
  public getYear_year: Date = null;
  public maxDate: any;
  public focusedDate: any;

  public checkvalidation_description: boolean = false;
  public checkvalidation_personalInfo: boolean = false;
  public checkvalidation_languageModal: boolean = false;
  public checkvalidation_skillModal: boolean = false;
  public checkvalidation_educationModal: boolean = false;
  public checkvalidation_certificationModal: boolean = false;

  public description: string;
  public firstName: string;
  public lastName: string;
  public userName: string;
  public queryParam: any;
  public _queryParams: any;
  public account_Created_date: any;
  public is_Buyer: boolean = false;

  //public autoCompCountryItem:any;
  public countryDetails: any = { id: 0, country_name: '' };

  ngOnInit() {
    this.maxDate = new Date();
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    this.is_Buyer = false;

    if (this.encrypt_decrypt.buyer_profile_id == undefined) {
      this._queryParams = (sessionStorage.getItem("buyer_profile") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      this.is_Buyer = this.queryParam.is_Buyer;
      this.userId = this.queryParam.buyer_profile_id;
    }
    else {
      if (this.encrypt_decrypt.buyer_profile_mode == 'self') {
        this.is_Buyer = true;
      }
      if (this.encrypt_decrypt.buyer_profile_id > 0) {
        this.userId = this.encrypt_decrypt.buyer_profile_id;
        let buyer_profile = { buyer_profile_id: this.userId, is_Buyer: this.is_Buyer }
        sessionStorage.setItem("buyer_profile", JSON.stringify(buyer_profile));
      }
    }

    this.getCountryList();
    this.getYearList();
    this.getTitleList();
    this.getLoggedin_profileDetails();
    this.getLinkedAccountsData();
  }


  //**********************
  //get Country List
  //***********************
  getCountryList() {
    this.httpServices.request('get', 'country/?country_name=', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.countryList = data;
        }
      });
  }


  //**********************
  //get Year List
  //***********************
  getYearList() {
    var year = new Date().getFullYear();

    for (var i = 1; i < 41; i++) {
      this.yearList.push({ year_id: i, year: year - 41 + i });
      this.yearListForEducation.push({ year_id: i, year: year - 41 + i });
    }
  }



  //**********************
  //get Education Title List
  //***********************
  getTitleList() {

    this.httpServices.request('get', 'education/education-title', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.titleList = data;
        }
      });
  }


  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  onlyCharacters_withApostrophe(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code == 39) && //apostrophe
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }

  //***************************
  //Get Logged in Profile Info
  //***************************
  getLoggedin_profileDetails() {
    this.progress.show();

    this.httpServices.request('get', 'buyers/info/' + this.userId, '', '', null)
      .subscribe((data) => {
        this.loggedIn_ProfileDetails = data;

        this.account_Created_date = this.loggedIn_ProfileDetails.created_date;
        //Year Control 
        let currentYear = new Date().getFullYear();
        this.focusedDate = new Date(currentYear - 1, 11, 31);

        // To get Profile Details
        if (this.loggedIn_ProfileDetails.personal_info != null && this.loggedIn_ProfileDetails.personal_info != undefined) {
          this.buyer_data_source = this.loggedIn_ProfileDetails.personal_info.profile_picture;
          this.description = this.loggedIn_ProfileDetails.personal_info.description;
          this.firstName = this.loggedIn_ProfileDetails.personal_info.first_name;
          this.lastName = this.loggedIn_ProfileDetails.personal_info.last_name;
          this.userName = this.loggedIn_ProfileDetails.user_name;
        } else {
          this.buyer_data_source = "/../assets/img/profile_pic.png";
          this.description = "";
          this.firstName = "";
          this.lastName = "";
          this.userName = "";
        }

        if (this.loggedIn_ProfileDetails != null) {
          //To get Language Details
          this.loggedIn_ProfileDetails.user_languages.forEach(element => {
            this.buyer_languageDetailsList.push({
              list_id: element.id,
              lang_id: element.lang.id,
              lang_name: element.lang.lang_name,
              lang_proficiency_id: element.lang_proficiency.id,
              lang_proficiency_name: element.lang_proficiency.lang_proficiency_name
            });
          });
        }


        if (this.loggedIn_ProfileDetails != null) {

          //To get Certification Details
          this.loggedIn_ProfileDetails.certifications.forEach(element => {
            this.buyer_CertificationDetailsList.push({
              list_id: element.id,
              certificate_Award: element.certification_name,
              certified_from: element.certification_from,
              certification_year: element.certification_year,
              year_id: 0
            });
          });
        }

        if (this.loggedIn_ProfileDetails != null) {

          //To get Education Details
          this.loggedIn_ProfileDetails.educations.forEach(element => {
            this.buyer_educationlDetailsList.push({
              list_id: element.id,
              country_id: element.country.id,
              country_name: element.country.country_name,
              university_name: element.university_name,
              education_title_id: element.education_title.id,
              education_title_name: element.education_title.education_title_name,
              education_major_Id: element.education_major.id,
              education_major_degree: element.education_major.education_major_name,
              //education_year: element.education_year
              education_year: this.dateFormatter.format(new Date(Number(element.education_year), 1, 1), 'yyyy')
            });
          });
        }

        if (this.loggedIn_ProfileDetails != null) {
          //To get Skills   
          this.loggedIn_ProfileDetails.skills.forEach(element => {
            this.buyer_skillDetailsList.push({
              list_id: element.id,
              skill_id: element.skill.id,
              skill_name: element.skill.skill_name,
              skill_proficiency_id: element.skill_level.id,
              skill_proficiency_name: element.skill_level.skill_level_name
            });
          });
        }
        this.progress.hide();
        //toastr.success("Profile fetched Successfully");
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }




  //**********************
  //Profile Photo Section
  //***********************
  fileChangedPhoto(event: any) {
    // this.progress.show();
    if (event.target.files.length == 0) {
      this.photoCopy = false;
      this.buyer_profile_photo = undefined;
      this.photoScan = this.block;
      // this.progress.hide();
      return;
    }
    this.photoScan = this.none;
    //To obtaine a File reference

    var files = event.target.files;
    const reader = new FileReader();
    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    if (files[0].type == "image/jpeg" || files[0].type == "image/png" || files[0].type == "image/jpg" || files[0].type == "image/bmp" || files[0].type == "image/gif") {

      fileType = "";
    }
    else {

      fileType = ("<li>" + "The file does not match file type." + "</li>");
    }

    //To check file Size according to upload conditions

    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "Your file does not match the upload conditions, Your file size is:" + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + "MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      // this.progress.hide();
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {

      this.buyer_profile_photo = event.target.files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.buyer_data_source = reader.result;
      }
      //this.buyer_data_source=event.target.result;
      if (this.buyer_profile_photo != "" || this.buyer_profile_photo != undefined) {
        formData.append("profile_picture", this.buyer_profile_photo[0]);
        this.photoCopy = true;
        this.photoScan = this.none;


        this.httpServices.request("patch", "buyers/personal-info/" + this.loggedIn_ProfileDetails.personal_info.id, "", "", formData).subscribe((data) => {
          toastr.success("Profile photo Uploaded Successfully");
        }, error => {
          console.log(error);
          // this.progress.hide();
        });

      }
      // this.progress.hide();
    }
    else {
      //this.resetFileInput(this.file_image);
      this.buyer_profile_photo = "";
      this.buyer_profile_photo = undefined;
      this.photoCopy = false;
      this.photoScan = this.block;
      this.progress.hide();

    }
  }
  // deleteImage() {

  //   //this.logistic_plan_urls.splice(this.logistic_plan_urls.findIndex(x => x.file_name == event.file_name), 1);
  //   this.buyer_profile_photo = "";
  //   this.buyer_profile_photo = undefined;
  //   this.photoCopy = false;
  //   this.photoScan = this.block;
  //   this.buyer_data_source="";
  // }


  //On Profile First name and Last name change
  onProfileName_Update() {

    this.checkvalidation_personalInfo = true;
    let profile_name: any = {
      first_name: '',
      last_name: ''
    }

    profile_name.first_name = this.firstName;
    profile_name.last_name = this.lastName;

    if (profile_name.first_name != null && profile_name.first_name != "" && profile_name.first_name != undefined
      && profile_name.last_name != null && profile_name.last_name != undefined && profile_name.last_name != "") {
      this.progress.show();
      if (this.loggedIn_ProfileDetails.personal_info == null) {
        profile_name.languages = [{
          lang_id: 1,
          lang_proficiency_id: 2
        }];
        this.httpServices.request("post", "sellers/personal-info", "", "", profile_name).subscribe((data) => {
          toastr.success("Profile Name Updated Successfully");
          this.onCancel_ProfileInfo();
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
      }
      else {
        this.httpServices.request("patch", "buyers/personal-info/" + this.loggedIn_ProfileDetails.personal_info.id, "", "", profile_name).subscribe((data) => {
          toastr.success("Profile Name Updated Successfully");
          this.onCancel_ProfileInfo();
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
      }
    }
  }

  onProfilename_editPencilbtn() {
    this.openProfileModal = true;
    this.checkvalidation_personalInfo = false;
  }

  onCancel_ProfileInfo() {
    this.openProfileModal = false;
  }


  // ========================
  // View Attached Imgae 
  // =========================
  public image_ViewUrl: any;
  openImage(item: any) {
    this.image_ViewUrl = item;
    jQuery('#imagemodal').modal({ backdrop: 'static', keyboard: false });
  }

  //*********************************** 
  // Description Section
  //***********************************  
  onEditDescription() {
    this.openEditDescriptionModal = true;
    this.checkvalidation_description = false;
  }

  onCancelDescriptionBtn() {
    this.openEditDescriptionModal = false;
  }

  //Post action to Add New Description
  onUpdateDescription() {
    this.progress.show();
    this.checkvalidation_description = true;
    let buyer_description: any = {
      description: ''
    }

    buyer_description.description = this.description;

    if (buyer_description.description != null && buyer_description.description != undefined && buyer_description.description != '') {
      this.httpServices.request('patch', 'buyers/personal-info/' + this.loggedIn_ProfileDetails.personal_info.id, null, null, buyer_description)
        .subscribe((data) => {
          this.progress.hide();
          toastr.success("Description Updated Successfully");
          this.onCancelDescriptionBtn();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    }
  }


  //**************************************
  // Languages Section
  //**************************************

  //Language Filter
  handleLanguageFilter(value: string) {
    this.autoCompLangugeListItem = undefined;
    this.languageDetails.id = 0;
    this.languageDetails.lang_name = "";
    if (value.length > 1) {

      this.httpServices.request('get', 'languages/?lang_name=' + value.replace('&', '~'), '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompLangugeListItem = data;
          }
        });
    }
  }


  // On Selecting Language
  onSpanLanguageSelected(dataitem: any) {
    this.languageDetails.id = dataitem.id;
    this.languageDetails.lang_name = dataitem.lang_name;
  }


  // onAddNewLanguageClick  
  onAddNewLanguageClick() {
    this.checkvalidation_languageModal = false;
    this.language_id = 0;
    this.openLanguageModal = true;
    this.getLanguageProfeciency.id = 0;
    this.getLanguageProfeciency.lang_proficiency_name = '';
    this.list_id = 0;
    this.languageDetails.id = 0;
    this.languageDetails.lang_name = '';
    this.getLanguageProficiency();
  }

  //on change of language proficiency
  onSelection() {
    this.languageProfeciencyList.forEach(element => {
      if (element.id == this.getLanguageProfeciency.id) {
        this.getLanguageProfeciency.lang_proficiency_name = element.lang_proficiency_name;
      }
    });
  }

  getLanguageProficiency() {
    this.httpServices.request('get', 'languages/profeciency', null, null, null).subscribe((data) => {
      this.languageProfeciencyList = data;
      //this.progress.hide();
    }, error => {
      //this.progress.hide();
      console.log(error);
    });
  }

  //On cancel btn click
  onLanguageCancelBtn() {
    this.openLanguageModal = false;
  }


  // Using Update Button for Language and Language Proficiency
  onLanguageUpdateClick() {

    this.checkvalidation_languageModal = true;
    let buyer_language: any = {
      lang_id: 0,
      lang_proficiency_id: 0
    }
    buyer_language.lang_id = this.languageDetails.id;
    buyer_language.lang_proficiency_id = this.getLanguageProfeciency.id;

    if (buyer_language.lang_id != 0 && buyer_language.lang_id != null && buyer_language.lang_proficiency_id != 0 && buyer_language.lang_proficiency_id != null) {
      this.progress.show();

      if (this.buyer_languageDetailsList.filter(element => element.lang_id == this.languageDetails.id &&
        element.list_id == this.language_id &&
        element.lang_name == this.languageDetails.lang_name &&
        element.lang_proficiency_id == this.getLanguageProfeciency.id &&
        element.lang_proficiency_name == this.getLanguageProfeciency.lang_proficiency_name).length > 0) {
        toastr.error("You have changed nothing for this record");
      }
      else if (this.buyer_languageDetailsList.filter(element => element.lang_id == this.languageDetails.id &&
        element.list_id !== this.language_id &&
        element.lang_name == this.languageDetails.lang_name).length > 0) {
        toastr.error("Language record already exist");
      }
      else {
        if (this.language_id == 0) {
          this.httpServices.request('post', 'user/languages', null, null, buyer_language).subscribe((data) => {
            toastr.success("Language Record Saved Successfully");
            this.buyer_languageDetailsList.push({
              list_id: data.id,
              lang_id: data.lang.id,
              lang_name: data.lang.lang_name,
              lang_proficiency_id: data.lang_proficiency.id,
              lang_proficiency_name: data.lang_proficiency.lang_proficiency_name
            });
            //this.onViewLanguage();
            this.onLanguageCancelBtn();
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
        else {
          this.httpServices.request('patch', 'user/languages/' + this.language_id, null, null, buyer_language).subscribe((data) => {
            toastr.success("Language Record Updated Successfully");
            this.onLanguageCancelBtn();
            this.buyer_languageDetailsList.forEach(x => {
              if (x.list_id == data.id) {
                x.lang_id = data.lang.id;
                x.lang_name = data.lang.lang_name;
                x.lang_proficiency_id = data.lang_proficiency.id;
                x.lang_proficiency_name = data.lang_proficiency.lang_proficiency_name;
              }
            });
            //this.onViewLanguage();
            this.onLanguageCancelBtn();
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
      }
      this.progress.hide();
    }
  }

  // to display language details below modal
  onViewLanguage() {
    this.buyer_languageDetailsList.push({
      list_id: this.buyer_languageDetailsList.length + 1,
      lang_id: this.languageDetails.id,
      lang_name: this.languageDetails.lang_name,
      lang_proficiency_id: this.getLanguageProfeciency.id,
      lang_proficiency_name: this.getLanguageProfeciency.lang_proficiency_name
    });
  }

  // =================================================
  // Remove Language from Language proficiency table
  // =================================================
  onDeleteLanguage(list: any) {
    this.progress.show();
    if (this.buyer_languageDetailsList.length > 1) {
      this.buyer_languageDetailsList.splice(this.buyer_languageDetailsList.findIndex(x => x.list_id == list), 1);

      this.httpServices.request('delete', 'user/languages/' + list.list_id, null, null, null).subscribe((data) => {
        toastr.success("Language Record Deleted Successfully");
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    } else {
      toastr.error("Atleast One Language is Mandatory");
      this.progress.hide();
    }
  }


  // public buyer_language: any = {
  //   lang_id: 0,
  //   lang_proficiency_id: 0
  // }
  //On edit Pencil btn action 
  onEditLanguage_pencilBtn(list: any) {
    this.checkvalidation_languageModal = false;
    this.openLanguageModal = true;

    if (this.languageProfeciencyList == null || this.languageProfeciencyList == undefined || this.languageProfeciencyList == ''
      || this.languageProfeciencyList.length == 0) {

      this.httpServices.request('get', 'languages/profeciency', null, null, null).subscribe((data) => {
        this.languageProfeciencyList = data;

        this.language_id = list.list_id;
        this.languageDetails.id = list.lang_id;
        this.languageDetails.lang_name = list.lang_name;
        this.getLanguageProfeciency.id = list.lang_proficiency_id;
        this.getLanguageProfeciency.lang_proficiency_name = list.lang_proficiency_name;
      }, error => {
        //this.progress.hide();
        console.log(error);
      });

    } else {
      this.language_id = list.list_id;
      this.languageDetails.id = list.lang_id;
      this.languageDetails.lang_name = list.lang_name;
      this.getLanguageProfeciency.id = list.lang_proficiency_id;
      this.getLanguageProfeciency.lang_proficiency_name = list.lang_proficiency_name;
    }
  }

  // ===============================================
  // Linked Accounts: Google, Facebook
  // ================================================
  linkedAccounts_connect_click(socialProvider: string) {

    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    //this.saves_response(socialProvider);
    this.authService.signIn(socialPlatformProvider).then(social_users => {
      this.saves_response(social_users);
    });
  }
  saves_response(social_users: Socialusers) {
    //console.log(social_users);

    this.progress.show();
    let login_info = { social_account_id: social_users.email, provider: social_users.provider };
    this.httpServices.request('post', 'sellers/linked-accounts', null, null, login_info).subscribe((data) => {
      if (data) {
        console.log(data);
      }

      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }



  //**************************************** 
  // Skill Section
  //****************************************

  handleSkillFilter(value: string) {
    this.autoCompSkillItem = undefined;
    this.skillDetails.id = 0;
    this.skillDetails.skill_name = "";
    if (value.length > 1) {
      this.httpServices.request('get', 'skills/?skill_name=' + value.replace('&', '~'), '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompSkillItem = data;
          }
        });
    }
  }

  // On Add New Skill Button Click
  onAddNewSkillClick() {
    this.checkvalidation_skillModal = false;
    this.openSkillModal = true;
    this.skillDetails_id = 0;
    this.skillDetails.skill_name = "";
    this.getSkillLevel.id = 0;
    this.getSkillLevel.skill_level_name = "";
    this.getSkillLevelList();
  }

  getSkillLevelList() {
    this.httpServices.request('get', 'skills/level', null, null, null).subscribe((data) => {
      this.skillsLevelList = data;
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }

  //On Cancel hide modal
  onCancelSkill() {
    this.openSkillModal = false;
  }

  //Skill Level Change
  onskillLevelChange() {
    this.skillsLevelList.forEach(element => {
      if (element.id == this.getSkillLevel.id) {
        this.getSkillLevel.skill_level_name = element.skill_level_name;
      }
    });
  }

  // On Selecting Language
  onSpanSkillSelected(dataitem: any) {
    this.skillDetails.id = dataitem.id;
    this.skillDetails.skill_name = dataitem.skill_name;
  }

  //Post action to add new Skill
  onUpdateSkill() {
    this.checkvalidation_skillModal = true;
    let buyer_skills: any = {
      skill_id: 0,
      skill_level_id: 0
    }
    buyer_skills.skill_id = this.skillDetails.id;
    buyer_skills.skill_level_id = this.getSkillLevel.id;


    if (buyer_skills.skill_id != 0 && buyer_skills.skill_id != undefined && buyer_skills.skill_level_id != 0 && buyer_skills.skill_level_id != undefined) {
      this.progress.show();

      if (this.buyer_skillDetailsList.filter(x => x.list_id == this.skillDetails_id &&
        x.skill_proficiency_id == this.getSkillLevel.id &&
        x.skill_id == this.skillDetails.id &&
        x.skill_name == this.skillDetails.skill_name &&
        x.skill_proficiency_name == this.getSkillLevel.skill_level_name).length > 0) {
        toastr.error("You have not changed anything for this skill");
      }
      else if (this.buyer_skillDetailsList.filter(x => x.list_id != this.skillDetails_id &&
        x.skill_id == this.skillDetails.id &&
        x.skill_name == this.skillDetails.skill_name).length > 0) {
        toastr.error("Skill Already Exists");
      }
      else {
        if (this.skillDetails_id == 0) {
          this.progress.show();
          this.httpServices.request('post', 'user/skills', null, null, buyer_skills).subscribe((data) => {
            toastr.success("Skill Record Created Successfully");
            this.onViewSkill();
            // this.buyer_skillDetailsList.push({
            //   list_id: this.buyer_skillDetailsList.length + 1,
            //   skill_id: this.skillDetails.id,
            //   skill_name: this.skillDetails.skill_name,
            //   skill_proficiency_id: this.getSkillLevel.id,
            //   skill_proficiency_name: this.getSkillLevel.skill_level_name
            // });
            this.onCancelSkill();
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        } else {
          this.httpServices.request('patch', 'user/skills/' + this.skillDetails_id, null, null, buyer_skills).subscribe((data) => {
            toastr.success("Skill Record updated Successfully");
            this.onCancelSkill();
            this.buyer_skillDetailsList.forEach(x => {
              if (x.list_id == data.id) {
                x.skill_id = data.skill.id;
                x.skill_name = data.skill.skill_name;
                x.skill_proficiency_id = data.skill_level.id;
                x.skill_proficiency_name = data.skill_level.skill_level_name;
              }
            });
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
      }
      this.progress.hide();
    }
  }


  onViewSkill() {
    this.buyer_skillDetailsList.push({
      list_id: this.buyer_skillDetailsList.length + 1,
      skill_id: this.skillDetails.id,
      skill_name: this.skillDetails.skill_name,
      skill_proficiency_id: this.getSkillLevel.id,
      skill_proficiency_name: this.getSkillLevel.skill_level_name
    });
  }


  onDeleteSkill(list: any) {

    this.progress.show();
    if (this.buyer_skillDetailsList.length > 1) {
      this.buyer_skillDetailsList.splice(this.buyer_skillDetailsList.findIndex(x => x.list_id == list.list_id), 1);
      this.httpServices.request('delete', 'user/skills/' + list.list_id, null, null, null).subscribe((data) => {
        toastr.success("Skill Record Deleted Successfully");
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    } else {
      toastr.error("Atleast one skill is mandatory");
      this.progress.hide();
    }
  }

  //On pencil btn click Edit action
  onEditSkill_pencilBtn(list: any) {

    this.openSkillModal = true;
    if (this.skillsLevelList == undefined || this.skillsLevelList == null || this.skillsLevelList == ''
      || this.skillsLevelList.length == 0) {
      this.httpServices.request('get', 'skills/level', null, null, null).subscribe(data => {
        this.skillsLevelList = data;

        this.skillDetails_id = list.list_id
        this.skillDetails.id = list.skill_id;
        this.skillDetails.skill_name = list.skill_name;
        this.getSkillLevel.id = list.skill_proficiency_id;
        this.getSkillLevel.skill_level_name = list.skill_proficiency_name;
      }, error => {
        console.log(error);
      });
    } else {
      this.skillDetails_id = list.list_id
      this.skillDetails.id = list.skill_id;
      this.skillDetails.skill_name = list.skill_name;
      this.getSkillLevel.id = list.skill_proficiency_id;
      this.getSkillLevel.skill_level_name = list.skill_proficiency_name;
    }
  }

  //*********************************** 
  //Education Section
  //***********************************

  // handle University filter 
  handleUniversityFilter(value: string) {
    this.autoCompUniversityListItem = undefined;
    this.universityDetails.university_name = "";
    this.universityDetails.id = 0;
    if (value.length > 1) {
      this.httpServices.request('get', 'user/university-list?university_name=' + value.replace('&', '~'), '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompUniversityListItem = data;
          }
          else {
            this.universityDetails.university_name = value;
          }
        });
    }
    this.universityDetails.university_name = value;
  }

  // On Selecting University 
  onSpanUniversitySelected(dataItem: any) {
    this.universityDetails.id = dataItem.id;
    this.universityDetails.university_name = dataItem.university_name;
  }


  //handle MajorEducation filter
  handleMajorFilter(value: string) {

    this.autoCompMajorListItem = undefined;
    this.majorDetails.id = 0;
    this.majorDetails.education_major_name = "";

    if (value.length > 0) {
      this.httpServices.request('get', 'education/education-major?education_major_name=' + value.replace('&', '~'), '', '', null).subscribe((data) => {
        if (data.length > 0) {
          this.autoCompMajorListItem = data;
        }
      }, error => {
        console.log(error);
      });
    }
    this.majorDetails.education_major_name = value;
  }


  //on Selecting Major Education
  onSpanMajorSelected(dataItem: any) {
    this.majorDetails.id = dataItem.id;
    this.majorDetails.education_major_name = dataItem.education_major_name;
  }

  //on add New Education Section 
  onAddNewEducation() {
    this.checkvalidation_educationModal = false;
    this.openEducationModal = true;
    this.educationDetails_id = 0;
    this.getCountry.id = 0;
    this.getCountry.country_name = "";
    this.universityDetails.university_name = "";
    this.getTitle.id = 0;
    this.getTitle.title_name = "";
    this.majorDetails.id = "";
    this.majorDetails.education_major_name = "";
    this.getYear.id = 0;
    this.getYear_year = null;
  }

  //on Education Cancel Btn
  onEducationCancelBtn() {
    this.openEducationModal = false;
  }


  //on education Title Change
  onTitleChange() {
    this.titleList.forEach(element => {
      if (element.id == this.getTitle.id) {
        this.getTitle.title_name = element.education_title_name;
      }
    });
  }

  // On year Change in Education section 
  onEducationYearChange() {
    if (this.getYear_year != null) {
      this.yearListForEducation.forEach(element => {
        if (element.year == this.getYear_year.getFullYear()) {
          this.getYear.id = element.year_id,
            this.getYear.year = element.year;
        }
      });
    }
  }

  //Post action to add New Education Record
  onUpdateEducationDetails() {

    this.checkvalidation_educationModal = true;

    if (this.getCountry.id != null && this.getCountry.id != 0 && this.getCountry.id != undefined &&
      this.universityDetails.university_name != null && this.universityDetails.university_name != undefined &&
      this.getTitle.id != null && this.getTitle.id != undefined && this.getTitle.id != 0 &&
      ((this.majorDetails.id == 0 && this.majorDetails.education_major_name != '') || (this.majorDetails.id != 0)) &&
      this.getYear.year != null && this.getYear.year != undefined) {

      if (this.majorDetails.id == 0) {
        let educationMajor: any = {
          education_major_name: ""
        }
        educationMajor.education_major_name = this.majorDetails.education_major_name;
        this.httpServices.request("post", "education/education-major", "", "", educationMajor).subscribe((data) => {
          this.majorDetails.id = data.id;
          this.addEducationDetails();
        });
      } else {
        this.majorDetails.id;
        this.addEducationDetails();
      }
    }
  }

  addEducationDetails() {

    let buyer_Education: any =
    {
      country_id: 0,
      university_name: '',
      education_title_id: 0,
      education_major_id: 0,
      education_year: 0
    }
    buyer_Education.country_id = this.getCountry.id;
    buyer_Education.university_name = this.universityDetails.university_name;
    buyer_Education.education_title_id = this.getTitle.id;
    buyer_Education.education_major_id = this.majorDetails.id;
    buyer_Education.education_year = this.getYear.year;

    if (this.getCountry.id != null && this.getCountry.id != 0 && this.getCountry.id != undefined &&
      this.universityDetails.university_name != null && this.universityDetails.university_name != undefined &&
      this.getTitle.id != null && this.getTitle.id != undefined && this.getTitle.id != 0 &&
      ((this.majorDetails.id == 0 && this.majorDetails.education_major_name != '') || (this.majorDetails.id != 0)) &&
      this.getYear.year != null && this.getYear.year != undefined) {
      this.progress.show();


      if (this.buyer_educationlDetailsList.filter(x => x.country_id == this.getCountry.id &&
        x.education_major_Id == this.majorDetails.id &&
        x.education_title_id == this.getTitle.id &&
        x.university_name == this.universityDetails.university_name &&
        x.education_year == this.getYear.year).length > 0) {
        toastr.error("This Education Record already in your List")
        this.progress.hide();
      } else {

        if (this.educationDetails_id == 0) {
          this.httpServices.request('post', 'user/educations', null, null, buyer_Education).subscribe((data) => {
            toastr.success("Education Record Created Successfully");
            this.onEducationCancelBtn();
            this.onViewEducation();
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide()
          });
        } else {

          this.httpServices.request('patch', 'user/educations/' + this.educationDetails_id, null, null, buyer_Education).subscribe((data) => {
            toastr.success("Education Record Updated Successfully");
            this.onEducationCancelBtn();
            this.buyer_educationlDetailsList.forEach(x => {
              if (x.list_id == data.id) {
                x.country_id = data.country.id;
                x.country_name = data.country.country_name;
                x.university_name = data.university_name;
                x.education_title_id = data.education_title.id;
                x.education_title_name = data.education_title.education_title_name;
                x.education_major_Id = data.education_major.id;
                x.education_major_degree = data.education_major.education_major_name;
                x.education_year = data.education_year;
              }
            });
            this.progress.hide()
          }, error => {
            console.log(error);
            this.progress.hide()
          });
        }
      }
    }
  }

  //to display readonly Education values below modal.
  onViewEducation() {
    this.buyer_educationlDetailsList.push({
      list_id: this.buyer_educationlDetailsList.length + 1,
      country_id: this.getCountry.id,
      country_name: this.getCountry.country_name,
      university_name: this.universityDetails.university_name,
      education_title_id: this.getTitle.id,
      education_title_name: this.getTitle.title_name,
      education_major_Id: this.majorDetails.id,
      education_major_degree: this.majorDetails.education_major_name,
      education_year: this.getYear.year
    });
  }


  //To Delete Read Only Values 
  onDeleteEducation(list: any) {
    this.progress.show();
    if (this.buyer_educationlDetailsList.length > 1) {
      this.buyer_educationlDetailsList.splice(this.buyer_educationlDetailsList.findIndex(x => x.list_id == list.list_id), 1);

      this.httpServices.request('delete', 'user/educations/' + list.list_id, null, null, null).subscribe((data) => {
        toastr.success("Educations Record Deleted Successfully");
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    } else {
      toastr.error("Atleast one Education is required");
      this.progress.hide();
    }
  }

  //On edit pencil btn click action
  onEditEducation_pencilBtn(list: any) {
    if (this.countryList == undefined || this.countryList == null || this.countryList == '' || this.countryList.length == 0) {
      this.httpServices.request('get', 'country/?country_name=', '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.countryList = data;
            this.openEducationModal = true;
            this.educationDetails_id = list.list_id;
            this.getCountry.id = list.country_id,
              this.getCountry.country_name = list.country_name,
              this.universityDetails.university_name = list.university_name,
              this.getTitle.title_name = list.education_title_name,
              this.getTitle.id = list.education_title_id,
              this.majorDetails.education_major_name = list.education_major_degree,
              this.majorDetails.id = list.education_major_Id,
              this.getYear.year = list.education_year;
            this.getYear_year = new Date(Number(list.education_year), 1, 1);
          }
        });
    }
    else {
      this.openEducationModal = true;
      this.educationDetails_id = list.list_id;
      this.getCountry.id = list.country_id,
        this.getCountry.country_name = list.country_name,
        this.universityDetails.university_name = list.university_name,
        this.getTitle.id = list.education_title_id,
        this.getTitle.title_name = list.education_title_name,
        this.majorDetails.education_major_name = list.education_major_degree,
        this.majorDetails.id = list.education_major_Id,
        this.getYear.year = list.education_year;
      this.getYear_year = new Date(Number(list.education_year), 1, 1);
    }
  }

  //*************************************** 
  //  Certification Section
  //***************************************
  //On Add New Btn click
  onAddNewCertification() {

    this.checkvalidation_certificationModal = false;
    this.openCertificationModal = true;
    this.certificate_id = 0;
    this.certificateOrAward = '';
    this.certificateFrom = '';
    this.getCertificationYear.year = 0;
    this.getCertificationYear.id = 0;
    this.certificateYear = null;
  }

  //On Cancel btn  
  onCertificateCancelBtn() {
    this.openCertificationModal = false;
  }

  // On year Change in Certification section 
  public certificateYear: Date = null;
  onCertificationYearChange() {
    if (this.certificateYear != null) {
      this.yearList.forEach(element => {
        if (element.year == this.certificateYear.getFullYear()) {
          this.getCertificationYear.year = element.year;
          this.getCertificationYear.id = element.year_id;
        }
      });
    }
  }

  //on Update Certificate btn click 
  onUpdateCertification() {
    this.checkvalidation_certificationModal = true;
    let buyer_certification: any =
    {
      certification_name: "",
      certification_from: "",
      certification_year: 0
    }

    buyer_certification.certification_name = this.certificateOrAward;
    buyer_certification.certification_from = this.certificateFrom;
    //buyer_certification.certification_year = this.dateFormatter.format(new Date(Number(this.getCertificationYear.year), 1, 1), 'yyyy');
    buyer_certification.certification_year = this.dateFormatter.format(new Date(Number(this.getCertificationYear.year), 1, 1), 'yyyy');

    if (buyer_certification.certification_name != "" && buyer_certification.certification_name != null && buyer_certification.certification_name != undefined
      && buyer_certification.certification_from != "" && buyer_certification.certification_from != null && buyer_certification.certification_from != undefined
      && buyer_certification.certification_year != "" && buyer_certification.certification_year != 1900 && buyer_certification.certification_year != undefined) {
      this.progress.show();

      if (this.buyer_CertificationDetailsList.filter(element => element.certified_from == this.certificateFrom &&
        element.certificate_Award == this.certificateOrAward &&
        element.certification_year == this.getCertificationYear.year).length > 0) {
        toastr.error("This Certificate is already on your list.");
        this.progress.hide();
      } else {
        if (this.certificate_id == 0) {
          this.httpServices.request('post', 'user/certifications', null, null, buyer_certification).subscribe((data) => {
            toastr.success("Certification Record Updated Successfully");
            this.onCertificateCancelBtn();
            this.onViewCertification();
            this.progress.hide();
          }, error => {
            console.log(error);
            toastr.error("Certificate Record Not Updated Successfully");
            this.progress.hide();
          });
        }
        else {
          this.httpServices.request('patch', 'user/certifications/' + this.certificate_id, null, null, buyer_certification).subscribe((data) => {
            toastr.success("Certification Record Updated Successfully");
            this.onCertificateCancelBtn();
            this.buyer_CertificationDetailsList.forEach(x => {
              if (x.list_id == data.id) {
                x.certificate_Award = data.certification_name;
                x.certified_from = data.certification_from;
                x.certification_year = data.certification_year;
                x.year_id = data.year_id;
              }
            });
            this.progress.hide();
            this.onLanguageCancelBtn();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
      }
    }
  }

  onViewCertification() {
    this.buyer_CertificationDetailsList.push({
      list_id: this.buyer_CertificationDetailsList.length + 1,
      certificate_Award: this.certificateOrAward,
      certified_from: this.certificateFrom,
      certification_year: this.getCertificationYear.year,
      year_id: this.getCertificationYear.id
    });
  }

  //on Delete btn 
  onDeleteCertification(list: any) {
    this.progress.show();

    if (this.buyer_CertificationDetailsList.length > 1) {
      this.buyer_CertificationDetailsList.splice(this.buyer_CertificationDetailsList.findIndex(x => x.list_id == list.list_id), 1);

      this.httpServices.request('delete', 'user/certifications/' + list.list_id, null, null, null).subscribe((data) => {
        toastr.success("Certificate Record Deleted Successfully");
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    }
    else {
      toastr.error("Atleast one Certificate is Required");
      this.progress.hide();
    }
  }

  onEditcertification_pencilBtn(list: any) {
    this.getYearList();
    this.openCertificationModal = true;
    this.certificate_id = list.list_id;
    this.certificateOrAward = list.certificate_Award,
      this.certificateFrom = list.certified_from,
      this.getCertificationYear.year = list.certification_year;
    this.certificateYear = new Date(Number(list.certification_year), 1, 1);
  }



  // ===========================================
  // get linked accounts details 
  // ===========================================
  public social_mediaList: social_media = {
    social_media_array: [
      {
        id: 0,
        social_media_name: "",
        is_verified: false
      }
    ]
  }
  getLinkedAccountsData() {
    this.httpServices.request('get', 'social_media/', null, null, null).subscribe((data) => {
      this.httpServices.request('get', 'user/social-accounts-linked/' + this.userId, null, null, null).subscribe((response) => {
        if (response != null) {
          for (let i = 0; i < data.length; i++) {
            if (response.filter(x => x.social_media.id == data[i].id).length > 0) {
              this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: true });
            }
            else {
              this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: false });
            }
          }
        }
        else {
          for (let i = 0; i < data.length; i++) {
            this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: false });
          }
        }
        this.social_mediaList.social_media_array.splice(this.social_mediaList.social_media_array.findIndex(x => x.id == 0), 1);
      });
    });
  }
}
interface social_media {
  social_media_array: [
    {
      id: number,
      social_media_name: string,
      is_verified: boolean
    }
  ]
}

