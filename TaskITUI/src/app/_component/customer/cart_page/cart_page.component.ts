import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { EncryptDecryptService } from '../../../_common_services/encrypt-decrypt.service';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { toODataString } from '@progress/kendo-data-query';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { Router } from '@angular/router';
import { ExternalLibraryService } from 'src/app/utils';
import { FindValueSubscriber } from 'rxjs/internal/operators/find';

declare let Razorpay: any;
declare var toastr: any;
declare var jQuery: any;
@Component({
  selector: 'app-cart_page',
  templateUrl: './cart_page.component.html',
  styleUrls: ['./cart_page.component.css']
})
export class Cart_pageComponent implements OnInit {

  name = 'Angular';
  response;
  razorpayResponse;
  constructor(private razorpayService: ExternalLibraryService, private cd: ChangeDetectorRef, private router: Router, private dateFormatter: DateFormatterService, private progress: NgxSpinnerService, private httpServices: HttpRequestService, private encrypt_decrypt: EncryptDecryptService) {
    this.loadScripts();
  }
  // Method to dynamically load JavaScript 
  loadScripts() {
    // This array contains all the files/CDNs 
    const dynamicScripts = [
      'assets/js/easyResponsiveTabs.js'
    ];

    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
  public cartDetails: cartDetails = {
    id: 0,
    title: "",
    description: "",
    total_views: 0,
    is_price_package: false,
    rating: 0,
    rated_no_of_records: 0,
    gigs_pricing: {
      gig_price:
      {
        id: 0,
        price_type: {
          id: 0,
          price_type_name: ""
        },
        price: ""
      },
      gig_price_details: [
        {
          id: 0,
          sub_category_price_scope: {
            id: 0,
            seq_no: 0,
            price_scope_name: "",
            is_heading_visible: false,
            watermark: "",
            price_scope_control_type: {
              id: 0,
              price_scope_control_type_name: ""
            }
          },
          sub_category_price_scope_dtl: {
            id: 0,
            seq_no: 0,
            value: "",
            display_value: ""
          },
          value: ""
        }]
    },
    gallery: [
      {
        id: 0,
        file_path: "",
        file_path_thumbnail: ""
      }
    ],
    extra_services: [{
      id: 0,
      price_extra_service: {
        id: 0,
        title: "",
        description: "",
        is_price_type_applicable: false,
        heading_1: "",
        heading_1_master_name: "",
        heading_2: "",
        heading_2_master_name: "",
        help: "",
        is_active: false
      },
      price_type: {
        id: 0,
        price_type_name: ""
      },
      extra_price: "",
      extra_qty: 0,
      extra_prev_qty: 0,
      extra_check: false,
      extra_total: 0,
      extra_days_id: 0,
      extra_day: "",
      is_custom: false,
      title: "",
      description: ""
    }
    ],

    created_by: 0,
    created_user: ""
  };
  public cartExtraServiceDetails: cartExtraServiceDetails = {
    pricing: [{
      id: 0,
      price_extra_service: {
        id: 0,
        title: "",
        description: "",
        is_price_type_applicable: false,
        heading_1: "",
        heading_1_master_name: "",
        heading_2: "",
        heading_2_master_name: "",
        help: "",
        is_active: true
      },
      price_type: {
        id: 0,
        price_type_name: ""
      },
      extra_price: "",
      extra_qty: 0,
      extra_prev_qty: 0,
      extra_check: false,
      extra_total: 0,
      extra_days: {
        id: 0,
        seq_no: 0,
        extra_days_name: "",
        extra_days_value: 0
      }
    }
    ]
  };
  public cartRequirementDetails: cartRequirementDetails = {
    requirement:
      [{
        id: 0,
        gig_draft_id: "",
        gig_id: "",
        is_mandatory: false,
        question: "",
        question_form: {
          id: 0,
          question_form_name: ""
        },
        is_multiselect: false,
        gig_description: [
          {
            id: 0,
            description: "",
            check: false
          }
        ],
        ansText: "",
        ansCheckBox: "",
        ansDropDown: "",
        urls: []
      }]
  }
  public gig_Packages_block: gig_pricing_block = {
    CheckBoxDetails: [{
      checkBoxName: "",
      checkBoxValue: false
    }],
  }
  public is_tick_taskmer_wallet: boolean = false;
  public taskmer_wallet_amount: number = 0;
  public descriptionCheckArray: Array<{ requirementId: number, descriptionId: number, description: string, check: boolean }> = [];
  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userName: any;
  public queryParam: any;
  public _queryParams: any;
  public gig_id: any = 0;
  public quantityList: Array<{ id: number, qty: number }> = [];
  public getQuantity_main: any = { id: 0, Qty: 0 };
  public subTotal: any;
  public showQtyDropdown: boolean = false;
  public serviceFee: number = 5;
  public serviceFeePercent: number = 0;
  public totalCartValue: number;
  public subTotalTemp: any;
  public packageAmount: any;
  public extraServicesIncluded: any;
  public orderId: any;
  public order_no: any;
  public userId: any;
  public showOrderDetails: boolean = true;
  public showConfirmAndPay: boolean = false;
  public showsubmitRequirements: boolean = false;
  public activeTab: any = "order_details";
  public gig_status_id: any;
  public price_type_id: any;
  public showPhoto: boolean = true;
  public groupId: any;
  public extrafastAdded: boolean = false;
  public fromGuestLogin: boolean = false;
  public request_id: any;
  public buyer_mobile_no: any;
  ngOnInit() {
    this.razorpayService
      .lazyLoadLibrary('https://checkout.razorpay.com/v1/checkout.js')
      .subscribe();

    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    if (this.userAuthInfo.currentUser.user == undefined || this.userAuthInfo.currentUser.user == null) {
      this.groupId = 0;
      this.userId = 0;
      this.userName = "";
      this.serviceFeePercent = 0;
    }
    else {
      this.userId = this.userAuthInfo.currentUser.user.id;
      this.userName = this.userAuthInfo.currentUser.user.user_name;
      this.groupId = this.userAuthInfo.currentUser.user.group_id;
      this.serviceFeePercent = this.userAuthInfo.currentUser.sys_config.order_service_charge;

    }
    this.encrypt_decrypt.data_cart_login$.subscribe(x => {
      if (x.is_login) {
        this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
        this.userInfo = this.userAuthInfo.currentUser;
        this.groupId = x.groupId;
        this.userId = this.userAuthInfo.currentUser.user.id;
        this.userName = this.userAuthInfo.currentUser.user.user_name;
        let calculatedServiceFee;
        calculatedServiceFee = (Number(this.subTotal) * Number(x.servicePercent)) / 100;
        this.serviceFee = Number(calculatedServiceFee);
      }
    })
    if (this.queryParam == null) {
      this._queryParams = (sessionStorage.getItem("queryParamsCart") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      this.gig_id = this.queryParam.id;
      this.gig_status_id = this.queryParam.gig_status_id;
      this.price_type_id = this.queryParam.price_type_id;
      this.extrafastAdded = this.queryParam.extrafastAdded;
      if (this.queryParam.activeTab != null && this.queryParam.activeTab != undefined) {
        if (this.queryParam.activeTab == "confirm_pay") {
          this.orderId = this.queryParam.order_id;
          this.order_no = this.queryParam.order_no;
          this.showConfirmAndPay = true;
          this.showOrderDetails = true;
          this.activeTab = "confirm_pay";
          this.request_id = this.queryParam.request_id;

        }
      }
    }
    this.getCartDetails(this.gig_id);
    //this.getExtraServiceDetails(this.gig_id);
    this.getQuantityList();
    this.encrypt_decrypt.cartDataLogin$.subscribe(x => {
      if (x.is_login) {
        this.fromGuestLogin = true;
        this.onContinueClick();
      }
    })
  }

  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }

  directFromManageRequest() {
    let cartData = {
      buyer_id: 0,
      seller_id: 0,
      gig_id: 0,
      amount: "",
      extra_service_amount: "",
      extra_service_custom_amount: "",
      service_fee: "",
      total_amount: "",
      delivery_time: 0,
      order_gig_price: {
        price_type_id: 0,
        price: "",
        quantity: 0,
        amount: ""
      },
      extra_service: [
        {
          price_extra_service_id: 0,
          extra_days: 0,
          price: "",
          quantity: 0,
          amount: ""
        }
      ],
      custom_services: [
        {
          title: "",
          description: "",
          extra_price: "",
          extra_days: 0,
          quantity: 0,
          amount: ""
        }
      ],
      sub_category_price_details: [
        {
          sub_category_price_scope_id: 0,
          sub_category_price_scope_dtl_id: 0,
          value: ""
        }
      ]
    }
    this.userId = this.userAuthInfo.currentUser.user.id;
    this.userName = this.userAuthInfo.currentUser.user.user_name;
    this.groupId = this.userAuthInfo.currentUser.user.group_id;
    cartData.buyer_id = this.userId;
    cartData.seller_id = this.cartDetails.created_by;
    cartData.gig_id = this.gig_id;
    cartData.amount = this.subTotal;
    cartData.extra_service_amount = "0";
    cartData.extra_service_custom_amount = "0";
    let calculatedServiceFee;
    calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
    this.serviceFee = Number(calculatedServiceFee);
    cartData.service_fee = this.serviceFee.toString();
    cartData.total_amount = this.totalCartValue.toString();
    let filterDelivery = this.cartDetails.gigs_pricing.gig_price_details.filter(element => element.sub_category_price_scope.price_scope_name == "Delivery Time");
    let currentDate = new Date();
    let deliveryDate = new Date();
    cartData.order_gig_price.price_type_id = this.price_type_id;
    cartData.order_gig_price.quantity = this.getQuantity_main.id;
    cartData.order_gig_price.price = this.cartDetails.gigs_pricing.gig_price.price;
    cartData.order_gig_price.amount = (Number(this.cartDetails.gigs_pricing.gig_price.price) * Number(this.getQuantity_main.id)).toString();
    this.packageAmount = cartData.order_gig_price.amount;
    let filterData = this.cartDetails.extra_services.filter(element => element.extra_check == true && element.is_custom == false);
    let filterDataCustom = this.cartDetails.extra_services.filter(element => element.extra_check == true && element.is_custom == true);
    this.extraServicesIncluded = this.cartDetails.extra_services.filter(element => element.extra_check == true);
    if (this.extrafastAdded == false) {
      cartData.delivery_time = Number(filterDelivery[0].sub_category_price_scope_dtl.value.replace(' Days Delivery', '').replace(' Day Delivery', ''));
      this.extraServicesIncluded.forEach(element => {
        cartData.delivery_time = cartData.delivery_time + element.extra_day;
      });
    }
    else if (this.extrafastAdded == true) {
      cartData.delivery_time = this.deliveryTimeFinal
      this.extraServicesIncluded.forEach(element => {
        if (element.price_extra_service.title != "Extra fast delivery") {
          cartData.delivery_time = cartData.delivery_time + element.extra_day;
        }
      });
    }


    this.deliveryTimeFinal = cartData.delivery_time;
    if (filterData.length > 0) {
      filterData.forEach(element => {
        cartData.extra_service.push({
          price_extra_service_id: element.price_extra_service.id, extra_days: element.extra_days_id != null ? element.extra_days_id : 0,
          price: element.extra_price, quantity: element.extra_qty, amount: (Number(element.extra_price) * Number(element.extra_qty)).toString()
        });

        cartData.extra_service_amount = (Number(cartData.extra_service_amount) + (Number(element.extra_price) * Number(element.extra_qty))).toString()
      });

    }
    filterDataCustom.forEach(element => {
      cartData.custom_services.push({
        title: element.title, description: element.description, extra_price: element.extra_price,
        extra_days: element.extra_days_id, quantity: element.extra_qty, amount: (Number(element.extra_price) * Number(element.extra_qty)).toString()
      })

      cartData.extra_service_custom_amount = (Number(cartData.extra_service_custom_amount) + (Number(element.extra_price) * Number(element.extra_qty))).toString();
    })
    cartData.extra_service.splice(cartData.extra_service.findIndex(x => x.price_extra_service_id == 0), 1);
    cartData.custom_services.splice(cartData.custom_services.findIndex(x => x.title == ""), 1);
    if (this.cartDetails.gigs_pricing.gig_price_details.length > 0) {
      for (let i = 0; i < this.cartDetails.gigs_pricing.gig_price_details.length; i++) {
        if (this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.price_scope_control_type.id == 3 && this.cartDetails.gigs_pricing.gig_price_details[i].value == "true") {
          cartData.sub_category_price_details.push({
            sub_category_price_scope_id: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.id,
            sub_category_price_scope_dtl_id: null, value: this.cartDetails.gigs_pricing.gig_price_details[i].value
          });
        }
        else if (this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.price_scope_control_type.id == 2 && this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope_dtl != null &&
          this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope_dtl != undefined) {
          cartData.sub_category_price_details.push({
            sub_category_price_scope_id: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.id,
            sub_category_price_scope_dtl_id: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope_dtl.id,
            value: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope_dtl.value
          });
        }
        else if (this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.price_scope_control_type.id == 1) {
          cartData.sub_category_price_details.push({
            sub_category_price_scope_id: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.id,
            sub_category_price_scope_dtl_id: null,
            value: this.cartDetails.gigs_pricing.gig_price_details[i].value
          });
        }
      }
    }
    cartData.sub_category_price_details.splice(cartData.sub_category_price_details.findIndex(x => x.sub_category_price_scope_id == 0), 1);

    // let body = { order_id: this.orderId }
    // this.httpServices.request("post", "payments/pay", null, null, body).subscribe((data_razor) => {
    //   this.razor_order_details = data_razor;
    //   this.progress.hide();
    // }, (error) => {
    //   this.progress.hide();
    // });
  }
  // =============================
  // tabs handling 
  // ==============================
  onClickorderDetails() {
    if (this.request_id != null && this.request_id != undefined && this.request_id > 0) {
      let passQueryParam = { request_id: this.request_id };
      sessionStorage.setItem("queryParamsRequest", JSON.stringify(passQueryParam));
      this.router.navigate(['/seller_offer'], {});
    }
    else {
      this.activeTab = "order_details";
    }
  }
  onClickConfirmAndPay() {
    if (this.showConfirmAndPay == true) {
      //this.extraServicesIncluded = this.cartDetails.extra_services.filter(element => element.extra_check == true);
      //this.showConfirmAndPay = true;
      this.activeTab = "confirm_pay";
    }
  }
  onClickSubmitRequirements() {
    if (this.showsubmitRequirements == true) {
      this.activeTab = "submit_requirement";
    }
  }
  // =====================================
  // get QuantityList 
  // =====================================
  getQuantityList() {
    for (let i = 1; i < 21; i++) {
      this.quantityList.push({ id: i, qty: i });
    }
    this.getQuantity_main.id = 1;
  }
  // =====================================
  // get cart details 
  // =====================================
  public deliveryTimeFinal: any;
  getCartDetails(gig_id: any) {
    this.progress.show();
    this.httpServices.request('get', 'gigs_extra/gig-min-details/' + this.gig_id + '?price_type_id=' + this.price_type_id, null, null, null).subscribe((data) => {
      this.cartDetails = data;
      this.subTotal = this.cartDetails.gigs_pricing.gig_price.price.replace(".00", "");
      let calculatedServiceFee;
      calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
      this.serviceFee = Number(calculatedServiceFee);
      this.totalCartValue = Number(this.subTotal) + Number(this.serviceFee);
      // this.subTotal=this.subTotal+".00";
      if (this.cartDetails.gigs_pricing.gig_price_details.length > 0) {
        this.cartDetails.gigs_pricing.gig_price_details.forEach(element => {
          if (element.sub_category_price_scope.price_scope_name != "Package Name" && element.sub_category_price_scope.price_scope_name != "Package Description" &&
            element.sub_category_price_scope.price_scope_name != "Price") {
            if (element.sub_category_price_scope_dtl == null || element.sub_category_price_scope_dtl == undefined) {
              this.gig_Packages_block.CheckBoxDetails.push({ checkBoxName: element.sub_category_price_scope.price_scope_name, checkBoxValue: element.value == "true" ? true : false });
            }
            else {
              this.gig_Packages_block.CheckBoxDetails.push({
                checkBoxName: element.sub_category_price_scope_dtl.display_value,
                checkBoxValue: (element.sub_category_price_scope_dtl.display_value == null || element.sub_category_price_scope_dtl.display_value == "" ||
                  element.sub_category_price_scope_dtl.display_value == undefined) ? false : true
              });
            }

          }
        })

      }
      if (this.cartDetails.extra_services.length > 0) {
        if (this.extrafastAdded == true) {
          let spliceIds: any = [{
            id: 0
          }];
          data.extra_services.forEach(element => {
            if (element.price_extra_service.title == "Extra fast delivery" && element.price_type.id != this.price_type_id) {
              // this.cartDetails.extra_services.splice(this.cartDetails.extra_services.findIndex(x => x == element), 1);
              spliceIds.push({ id: element.id });
            }
            else if (element.price_extra_service.title == "Extra fast delivery" && element.price_type.id == this.price_type_id) {
              this.deliveryTimeFinal = element.extra_day;
            }
          }
          )
          spliceIds.splice(spliceIds.findIndex(x => x.id == 0), 1);
          spliceIds.forEach(element => {
            this.cartDetails.extra_services.splice(this.cartDetails.extra_services.findIndex(x => x.id == element.id), 1);
          });
          //this.cartDetails.extra_services.splice(this.cartDetails.extra_services.findIndex(element=>element.price_extra_service.title=="Extra fast delivery" && element.price_type.id!=this.price_type_id),2);
        }
        else {
          this.cartDetails.extra_services.forEach(element => {
            if (element.price_extra_service.title == "Extra fast delivery") {
              this.cartDetails.extra_services.splice(this.cartDetails.extra_services.findIndex(x => x == element), 1);
            }
          }
          )

          //this.cartDetails.extra_services.splice(this.cartDetails.extra_services.findIndex(element=>element.price_extra_service.title=="Extra fast delivery"),3);
        }
        this.cartDetails.extra_services.forEach(element => {
          element.is_custom = false;
          if (element.price_extra_service.title == "Extra fast delivery" && element.price_type.id == this.price_type_id) {
            element.extra_check = true;
            element.extra_qty = 1;
            this.subTotalTemp = 0;
            this.subTotal = 0;
            this.totalCartValue = 0;

            element.extra_total = Number(element.extra_qty) * Number(element.extra_price);
            this.subTotalTemp = Number(this.subTotalTemp) + Number(element.extra_total);
            this.subTotal = this.getQuantity_main.id * Number(this.cartDetails.gigs_pricing.gig_price.price) + Number(this.subTotalTemp);
            let calculatedServiceFee;
            calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
            this.serviceFee = Number(calculatedServiceFee);
            this.totalCartValue = Number(this.subTotal) + Number(this.serviceFee);
            // this.subTotal=this.subTotal+".00";
          }
          if (this.request_id != null && this.request_id != undefined && this.request_id > 0) {
            this.directFromManageRequest();
          }
        });
      }
      else {
        if (this.request_id != null && this.request_id != undefined && this.request_id > 0) {
          this.directFromManageRequest();
        }
      }
      //   id: 0,
      let price_extra_service_Temp: any = {
        id: 0,
        title: "",
        description: "",
        is_price_type_applicable: false,
        heading_1: "",
        heading_1_master_name: "",
        heading_2: "",
        heading_2_master_name: "",
        help: "",
        is_active: false
      };
      let price_type_Temp: any = {
        id: 0,
        price_type_name: ""
      };
      //   extra_price: "",
      //   extra_qty:0,
      //   extra_prev_qty:0,
      //   extra_check:false,
      //   extra_total:0,
      //   extra_days_id: 0,
      //   extra_day: ""
      // }
      if (data.extra_custom_services.length > 0) {
        data.extra_custom_services.forEach(element => {
          price_extra_service_Temp.id = element.id;
          price_extra_service_Temp.title = element.title;
          price_extra_service_Temp.description = element.description;
          price_extra_service_Temp.is_price_type_applicable = false;
          price_extra_service_Temp.heading_1 = "";
          price_extra_service_Temp.heading_1_master_name = "";
          price_extra_service_Temp.heading_2 = "";
          price_extra_service_Temp.heading_2_master_name = "";
          price_extra_service_Temp.help = "";
          price_extra_service_Temp.is_active = true;
          price_type_Temp.id = 1;
          price_type_Temp.price_type_name = "";
          this.cartDetails.extra_services.push({
            id: element.id, price_extra_service: price_extra_service_Temp,
            price_type: price_type_Temp, extra_price: element.extra_price, extra_qty: 0, extra_prev_qty: 0, extra_check: false,
            extra_total: 0, extra_days_id: element.extra_day, extra_day: element.extra_days, is_custom: true, title: element.title,
            description: element.description
          })
        });
      }
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
    //this.subTotal=this.subTotal+".00";
  }


  showgigDetails() {
    this.router.navigate(['/gigs_details'], { /*queryParams: passQueryParam /*, skipLocationChange: true*/ });
  }


  // ===================================
  // get extra Service details 
  // ===================================
  getExtraServiceDetails(gig_id: any) {
    this.progress.show();
    this.httpServices.request('get', 'gigs_extra/gig-extra-services/1', null, null, null).subscribe((data) => {

      this.cartExtraServiceDetails.pricing = data;
      this.cartExtraServiceDetails.pricing.forEach(element => element.extra_price = 200);
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }
  // ===================================
  // On Change of main quantity 
  // ===================================
  onQuantityMainChange() {

    if (this.getQuantity_main.id > 0) {
      if (this.cartDetails.extra_services.length > 0) {
        this.subTotalTemp = 0;
        this.subTotal = 0;
        this.totalCartValue = 0;
        let filterData = this.cartDetails.extra_services.filter(element => element.extra_check == true);
        if (filterData.length > 0) {
          for (let i = 0; i < filterData.length; i++) {
            this.subTotalTemp = Number(this.subTotalTemp) + Number(filterData[i].extra_total);
          }
          this.subTotal = this.getQuantity_main.id * Number(this.cartDetails.gigs_pricing.gig_price.price) + Number(this.subTotalTemp);
          let calculatedServiceFee;
          calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
          this.serviceFee = Number(calculatedServiceFee);
          this.totalCartValue = Number(this.subTotal) + Number(this.serviceFee);
        }
        else {
          this.subTotal = this.getQuantity_main.id * this.cartDetails.gigs_pricing.gig_price.price;
          let calculatedServiceFee;
          calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
          this.serviceFee = Number(calculatedServiceFee);
          this.totalCartValue = Number(this.subTotal) + Number(this.serviceFee);
        }

      }
      else {
        this.subTotal = this.getQuantity_main.id * this.cartDetails.gigs_pricing.gig_price.price;
        let calculatedServiceFee;
        calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
        this.serviceFee = Number(calculatedServiceFee);
        this.totalCartValue = Number(this.subTotal) + Number(this.serviceFee);
      }
    }
    //this.subTotal=this.subTotal+".00";

  }
  // ============================
  // on Extra checkBoxChange 
  // =============================
  onExtraCheckChange(item: any) {

    if (item.extra_check == true) {
      item.extra_qty = 1;
      item.extra_total = Number(item.extra_qty) * Number(item.extra_price);
    }
    else {
      item.extra_total = 0;
    }
    this.subTotal = 0;
    this.subTotalTemp = 0;
    this.totalCartValue = 0;
    for (let i = 0; i < this.cartDetails.extra_services.length; i++) {
      if (this.cartDetails.extra_services[i].extra_check == true) {
        this.subTotalTemp = Number(this.subTotalTemp) + Number(this.cartDetails.extra_services[i].extra_total);
      }
    }
    this.subTotal = this.getQuantity_main.id * Number(this.cartDetails.gigs_pricing.gig_price.price) + Number(this.subTotalTemp);
    let calculatedServiceFee;
    calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
    this.serviceFee = Number(calculatedServiceFee);
    this.totalCartValue = Number(this.subTotal) + Number(this.serviceFee);
    // this.subTotal=this.subTotal+".00";
  }
  // ===================================
  // on extra quantity change 
  // ===================================

  onQuantityExtraChange(item: any) {

    this.subTotalTemp = 0;
    this.subTotal = 0;
    this.totalCartValue = 0;
    if (item.extra_check == true) {
      item.extra_total = Number(item.extra_qty) * Number(item.extra_price);
    }
    else {
      item.extra_total = 0;
    }
    for (let i = 0; i < this.cartDetails.extra_services.length; i++) {
      if (this.cartDetails.extra_services[i].extra_check == true) {
        this.subTotalTemp = Number(this.subTotalTemp) + Number(this.cartDetails.extra_services[i].extra_total);
      }
    }
    this.subTotal = this.getQuantity_main.id * Number(this.cartDetails.gigs_pricing.gig_price.price) + Number(this.subTotalTemp);
    let calculatedServiceFee;
    calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
    this.serviceFee = Number(calculatedServiceFee);
    this.totalCartValue = Number(this.subTotal) + Number(this.serviceFee);
    // this.subTotal=this.subTotal+".00";
  }
  // =========================================
  //   On Click of 'Continue to Checkout'
  // =========================================
  onContinueClick() {
    if (this.userAuthInfo.currentUser.user == undefined) {
      this.encrypt_decrypt.changeDataCart({ flag: true });
    }
    else {
      if (this.userAuthInfo.currentUser.user.id == this.cartDetails.created_by) {
        toastr.error("You can not create order against own gig.");
      }
      else {
        if (this.fromGuestLogin) {
          this.encrypt_decrypt.changeDataTagCartLogin({
            is_login: false
          });
          this.fromGuestLogin = false;
        }
        let cartData = {
          buyer_id: 0,
          seller_id: 0,
          gig_id: 0,
          amount: "",
          extra_service_amount: "",
          extra_service_custom_amount: "",
          service_fee: "",
          total_amount: "",
          delivery_time: 0,
          order_gig_price: {
            price_type_id: 0,
            price: "",
            quantity: 0,
            amount: ""
          },
          extra_service: [
            {
              price_extra_service_id: 0,
              extra_days: 0,
              price: "",
              quantity: 0,
              amount: ""
            }
          ],
          custom_services: [
            {
              title: "",
              description: "",
              extra_price: "",
              extra_days: 0,
              quantity: 0,
              amount: ""
            }
          ],
          sub_category_price_details: [
            {
              sub_category_price_scope_id: 0,
              sub_category_price_scope_dtl_id: 0,
              value: ""
            }
          ]
        }
        this.userId = this.userAuthInfo.currentUser.user.id;
        this.userName = this.userAuthInfo.currentUser.user.user_name;
        this.groupId = this.userAuthInfo.currentUser.user.group_id;
        cartData.buyer_id = this.userId;
        cartData.seller_id = this.cartDetails.created_by;
        cartData.gig_id = this.gig_id;
        cartData.amount = this.subTotal;
        cartData.extra_service_amount = "0";
        cartData.extra_service_custom_amount = "0";
        let calculatedServiceFee;
        calculatedServiceFee = (Number(this.subTotal) * Number(this.serviceFeePercent)) / 100;
        this.serviceFee = Number(calculatedServiceFee);
        cartData.service_fee = this.serviceFee.toString();
        cartData.total_amount = this.totalCartValue.toString();
        let filterDelivery = this.cartDetails.gigs_pricing.gig_price_details.filter(element => element.sub_category_price_scope.price_scope_name == "Delivery Time");
        // cartData.delivery_time=Number(filterDelivery[0].sub_category_price_scope_dtl.value.replace(' Days Delivery','').replace(' Day Delivery',''));
        //cartData.delivery_time=Number(filterDelivery[0].sub_category_price_scope_dtl.value.replace(' Day Delivery',''));
        let currentDate = new Date();
        let deliveryDate = new Date();
        //deliveryDate=deliveryDate.setDate(currentDate.getDate()+cartData.delivery_time);
        //cartData.delivered_date=deliveryDate.setDate(deliveryDate.getDate()+cartData.delivery_time).toString();
        // cartData.delivered_date=this.dateFormatter.format(new Date(deliveryDate), 'yyyy-MM-dd');
        //cartData.delivered_date="2021-02-25 01:12:12";
        //cartData.delivered_date=new Date(deliveryDate.setDate(currentDate.getDate()+cartData.delivery_time));
        //cartData.delivered_date="2021-01-04T12:32:24.750Z";
        cartData.order_gig_price.price_type_id = this.price_type_id;
        cartData.order_gig_price.quantity = this.getQuantity_main.id;
        cartData.order_gig_price.price = this.cartDetails.gigs_pricing.gig_price.price;
        cartData.order_gig_price.amount = (Number(this.cartDetails.gigs_pricing.gig_price.price) * Number(this.getQuantity_main.id)).toString();
        this.packageAmount = cartData.order_gig_price.amount;
        let filterData = this.cartDetails.extra_services.filter(element => element.extra_check == true && element.is_custom == false);
        let filterDataCustom = this.cartDetails.extra_services.filter(element => element.extra_check == true && element.is_custom == true);
        this.extraServicesIncluded = this.cartDetails.extra_services.filter(element => element.extra_check == true);
        if (this.extrafastAdded == false) {
          cartData.delivery_time = Number(filterDelivery[0].sub_category_price_scope_dtl.value.replace(' Days Delivery', '').replace(' Day Delivery', ''));
          this.extraServicesIncluded.forEach(element => {
            cartData.delivery_time = cartData.delivery_time + element.extra_day;
          });
        }
        else if (this.extrafastAdded == true) {
          cartData.delivery_time = this.deliveryTimeFinal
          this.extraServicesIncluded.forEach(element => {
            if (element.price_extra_service.title != "Extra fast delivery") {
              cartData.delivery_time = cartData.delivery_time + element.extra_day;
            }
          });
        }


        this.deliveryTimeFinal = cartData.delivery_time;
        if (filterData.length > 0) {
          filterData.forEach(element => {
            cartData.extra_service.push({
              price_extra_service_id: element.price_extra_service.id, extra_days: element.extra_days_id != null ? element.extra_days_id : 0,
              price: element.extra_price, quantity: element.extra_qty, amount: (Number(element.extra_price) * Number(element.extra_qty)).toString()
            });

            cartData.extra_service_amount = (Number(cartData.extra_service_amount) + (Number(element.extra_price) * Number(element.extra_qty))).toString()
          });

        }
        filterDataCustom.forEach(element => {
          cartData.custom_services.push({
            title: element.title, description: element.description, extra_price: element.extra_price,
            extra_days: element.extra_days_id, quantity: element.extra_qty, amount: (Number(element.extra_price) * Number(element.extra_qty)).toString()
          })

          cartData.extra_service_custom_amount = (Number(cartData.extra_service_custom_amount) + (Number(element.extra_price) * Number(element.extra_qty))).toString();
        })
        cartData.extra_service.splice(cartData.extra_service.findIndex(x => x.price_extra_service_id == 0), 1);
        cartData.custom_services.splice(cartData.custom_services.findIndex(x => x.title == ""), 1);
        if (this.cartDetails.gigs_pricing.gig_price_details.length > 0) {
          for (let i = 0; i < this.cartDetails.gigs_pricing.gig_price_details.length; i++) {
            if (this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.price_scope_control_type.id == 3 && this.cartDetails.gigs_pricing.gig_price_details[i].value == "true") {
              cartData.sub_category_price_details.push({
                sub_category_price_scope_id: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.id,
                sub_category_price_scope_dtl_id: null, value: this.cartDetails.gigs_pricing.gig_price_details[i].value
              });
            }
            else if (this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.price_scope_control_type.id == 2 && this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope_dtl != null &&
              this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope_dtl != undefined) {
              cartData.sub_category_price_details.push({
                sub_category_price_scope_id: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.id,
                sub_category_price_scope_dtl_id: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope_dtl.id,
                value: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope_dtl.value
              });
            }
            else if (this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.price_scope_control_type.id == 1) {
              cartData.sub_category_price_details.push({
                sub_category_price_scope_id: this.cartDetails.gigs_pricing.gig_price_details[i].sub_category_price_scope.id,
                sub_category_price_scope_dtl_id: null,
                value: this.cartDetails.gigs_pricing.gig_price_details[i].value
              });
            }
          }
        }
        cartData.sub_category_price_details.splice(cartData.sub_category_price_details.findIndex(x => x.sub_category_price_scope_id == 0), 1);
        this.progress.show();
        this.httpServices.request("post", "orders/create", null, null, cartData).subscribe((data) => {
          this.orderId = data.id;
          this.order_no = data.order_no;
          //toastr.success("Records saved successfully");
          if (this.userAuthInfo.currentUser.user.is_seller == 1) {
            this.httpServices.request("get", "user/seller-wallet-balance", null, null, cartData).subscribe((data) => {
              this.taskmer_wallet_amount = data.total_balance_amount;
              this.progress.hide();
            });
          }
          else {
            this.progress.hide();
          }
          this.showConfirmAndPay = true;
          this.showOrderDetails = true;
          this.activeTab = "confirm_pay";
        }, (error) => {
          this.progress.hide();
        });
      }
    }
  }
  public razor_order_details: any;
  // =========================================
  // on click of submit on Confirm & pay tab 
  // =========================================
  onSubMitPayment() {
    if (this.buyer_mobile_no != '' && this.buyer_mobile_no != undefined) {
      this.progress.show();
      let body = { order_id: this.orderId }
      if (this.is_tick_taskmer_wallet == true && this.taskmer_wallet_amount >= this.totalCartValue) {
        let wallet_body = {
          order_id: this.orderId,
          razorpay_payment_id: null,
          wallet_amount: this.totalCartValue,
          is_complete_wallet_payment: true
        }
        this.httpServices.request("post", "payments/wallet-payment", null, null, wallet_body).subscribe((wallet) => {
          let orderId = {
            order_id: 0
          }
          orderId.order_id = this.orderId;
          this.httpServices.request("patch", "orders/active/" + this.orderId, null, null, null).subscribe((data) => {
            this.httpServices.request("post", "orders/empty-requirements-create", null, null, orderId).subscribe((data) => {
              this.httpServices.request("get", "orders/requirement/" + this.orderId, null, null, null).subscribe((response) => {
                this.progress.hide();
                this.cartRequirementDetails.requirement = response;
                if (this.cartRequirementDetails.requirement != null && this.cartRequirementDetails.requirement != undefined
                  && this.cartRequirementDetails.requirement.length > 0) {
                  this.is_back_to_order = false;
                  this.cartRequirementDetails.requirement.forEach(element => {
                    element.urls = [];
                    if (element.question_form.id == 2 && element.is_multiselect == false) {
                      element.ansDropDown = "0";
                    }
                  });
                }
                else {
                  this.update_inprocess_status();
                }
                //this.getRequirementDetails();
                this.showsubmitRequirements = true;
                this.showConfirmAndPay = true;
                this.showOrderDetails = true;
                this.activeTab = "submit_requirement";
                //this.onClickSubmitRequirements();
                document.getElementById('submit_requirement_step').click();
              });
            });
          });
          //this.progress.hide();
        });
      }
      else {
        this.httpServices.request("post", "payments/pay", null, null, body).subscribe((data_razor) => {
          this.razor_order_details = data_razor;
          this.progress.hide();
          this.payWithRazor(this.razor_order_details);
        }, (error) => {
          this.progress.hide();
        });
      }
    }
    else {
      toastr.error("Please enter mobile no.");
    }
    // this.progress.show();
    // this.httpServices.request("post","orders/payment-create",null,null,null).subscribe((data)=>{
    //   this.progress.hide()
    //   toastr.success("Payment done successfully");
    //   this.showsubmitRequirements=true;
    // this.showConfirmAndPay=true;
    // this.showOrderDetails=true;
    // this.activeTab="submit_requirement";
    // }, (error) => {
    //   this.progress.hide();
    // });
  }
  // ============================================
  // get requirement details 
  // ============================================
  getRequirementDetails() {

    this.progress.show();
    this.httpServices.request("get", "gigs/requirements-list/" + this.gig_id, null, null, null).subscribe((data) => {

      //this.cartRequirementDetails.requirement=data;
      // for (let i = 0; i < this.cartRequirementDetails.requirement.length; i++) {
      //   data.forEach(element => {
      //     if (this.cartRequirementDetails.requirement[i].id == element.id) {
      //       this.cartRequirementDetails.requirement[i].gig_description = element.gig_description;
      //     }
      //   });
      // }    
      for (let i = 0; i < data.length; i++) {
        let gig_description: any = [];
        if (data[i].descriptions != null && data[i].descriptions != undefined && data[i].descriptions.length > 0) {
          data[i].descriptions.forEach(element => {
            gig_description.push({ id: element.id, description: element.description, check: false })
          });
        }
        this.cartRequirementDetails.requirement.push({
          id: data[i].id,
          gig_draft_id: "",
          gig_id: data[i].gig_id,
          is_mandatory: data[i].is_mandatory,
          question: data[i].question,
          question_form: data[i].question_form,
          is_multiselect: data[i].is_multiselect,
          gig_description: gig_description,
          ansText: "",
          ansCheckBox: "",
          ansDropDown: "",
          urls: []
        })
      }
      if (this.cartRequirementDetails.requirement.length > 0) {
        this.cartRequirementDetails.requirement.splice(this.cartRequirementDetails.requirement.findIndex(element => element.id == 0), 1);
      }
      this.showsubmitRequirements = true;
      this.showConfirmAndPay = true;
      this.showOrderDetails = true;
      this.activeTab = "submit_requirement";
      this.progress.hide();
      document.getElementById("myCheck").click();
    }, (error) => {
      this.progress.hide();
    });
  }
  onABCClick() {
    this.showsubmitRequirements = true;
    this.showConfirmAndPay = true;
    this.showOrderDetails = true;
    this.activeTab = "submit_requirement";
    this.progress.hide();
  }
  // ========================================
  // on check of requirement answers 
  // =========================================
  onrequirementCheckChange(requirementId: any, descriptionId: any, description: any, check: boolean) {

    if (check == true) {
      this.descriptionCheckArray.push({
        requirementId: requirementId, descriptionId: descriptionId, description: description,
        check: check
      });
    }
    else {
      if (this.descriptionCheckArray.filter(element => element.requirementId == requirementId && element.descriptionId == descriptionId).length > 0) {
        this.descriptionCheckArray.splice(this.descriptionCheckArray.findIndex(element => element.requirementId == requirementId && element.descriptionId == descriptionId), 1);
      }
    }
  }
  public fileErrorMsgArr: any[] = [];
  public campFileSpan: any;
  public none = "none";
  public filename: string = "";
  //public urls: any[] = [];
  fileChanged(e: any, item: any) {
    this.fileErrorMsgArr = [];
    this.campFileSpan = this.none;
    //to make sure the user select file/files
    if (!e.target.files) {
      this.filename = "No file choosen";
      return;
    }
    if (e.target.files.length > 3) {
      toastr.error("You can upload only 3 files.");
      this.resetFileInput(this.file_image);
      return;
    }
    //To obtaine a File reference
    var files = e.target.files;
    let index: number = 0;
    let fileLength: number = 0;
    fileLength = files.length;
    // this.filename=files[0].name;
    // Loop through the FileList and then to render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      //instantiate a FileReader object to read its contents into memory
      var fileReader = new FileReader();
      index = i;
      // Closure to capture the file information and apply validation.
      fileReader.onload = (function (readerEvt, urls, fileErrorMsgArr, index, fileLength) {
        return function (e) {

          let fileType = "";
          let fileSize = "";
          let alertstring = '';

          //To check file type according to upload conditions
          if (readerEvt.type == "image/jpeg" || readerEvt.type == "image/png" || readerEvt.type == "image/jpg" ||
            readerEvt.type == "image/bmp" || readerEvt.type == "image/gif" || readerEvt.type == "image/gif" || readerEvt.type == "application/pdf"
            || readerEvt.type == "audio/ogg" || readerEvt.type == "audio/mpeg" || readerEvt.type == "audio/mp3" ||
            readerEvt.type == "video/mp4" || readerEvt.type == "video/ogg" || readerEvt.type == "application/pptx" || readerEvt.type == "application/docx"
            || readerEvt.type == "application/txt" || readerEvt.type == "application/xlsx" || readerEvt.type == "csv" || readerEvt.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
            readerEvt.type == "application/vnd.ms-excel" || readerEvt.type == "application/msword" || readerEvt.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            || readerEvt.type == "application/vnd.ms-powerpoint" || readerEvt.type == "text/plain") {
            fileType = "";
          }
          else {
            fileErrorMsgArr.push({ "issue": "filetype", "filename": readerEvt.name, "fileSize": 0 });
            fileType = ("<li>" + "The file(" + readerEvt.name
              + ") does not match the upload conditions, You can only upload jpeg/png/jpg/bmp/gif/pdf files" + "</li>");
          }

          //To check file Size according to upload conditions
          if (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024 <= 5) {
            fileSize = "";
          }
          else {
            fileErrorMsgArr.push({ "issue": "filesize", "filename": readerEvt.name, "fileSize": (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) });

            fileSize = ("<li>" + "The file(" + readerEvt.name
              + ") does not match the upload conditions, Your file size is: "
              + (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2)
              + " MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
          }
          if (fileType == "" && fileSize == "" && item.urls.length == 0) {
            item.urls.push({ id: index, file: readerEvt, file_path: e.target.result, file_name: readerEvt.name });
          }
          else if (fileType == "" && fileSize == "" && urls.length > 0 && urls.filter(abc => abc.file_name == readerEvt.name).length == 0) {
            item.urls.push({ id: index, file: readerEvt, file_path: e.target.result, file_name: readerEvt.name });
          }
          alertstring = alertstring + fileType + fileSize;
          if (alertstring != '') {
            this.filename = "No file chosen";//this.translate.instant('common.nofilechosen');
            //toastr.error(alertstring);
            if (index == (fileLength - 1))
              jQuery("#fileErrorMsg").click();
          }
        };
      })(f, item.urls, this.fileErrorMsgArr, index, fileLength);


      fileReader.readAsDataURL(f);

    }
    this.resetFileInput(this.file_image);

  }
  displayFileErrorMsg() {
    let errorMsg: string = "";
    if (this.fileErrorMsgArr.length > 0) {
      this.fileErrorMsgArr.forEach(element => {
        if (element.issue == "filetype") {
          errorMsg = errorMsg + "<li>" + "The file " +
            element.filename + " does not match file type." + "</li>";
        }
        else if (element.issue == "filesize") {
          errorMsg = errorMsg + "<li>" + "The file " +
            element.filename + " having sizze: "
            + element.filesize + " does not match file size condition." + "</li>";
        }
      });
      toastr.error(errorMsg);
    }
  }
  deleteImage(event: any, item: any) {
    item.urls.splice(item.urls.findIndex(x => x.file_name == event.file_name), 1);
  }
  // ==================================
  // on CLick of Submit Requirement 
  // ==================================
  onSubmitRequirement() {

    let cartRequirementData = {

      requirements: [
        {
          id: 0,
          is_mandatory: true,
          question: "",
          question_form_id: 0,
          is_multiselect: true,
          description: [
            {
              file_path: "",
              description: ""
            }
          ]
        }
      ]
    }

    // let formData = new FormData();

    // for(let i=0;i<this.cartRequirementDetails.requirement.length;i++)
    // {
    //   let description:Array<{file_path:string,description:string}>=[];
    //   if(this.cartRequirementDetails.requirement[i].question_form.id==1 && 
    //     (this.cartRequirementDetails.requirement[i].ansText!=null || this.cartRequirementDetails.requirement[i].ansText!=undefined
    //        || this.cartRequirementDetails.requirement[i].ansText!=""))
    //        {
    //         count = count+1;
    //         description.push({file_path:null,description:this.cartRequirementDetails.requirement[i].ansText});
    //         formData.append("requirements[" + count + "]is_mandatory", this.cartRequirementDetails.requirement[i].is_mandatory.toString());
    //         formData.append("requirements[" + count + "]question", this.cartRequirementDetails.requirement[i].question);
    //         formData.append("requirements[" + count + "]question_form_id", this.cartRequirementDetails.requirement[i].question_form.id.toString());
    //         formData.append("requirements[" + count + "]is_multiselect", this.cartRequirementDetails.requirement[i].is_multiselect.toString());
    //         // formData.append("requirements[" + count + "]description", description.toString());
    //         formData.append("requirements[" + count + "]description[1]file_path", null);
    //         formData.append("requirements[" + count + "]description[1]description", this.cartRequirementDetails.requirement[i].ansText);
    //        }
    //   else if(this.cartRequirementDetails.requirement[i].question_form.id==2 && this.cartRequirementDetails.requirement[i].is_multiselect==false
    //      && (this.cartRequirementDetails.requirement[i].ansDropDown!=null || this.cartRequirementDetails.requirement[i].ansDropDown!=undefined
    //         || this.cartRequirementDetails.requirement[i].ansDropDown!=""))
    //           {
    //             count = count + 1;
    //             description.push({file_path:null,description:this.cartRequirementDetails.requirement[i].ansDropDown});
    //             formData.append("requirements[" + count + "]is_mandatory", this.cartRequirementDetails.requirement[i].is_mandatory.toString());
    //             formData.append("requirements[" + count + "]question", this.cartRequirementDetails.requirement[i].question);
    //             formData.append("requirements[" + count + "]question_form_id", this.cartRequirementDetails.requirement[i].question_form.id.toString());
    //             formData.append("requirements[" + count + "]is_multiselect", this.cartRequirementDetails.requirement[i].is_multiselect.toString());
    //             //formData.append("requirements[" + count + "]description", description.toString());
    //             formData.append("requirements[" + count + "]description[1]file_path", null);
    //             formData.append("requirements[" + count + "]description[1]description", this.cartRequirementDetails.requirement[i].ansDropDown);
    //           }
    //   else if(this.cartRequirementDetails.requirement[i].question_form.id==2 && this.cartRequirementDetails.requirement[i].is_multiselect==true 
    //     && this.descriptionCheckArray.length>0)
    //           {
    //           let filterData=this.descriptionCheckArray.filter(element=>element.requirementId==this.cartRequirementDetails.requirement[i].id);
    //           filterData.forEach(x=>{
    //             description.push({file_path:null,description:x.description});
    //            })
    //            count = count + 1;
    //             formData.append("requirements[" + count + "]is_mandatory", this.cartRequirementDetails.requirement[i].is_mandatory.toString());
    //             formData.append("requirements[" + count + "]question", this.cartRequirementDetails.requirement[i].question);
    //             formData.append("requirements[" + count + "]question_form_id", this.cartRequirementDetails.requirement[i].question_form.id.toString());
    //             formData.append("requirements[" + count + "]is_multiselect", this.cartRequirementDetails.requirement[i].is_multiselect.toString());
    //             //formData.append("requirements[" + count + "]description", description.toString());
    //             for(let x=0;x<filterData.length;x++)
    //             {
    //               formData.append("requirements[" + count + "]description["+(x+1)+"]file_path", null);
    //               formData.append("requirements[" + count + "]description["+(x+1)+"]description", filterData[x].description);
    //             }
    //           }
    //   else if(this.cartRequirementDetails.requirement[i].question_form.id==3 && this.urls != null && this.urls != undefined && this.urls != []) {
    //             // for (var k = 0; k < this.urls.length; k++) {
    //             //   count = count + 1;
    //             //   description.push({file_path:this.urls[k].file,description:""});
    //             // }
    //               count = count + 1;
    //               formData.append("requirements[" + count + "]is_mandatory", this.cartRequirementDetails.requirement[i].is_mandatory.toString());
    //               formData.append("requirements[" + count + "]question", this.cartRequirementDetails.requirement[i].question);
    //               formData.append("requirements[" + count + "]question_form_id", this.cartRequirementDetails.requirement[i].question_form.id.toString());
    //               formData.append("requirements[" + count + "]is_multiselect", this.cartRequirementDetails.requirement[i].is_multiselect.toString());
    //               //formData.append("requirements[" + count + "]description", description.toString());
    //               for (var k = 0; k < this.urls.length; k++) {
    //                 // count = count + 1;
    //                 // description.push({file_path:this.urls[k].file,description:""});
    //                 formData.append("requirements[" + count + "]description["+(k+1)+"]file_path", this.urls[k].file);
    //                 formData.append("requirements[" + count + "]description["+(k+1)+"]description", "");
    //               }
    //           }

    // }
    // formData.append("order_id", this.orderId);

    let flag = true;
    if (this.cartRequirementDetails.requirement.length > 0) {
      this.cartRequirementDetails.requirement.forEach(element => {
        if (element.question_form.id == 1 && element.is_mandatory == true && (element.ansText == "" || element.ansText == undefined || element.ansText == null)) {
          flag = false;
        }
        else if (element.question_form.id == 2 && element.is_multiselect == false && element.is_mandatory == true && (element.ansDropDown == "" ||
          element.ansDropDown == null || element.ansDropDown == undefined || element.ansDropDown == "0")) {
          flag = false;
        }
        else if (element.question_form.id == 2 && element.is_multiselect == true && element.is_mandatory == true && this.descriptionCheckArray.length <= 0) {
          flag = false;
        }
        else if (element.question_form.id == 3 && element.is_mandatory == true && element.urls.length <= 0) {
          flag = false;
        }
      }
      )
      if (flag == true) {
        let formData = new FormData();
        let count = 0;
        for (let i = 0; i < this.cartRequirementDetails.requirement.length; i++) {
          let description: Array<{ file_path: string, description: string }> = [];
          if (this.cartRequirementDetails.requirement[i].question_form.id == 1 &&
            (this.cartRequirementDetails.requirement[i].ansText != null || this.cartRequirementDetails.requirement[i].ansText != undefined
              || this.cartRequirementDetails.requirement[i].ansText != "")) {
            description.push({ file_path: null, description: this.cartRequirementDetails.requirement[i].ansText });
            cartRequirementData.requirements.push({
              id: this.cartRequirementDetails.requirement[i].id, is_mandatory: this.cartRequirementDetails.requirement[i].is_mandatory,
              question: this.cartRequirementDetails.requirement[i].question, question_form_id: this.cartRequirementDetails.requirement[i].question_form.id,
              is_multiselect: this.cartRequirementDetails.requirement[i].is_multiselect, description: description
            });
          }
          else if (this.cartRequirementDetails.requirement[i].question_form.id == 2 && this.cartRequirementDetails.requirement[i].is_multiselect == false
            && (this.cartRequirementDetails.requirement[i].ansDropDown != null || this.cartRequirementDetails.requirement[i].ansDropDown != undefined
              || this.cartRequirementDetails.requirement[i].ansDropDown != "")) {
            description.push({ file_path: null, description: this.cartRequirementDetails.requirement[i].ansDropDown });
            cartRequirementData.requirements.push({
              id: this.cartRequirementDetails.requirement[i].id, is_mandatory: this.cartRequirementDetails.requirement[i].is_mandatory,
              question: this.cartRequirementDetails.requirement[i].question, question_form_id: this.cartRequirementDetails.requirement[i].question_form.id,
              is_multiselect: this.cartRequirementDetails.requirement[i].is_multiselect, description: description
            });
          }
          else if (this.cartRequirementDetails.requirement[i].question_form.id == 2 && this.cartRequirementDetails.requirement[i].is_multiselect == true
            && this.descriptionCheckArray.length > 0) {
            let filterData = this.descriptionCheckArray.filter(element => element.requirementId == this.cartRequirementDetails.requirement[i].id);
            filterData.forEach(x => {
              description.push({ file_path: null, description: x.description });
            })
            cartRequirementData.requirements.push({
              id: this.cartRequirementDetails.requirement[i].id, is_mandatory: this.cartRequirementDetails.requirement[i].is_mandatory,
              question: this.cartRequirementDetails.requirement[i].question, question_form_id: this.cartRequirementDetails.requirement[i].question_form.id,
              is_multiselect: this.cartRequirementDetails.requirement[i].is_multiselect, description: description
            });
          }
          else if (this.cartRequirementDetails.requirement[i].question_form.id == 3 && this.cartRequirementDetails.requirement[i] != null && this.cartRequirementDetails.requirement[i].urls != undefined && this.cartRequirementDetails.requirement[i].urls != []) {
            count = count + 1;
            formData.append("requirements[" + count + "]id", this.cartRequirementDetails.requirement[i].id.toString());
            formData.append("requirements[" + count + "]is_mandatory", this.cartRequirementDetails.requirement[i].is_mandatory.toString());
            formData.append("requirements[" + count + "]question", this.cartRequirementDetails.requirement[i].question);
            formData.append("requirements[" + count + "]question_form_id", this.cartRequirementDetails.requirement[i].question_form.id.toString());
            formData.append("requirements[" + count + "]is_multiselect", this.cartRequirementDetails.requirement[i].is_multiselect.toString());
            //formData.append("requirements[" + count + "]description", description.toString());
            for (var k = 0; k < this.cartRequirementDetails.requirement[i].urls.length; k++) {
              // count = count + 1;
              // description.push({file_path:this.urls[k].file,description:""});
              // formData.append("requirements[" + count + "]description["+(k+1)+"]id", "0");
              formData.append("requirements[" + count + "]description[" + (k + 1) + "]file_path", this.cartRequirementDetails.requirement[i].urls[k].file);
              formData.append("requirements[" + count + "]description[" + (k + 1) + "]description", "");
            }
          }
        }
        cartRequirementData.requirements.splice(cartRequirementData.requirements.findIndex(x => x.question_form_id == 0), 1);

        this.progress.show();
        this.httpServices.request("patch", "orders/requirements-update/" + this.orderId, null, null, cartRequirementData).subscribe((data) => {
          this.httpServices.request("patch", "orders/requirements-update/" + this.orderId, null, null, formData).subscribe((response) => {
            toastr.success("Requirement has been saved successfully");
            let passQueryParam = { activeTab: "in_process" };
            sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
            this.router.navigate(['/buyer_orders'], {});
            this.progress.hide();
          }, (error) => {
            this.progress.hide();
          });
        }, (error) => {
          this.progress.hide();
        });
      }
      else {
        toastr.error("Please fill mandatory details");

      }
    }
    else {

    }
  }
  public is_back_to_order: boolean = false;
  update_inprocess_status() {
    this.is_back_to_order = true;
    this.httpServices.request("patch", "orders/requirements-update/" + this.orderId, null, null, null).subscribe((data) => {
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }

  back_to_order() {
    let passQueryParam = { activeTab: "in_process" };
    sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
    this.router.navigate(['/buyer_orders'], {});
  }

  onlyCharacters_withApostrophe(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code == 39) && // apostrophe
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }
  click_changefilePhoto() {
    //this.labourDetails.labour_photo_data = '';
    this.showPhoto = true;
  }
  click_viewfile(item: any) {
    window.open(item);
  }
  resetFileInput(file: any) {
    if (file)
      file.nativeElement.value = "";
  }
  @ViewChild('file_image', { read: ElementRef }) file_image: ElementRef;


  RAZORPAY_OPTIONS = {
    "key": "",
    "amount": "",
    "name": "Gig",
    "order_id": "",
    "description": "",
    "image": "https://livestatic.novopay.in/resources/img/nodeapp/img/Logo_NP.jpg",
    "prefill": {
      "name": "",
      "email": "test@test.com",
      "contact": "",
      "method": ""
    },
    "modal": {},
    "theme": {
      "color": "#0096C5"
    }
  };
  payWithRazor(razor_order_details: any) {
    this.RAZORPAY_OPTIONS.order_id = razor_order_details.id;
    this.RAZORPAY_OPTIONS.prefill.email = this.userAuthInfo.currentUser.user.email_address;
    this.RAZORPAY_OPTIONS.prefill.contact = this.buyer_mobile_no;
    this.RAZORPAY_OPTIONS.name = this.cartDetails.title;
    this.RAZORPAY_OPTIONS.image = this.cartDetails.gallery[0].file_path;

    // binding this object to both success and dismiss handler
    this.RAZORPAY_OPTIONS['handler'] = this.razorPaySuccessHandler.bind(this);

    // this.showPopup();

    let razorpay = new Razorpay(this.RAZORPAY_OPTIONS)
    razorpay.open();
  }

  public razorPaySuccessHandler(response) {
    console.log(response);
    let body = {
      razorpay_order_id: response.razorpay_order_id,
      razorpay_payment_id: response.razorpay_payment_id,
      razorpay_signature: response.razorpay_signature,
      amount: this.is_tick_taskmer_wallet == true ? (this.totalCartValue - this.taskmer_wallet_amount) : this.totalCartValue
    }
    //this.progress.show();
    this.httpServices.request("post", "payments/success", null, null, body).subscribe((data_razor) => {
      if (this.is_tick_taskmer_wallet == true) {
        let wallet_body = {
          order_id: this.orderId,
          razorpay_payment_id: response.razorpay_payment_id,
          wallet_amount: this.taskmer_wallet_amount,
          is_complete_wallet_payment: false
        }
        this.httpServices.request("post", "payments/wallet-payment", null, null, wallet_body).subscribe((wallet) => {

        });
      }
      let orderId = {
        order_id: 0
      }
      orderId.order_id = this.orderId;
      this.httpServices.request("patch", "orders/active/" + this.orderId, null, null, null).subscribe((data) => {
        this.httpServices.request("post", "orders/empty-requirements-create", null, null, orderId).subscribe((data) => {
          this.httpServices.request("get", "orders/requirement/" + this.orderId, null, null, null).subscribe((response) => {
            //this.progress.hide();
            this.cartRequirementDetails.requirement = response;
            if (this.cartRequirementDetails.requirement != null && this.cartRequirementDetails.requirement != undefined
              && this.cartRequirementDetails.requirement.length > 0) {
              this.is_back_to_order = false;
              this.cartRequirementDetails.requirement.forEach(element => {
                element.urls = [];
                if (element.question_form.id == 2 && element.is_multiselect == false) {
                  element.ansDropDown = "0";
                }
              });
            }
            else {
              this.update_inprocess_status();
            }
            //this.getRequirementDetails();
            this.showsubmitRequirements = true;
            this.showConfirmAndPay = true;
            this.showOrderDetails = true;
            this.activeTab = "submit_requirement";
            //this.onClickSubmitRequirements();
            document.getElementById('submit_requirement_step').click();
          });
        });
      });
      //this.getRequirementDetails();
    }, (error) => {
      console.log(error);
      this.progress.hide();
    });
    this.cd.detectChanges()
  }

  public test() {
    document.getElementById('response-modal').style.display = 'block';
    this.response = `dummy text`;
  }
}
interface cartDetails {
  id: number,
  title: string,
  description: string,
  total_views: number,
  is_price_package: boolean,
  rating: number,
  rated_no_of_records: number,
  gigs_pricing: {
    gig_price:
    {
      id: number,
      price_type: {
        id: number,
        price_type_name: string
      },
      price: any
    }
    gig_price_details: [
      {
        id: number,
        sub_category_price_scope: {
          id: number,
          seq_no: number,
          price_scope_name: string,
          is_heading_visible: boolean,
          watermark: string,
          price_scope_control_type: {
            id: number,
            price_scope_control_type_name: string
          }
        },
        sub_category_price_scope_dtl: {
          id: number,
          seq_no: number,
          value: string,
          display_value: string
        },
        value: string
      }]
  },
  gallery: [
    {
      id: number,
      file_path: string,
      file_path_thumbnail: string
    }
  ],
  extra_services: [{
    id: number,
    price_extra_service: {
      id: number,
      title: string,
      description: string,
      is_price_type_applicable: boolean,
      heading_1: string,
      heading_1_master_name: string,
      heading_2: string,
      heading_2_master_name: string,
      help: string,
      is_active: boolean
    },
    price_type: {
      id: number,
      price_type_name: string
    },
    extra_price: any,
    extra_qty: number,
    extra_prev_qty: number,
    extra_check: boolean,
    extra_total: any,
    extra_days_id: number,
    extra_day: string,
    is_custom: boolean,
    title: string,
    description: string
  }
  ]
  created_by: number,
  created_user: string
}
interface cartExtraServiceDetails {
  pricing: [{
    id: number,
    price_extra_service: {
      id: number,
      title: string,
      description: string,
      is_price_type_applicable: boolean,
      heading_1: string,
      heading_1_master_name: string,
      heading_2: string,
      heading_2_master_name: string,
      help: string,
      is_active: boolean
    },
    price_type: {
      id: number,
      price_type_name: string
    },
    extra_price: any,
    extra_qty: number,
    extra_prev_qty: number,
    extra_check: boolean,
    extra_total: any,
    extra_days: {
      id: number,
      seq_no: number,
      extra_days_name: string,
      extra_days_value: number
    }
  }
  ]
}
interface cartRequirementDetails {
  requirement:
  [{
    id: number,
    gig_draft_id: string,
    gig_id: string,
    is_mandatory: boolean,
    question: string,
    question_form: {
      id: number,
      question_form_name: string
    },
    is_multiselect: boolean,
    gig_description: [
      {
        id: number,
        description: string,
        check: boolean
      }
    ]
    ansText: string,
    ansCheckBox: string,
    ansDropDown: string
    urls: any[]
  }]
}

interface gig_pricing_block {
  CheckBoxDetails: [{
    checkBoxName: string,
    checkBoxValue: boolean
  }],
}
