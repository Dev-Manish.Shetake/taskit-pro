import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  
  // name: string = 'My first AGM project';
  // lat: number = -31.197;
  // lng: number = 150.744;
  constructor() { }

  ngOnInit() {
    jQuery('body').scrollTop(0);
  }

}
