import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PageChangeEvent } from '@progress/kendo-angular-dropdowns/dist/es2015/common/page-change-event';
import { DataStateChangeEvent, GridDataResult } from '@progress/kendo-angular-grid';
import { SortDescriptor, State, process, orderBy, toODataString } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

declare var toastr: any;
@Component({
  selector: 'app-earnings',
  templateUrl: './earnings.component.html',
  styleUrls: ['./earnings.component.scss']
})
export class EarningsComponent implements OnInit {

  constructor(private httpServices: HttpRequestService, private router: Router, private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService, private dateFormatter: DateFormatterService) { }

  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public state: State = {
    skip: 0,
    take: 10
  }
  public withdrawal_gridData: GridDataResult | undefined;
  public withdrawalList: any;
  public incoming_WithdrawalList: any;
  public incomingWithdrawal_gridData: GridDataResult | undefined;
  public pageNumber: any;
  public search: any;
  public userAuthInfo: any;
  public tab_name: string;
  public paid_withdrawal: any;
  public total_withdrawal: any;
  public taskmer_wallet: any;
  public withdraw_Amt: any;
  public incomming_count: number = 0;
  public withdrawal_count: number = 0;
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.tab_name = "incoming_tab";
    this.getTab_Count();
    this.getIncoming_Withdrawals();    
    this.getOrderWallet_Summary();
  }

  getTab_Count() {
    this.progress.show();
    this.httpServices.request('get', 'orders_wallet/withdrawal-count', '', '', null).subscribe((data) => {
      this.incomming_count = data.total_incomming_count;
      this.withdrawal_count = data.total_withdrawal_count;
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  onTabChange(tab_name) {
    if (tab_name != undefined) {
      this.tab_name = tab_name;
    }
    if (tab_name == "incoming_tab") {
      this.getIncoming_Withdrawals();
    } else {
      this.getWithdrawals();
    }
  }

  //*********************************
  // ORDER WALLET COUNT.
  //*********************************
  getOrderWallet_Summary() {
    
    this.httpServices.request('get', 'orders_wallet/summary', '', '', null).subscribe((data) => {
      this.paid_withdrawal = data.paid_withdrawal;
      this.total_withdrawal = data.total_withdrawal;
      this.taskmer_wallet = data.taskmer_wallet;
    }, error => {
      console.log(error);
    });
  }

  //*********************************
  // INCOMING WITHDRAWAL PART.
  //*********************************
  //To populate WITHDRAWAL in Kendo Grid
  getIncoming_Withdrawals() {
    
    this.progress.show();
    this.httpServices.request("get", "orders_wallet/incoming?page=1&page_size=" + this.pageSize, null, null, null)
      .subscribe((data) => {
        this.incoming_WithdrawalList = data.results;
        this.loadIncoming_withdrawalData(data.count);
        this.manipulateStatus();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }


  private loadIncoming_withdrawalData(recordCount: number): void {
    if (this.incoming_WithdrawalList.length > 0) {
      this.incomingWithdrawal_gridData = {
        data: orderBy(this.incoming_WithdrawalList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.incomingWithdrawal_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortIncoming_withdrawal(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadIncoming_withdrawalData(this.totalRecordCount);
  }


  public incoming_withdrawalDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.incomingWithdrawal_gridData = process(this.incoming_WithdrawalList, this.state);
  }


  incoming_withdrawal_pageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "orders_wallet/incoming?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.search != null && this.search != undefined && this.search != '') {
      url = url + '&search=' + this.search
    }
    //var url = url = "/?page=" + this.pageNumber + "&page_size=" + this.pageSize;
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.incoming_WithdrawalList = data.results;
        this.loadIncoming_withdrawalData(data.count);
        this.manipulateStatus();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  //*****************
  //WITHDRAWAL PART.
  //*****************
  //To populate WITHDRAWAL in Kendo Grid
  getWithdrawals() {
    
    this.progress.show();
    this.httpServices.request("get", "orders_wallet/withdrawal?page=1&page_size=" + this.pageSize, null, null, null)
      .subscribe((data) => {
        this.withdrawalList = data.results;
        this.loadwithdrawalData(data.count);
        //this.manipulateStatus();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }
  
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }


  private loadwithdrawalData(recordCount: number): void {
    if (this.withdrawalList.length > 0) {
      this.withdrawal_gridData = {
        data: orderBy(this.withdrawalList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.withdrawal_gridData = undefined;
    }
  }

  // To handle sorting on KENDO COLUMNS 
  public sortwithdrawal(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadwithdrawalData(this.totalRecordCount);
  }


  public withdrawalDataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.withdrawal_gridData = process(this.withdrawalList, this.state);
  }

  withdrawalpageChange(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "orders_wallet/withdrawal?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    if (this.search != null && this.search != undefined && this.search != '') {
      url = url + '&transaction_id=' + this.search
    }
    this.httpServices.request("get", url, null, null, null)
      .subscribe((data) => {
        this.withdrawalList = data.results;
        this.loadwithdrawalData(data.count);
        this.manipulateStatus();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  // =====================================
  // On Buyer Name to buyer Profile 
  // ======================================
  OnbuyernameClick(buyerId) {

    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == buyerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.buyer_profile_id = buyerId
    this.encrypt_decrypt.buyer_profile_mode = profile_mode;
    this.router.navigate(['/buyer_profile'], {});
  }

  // ====================================
  // Manipulate status 
  // ===================================
  manipulateStatus() {
    if (this.incoming_WithdrawalList.length > 0) {
      for (let i = 0; i < this.incoming_WithdrawalList.length; i++) {
        if (this.incoming_WithdrawalList[i].order_status.id == 2) {
          this.incoming_WithdrawalList[i].property = "badge badge-success custom_badge_css";
        } else if (this.incoming_WithdrawalList[i].order_status.id == 1) {
          this.incoming_WithdrawalList[i].property = "badge badge-secondary custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 6) {
          this.incoming_WithdrawalList[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 7) {
          this.incoming_WithdrawalList[i].property = "badge badge-primary custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 8) {
          this.incoming_WithdrawalList[i].property = "badge badge-danger custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 3) {
          this.incoming_WithdrawalList[i].property = "badge badge-success custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 4) {
          this.incoming_WithdrawalList[i].property = "badge badge-warning custom_badge_css";
        }
        else if (this.incoming_WithdrawalList[i].order_status.id == 5) {
          this.incoming_WithdrawalList[i].property = "badge badge-success custom_badge_css";
        }
      }
    }
  }

  // ============================
  // Search Button
  // ============================
  // this.offerInfo.budget = this.offerInfo.budget.replace(".00", "");
  onSearchBtnClick() {
    debugger
    this.progress.show();
    var url = "orders_wallet/incoming?page=1&page_size=" + this.pageSize;
    if (this.tab_name == "incoming_tab") {
      //var url = "orders/?order_status_id=" + this.orderStatusId + "&page=1&page_size=" + this.pageSize;
      //var url = "orders_wallet/incoming?page=" + this.pageNumber + "&page_size=" + this.pageSize;
      if (this.search != null && this.search != '' && this.search != undefined) {
        url = url + "&search=" + this.search;
      }

      this.httpServices.request("get", url, "", "", null)
        .subscribe((data) => {
          this.incoming_WithdrawalList = data.results;
          this.loadIncoming_withdrawalData(data.count);
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    } else {
      var url = "orders_wallet/withdrawal?page=" + this.pageNumber + "&page_size=" + this.pageSize;
      if (this.search != null && this.search != '' && this.search != undefined) {
        url = url + "&transaction_id=" + this.search;
      }

      this.httpServices.request("get", url, "", "", null)
        .subscribe((data) => {
          this.withdrawalList = data.results;
          this.loadwithdrawalData(data.count);
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    }
  }

  // ============================
  // Add Withdraw Btn
  // ============================
  onAddWithdrawbtn() {
    
    let withdraw_amount: any = {
      amount: ""
    }
    if (this.withdraw_Amt != null && this.withdraw_Amt != undefined) {
      this.progress.show();
      withdraw_amount.amount = this.withdraw_Amt;
      this.httpServices.request('post', 'orders_wallet/withdraw-amount', '', '', withdraw_amount).subscribe((data) => {
        toastr.success("Amount Withdrawn Successfully");
        this.progress.show();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    } else {
      toastr.error("Please enter the amount to be withdrawn");
      this.progress.hide();
    }
  }
}
