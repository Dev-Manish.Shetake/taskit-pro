import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
declare var toastr: any;
@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.css']
})
export class FavoriteComponent implements OnInit {


  constructor(private encrypt_decrypt: EncryptDecryptService, private router: Router, private httpServices: HttpRequestService, private progress: NgxSpinnerService) { }

  public favorites_gigList: any;
  public suggestion_gigList: any;
  public subCategoryId: any;
  // public categoryList: any;
  public printStar: string = '';
  public userAuthInfo: any;
  public printStarEmpty: string = '';
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    // this.httpServices.request('get', 'categories/subcategory-list?is_active=true', null, null, null).subscribe((data) => {
    //   this.categoryList = data;
    //   this.subCategoryId = 1;
    //   // this.getBestSeller(this.subCategoryId);
    // });

    this.getGig_favorites();
    //this.get_BrowserHistory();
  }

  getGig_favorites() {
    this.progress.show();
    this.httpServices.request('get', 'home_screen/favourite-list', null, null, null).subscribe((data) => {
      this.favorites_gigList = data.results;
      //toastr.success("Saved Gigs Fetched Successfully");
      this.progress.hide();
    }, error => {
      console.log(error);
    });
  }

  // ================================
  // On click of particular gig 
  // =================================
  onGigClick(gigid: any, gig_status_id: any) {
    let passQueryParam = { id: gigid, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], {});
  }

  get_BrowserHistory() {

    this.httpServices.request('get', 'home_screen/browsing-history', null, null, null).subscribe((data) => {
      this.suggestion_gigList = data.results;
      //toastr.success("Suggestions Fetched Successfully");
    }, error => {
      console.log(error);
    });
  }

  // ==================================
  // Display rating 
  // ==================================
  getStar(value: any) {
    if (value == null) {
      value = 0;
    }
    if (value > 5) {
      value = 5;
    }
    let printStar = '';
    for (let i = 0; i < value; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStar = printStar;
  }
  getEmptyStar() {

    let printStar = '';
    for (let i = 0; i < 5; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStarEmpty = printStar;
  }
  // ============================
  // User Name to Seller Profile
  // ============================
  onSellerClick(sellerId) {
    // let passQueryParam = { userId: sellerId };
    // sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));

    let profile_mode: string;
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      } else {
        profile_mode = 'other';
      }

    } else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId;
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }

  // ================================
  // On click of Favourite gig 
  // =================================
  tagFavourite(event: any, flag: any) {
    this.progress.show();
    let gigDetails = { gig_id: event.id }
    this.httpServices.request('post', 'buyers/user-favourite-gigs?is_favourite=' + flag, null, null, gigDetails).subscribe((data) => {
      this.progress.hide();      
      this.favorites_gigList.splice(this.favorites_gigList.findIndex(x => x.id == event.id), 1);
      return event.is_favourite = flag;
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }

}


