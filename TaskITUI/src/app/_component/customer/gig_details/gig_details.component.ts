import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { EncryptDecryptService } from '../../../_common_services/encrypt-decrypt.service';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import { DateAgoPipe } from '../../../_pipes/date-ago.pipe';
import { Router } from '@angular/router';

declare var jQuery: any;
declare var toastr: any;
@Component({
  selector: 'app-gig_details',
  templateUrl: './gig_details.component.html',
  styleUrls: ['./gig_details.component.scss']
})
export class Gig_detailsComponent implements OnInit {

  constructor(private router: Router, private progress: NgxSpinnerService, private httpServices: HttpRequestService, private encrypt_decrypt: EncryptDecryptService) { }

  public gig_details: gig_details_byId = {
    id: 0,
    title: "",
    sub_category: {
      id: 0,
      sub_category_name: "",
      is_active: true
    },
    category: {
      id: 0,
      category_name: "",
      is_active: true
    },
    is_approved: false,
    approved_by: 0,
    approved_user: "",
    approved_date: "",
    is_price_package: false,
    description: "",
    gig_status: {
      id: 0,
      gig_status_name: ""
    },
    total_views: 0,
    rating: 0,
    gig_tags: [
      {
        id: 0,
        search_tags: ""
      }
    ],
    gallery: [
      {
        id: 0,
        file_path: "",
        file_path_thumbnail: "",
        gig_gallery_type: {
          gig_gallery_type_name: '',
          id: 0
        }
      }
    ],
    faq: [
      {
        id: 0,
        seq_no: 0,
        question: "",
        answer: ""
      }
    ],
    is_active: true,
    created_by: {
      id: 0,
      user_name: "",
      level: {
        id: 0,
        level_name: ""
      },
      rating: ""
    },
    order_rating: [
      {
        id: 0,
        seller_feedback: "",
        buyer_feedback: "",
        buyer_rating: 0,
        seller_rating: 0,
        created_user: "",
        created_date: ""
      }
    ],
    order_comment: [
      {
        id: 0,
        comment: "",
        created_user: ""
      }
    ],
    metadata: [
      {
        id: 0,
        metadata_type_dtl: {
          id: 0,
          metadata_type: {
            id: 0,
            metadata_type_name: "",
            is_single: true,
            is_mandatory: true,
            is_suggest_other: true,
            is_active: true,
            created_date: "",
            modified_date: ""
          },
          seq_no: 0,
          metadata_type_name: "",
          is_active: true
        }
      }
    ],
    about_seller: {
      description: "",
      first_name: "",
      id: 0,
      last_name: "",
      personal_website: "",
      profile_picture: ""
    },
    seller_info: "",
    created_date: "",
    last_delivery_date: ""
  }
  msaapDisplayTitle = false;
  msaapDisplayVolumeControls = false;
  msbapAudioUrl = "";
  msbapTitle = "";
  public queryParam: any;
  public _queryParams: any;
  public gig_id: any;
  public currentTime: any;
  public messageOnContactMe: any;
  public photoCopy: boolean = false;
  public labour_photo_data: any;
  public photoScan: any;
  public block = "block";
  public none = "none";
  public filename: string = "";
  public memberSinceMonth: string;
  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userName: any;
  public about_seller_One: any;
  public about_seller_Two: any;
  public showFullAboutSeller: boolean = false;
  public fiveStarCount: any;
  public fourStarCount: any;
  public threeStarCount: any;
  public twoStarCount: any;
  public oneStarCount: any;
  public averageRating: any;
  public avarageRatingRound: any;
  public gigPricingDetails: any;
  public printStar: string = '';
  public buyerProfilePic: any;
  public gig_status_id: any;
  public silverDeliveryCheckBox: boolean = true;
  public goldDeliveryCheckBox: boolean = true;
  public platiDeliveryCheckBox: boolean = true;
  public silverExtraFastCheckBox: boolean = false;
  public goldExtraFastCheckBox: boolean = false;
  public platiExtraFastCheckBox: boolean = false;
  public gig_pricing_block: gig_pricing_block = {
    silverPackageName: "",
    silverPackageDescription: "",
    silverDeliveryTime: "",
    silverRevisions: "",
    silverPrice: "",
    silverInitialConcepts: "",
    silverCheckBoxDetails: [{
      silverCheckBox: "",
      silverCheckBoxValue: false
    }],
    goldPackageName: "",
    goldPackageDescription: "",
    goldDeliveryTime: "",
    goldRevisions: "",
    goldPrice: "",
    goldInitialConcepts: "",
    goldCheckBoxDetails: [{
      goldCheckBox: "",
      goldCheckBoxValue: false
    }],
    platinumPackageName: "",
    platinumPackageDescription: "",
    platinumDeliveryTime: "",
    platinumRevisions: "",
    platinumPrice: "",
    platinumInitialConcepts: "",
    platinumCheckBoxDetails: [{
      platinumCheckBox: "",
      platinumCheckBoxValue: false
    }]
  }
  public groupId: any;
  public modification_required_comment: any;
  public check_modification_required_comment: boolean = false;
  public reject_comment: any;
  public check_reject_comment: boolean = false;

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    if (this.userAuthInfo.currentUser.user == undefined || this.userAuthInfo.currentUser.user == null) {
      this.groupId = 0;
      this.userName = "";
    }
    else {
      this.userName = this.userAuthInfo.currentUser.user.user_name;
      this.groupId = this.userAuthInfo.currentUser.user.group_id;
    }

    if (this.queryParam == null) {
      this._queryParams = (sessionStorage.getItem("gigs_details_queryParams") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      this.gig_id = this.queryParam.id;
      this.gig_status_id = this.queryParam.gig_status_id;

      if (this.groupId == 2) {
        this.gig_last_visited();
      }
      this.getGigDetails(this.gig_id, this.gig_status_id);
      // if( this.userInfo.user.group_id!=1)
      // {
      this.getGigPricingDetails(this.gig_id, this.gig_status_id);

      //}
    }

  }

  // ========================
  // View Attached Imgae 
  // =========================
  public image_ViewUrl: any;
  openImage(item: any) {
    this.image_ViewUrl = item;
    jQuery('#imagemodal').modal({ backdrop: 'static', keyboard: false });
  }

  // ===================================
  // Gig last visited 
  // ====================================
  gig_last_visited() {
    let gig_id = { gig_id: this.gig_id };
    this.httpServices.request('post', 'gigs/last-visited', null, null, gig_id).subscribe((data) => {
    }, (error) => {
      this.progress.hide();
    });
  }


  onDeliveryTimeCheckBoxClick() {
    if (this.silverDeliveryCheckBox == false) {
      this.silverExtraFastCheckBox = false;
    }
    if (this.goldDeliveryCheckBox == false) {
      this.goldExtraFastCheckBox = false;
    }
    if (this.platiDeliveryCheckBox == false) {
      this.platiExtraFastCheckBox = false;
    }
  }
  openPDF(file_path) {
    window.open(file_path);
  }
  // ==========================
  // Get Gig Details 
  // ==========================
  public metaDataArray: Array<{ id: number, metadatatype: string }> = [];
  public metaDataTypes: string;
  public metadataDisplay: Array<{ metadataTypeId: number, metadataTypeName: string, subTypeId: number, subTypeName: string }> = [];
  public statusClass: any;
  getGigDetails(gig_id: any, status_id: any) {
    this.progress.show();
    var url = "";
    if (status_id == 5 || status_id == 6) {
      url = "gigs_extra/gig-details/" + gig_id;
    }
    else {
      url = "gigs_extra/gig-draft-details/" + gig_id;
    }
    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      if (data != null && data != undefined && data != '') {
        this.gig_details = data;
        if (this.gig_details.order_comment != null && this.gig_details.order_comment != undefined) {
          this.gig_details.order_comment.splice(this.gig_details.order_comment.findIndex(x => x.id == 0), 1);
        }
        if (this.gig_details.created_date != null && this.gig_details.created_date != undefined) {
          const monthNames = ["Jan", "Feb", "March", "April", "May", "June",
            "July", "Aug", "Sept", "Oct", "Nov", "Dec"
          ];
          let newDate = new Date(this.gig_details.created_date);
          const d = newDate;
          this.memberSinceMonth = monthNames[d.getMonth() + 1] + " " + d.getFullYear();
          // this.contractDetails.requisition_name = dataitem.project.project_code + "/" + monthNames[d.getMonth()] + d.getFullYear() + "/" + dataitem.trade.trade_name;

        }
        if (this.gig_details.gig_status.id == 1) {
          this.statusClass = "btn btn-success btn-sm";
        }
        else if (this.gig_details.gig_status.id == 2) {
          this.statusClass = "";
        }
        else if (this.gig_details.gig_status.id == 3) {
          this.statusClass = "";
        }
        else if (this.gig_details.gig_status.id == 4) {
          this.statusClass = "";
        }
        else if (this.gig_details.gig_status.id == 5) {
          this.statusClass = "btn btn-success btn-sm";
        }
        else if (this.gig_details.gig_status.id == 6) {
          this.statusClass = "";
        }
        if (this.gig_details.about_seller != null && this.gig_details.about_seller != undefined) {
          if (this.gig_details.about_seller.description.length > 150) {
            this.about_seller_One = this.gig_details.about_seller.description.substring(0, 150);
            this.about_seller_Two = this.gig_details.about_seller.description.substring(150, this.gig_details.about_seller.description.length);
          }
          else {
            this.about_seller_One = this.gig_details.about_seller.description;
          }
        }

        this.gig_details.gallery.forEach(element => {
          if (element.gig_gallery_type.id == 4) {
            this.msbapAudioUrl = element.file_path;
          }
        });

        // this.gig_details.order_rating.push({ id: 1,
        //     seller_feedback: "ABC",
        //     buyer_feedback: "XYZ",
        //     buyer_rating: 2,
        //     seller_rating: 0,
        //     created_user: "",
        //     created_date: ""});
        // this.gig_details.order_rating.push({ id: 1,
        //     seller_feedback: "ABC",
        //     buyer_feedback: "XYZ",
        //     buyer_rating: 2,
        //     seller_rating: 0,
        //     created_user: "",
        //     created_date: ""});
        // this.gig_details.order_rating.push({ id: 1,
        //     seller_feedback: "ABC",
        //     buyer_feedback: "XYZ",
        //     buyer_rating: 4,
        //     seller_rating: 0,
        //     created_user: "",
        //     created_date: ""});
        // this.gig_details.order_rating.push({ id: 1,
        //     seller_feedback: "ABC",
        //     buyer_feedback: "XYZ",
        //     buyer_rating: 2,
        //     seller_rating: 0,
        //     created_user: "",
        //     created_date: ""});
        // this.gig_details.order_rating.push({ id: 1,
        //     seller_feedback: "ABC",
        //     buyer_feedback: "XYZ",
        //     buyer_rating: 5,
        //     seller_rating: 0,
        //     created_user: "",
        //     created_date: ""});
        // this.gig_details.order_rating.push({ id: 1,
        //     seller_feedback: "ABC",
        //     buyer_feedback: "XYZ",
        //     buyer_rating: 1,
        //     seller_rating: 0,
        //     created_user: "",
        //     created_date: ""});
        // this.gig_details.order_rating.push({ id: 1,
        //     seller_feedback: "ABC",
        //     buyer_feedback: "XYZ",
        //     buyer_rating: 3,
        //     seller_rating: 0,
        //     created_user: "",
        //     created_date: ""});
        if (this.gig_details.order_rating != null && this.gig_details.order_rating != undefined) {
          this.fiveStarCount = this.gig_details.order_rating.filter(x => x.seller_rating == 5);
          this.fourStarCount = this.gig_details.order_rating.filter(x => x.seller_rating == 4);
          this.threeStarCount = this.gig_details.order_rating.filter(x => x.seller_rating == 3);
          this.twoStarCount = this.gig_details.order_rating.filter(x => x.seller_rating == 2);
          this.oneStarCount = this.gig_details.order_rating.filter(x => x.seller_rating == 1);
          if (this.gig_details.order_rating.length > 0) {
            let sum: number = this.gig_details.order_rating.map(a => a.seller_rating).reduce(function (a, b) {
              return a + b;
            });
            this.averageRating = sum / this.gig_details.order_rating.length;
            this.avarageRatingRound = Math.round(this.averageRating);
            this.averageRating = this.averageRating.toFixed(2);
          }

        }

        if (this.gig_details.metadata != null && this.gig_details.metadata != undefined) {
          for (let i = 0; i < this.gig_details.metadata.length; i++) {
            if (i == 0) {
              this.metaDataArray.push({ id: this.gig_details.metadata[i].metadata_type_dtl.metadata_type.id, metadatatype: this.gig_details.metadata[i].metadata_type_dtl.metadata_type.metadata_type_name });
            }
            else {
              if (this.metaDataArray.filter(x => x.id == this.gig_details.metadata[i].metadata_type_dtl.metadata_type.id).length > 0) {

              }
              else {
                this.metaDataArray.push({ id: this.gig_details.metadata[i].metadata_type_dtl.metadata_type.id, metadatatype: this.gig_details.metadata[i].metadata_type_dtl.metadata_type.metadata_type_name });
              }
            }
          }
          if (this.metaDataArray != null && this.metaDataArray != undefined) {
            for (let k = 0; k < this.metaDataArray.length; k++) {

              let filterData = this.gig_details.metadata.filter(element => element.metadata_type_dtl.metadata_type.id == this.metaDataArray[k].id);
              let subTypeNameCommaSeperated = "";
              for (let l = 0; l < filterData.length; l++) {
                if (filterData.length > 1) {
                  if (l == filterData.length - 1) {
                    subTypeNameCommaSeperated = subTypeNameCommaSeperated + filterData[l].metadata_type_dtl.metadata_type_name;
                  }
                  else {
                    subTypeNameCommaSeperated = subTypeNameCommaSeperated + filterData[l].metadata_type_dtl.metadata_type_name + ", ";
                  }

                }
                else {
                  subTypeNameCommaSeperated = subTypeNameCommaSeperated + filterData[l].metadata_type_dtl.metadata_type_name;
                }
              }
              this.metadataDisplay.push({
                metadataTypeId: this.metaDataArray[k].id, metadataTypeName: this.metaDataArray[k].metadatatype,
                subTypeId: 0, subTypeName: subTypeNameCommaSeperated
              });
            }
          }
        }

        // this.getLoggedInUserDetails();
        //this.progress.hide();
      }
    }, (error) => {
      this.progress.hide();
    });
  }
  // ===================================
  // Get Logged iN User details 
  // ====================================
  getLoggedInUserDetails() {
    this.httpServices.request('get', 'user/me', null, null, null).subscribe((data) => {

      this.buyerProfilePic = data.profile_picture;
    }, (error) => {
      this.progress.hide();
    });
  }
  // ============================
  // get gig pricing details
  // ============================
  public gig_pricing: gig_pricing = {
    details: [{
      id: 0,
      gig: 0,
      gig_draft: 0,
      price_type: {
        id: 0,
        price_type_name: ""
      },
      price: "",
      price_details: [
        {
          id: 0,
          sub_category_price_scope: {
            id: 0,
            seq_no: 0,
            price_scope_name: "",
            price_scope_control_type: {
              id: 0,
              price_scope_control_type_name: ""
            },
            is_heading_visible: false,
            watermark: ""
          },
          sub_category_price_scope_dtl: {
            id: 0,
            seq_no: 0,
            value: ""
          },
          value: ""
        }
      ]
    }]
  }
  public gig_pricing_details: gig_pricing_details = {
    details: [{
      price_type_id: 0,
      price_type_name: "",
      silverprice: "",
      goldPrice: "",
      platinumPrice: "",
      sub_category_price_scope_id: 0,
      sub_category_price_scope_name: "",
      price_scope_control_type_id: 0,
      silverValue: "",
      goldValue: "",
      platinumValue: "",
      deliveryTimeExtraFastSilver: "",
      deliveryTimeExtraFastGold: "",
      deliveryTimeExtraFastPlatinum: "",
      deliveryTimePriceSilver: "",
      deliveryTimePriceGold: "",
      deliveryTimePricePlatinum: "",
    }]
  }
  public showgoloPlatinumBlocks: boolean = false;
  getGigPricingDetails(gig_id: any, status_id: any) {
    this.progress.show();
    var url = "";
    if (status_id == 5 || status_id == 6) {
      url = "gigs_extra/gig-pricing-details/" + gig_id;
    }
    else {
      url = "gigs_extra/gig-draft-pricing-details/" + gig_id;
    }
    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      // this.gigPricingDetails=data;
      if (data != null && data != undefined && data.length > 0) {
        this.gig_pricing.details = data;
        if (data.filter(element => element.price_type.id > 1).length > 0) {
          this.showgoloPlatinumBlocks = true;
        }
        this.gig_pricing_details.details.splice(this.gig_pricing_details.details.findIndex(element => element.price_type_id == 0), 1);
        let sub_category_price_scope_Ids: any = [];
        for (let i = 0; i < this.gig_pricing.details.length; i++) {
          this.gig_pricing.details[i].price_details.forEach(element => {
            if (sub_category_price_scope_Ids.filter(x => x.sub_category_price_scope_id == element.sub_category_price_scope.id).length > 0) {

            }
            else {
              sub_category_price_scope_Ids.push({ sub_category_price_scope_id: element.sub_category_price_scope.id });
            }

          });
          if (sub_category_price_scope_Ids.filter(element => element.sub_category_price_scope_id == 0).length > 0) {

          }
          else {
            sub_category_price_scope_Ids.push({ sub_category_price_scope_id: 0 });
          }

          //sub_category_price_scope_Ids.splice(sub_category_price_scope_Ids.findIndex(y => y.sub_category_price_scope_id == 0), 1);
          let dataTempDeliveryTime = [];
          for (let k = 0; k < sub_category_price_scope_Ids.length; k++) {
            // if( this.userInfo.user.group_id!=1)
            // {
            // if(sub_category_price_scope_Ids[k].sub_category_price_scope_id!=0)
            // {
            let dataTemp = this.gig_pricing.details[i].price_details.filter(a => a.sub_category_price_scope.id == sub_category_price_scope_Ids[k].sub_category_price_scope_id
              && a.sub_category_price_scope.price_scope_name != 'Delivery Time');
            // }
            // else{

            // }
            // if(this.gig_pricing_details.details.length<=1){
            if (i == 0) {
              if (dataTemp.length > 0) {
                this.gig_pricing_details.details.push({
                  price_type_id: this.gig_pricing.details[i].price_type.id,
                  price_type_name: this.gig_pricing.details[i].price_type.price_type_name,
                  silverprice: this.gig_pricing.details[i].price,
                  goldPrice: "",
                  platinumPrice: "",
                  sub_category_price_scope_id: dataTemp[0].sub_category_price_scope.id,
                  sub_category_price_scope_name: dataTemp[0].sub_category_price_scope.price_scope_name,
                  price_scope_control_type_id: dataTemp[0].sub_category_price_scope.price_scope_control_type.id,
                  silverValue: dataTemp[0].value == "" ? dataTemp[0].sub_category_price_scope_dtl.value : dataTemp[0].value, goldValue: "", platinumValue: "",
                  deliveryTimeExtraFastSilver: "",
                  deliveryTimeExtraFastGold: "",
                  deliveryTimeExtraFastPlatinum: "",
                  deliveryTimePriceSilver: "",
                  deliveryTimePriceGold: "",
                  deliveryTimePricePlatinum: ""
                })
              }

            }
            else {
              this.gig_pricing_details.details.forEach(element => {
                if (dataTemp.length > 0) {
                  if (element.sub_category_price_scope_id == dataTemp[0].sub_category_price_scope.id && this.gig_pricing.details[i].price_type.id == 2) {

                    element.goldValue = dataTemp[0].value == "" ? dataTemp[0].sub_category_price_scope_dtl.value : dataTemp[0].value;
                    element.goldPrice = this.gig_pricing.details[i].price;

                  }

                  else if (element.sub_category_price_scope_id == dataTemp[0].sub_category_price_scope.id && this.gig_pricing.details[i].price_type.id == 3) {

                    element.platinumValue = dataTemp[0].value == "" ? dataTemp[0].sub_category_price_scope_dtl.value : dataTemp[0].value;
                    element.platinumPrice = this.gig_pricing.details[i].price;

                  }
                }

              })
            }
            //}
          }

          for (let k = 0; k < sub_category_price_scope_Ids.length; k++) {
            // if(sub_category_price_scope_Ids[k].sub_category_price_scope_id!=0)
            // {
            dataTempDeliveryTime = this.gig_pricing.details[i].price_details.filter(a => a.sub_category_price_scope.id == sub_category_price_scope_Ids[k].sub_category_price_scope_id
              && a.sub_category_price_scope.price_scope_name == 'Delivery Time');
            if (dataTempDeliveryTime.length > 0) {
              if (i == 0) {
                this.gig_pricing_details.details.push({
                  price_type_id: this.gig_pricing.details[i].price_type.id,
                  price_type_name: this.gig_pricing.details[i].price_type.price_type_name,
                  silverprice: this.gig_pricing.details[i].price,
                  goldPrice: "",
                  platinumPrice: "",
                  sub_category_price_scope_id: dataTempDeliveryTime[0].sub_category_price_scope.id,
                  sub_category_price_scope_name: dataTempDeliveryTime[0].sub_category_price_scope.price_scope_name,
                  price_scope_control_type_id: dataTempDeliveryTime[0].sub_category_price_scope.price_scope_control_type.id,
                  silverValue: dataTempDeliveryTime[0].value == "" ? dataTempDeliveryTime[0].sub_category_price_scope_dtl.value : dataTempDeliveryTime[0].value, goldValue: "", platinumValue: "",
                  deliveryTimeExtraFastSilver: "",
                  deliveryTimeExtraFastGold: "",
                  deliveryTimeExtraFastPlatinum: "",
                  deliveryTimePriceSilver: "",
                  deliveryTimePriceGold: "",
                  deliveryTimePricePlatinum: ""
                })
              }
              else {
                this.gig_pricing_details.details.forEach(element => {
                  if (element.sub_category_price_scope_id == dataTempDeliveryTime[0].sub_category_price_scope.id && this.gig_pricing.details[i].price_type.id == 2) {

                    element.goldValue = dataTempDeliveryTime[0].value == "" ? dataTempDeliveryTime[0].sub_category_price_scope_dtl.value : dataTempDeliveryTime[0].value;
                    element.goldPrice = this.gig_pricing.details[i].price;

                  }

                  else if (element.sub_category_price_scope_id == dataTempDeliveryTime[0].sub_category_price_scope.id && this.gig_pricing.details[i].price_type.id == 3) {

                    element.platinumValue = dataTempDeliveryTime[0].value == "" ? dataTempDeliveryTime[0].sub_category_price_scope_dtl.value : dataTempDeliveryTime[0].value;
                    element.platinumPrice = this.gig_pricing.details[i].price;

                  }
                });
              }
              break;
            }
            // }
          }
          for (let k = 0; k < sub_category_price_scope_Ids.length; k++) {
            // dataTempDeliveryTime = this.gig_pricing.details[i].price_details.filter(a => a.sub_category_price_scope.id == sub_category_price_scope_Ids[k].sub_category_price_scope_id
            //   && a.sub_category_price_scope.price_scope_name == 'Delivery Time');
            if (sub_category_price_scope_Ids[k].sub_category_price_scope_id == 0) {
              if (i == 0) {
                this.gig_pricing_details.details.push({
                  price_type_id: this.gig_pricing.details[i].price_type.id,
                  price_type_name: this.gig_pricing.details[i].price_type.price_type_name,
                  silverprice: "",
                  goldPrice: "",
                  platinumPrice: "",
                  sub_category_price_scope_id: 0,
                  sub_category_price_scope_name: "Extra Fast Delivery",
                  price_scope_control_type_id: 0,
                  silverValue: "",
                  goldValue: "",
                  platinumValue: "",
                  deliveryTimeExtraFastSilver: "",
                  deliveryTimeExtraFastGold: "",
                  deliveryTimeExtraFastPlatinum: "",
                  deliveryTimePriceSilver: "",
                  deliveryTimePriceGold: "",
                  deliveryTimePricePlatinum: ""
                })
              }
              else {
                this.gig_pricing_details.details.forEach(element => {
                  if (element.sub_category_price_scope_name == "Extra Fast Delivery") {
                    element.goldValue = ""
                    element.goldPrice = "";
                    element.platinumValue = ""
                    element.platinumPrice = "";
                  }
                });
              }
            }
          }
        }
        let dataSilverPriceBlock = data.filter(element => element.price_type.id == 1);
        let dataGoldPriceBlock = data.filter(element => element.price_type.id == 2);
        let dataPlatinumPriceBlock = data.filter(element => element.price_type.id == 3);
        if (dataSilverPriceBlock.length > 0) {
          this.gig_pricing_block.silverPrice = dataSilverPriceBlock[0].price;
          dataSilverPriceBlock[0].price_details.forEach(element => {
            if (element.sub_category_price_scope.price_scope_name == "Package Name") {
              this.gig_pricing_block.silverPackageName = element.value;
            }
            else if (element.sub_category_price_scope.price_scope_name == "Package Description") {
              this.gig_pricing_block.silverPackageDescription = element.value;
            }
            else {
              if (element.sub_category_price_scope.price_scope_name == "Price") {

              }
              else if (element.sub_category_price_scope.price_scope_name == "Delivery Time") {
                this.gig_pricing_block.silverDeliveryTime = element.sub_category_price_scope_dtl.display_value;
              }
              else {
                if (element.sub_category_price_scope_dtl == null || element.sub_category_price_scope_dtl == undefined) {
                  this.gig_pricing_block.silverCheckBoxDetails.push({ silverCheckBox: element.sub_category_price_scope.price_scope_name, silverCheckBoxValue: element.value });
                }
                else {
                  this.gig_pricing_block.silverCheckBoxDetails.push({
                    silverCheckBox: element.sub_category_price_scope_dtl.display_value, silverCheckBoxValue: (element.sub_category_price_scope_dtl.display_value == "" ||
                      element.sub_category_price_scope_dtl.display_value == null || element.sub_category_price_scope_dtl.display_value == undefined) ? false : true
                  });
                }
              }

            }

          });
        }
        this.gig_pricing_block.silverCheckBoxDetails.splice(this.gig_pricing_block.silverCheckBoxDetails.findIndex(x => x.silverCheckBox == ""), 1);
        if (dataGoldPriceBlock.length > 0) {
          this.gig_pricing_block.goldPrice = dataGoldPriceBlock[0].price;
          dataGoldPriceBlock[0].price_details.forEach(element => {
            if (element.sub_category_price_scope.price_scope_name == "Package Name") {
              this.gig_pricing_block.goldPackageName = element.value;
            }
            else if (element.sub_category_price_scope.price_scope_name == "Package Description") {
              this.gig_pricing_block.goldPackageDescription = element.value;
            }
            else {
              if (element.sub_category_price_scope.price_scope_name == "Price") {

              }
              else if (element.sub_category_price_scope.price_scope_name == "Delivery Time") {
                this.gig_pricing_block.goldDeliveryTime = element.sub_category_price_scope_dtl.display_value;
              }
              else {
                if (element.sub_category_price_scope_dtl == null || element.sub_category_price_scope_dtl == undefined) {
                  this.gig_pricing_block.goldCheckBoxDetails.push({ goldCheckBox: element.sub_category_price_scope.price_scope_name, goldCheckBoxValue: element.value });
                }
                else {
                  this.gig_pricing_block.goldCheckBoxDetails.push({
                    goldCheckBox: element.sub_category_price_scope_dtl.display_value, goldCheckBoxValue: (element.sub_category_price_scope_dtl.display_value == "" ||
                      element.sub_category_price_scope_dtl.display_value == null || element.sub_category_price_scope_dtl.display_value == undefined) ? false : true
                  });
                }
              }
            }
          });
        }
        this.gig_pricing_block.goldCheckBoxDetails.splice(this.gig_pricing_block.goldCheckBoxDetails.findIndex(x => x.goldCheckBox == ""), 1);
        if (dataPlatinumPriceBlock.length > 0) {
          this.gig_pricing_block.platinumPrice = dataPlatinumPriceBlock[0].price;
          dataPlatinumPriceBlock[0].price_details.forEach(element => {
            if (element.sub_category_price_scope.price_scope_name == "Package Name") {
              this.gig_pricing_block.platinumPackageName = element.value;
            }
            else if (element.sub_category_price_scope.price_scope_name == "Package Description") {
              this.gig_pricing_block.platinumPackageDescription = element.value;
            }
            else {
              if (element.sub_category_price_scope.price_scope_name == "Price") {

              }
              else if (element.sub_category_price_scope.price_scope_name == "Delivery Time") {
                this.gig_pricing_block.platinumDeliveryTime = element.sub_category_price_scope_dtl.display_value;
              }
              else {
                if (element.sub_category_price_scope_dtl == null || element.sub_category_price_scope_dtl == undefined) {
                  this.gig_pricing_block.platinumCheckBoxDetails.push({ platinumCheckBox: element.sub_category_price_scope.price_scope_name, platinumCheckBoxValue: element.value });
                }
                else {
                  this.gig_pricing_block.platinumCheckBoxDetails.push({
                    platinumCheckBox: element.sub_category_price_scope_dtl.display_value, platinumCheckBoxValue: (element.sub_category_price_scope_dtl.display_value == "" ||
                      element.sub_category_price_scope_dtl.display_value == null || element.sub_category_price_scope_dtl.display_value == undefined) ? false : true
                  });
                }
              }
            }
          });
        }
        this.gig_pricing_block.platinumCheckBoxDetails.splice(this.gig_pricing_block.platinumCheckBoxDetails.findIndex(x => x.platinumCheckBox == ""), 1);
      }
      this.getExtraServicesdetails(this.gig_id, this.gig_status_id);
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }
  // ==================================================
  // get extra services details 
  // ===================================================
  public extra_service: extra_services = {
    extra_service: [{
      extra_service_name: "",
      extra_service_days: "",
      extra_service_price: ""
    }]
  }
  getExtraServicesdetails(gig_id: any, status_id: any) {
    var url = "";
    var url_custom = "";
    if (status_id == 5 || status_id == 6) {
      url = "gigs/pricing-extra-services/" + gig_id;
      url_custom = "gigs/pricing-extra-custom-services/" + gig_id;
    }
    else {
      url = "gigs/draft-pricing-extra-services/" + gig_id;
      url_custom = "gigs/draft-pricing-extra-custom-services/" + gig_id;
    }
    this.progress.show();
    this.extra_service.extra_service.splice(0, 1);
    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      data.forEach(element => {
        if (element.price_extra_service.title != 'Extra fast delivery') {
          this.extra_service.extra_service.push({
            extra_service_name: element.price_extra_service.title, extra_service_days: element.extra_days.extra_days_name,
            extra_service_price: element.extra_price
          })
        }
        if (element.price_extra_service.title == 'Extra fast delivery') {
          this.gig_pricing_details.details.forEach(y => {
            if (y.sub_category_price_scope_name == 'Extra Fast Delivery' && element.price_type.id == 1) {
              y.deliveryTimeExtraFastSilver = element.extra_days.extra_days_name;
              y.deliveryTimePriceSilver = element.extra_price;
            }
            else if (y.sub_category_price_scope_name == 'Extra Fast Delivery' && element.price_type.id == 2) {
              y.deliveryTimeExtraFastGold = element.extra_days.extra_days_name;
              y.deliveryTimePriceGold = element.extra_price;
            }
            else if (y.sub_category_price_scope_name == 'Extra Fast Delivery' && element.price_type.id == 3) {
              y.deliveryTimeExtraFastPlatinum = element.extra_days.extra_days_name;
              y.deliveryTimePricePlatinum = element.extra_price;
            }
          })
        }

      });
      for (let i = 0; i < this.gig_pricing_details.details.length; i++) {
        if (this.gig_pricing_details.details[i].sub_category_price_scope_name == "Extra Fast Delivery") {
          if (this.gig_pricing_details.details[i].deliveryTimeExtraFastSilver == "" || this.gig_pricing_details.details[i].deliveryTimeExtraFastSilver == null ||
            this.gig_pricing_details.details[i].deliveryTimeExtraFastSilver == undefined) {
            this.gig_pricing_details.details.splice(this.gig_pricing_details.details.findIndex(element => element.sub_category_price_scope_name == "Extra Fast Delivery"), 1);
            break;
          }
        }
      }
      this.httpServices.request('get', url_custom, null, null, null).subscribe((response) => {
        response.forEach(element => {
          this.extra_service.extra_service.push({
            extra_service_name: element.title, extra_service_days: element.extra_days,
            extra_service_price: element.extra_price
          })
        });
      }, (error) => {
        this.progress.hide();
      });
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });

  }
  // ==============================================
  // on Apply btn click in compare packages 
  // ==============================================
  public packageValue: string = "silver";
  public platinumRadio: any;
  public silverRadio: any = "silver";
  public goldRadio: any;
  public tempSilverPrice: any;
  public tempSilverDeliveryTime: any;
  public tempGoldPrice: any;
  public tempGoldDeliveryTime: any;
  public tempPlatinumPrice: any;
  public tempPlatinumDeliveryTime: any;
  public extrafastAdded: boolean = false;
  public extraFastAddedSilver: boolean = false;
  public extraFastAddedGold: boolean = false;
  public extraFastAddedPlatinum: boolean = false
  public packageSelectedValue: string = 'silver';
  public lblactive_class_silver_header: string = 'active_package_header';
  public lblactive_class_silver_body: string = 'active_package_body';
  public lblactive_class_gold_header: string = '';
  public lblactive_class_gold_body: string = '';
  public lblactive_class_platinum_header: string = '';
  public lblactive_class_platinum_body: string = '';

  package_header_click(event: any) {
    if (event == 'silver') {
      this.lblactive_class_silver_header = 'active_package_header';
      this.lblactive_class_silver_body = 'active_package_body';
      this.lblactive_class_gold_header = '';
      this.lblactive_class_gold_body = '';
      this.lblactive_class_platinum_header = '';
      this.lblactive_class_platinum_body = '';
    }
    else if (event == 'gold') {
      this.lblactive_class_silver_header = '';
      this.lblactive_class_silver_body = '';
      this.lblactive_class_gold_header = 'active_package_header';
      this.lblactive_class_gold_body = 'active_package_body';
      this.lblactive_class_platinum_header = '';
      this.lblactive_class_platinum_body = '';
    }
    else if (event == 'platinum') {
      this.lblactive_class_silver_header = '';
      this.lblactive_class_silver_body = '';
      this.lblactive_class_gold_header = '';
      this.lblactive_class_gold_body = '';
      this.lblactive_class_platinum_header = 'active_package_header';
      this.lblactive_class_platinum_body = 'active_package_body';
    }
  }

  onApplyClick(packagevalue: string, extraFastPrice: any, extraFastDeliveryTime: any) {
    this.packageValue = packagevalue;
    if (this.packageValue == "platinum") {
      if (this.silverRadio == "platinum") {
        if (this.tempPlatinumPrice > 0) {
          this.gig_pricing_block.platinumPrice = this.tempPlatinumPrice;
        }
        else {

        }
        if (this.tempPlatinumDeliveryTime > 0) {
          if (this.tempPlatinumDeliveryTime > 1) {
            this.gig_pricing_block.platinumDeliveryTime = this.tempPlatinumDeliveryTime + " Days Delivery";
          }
          else {
            this.gig_pricing_block.platinumDeliveryTime = this.tempPlatinumDeliveryTime + " Day Delivery";
          }
        }
        this.extrafastAdded = false;
        this.extraFastAddedPlatinum = false
      }
      else if (this.silverRadio == "platinumExtra") {
        if (this.extraFastAddedPlatinum == false) {
          this.tempPlatinumPrice = this.gig_pricing_block.platinumPrice;
          this.tempPlatinumDeliveryTime = this.gig_pricing_block.platinumDeliveryTime.replace(' Days Delivery', '').replace(' Day Delivery', '');
          this.gig_pricing_block.platinumPrice = (Number(this.gig_pricing_block.platinumPrice) + Number(extraFastPrice)).toString();
          // this.gig_pricing_block.platinumDeliveryTime=(Number(this.gig_pricing_block.platinumDeliveryTime.replace(' Days Delivery','').replace(' Day Delivery',''))
          // +Number(extraFastDeliveryTime.replace(' DAYS','').replace(' DAY',''))).toString();
          this.gig_pricing_block.platinumDeliveryTime = Number(extraFastDeliveryTime.replace(' DAYS', '').replace(' DAY', '')).toString();
          if (Number(this.gig_pricing_block.platinumDeliveryTime) > 1) {
            this.gig_pricing_block.platinumDeliveryTime = this.gig_pricing_block.platinumDeliveryTime + " Days Delivery";
          }
          else {
            this.gig_pricing_block.platinumDeliveryTime = this.gig_pricing_block.platinumDeliveryTime + " Day Delivery";
          }
          this.extrafastAdded = true;
          this.extraFastAddedPlatinum = true;
        }
      }
      this.lblactive_class_silver_header = '';
      this.lblactive_class_silver_body = '';
      this.lblactive_class_gold_header = '';
      this.lblactive_class_gold_body = '';
      this.lblactive_class_platinum_header = 'active_package_header';
      this.lblactive_class_platinum_body = 'active_package_body';
    }
    else if (this.packageValue == "gold") {
      if (this.silverRadio == "gold") {
        if (this.tempGoldPrice > 0) {
          this.gig_pricing_block.goldPrice = this.tempGoldPrice;
        }
        else {

        }
        if (this.tempGoldDeliveryTime > 0) {
          if (this.tempGoldDeliveryTime > 1) {
            this.gig_pricing_block.goldDeliveryTime = this.tempGoldDeliveryTime + " Days Delivery";
          }
          else {
            this.gig_pricing_block.goldDeliveryTime = this.tempGoldDeliveryTime + " Day Delivery";
          }
        }
        this.extrafastAdded = false;
        this.extraFastAddedGold = false;
      }
      else if (this.silverRadio == "goldExtra") {
        if (this.extraFastAddedGold == false) {
          this.tempGoldPrice = this.gig_pricing_block.goldPrice;
          this.tempGoldDeliveryTime = this.gig_pricing_block.goldDeliveryTime.replace(' Days Delivery', '').replace(' Day Delivery', '');
          this.gig_pricing_block.goldPrice = (Number(this.gig_pricing_block.goldPrice) + Number(extraFastPrice)).toString();
          this.gig_pricing_block.goldDeliveryTime = Number(extraFastDeliveryTime.replace(' DAYS', '').replace(' DAY', '')).toString();
          if (Number(this.gig_pricing_block.goldDeliveryTime) > 1) {
            this.gig_pricing_block.goldDeliveryTime = this.gig_pricing_block.goldDeliveryTime + " Days Delivery";
          }
          else {
            this.gig_pricing_block.goldDeliveryTime = this.gig_pricing_block.goldDeliveryTime + " Day Delivery";
          }
          this.extrafastAdded = true;
          this.extraFastAddedGold = true;
        }
      }
      this.lblactive_class_silver_header = '';
      this.lblactive_class_silver_body = '';
      this.lblactive_class_gold_header = 'active_package_header';
      this.lblactive_class_gold_body = 'active_package_body';
      this.lblactive_class_platinum_header = '';
      this.lblactive_class_platinum_body = '';
    }
    else if (this.packageValue == "silver") {
      if (this.silverRadio == "silver") {
        if (this.tempSilverPrice > 0) {
          this.gig_pricing_block.silverPrice = this.tempSilverPrice;
        }
        else {

        }
        if (this.tempSilverDeliveryTime > 0) {
          if (this.tempSilverDeliveryTime > 1) {
            this.gig_pricing_block.silverDeliveryTime = this.tempSilverDeliveryTime + " Days Delivery";
          }
          else {
            this.gig_pricing_block.silverDeliveryTime = this.tempSilverDeliveryTime + " Day Delivery";
          }
        }
        this.extraFastAddedSilver = false;
        this.extrafastAdded = false;
      }
      else if (this.silverRadio == "silverExtra") {
        if (this.extraFastAddedSilver == false) {
          this.tempSilverPrice = this.gig_pricing_block.silverPrice;
          this.tempSilverDeliveryTime = this.gig_pricing_block.silverDeliveryTime.replace(' Days Delivery', '').replace(' Day Delivery', '');
          this.gig_pricing_block.silverPrice = (Number(this.gig_pricing_block.silverPrice) + Number(extraFastPrice)).toString();
          this.gig_pricing_block.silverDeliveryTime = Number(extraFastDeliveryTime.replace(' DAYS', '').replace(' DAY', '')).toString();
          if (Number(this.gig_pricing_block.silverDeliveryTime) > 1) {
            this.gig_pricing_block.silverDeliveryTime = this.gig_pricing_block.silverDeliveryTime + " Days Delivery";
          }
          else {
            this.gig_pricing_block.silverDeliveryTime = this.gig_pricing_block.silverDeliveryTime + " Day Delivery";
          }
          this.extraFastAddedSilver = true;
          this.extrafastAdded = true;
        }
      }
      this.lblactive_class_silver_header = 'active_package_header';
      this.lblactive_class_silver_body = 'active_package_body';
      this.lblactive_class_gold_header = '';
      this.lblactive_class_gold_body = '';
      this.lblactive_class_platinum_header = '';
      this.lblactive_class_platinum_body = '';
    }
    jQuery("#compare_packages").modal("hide");
  }
  onContinueClick(packagevalue: string, price_type_id: any) {
    if (packagevalue != "" && packagevalue != null && packagevalue != undefined) {
      // alert("Hello Pradnya");
      let passQueryParam = { id: this.gig_id, gig_status_id: this.gig_status_id, price_type_id: price_type_id, extrafastAdded: this.extrafastAdded };
      sessionStorage.setItem("queryParamsCart", JSON.stringify(passQueryParam));
      this.router.navigate(['/cart'], { /*queryParams: passQueryParam /*, skipLocationChange: true*/ });
    }
  }
  //==========================================================
  // get star event - to display rating
  //==========================================================
  getStar(value: any) {

    if (value == null) {
      value = 0;
    }
    if (value > 5) {
      value = 5;
    }
    let printStar = '';
    for (let i = 0; i < value; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');

    }
    this.printStar = printStar;
    //this.printStar = Array(value).fill('whatever');
    //this.printStar = Array(value).fill('rating_stars');
  }
  // ==================================
  // Display rating 
  // ==================================
  public printStarEmpty: string = '';
  getEmptyStar() {

    let printStar = '';
    for (let i = 0; i < 5; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStarEmpty = printStar;
  }
  // ===================================
  // On Click of See More 
  // ===================================
  onSeeMoreClick() {
    this.showFullAboutSeller = true;
  }
  // ===============================
  // On Click of Contact Me 
  // ===============================
  onContactMeClick() {
    this.messageOnContactMe = "";
    this.filename = undefined;
    var today = new Date();
    this.currentTime = today.getHours() + ":" + today.getMinutes();
    jQuery('#ContactMe_POP').modal({ backdrop: 'static', keyboard: false });
  }
  // =====================================
  // on file choose of contact me popup 
  // =====================================
  fileChangedPhoto(event: any) {
    //to make sure the user select file/files

    if (event.target.files.length == 0) {
      this.photoCopy = false;
      this.labour_photo_data = undefined;
      this.photoScan = this.block;
      return;
    }
    this.photoScan = this.none;
    //To obtaine a File reference

    var files = event.target.files;

    var fileReader = new FileReader();

    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    // if (files[0].type == "image/jpeg" || files[0].type == "image/png" || files[0].type == "image/jpg" || files[0].type == "image/bmp" || files[0].type == "image/gif") {

    //   fileType = "";
    // }
    // else {

    //   fileType = ("<li>" + this.translate.instant('toastrMsgs.thefile') + files[0].name + this.translate.instant('toastrMsgs.doesnotmatchfiletype') + "</li>");
    // }


    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "The file: (" + files[0].name + ") does not match the upload conditions, Your file size is:" + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + " MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {
      this.labour_photo_data = event.target.files;
      if (this.labour_photo_data.fileName != "" || this.labour_photo_data.fileName != undefined) {
        formData.append("labour_photo_data", this.labour_photo_data[0]);
        this.filename = files[0].name;
        this.photoCopy = true;
        this.photoScan = this.none;
      }
    }
    else {
      this.resetFileInput(this.file_image);
      this.labour_photo_data = "";
      this.labour_photo_data = undefined;
      this.photoCopy = false;
      this.photoScan = this.block;


    }

  }
  @ViewChild('labour_photo_data', { read: ElementRef }) file_image: ElementRef;
  resetFileInput(file: any) {
    if (file)
      file.nativeElement.value = "";
  }
  // ===========================================
  // On Cancel button click of Contact Me pop up 
  // ============================================
  onCancelBtnClick() {
    jQuery("#ContactMe_POP").modal("hide");
  }
  // ====================================
  // on Approve btn click 
  // ====================================
  onApproveClick() {
    this.progress.show();
    this.httpServices.request('patch', 'gigs/approve/' + this.gig_id, null, null, null).subscribe((data) => {
      toastr.success("This gig has been approved successfully");
      this.router.navigate(['gig_list_admin'], {});
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }
  // =======================================
  // On requires ModificationClick 
  // =======================================
  onRequireModificationClick() {
    this.check_modification_required_comment = false;
    this.modification_required_comment = '';
    jQuery('#modification_required_comment').modal({ backdrop: 'static', keyboard: false });
  }

  // =======================================
  // on Rejected click 
  // ======================================
  onRejectedClick() {
    this.check_reject_comment = false;
    this.reject_comment = '';
    jQuery('#reject_comment').modal({ backdrop: 'static', keyboard: false });
  }


  //========================================
  // Modification Required Submit
  //========================================
  onClickModificationRequiredSubmit() {
    this.check_modification_required_comment = true;
    if (this.modification_required_comment != null && this.modification_required_comment != undefined && this.modification_required_comment.toString().trim() != "") {
      let modification_required_body: any = {
        modification_required_comment: ""
      }
      modification_required_body.modification_required_comment = this.modification_required_comment;
      this.progress.show();
      this.httpServices.request('patch', 'gigs/modify/' + this.gig_id, null, null, modification_required_body).subscribe((data) => {
        toastr.success("This gig has been submitted for modified required.");
        jQuery('#modification_required_comment').modal("hide");
        this.router.navigate(['gig_list_admin'], {});
        this.progress.hide();

      }, (error) => {
        this.progress.hide();
      });
    }
  }

  //========================================
  // Reject Submit
  //========================================
  onClickRejectCommentSubmit() {
    this.check_reject_comment = true;
    if (this.reject_comment != null && this.reject_comment != undefined && this.reject_comment.toString().trim() != "") {
      let reject_comment_body: any = {
        reject_comment: ""
      }
      reject_comment_body.reject_comment = this.reject_comment;
      this.progress.show();
      this.httpServices.request('patch', 'gigs/reject/' + this.gig_id, null, null, reject_comment_body).subscribe((data) => {
        toastr.success("This gig has been rejected successfully.");
        jQuery('#reject_comment').modal("hide");
        this.router.navigate(['gig_list_admin'], {});
        this.progress.hide();

      }, (error) => {
        this.progress.hide();
      });
    }
  }

  // ==============================
  // on paused click 
  // ===============================
  onPausedClick() {
    this.progress.show();
    this.httpServices.request('patch', 'gigs/paused/' + this.gig_id, null, null, null).subscribe((data) => {
      this.progress.hide();
      toastr.success("This gig has been paused successfully");
      if (this.userAuthInfo.currentUser.user.group_id == 1) {
        this.router.navigate(['gig_list_admin'], {});
      }
      else if (this.userInfo.user.group_id == 3) {
        this.router.navigate(['gig_list'], {});
      }
    }, (error) => {
      this.progress.hide();
    });
  }
  // ===============================
  // on Resume click
  // =================================
  onResumeClick() {
    this.progress.show();
    this.httpServices.request('patch', 'gigs/resume/' + this.gig_id, null, null, null).subscribe((data) => {
      this.progress.hide();
      toastr.success("This gig has been resumed successfully");
      if (this.userAuthInfo.currentUser.user.group_id == 1) {
        this.router.navigate(['gig_list_admin'], {});
      }
      else if (this.userInfo.user.group_id == 3) {
        this.router.navigate(['gig_list'], {});
      }
    }, (error) => {
      this.progress.hide();
    });
  }

  // ============================================
  // on Seller Name click to seller profile
  // ============================================
  onSellernameClick(sellerId) {
    let profile_mode: string;
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      } else {
        profile_mode = 'other';
      }

    } else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId;
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }



}

interface gig_details_byId {
  id: number,
  title: string,
  sub_category: {
    id: number,
    sub_category_name: string,
    is_active: boolean
  },
  category: {
    id: number,
    category_name: string,
    is_active: boolean
  },
  is_approved: boolean,
  approved_by: number,
  approved_user: string,
  approved_date: string,
  is_price_package: boolean,
  description: string,
  gig_status: {
    id: number,
    gig_status_name: string
  },
  total_views: number,
  rating: number,
  gig_tags: [
    {
      id: number,
      search_tags: string
    }
  ],
  gallery: [
    {
      id: number,
      file_path: string,
      file_path_thumbnail: string,
      gig_gallery_type: {
        gig_gallery_type_name: string,
        id: number
      }
    }
  ],
  faq: [
    {
      id: number,
      seq_no: number,
      question: string,
      answer: string
    }
  ],
  is_active: boolean,
  created_by: {
    id: number,
    user_name: string,
    level: {
      id: number,
      level_name: string
    },
    rating: string
  },
  order_rating: [
    {
      id: number,
      seller_feedback: string,
      buyer_feedback: string,
      buyer_rating: number,
      seller_rating: number,
      created_user: string,
      created_date: string
    }
  ],
  order_comment: [
    {
      id: number,
      comment: string,
      created_user: string
    }
  ],
  metadata: [
    {
      id: number,
      metadata_type_dtl: {
        id: number,
        metadata_type: {
          id: number,
          metadata_type_name: string,
          is_single: boolean,
          is_mandatory: boolean,
          is_suggest_other: boolean,
          is_active: boolean,
          created_date: string,
          modified_date: string
        },
        seq_no: number,
        metadata_type_name: string,
        is_active: boolean
      }
    }
  ],
  about_seller: {
    description: string,
    first_name: string,
    id: number,
    last_name: string,
    personal_website: string,
    profile_picture: string
  },
  seller_info: string,
  created_date: string,
  last_delivery_date: string
}
interface gig_pricing {
  details: [{
    id: number,
    gig: number,
    gig_draft: number,
    price_type: {
      id: number,
      price_type_name: string
    },
    price: string,
    price_details: [
      {
        id: number,
        sub_category_price_scope: {
          id: number,
          seq_no: number,
          price_scope_name: string,
          price_scope_control_type: {
            id: number,
            price_scope_control_type_name: string
          },
          is_heading_visible: boolean,
          watermark: string
        },
        sub_category_price_scope_dtl: {
          id: number,
          seq_no: number,
          value: string
        },
        value: string
      }
    ]
  }]
}
interface gig_pricing_details {
  details: [{
    price_type_id: number,
    price_type_name: string,
    silverprice: string,
    goldPrice: string,
    platinumPrice: string,
    sub_category_price_scope_id: number,
    sub_category_price_scope_name: string,
    price_scope_control_type_id: number,
    silverValue: any,
    goldValue: any,
    platinumValue: any,
    deliveryTimeExtraFastSilver: any,
    deliveryTimeExtraFastGold: any,
    deliveryTimeExtraFastPlatinum: any,
    deliveryTimePriceSilver: any,
    deliveryTimePriceGold: any,
    deliveryTimePricePlatinum: any
  }]
}
interface gig_pricing_block {
  silverPackageName: string,
  silverPackageDescription: string,
  silverDeliveryTime: string,
  silverRevisions: string,
  silverPrice: string,
  silverInitialConcepts: string,
  silverCheckBoxDetails: [{
    silverCheckBox: string,
    silverCheckBoxValue: boolean
  }],
  goldPackageName: string,
  goldPackageDescription: string,
  goldDeliveryTime: string,
  goldRevisions: string,
  goldPrice: string,
  goldInitialConcepts: string,
  goldCheckBoxDetails: [{
    goldCheckBox: string,
    goldCheckBoxValue: boolean
  }],
  platinumPackageName: string,
  platinumPackageDescription: string,
  platinumDeliveryTime: string,
  platinumRevisions: string,
  platinumPrice: string,
  platinumInitialConcepts: string,
  platinumCheckBoxDetails: [{
    platinumCheckBox: string,
    platinumCheckBoxValue: boolean
  }]
}
interface extra_services {
  extra_service: [{
    extra_service_name: string,
    extra_service_days: string,
    extra_service_price: string
  }]
}