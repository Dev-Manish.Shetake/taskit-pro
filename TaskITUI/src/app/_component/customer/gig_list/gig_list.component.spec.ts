/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Gig_listComponent } from './gig_list.component';

describe('Gig_listComponent', () => {
  let component: Gig_listComponent;
  let fixture: ComponentFixture<Gig_listComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gig_listComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gig_listComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
