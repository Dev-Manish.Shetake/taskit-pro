import { Component, OnInit } from '@angular/core';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { orderBy, SortDescriptor, State, process } from '@progress/kendo-data-query';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { EncryptDecryptService } from '../../../_common_services/encrypt-decrypt.service';
//import { DataSourceRequestState, DataResult, SortDescriptor, orderBy, State, process, filterBy, FilterDescriptor, CompositeFilterDescriptor, } from '@progress/kendo-data-query';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';

declare var jQuery: any;
declare var toastr: any;
@Component({
  selector: 'app-gig_list',
  templateUrl: './gig_list.component.html',
  styleUrls: ['./gig_list.component.css']
})
export class Gig_listComponent implements OnInit {

  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public pageNumber: any = 1;

  public gigList: any;
  public gigActivelist: any;
  public gigPendinglist: any;
  public gigRequiresModificationlist: any;
  public gigDraftlist: any;
  public gigDeniedlist: any;
  public gigPauselist: any;

  public result: any;
  public all_Count: number = 0;
  public active_Count: number = 0;
  public pendingApproval_Count: number = 0;
  public requiresModification_Count: number = 0;
  public draft_Count: number = 0;
  public denied_Count: number = 0;
  public pause_Count: number = 0;

  public duration_All: any;
  public duration_Active: any;
  public duration_PendingApproval: any;
  public duration_RequiresModification: any;
  public duration_Draft: any;
  public duration_Denied: any;
  public duration_Pause: any;

  public allGrid_data: GridDataResult | undefined;
  public activeGrid_data: GridDataResult | undefined;
  public pendingGrid_data: GridDataResult | undefined;
  public requiresModificationGrid_data: GridDataResult | undefined;
  public draftGrid_data: GridDataResult | undefined;
  public deniedGrid_data: GridDataResult | undefined;
  public pauseGrid_data: GridDataResult | undefined;
  public pageable_Active: boolean = true;
  public gridHeight_Active: any;
  public pageable_Pending: boolean = true;
  public gridHeight_Pending: any;
  public pageable_Modification: boolean = true;
  public gridHeight_Modification: any;
  public pageable_Denied: boolean = true;
  public gridHeight_Denied: any;
  public pageable_Paused: boolean = true;
  public gridHeight_Draft: any;
  public pageable_Draft: boolean = true;
  public gridHeight_Paused: any;
  public backoffice_list_count: any;
  public userAuthInfo: any;

  public state: State = {
    skip: 0,
    take: 10,
  };


  constructor(private router: Router, private httpServices: HttpRequestService, private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService) { }

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.progress.show();
    this.getActive_gridData(5);
    this.getGig_StatustabCount();
  }

  getGig_StatustabCount() {
    this.httpServices.request('get', 'gigs/backoffice-list-count', '', '', null).subscribe((data) => {
      this.backoffice_list_count = data;
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });

  }


  //**********************************  
  //  All Grid Data
  //**********************************

  getAll_gridData() {
    this.httpServices.request("get", "gigs/backoffice-list", null, null, null)
      .subscribe((data) => {
        this.progress.hide();
        this.gigList = data.results;
        this.loadAll_gigData(data.count);
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }
  onEditClick(gig_id: any) {
    let passQueryParam = { id: gig_id, flag: true };
    sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/new_gig'], {});
  }
  onCreateNewGigClick() {
    sessionStorage.setItem("queryParams", null);
    this.router.navigate(['/new_gig'], {});
  }
  // pageChange(event: PageChangeEvent):void{

  //   this.httpServices.request("get", "gigs/backoffice-list?status_id=" + this.statusId+"&page="+ (event.skip + this.pageSize) / this.pageSize+ "&page_size=" + this.pageSize, null, null, null)
  //   .subscribe((data) => {
  //     this.progress.hide();
  //     this.gigDraftlist = data.results;
  //     this.loadDraft_gigData(data.results.length);
  //   }, error => {
  //     console.log(error);
  //     this.progress.hide();
  //   })
  // }
  //A particular method that is always to be followed when using Kendo GRID
  private loadAll_gigData(recordCount: number): void {
    if (this.gigList.length > 0) {
      this.allGrid_data = {
        data: orderBy(this.gigList, this.sort).slice(this.skip, this.skip + this.pageSize),
        //In case if gig list page has 40 record....SLICE to split record to next position as Record to be shown for Page is 10.
        //So remaining records are sliced to skip btn press.
        total: recordCount
      };
    } else {
      this.allGrid_data = undefined;
    }
  }

  //Kendo Grid - Sorting For All Gig List
  // To handle sorting on KENDO COLUMNS 
  public sortAllgigChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadAll_gigData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For All Gig List
  public Allgig_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.allGrid_data = process(this.gigList, this.state);
  }

  openGigDetails(gig_id: any, gig_status_id: any) {
    let passQueryParam = { id: gig_id, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], { /*queryParams: passQueryParam /*, skipLocationChange: true*/ });
    //window.open("gigs_details?id=" + gig_id, "");
  }

  //**********************************  
  //  Active Grid Data
  //**********************************
  getActive_gridData(statusId: any) {
    this.progress.show();
    this.httpServices.request("get", "gigs/backoffice-list?page=1&page_size=" + this.pageSize + "&status_id=" + statusId, null, null, null)
      .subscribe((data) => {
        this.progress.hide();
        this.gigActivelist = data.results;
        if (data.results.length > 5) {
          this.gridHeight_Active = "auto";
          this.pageable_Active = true;
        }
        else {
          this.gridHeight_Active = "270";
          this.pageable_Active = true;
        }
        //this.gridHeight_Active = "auto";
        this.loadActive_gigData(data.count);
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }


  private loadActive_gigData(recordCount: number): void {
    if (this.gigActivelist.length > 0) {
      this.activeGrid_data = {
        data: orderBy(this.gigActivelist, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.activeGrid_data = undefined;
    }
  }

  //Kendo Grid - Sorting For Active Gig List
  public sortActive_gigChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadActive_gigData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For Active Gig List
  public Activegig_dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.activeGrid_data = process(this.gigActivelist, this.state);
  }

  //page Change Event
  pageChange_activeGig(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;

    var url = "gigs/backoffice-list?page=" + this.pageNumber + "&page_size=" + this.pageSize + "&status_id=" + this.statusId;

    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.progress.hide();
      this.gigActivelist = data.results;
      this.loadActive_gigData(data.count);
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }



  // ==============================
  // on paused click 
  // ===============================
  onPausedClick(id: any) {
    this.progress.show();
    this.httpServices.request('patch', 'gigs/paused/' + id, null, null, null).subscribe((data) => {
      this.progress.hide();
      toastr.success("This gig has been paused successfully");
      if (this.userAuthInfo.currentUser.user.group_id == 1) {
        this.router.navigate(['gig_list_admin'], {});
      }
      else if (this.userAuthInfo.currentUser.user.group_id == 3) {
        this.router.navigate(['gig_list'], {});
      }
      this.getActive_gridData(5);
      this.getGig_StatustabCount();
    }, (error) => {
      this.progress.hide();
    });
  }
  // ===============================
  // on Resume click
  // =================================
  onResumeClick(id: any) {
    this.progress.show();
    this.httpServices.request('patch', 'gigs/resume/' + id, null, null, null).subscribe((data) => {
      this.progress.hide();
      toastr.success("This gig has been resumed successfully");
      if (this.userAuthInfo.currentUser.user.group_id == 1) {
        this.router.navigate(['gig_list_admin'], {});
      }
      else if (this.userAuthInfo.currentUser.user.group_id == 3) {
        this.router.navigate(['gig_list'], {});
      }
      this.getPause_gridData(6);
      this.getActive_gridData(5);
      this.getGig_StatustabCount();
    }, (error) => {
      this.progress.hide();
    });
  }


  //**********************************  
  //  Pending Approval Grid Data
  //**********************************

  getPending_gridData(statusId: any) {
    this.progress.show();
    this.httpServices.request("get", "gigs/backoffice-list?page=1&page_size=" + this.pageSize + "&status_id=" + statusId, null, null, null)
      .subscribe((data) => {
        this.progress.hide();
        this.gigPendinglist = data.results;
        if (data.results.length > 5) {
          this.gridHeight_Pending = "auto";
          this.pageable_Pending = true;
        }
        else {
          this.gridHeight_Pending = "270";
          this.pageable_Pending = true;
        }
        //this.gridHeight_Pending = "auto";
        this.loadPending_gigData(data.count);
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  private loadPending_gigData(recordCount: number): void {
    if (this.gigPendinglist.length > 0) {
      this.pendingGrid_data = {
        data: orderBy(this.gigPendinglist, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.pendingGrid_data = undefined;
    }
  }

  //Kendo Grid - Sorting for Pending gig List
  public sortPending_gigChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadPending_gigData(this.totalRecordCount);
  }

  // Kendo Grid - data state change for Pending gig List
  public Pendinggig_dataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.pendingGrid_data = process(this.gigPendinglist, this.state);
  }

  //Pending page Change Event
  pageChange_pendingGig(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;

    var url = "gigs/backoffice-list?page=" + this.pageNumber + "&page_size=" + this.pageSize + "&status_id=" + this.statusId;
    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.progress.hide();
      this.gigPendinglist = data.results;
      this.loadPending_gigData(data.count);
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //**********************************  
  //  Requires Modification Grid Data
  //**********************************

  getRequiresModification_gridData(statusId: any) {
    this.progress.show();
    this.httpServices.request("get", "gigs/backoffice-list?page=1&page_size=" + this.pageSize + "&status_id=" + statusId, null, null, null)
      .subscribe((data) => {
        this.progress.hide();
        this.gigRequiresModificationlist = data.results;
        if (data.results.length > 5) {
          this.gridHeight_Modification = "auto";
          this.pageable_Modification = true;
        }
        else {
          this.gridHeight_Modification = "270";
          this.pageable_Modification = true;
        }
        //this.gridHeight_Modification = "auto";
        this.loadRequiresModification_gigData(data.count);
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  private loadRequiresModification_gigData(recordCount: number): void {
    if (this.gigRequiresModificationlist.length > 0) {
      this.requiresModificationGrid_data = {
        data: orderBy(this.gigRequiresModificationlist, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.requiresModificationGrid_data = undefined;
    }
  }

  //Kendo Grid - Sorting For Requires Modification Gig List
  public sortRequiresModification_gigChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadRequiresModification_gigData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For Requires Modification Gig List
  public RequiresModificationgig_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.requiresModificationGrid_data = process(this.gigRequiresModificationlist, this.state);
  }

  // Requires Modification  page Change Event
  pageChange_requiresModificationgig(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;

    var url = "gigs/backoffice-list?page=" + this.pageNumber + "&page_size=" + this.pageSize + "&status_id=" + this.statusId;

    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.progress.hide();
      this.gigRequiresModificationlist = data.results;
      this.loadRequiresModification_gigData(data.count);
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //**********************************  
  //  Draft Grid Data
  //**********************************
  public statusId: any;
  getDraft_gridData(statusId: number) {
    this.progress.show();
    this.statusId = statusId;
    this.httpServices.request("get", "gigs/backoffice-list?page=1&page_size=" + this.pageSize + "&status_id=" + statusId, '', '', '')
      .subscribe((data) => {
        this.progress.hide();
        this.gigDraftlist = data.results;
        if (data.results.length > 5) {
          this.gridHeight_Draft = "auto";
          this.pageable_Draft = true;
        }
        else {
          this.gridHeight_Draft = "270";
          this.pageable_Draft = true;
        }
        //this.gridHeight_Draft = "auto";
        this.loadDraft_gigData(data.count);
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  private loadDraft_gigData(recordCount: number): void {
    if (this.gigDraftlist.length > 0) {
      this.draftGrid_data = {
        data: orderBy(this.gigDraftlist, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.draftGrid_data = undefined;
    }
  }

  //Kendo Grid - Sorting For Draft Gig List
  public sortDraft_gigChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadDraft_gigData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For Draft Gig List
  public Draftgig_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.draftGrid_data = process(this.gigDraftlist, this.state);
  }

  //page Change Event
  pageChange(event: PageChangeEvent): void {

    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;

    var url = "gigs/backoffice-list?page=" + this.pageNumber + "&page_size=" + this.pageSize + "&status_id=" + this.statusId;
    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.progress.hide();
      this.gigDraftlist = data.results;
      this.loadDraft_gigData(data.count);
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //   pageChange(event: PageChangeEvent):void{ 
  //     
  //   this.httpServices.request("get", "gigs/backoffice-list?status_id=" + this.statusId+"&page="+ (event.skip + this.pageSize) / this.pageSize+ "&page_size=" + this.pageSize, null, null, null)
  //   .subscribe((data) => {
  //     this.progress.hide();
  //     this.gigDraftlist = data.results;
  //     this.loadDraft_gigData(data.count);
  //   }, error => {
  //     console.log(error);
  //     this.progress.hide();
  //   })
  // }


  //**********************************  
  //  Denied Grid Data
  //**********************************

  getDenied_gridData(statusId: any) {
    this.progress.show();
    this.httpServices.request("get", "gigs/backoffice-list?page=1&page_size=" + this.pageSize + "&status_id=" + statusId, null, null, null)
      .subscribe((data) => {
        this.progress.hide();
        this.gigDeniedlist = data.results;
        if (data.results.length > 5) {
          this.gridHeight_Denied = "auto";
          this.pageable_Denied = true;
        }
        else {
          this.gridHeight_Denied = "270";
          this.pageable_Denied = true;
        }
        //this.gridHeight_Denied = "auto";
        this.loadDenied_gigData(data.count);
      }, error => {
        console.log(error);
        this.progress.hide();

      });
  }

  private loadDenied_gigData(recordCount: number): void {
    if (this.gigDeniedlist.length > 0) {
      this.deniedGrid_data = {
        data: orderBy(this.gigDeniedlist, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.deniedGrid_data = undefined;
    }
  }

  //Kendo Grid - Sorting For Denied Gig List
  public sortDenied_gigChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadDenied_gigData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For Denied Gig List
  public Deniedgig_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.deniedGrid_data = process(this.gigDeniedlist, this.state);
  }

  //page Change Event
  pageChange_Deniedgig(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;

    var url = "gigs/backoffice-list?page=" + this.pageNumber + "&page_size=" + this.pageSize + "&status_id=" + this.statusId;

    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.progress.hide();
      this.gigDeniedlist = data.results;
      this.loadDenied_gigData(data.count);
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //**********************************  
  //  Pause Grid Data
  //**********************************

  getPause_gridData(statusId: any) {
    this.progress.show();
    this.httpServices.request("get", "gigs/backoffice-list?page=1&page_size=" + this.pageSize + "&status_id=" + statusId, null, null, null)
      .subscribe((data) => {
        this.progress.hide();
        this.gigPauselist = data.results;
        if (data.results.length > 5) {
          this.gridHeight_Paused = "auto";
          this.pageable_Paused = true;
        }
        else {
          this.gridHeight_Paused = "270";
          this.pageable_Paused = true;
        }
        //this.gridHeight_Paused = "auto";
        this.loadPause_gigData(data.count);
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  private loadPause_gigData(recordCount: number): void {
    if (this.gigPauselist.length > 0) {
      this.pauseGrid_data = {
        data: orderBy(this.gigPauselist, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.pauseGrid_data = undefined;
    }
  }

  //Kendo Grid - Sorting For Pause Gig List
  public sortPause_gigChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadPause_gigData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For Pause Gig List
  public Pausegig_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.pauseGrid_data = process(this.gigPauselist, this.state);
  }

  //page Change Event
  pageChange_Pausegig(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;

    var url = "gigs/backoffice-list?page=" + this.pageNumber + "&page_size=" + this.pageSize + "&status_id=" + this.statusId;

    this.httpServices.request("get", url, '', '', null).subscribe((data) => {
      this.progress.hide();
      this.gigPauselist = data.results;
      this.loadPause_gigData(data.count);
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //************************************************************  
  // To Search Gif Records As per duration : 7days || 14 days
  //************************************************************

  onSearchGigList(statusId: any, durationId: any) {
    this.progress.show();
    let url = "";
    if (statusId == 0) {
      url = "gigs/list?duration" + durationId;
    }
    else {
      url = "gigs/list?duration" + durationId + "&status_id=" + statusId;
    }
    this.httpServices.request("get", url, null, null, null)
      .subscribe(data => {
        this.progress.hide();
        this.gigList = data.results;
        this.loadAll_gigData(data.results.length);
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  //************************************************************  
  // Grid's action button click
  //************************************************************
  public gigIdDelete:any;
  public gigGrid:any;
  actionClick(grid: any, button_name: any, gig_id: any, gig_status_id: any) {
    this.gigGrid=grid;
    if (grid == 'active') {
      if (button_name == 'preview') {
        this.openGigDetails(gig_id, gig_status_id);
      }
      else if (button_name == 'edit') {
        let passQueryParam = { id: gig_id, gig_status_id: gig_status_id, flag: true };
        sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));
        this.router.navigate(['new_gig'], {/* queryParams: passQueryParam /*, skipLocationChange: true*/ });
      }
      else if (button_name == 'pause') {
        this.onPausedClick(gig_id);
      }
      else if (button_name == 'delete') {
        this.gigIdDelete=gig_id;
        jQuery('#delete_gig_modal').modal({ backdrop: 'static', keyboard: false });
      }
    }
    else if (grid == 'pending_4_approval') {
      if (button_name == 'preview') {
        this.openGigDetails(gig_id, gig_status_id);
      }
      else if (button_name == 'edit') {
        let passQueryParam = { id: gig_id, gig_status_id: gig_status_id, flag: true };
        sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));
        this.router.navigate(['new_gig'], {/* queryParams: passQueryParam /*, skipLocationChange: true*/ });
      }
      else if (button_name == 'delete') {
        this.gigIdDelete=gig_id;
        jQuery('#delete_gig_modal').modal({ backdrop: 'static', keyboard: false });
      }
    }
    else if (grid == 'req_modification') {
      if (button_name == 'preview') {
        this.openGigDetails(gig_id, gig_status_id);
      }
      else if (button_name == 'edit') {
        let passQueryParam = { id: gig_id, gig_status_id: gig_status_id, flag: true };
        sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));
        this.router.navigate(['new_gig'], {/* queryParams: passQueryParam /*, skipLocationChange: true*/ });
      }
      else if (button_name == 'delete') {
        this.gigIdDelete=gig_id;
        jQuery('#delete_gig_modal').modal({ backdrop: 'static', keyboard: false });
      }
    }
    else if (grid == 'draft') {
      if (button_name == 'preview') {
        this.openGigDetails(gig_id, gig_status_id);
      }
      else if (button_name == 'edit') {
        let passQueryParam = { id: gig_id, gig_status_id: gig_status_id, flag: true };
        sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));
        this.router.navigate(['new_gig'], {/* queryParams: passQueryParam /*, skipLocationChange: true*/ });
      }
      else if (button_name == 'delete') {
        this.gigIdDelete=gig_id;
        jQuery('#delete_gig_modal').modal({ backdrop: 'static', keyboard: false });
      }
    }
    else if (grid == 'denied') {
      if (button_name == 'preview') {
        this.openGigDetails(gig_id, gig_status_id);
      }
      else if (button_name == 'edit') {
        let passQueryParam = { id: gig_id, gig_status_id: gig_status_id, flag: true };
        sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));
        this.router.navigate(['new_gig'], {/* queryParams: passQueryParam /*, skipLocationChange: true*/ });
      }
      else if (button_name == 'delete') {
        this.gigIdDelete=gig_id;
        jQuery('#delete_gig_modal').modal({ backdrop: 'static', keyboard: false });
      }
    }
    else if (grid == 'paused') {
      if (button_name == 'preview') {
        this.openGigDetails(gig_id, gig_status_id);
      }
      else if (button_name == 'edit') {
        let passQueryParam = { id: gig_id, gig_status_id: gig_status_id, flag: true };
        sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));
        this.router.navigate(['new_gig'], {/* queryParams: passQueryParam /*, skipLocationChange: true*/ });
      }
      else if (button_name == 'resume') {
        this.onResumeClick(gig_id);
      }
      else if (button_name == 'delete') {
        this.gigIdDelete=gig_id;
        jQuery('#delete_gig_modal').modal({ backdrop: 'static', keyboard: false });
      }
    }
  }
  removeGig()
  {
    if(this.gigIdDelete>0)
    {
      let url=""
      if(this.gigGrid=='active' || this.gigGrid=='paused')
      {
        url="gigs/"+this.gigIdDelete;
      }
      else{
        url="gigs/draft/"+this.gigIdDelete;
      }
      this.progress.show();
      this.httpServices.request('delete', url, null, null, null).subscribe((data) => {
       if(this.gigGrid=='active')
       {
        this.getActive_gridData(5);
       }
       else if(this.gigGrid=='req_modification')
       {
        this.getRequiresModification_gridData(3);
       }
       else if(this.gigGrid=='draft')
       {
        this.getDraft_gridData(1);
       }
       else if(this.gigGrid=='denied')
       {
        this.getDenied_gridData(4);
       }
       else if(this.gigGrid=='paused')
       {
         this.getPause_gridData(6);
       }
       else if(this.gigGrid=='pending_4_approval')
       {
         this.getPending_gridData(2);
       }
       this.getGig_StatustabCount();
       jQuery('#delete_gig_modal').modal("hide");
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
  }
}// End of Gig_listComponent Class



