/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Gig_offersComponent } from './gig_offers.component';

describe('Gig_offersComponent', () => {
  let component: Gig_offersComponent;
  let fixture: ComponentFixture<Gig_offersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Gig_offersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Gig_offersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
