import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-gig_offers',
  templateUrl: './gig_offers.component.html',
  styleUrls: ['./gig_offers.component.css']
})
export class Gig_offersComponent implements OnInit {
  public gig_offersList:any;
  public queryParam:any;
  public _queryParams:any;
  public category_id:number;
  public subCategory_id:number;
  public delivery_time_value:any;
  public budget:any
  public userAuthInfo: any;
  public fromGuestLogin: boolean = false;
  constructor(private router: Router,private httpServices: HttpRequestService,private encrypt_decrypt: EncryptDecryptService,private progress: NgxSpinnerService) { }

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    if (this.queryParam == null || this.queryParam ==undefined) {
      this._queryParams = (sessionStorage.getItem("queryParams") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      this.category_id = this.queryParam.category_id;
      this.subCategory_id=this.queryParam.subCategory_id;
      this.delivery_time_value=this.queryParam.delivery_time_value;
      this.budget=this.queryParam.budget.replace(".00","");
      this.getgig_offers();
    }
  }

  getgig_offers() {
    this.progress.show();
    var url="home_screen/search-result?category_id="+this.category_id+"&sub_category_id="+this.subCategory_id+"&offer_delivery_time="+this.delivery_time_value+"&offer_budget="+this.budget;
    this.httpServices.request("get", url, "", "", null)
      .subscribe((data) => {
        this.gig_offersList = data.results;
        this.progress.hide();
      },error=>{
        console.log(error);
        this.progress.hide();
      });
  }

public printStar:any;
public printStarEmpty:any;
   // ==================================
  // Display rating 
  // ==================================
  getStar(value:any){
    if(value==null){
      value=0;
    }
    if(value>5){
      value=5;
    }
    let printStar='';
    for (let i = 0; i < value; i++) {
      printStar=printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>'
    }
    this.printStar=printStar;
  }

  getEmptyStar() {
    let printStar = '';
    for (let i = 0; i < 5; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStarEmpty = printStar;
  }


  // ================================
  // On click of Favourite gig 
  // =================================
  tagFavourite(event: any, flag: any) {
    if (this.userAuthInfo.currentUser.user == undefined) {
      this.encrypt_decrypt.changeDataFavourite({ flag: true, location: "tagFav", is_Favourite: flag, gig_Data: event });
    }
    else {
      if (this.fromGuestLogin) {
        this.encrypt_decrypt.changeDataTagFav({
          is_login: false, is_Favourite: false
          , gig_Data: null
        });
        this.fromGuestLogin = false;
      }
      this.progress.show();
      let gigDetails = { gig_id: event.id }
      this.httpServices.request('post', 'buyers/user-favourite-gigs?is_favourite=' + flag, null, null, gigDetails).subscribe((data) => {
        //this.categoryList = data;
        this.progress.hide();
        return event.is_favourite = flag;
      }, error => {
        this.progress.hide();
        console.log(error);
      });
    }
  }


  // ================================
  // On click of particular gig 
  // =================================
  onGigClick(gigid: any, gig_status_id: any) {
    let passQueryParam = { id: gigid, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], {});
  }
  
  // ============================
  // User Name to Seller Profile
  // ============================
  onSellerClick(sellerId) {
    // let passQueryParam = { userId: sellerId };
    // sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));

    let profile_mode: string;
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      } else {
        profile_mode = 'other';
      }

    } else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId;
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
}
