/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Help_and_supportComponent } from './help_and_support.component';

describe('Help_and_supportComponent', () => {
  let component: Help_and_supportComponent;
  let fixture: ComponentFixture<Help_and_supportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Help_and_supportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Help_and_supportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
