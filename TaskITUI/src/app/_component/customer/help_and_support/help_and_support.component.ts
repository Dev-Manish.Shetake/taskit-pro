import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
@Component({
  selector: 'app-help_and_support',
  templateUrl: './help_and_support.component.html',
  styleUrls: ['./help_and_support.component.css']
})
export class Help_and_supportComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    jQuery('body').scrollTop(0);
  }

}
