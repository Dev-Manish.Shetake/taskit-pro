import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
declare var toastr: any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService, private httpServices: HttpRequestService, private authService: SocialAuthService, private router: Router) {
    this.loadScripts();
  }
  loadScripts() {
    // This array contains all the files/CDNs 
    const dynamicScripts = [
      'assets/js/custom.js'
    ];

    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      document.getElementsByTagName('body')[0].appendChild(node);
    }
  }
  // public categoryList: any;
  public bestsellerList: any;
  public favouriteList: any;
  public topServicesList: any;
  public browsingHistoryList: any;
  public taskitPicsList: any;
  public printStar: string = '';
  public subCategoryId: any;
  public userAuthInfo: any;
  public printStarEmpty: string = '';
  public bestSellers_Row: string = 'col-md-12 col-lg-12 custom_col_md_12_padding';
  public favorites_Row: string = 'col-md-12 col-lg-12 custom_col_md_12_padding';
  public topServices_Row: string = 'col-md-12 col-lg-12 custom_col_md_12_padding';
  public browsingHistory_Row: string = 'col-md-12 col-lg-12 custom_col_md_12_padding';
  public taskitPics_Row: string = 'col-md-12 col-lg-12 custom_col_md_12_padding';
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.subCategoryId = 7; //Logo Design;
    this.getBestSeller(this.subCategoryId);
    //this.progress.show()
    // this.httpServices.request('get', 'categories/subcategory-list?is_active=true', null, null, null).subscribe((data) => {
    //   this.categoryList = data;
    //   this.subCategoryId=6;
    //   this.getBestSeller(this.subCategoryId);
    // });
  }
  // ===============================
  // On Subcategory Click 
  // ===============================
  onSubCategoryClick(subCategoryId: any) {
    //this.progress.show();
    this.getBestSeller(subCategoryId);
    // this.getFavourites(subCategoryId);
    // this.getTopServices(subCategoryId);
    // this.getBrowsingHistory(subCategoryId);
    // this.getTaskitPics(subCategoryId);
    // this.progress.hide();
  }
  // ==================================
  // Get Best Seller Info 
  // ==================================
  getBestSeller(subCategoryId: any) {
    this.progress.show()
    this.httpServices.request('get', 'home_screen/best-sellers?sub_category_id=' + subCategoryId, null, null, null).subscribe((data) => {
      this.bestsellerList = data.results;

      if (data.results.length == 1) {
        this.bestSellers_Row = 'col-md-3 col-lg-3 custom_col_md_12_padding';
      }
      else if (data.results.length == 2) {
        this.bestSellers_Row = 'col-md-6 col-lg-6 custom_col_md_12_padding';
      }
      else if (data.results.length == 3) {
        this.bestSellers_Row = 'col-md-9 col-lg-9 custom_col_md_12_padding';
      }

      this.getFavourites(subCategoryId);
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // ==================================
  // get Favourites info 
  // ==================================
  getFavourites(subCategoryId: any) {
    this.httpServices.request('get', 'home_screen/favourite-list', null, null, null).subscribe((data) => {
      this.favouriteList = data.results;

      if (data.results.length == 1) {
        this.favorites_Row = 'col-md-3 col-lg-3 custom_col_md_12_padding';
      }
      else if (data.results.length == 2) {
        this.favorites_Row = 'col-md-6 col-lg-6 custom_col_md_12_padding';
      }
      else if (data.results.length == 3) {
        this.favorites_Row = 'col-md-9 col-lg-9 custom_col_md_12_padding';
      }

      this.getTopServices(subCategoryId);
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // ================================
  // get top services info 
  // =================================
  getTopServices(subCategoryId: any) {
    this.httpServices.request('get', 'home_screen/top-services?sub_category_id=7', null, null, null).subscribe((data) => {
      this.topServicesList = data.results;

      if (data.results.length == 1) {
        this.topServices_Row = 'col-md-3 col-lg-3 custom_col_md_12_padding';
      }
      else if (data.results.length == 2) {
        this.topServices_Row = 'col-md-6 col-lg-6 custom_col_md_12_padding';
      }
      else if (data.results.length == 3) {
        this.topServices_Row = 'col-md-9 col-lg-9 custom_col_md_12_padding';
      }

      this.getBrowsingHistory(subCategoryId);
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // =================================
  // get browsing history info 
  // ==================================
  getBrowsingHistory(subCategoryId: any) {
    this.httpServices.request('get', 'home_screen/browsing-history', null, null, null).subscribe((data) => {
      this.browsingHistoryList = data.results;

      if (data.results.length == 1) {
        this.browsingHistory_Row = 'col-md-3 col-lg-3 custom_col_md_12_padding';
      }
      else if (data.results.length == 2) {
        this.browsingHistory_Row = 'col-md-6 col-lg-6 custom_col_md_12_padding';
      }
      else if (data.results.length == 3) {
        this.browsingHistory_Row = 'col-md-9 col-lg-9 custom_col_md_12_padding';
      }

      this.getTaskitPics(subCategoryId);
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // =================================
  // get taskit pics info 
  // =================================
  getTaskitPics(subCategoryId: any) {
    this.httpServices.request('get', 'home_screen/taskit-pics', null, null, null).subscribe((data) => {
      this.taskitPicsList = data.results;

      if (data.results.length == 1) {
        this.taskitPics_Row = 'col-md-3 col-lg-3 custom_col_md_12_padding';
      }
      else if (data.results.length == 2) {
        this.taskitPics_Row = 'col-md-6 col-lg-6 custom_col_md_12_padding';
      }
      else if (data.results.length == 3) {
        this.taskitPics_Row = 'col-md-9 col-lg-9 custom_col_md_12_padding';
      }

      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }

  //==========================================================
  //  Most Popular Services    
  //==========================================================
  customOptions: OwlOptions = {
    loop: true,
    items: 4,
    margin: 20,
    stagePadding: 0,
    // mouseDrag: false,
    // touchDrag: false,
    // pullDrag: false,
    dots: false,
    navSpeed: 200,
    navText: ['&#8249', '&#8250;'],
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      760: {
        items: 3
      },
      1000: {
        items: 4
      }
    },
  }


  // ==================================
  // Display rating 
  // ==================================
  getStar(value: any) {
    if (value == null) {
      value = 0;
    }
    if (value > 5) {
      value = 5;
    }
    let printStar = '';
    for (let i = 0; i < value; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStar = printStar;
  }
  getEmptyStar() {

    let printStar = '';
    for (let i = 0; i < 5; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStarEmpty = printStar;
  }

  // ================================================
  // On Category Bar Click show SubCategories Screen
  // ================================================
  onCategory_barClick(category_id: number) {
    let passQueryParam = { category_id: category_id };
    sessionStorage.setItem("queryCategory_idParam", JSON.stringify(passQueryParam));
    this.router.navigate(['/subCategories_screen']);
  }
  // ================================
  // On click of particular gig 
  // =================================
  onGigClick(gigid: any, gig_status_id: any) {
    let passQueryParam = { id: gigid, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], {});
  }
  // ============================
  // User Name to Seller Profile
  // ============================
  onSellerClick(sellerId) {
    // let passQueryParam = { userId: sellerId };
    // sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));

    let profile_mode: string;
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      } else {
        profile_mode = 'other';
      }

    } else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId;
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
}


