import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { formatDate, getLocaleDateFormat } from '@angular/common';
import { CommonModule } from '@angular/common';
import { EncryptDecryptService } from '../../../_common_services/encrypt-decrypt.service';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import { anyChanged } from '@progress/kendo-angular-common';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { Router } from '@angular/router';
declare var toastr: any;
declare var jQuery: any;
@Component({
  selector: 'app-order_details',
  templateUrl: './order_details.component.html',
  styleUrls: ['./order_details.component.css']
})
export class Order_detailsComponent implements OnInit {

  constructor(private router: Router, private dateFormatter: DateFormatterService, private progress: NgxSpinnerService, private httpServices: HttpRequestService, private encrypt_decrypt: EncryptDecryptService) { }
  public queryParam: any;
  public _queryParams: any;
  public orderId: any;
  public orderDetails: any;
  public chat_message: any;
  public orders_chat: orders_chat = {
    results: [
      {
        id: 0,
        chat: "",
        file_path: "",
        created_by: {
          id: 0,
          user_name: "",
          level: {
            id: 0,
            level_name: ""
          },
          rating: ""
        },
        created_user: "",
        created_date: "",
        file_type: "",
        file_name: ""
      }
    ]
  }
  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userId: any;
  public showRequirement: boolean = false;
  public requirement_details: requirement_details = {
    results: [
      {
        order_id: 0,
        is_mandatory: false,
        question: "",
        question_form: {
          id: 0,
          question_form_name: ""
        },
        is_multiselect: false,
        description: [
          {
            id: 0,
            file_path: "",
            description: ""
          },
          {
            id: 0,
            file_path: "",
            description: ""
          }
        ],
        buildDescription: ""
      }
    ]
  }
  public orderRequirementDetails: orderRequirementDetails = {
    requirement:
      [{
        id: 0,
        gig_draft_id: "",
        gig_id: "",
        is_mandatory: false,
        question: "",
        question_form: {
          id: 0,
          question_form_name: ""
        },
        is_multiselect: false,
        gig_description: [
          {
            id: 0,
            description: "",
            check: false
          }
        ],
        ansText: "",
        ansCheckBox: "",
        ansDropDown: "",
        urls: []
      }]
  }
  public groupId: any;
  public submit_reviewFile: any;
  @ViewChild('submit_reviewFile', { read: ElementRef }) submit_review_file: ElementRef;
  public chat_File: any;
  @ViewChild('chat_File', { read: ElementRef }) chatFile: ElementRef;
  public order_delivery_details: order_delivery_details = {
    result: [
      {
        id: 0,
        seller: 0,
        filename: "",
        file_size: "",
        buyer_download: false,
        created_date: "",
        comment: [
          {
            id: 0,
            comment: "",
            created_date: ""
          }
        ],
        order_complete_days_limit: 0
      }
    ]
  }
  public order_review_details: any;
  public cancel_reason: any;
  ngOnInit() {
    jQuery('body').scrollTop(0);
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    this.userId = this.userAuthInfo.currentUser.user.id;
    this.groupId = this.userAuthInfo.currentUser.user.group_id;
    if (this.queryParam == null) {
      this._queryParams = (sessionStorage.getItem("order_details_queryParams") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      this.orderId = this.queryParam.id;
      if (this.queryParam.cancel_reason != undefined)
        this.cancel_reason = this.queryParam.cancel_reason;
      this.getOrderDetails(this.orderId);
      this.getOrderChatList(this.orderId);
      this.getDeliveryDetails(this.orderId);
      this.getReviews(this.orderId);
    }
  }

  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }

  public requirementMandatoryFound: boolean = false;
  public showRequirementPendingBand: boolean = false;
  getOrderDetails(orderId) {
    this.progress.show();
    this.httpServices.request('get', "orders/" + orderId, null, null, null).subscribe((data) => {
      this.orderDetails = data;
      this.orderDetails.created_date = this.dateFormatter.format(new Date(this.orderDetails.created_date), 'yyyy-MM-dd hr:mm:ss')
      if (data.requirement_submit_date != null && data.requirement_submit_date != undefined && data.requirement_submit_date != "") {
        this.showRequirement = true;
        this.showRequirementPendingBand = false;
        this.httpServices.request('get', "orders/gig-requirement-details/" + orderId, null, null, null).subscribe((dataRequirement) => {
          this.requirement_details.results = dataRequirement;
          for (let i = 0; i < this.requirement_details.results.length; i++) {
            if (this.requirement_details.results[i].question_form.id == 1) {
              if (this.requirement_details.results[i].description != null && this.requirement_details.results[i].description != undefined && this.requirement_details.results[i].description.length > 0) {
                this.requirement_details.results[i].buildDescription = this.requirement_details.results[i].description[0].description;
              }
            }
            else if (this.requirement_details.results[i].question_form.id == 2 && this.requirement_details.results[i].is_multiselect == true) {
              if (this.requirement_details.results[i].description != null && this.requirement_details.results[i].description != undefined && this.requirement_details.results[i].description.length > 0) {
                if (this.requirement_details.results[i].description.length > 1) {
                  for (let k = 0; k < this.requirement_details.results[i].description.length; k++) {
                    if (k == 0) {
                      this.requirement_details.results[i].buildDescription = this.requirement_details.results[i].description[k].description;
                    }
                    else {
                      this.requirement_details.results[i].buildDescription = this.requirement_details.results[i].buildDescription + ", " + this.requirement_details.results[i].description[k].description;
                    }
                  }
                }
                else {
                  this.requirement_details.results[i].buildDescription = this.requirement_details.results[i].description[0].description;
                }
              }
            }
            else if (this.requirement_details.results[i].question_form.id == 2 && this.requirement_details.results[i].is_multiselect == false) {
              if (this.requirement_details.results[i].description != null && this.requirement_details.results[i].description != undefined && this.requirement_details.results[i].description.length > 0) {
                this.requirement_details.results[i].buildDescription = this.requirement_details.results[i].description[0].description;
              }
            }
          }
          for (let i = 0; i < this.requirement_details.results.length; i++) {
            if (this.requirement_details.results[i].is_mandatory == true && this.requirement_details.results[i].buildDescription == '') {
              this.requirementMandatoryFound = true;
              break;
            }
          }
        }, (error) => {
          this.progress.hide();
        });
      }
      else {
        this.showRequirementPendingBand = true;
        this.httpServices.request("get", "orders/requirement/" + this.orderId, null, null, null).subscribe((response) => {
          this.orderRequirementDetails.requirement = response;
          this.orderRequirementDetails.requirement.forEach(element => {
            element.urls = [];
            if (element.question_form.id == 2 && element.is_multiselect == false) {
              element.ansDropDown = "0";
            }
          })
          this.progress.hide();
        });
      }
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }
  public lastCommentId: any;
  getDeliveryDetails(orderId) {
    this.httpServices.request('get', "orders/delivery-list/" + orderId, null, null, null).subscribe((data) => {
      this.order_delivery_details.result = data;
      if (this.order_delivery_details.result.length > 0) {
        for (let i = 0; i < this.order_delivery_details.result.length; i++) {
          this.order_delivery_details.result[i].created_date = this.dateFormatter.format(new Date(this.order_delivery_details.result[i].created_date), 'dd MMM yyyy hr:mm');
          if (i == this.order_delivery_details.result.length - 1) {
            if (this.order_delivery_details.result[i].comment.length > 0) {
              for (let k = 0; k < this.order_delivery_details.result[i].comment.length; k++) {
                if (k == this.order_delivery_details.result[i].comment.length - 1) {
                  this.lastCommentId = this.order_delivery_details.result[i].comment[k].id;
                }
              }
            }
          }
        }
      }

    });
  }
  getReviews(orderId) {
    this.httpServices.request('get', "orders/delivery-review-list/" + orderId, null, null, null).subscribe((data) => {
      this.order_review_details = data;
    });
  }
  //==========================================================
  // get star event - to display rating
  //==========================================================
  public printStar: string = '';
  getStar(value: any) {

    if (value == null) {
      value = 0;
    }
    if (value > 5) {
      value = 5;
    }
    let printStar = '';
    for (let i = 0; i < value; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');

    }
    this.printStar = printStar;
    //this.printStar = Array(value).fill('whatever');
    //this.printStar = Array(value).fill('rating_stars');
  }
  // ==================================
  // Display rating 
  // ==================================
  public printStarEmpty: string = '';
  getEmptyStar() {

    let printStar = '';
    for (let i = 0; i < 5; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStarEmpty = printStar;
  }
  openFile(item: any) {
    window.open(item);
  }
  public showChat: boolean = false;
  getOrderChatList(orderId) {
    this.httpServices.request('get', "orders_chat/list/" + orderId, null, null, null).subscribe((data) => {
      this.orders_chat.results = data.results;
      this.orders_chat.results.forEach(element => {
        if (element.chat == null && element.file_path != null) {

          let lastIndexOfDot;
          lastIndexOfDot = element.file_path.lastIndexOf(".");
          element.file_type = element.file_path.substring(lastIndexOfDot + 1, element.file_path.length);
          if (element.file_type.toLowerCase() == "jpg" || element.file_type.toLowerCase() == "png" || element.file_type.toLowerCase() == "gif"
            || element.file_type.toLowerCase() == "webp" || element.file_type.toLowerCase() == "tiff" || element.file_type.toLowerCase() == "psd"
            || element.file_type.toLowerCase() == "bmp" || element.file_type.toLowerCase() == "svg" || element.file_type.toLowerCase() == "jpeg"
            || element.file_type.toLowerCase() == "ai") {
            element.file_type = "image";
          }
          else if (element.file_type.toLowerCase() == "mp3" || element.file_type.toLowerCase() == "aac" || element.file_type.toLowerCase() == "flac"
            || element.file_type.toLowerCase() == "ogg" || element.file_type.toLowerCase() == "wma") {
            element.file_type = "audio";
          }
          else if (element.file_type.toLowerCase() == "mp4" || element.file_type.toLowerCase() == "mov" || element.file_type.toLowerCase() == "wmv"
            || element.file_type.toLowerCase() == "avi" || element.file_type.toLowerCase() == "avchd" || element.file_type.toLowerCase() == "flv" ||
            element.file_type.toLowerCase() == "f4v") {
            element.file_type = "video";
          }
          else if (element.file_type.toLowerCase() == "doc" || element.file_type.toLowerCase() == "docx" || element.file_type.toLowerCase() == "html"
            || element.file_type.toLowerCase() == "htm" || element.file_type.toLowerCase() == "odt" || element.file_type.toLowerCase() == "pdf"
            || element.file_type.toLowerCase() == "xls" || element.file_type.toLowerCase() == "xlsx" || element.file_type.toLowerCase() == "ods"
            || element.file_type.toLowerCase() == "ppt" || element.file_type.toLowerCase() == "pptx" || element.file_type.toLowerCase() == "txt") {
            let lastIndexofHash;
            lastIndexofHash = element.file_path.lastIndexOf("/");
            element.file_name = element.file_path.substring(lastIndexofHash + 1, element.file_path.length);
            element.file_name = element.file_name.substring(element.file_name.indexOf("_") + 1, element.file_name.length);
            element.file_type = "doc"
          }

        }
      })
      this.showChat = true;
    });
  }
  sendMessage() {
    if (this.chat_File != null && this.chat_File != undefined && this.chat_File != "") {
      let formData = new FormData();
      formData.append("order_id", this.orderId.toString());
      formData.append("file_path", this.chat_File[0]);
      this.httpServices.request('post', "orders_chat/create", null, null, formData).subscribe((data) => {
        let level: any = {
          id: 0,
          level_name: ""
        }
        if (data.created_by != null && data.created_by.level != null) {
          level.id = data.created_by.level.id;
          level.level_name = data.created_by.level.level_name;
        }
        let created_by: any = {
          id: 0,
          user_name: "",
          level: "",
          rating: ""
        }
        created_by.id = data.created_by.id;
        created_by.user_name = data.created_by.user_name;
        created_by.level = level;
        created_by.rating = data.created_by.rating;

        let file_type;
        let file_name;
        if (data.chat == null && data.file_path != null) {

          let lastIndexOfDot;

          lastIndexOfDot = data.file_path.lastIndexOf(".");
          file_type = data.file_path.substring(lastIndexOfDot + 1, data.file_path.length);
          if (file_type.toLowerCase() == "jpg" || file_type.toLowerCase() == "png" || file_type.toLowerCase() == "gif"
            || file_type.toLowerCase() == "webp" || file_type.toLowerCase() == "tiff" || file_type.toLowerCase() == "psd"
            || file_type.toLowerCase() == "bmp" || file_type.toLowerCase() == "svg" || file_type.toLowerCase() == "jpeg"
            || file_type.toLowerCase() == "ai") {
            file_type = "image";
          }
          else if (file_type.toLowerCase() == "mp3" || file_type.toLowerCase() == "aac" || file_type.toLowerCase() == "flac"
            || file_type.toLowerCase() == "ogg" || file_type.toLowerCase() == "wma") {
            file_type = "audio";
          }
          else if (file_type.toLowerCase() == "mp4" || file_type.toLowerCase() == "mov" || file_type.toLowerCase() == "wmv"
            || file_type.toLowerCase() == "avi" || file_type.toLowerCase() == "avchd" || file_type.toLowerCase() == "flv" ||
            file_type.toLowerCase() == "f4v") {
            file_type = "video";
          }
          else if (file_type.toLowerCase() == "doc" || file_type.toLowerCase() == "docx" || file_type.toLowerCase() == "html"
            || file_type.toLowerCase() == "htm" || file_type.toLowerCase() == "odt" || file_type.toLowerCase() == "pdf"
            || file_type.toLowerCase() == "xls" || file_type.toLowerCase() == "xlsx" || file_type.toLowerCase() == "ods"
            || file_type.toLowerCase() == "ppt" || file_type.toLowerCase() == "pptx" || file_type.toLowerCase() == "txt") {
            let lastIndexofHash;
            lastIndexofHash = data.file_path.lastIndexOf("/");
            file_name = data.file_path.substring(lastIndexofHash + 1, data.file_path.length);
            file_name = file_name.substring(file_name.indexOf("_") + 1, file_name.length);
            file_type = "doc"
          }

        }


        this.orders_chat.results.push({
          id: data.id, chat: data.chat, file_path: data.file_path, created_by: created_by,
          created_user: data.created_user, created_date: data.created_date, file_type: file_type,
          file_name: file_name
        });
        this.chat_message = "";
        this.showFileChatInput = false;
      });
    }
    else {
      let chatBody: any = {
        order_id: 0,
        chat: ""
      }
      chatBody.order_id = this.orderId;
      chatBody.chat = this.chat_message;
      this.httpServices.request('post', "orders_chat/create", null, null, chatBody).subscribe((data) => {
        let level: any = {
          id: 0,
          level_name: ""
        }
        if (data.created_by != null && data.created_by.level != null) {
          level.id = data.created_by.level.id;
          level.level_name = data.created_by.level.level_name;
        }
        let created_by: any = {
          id: 0,
          user_name: "",
          level: "",
          rating: ""
        }
        created_by.id = data.created_by.id;
        created_by.user_name = data.created_by.user_name;
        created_by.level = level;
        created_by.rating = data.created_by.rating;
        this.orders_chat.results.push({
          id: data.id, chat: data.chat, file_path: data.file_path, created_by: created_by,
          created_user: data.created_user, created_date: data.created_date, file_type: "", file_name: ""
        });
        this.chat_message = "";
      });
    }

  }
  // ======================================
  // on Enter click 
  // ====================================
  enterClick(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if (charCode == 13) {
      document.getElementById('btnSendMessage').click();
    }
  }
  public fileErrorMsgArr: any[] = [];
  public campFileSpan: any;
  public none = "none";
  public filename: string = "";
  public descriptionCheckArray: Array<{ requirementId: number, descriptionId: number, description: string, check: boolean }> = [];
  displayFileErrorMsg() {
    let errorMsg: string = "";
    if (this.fileErrorMsgArr.length > 0) {
      this.fileErrorMsgArr.forEach(element => {
        if (element.issue == "filetype") {
          errorMsg = errorMsg + "<li>" + "The file " +
            element.filename + " does not match file type." + "</li>";
        }
        else if (element.issue == "filesize") {
          errorMsg = errorMsg + "<li>" + "The file " +
            element.filename + " having sizze: "
            + element.filesize + " does not match file size condition." + "</li>";
        }
      });
      toastr.error(errorMsg);
    }
  }
  // ========================================
  // on check of requirement answers 
  // =========================================
  onrequirementCheckChange(requirementId: any, descriptionId: any, description: any, check: boolean) {

    if (check == true) {
      this.descriptionCheckArray.push({
        requirementId: requirementId, descriptionId: descriptionId, description: description,
        check: check
      });
    }
    else {
      if (this.descriptionCheckArray.filter(element => element.requirementId == requirementId && element.descriptionId == descriptionId).length > 0) {
        this.descriptionCheckArray.splice(this.descriptionCheckArray.findIndex(element => element.requirementId == requirementId && element.descriptionId == descriptionId), 1);
      }
    }
  }
  deleteImage(event: any, item: any) {
    item.urls.splice(item.urls.findIndex(x => x.file_name == event.file_name), 1);
  }

  fileChanged(e: any, item: any) {
    this.fileErrorMsgArr = [];
    this.campFileSpan = this.none;
    //to make sure the user select file/files
    if (!e.target.files) {
      this.filename = "No file choosen";
      return;
    }
    if (e.target.files.length > 3) {
      toastr.error("You can upload only 3 files.");
      this.resetFileInput(this.file_image);
      return;
    }
    //To obtaine a File reference
    var files = e.target.files;
    let index: number = 0;
    let fileLength: number = 0;
    fileLength = files.length;
    // this.filename=files[0].name;
    // Loop through the FileList and then to render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      //instantiate a FileReader object to read its contents into memory
      var fileReader = new FileReader();
      index = i;
      // Closure to capture the file information and apply validation.
      fileReader.onload = (function (readerEvt, urls, fileErrorMsgArr, index, fileLength) {
        return function (e) {

          let fileType = "";
          let fileSize = "";
          let alertstring = '';

          //To check file type according to upload conditions
          if (readerEvt.type == "image/jpeg" || readerEvt.type == "image/png" || readerEvt.type == "image/jpg" ||
            readerEvt.type == "image/bmp" || readerEvt.type == "image/gif" || readerEvt.type == "image/gif" || readerEvt.type == "application/pdf"
            || readerEvt.type == "audio/ogg" || readerEvt.type == "audio/mpeg" || readerEvt.type == "audio/mp3" ||
            readerEvt.type == "video/mp4" || readerEvt.type == "video/ogg" || readerEvt.type == "application/pptx" || readerEvt.type == "application/docx"
            || readerEvt.type == "application/txt" || readerEvt.type == "application/xlsx" || readerEvt.type == "csv" || readerEvt.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ||
            readerEvt.type == "application/vnd.ms-excel" || readerEvt.type == "application/msword" || readerEvt.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
            || readerEvt.type == "application/vnd.ms-powerpoint" || readerEvt.type == "text/plain") {
            fileType = "";
          }
          else {
            fileErrorMsgArr.push({ "issue": "filetype", "filename": readerEvt.name, "fileSize": 0 });
            fileType = ("<li>" + "The file(" + readerEvt.name
              + ") does not match the upload conditions, You can only upload jpeg/png/jpg/bmp/gif/pdf files" + "</li>");
          }

          //To check file Size according to upload conditions
          if (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024 <= 5) {
            fileSize = "";
          }
          else {
            fileErrorMsgArr.push({ "issue": "filesize", "filename": readerEvt.name, "fileSize": (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) });

            fileSize = ("<li>" + "The file(" + readerEvt.name
              + ") does not match the upload conditions, Your file size is: "
              + (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2)
              + " MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
          }
          if (fileType == "" && fileSize == "" && item.urls.length == 0) {
            item.urls.push({ id: index, file: readerEvt, file_path: e.target.result, file_name: readerEvt.name });
          }
          else if (fileType == "" && fileSize == "" && urls.length > 0 && urls.filter(abc => abc.file_name == readerEvt.name).length == 0) {
            item.urls.push({ id: index, file: readerEvt, file_path: e.target.result, file_name: readerEvt.name });
          }
          alertstring = alertstring + fileType + fileSize;
          if (alertstring != '') {
            this.filename = "No file chosen";//this.translate.instant('common.nofilechosen');
            //toastr.error(alertstring);
            if (index == (fileLength - 1))
              jQuery("#fileErrorMsg").click();
          }
        };
      })(f, item.urls, this.fileErrorMsgArr, index, fileLength);


      fileReader.readAsDataURL(f);

    }
    this.resetFileInput(this.file_image);

  }
  resetFileInput(file: any) {
    if (file)
      file.nativeElement.value = "";
  }
  @ViewChild('file_image', { read: ElementRef }) file_image: ElementRef;
  // ==================================
  // on CLick of Submit Requirement 
  // ==================================
  onSubmitRequirement() {

    let cartRequirementData = {

      requirements: [
        {
          id: 0,
          is_mandatory: true,
          question: "",
          question_form_id: 0,
          is_multiselect: true,
          description: [
            {
              file_path: "",
              description: ""
            }
          ]
        }
      ]
    }
    let flag = true;
    if (this.orderRequirementDetails.requirement.length > 0) {
      this.orderRequirementDetails.requirement.forEach(element => {
        if (element.question_form.id == 1 && element.is_mandatory == true && (element.ansText == "" || element.ansText == undefined || element.ansText == null)) {
          flag = false;
        }
        else if (element.question_form.id == 2 && element.is_multiselect == false && element.is_mandatory == true && (element.ansDropDown == "" ||
          element.ansDropDown == null || element.ansDropDown == undefined || element.ansDropDown == "0")) {
          flag = false;
        }
        else if (element.question_form.id == 2 && element.is_multiselect == true && element.is_mandatory == true && this.descriptionCheckArray.length <= 0) {
          flag = false;
        }
        else if (element.question_form.id == 3 && element.is_mandatory == true && element.urls.length <= 0) {
          flag = false;
        }
      }
      )
      if (flag == true) {
        let formData = new FormData();
        let count = 0;
        for (let i = 0; i < this.orderRequirementDetails.requirement.length; i++) {
          let description: Array<{ file_path: string, description: string }> = [];
          if (this.orderRequirementDetails.requirement[i].question_form.id == 1 &&
            (this.orderRequirementDetails.requirement[i].ansText != null || this.orderRequirementDetails.requirement[i].ansText != undefined
              || this.orderRequirementDetails.requirement[i].ansText != "")) {
            description.push({ file_path: null, description: this.orderRequirementDetails.requirement[i].ansText });
            cartRequirementData.requirements.push({
              id: this.orderRequirementDetails.requirement[i].id, is_mandatory: this.orderRequirementDetails.requirement[i].is_mandatory,
              question: this.orderRequirementDetails.requirement[i].question, question_form_id: this.orderRequirementDetails.requirement[i].question_form.id,
              is_multiselect: this.orderRequirementDetails.requirement[i].is_multiselect, description: description
            });
          }
          else if (this.orderRequirementDetails.requirement[i].question_form.id == 2 && this.orderRequirementDetails.requirement[i].is_multiselect == false
            && (this.orderRequirementDetails.requirement[i].ansDropDown != null || this.orderRequirementDetails.requirement[i].ansDropDown != undefined
              || this.orderRequirementDetails.requirement[i].ansDropDown != "")) {
            description.push({ file_path: null, description: this.orderRequirementDetails.requirement[i].ansDropDown });
            cartRequirementData.requirements.push({
              id: this.orderRequirementDetails.requirement[i].id, is_mandatory: this.orderRequirementDetails.requirement[i].is_mandatory,
              question: this.orderRequirementDetails.requirement[i].question, question_form_id: this.orderRequirementDetails.requirement[i].question_form.id,
              is_multiselect: this.orderRequirementDetails.requirement[i].is_multiselect, description: description
            });
          }
          else if (this.orderRequirementDetails.requirement[i].question_form.id == 2 && this.orderRequirementDetails.requirement[i].is_multiselect == true
            && this.descriptionCheckArray.length > 0) {
            let filterData = this.descriptionCheckArray.filter(element => element.requirementId == this.orderRequirementDetails.requirement[i].id);
            filterData.forEach(x => {
              description.push({ file_path: null, description: x.description });
            })
            cartRequirementData.requirements.push({
              id: this.orderRequirementDetails.requirement[i].id, is_mandatory: this.orderRequirementDetails.requirement[i].is_mandatory,
              question: this.orderRequirementDetails.requirement[i].question, question_form_id: this.orderRequirementDetails.requirement[i].question_form.id,
              is_multiselect: this.orderRequirementDetails.requirement[i].is_multiselect, description: description
            });
          }
          else if (this.orderRequirementDetails.requirement[i].question_form.id == 3 && this.orderRequirementDetails.requirement[i] != null && this.orderRequirementDetails.requirement[i].urls != undefined && this.orderRequirementDetails.requirement[i].urls != []) {
            count = count + 1;
            formData.append("requirements[" + count + "]id", this.orderRequirementDetails.requirement[i].id.toString());
            formData.append("requirements[" + count + "]is_mandatory", this.orderRequirementDetails.requirement[i].is_mandatory.toString());
            formData.append("requirements[" + count + "]question", this.orderRequirementDetails.requirement[i].question);
            formData.append("requirements[" + count + "]question_form_id", this.orderRequirementDetails.requirement[i].question_form.id.toString());
            formData.append("requirements[" + count + "]is_multiselect", this.orderRequirementDetails.requirement[i].is_multiselect.toString());
            //formData.append("requirements[" + count + "]description", description.toString());
            for (var k = 0; k < this.orderRequirementDetails.requirement[i].urls.length; k++) {
              // count = count + 1;
              // description.push({file_path:this.urls[k].file,description:""});
              // formData.append("requirements[" + count + "]description["+(k+1)+"]id", "0");
              formData.append("requirements[" + count + "]description[" + (k + 1) + "]file_path", this.orderRequirementDetails.requirement[i].urls[k].file);
              formData.append("requirements[" + count + "]description[" + (k + 1) + "]description", "");
            }
          }
        }
        cartRequirementData.requirements.splice(cartRequirementData.requirements.findIndex(x => x.question_form_id == 0), 1);

        this.progress.show();
        this.httpServices.request("patch", "orders/requirements-update/" + this.orderId, null, null, cartRequirementData).subscribe((data) => {
          this.httpServices.request("patch", "orders/requirements-update/" + this.orderId, null, null, formData).subscribe((response) => {
            toastr.success("Requirement has been saved successfully");
            // let passQueryParam = { flag: "fromCart" };
            // sessionStorage.setItem("queryParamsFromCart", JSON.stringify(passQueryParam));
            // this.router.navigate(['/buyer_orders'], {});
            this.progress.hide();
          }, (error) => {
            this.progress.hide();
          });
        }, (error) => {
          this.progress.hide();
        });
      }
      else {
        toastr.error("Please fill mandatory details");

      }
    }
    else {
      this.httpServices.request("patch", "orders/requirements-update/" + this.orderId, null, null, null).subscribe((data) => {
        toastr.success("Requirement has been saved successfully");
        // let passQueryParam = { flag: "fromCart" };
        // sessionStorage.setItem("queryParamsFromCart", JSON.stringify(passQueryParam));
        // this.router.navigate(['/buyer_orders'], {});
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
  }
  // ===============================
  // on click of Inprocess button
  // ================================
  onClickInProcess() {
    this.httpServices.request("patch", "orders/requirements-update/" + this.orderId, null, null, null).subscribe((data) => {
      toastr.success("Order is in process.");
      this.progress.hide();
      let passQueryParam = { activeTab: "in_process" };
      sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
      this.router.navigate(['/seller_orders'], {});
    }, (error) => {
      this.progress.hide();
    });
  }
  // =====================================
  // on click of submit for review button 
  // ======================================
  onClickSubMitForReview() {
    jQuery('#submit_for_review_btn_click').modal({ backdrop: 'static', keyboard: false });
  }
  public photoCopy: boolean = false;
  public block = "block";
  public photoScan: any;
  public photoCopyChat: boolean = false;
  public blockChat = "block";
  public photoScanChat: any;
  public filenameSubmitReview: string = "";
  public filenameChat: string = "";
  public submitForReviewComment: any;
  public modificationSuggestion: any;
  public fileSizeOfSubmitReview: any;
  SubmitReviewfileChanged(event: any) {
    //to make sure the user select file/files
    if (!event.target.files) {
      this.filenameSubmitReview = "No file choosen";
      return;
    }
    if (event.target.files.length == 0) {
      this.photoCopy = false;
      this.submit_reviewFile = undefined;
      this.photoScan = this.block;

      return;
    }
    this.photoScan = this.none;
    //To obtaine a File reference

    var files = event.target.files;

    var fileReader = new FileReader();

    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    // if (files[0].type == "image/jpeg" || files[0].type == "image/png" || files[0].type == "image/jpg" || files[0].type == "image/bmp" || files[0].type == "image/gif") {

    //   fileType = "";
    // }
    // else {

    //   fileType = ("<li>" + "The file (" + files[0].name + ") does not match file type." + "</li>");
    // }
    this.fileSizeOfSubmitReview = (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024);
    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "The file (" + files[0].name + ") does not match the upload conditions, Your file size is: " + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + " MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {
      this.submit_reviewFile = event.target.files;
      // if (this.submit_reviewFile.fileName != "" || this.labour_photo_data.fileName != undefined) {
      //   formData.append("labour_photo_data", this.labour_photo_data[0]);
      this.filenameSubmitReview = files[0].name;
      //   this.photoCopy = true;
      //   this.photoScan = this.none;
      // }
    }
    else {
      this.resetFileInput(this.submit_review_file);
      this.submit_reviewFile = "";
      this.submit_reviewFile = undefined;
      this.photoCopy = false;
      this.photoScan = this.block;


    }

  }
  public showFileChatInput: boolean = false;
  ChatfileChanged(event: any) {
    //to make sure the user select file/files
    if (!event.target.files) {
      //this.filenameChat = "No file choosen";
      this.showFileChatInput = false;
      return;
    }
    if (event.target.files.length == 0) {
      this.photoCopyChat = false;
      this.chat_File = undefined;
      this.photoScanChat = this.block;

      return;
    }
    this.photoScanChat = this.none;
    //To obtaine a File reference

    var files = event.target.files;

    var fileReader = new FileReader();

    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    // if (files[0].type == "image/jpeg" || files[0].type == "image/png" || files[0].type == "image/jpg" || files[0].type == "image/bmp" || files[0].type == "image/gif") {

    //   fileType = "";
    // }
    // else {

    //   fileType = ("<li>" + "The file (" + files[0].name + ") does not match file type." + "</li>");
    // }
    //this.fileSizeOfSubmitReview=(files[0].size / 1024 / 1024 / 1024 / 1024 / 1024);
    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "The file (" + files[0].name + ") does not match the upload conditions, Your file size is: " + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + " MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {
      this.chat_File = event.target.files;
      // if (this.submit_reviewFile.fileName != "" || this.labour_photo_data.fileName != undefined) {
      //   formData.append("labour_photo_data", this.labour_photo_data[0]);
      this.filenameChat = files[0].name;
      this.showFileChatInput = true;
      //   this.photoCopy = true;
      //   this.photoScan = this.none;
      // }
    }
    else {
      this.resetFileInput(this.chatFile);
      this.chat_File = "";
      this.chat_File = undefined;
      this.photoCopyChat = false;
      this.photoScanChat = this.block;


    }

  }
  removeFileChat() {
    this.showFileChatInput = false;
    this.photoCopyChat = false;
    this.chat_File = undefined;
    this.photoScanChat = this.block;
  }
  // ==============================================
  // on click of submit on submit review pop up 
  // ==============================================
  public checkSubmitReview: boolean = false;
  onSubMitReview() {
    this.checkSubmitReview = true;
    if (this.submitForReviewComment == null || this.submitForReviewComment == undefined || this.submitForReviewComment.toString().trim() == '') {

    }
    else {
      if (this.submit_reviewFile != undefined && this.submit_reviewFile != null) {
        let submitReviewBody: any = {
          order_id: 0,
          digital_download: {
            seller_id: 0,
            buyer_id: 0,
            file_size: ""
          },
          comment: {
            comment: "",
          }
        }
        let formData = new FormData();
        formData.append("filename", this.submit_reviewFile[0]);
        submitReviewBody.order_id = this.orderId;
        submitReviewBody.digital_download.seller_id = this.orderDetails.gig.created_by.id;
        submitReviewBody.digital_download.buyer_id = this.userId;
        submitReviewBody.digital_download.file_size = this.fileSizeOfSubmitReview;
        submitReviewBody.comment.comment = this.submitForReviewComment;
        this.httpServices.request("post", "orders/submit-review", null, null, submitReviewBody).subscribe((data) => {
          this.httpServices.request("patch", "orders/submit-review-attchment/" + data.order_digital_download_id, null, null, formData).subscribe((data) => {
            toastr.success("Order has been submitted for review.");
            jQuery('#submit_for_review_btn_click').modal("hide");
            this.checkSubmitReview = false;
            this.progress.hide();
            let passQueryParam = { activeTab: "delivered" };
            sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
            this.router.navigate(['/seller_orders'], {});
          }, (error) => {
            this.progress.hide();
          });
        }, (error) => {
          this.progress.hide();
        });
      }
      else {
        let submitReviewBody: any = {
          order_id: 0,
          comment: {
            comment: "",
          }
        }

        // formData.append("digital_download.seller_id",);
        submitReviewBody.order_id = this.orderId;
        submitReviewBody.comment.comment = this.submitForReviewComment;
        this.httpServices.request("post", "orders/submit-review", null, null, submitReviewBody).subscribe((data) => {
          toastr.success("Order has been submitted for review.");
          jQuery('#submit_for_review_btn_click').modal("hide");
          this.checkSubmitReview = false;
          this.progress.hide();
          let passQueryParam = { activeTab: "delivered" };
          sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
          this.router.navigate(['/seller_orders'], {});
        }, (error) => {
          this.progress.hide();
        });

      }

    }

  }
  // ==========================================
  // on click of modification required button 
  // ===========================================
  onClickModificationRequired() {
    jQuery('#modification_required_btn_click').modal({ backdrop: 'static', keyboard: false });
  }
  // ===============================================
  // on submit button click of modification pop up 
  // ===============================================
  public checkModification: boolean = false;
  onSubmitModificationPop() {
    this.checkModification = true;
    if (this.modificationSuggestion != null && this.modificationSuggestion != undefined && this.modificationSuggestion.toString().trim() != "") {
      let requiredModificationBody: any = {
        order_comment_id: 0,
        comment: ""
      }
      requiredModificationBody.order_comment_id = (this.lastCommentId == undefined || this.lastCommentId == null) ? 0 : this.lastCommentId;
      requiredModificationBody.comment = this.modificationSuggestion;
      this.progress.show();
      this.httpServices.request("post", "orders/modification-required", null, null, requiredModificationBody).subscribe((data) => {
        toastr.success("Done successfully.");
        jQuery('#modification_required_btn_click').modal("hide");
        this.checkModification = false;
        let passQueryParam = { activeTab: "in_process" };
        sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
        this.router.navigate(['/buyer_orders'], {});
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
  }

  public refund_amount: any;
  //=============================================
  // Initiate refund process
  //=============================================
  public refund_remark:any;
  initiate_refund_process() {
    if (this.refund_amount != '' && this.refund_amount != undefined) {
      if (this.refund_amount > this.orderDetails.total_amount) {
        toastr.error('Amount should not be greater than ₹ ' + this.orderDetails.total_amount + '.');
      }
      else {
        let refund_body = { 
          razorpay_payment_id: this.orderDetails.order_payment[0].razorpay_payment_no, 
          payment_amount: this.refund_amount, 
          remark:this.refund_remark
        }
        this.progress.show();
        this.httpServices.request("post", "payments/refund-payment", null, null, refund_body).subscribe((data) => {
          toastr.success('Refund has been initiated successfully.');
        });
      }
    }
    else {
      toastr.error('Please enter refund amount.');
    }
  }

  // =============================================
  // on Click of completed button
  // =============================================
  public rating: any;
  public ratingCount: any;
  public completedComment: any;
  onclickCompleted() {
    jQuery('#give_rating').modal({ backdrop: 'static', keyboard: false });
  }
  onStarClick(count: number) {

    this.ratingCount = String(count);
  }
  // ====================================================
  // on submit btn click on completed pop up 
  // =====================================================
  public checkCompleted: boolean = false;
  onSubmitCompleted() {
    this.checkCompleted = true;
    if (this.ratingCount != 0 && this.ratingCount != null && this.ratingCount != undefined && this.completedComment != null &&
      this.completedComment != undefined && this.completedComment.toString().trim() != "") {
      let submitCompletedBody: any = {
        order_id: 0,
        rating: {
          seller_id: 0,
          buyer_id: 0,
          seller_feedback: "",
          seller_rating: 0
        }
      }
      submitCompletedBody.order_id = this.orderId;
      submitCompletedBody.rating.seller_id = this.orderDetails.gig.created_by.id;
      submitCompletedBody.rating.buyer_id = this.userId;
      submitCompletedBody.rating.seller_feedback = this.completedComment;
      submitCompletedBody.rating.seller_rating = Number(this.ratingCount);
      this.progress.show();
      this.httpServices.request("post", "orders/completed", null, null, submitCompletedBody).subscribe((data) => {
        toastr.success("Order is completed.");
        jQuery('#give_rating').modal("hide");
        this.checkCompleted = false;
        let passQueryParam = { activeTab: "completed" };
        sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
        this.router.navigate(['/buyer_orders'], {});
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
  }
  // ======================================
  // on click of raise dispute button 
  // ======================================
  public raiseDisputeRemark: any;
  onClickRaiseDispute() {
    jQuery('#Remark').modal({ backdrop: 'static', keyboard: false });
  }
  // ===============================================
  // on submit btn click of raise dispute popup 
  // ===============================================
  public checkRaiseDispute: boolean = false;
  onClickRaiseDisputeRemarkSubmit() {
    this.checkRaiseDispute = true;
    if (this.raiseDisputeRemark != null && this.raiseDisputeRemark != undefined && this.raiseDisputeRemark.toString().trim() != "") {
      let raiseDisputeBody: any = {
        dispute_remark: ""
      }
      raiseDisputeBody.dispute_remark = this.raiseDisputeRemark;
      this.progress.show();
      this.httpServices.request("patch", "orders/raise-dispute/" + this.orderId, null, null, raiseDisputeBody).subscribe((data) => {
        toastr.success("Done successfully.");
        jQuery('#Remark').modal("hide");
        this.checkRaiseDispute = false;
        this.progress.hide();
        let passQueryParam = { activeTab: "dispute" };
        sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
        this.router.navigate(['/buyer_orders'], {});
      }, (error) => {
        this.progress.hide();
      });
    }
  }
  // ==================================================
  // on click of cancel btn 
  // ===================================================
  public getOrderCancellationReason: any = { id: 0, order_cancel_reason_name: '' };
  public cancelReasonList: any;
  public cancelComment: any;
  //===========================================
  // on click of cancel order btn
  // ============================================
  onCancelOrderClick() {
    this.progress.show();
    this.httpServices.request("get", "order_cancel_reason/", null, null, null).subscribe((data) => {
      this.cancelReasonList = data;
      jQuery('#cancelled_btn_click').modal({ backdrop: 'static', keyboard: false });
      this.getOrderCancellationReason.id = 0;
      this.getOrderCancellationReason.order_cancel_reason_name = '';
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }
  // ===========================================
  // on click of submit btn on cancel popup 
  // ============================================
  public checkcancel: boolean = false;
  onSubmitCancelPop() {
    this.checkcancel = true;
    if (this.getOrderCancellationReason.id == 0 || (this.getOrderCancellationReason.id == 4 && (this.cancelComment == null
      || this.cancelComment == undefined || this.cancelComment.toString().trim() == ""))) {
    }
    else {
      let orderCancelBody: any = {
        order_cancel_reason_id: 0,
        order_cancel_reason_other: ""
      }
      orderCancelBody.order_cancel_reason_id = this.getOrderCancellationReason.id;
      orderCancelBody.order_cancel_reason_other = (this.cancelComment != null && this.cancelComment != undefined) ? this.cancelComment : null;
      this.progress.show();
      this.httpServices.request("patch", "orders/cancelled/" + this.orderId, null, null, orderCancelBody).subscribe((data) => {
        jQuery('#cancelled_btn_click').modal("hide");
        toastr.success("This order has been cancelled successfully.");
        this.checkcancel = false;
        this.progress.hide();
        if (this.groupId == 2) {
          let passQueryParam = { activeTab: "cancelled" };
          sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
          this.router.navigate(['/buyer_orders'], {});
        }
        else if (this.groupId == 3) {
          let passQueryParam = { activeTab: "cancelled" };
          sessionStorage.setItem("queryParamsFromOrderDetails", JSON.stringify(passQueryParam));
          this.router.navigate(['/seller_orders'], {});
        }

      }, (error) => {
        this.progress.hide();
      });
    }
  }
  // ===========================================
  // on click of download file in delivery tab 
  // ===========================================
  onDeliveryFileClick(file: any) {
    window.open(file);
  }

  // ============================================
  // on gig title click to gig details Page
  // ============================================

  showgigDetails(gig_id: any, gig_status_id: any) {
    let passQueryParam = { id: gig_id, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], { /*queryParams: passQueryParam /*, skipLocationChange: true*/ });
  }

  onClickforRefund() {

  }

  // ============================
  // User Name to Seller Profile
  // ============================
  onSellerClick(sellerId) {
    // let passQueryParam = { userId: sellerId };
    // sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));

    let profile_mode: string;
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      } else {
        profile_mode = 'other';
      }

    } else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId;
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
}

interface orders_chat {
  results: [
    {
      id: number,
      chat: string,
      file_path: string,
      created_by: {
        id: number,
        user_name: string,
        level: {
          id: number,
          level_name: string
        },
        rating: string
      },
      created_user: string,
      created_date: string,
      file_type: string,
      file_name: string
    }
  ]
}
interface requirement_details {
  results: [
    {
      order_id: number,
      is_mandatory: boolean,
      question: string,
      question_form: {
        id: number,
        question_form_name: string
      },
      is_multiselect: boolean,
      description: [
        {
          id: number,
          file_path: string,
          description: string
        },
        {
          id: number,
          file_path: string,
          description: string
        }
      ],
      buildDescription: string
    }
  ]
}
interface orderRequirementDetails {
  requirement:
  [{
    id: number,
    gig_draft_id: string,
    gig_id: string,
    is_mandatory: boolean,
    question: string,
    question_form: {
      id: number,
      question_form_name: string
    },
    is_multiselect: boolean,
    gig_description: [
      {
        id: number,
        description: string,
        check: boolean
      }
    ]
    ansText: string,
    ansCheckBox: string,
    ansDropDown: string
    urls: any[]
  }]
}
interface order_delivery_details {
  result: [
    {
      id: number,
      seller: number,
      filename: string,
      file_size: string,
      buyer_download: boolean,
      created_date: string,
      comment: [
        {
          id: number,
          comment: string,
          created_date: string
        }
      ],
      order_complete_days_limit: number
    }
  ]
}