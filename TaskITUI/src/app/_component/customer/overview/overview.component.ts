import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { toODataString } from '@progress/kendo-data-query';
declare var toastr: any;
@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  constructor( private httpServices: HttpRequestService, private encrypt_decrypt: EncryptDecryptService,private router: Router,private progress: NgxSpinnerService) { }
  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userId: any;
  public is_email_verified:any;
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    this.userId = this.userAuthInfo.currentUser.user.id;
    this.is_email_verified=this.userAuthInfo.currentUser.user.is_verified_email_address;
  }
  // ===============================
  // Click on continue 
  // ===============================
  clickOnContinue()
  {
    this.router.navigate(['personal_info']);
  }
  reSendEmail(){
    this.progress.show();
    let userId:any={
      user_id:0
    }
    userId.user_id=  this.userId;
    this.httpServices.request('post', 'user/resend_registration_link', null, null, userId).subscribe((data) => {
      toastr.success("Link has been sent to your email address.");
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
}
