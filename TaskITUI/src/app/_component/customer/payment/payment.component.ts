import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  public payuform: any = {};
  disablePaymentButton: boolean = true;
  public payu_key:string;
  public hash_key:string;
  public transactionID:string;

  constructor() { }

  confirmPayment() {
    const paymentPayload = {
      email: this.payuform.email,
      name: this.payuform.firstname,
      phone: this.payuform.phone,
      productInfo: this.payuform.productinfo,
      amount: this.payuform.amount
    }

    //console.log(data);
    this.payuform.txnid = this.transactionID;
    this.payuform.surl = 'localhost:4200/payment';
    this.payuform.furl = 'localhost:4200/payment';
    this.payuform.key = this.payu_key;
    this.payuform.hash = this.hash_key;
    this.payuform.txnid = this.transactionID;
    this.disablePaymentButton = false;
    this.payuform.service_provider = 'payu_paisa';
  }

  ngOnInit() {
  }

}
