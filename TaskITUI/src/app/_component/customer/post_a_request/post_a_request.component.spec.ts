/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Post_a_requestComponent } from './post_a_request.component';

describe('Post_a_requestComponent', () => {
  let component: Post_a_requestComponent;
  let fixture: ComponentFixture<Post_a_requestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Post_a_requestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Post_a_requestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
