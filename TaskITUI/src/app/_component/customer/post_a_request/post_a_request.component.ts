import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { toODataString } from '@progress/kendo-data-query';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Script } from 'vm';
import { Router } from '@angular/router';

declare var toastr: any;
declare var jQuery: any;

@Component({
  selector: 'app-post_a_request',
  templateUrl: './post_a_request.component.html',
  styleUrls: ['./post_a_request.component.css']
})
export class Post_a_requestComponent implements OnInit {

  public menuCategoryList: any;
  public subCategoryId: any;

  public getCategory: any = { id: 0, category_name: "" };
  public categoryList: any;

  public getSubCategory: any = { id: 0, sub_category_name: "" };
  public subCategoryList: any;

  public getStyle: any = { id: 0, sub_category_name: "" };
  public getSubject: any = { id: 0, subject_name: "" };
  public metadata_Style_list: any;
  public showMetaDataStyleList: boolean = false;

  public serviceDescription: string;
  public serviceBudget: any;
  public serviceDelivery_time: Array<{ id: number, name: string, value: boolean, class: string }> = [];
  public user_data_source: any;
  public checkvalidation: boolean = false;
  public photoCopy: boolean = false;
  public photoScan: any;
  public block = "block";
  public none = "none";
  public upload_image: any = undefined
  public service_delivery_other: any;
  public urls: any = [];
  public filename: string = "";
  public fileErrorMsgArr: any[] = [];
  public campFileSpan: any;
  public meta_Details: any;
  public deliveryId: number;
  public serviceDelivery_timeList: service_delivery_time = {
    service: [{
      id: 0,
      seq_no: 0,
      service_delivery_name: "",
      service_delivery_value: 0,
      created_date: "",
      modified_date: "",
      is_selected: false,
      class: "",
      selected_value: 0
    }]
  };
  public _queryParams: any;
  public queryParam: any;
  public request_id: any;
  constructor(private httpServices: HttpRequestService, private router: Router, private progress: NgxSpinnerService) { }

  ngOnInit() {
    if (this.queryParam == null || this.queryParam == undefined) {
      this._queryParams = (sessionStorage.getItem("post_request_queryParams") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      this.request_id = this.queryParam.request_id;
      if (this.request_id > 0) {
        this.getRequestDetails();
      }
      else {
        this.getServicedelivery(0);
        this.getCategoryList();
      }
    }
    // this.httpServices.request('get', 'categories/subcategory-list?is_active=true', null, null, null).subscribe((data) => {
    //   this.menuCategoryList = data;
    // });
  }

  // =========================================
  // Get Service Delivery Time
  // ==========================================
  getServicedelivery(service_delivery_id: any) {
    this.httpServices.request('get', 'service_delivery/', '', '', null).subscribe((data) => {
      // this.serviceDelivery_timeList.service=data;
      //Binding data to serviceDelivery_timeList interface for delivery time btn Active class css 
      if (service_delivery_id == 0) {
        data.forEach(element => {
          this.serviceDelivery_timeList.service.push({
            id: element.id,
            seq_no: element.seq_no,
            service_delivery_name: element.service_delivery_name,
            service_delivery_value: element.service_delivery_value,
            created_date: element.created_date,
            modified_date: element.modified_date,
            class: "btn btn-primary btn-block",
            is_selected: false,
            selected_value: 0
          });
        });
      }
      else {
        data.forEach(element => {
          if (element.id == service_delivery_id) {
            this.serviceDelivery_timeList.service.push({
              id: element.id,
              seq_no: element.seq_no,
              service_delivery_name: element.service_delivery_name,
              service_delivery_value: element.service_delivery_value,
              created_date: element.created_date,
              modified_date: element.modified_date,
              class: "btn btn-primary btn-block",
              is_selected: true,
              selected_value: service_delivery_id
            });
          }
          else {
            this.serviceDelivery_timeList.service.push({
              id: element.id,
              seq_no: element.seq_no,
              service_delivery_name: element.service_delivery_name,
              service_delivery_value: element.service_delivery_value,
              created_date: element.created_date,
              modified_date: element.modified_date,
              class: "btn btn-primary btn-block",
              is_selected: false,
              selected_value: 0
            });
          }
        });
      }
      this.serviceDelivery_timeList.service.splice(this.serviceDelivery_timeList.service.findIndex(x => x.id == 0), 1);
    }, error => {
      console.log(error);
    });
  }

  public service_delivery_time_id: any;
  getRequestDetails() {
    this.httpServices.request('get', 'requests/' + this.request_id, null, null, null).subscribe((data) => {
      this.service_delivery_time_id = data.service_delivery.id;
      this.deliveryId = data.service_delivery.id;
      this.getServicedelivery(data.service_delivery.id);
      this.getCategoryList();
      this.serviceDescription = data.description;
      this.urls = data.attachments;
      this.serviceBudget = data.budget.replace('.00', '');
      this.getCategory = data.category;
      this.getSubCategory = data.sub_category;
      this.getsubCategoryList(data.category.id);
    });
  }

  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }

  //********************************
  // To get Category List Dropdown
  //********************************
  getCategoryList() {

    this.httpServices.request('get', 'categories/list', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.categoryList = data;
        }
        //toastr.success("Category List Fetched");
      }, error => {
        console.log(error);
      });
  }


  //************************************************
  //Get subcategory on the basis of category List
  //************************************************
  getsubCategoryList(categoryID: any) {
    this.progress.show();
    if (this.getCategory.id > 0) {
      this.httpServices.request('get', 'subcategories/list?category_id=' + categoryID, null, null, null)
        .subscribe(data => {
          if (data.length > 0) {
            this.subCategoryList = data;
            this.progress.hide();
          }
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    }
  }

  // *************************************
  // On sub category change 
  // **************************************
  public styleExist: boolean = false;
  onSubCategoryChange() {
    if (this.getSubCategory.id > 0) {
      this.httpServices.request('get', 'subcategories/metadata-type-details-list?sub_category_metadata_type_id=' + this.getSubCategory.id, null, null, null)
        .subscribe(data => {
          if (data.length > 0) {
            this.styleExist = true;
          }
          else {
            this.styleExist = false;
          }
          this.metadata_Style_list = data;

          if (this.metadata_Style_list != null && this.metadata_Style_list != "" && this.metadata_Style_list.length > 0) {
            this.showMetaDataStyleList = true;
          }
          else {
            this.showMetaDataStyleList = false;
          }

        }, error => {
          console.log(error);
        });
    }
  }


  // ******************************************
  // Method to set ID for Delivery Time Button
  // ******************************************

  getServiceDeliveryTime(item: any) {
    //Using for loop to traverse through Deleivery time as it has 3 values,
    //Comparing item.id with for loop traversed ID,
    //Checked with first if(this.serviceDelivery_timeList.service[i].is_selected==false) whether it is false basically its preSelected value.
    //So if is_selected==false then we r changing is_selected==true and adding Active class to it,
    //this.deliveryId=this.serviceDelivery_timeList.service[i].id; to consume deliveryId for POST request,
    //And else loop if one button selected rest button should remain unselected i.e is_selected=false

    for (let i = 0; i < this.serviceDelivery_timeList.service.length; i++) {
      if (this.serviceDelivery_timeList.service[i].id == item.id) {
        if (this.serviceDelivery_timeList.service[i].is_selected == false) {
          this.serviceDelivery_timeList.service[i].is_selected = true;
          this.serviceDelivery_timeList.service[i].class = "btn btn-primary btn-block active";
          this.deliveryId = this.serviceDelivery_timeList.service[i].id;
          this.service_delivery_other = '';
        }
        else {
          this.serviceDelivery_timeList.service[i].is_selected = false;
          this.serviceDelivery_timeList.service[i].class = "btn btn-primary btn-block";
        }
      }
      else {
        this.serviceDelivery_timeList.service[i].is_selected = false;
        this.serviceDelivery_timeList.service[i].class = "btn btn-primary btn-block";
      }
    }
  }
  // ******************************************************************
  // Method for Upload Image Button with Change and Delete Actions 
  // ****************************************************************** 

  @ViewChild('file_image', { read: ElementRef }) file_image: ElementRef;
  resetFileInput(file: any) {
    if (file)
      file.nativeElement.value = "";
  }

  fileChanged(e: any) {

    this.fileErrorMsgArr = [];
    this.campFileSpan = this.none;
    //to make sure the user select file/files
    if (!e.target.files) {
      this.filename = "No file choosen";
      return;
    }
    if (e.target.files.length + this.urls.length > 3) {
      toastr.error("You can only upload 3 images.");
      this.resetFileInput(this.file_image);
      return;
    }
    //To obtaine a File reference
    var files = e.target.files;
    let index: number = 0;
    let fileLength: number = 0;
    fileLength = files.length;
    // this.filename=files[0].name;
    // Loop through the FileList and then to render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      //instantiate a FileReader object to read its contents into memory
      var fileReader = new FileReader();
      index = i;
      // Closure to capture the file information and apply validation.
      fileReader.onload = (function (readerEvt, urls, fileErrorMsgArr, index, fileLength) {
        return function (e) {

          let fileType = "";
          let fileSize = "";
          let alertstring = '';

          //To check file type according to upload conditions
          // if (readerEvt.type == "image/jpeg" || readerEvt.type == "image/png") {
          //   fileType = "";
          // }
          if (readerEvt.type == "image/jpeg" || readerEvt.type == "image/png" || readerEvt.type == "image/jpg" || readerEvt.type == "image/bmp" || readerEvt.type == "image/gif") {
            fileType = "";
          }
          else {
            fileErrorMsgArr.push({ "issue": "filetype", "filename": readerEvt.name, "fileSize": 0 });
            fileType = ("<li>" + "The file(" + readerEvt.name
              + ") does not match the upload conditions, You can only upload jpeg/png files" + "</li>");
          }

          //To check file Size according to upload conditions
          if (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024 <= 5) {
            fileSize = "";
          }
          else {
            fileErrorMsgArr.push({ "issue": "filesize", "filename": readerEvt.name, "fileSize": (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) });

            fileSize = ("<li>" + "The file(" + readerEvt.name
              + ") does not match the upload conditions, Your file size is: "
              + (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2)
              + " MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
          }
          if (fileType == "" && fileSize == "" && urls.length == 0) {
            urls.push({ file: readerEvt, file_path: e.target.result, file_name: readerEvt.name, mode: 'new' });
          }
          else if (fileType == "" && fileSize == "" && urls.length > 0 && urls.filter(abc => abc.file_name == readerEvt.name).length == 0) {
            urls.push({ file: readerEvt, file_path: e.target.result, file_name: readerEvt.name, mode: 'new' });
          }
          alertstring = alertstring + fileType + fileSize;
          if (alertstring != '') {
            this.filename = "No file chosen";//this.translate.instant('common.nofilechosen');
            //toastr.error(alertstring);
            if (index == (fileLength - 1))
              jQuery("#fileErrorMsg").click();
          }
        };
      })(f, this.urls, this.fileErrorMsgArr, index, fileLength);

      // Read in the image file as a data URL.
      // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
      // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
      fileReader.readAsDataURL(f);
      // this.urls.forEach(element=>{
      //   this.preview( this.urls[element].fil);
      // })

      // fileReader.onload = (_event) => { 
      //   this.previewUrl = fileReader.result; 
      // }
      // this.preview(f[i]);
    }
    this.resetFileInput(this.file_image);

  }

  public galleryImageDeleteId: any;
  public galleryRecordInfo: any;
  // ================================
  // Remove attached image 
  // =================================
  deleteImage(event: any) {
    if (event.file == undefined) {
      this.galleryImageDeleteId = event.id;
      this.galleryRecordInfo = event;
      jQuery('#delete_gallery_Image').modal({ backdrop: 'static', keyboard: false });
      // this.urls.splice(this.urls.findIndex(x => x.file_name == event.file_name), 1);
    }
    else {
      this.urls.splice(this.urls.findIndex(x => x.file_name == event.file_name), 1);
    }
  }

  // ======================================
  // Remove gallery Image from database 
  // ======================================
  removegalleryImage() {
    this.progress.show();
    this.httpServices.request('delete', 'requests/attachments-delete/' + this.galleryImageDeleteId, null, null, null).subscribe((data) => {
      this.urls.splice(this.urls.findIndex(x => x.id == this.galleryRecordInfo.id), 1);
      toastr.success("Record deleted successfully.");
      jQuery('#delete_gallery_Image').modal('hide');
      this.progress.hide();
      this.galleryRecordInfo = "";
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }

  deSelectRadio(event: any) {
    if (event.target.value.length > 0) {
      this.serviceDelivery_timeList.service.forEach(x => {
        x.selected_value = 0;
        this.deliveryId = undefined;
      });
    }
  }


  // *************************************
  // On Submit Request
  // **************************************
  //   onSubmitRequest() {
  //     this.checkvalidation = true;
  //     if (this.getStyle.id != 0) {
  //       if (this.deliveryId > 0 || this.service_delivery_other != null) {
  //         if (this.urls > 0 || this.urls != null) {
  //           if (this.serviceDescription != null && this.serviceDescription != undefined && this.serviceDescription != ''
  //             && this.getCategory.id > 0 && this.urls.length > 0
  //             && this.deliveryId > 0
  //             && this.serviceBudget != null && this.serviceBudget != undefined && this.serviceBudget != '') {

  //             let postRequestdata: any =
  //             {
  //               description: '',
  //               category_id: 0,
  //               sub_category_id: 0,
  //               service_delivery_id: 0,
  //               service_delivery_other: 0,
  //               budget: '',
  //               details: [
  //                 {
  //                   sub_category_metadata_type_id: 0,
  //                   metadata_type_dtl_id: 0
  //                 }
  //               ],
  //             }

  //             postRequestdata.description = this.serviceDescription;
  //             postRequestdata.category_id = this.getCategory.id;
  //             postRequestdata.sub_category_id = this.getSubCategory.id;
  //             postRequestdata.budget = this.serviceBudget;
  //             postRequestdata.service_delivery_id = this.deliveryId;
  //             postRequestdata.service_delivery_other = this.service_delivery_other;
  //             postRequestdata.details = [];


  //             if (this.getStyle.id != 0) {
  //               if (this.metadata_Style_list.length > 0) {
  //                 this.metadata_Style_list.forEach(element => {
  //                   if (this.getStyle.id == element.metadata_type_dtl.id) {
  //                     postRequestdata.details.push({
  //                       metadata_type_dtl_id: element.metadata_type_dtl.id,
  //                       sub_category_metadata_type_id: element.sub_category_metadata_type
  //                     });
  //                   }
  //                   // postRequestdata.details.splice(postRequestdata.details.findIndex(element => element.id == 0), 1);
  //                 })
  //               }
  //             }

  //             let attachments_temp: any = {
  //               attachments: [
  //                 {
  //                   id: 0,
  //                   file_path: ""
  //                 }
  //               ]
  //             }

  //             const formData = new FormData();
  //             if (this.urls != null && this.urls != undefined && this.urls != "") {
  //               for (let i = 1; i < this.urls.length; i++) {
  //                 formData.append("attachments", attachments_temp.attachments.push({ id: i + 1, file_path: this.urls[i] }))
  //               }
  //             }
  //             this.httpServices.request('post', 'requests/create', null, null, postRequestdata)
  //               .subscribe((request_data) => {
  //                 this.httpServices.request('patch', 'requests/attachments/' + request_data.request_id, null, null, formData)
  //                   .subscribe((data) => {
  //                     toastr.success("Request saved successfully.");
  //                     this.progress.hide();
  //                   }, error => {
  //                     console.log(error);
  //                     this.progress.hide();
  //                   });
  //               }, error => {
  //                 console.log(error);
  //                 this.progress.hide();
  //               });
  //           }
  //         }
  //         else {
  //           toastr.error("Please Upload an attachment");
  //         }
  //       }
  //       else {
  //         toastr.error("Please Select a value for Delivery Time");
  //       }
  //     } else {
  //       if (this.deliveryId > 0 || this.service_delivery_other != null) {
  //         if (this.urls > 0 || this.urls != null) {
  //           if (this.serviceDescription != null && this.serviceDescription != undefined && this.serviceDescription != ''
  //             && this.getCategory.id > 0 && this.urls.length > 0
  //             && this.deliveryId > 0
  //             && this.serviceBudget != null && this.serviceBudget != undefined && this.serviceBudget != '') {


  //             let postRequestdata: any =
  //             {
  //               description: '',
  //               category_id: 0,
  //               sub_category_id: 0,
  //               service_delivery_id: 0,
  //               service_delivery_other: 0,
  //               budget: '',
  //               // details: [
  //               //   {
  //               //     sub_category_metadata_type_id: 0,
  //               //     metadata_type_dtl_id: 0
  //               //   }
  //               // ],
  //             }

  //             postRequestdata.description = this.serviceDescription,
  //               postRequestdata.category_id = this.getCategory.id,
  //               postRequestdata.sub_category_id = this.getSubCategory.id,
  //               postRequestdata.budget = this.serviceBudget;
  //             postRequestdata.service_delivery_id = this.deliveryId,
  //               postRequestdata.service_delivery_other = this.service_delivery_other


  //             let attachments_temp: any = {
  //               attachments: [
  //                 {
  //                   id: 0,
  //                   file_path: ""
  //                 }
  //               ]
  //             }

  //             const formData = new FormData();

  //             if (this.urls != null && this.urls != undefined && this.urls != "") {
  //               // for (let i = 1; i < this.urls.length; i++) {
  //               //   attachments_temp.attachments.push({ id: i + 1, file_path: this.urls[i] });
  //               // }


  //               for (var i = 0; i < this.urls.length; i++) {
  //                 formData.append("attachments[" + i + "]id", "1");
  //                 formData.append("attachments[" + i + "]file_path", this.urls[i].file);
  //                 //formData.append("gallery[" + i + "]file_path_thumbnail", this.urls[i].file);
  //               }

  //             }
  //             this.httpServices.request('post', 'requests/create', null, null, postRequestdata)
  //               .subscribe((data) => {

  //                 this.httpServices.request('patch', 'requests/attachments/' + data.id, null, null, formData)
  //                   .subscribe((data) => {

  //                     toastr.success("Request saved successfully.");
  //                     this.progress.hide();
  //                   }, error => {
  //                     console.log(error);
  //                     this.progress.hide();
  //                   });
  //               }, error => {
  //                 console.log(error);
  //                 this.progress.hide();
  //               });
  //           }
  //         }
  //         else {
  //           toastr.error("Please Upload an attachment");
  //         }
  //       }
  //       else {
  //         toastr.error("Please Select a value for Delivery Time");
  //       }
  //     }
  //   }
  // }

  public chk_DeliveryTime: boolean = false;
  onSubmitRequestBtn() {
    this.checkvalidation = true;
    if (this.deliveryId > 0 || this.service_delivery_other != null) {
      if (this.urls > 0 || this.urls != null) {
        if (this.serviceDescription != null && this.serviceDescription != undefined && this.serviceDescription != ''
          && this.getCategory.id > 0 && this.urls.length > 0
          && (this.deliveryId > 0 || this.service_delivery_other != null)
          && this.serviceBudget != null && this.serviceBudget != undefined && this.serviceBudget != '') {

          let postRequestdata: any;
          if (this.request_id > 0) {
            postRequestdata =
            {
              description: '',
              category_id: 0,
              sub_category_id: 0,
              service_delivery_id: 0,
              service_delivery_other: 0,
              budget: ''
            }
          }
          else {
            postRequestdata =
            {
              description: '',
              category_id: 0,
              sub_category_id: 0,
              service_delivery_id: 0,
              service_delivery_other: 0,
              budget: '',
              details: [
                {
                  sub_category_metadata_type_id: 0,
                  metadata_type_dtl_id: 0
                }
              ],
            }
          }

          postRequestdata.description = this.serviceDescription;
          postRequestdata.category_id = this.getCategory.id;
          postRequestdata.sub_category_id = this.getSubCategory.id;
          postRequestdata.budget = this.serviceBudget;
          //if (this.deliveryId != undefined){
          postRequestdata.service_delivery_id = this.deliveryId == undefined ? null : this.deliveryId;
          postRequestdata.service_delivery_other = this.service_delivery_other == "" ? 0 : this.service_delivery_other;
          if (this.request_id < 0 || this.request_id == undefined) {
            postRequestdata.details = [];
          }
          //postRequestdata.details = [];

          let attachments_temp: any = {
            attachments: [
              {
                id: 0,
                file_path: ""
              }
            ]
          }
          let image_date: boolean = false;
          const formData = new FormData();
          if (this.urls != null && this.urls != undefined && this.urls != "") {
            for (var i = 0; i < this.urls.length; i++) {
              if (this.urls[i].mode == 'new') {
                formData.append("attachments[" + i + "]id", "1");
                formData.append("attachments[" + i + "]file_path", this.urls[i].file);
                image_date = true;
              }
            }
          }

          if (this.request_id > 0) {
            this.progress.show();
            this.httpServices.request('patch', 'requests/' + this.request_id, null, null, postRequestdata)
              .subscribe((request_data) => {
                if (image_date) {
                  this.httpServices.request('patch', 'requests/attachments/' + this.request_id, null, null, formData)
                    .subscribe((data) => {
                      toastr.success("Request updated successfully.");
                      this.progress.hide();
                      this.router.navigate(["/manage_request"]);
                    }, error => {
                      console.log(error);
                      this.progress.hide();
                    });
                }
                else {
                  this.progress.hide();
                  this.router.navigate(["/manage_request"]);
                }
              }, error => {
                console.log(error);
                this.progress.hide();
              });
          }
          else {
            this.progress.show();
            this.httpServices.request('post', 'requests/create', null, null, postRequestdata)
              .subscribe((request_data) => {
                this.httpServices.request('patch', 'requests/attachments/' + request_data.request_id, null, null, formData)
                  .subscribe((data) => {
                    toastr.success("Request saved successfully.");
                    this.progress.hide();
                    this.router.navigate(["/manage_request"]);
                  }, error => {
                    console.log(error);
                    this.progress.hide();
                  });
              }, error => {
                console.log(error);
                this.progress.hide();
              });
          }
        }
      }
      else {
        //toastr.error("Please Upload an attachment");
      }
    }
    else {
      this.chk_DeliveryTime = true;
    }
  }
}


interface service_delivery_time {
  service: [{
    id: number,
    seq_no: number,
    service_delivery_name: string,
    service_delivery_value: number,
    created_date: string,
    modified_date: string,
    is_selected: boolean,
    class: string,
    selected_value: number
  }]
}

