/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Search_resultComponent } from './search_result.component';

describe('Search_resultComponent', () => {
  let component: Search_resultComponent;
  let fixture: ComponentFixture<Search_resultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Search_resultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Search_resultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
