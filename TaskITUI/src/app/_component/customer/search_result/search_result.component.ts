import { Component, OnInit, HostListener, EventEmitter, Output } from '@angular/core';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
declare var jQuery: any;

@Component({
  selector: 'app-search_result',
  templateUrl: './search_result.component.html',
  styleUrls: ['./search_result.component.css']
})
export class Search_resultComponent implements OnInit {

  constructor(private router: Router, private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService, private httpServices: HttpRequestService) { }
  // public categoryList:Array<{id:number,category_name:string}>=[];
  public categoryList: any;
  public sellerLevelList: any;
  public servicedeliveryList: any;
  public result: any;
  public printStar: string = '';
  public printStarEmpty: string = '';
  public showCategoryList: boolean = false;
  public categoryId: any;
  public sortById: any;
  public pageSize: any = 20;
  public minBudget: any;
  public maxBudget: any;
  public languageList: any;//Array<{ id: number, lang_name: string, value: boolean }> = [];
  public pageNumbers: any;
  public pageNumberArray: Array<{ id: number, pageNumber: number }> = [];
  public currentPageNumber: any;
  public _searchQueryParams: any;
  public searchQueryParams: any;
  public userAuthInfo: any;
  public fromGuestLogin: boolean = false;
  public deliveryTimeValue: any;
  public createdBy_userId: number;
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.sortById = 3;
    this.encrypt_decrypt.gigSearch$.subscribe(x => {
      this.getResult(x);
    });
    // this.progress.show();
    this.getCategoryList();
    this.getLanguageList();
    this.getSellerLevel();
    this.getDeliveryTimeList();
    //this.getResult();
    this.currentPageNumber = 1;
    this.encrypt_decrypt.tagFav$.subscribe(x => {
      if (x.is_login) {
        this.fromGuestLogin = true;
        this.tagFavourite(x.gig_Data, x.is_Favourite);
      }
    })
  }
  // ==========================
  // get Category List 
  // ==========================
  public all_categories_count: number = 0;
  getCategoryList() {
    // this.progress.show();
    this.httpServices.request('get', 'categories/gigs-count-list', null, null, null).subscribe((data) => {
      this.categoryList = data;
      if (data) {
        this.categoryList.forEach(x => {
          this.all_categories_count = this.all_categories_count + Number(x.gigs_count);
        })
      }
      // this.progress.hide();
    }, error => {
      // this.progress.hide();
      console.log(error);
    });
  }

  // ==========================
  // get Seller Level 
  // ==========================
  getSellerLevel() {
    // this.progress.show();
    this._searchQueryParams = (sessionStorage.getItem("searchQueryParams") || '{}');
    this.searchQueryParams = JSON.parse(this._searchQueryParams);
    let queryString: string = '';
    queryString = "?search=" + this.searchQueryParams.search_keyword;
    this.httpServices.request('get', 'service_level/seller-gigs' + queryString, null, null, null).subscribe((data) => {
      this.sellerLevelList = data;
      if (data) {
        for (let i = 0; i < this.sellerLevelList.length; i++) {
          this.sellerLevelList[i].value = false;
        }
      }
      // this.progress.hide();
    }, error => {
      // this.progress.hide();
      console.log(error);
    });
  }


  // ===========================
  // get Language list 
  // ============================
  getLanguageList() {
    // this.progress.show();
    this._searchQueryParams = (sessionStorage.getItem("searchQueryParams") || '{}');
    this.searchQueryParams = JSON.parse(this._searchQueryParams);
    let queryString: string = '';
    queryString = "?search=" + this.searchQueryParams.search_keyword;
    this.httpServices.request('get', 'languages/seller-gigs' + queryString, null, null, null).subscribe((data) => {
      this.languageList = data;
      if (data) {
        for (let i = 0; i < this.languageList.length; i++) {
          this.languageList[i].value = false;
        }
      }
      // this.progress.hide();
    }, error => {
      // this.progress.hide();
      console.log(error);
    });
  }

  // ===========================
  // get Language list 
  // ============================
  getDeliveryTimeList() {
    // this.progress.show();
    this.httpServices.request('get', 'service_delivery', null, null, null).subscribe((data) => {
      this.servicedeliveryList = data;
      if (data) {
        for (let i = 0; i < this.servicedeliveryList.length; i++) {
          this.servicedeliveryList[i].value = false;
        }
      }
      // this.progress.hide();
    }, error => {
      // this.progress.hide();
      console.log(error);
    });
  }

  // =================================
  // get data 
  // ================================
  public flag:boolean=false;
  getResult(event: any) {
    if (event.flag == true || this.result == undefined) {
      this.progress.show();
      this._searchQueryParams = (sessionStorage.getItem("searchQueryParams") || '{}');
      this.searchQueryParams = JSON.parse(this._searchQueryParams);
      let queryString: string = '';
      queryString = "&search=" + this.searchQueryParams.search_keyword;
      let URL: string = '';
      if (this.userAuthInfo.currentUser.user == undefined) {
        URL = 'home_screen/search-result?page=1&page_size=' + this.pageSize + "&sort_by=" + this.sortById;
      }
      else {
        URL = 'home_screen/active-search-result?page=1&page_size=' + this.pageSize + "&sort_by=" + this.sortById;
      }
      this.httpServices.request('get', URL + queryString, null, null, null).subscribe((data) => {
        this.result = data.results;
        if (data.results.length > 0) {
          this.createdBy_userId = data.results[0].created_by.id;
          // this.pageSize = 3;
          this.pageNumberArray.splice(0, this.pageNumberArray.length);
          if (Math.round(data.count % this.pageSize) > 0) {
            this.pageNumbers = Number(data.count / this.pageSize) + 1;
            this.pageNumbers = parseInt(this.pageNumbers);
          }
          else {
            this.pageNumbers = data.count / this.pageSize;
            this.pageNumbers = parseInt(this.pageNumbers);
          }
          for (let i = 1; i <= this.pageNumbers; i++) {
            this.pageNumberArray.push({ id: i, pageNumber: i });
          }
          this.encrypt_decrypt.changeDataGigSearch({ flag: false, location: "" });
        }
        this.progress.hide();
      }, error => {
        this.encrypt_decrypt.changeDataGigSearch({ flag: false, location: "" });
        this.progress.hide();
        console.log(error);
      });
    }
  }
  // ===============================
  // On Pagechange 
  // ================================
  onPageNumberClick(pageNumber: any) {

    this.progress.show();
    window.scrollTo(0, 0);
    this._searchQueryParams = (sessionStorage.getItem("searchQueryParams") || '{}');
    this.searchQueryParams = JSON.parse(this._searchQueryParams);
    let queryString: string = '';
    queryString = "&search=" + this.searchQueryParams.search_keyword;

    let url = "";

    if (this.userAuthInfo.currentUser.user == undefined) {
      if (this.categoryId != null && this.categoryId != undefined && this.categoryId != 0) {
        url = "home_screen/search-result?page=" + pageNumber + "&page_size=" + this.pageSize + "&category_id=" + this.categoryId;
      }
      else {
        url = "home_screen/search-result?page=" + pageNumber + "&page_size=" + this.pageSize + queryString;
      }
    }
    else {
      if (this.categoryId != null && this.categoryId != undefined && this.categoryId != 0) {
        url = "home_screen/active-search-result?page=" + pageNumber + "&page_size=" + this.pageSize + "&category_id=" + this.categoryId;
      }
      else {
        url = "home_screen/active-search-result?page=" + pageNumber + "&page_size=" + this.pageSize + queryString;
      }
    }
    let sellerLevel: string = ""; let language: string = ""; let deliveryTime: string = "";
    if (this.sellerLevelList != undefined) {
      this.sellerLevelList.forEach(x => {
        if (x.value == true) {
          sellerLevel = sellerLevel == '' ? x.id : sellerLevel + ',' + x.id;
        }
      });
    }
    if (this.languageList != undefined) {
      this.languageList.forEach(x => {
        if (x.value == true) {
          language = language == '' ? x.id : language + ',' + x.id;
        }
      });
    }
    this.servicedeliveryList.forEach(element => {
      if (element.value == true) {
        deliveryTime = deliveryTime == '' ? element.id : deliveryTime + ',' + element.id;
      }
    });

    if (sellerLevel != '') {
      url = url + "&seller_level=" + sellerLevel;
    }
    if (language != '') {
      url = url + "&seller_lang_id=" + language;
    }
    if (deliveryTime != '') {
      url = url + "&delivery_time =" + deliveryTime;
    }
    if (this.sortById != 0 && this.sortById != undefined && this.sortById != null) {
      url = url + "&sort_by=" + this.sortById;
    }
    if (this.minBudget != null && this.minBudget != undefined && this.minBudget != "") {
      url = url + "&min_price=" + this.minBudget;
    }
    if (this.maxBudget != null && this.maxBudget != undefined && this.maxBudget != "") {
      url = url + "&max_price=" + this.maxBudget;
    }
    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      this.result = data.results;
      this.currentPageNumber = pageNumber;
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }

  // ==================================
  // On Previous button click : pagination 
  // ==================================
  onPreviousPageClick() {
    if (this.currentPageNumber > 1) {
      this.currentPageNumber = this.currentPageNumber - 1;
      this.onPageNumberClick(this.currentPageNumber);
    }
  }
  // ==================================
  // On Next button click: pagination 
  // ===================================
  onNextPageClick() {
    if (this.currentPageNumber != this.pageNumbers) {
      this.currentPageNumber = this.currentPageNumber + 1;
      this.onPageNumberClick(this.currentPageNumber);
    }
  }
  // ================================
  // On click of particular gig 
  // =================================
  onGigClick(gigid: any, gig_status_id: any) {
    let passQueryParam = { id: gigid, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], {});
  }

  // ================================
  // On click of Favourite gig 
  // =================================
  tagFavourite(event: any, flag: any) {
    if (this.userAuthInfo.currentUser.user == undefined) {
      this.encrypt_decrypt.changeDataFavourite({ flag: true, location: "tagFav", is_Favourite: flag, gig_Data: event });
    }
    else {
      if (this.fromGuestLogin) {
        this.encrypt_decrypt.changeDataTagFav({
          is_login: false, is_Favourite: false
          , gig_Data: null
        });
        this.fromGuestLogin = false;
      }
      this.progress.show();
      let gigDetails = { gig_id: event.id }
      this.httpServices.request('post', 'buyers/user-favourite-gigs?is_favourite=' + flag, null, null, gigDetails).subscribe((data) => {
        //this.categoryList = data;
        this.progress.hide();
        return event.is_favourite = flag;
      }, error => {
        //this.progress.hide();
        console.log(error);
      });
    }
  }

  // ==================================
  // Display rating 
  // ==================================
  getStar(value: any) {
    if (value == null) {
      value = 0;
    }
    if (value > 5) {
      value = 5;
    }
    let printStar = '';
    for (let i = 0; i < value; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStar = printStar;
  }
  getEmptyStar() {

    let printStar = '';
    for (let i = 0; i < 5; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStarEmpty = printStar;
  }
  // ===============================
  // Open Category list 
  // ===============================
  openCategoryList() {
    this.showCategoryList = true;
    // document.getElementById("myDropdown0").classList.toggle("show");
    // document.getElementById("myDropdown2").classList.remove("show");
    // document.getElementById("myDropdown3").classList.remove("show");
    // document.getElementById("myDropdown4").classList.remove("show");


    // jQuery("#myDropdown0").css("display","block");
    jQuery("#myDropdown0").toggle("show");
    jQuery("#myDropdown2").css("display", "none");
    jQuery("#myDropdown3").css("display", "none");
    jQuery("#myDropdown4").css("display", "none");
  }

  // ===================================
  // On Seller details click 
  // =====================================
  onSellerDetailsClick() {
    // document.getElementById("myDropdown0").classList.remove("show");
    // document.getElementById("myDropdown2").classList.toggle("show");
    // document.getElementById("myDropdown3").classList.remove("show");
    // document.getElementById("myDropdown4").classList.remove("show");
    jQuery("#myDropdown0").css("display", "none");
    // jQuery("#myDropdown2").css("display","block");
    jQuery("#myDropdown2").toggle("show");
    jQuery("#myDropdown3").css("display", "none");
    jQuery("#myDropdown4").css("display", "none");

  }

  @HostListener('document:click', ['$event'])
  onDocumentClick(event) {
    if (event.target.className.indexOf('category_dropdown') >= 0) {
      if (jQuery("#myDropdown0").css('display') == 'block') {
        jQuery("#myDropdown0").css("display", "none");
      }
      else {
        jQuery("#myDropdown0").css("display", "block");
        jQuery("#myDropdown2").css("display", "none");
        jQuery("#myDropdown3").css("display", "none");
        jQuery("#myDropdown4").css("display", "none");
      }
    }
    else if (event.target.className.indexOf('Seller_Details_dropdown') >= 0) {
      if (jQuery("#myDropdown2").css('display') == 'block') {
        jQuery("#myDropdown2").css("display", "none");
      }
      else {
        jQuery("#myDropdown2").css("display", "block");
        jQuery("#myDropdown0").css("display", "none");
        jQuery("#myDropdown3").css("display", "none");
        jQuery("#myDropdown4").css("display", "none");
      }
    }
    else if (event.target.className.indexOf('Budget_dropdown') >= 0) {
      if (jQuery("#myDropdown3").css('display') == 'block') {
        jQuery("#myDropdown3").css("display", "none");
      }
      else {
        jQuery("#myDropdown3").css("display", "block");
        jQuery("#myDropdown0").css("display", "none");
        jQuery("#myDropdown2").css("display", "none");
        jQuery("#myDropdown4").css("display", "none");
      }
    }
    else if (event.target.className.indexOf('delivery_time_dropdown') >= 0) {
      if (jQuery("#myDropdown4").css('display') == 'block') {
        jQuery("#myDropdown4").css("display", "none");
      }
      else {
        jQuery("#myDropdown4").css("display", "block");
        jQuery("#myDropdown0").css("display", "none");
        jQuery("#myDropdown2").css("display", "none");
        jQuery("#myDropdown3").css("display", "none");
      }
    }
    else if (event.target.className.indexOf('drop_body') >= 0) {

    }
    else {
      jQuery("#myDropdown0").css("display", "none");
      jQuery("#myDropdown2").css("display", "none");
      jQuery("#myDropdown3").css("display", "none");
      jQuery("#myDropdown4").css("display", "none");
    }
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }



  // ===============================
  // on Click of particular category 
  // ================================
  onCategoryClick(categoryId: any) {
    this.categoryId = categoryId;
    this.search();
    jQuery("#myDropdown0").css("display", "none");
  }
  // ===============================
  // On Sort by dropdown change 
  // ===============================
  onSortByChange() {
    this.search();
  }
  public deliveryTimeSelected: any;
  onDeliveryApplyClick() {
    this.search();
    jQuery("#myDropdown4").css("display", "none");
  }

  // =================================
  // On Budget Click 
  // =================================
  onBudgetClick() {
    // document.getElementById("myDropdown0").classList.remove("show");
    // document.getElementById("myDropdown2").classList.remove("show");
    // document.getElementById("myDropdown3").classList.toggle("show");
    // document.getElementById("myDropdown4").classList.remove("show");

    jQuery("#myDropdown0").css("display", "none");
    jQuery("#myDropdown2").css("display", "none");
    // jQuery("#myDropdown3").css("display","block");
    jQuery("#myDropdown3").toggle("show");
    jQuery("#myDropdown4").css("display", "none");

  }

  // =================================
  // On Delivery Time Click 
  // =================================
  onDeliveryTime() {
    // document.getElementById("myDropdown0").classList.remove("show");
    // document.getElementById("myDropdown2").classList.remove("show");
    // document.getElementById("myDropdown3").classList.remove("show");
    // document.getElementById("myDropdown4").classList.toggle("show");
    jQuery("#myDropdown0").css("display", "none");
    jQuery("#myDropdown2").css("display", "none");
    jQuery("#myDropdown3").css("display", "none");
    // jQuery("#myDropdown4").css("display","block");
    jQuery("#myDropdown4").toggle("show");
  }

  // =================================
  // On Budget Apply click 
  // =================================
  onBudgetApplyClick() {
    this.search();
    jQuery("#myDropdown3").css("display", "none");
  }
  // ================================
  // On Budget Clear All click 
  // ================================
  onBudgetClearAllClick() {
    this.minBudget = "";
    this.maxBudget = "";
  }
  // ==================================
  // on Delivery time clear all click 
  // ==================================
  onDeliveryTimeClearAllClick() {
    this.servicedeliveryList.forEach(element => {
      element.value = false;
    });
  }
  // ==================================
  // on Seller details clear all click 
  // ==================================
  onSellerDetailsClearAllClick() {
    this.sellerLevelList.forEach(element => {
      element.value = false;
    });
    this.languageList.forEach(element => {
      element.value = false;
    });
  }
  // ===============================
  // On Seller Details Apply click 
  // ===============================
  onApplySellerDetailsClick() {
    this.search();
    jQuery("#myDropdown2").css("display", "none");
  }

  // onDeliveryTimeChange(deliveryTimeValue:any)
  // {
  //   this.deliveryTimeValue=deliveryTimeValue;
  // }
  // ============================
  // Search function 
  // ============================
  search() {
    this.progress.show();
    this._searchQueryParams = (sessionStorage.getItem("searchQueryParams") || '{}');
    this.searchQueryParams = JSON.parse(this._searchQueryParams);
    let queryString: string = '';
    queryString = "&search=" + this.searchQueryParams.search_keyword;

    let url = "";
    if (this.userAuthInfo.currentUser.user == undefined) {
      if (this.categoryId != null && this.categoryId != undefined && this.categoryId != 0) {
        url = "home_screen/search-result?page_size=" + this.pageSize + "&category_id=" + this.categoryId;
      }
      else {
        url = "home_screen/search-result?page_size=" + this.pageSize + queryString;
      }
    }
    else {
      if (this.categoryId != null && this.categoryId != undefined && this.categoryId != 0) {
        url = "home_screen/active-search-result?page_size=" + this.pageSize + "&category_id=" + this.categoryId;
      }
      else {
        url = "home_screen/active-search-result?page_size=" + this.pageSize + queryString;
      }
    }
    let sellerLevel: string = ""; let language: string = ""; let deliveryTime: string = "";
    this.sellerLevelList.forEach(x => {
      if (x.value == true) {
        sellerLevel = sellerLevel == '' ? x.id : sellerLevel + ',' + x.id;
      }
    });

    this.languageList.forEach(x => {
      if (x.value == true) {
        language = language == '' ? x.id : language + ',' + x.id;
      }
    });

    this.servicedeliveryList.forEach(element => {
      if (element.value == true) {
        deliveryTime = deliveryTime == '' ? element.id : deliveryTime + ',' + element.id;
      }
    });

    if (sellerLevel != '') {
      url = url + "&seller_level=" + sellerLevel;
    }
    if (language != '') {
      url = url + "&seller_lang_id=" + language;
    }
    if (deliveryTime != '') {
      url = url + "&delivery_time =" + deliveryTime;
    }
    if (this.sortById != 0 && this.sortById != undefined && this.sortById != null) {
      url = url + "&sort_by=" + this.sortById;
    }
    if (this.minBudget != null && this.minBudget != undefined && this.minBudget != "") {
      url = url + "&min_price=" + this.minBudget;
    }
    if (this.maxBudget != null && this.maxBudget != undefined && this.maxBudget != "") {
      url = url + "&max_price=" + this.maxBudget;
    }

    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      this.progress.hide();
      this.result = data.results;
      this.pageNumberArray.splice(0, this.pageNumberArray.length);
      if (Math.round(data.count % this.pageSize) > 0) {
        this.pageNumbers = Number(data.count / this.pageSize) + 1;
        this.pageNumbers = parseInt(this.pageNumbers);
      }
      else {
        this.pageNumbers = data.count / this.pageSize;
        this.pageNumbers = parseInt(this.pageNumbers);
      }
      for (let i = 1; i <= this.pageNumbers; i++) {
        this.pageNumberArray.push({ id: i, pageNumber: i });
      }
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }

  // ============================
  // User Name to Seller Profile
  // ============================
  onSellerClick(sellerId) {
    // let passQueryParam = { userId: sellerId };
    // sessionStorage.setItem("queryParams", JSON.stringify(passQueryParam));

    let profile_mode: string;
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == sellerId) {
        profile_mode = 'self';
      } else {
        profile_mode = 'other';
      }

    } else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = sellerId;
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
}
