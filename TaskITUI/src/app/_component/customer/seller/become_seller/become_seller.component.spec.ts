/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Become_sellerComponent } from './become_seller.component';

describe('Become_sellerComponent', () => {
  let component: Become_sellerComponent;
  let fixture: ComponentFixture<Become_sellerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Become_sellerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Become_sellerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
