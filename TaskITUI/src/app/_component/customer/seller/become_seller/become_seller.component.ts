import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService } from 'angularx-social-login';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-become_seller',
  templateUrl: './become_seller.component.html',
  styleUrls: ['./become_seller.component.css']
})
export class Become_sellerComponent implements OnInit {

  // constructor(private router: Router,) { }

  constructor(private router: Router,private progress: NgxSpinnerService, private httpServices: HttpRequestService, private authService: SocialAuthService) {
    this.loadScripts();
  }
  loadScripts() {
    const dynamicScripts = [
      'assets/js/custom.js'
    ];

    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      document.getElementsByTagName('body')[0].appendChild(node);
    }
  }

  ngOnInit() {
  }
  // ==================================
  // On 'Become a seller click' button 
  // ==================================
  becomeSellerClick()
  {
    this.router.navigate(['overview']);
  }
}
