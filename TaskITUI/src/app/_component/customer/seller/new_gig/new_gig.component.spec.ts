/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { New_gigComponent } from './new_gig.component';

describe('New_gigComponent', () => {
  let component: New_gigComponent;
  let fixture: ComponentFixture<New_gigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ New_gigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(New_gigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
