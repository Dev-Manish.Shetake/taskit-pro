import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { forEachChild } from 'typescript';
import { numberSymbols } from '@progress/kendo-angular-intl';
import { element } from 'protractor';
import { or } from '@progress/kendo-angular-grid/dist/es2015/utils';
import { Multi_autocompleteComponent } from 'src/app/_common_component/multi_autocomplete/multi_autocomplete.component';
import { identifierModuleUrl } from '@angular/compiler';
import { Router } from '@angular/router';
import { Track } from 'ngx-audio-player';
import { HttpMessageService } from '../../../../_common_services/http-message.service';

declare var toastr: any;
declare var jQuery: any;
@Component({
  selector: 'app-new_gig',
  templateUrl: './new_gig.component.html',
  styleUrls: ['./new_gig.component.css']
})
export class New_gigComponent implements OnInit {
  constructor(private httpMessage: HttpMessageService, private router: Router, private progress: NgxSpinnerService, private httpServices: HttpRequestService) { this.loadScripts(); }

  // Method to dynamically load JavaScript 
  loadScripts() {
    // This array contains all the files/CDNs 
    const dynamicScripts = [
      'assets/js/jquery.validate.min.js',
      'assets/js/jquery.steps.min.js',
      'assets/js/stepper_main.js'
    ];

    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }

  public gig_title: any;
  public searchTags: any;
  public gig_description: any;
  public getCategory: any = { id: 0, category_name: '' };
  public getSubCategory: any = { id: 0, sub_category_name: '' };
  public categoryList: any;
  public subCategoryList: any;
  public searchTagsList: any[] = [{ search_tags: 'Tag1' }, { search_tags: 'Tag2' }];

  public metadata_types_list: any;
  public metadata_Subtype_list: metaDataSubTypeList = {
    mataData:
      [{
        metaData_draft_id: 0,
        created_date: '',
        modified_date: '',
        id: 0,
        is_active: true,
        is_allowed_on_request: true,
        is_mandatory: false,
        is_single: false,
        is_suggest_other: false,
        is_suggest_other_checked_value: false,
        is_suggest_other_id: 0,
        is_suggest_other_text: '',
        max_value: 0,
        min_value: 0,
        seq_no: 0,
        metadata_type: {
          created_date: '',
          id: 0,
          is_active: true,
          is_mandatory: false,
          is_single: false,
          is_suggest_other: false,
          metadata_type_name: '',
          modified_date: ''
        },
        metadata_type_details: [{
          id: 0,
          metadata_type_dtl: {
            id: 0,
            is_active: true,
            metadata_type_name: '',
            seq_no: 0
          },
          seq_no: 0,
          checked_value: false
        }],
        sub_category: {
          category: {
            id: 0,
            category_name: '',
            is_active: true
          },
          id: 0,
          is_active: true,
          modified_date: '',
          seq_no: 0,
          sub_category_name: '',
          sub_category_picture: '',
          sub_category_thumb_picture: '',
          tagline: ''
        }
      }]
  };
  public showMetaDataTypesList: boolean = false;
  public gig_draft_Id: any;
  public gig_status_id: any;
  public valueID: number = 0;
  public metadatasubType: any;
  public suggestOther: boolean = false;
  public suggestOtherText: any;
  public showSuggestCheckbox: boolean = false;
  public is_single_metatypelist: boolean = false;
  public selectedMetadataTypeId: any;
  public video_data_source: any;
  public audio_data_source: any;
  public pdf_data_source: any;
  public gigId: any;
  public imageList: any = {
    gallery: [{
      gig_gallery_type_id: 0,
      file_path: '',
      file_path_thumbnail: ''
    }]
  };
  public video_data: any;
  public audio_data: any;
  public pdf_data: any;
  public metaSubTypeCheckBoxes: metaSubTypeCheckBoxes = {
    metadata_type_draft_details: [{
      gig_sub_category_metadata_type_draft_id: 0,
      metadata_type_dtl_id: 0
    }],
  }
  public metaTypeDetailsTemp: metaTypeDetails = {
    metatypedata:
      [{
        category_id: 0,
        sub_category_id: 0,
        metadata_type_id: 0,
        is_single: false,
        is_mandatory: false,
        min_value: 0,
        max_value: 0,
        is_suggest_other: false,
        metadata_type_draft_details: [{
          gig_sub_category_metadata_type_draft_id: 0,
          metadata_type_dtl_id: 0
        }],
        other: ''
      }]
  }
  public showGigInfo: boolean = true;
  public showGallery: boolean = false;
  public showPricing: boolean = false;
  public showFaq: boolean = false;
  public showRequirements: boolean = false;
  public showPublish: boolean = false;
  public activeTab: any = "gig_info";
  public active_metadata_type_id: number = 0;
  public queryParam: any;
  public _queryParams: any;
  public editMode: boolean = false;
  public urlString: string = "gigs/search-extender";
  public galleryTick: boolean = false;
  public gig_id_afterActiveGigSave: any;
  ngOnInit() {
    // this.showGallery = true;
    //  this.showPricing = true;
    //  this.showFaq = true;
    // this.showRequirements = true;
    // this.showPublish = true;

    if (this.queryParam == null || this.queryParam == undefined) {
      this._queryParams = (sessionStorage.getItem("queryParams") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      if (this.queryParam != null && this.queryParam != "null" && this.queryParam != undefined) {
        this.valueID = this.queryParam.id;
        this.gig_draft_Id = this.queryParam.id;
        this.gig_status_id = this.queryParam.gig_status_id;
        this.editMode = this.queryParam.flag == true ? true : false;
      }

    }
    this.getCategoryList();
    if (this.editMode == true) {
      this.galleryTick = true;
      //this.getgigInfo(this.valueID, this.gig_status_id);
      this.verifyGig();
    }
    else {
      this.searchTagsNew.search_Tags.splice(0, 1);
    }

  }
  // ====================================
  // on autocomplete change 
  // ====================================
  public searchTagsNew: searchTags = {
    search_Tags: [{
      search_id: 0,
      search_keyword: "",
      search_type: ""
    }]
  }
  public searchTagCustom: any;
  searchTagAutoCompleteChange(value: any, control: any) {

    let temparray: any[] = value;
    let oriaaray: any[] = [];

    if (temparray.length > 0) {
      temparray.forEach(x => {
        oriaaray.push({ code: x.code, description: x.colName });
      })
    }
    control.value3 = oriaaray;
    control.value5 = value;
  }
  searchtagAutocompleteTextChange(value: any) {
    this.searchTagsNew.search_Tags = value;
  }

  textChange(event: any) {

    this.searchTagCustom = event;
  }
  public autocompleteBlank: boolean = false;
  onClickPlusSearchTags(event: any) {

    this.autocompleteBlank = false;
    if (this.searchTagCustom != "" && this.searchTagCustom != undefined && this.searchTagCustom != null) {
      if (this.searchTagsNew.search_Tags.filter(x => x.search_keyword.toLowerCase() == this.searchTagCustom.toLowerCase()).length > 0) {
        toastr.error("Already added");
        this.searchTagCustom = "";
      }
      else {
        this.searchTagsNew.search_Tags.push({ search_id: 0, search_keyword: this.searchTagCustom, search_type: "" });
        this.searchTagAutoCompleteChange(this.searchTagsNew.search_Tags, this.searchTagsNew);
        this.autocompleteBlank = true;
      }
    }
  }
  // ================================
  // get gig info 
  // =================================
  public meta_data_type_draft_details_Ids: Array<{ id: number, metadatatypeId: number }> = [];
  public gig_SearchTags_Ids: Array<{ id: number }> = [];
  public verify_gig_draft_id: any;
  verifyGig() {
    if (this.gig_status_id == 5) {
      this.progress.show();
      this.httpServices.request('get', 'gigs/draft-verify/' + this.valueID, null, null, null).subscribe((verify_gig) => {
        if (verify_gig.gig_draft_id != null && verify_gig.gig_draft_id > 0) {
          this.progress.hide();
          this.verify_gig_draft_id = verify_gig.gig_draft_id;
          jQuery('#msg_after_verify_gig').modal({ backdrop: 'static', keyboard: false });
        }
        else {
          this.progress.hide();
          this.getgigInfo(this.valueID, this.gig_status_id);
        }
      });
    }
    else {
      this.getgigInfo(this.valueID, this.gig_status_id);
    }
  }
  onDiscardClick() {
    this.progress.show();
    this.httpServices.request('delete', 'gigs/draft/' + this.verify_gig_draft_id, null, null, null).subscribe((verify_gig) => {
      this.progress.hide();
      jQuery('#msg_after_verify_gig').modal("hide");
      this.getgigInfo(this.valueID, this.gig_status_id);
    });
  }
  onViewPendingClick() {
    jQuery('#msg_after_verify_gig').modal("hide");
    this.getgigInfo(this.verify_gig_draft_id, 1);
  }
  getgigInfo(id: any, gig_status_id: any) {

    this.progress.show();
    let url: any;

    if (this.activeGigSaved == true) {
      gig_status_id = 1;
    }
    if (gig_status_id == 5 || gig_status_id == 6) {

      url = 'gigs/' + id;
    }
    else {
      url = 'gigs/draft/' + id;
    }
    this.showGigInfo = true;
    this.httpServices.request('get', url, null, null, null).subscribe((data_gig_details) => {
      if (data_gig_details) {
        this.gig_title = data_gig_details.title;
        this.gig_description = data_gig_details.description;
        this.getCategory.id = data_gig_details.category.id;
        this.getCategory.category_name = data_gig_details.category.category_name;
        if (data_gig_details.metadata_type_drafts != undefined && data_gig_details.metadata_type_drafts != null) {
          for (let i = 0; i < data_gig_details.metadata_type_drafts.length; i++) {
            data_gig_details.metadata_type_drafts[i].metadata_type_draft_details.forEach(element => {
              this.meta_data_type_draft_details_Ids.push({ id: element.id, metadatatypeId: data_gig_details.metadata_type_drafts[i].metadata_type.id });
            });
          }
        }
        data_gig_details.gig_tags.forEach(element => {
          this.gig_SearchTags_Ids.push({ id: element.id })
        });


        this.getSubCategory.sub_category_name = data_gig_details.sub_category.sub_category_name;
        data_gig_details.gig_tags.forEach(x => {
          this.searchTagsNew.search_Tags.push({
            search_id: x.id,
            search_keyword: x.search_tags,
            search_type: ""
          });
        });
        this.searchTagsNew.search_Tags.splice(this.searchTagsNew.search_Tags.findIndex(x => x.search_keyword == ""), 1);
        this.httpServices.request('get', 'subcategories/?category_id=' + this.getCategory.id + "&page=1&page_size=1000", null, null, null).subscribe((data) => {
          this.subCategoryList = data.results;
          this.getSubCategory.id = data_gig_details.sub_category.id;
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });

        this.metadata_Subtype_list.mataData = null;
        this.httpServices.request('get', 'subcategories/metadata-type-list?sub_category_id=' + data_gig_details.sub_category.id, null, null, null).subscribe((data_meta_data_details) => {
          this.metadata_Subtype_list.mataData = data_meta_data_details;
          if (this.metadata_Subtype_list.mataData != null && this.metadata_Subtype_list.mataData != undefined && this.metadata_Subtype_list.mataData.length > 0) {
            this.showMetaDataTypesList = true;
          }
          else {
            this.showMetaDataTypesList = false;
          }
          if (this.gig_status_id != 5 && this.gig_status_id != 6) {
            this.metadata_Subtype_list.mataData.forEach(x => {
              if (data_gig_details.metadata_type_drafts.filter(y => y.metadata_type.id == x.metadata_type.id).length == 1) {
                let meta_data_type: any;
                meta_data_type = data_gig_details.metadata_type_drafts.filter(y => y.metadata_type.id == x.metadata_type.id)[0];
                if (meta_data_type.suggest_other != null && meta_data_type.suggest_other.length == 1) {
                  x.is_suggest_other_id = meta_data_type.suggest_other[0].id;
                  x.is_suggest_other_text = meta_data_type.suggest_other[0].other;
                  x.is_suggest_other_checked_value = true;
                }
                x.metaData_draft_id = meta_data_type.id;
                meta_data_type.metadata_type_draft_details.forEach(z => {
                  x.metadata_type_details.forEach(element => {
                    if (element.metadata_type_dtl.id == z.id) {
                      if (x.is_single == true) {
                        element.checked_value = z.id.toString();
                      }
                      else {
                        element.checked_value = true;
                      }
                    }
                    else {
                      element.checked_value = false;
                    }
                  });

                });
                // meta_data_type.metadata_type_draft_details.forEach(z => {
                //   x.metadata_type_details.forEach(k => {
                //     if (k.metadata_type_dtl.id == z.id) {
                //       if (x.is_single == true) {
                //         k.checked_value = z.id.toString();
                //       }
                //       else {
                //         k.checked_value = true;
                //       }
                //     }
                //   });
                // });
              }
            });
          }

          if (this.gig_status_id == 5 || this.gig_status_id == 6) {
            this.metadata_Subtype_list.mataData.forEach(x => {
              if (data_gig_details.metadata_type.filter(y => y.metadata_type.id == x.metadata_type.id).length == 1) {
                let meta_data_type: any;
                meta_data_type = data_gig_details.metadata_type.filter(y => y.metadata_type.id == x.metadata_type.id)[0];
                if (meta_data_type.suggest_other != null && meta_data_type.suggest_other.length == 1) {
                  x.is_suggest_other_id = meta_data_type.suggest_other[0].id;
                  x.is_suggest_other_text = meta_data_type.suggest_other[0].other;
                  x.is_suggest_other_checked_value = true;
                }
                x.metaData_draft_id = meta_data_type.id;
                meta_data_type.metadata_type_details.forEach(z => {
                  x.metadata_type_details.forEach(element => {
                    if (element.metadata_type_dtl.id == z.id) {
                      if (x.is_single == true) {
                        element.checked_value = z.id.toString();
                      }
                      else {
                        element.checked_value = true;
                      }
                    }
                  });

                });
                // meta_data_type.metadata_type_details.forEach(z => {
                //   x.metadata_type_details.forEach(k => {
                //     if (k.metadata_type_dtl.id == z.id) {
                //       k.checked_value = true;
                //     }
                //   });
                // });
              }
            });
          }
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });

        // this.httpServices.request('get', 'subcategories/?category_id=' + this.getCategory.id, null, null, null).subscribe((response) => {
        //   this.subCategoryList = response;
        //   this.getSubCategory.id = data.sub_category.id;
        //   this.progress.hide();
        // });
      }
    }, error => {
      this.progress.hide();
      console.log(error);
    });

  }
  // ========================================
  // Get Category List on Gig_info tab 
  // ========================================

  getCategoryList() {

    this.httpServices.request('get', 'categories/', null, null, null).subscribe((data) => {
      this.categoryList = data.results;

    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // ====================================
  // Get SubCategoryList on Gig_info tab 
  // ========================================
  getSubCategoryList(categoryId: any) {
    this.progress.show();
    this.httpServices.request('get', 'subcategories/?category_id=' + categoryId + "&page=1&page_size=1000", null, null, null).subscribe((data) => {
      this.subCategoryList = data.results;
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // =====================================
  // On sub category change 
  // =====================================
  onSubCategoryChange(event: any) {
    this.progress.show();
    this.subCategoryList.forEach(element => {
      if (element.id.toString() == event.target.value) {
        this.getSubCategory.id = event.target.value;
        this.getSubCategory.sub_category_name = element.sub_category_name;
      }
    });
    this.metadata_Subtype_list.mataData = null;
    this.httpServices.request('get', 'subcategories/metadata-type-list?sub_category_id=' + this.getSubCategory.id, null, null, null).subscribe((data) => {
      //this.metadata_types_list = data;
      this.metadata_Subtype_list.mataData = data;


      if (this.metadata_Subtype_list.mataData != null && this.metadata_Subtype_list.mataData != undefined && this.metadata_Subtype_list.mataData.length > 0) {
        this.showMetaDataTypesList = true;
        //this.onMetaTypeClick(this.metadata_types_list[0].id);
        this.metadata_Subtype_list.mataData.forEach(x => {
          x.metadata_type_details.forEach(element => {
            element.checked_value = false;
          });
        });
      }
      else {
        this.showMetaDataTypesList = false;
      }
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  public metaDataRadio: Array<{ metaDataTypeId: number, metaDataSubTypeId: number }> = [];
  public metaDataSuggestOther: Array<{ metaDataTypeId: number, suggestOtherCheck: boolean, suggestOtherText: string }> = [];

  // ========================================
  // On Radio btn Change 
  // =========================================
  onChangeRadio(metaDataTypeId: any, selectedValue: any, metadata_type_details: any) {
    // if (selectedValue > 0) {
    //   this.metaDataRadio = [];
    //   this.metaDataRadio.push({ metaDataTypeId: metaDataTypeId, metaDataSubTypeId: selectedValue });
    // }

    // this.metadata_Subtype_list.mataData.forEach(x => {
    //   if (x.metadata_type_details != null && x.metadata_type_details != undefined) {
    //     //this.metaDataRadio.push({ metaDataTypeId: metaDataTypeId, metaDataSubTypeId: selectedValue });
    //     if (x.metadata_type_details.filter(y => y.checked_value == true).length > 0 || this.metaDataRadio.length>0) {
    //       let str_metadata_type_draft_details: any[] = [];
    //       let str_is_suggest_other_text: any;
    //       x.metadata_type_details.forEach(element => {
    if (selectedValue > 0) {
      if (metadata_type_details != null && metadata_type_details != undefined) {
        metadata_type_details.forEach(element => {
          if (element.id == metaDataTypeId)
          //if(element.id==Number(element.checked_value))
          {
            // element.checked_value=true;
            // element.checked_value=true;
            element.checked_value=element.id.toString();
          }
          else {
            element.checked_value = "0";
          }
        });
      }
    }
  }
  // =========================================
  // On Suggest Other checkbox change 
  // ========================================
  onSuggestOtherChange(metaDataTypeId: any, checkboxValue: any, suggestOtherText: any) {
    if (checkboxValue == true) {
      this.metaDataSuggestOther = [];
      this.metaDataSuggestOther.push({ metaDataTypeId: metaDataTypeId, suggestOtherCheck: checkboxValue, suggestOtherText: suggestOtherText });
    }
  }

  // =========================================
  // On Meta data type click
  // =========================================
  onMetaTypeClick(metadataTypeId: any) {
    this.active_metadata_type_id = metadataTypeId;
    // Saving values of previous meta type
    // if(this.metaDataRadio.length>0 || this.metaDataSuggestOther.length>0)
    // {
    // }
    // let filterData=this.metadata_Subtype_list.mataData.filter(element=>element.metadata_type_dtl.metadata_type.id==this.selectedMetadataTypeId);
    // if(filterData)
    //     if (this.metadata_Subtype_list.mataData != undefined && this.metadata_Subtype_list.mataData != null) {
    //       if (this.is_single_metatypelist == true && this.metadata_Subtype_list.mataData[0].radiovalue > 0) {
    //         this.metaTypeDetailsTemp.metatypedata.push({
    //           category_id: this.getCategory.id,
    //           sub_category_id: this.getSubCategory.id,
    //           metadata_type_id: this.selectedMetadataTypeId,
    //           is_single: this.metadata_types_list.is_single,
    //           is_mandatory: this.metadata_types_list.is_mandatory,
    //           min_value: this.metadata_types_list.min_value,
    //           max_value: this.metadata_types_list.max_value,
    //           is_suggest_other: this.suggestOther,
    //           metadata_type_draft_details: [{
    //             gig_sub_category_metadata_type_draft_id: this.selectedMetadataTypeId,
    //             metadata_type_dtl_id: this.metadatasubType
    //           }],
    //           other: this.suggestOtherText
    //         })
    //       }
    //       else {
    //         this.metaTypeDetailsTemp.metatypedata.push({
    //           category_id: this.getCategory.id,
    //           sub_category_id: this.getSubCategory.id,
    //           metadata_type_id: this.selectedMetadataTypeId,
    //           is_single: this.metadata_types_list.is_single,
    //           is_mandatory: this.metadata_types_list.is_mandatory,
    //           min_value: this.metadata_types_list.min_value,
    //           max_value: this.metadata_types_list.max_value,
    //           is_suggest_other: this.suggestOther,
    //           metadata_type_draft_details: [{
    //             gig_sub_category_metadata_type_draft_id: 0,
    //             metadata_type_dtl_id: 0
    //           }],
    //           other: this.suggestOtherText
    //         })
    //         for (let i = 0; i < this.metadata_Subtype_list.mataData.length; i++) {
    //           if (this.metadata_Subtype_list.mataData[i].checkValue == true) {
    //             this.metaSubTypeCheckBoxes.metadata_type_draft_details.push({
    //               gig_sub_category_metadata_type_draft_id: this.metadata_Subtype_list.mataData[i].metadata_type_dtl.metadata_type.id,
    //               metadata_type_dtl_id: this.metadata_Subtype_list.mataData[i].metadata_type_dtl.id
    //             })
    //           }
    //         }
    //       }
    //     }

    // this.progress.show();
    // this.httpServices.request('get', 'subcategories/metadata-type-details-list?sub_category_metadata_type_id=' + metadataTypeId, null, null, null).subscribe((data) => {
    //   this.metadata_Subtype_list.mataData = data;
    //   if (this.metadata_Subtype_list.mataData.length > 0) {
    //     if (this.metadata_Subtype_list.mataData[0].metadata_type_dtl.metadata_type.is_single == false) {
    //       for (let i = 0; i < this.metadata_Subtype_list.mataData.length; i++) {
    //         this.metadata_Subtype_list.mataData[i].checkValue = false;
    //         this.metadata_Subtype_list.mataData[i].radiovalue = false;
    //       }
    //     }
    //     if (this.metadata_Subtype_list.mataData[0].metadata_type_dtl.metadata_type.is_suggest_other == true) {
    //       this.showSuggestCheckbox = true;
    //     }
    //     else {
    //       this.showSuggestCheckbox = false;
    //     }
    //     this.is_single_metatypelist = this.metadata_Subtype_list.mataData[0].metadata_type_dtl.metadata_type.is_single;
    //     this.selectedMetadataTypeId = metadataTypeId;
    //   }
    //   this.progress.hide();
    // }, error => {
    //   this.progress.hide();
    //   console.log(error);
    // });
  }
  // ==================================
  // patch gig info 
  // ===================================
  patchGigInfo() {

    let toastrmsg: string = "";

    if (this.gig_title == null || this.gig_title == undefined || this.gig_title.toString().trim() == "") {
      toastrmsg = toastrmsg + "<li>" + 'Please enter Gig Title';
    }
    if (this.searchTagsNew.search_Tags == null || this.searchTagsNew.search_Tags == undefined ||
      this.searchTagsNew.search_Tags.length < 1) {
      toastrmsg = toastrmsg + "<li>" + 'Please enter Search Tags';
    }
    if (this.gig_description == null || this.gig_description == undefined ||
      this.gig_description == "") {
      toastrmsg = toastrmsg + "<li>" + 'Please enter Gig Description';
    }
    if (this.getCategory.id == null || this.getCategory.id == undefined ||
      this.getCategory.id == 0) {
      toastrmsg = toastrmsg + "<li>" + 'Please select Category';
    }
    if (this.getSubCategory.id == null || this.getSubCategory.id == undefined ||
      this.getSubCategory.id == 0) {
      toastrmsg = toastrmsg + "<li>" + 'Please select Subcategory';
    }
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id != 5 && this.gig_status_id != 6) {
      let gig_Info: any = {
        title: '',
        sub_category_id: 0,
        category_id: 0,
        is_price_package: '',
        description: '',
        gig_tags: [{
          id: 0,
          search_tags: '',
        }],
        metadata_type_draft: [{
          id: 0,
          sub_category_id: 0,
          metadata_type_id: 0,
          is_single: false,
          is_mandatory: false,
          min_value: 0,
          max_value: 0,
          is_suggest_other: false,
          metadata_type_draft_details: [{
            id: 0,
            metadata_type_dtl_id: 0
          }],
          suggest_other: {
            id: 0,
            other: ''
          }
        }],
      }
      if (toastrmsg == "") {
        gig_Info.title = this.gig_title;
        gig_Info.sub_category_id = this.getSubCategory.id;
        gig_Info.category_id = this.getCategory.id;
        gig_Info.is_price_package = this.packageCount == true ? true : false;
        gig_Info.description = this.gig_description;
        this.searchTagsNew.search_Tags.forEach(element => {
          if (this.gig_SearchTags_Ids.filter(x => x.id == element.search_id).length > 0) {
            gig_Info.gig_tags.push({ id: element.search_id, search_tags: element.search_keyword });
          }
          else {
            gig_Info.gig_tags.push({ id: 0, search_tags: element.search_keyword });
          }
        })
        gig_Info.gig_tags.splice(gig_Info.gig_tags.findIndex(x => x.search_tags == ""), 1);
        if (this.metadata_Subtype_list.mataData != null && this.metadata_Subtype_list.mataData != undefined) {
          // let style_info = this.metadata_types_list.filter(element => element.id == 1);
          this.metadata_Subtype_list.mataData.forEach(x => {
            if (x.metadata_type_details != null && x.metadata_type_details != undefined) {
              if (x.is_single == true) {
                if (x.metadata_type_details != null && x.metadata_type_details != undefined) {
                  x.metadata_type_details.forEach(element => {
                    // if(element.id==metaDataTypeId)
                    if (element.id == Number(element.checked_value)) {
                      element.checked_value = true;
                    }
                    else {
                      element.checked_value = false;
                    }
                  });
                }
                // if (x.metadata_type_details.filter(y => y.checked_value != true && y.checked_value != false).length > 0) {
                if (x.metadata_type_details.filter(y => y.checked_value == true).length > 0) {
                  let str_metadata_type_draft_details: any[] = [];
                  let str_is_suggest_other_text: any;
                  x.metadata_type_details.forEach(element => {
                    // if (element.checked_value != true && element.checked_value != false) {
                    if (element.checked_value == true) {
                      //if (this.meta_data_type_draft_details_Ids.filter(y => y.id == element.metadata_type_dtl.id && y.metadatatypeId == x.metadata_type.id).length > 0) {
                      str_metadata_type_draft_details.push({
                        id: element.id,
                        metadata_type_dtl_id: element.metadata_type_dtl.id
                      });
                      // }
                      // else {
                      //   str_metadata_type_draft_details.push({
                      //     id: 0,
                      //     metadata_type_dtl_id: element.metadata_type_dtl.id
                      //   });
                      // }
                    }
                  });
                  gig_Info.metadata_type_draft.push({
                    id: x.metaData_draft_id,
                    sub_category_id: this.getSubCategory.id,
                    metadata_type_id: x.metadata_type.id,
                    is_single: x.is_single,
                    is_mandatory: x.is_mandatory,
                    min_value: x.min_value,
                    max_value: x.max_value,
                    is_suggest_other: (x.is_suggest_other_checked_value == undefined || x.is_suggest_other_checked_value == false) ? false : true,
                    metadata_type_draft_details: str_metadata_type_draft_details,
                    suggest_other: x.is_suggest_other_checked_value == true ? ({ other: (x.is_suggest_other_text == "" || x.is_suggest_other_text == undefined || x.is_suggest_other_text == null) ? "" : x.is_suggest_other_text }) : null
                  });
                }
              }
              else {
                if (x.metadata_type_details.filter(y => y.checked_value == true).length > 0) {
                  let str_metadata_type_draft_details: any[] = [];
                  let str_is_suggest_other_text: any;
                  x.metadata_type_details.forEach(element => {
                    if (element.checked_value == true) {
                      if (this.meta_data_type_draft_details_Ids.filter(y => y.id == element.metadata_type_dtl.id && y.metadatatypeId == x.metadata_type.id).length > 0) {
                        str_metadata_type_draft_details.push({
                          id: element.id,
                          metadata_type_dtl_id: element.metadata_type_dtl.id
                        });
                      }
                      else {
                        str_metadata_type_draft_details.push({
                          id: 0,
                          metadata_type_dtl_id: element.metadata_type_dtl.id
                        });
                      }
                    }
                  });
                  gig_Info.metadata_type_draft.push({
                    id: x.metaData_draft_id,
                    sub_category_id: this.getSubCategory.id,
                    metadata_type_id: x.metadata_type.id,
                    is_single: x.is_single,
                    is_mandatory: x.is_mandatory,
                    min_value: x.min_value,
                    max_value: x.max_value,
                    is_suggest_other: (x.is_suggest_other_checked_value == undefined || x.is_suggest_other_checked_value == false) ? false : true,
                    metadata_type_draft_details: str_metadata_type_draft_details,
                    suggest_other: x.is_suggest_other_checked_value == true ? ({ other: (x.is_suggest_other_text == "" || x.is_suggest_other_text == undefined || x.is_suggest_other_text == null) ? "" : x.is_suggest_other_text }) : null
                  });
                }
              }
            }
          });
        }
        gig_Info.metadata_type_draft.splice(gig_Info.metadata_type_draft.findIndex(x => x.sub_category_id == 0), 1);
        if (gig_Info.metadata_type_draft.filter(x => x.is_suggest_other == true && (x.suggest_other == null || x.suggest_other.other == "")).length > 0) {
          toastr.error("Please enter suggest other.");
        }
        else {
          this.progress.show();

          this.httpServices.request('patch', "gigs/draft-update/" + this.gig_draft_Id, null, null, gig_Info).subscribe((data) => {
            this.gig_draft_Id = data.id;
            this.activeTab = "gig_gallery";
            toastr.success("Record updated successfully.");
            this.showGallery = true;
            this.showGigInfo = true;
            this.getGalleryInfo();
            this.progress.hide();
          }, error => {
            this.progress.hide();
            console.log(error);
          });
        }
      }
      else {
        toastr.error(toastrmsg);
      }
    }
    else {
      let gig_active_info: any = {
        gig_id: 0,
        gig_info: {
          title: "",
          sub_category_id: 0,
          category_id: 0,
          is_price_package: true,
          description: "",
          gig_tags: [
            {
              search_tags: ""
            }
          ],
          metadata_type_draft: [
            {
              sub_category_id: 0,
              metadata_type_id: 0,
              is_single: true,
              is_mandatory: true,
              min_value: 0,
              max_value: 0,
              is_suggest_other: true,
              metadata_type_draft_details: [
                {
                  metadata_type_dtl_id: 0
                }
              ],
              suggest_other: {
                other: ""
              }
            }
          ]
        }
      }
      if (toastrmsg == "") {

        gig_active_info.gig_id = this.valueID;
        gig_active_info.gig_info.title = this.gig_title;
        gig_active_info.gig_info.sub_category_id = this.getSubCategory.id;
        gig_active_info.gig_info.category_id = this.getCategory.id;
        gig_active_info.gig_info.is_price_package = this.packageCount == true ? true : false;
        gig_active_info.gig_info.description = this.gig_description;
        this.searchTagsNew.search_Tags.forEach(element => {
          gig_active_info.gig_info.gig_tags.push({ search_tags: element.search_keyword });
        })
        gig_active_info.gig_info.gig_tags.splice(gig_active_info.gig_info.gig_tags.findIndex(x => x.search_tags == ""), 1);
        if (this.metadata_Subtype_list.mataData != null && this.metadata_Subtype_list.mataData != undefined) {
          this.metadata_Subtype_list.mataData.forEach(x => {
            if (x.metadata_type_details != null && x.metadata_type_details != undefined) {
              //this.metaDataRadio.push({ metaDataTypeId: metaDataTypeId, metaDataSubTypeId: selectedValue });
              if (x.metadata_type_details.filter(y => y.checked_value == true).length > 0) {
                let str_metadata_type_draft_details: any[] = [];
                let str_is_suggest_other_text: any;
                x.metadata_type_details.forEach(element => {
                  if (x.is_single == true) {
                    if (x.metadata_type_details != null && x.metadata_type_details != undefined) {
                      x.metadata_type_details.forEach(element => {
                        // if(element.id==metaDataTypeId)
                        if (element.id == Number(element.checked_value)) {
                          element.checked_value = true;
                        }
                        else {
                          element.checked_value = false;
                        }
                      });
                    }
                    if (element.checked_value.toString() != '') {
                      if (element.checked_value == true) {
                        str_metadata_type_draft_details.push({
                          //metadata_type_dtl_id:element.id
                          metadata_type_dtl_id: element.metadata_type_dtl.id
                        })
                      }
                    }
                  }
                  else {
                    if (element.checked_value == true) {
                      str_metadata_type_draft_details.push({
                        //metadata_type_dtl_id:element.id
                        metadata_type_dtl_id: element.metadata_type_dtl.id
                      })
                    }
                  }
                });
                // let style_info = this.metadata_types_list.filter(element => element.id == 1);
                // let style_info = this.metadata_types_list.filter(element => element.id == 1);
                gig_active_info.gig_info.metadata_type_draft.push({
                  sub_category_id: this.getSubCategory.id,
                  metadata_type_id: x.metadata_type.id,
                  is_single: x.is_single,
                  is_mandatory: x.is_mandatory,
                  min_value: x.min_value,
                  max_value: x.max_value,
                  is_suggest_other: (x.is_suggest_other_checked_value == undefined || x.is_suggest_other_checked_value == false) ? false : true,
                  metadata_type_draft_details: str_metadata_type_draft_details,
                  suggest_other: x.is_suggest_other_checked_value == true ? ({ other: (x.is_suggest_other_text == "" || x.is_suggest_other_text == undefined || x.is_suggest_other_text == null) ? "" : x.is_suggest_other_text }) : null
                });
              }
            }
          });
        }
        gig_active_info.gig_info.metadata_type_draft.splice(gig_active_info.gig_info.metadata_type_draft.findIndex(x => x.sub_category_id == 0), 1);

        if (gig_active_info.gig_info.metadata_type_draft.filter(x => x.is_suggest_other == true && (x.suggest_other == null || x.suggest_other.other == "")).length > 0) {
          toastr.error("Please enter suggest other.");
        }
        else {
          this.progress.show();
          this.httpServices.request('post', 'gigs_extra/active-gig-update', null, null, gig_active_info).subscribe((data) => {
            this.gig_draft_Id = data.id;
            this.activeGigSaved = true;
            this.gig_draft_Id = data.gig_draft_id;
            this.valueID = data.gig_draft_id;
            toastr.success("Record updated successfully");
            this.activeTab = "gig_gallery";
            this.showGallery = true;
            this.showGigInfo = true;
            this.getGalleryInfo();
            this.progress.hide();
          }, error => {
            this.progress.hide();
            console.log(error);
          });
        }
      }
      else {
        toastr.error(toastrmsg);
      }
    }
  }
  // ======================================
  // Next and continue----Gig Info 
  // ======================================
  public checkgigInfoValidation: boolean = false;
  onClickofNext_gigInfo() {
    this.checkgigInfoValidation = true;
    if (this.gig_draft_Id > 0) {
      this.patchGigInfo();
    }
    else {
      let gig_Info: any = {
        title: '',
        sub_category_id: 0,
        category_id: 0,
        is_price_package: '',
        description: '',
        gig_tags: [{
          search_tags: '',
        }],
        metadata_type_draft: [{
          sub_category_id: 0,
          metadata_type_id: 0,
          is_single: false,
          is_mandatory: false,
          min_value: 0,
          max_value: 0,
          is_suggest_other: false,
          metadata_type_draft_details: [{
            // sub_category_metadata_type_id: 0,
            metadata_type_dtl_id: 0
          }],
          suggest_other: {
            // sub_category_metadata_type_id: 0,
            other: ''
          }
        }],

      }

      let toastrmsg: string = "";

      if (this.gig_title == null || this.gig_title == undefined || this.gig_title.toString().trim() == "") {
        toastrmsg = toastrmsg + "<li>" + 'Please enter Gig Title';
      }
      if (this.searchTagsNew.search_Tags == null || this.searchTagsNew.search_Tags == undefined ||
        this.searchTagsNew.search_Tags.length < 1) {
        toastrmsg = toastrmsg + "<li>" + 'Please enter Search Tags';
      }
      if (this.gig_description == null || this.gig_description == undefined ||
        this.gig_description == "") {
        toastrmsg = toastrmsg + "<li>" + 'Please enter Gig Description';
      }
      if (this.getCategory.id == null || this.getCategory.id == undefined ||
        this.getCategory.id == 0) {
        toastrmsg = toastrmsg + "<li>" + 'Please select Category';
      }
      if (this.getSubCategory.id == null || this.getSubCategory.id == undefined ||
        this.getSubCategory.id == 0) {
        toastrmsg = toastrmsg + "<li>" + 'Please select Subcategory';
      }
      if (toastrmsg == "") {
        gig_Info.title = this.gig_title;
        gig_Info.sub_category_id = this.getSubCategory.id;
        gig_Info.category_id = this.getCategory.id;
        gig_Info.is_price_package = true;
        gig_Info.description = this.gig_description;

        this.searchTagsNew.search_Tags.forEach(element => {
          gig_Info.gig_tags.push({ search_tags: element.search_keyword });
        })
        gig_Info.gig_tags.splice(gig_Info.gig_tags.findIndex(x => x.search_tags == ""), 1);
        if (this.metadata_Subtype_list.mataData != null && this.metadata_Subtype_list.mataData != undefined) {
          // let style_info = this.metadata_types_list.filter(element => element.id == 1);
          this.metadata_Subtype_list.mataData.forEach(x => {
            if (x.metadata_type_details != null && x.metadata_type_details != undefined) {
              if (x.is_single) {
                if (x.metadata_type_details != null && x.metadata_type_details != undefined) {
                  x.metadata_type_details.forEach(element => {
                    // if(element.id==metaDataTypeId)
                    if (element.id == Number(element.checked_value)) {
                      element.checked_value = true;
                    }
                    else {
                      element.checked_value = false;
                    }
                  });
                }
                // if (x.metadata_type_details.filter(y => y.checked_value != true && y.checked_value != false).length > 0) {
                if (x.metadata_type_details.filter(y => y.checked_value == true).length > 0) {
                  let str_metadata_type_draft_details: any[] = [];
                  let str_is_suggest_other_text: any;
                  x.metadata_type_details.forEach(element => {
                    // if (element.checked_value != true && element.checked_value != false) {
                    //   str_metadata_type_draft_details.push({
                    //     //metadata_type_dtl_id:element.id
                    //     metadata_type_dtl_id: element.metadata_type_dtl.id
                    //   })
                    // }
                    if (element.checked_value == true) {
                      str_metadata_type_draft_details.push({
                        metadata_type_dtl_id: element.metadata_type_dtl.id
                      })
                    }
                  });
                  gig_Info.metadata_type_draft.push({
                    sub_category_id: this.getSubCategory.id,
                    metadata_type_id: x.metadata_type.id,
                    is_single: x.is_single,
                    is_mandatory: x.is_mandatory,
                    min_value: x.min_value,
                    max_value: x.max_value,
                    is_suggest_other: (x.is_suggest_other_checked_value == undefined || x.is_suggest_other_checked_value == false) ? false : true,
                    metadata_type_draft_details: str_metadata_type_draft_details,
                    suggest_other: x.is_suggest_other_checked_value == true ? ({ other: (x.is_suggest_other_text == "" || x.is_suggest_other_text == undefined || x.is_suggest_other_text == null) ? "" : x.is_suggest_other_text }) : null
                  });
                }
              }
              else {
                if (x.metadata_type_details.filter(y => y.checked_value == true).length > 0) {
                  let str_metadata_type_draft_details: any[] = [];
                  let str_is_suggest_other_text: any;
                  x.metadata_type_details.forEach(element => {
                    if (element.checked_value == true) {
                      str_metadata_type_draft_details.push({
                        //metadata_type_dtl_id:element.id
                        metadata_type_dtl_id: element.metadata_type_dtl.id
                      })
                    }
                  });
                  gig_Info.metadata_type_draft.push({
                    sub_category_id: this.getSubCategory.id,
                    metadata_type_id: x.metadata_type.id,
                    is_single: x.is_single,
                    is_mandatory: x.is_mandatory,
                    min_value: x.min_value,
                    max_value: x.max_value,
                    is_suggest_other: (x.is_suggest_other_checked_value == undefined || x.is_suggest_other_checked_value == false) ? false : true,
                    metadata_type_draft_details: str_metadata_type_draft_details,
                    suggest_other: x.is_suggest_other_checked_value == true ? ({ other: (x.is_suggest_other_text == "" || x.is_suggest_other_text == undefined || x.is_suggest_other_text == null) ? "" : x.is_suggest_other_text }) : null
                  });
                }
              }
            }
          });
        }
        gig_Info.metadata_type_draft.splice(gig_Info.metadata_type_draft.findIndex(x => x.sub_category_id == 0), 1);
        // gig_Info.metadata_type_draft.forEach(element => {
        //   if(element.suggest_other!=null && element.suggest_other!=undefined)
        //   {
        //     if(gig_Info.metadata_type_draft.filter(x=>x.is_suggest_other==true && x.suggest_other.other=="").length>0)
        //     {
        //       toastr.error("Please enter suggest other.");
        //     }
        //   }

        // });
        if (gig_Info.metadata_type_draft.filter(x => x.is_suggest_other == true && (x.suggest_other == null || x.suggest_other.other == "")).length > 0) {
          toastr.error("Please enter suggest other.");
        }
        else {
          this.progress.show();
          this.httpServices.request('post', 'gigs/draft', null, null, gig_Info).subscribe((data) => {
            this.gig_draft_Id = data.id;
            this.valueID = data.id;
            toastr.success("Record saved successfully");
            this.activeTab = "gig_gallery";
            this.showGigInfo = true;
            this.showGallery = true;
            this.getGalleryInfo();
            this.progress.hide();
          }, error => {
            this.progress.hide();
            console.log(error);
          });
        }
      }
      else {
        toastr.error(toastrmsg);

      }

    }

  }
  onChangeSuggestOther(is_suggest_other_checked_value: boolean, itemData: any) {
    if (is_suggest_other_checked_value == false) {
      itemData.is_suggest_other_text = "";
    }
  }
  public galleryData: galleryData = {
    galleryInfo: [{
      id: 0,
      gig_gallery_type: {
        id: 0,
        gig_gallery_type_name: ""
      },
      file_path: "",
      file_path_thumbnail: ""
    }]
  }
  // =====================================
  // get gallery Info 
  // =======================================
  public galleryAlreadyPresent: boolean = false;

  msaapDisplayTitle = false;
  msaapDisplayVolumeControls = false;
  msbapAudioUrl = "";
  msbapTitle = "";

  getGalleryInfo() {
    this.progress.show();
    let url: any;
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id == 5 || this.gig_status_id == 6) {
      url = 'gigs/gallery-list/' + this.valueID;
    }
    else {
      url = 'gigs/draft-gallery-list/' + this.valueID;
    }
    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      // if(data!=null){
      //   data.forEach(element => {
      //     if(element.file_path!=""){
      //       element.file_path = element.file_path.replace("127.0.0.1:8000","192.168.3.221:3001")
      //     }
      //     if(element.file_path_thumbnail!=""){
      //       element.file_path_thumbnail = element.file_path_thumbnail.replace("127.0.0.1:8000","192.168.3.221:3001")
      //     }
      //   });
      // }
      this.urls = []; this.video_data = []; this.pdf_data = []; this.audio_data = [];
      this.pdf_data_source = undefined;
      this.galleryData.galleryInfo = [{
        id: 0,
        gig_gallery_type: {
          id: 0,
          gig_gallery_type_name: ""
        },
        file_path: "",
        file_path_thumbnail: ""
      }];
      if (data.filter(x => x.gig_gallery_type.id == 1).length > 0) {
        this.urls = data.filter(element => element.gig_gallery_type.id == 1);
      }
      if (data.filter(x => x.gig_gallery_type.id == 2).length > 0) {
        this.video_data = data.filter(element => element.gig_gallery_type.id == 2);
      }
      if (data.filter(x => x.gig_gallery_type.id == 3).length > 0) {
        this.pdf_data = data.filter(element => element.gig_gallery_type.id == 3);
      }
      if (data.filter(x => x.gig_gallery_type.id == 4).length > 0) {
        this.audio_data = data.filter(element => element.gig_gallery_type.id == 4);
        this.msbapAudioUrl = this.audio_data[0].file_path;
      }
      if (data != null && data != undefined && data != [] && data.length > 0) {
        this.galleryAlreadyPresent = true;
      }
      else {
        this.galleryAlreadyPresent = false;
      }
      data.forEach(element => {
        let gig_galleryType: any = {
          id: 0,
          gig_gallery_type_name: ""
        }
        // gig_galleryType.push({ id: element.gig_gallery_type.id, gig_gallery_type_name: element.gig_gallery_type.gig_gallery_type_name });
        gig_galleryType.id = element.gig_gallery_type.id;
        gig_galleryType.gig_gallery_type_name = element.gig_gallery_type.gig_gallery_type_name;
        this.galleryData.galleryInfo.push({
          id: element.id,
          gig_gallery_type: gig_galleryType,
          file_path: element.file_path,
          file_path_thumbnail: element.file_path_thumbnail
        })
      });
      this.galleryData.galleryInfo.splice(this.galleryData.galleryInfo.findIndex(x => x.id == 0), 1);
      this.progress.hide();
    });
  }


  extraServiceChange(item: any) {
    // if(item.title=='Extra fast delivery')
    // {
    //   this.priceScope.package.forEach(element => {
    //     if (element.price_scope_name == "Delivery Time") {
    //       if (this.extraFast_silver_deliver) {
    //         this.extraFast_silver_deliver = 0;
    //         toastr.error("Extra Fast delivery time should be less than Basic delivery time.");
    //       }
    //     }
    //   }
    //   )

    // }
    item.extra_days = 0
    if (item.is_selected == false) {
      item.service_days = 0;
      item.service_price = '';
      item.extra_days = this.daysListForExtraGig;
      return item;
    }
  }

  // ==========================================
  // gallery patch 
  // ==========================================
  public appended: boolean = false;
  GalleryUpdate() {
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id != 5 && this.gig_status_id != 6) {
      let galleryBody: any = {
        galleryArray: [{
          id: 0,
          gig_gallery_type_id: 0,
          file_path: "",
          file_path_thumbnail: ""
        }]

      }
      // this.galleryData.galleryInfo.forEach(element=>
      //   galleryBody.galleryArray.push({id:element.id,gig_gallery_type_id:element.gig_gallery_type.id,file_path:element.file_path,file_path_thumbnail:element.file_path_thumbnail})
      //   )
      const formData = new FormData();
      let count = 0;
      if (this.urls != null && this.urls != undefined && this.urls.length > 0) {
        for (var i = 0; i < this.urls.length; i++) {
          if (this.urls[i].file != undefined) {
            if (this.urls[i].file_mode == 'new') {
              formData.append("gallery[" + i + "]id", "0");
            }
            else {
              formData.append("gallery[" + i + "]id", this.urls[i].id.toString());
              // formData.append("gallery[" + i + "]gig_gallery_type_id", null);
            }
            formData.append("gallery[" + i + "]gig_gallery_type_id", "1");
            formData.append("gallery[" + i + "]file_path", this.urls[i].file);
            formData.append("gallery[" + i + "]file_path_thumbnail", this.urls[i].file);
            this.appended = true;
            count = i + 1;
          }

        }
      }
      if (this.video_data != null && this.video_data != undefined && this.video_data != '') {

        // if (this.video_data[0].id != undefined && this.video_data[0].id != null && this.video_data[0].id != 0) {
        //   formData.append("gallery[" + count + "]id", this.video_data[0].id);
        // }
        // else {
        //   formData.append("gallery[" + count + "]id", "0");
        // }
        if (this.video_data[0].file != undefined && this.video_data[0].file != null) {
          formData.append("gallery[" + count + "]id", "0");
          formData.append("gallery[" + count + "]gig_gallery_type_id", "2");
          formData.append("gallery[" + count + "]file_path", this.video_data[0]);
          formData.append("gallery[" + count + "]file_path_thumbnail", this.video_data[0]);
          this.appended = true;
          count = count + 1;
        }

      }
      if (this.audio_data != null && this.audio_data != undefined && this.audio_data != '') {
        if (count > 0)
          count = count + 1;
        // if (this.audio_data[0].id != undefined && this.audio_data[0].id != null && this.audio_data[0].id != 0) {
        //   formData.append("gallery[" + count + "]id", this.audio_data[0].id);
        //   formData.append("gallery[" + i + "]gig_gallery_type_id", null);
        // }
        // else {
        //   formData.append("gallery[" + count + "]id", "0");
        // }
        if (this.audio_data[0].file != undefined && this.audio_data[0].file != null) {
          formData.append("gallery[" + count + "]id", "0");
          // formData.append("gallery[" + i + "]gig_gallery_type_id", null);
          formData.append("gallery[" + count + "]gig_gallery_type_id", "4");
          formData.append("gallery[" + count + "]file_path", this.audio_data[0]);
          formData.append("gallery[" + count + "]file_path_thumbnail", this.audio_data[0]);
        }
        if (count == 0) {
          count = count + 1;
        }
      }
      if (this.pdf_data != null && this.pdf_data != undefined && this.pdf_data != '') {
        if (count > 0)
          count = count + 1;
        // if (this.pdf_data[0].id > 0) {
        //   formData.append("gallery[" + count + "]id", this.pdf_data[0].id);
        // }
        // else {
        //   formData.append("gallery[" + count + "]id", "0");
        // }
        if (this.pdf_data[0].file != null && this.pdf_data[0].file != undefined) {
          formData.append("gallery[" + count + "]id", "0");
          formData.append("gallery[" + count + "]gig_gallery_type_id", "3");
          formData.append("gallery[" + count + "]file_path", this.pdf_data[0]);
          formData.append("gallery[" + count + "]file_path_thumbnail", this.pdf_data[0]);
        }
        if (count == 0) {
          count = count + 1;
        }
      }
      if (count > 0) {
        this.progress.show();
        this.httpServices.request('patch', "gigs/draft-gallery-update/" + this.gig_draft_Id, null, null, formData).subscribe((data) => {
          toastr.success("Records updated successfully.");
          this.activeTab = "gig_pricing";
          this.showPricing = true;
          this.showGigInfo = true;
          this.showGallery = true;
          this.pdf_data_source = undefined;
          if (this.valueID == 0) {
            this.getPackagesList();
            this.getExtraServicesList();
          }
          else {
            this.getPriceTabDetails(this.valueID);
          }
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });
      }
      else if (this.urls.length > 0 || this.video_data.length > 0 || this.audio_data.length > 0 || this.pdf_data.length > 0) {
        this.activeTab = "gig_pricing";
        this.showPricing = true;
        this.showGigInfo = true;
        this.showGallery = true;
        this.pdf_data_source = undefined;
        if (this.valueID == 0) {
          this.getPackagesList();
          this.getExtraServicesList();
        }
        else {
          this.getPriceTabDetails(this.valueID);
        }
      }
      else {
        this.progress.hide();
        toastr.error("Please provide atleast one media.");
      }
    }
    else {
      let galleryBody: any = {
        gig_id: 0,
        gallery: [
          {
            id: 0,
            gig_gallery_type_id: 0,
            gig_gallery_type: 0
          }
        ]
      }
      const formData = new FormData();
      let count = 0;
      if (this.urls != null && this.urls != undefined && this.urls.length > 0) {
        for (var i = 0; i < this.urls.length; i++) {
          if (this.urls[i].id > 0) {
            formData.append("gallery[" + i + "]id", this.urls[i].id.toString());
          }
          else {
            formData.append("gallery[" + i + "]id", "0");
            formData.append("gallery[" + i + "]gig_gallery_type_id", "1");
            formData.append("gallery[" + i + "]file_path", this.urls[i].file);
            formData.append("gallery[" + i + "]file_path_thumbnail", this.urls[i].file);
          }

          count = i + 1;
        }
      }
      if (this.video_data != null && this.video_data != undefined && this.video_data != '') {

        if (this.video_data[0].id != undefined && this.video_data[0].id != null && this.video_data[0].id != 0) {
          formData.append("gallery[" + count + "]id", this.video_data[0].id);
        }
        else {
          formData.append("gallery[" + count + "]id", "0");
          formData.append("gallery[" + count + "]gig_gallery_type_id", "2");
          formData.append("gallery[" + count + "]file_path", this.video_data[0]);
          formData.append("gallery[" + count + "]file_path_thumbnail", this.video_data[0]);
        }
        count = count + 1;
      }

      if (this.audio_data != null && this.audio_data != undefined && this.audio_data != '') {

        if (this.audio_data[0].id != undefined && this.audio_data[0].id != null && this.audio_data[0].id != 0) {
          formData.append("gallery[" + count + "]id", this.audio_data[0].id);
        }
        else {
          formData.append("gallery[" + count + "]id", "0");
          formData.append("gallery[" + count + "]gig_gallery_type_id", "4");
          formData.append("gallery[" + count + "]file_path", this.audio_data[0]);
          formData.append("gallery[" + count + "]file_path_thumbnail", this.audio_data[0]);
        }
        count = count + 1;
      }
      if (this.pdf_data != null && this.pdf_data != undefined && this.pdf_data != '') {
        if (this.pdf_data[0].id != undefined && this.pdf_data[0].id != null && this.pdf_data[0].id != 0) {
          if (this.pdf_data[0].id > 0) {
            formData.append("gallery[" + count + "]id", this.pdf_data[0].id);
          }
          else {
            formData.append("gallery[" + count + "]id", "0");
            formData.append("gallery[" + count + "]gig_gallery_type_id", "3");
            formData.append("gallery[" + count + "]file_path", this.pdf_data[0]);
            formData.append("gallery[" + count + "]file_path_thumbnail", this.pdf_data[0]);
          }
          count = count + 1;
        }
      }

      formData.append("gig_id", this.valueID.toString());
      if (count > 0) {
        this.progress.show();
        this.httpServices.request('post', 'gigs_extra/active-gig-update', null, null, formData).subscribe((data) => {
          toastr.success("Records updated successfully.");
          this.activeTab = "gig_pricing";
          this.activeGigSaved = true;
          this.gig_draft_Id = data.gig_draft_id;
          this.valueID = data.gig_draft_id;
          this.showPricing = true;
          this.showGigInfo = true;
          this.showGallery = true;
          if (this.valueID == 0) {
            this.getPackagesList();
            this.getExtraServicesList();
          }
          else {
            this.getPriceTabDetails(this.valueID);
          }
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });
      }
      else if (this.urls.length > 0 || this.video_data.length > 0 || this.audio_data.length > 0 || this.pdf_data.length > 0) {
        this.activeTab = "gig_pricing";
        this.showPricing = true;
        this.showGigInfo = true;
        this.showGallery = true;
        this.pdf_data_source = undefined;
        if (this.valueID == 0) {
          this.getPackagesList();
          this.getExtraServicesList();
        }
        else {
          this.getPriceTabDetails(this.valueID);
        }
      }
      else {
        this.progress.hide();
        toastr.error("Please provide atleast one media.");
      }
    }
  }
  // =====================================
  // get requirement info 
  // ======================================
  public requirementAlreadyExistIds: Array<{ id: number }> = [];
  public requirementListAlreadyPresent: boolean = false;
  getRequirementInfo() {
    this.progress.show();
    let url: any;
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id == 5 || this.gig_status_id == 6) {
      url = 'gigs/requirements-list/' + this.valueID;
    }
    else {
      url = 'gigs/draft-requirements-list/' + this.valueID;
    }
    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      if (data != null && data != undefined) {
        this.requirementAlreadyExistIds = [];
        data.forEach(element => {
          this.requirementAlreadyExistIds.push({ id: element.id });
        });
        this.showNewRequirementSection = false;
        this.requirementList.splice(0, this.requirementList.length);
        this.requirement_option_list_temp.splice(0, this.requirement_option_list_temp.length);
        data.forEach(element => {
          let descriptions: any = [
            {
              id: 0,
              description: ""
            },
          ]
          if (element.descriptions != null && element.descriptions != undefined && element.descriptions != "" && element.descriptions.length > 0) {
            // element.descriptions.forEach(element => {
            //   descriptions.push({id:element.id,description:element.description});
            //   this.requirement_option_list_temp.push({id:element,requirement_id:element.id,optionText:element.description});
            // });
            for (let i = 0; i < element.descriptions.length; i++) {
              descriptions.push({ id: element.descriptions[i].id, description: element.descriptions[i].description });
              this.requirement_option_list_temp.push({ id: element.descriptions[i].id, requirement_id: element.id, optionText: element.descriptions[i].description });
            }
          }
          descriptions.splice(descriptions.findIndex(x => x.id == 0), 1);
          this.requirementList.push({ id: element.id, is_mandatory: element.is_mandatory, question: element.question, question_form: element.question_form.id, is_multiselect: element.is_multiselect, description: descriptions });

        });
        this.requirementListAlreadyPresent = true;

      }
      if (data.length > 0) {
        this.showNewRequirementSection = false;
      }
      else {
        this.showNewRequirementSection = true;
        this.requirement_id = 0;
      }
      this.progress.hide();
    });
  }

  // =====================================
  // Tabs handling 
  // =====================================
  onClickGigInfo() {
    this.activeTab = "gig_info";
    if (this.valueID == 0) {
    }
    else {
      this.showGigInfo = true;
      this.showGallery = false;
      this.showPricing = false;
      this.showFaq = false;
      this.showRequirements = false;
      this.showPublish = false;
    }
  }
  onClickGallery() {
    this.activeTab = "gig_gallery";
    if (this.valueID == 0) {
    }
    else {
      this.showGigInfo = true;
      this.showGallery = true;
      this.showPricing = false;
      this.showFaq = false;
      this.showRequirements = false;
      this.showPublish = false;
      this.getGalleryInfo();
    }
  }
  onClickPricing() {
    this.activeTab = "gig_pricing";
    if (this.valueID == 0) {
      // this.getPackagesList();
      // this.getExtraServicesList();
    }
    else {
      this.showGigInfo = true;
      this.showGallery = true;
      this.showPricing = true;
      this.showFaq = false;
      this.showRequirements = false;
      this.showPublish = false;
      this.getPriceTabDetails(this.valueID);
    }
  }

  onClickFAQ() {
    this.activeTab = "gig_faq";
    if (this.valueID == 0) {
    }
    else {
      this.showGigInfo = true;
      this.showGallery = true;
      this.showPricing = true;
      this.showFaq = true;
      this.showRequirements = false;
      this.showPublish = false;
      this.getFaqInfo();
    }
  }
  onClickRequirement() {
    this.activeTab = "gig_requirement";
    if (this.valueID == 0) {
    }
    else {
      this.showGigInfo = true;
      this.showGallery = true;
      this.showPricing = true;
      this.showFaq = true;
      this.showRequirements = true;
      this.showPublish = false;
      this.getRequirementInfo();
    }
  }
  onClickPublish() {
    this.activeTab = "gig_publish";
    if (this.valueID == 0) {
    }
    else {
      this.showGigInfo = true;
      this.showGallery = true;
      this.showPricing = true;
      this.showFaq = true;
      this.showRequirements = true;
      this.showPublish = true;
    }
  }

  public pricingListAlreadyPresent: boolean = false;
  public pricingListExtraServiceAlreadyPresent: boolean = false;
  getPriceTabDetails(id: any) {
    this.progress.show();
    let url_pricing: any;
    let url_extra_pricing: any;
    let url_custom_extra_pricing: any;
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id == 5 || this.gig_status_id == 6) {
      url_pricing = 'gigs/pricing-details/' + this.valueID;
      url_extra_pricing = 'gigs/pricing-extra-services/' + this.valueID;
      url_custom_extra_pricing = 'gigs/pricing-extra-custom-services/' + this.valueID;
    }
    else {
      url_pricing = 'gigs/draft-pricing-details/' + this.valueID;
      url_extra_pricing = 'gigs/draft-pricing-extra-services/' + this.valueID;
      url_custom_extra_pricing = 'gigs/draft-pricing-extra-custom-services/' + this.valueID;
    }
    this.httpServices.request("get", "subcategories/price-scope?sub_category_id=" + this.getSubCategory.id, null, null, null).subscribe((dataPriceScope) => {
      if (dataPriceScope != null && dataPriceScope != undefined && dataPriceScope != '') {
        this.httpServices.request("get", url_pricing, null, null, null).subscribe((priceDetails) => {
          this.priceScope.package = dataPriceScope;
          if (priceDetails != null && priceDetails != undefined && priceDetails != '') {
            let pricing_details: any;
            pricing_details = priceDetails;
            if (pricing_details.length > 0) {
              this.pricingListAlreadyPresent = true;
            }
            if (pricing_details.length > 1) {
              this.packageCount = true;
            }
            for (var j = 0; j < pricing_details.length; j++) {
              if (pricing_details[j].price_type.id == 1) {
                pricing_details[j].price_details.forEach(strpriceDetails => {
                  for (var i = 0; i < this.priceScope.package.length; i++) {
                    if (this.priceScope.package[i].id == strpriceDetails.sub_category_price_scope.id) {
                      if (this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) {
                        this.priceScope.package[i].txtSilver = strpriceDetails.value;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_silver = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_silver = pricing_details[j].id;
                        break;
                      }
                      else if (this.priceScope.package[i].price_scope_control_type.id == 2) {
                        this.priceScope.package[i].dropdownSilver = strpriceDetails.sub_category_price_scope_dtl.id;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_silver = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_silver = pricing_details[j].id;
                        break;
                      }
                      else if (this.priceScope.package[i].price_scope_control_type.id == 3) {
                        this.priceScope.package[i].checkBoxSilver = strpriceDetails.value == "true" ? true : false;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_silver = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_silver = pricing_details[j].id;
                        break;
                      }
                    }
                  }
                });
              }
              else if (pricing_details[j].price_type.id == 2) {
                pricing_details[j].price_details.forEach(strpriceDetails => {
                  for (var i = 0; i < this.priceScope.package.length; i++) {
                    if (this.priceScope.package[i].id == strpriceDetails.sub_category_price_scope.id) {
                      if (this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) {
                        this.priceScope.package[i].txtGold = strpriceDetails.value;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_gold = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_gold = pricing_details[j].id;
                        break;
                      }
                      else if (this.priceScope.package[i].price_scope_control_type.id == 2) {
                        this.priceScope.package[i].dropdownGold = strpriceDetails.sub_category_price_scope_dtl.id;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_gold = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_gold = pricing_details[j].id;
                        break;
                      }
                      else if (this.priceScope.package[i].price_scope_control_type.id == 3) {
                        this.priceScope.package[i].checkBoxGold = strpriceDetails.value == "true" ? true : false;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_gold = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_gold = pricing_details[j].id;
                        break;
                      }
                    }
                  }
                });
              }
              else if (pricing_details[j].price_type.id == 3) {
                pricing_details[j].price_details.forEach(strpriceDetails => {
                  for (var i = 0; i < this.priceScope.package.length; i++) {
                    if (this.priceScope.package[i].id == strpriceDetails.sub_category_price_scope.id) {
                      if (this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) {
                        this.priceScope.package[i].txtPlatinum = strpriceDetails.value;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_platinum = pricing_details[j].id;
                        break;
                      }
                      else if (this.priceScope.package[i].price_scope_control_type.id == 2) {
                        this.priceScope.package[i].dropdownPlatinum = strpriceDetails.sub_category_price_scope_dtl.id;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_platinum = pricing_details[j].id;
                        break;
                      }
                      else if (this.priceScope.package[i].price_scope_control_type.id == 3) {
                        this.priceScope.package[i].checkBoxPlatinum = strpriceDetails.value == "true" ? true : false;
                        this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum = strpriceDetails.id;
                        this.priceScope.package[i].gig_price_id_platinum = pricing_details[j].id;
                        break;
                      }
                    }
                  }
                });
              }
            }
          }
          else {
            for (let i = 0; i < this.priceScope.package.length; i++) {
              this.priceScope.package[i].dropdownSilver = 0;
              this.priceScope.package[i].dropdownGold = 0;
              this.priceScope.package[i].dropdownPlatinum = 0;
            }
          }
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });
        //this.progress.hide();
      }
    }, error => {
      this.progress.hide();
      console.log(error);
    });
    this.httpServices.request("get", "extra_services", null, null, null).subscribe((extra_services) => {
      if (extra_services != null && extra_services != undefined && extra_services != '') {
        this.extraServicesList.services = extra_services;
        this.extraServicesList.services.forEach(element => element.is_custom = false);
        this.daysListForExtraGig = extra_services[0].extra_days;
        this.priceListForExtraGig = extra_services[0].extra_price;
        this.httpServices.request("get", url_extra_pricing, null, null, null).subscribe((extra_services_byId) => {
          if (extra_services_byId.length > 0) {
            this.pricingListExtraServiceAlreadyPresent = true;
          }
          for (let i = 0; i < extra_services_byId.length; i++) {
            this.extraServicesList.services.forEach(element => {
              if (extra_services_byId[i].price_extra_service.id == element.id) {
                element.is_selected = true;
                element.gig_price_extra_service_id = extra_services_byId[i].id;
                if (extra_services_byId[i].price_extra_service.id == 1 && extra_services_byId[i].price_type.id == 1) {
                  this.extraFast_silver_deliver = Number(extra_services_byId[i].extra_day);
                  this.extra_fast_silver_for = Number(extra_services_byId[i].extra_price);
                }
                else if (extra_services_byId[i].price_extra_service.id == 1 && extra_services_byId[i].price_type.id == 2) {
                  this.extraFast_gold_deliver = Number(extra_services_byId[i].extra_day);
                  this.extra_fast_gold_for = Number(extra_services_byId[i].extra_price);
                }
                else if (extra_services_byId[i].price_extra_service.id == 1 && extra_services_byId[i].price_type.id == 3) {
                  this.extraFast_platinum_deliver = Number(extra_services_byId[i].extra_day);
                  this.extra_fast_platinum_for = Number(extra_services_byId[i].extra_price);
                }
                else {
                  element.service_price = Number(extra_services_byId[i].extra_price);
                  element.service_days = Number(extra_services_byId[i].extra_day);
                }
              }

            });
          }
          this.priceScope.package.forEach(element => {
            if (element.price_scope_name == "Delivery Time" && element.dropdownSilver == 1) {
              this.allowedSilverExtraFast = true;
              this.extraFast_silver_deliver = 0;
              this.extra_fast_silver_for = "";
              // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
            }
            if (element.price_scope_name == "Delivery Time" && element.dropdownGold == 1) {
              this.allowedGoldExtraFast = true;
              this.extraFast_gold_deliver = 0;
              this.extra_fast_gold_for = "";
              // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
            }
            if (element.price_scope_name == "Delivery Time" && element.dropdownPlatinum == 1) {
              this.allowedPlatinumExtraFast = true;
              this.extraFast_platinum_deliver = 0;
              this.extra_fast_platinum_for = "";
              // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
            }
          });
          this.httpServices.request("get", url_custom_extra_pricing, null, null, null).subscribe((extra_Customservices_byId) => {
            for (let i = 0; i < extra_Customservices_byId.length; i++) {
              this.extraServicesList.services.push({
                id: extra_Customservices_byId[i].id, gig_price_extra_service_id: 0, title: extra_Customservices_byId[i].title, description: extra_Customservices_byId[i].description,
                is_price_type_applicable: true,
                heading_1: "for an Extra", heading_1_master_name: '', heading_2: "and additional", heading_2_master_name: '',
                help: '',
                extra_days: this.daysListForExtraGig,
                extra_price: this.priceListForExtraGig,
                is_selected: true, service_price: Number(extra_Customservices_byId[i].extra_price), service_days: Number(extra_Customservices_byId[i].extra_days),
                extraFastDeliveryDescription: [], is_custom: true
              });
            }
          });
        });
      }
    });
  }

  onPreviousRequirement() {
    this.activeTab = "gig_faq";
    this.showGigInfo = true;
    this.showGallery = true;
    this.showPricing = true;
    this.showFaq = true;
    this.showRequirements = false;
    this.showPublish = false;
  }
  onPreviousPublish() {
    this.activeTab = "gig_requirement";
    this.showGigInfo = true;
    this.showGallery = true;
    this.showPricing = true;
    this.showFaq = true;
    this.showRequirements = true;
    this.showPublish = false;
  }
  onPreviousFaq() {
    this.activeTab = "gig_pricing";
    this.showGigInfo = true;
    this.showGallery = true;
    this.showPricing = true;
    this.showFaq = false;
    this.showRequirements = false;
    this.showPublish = false;
  }
  onPreviousPricing() {
    this.activeTab = "gig_gallery";
    this.showGigInfo = true;
    this.showGallery = true;
    this.showPricing = false;
    this.showFaq = false;
    this.showRequirements = false;
    this.showPublish = false;
  }
  onPreviousGallery() {
    this.activeTab = "gig_info";
    this.showGigInfo = true;
    this.showGallery = false;
    this.showPricing = false;
    this.showFaq = false;
    this.showRequirements = false;
    this.showPublish = false;
  }
  // =====================================
  // on search tag key space 
  // ======================================
  doSomething() {

    var searchTagsSplit = this.searchTags.includes(',');
    if (searchTagsSplit) {
      searchTagsSplit = this.searchTags.split(',');
      searchTagsSplit.forEach(element => {
        this.searchTagsList.push({ search_tag_name: element.replace(' ', '') })
      });
      this.searchTagsList.splice(this.searchTagsList.findIndex(x => x.search_tag_name == ''), 1);
    }
  }
  // =======================================
  // Remove tag click 
  // =======================================
  removeTagClick(searchName: any) {
    this.searchTagsList.splice(this.searchTagsList.findIndex(x => x.search_tag_name == searchName), 1);
  }
  // ==============================
  //   On Change of Video
  // ==============================
  VideoChanged(event: any) {
    this.progress.show();
    if (event.target.files.length == 0) {
      this.progress.hide();
      return;
    }

    //To obtaine a File reference

    var files = event.target.files;
    const reader = new FileReader();
    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    if (files[0].type == "video/mp4" || files[0].type == "video/ogg") {

      fileType = "";
    }
    else {

      fileType = ("<li>" + "The file does not match file type." + "</li>");
    }

    //To check file Size according to upload conditions
    // if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {
    if ((files[0].size / 1024 / 1024) <= 50) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "Your file does not match the upload conditions, Your file size is:" + (files[0].size / 1024 / 1024).toFixed(2) + "MB. The maximum file size for uploads should not exceed 50 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      this.progress.hide();
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {

      this.video_data = event.target.files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.video_data_source = reader.result;
      }

      this.progress.hide();
    }
    else {
      this.resetFileVideoInput(this.file_video);
      this.video_data = "";
      this.video_data = undefined;

      this.progress.hide();

    }
  }
  @ViewChild('file_video', { read: ElementRef }) file_video: ElementRef;
  resetFileVideoInput(file: any) {
    if (file)
      file.nativeElement.value = "";
  }
  // =========================
  // On Change of Audio 
  // =========================
  AudioChanged(event: any) {

    this.progress.show();
    if (event.target.files.length == 0) {
      this.progress.hide();
      return;
    }

    //To obtaine a File reference

    var files = event.target.files;
    const reader = new FileReader();
    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    if (files[0].type == "audio/ogg" || files[0].type == "audio/mpeg" || files[0].type == "audio/mp3") {

      fileType = "";
    }
    else {

      fileType = ("<li>" + "The file does not match file type." + "</li>");
    }

    //To check file Size according to upload conditions

    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "Your file does not match the upload conditions, Your file size is:" + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + "MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      this.progress.hide();
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {

      this.audio_data = event.target.files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.audio_data_source = reader.result;
      }

      this.progress.hide();
    }
    else {
      this.resetFileAudioInput(this.file_audio);
      this.audio_data = "";
      this.audio_data = undefined;
      this.progress.hide();

    }
  }
  @ViewChild('file_audio', { read: ElementRef }) file_audio: ElementRef;
  resetFileAudioInput(file: any) {
    if (file)
      file.nativeElement.value = "";
  }
  // ==========================
  // On Change of PDF 
  // ==========================
  PdfChanged(event: any) {

    this.progress.show();
    if (event.target.files.length == 0) {
      this.progress.hide();
      return;
    }

    //To obtaine a File reference

    var files = event.target.files;
    const reader = new FileReader();
    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    if (files[0].type == "application/pdf") {

      fileType = "";
    }
    else {

      fileType = ("<li>" + "The file does not match file type." + "</li>");
    }

    //To check file Size according to upload conditions

    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "Your file does not match the upload conditions, Your file size is:" + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + "MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      this.progress.hide();
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {

      this.pdf_data = event.target.files;
      this.pdf_data_source = this.pdf_data[0];
      //   reader.readAsDataURL(files[0]); 
      //   reader.onload = (_event) => { 
      //     this.pdf_data_source = reader.result; 
      // }

      this.progress.hide();
    }
    else {
      this.resetFilePdfInput(this.file_pdf);
      this.pdf_data = "";
      this.pdf_data = undefined;
      this.progress.hide();

    }
  }
  @ViewChild('file_pdf', { read: ElementRef }) file_pdf: ElementRef;
  resetFilePdfInput(file: any) {
    if (file)
      file.nativeElement.value = "";
  }
  public fileErrorMsgArr: any[] = [];
  public campFileSpan: any;
  public none = "none";
  public filename: string = "";
  public urls: any[] = [];
  @ViewChild('file_image', { read: ElementRef }) file_image: ElementRef;
  resetFileInput(file: any) {
    if (file)
      file.nativeElement.value = "";
  }
  fileChanged(e: any) {

    this.fileErrorMsgArr = [];
    this.campFileSpan = this.none;
    //to make sure the user select file/files
    if (!e.target.files) {
      this.filename = "No file choosen";
      return;
    }
    if (e.target.files.length + this.urls.length > 3) {
      toastr.error("You can upload only 3 images.");
      this.resetFileInput(this.file_image);
      return;
    }
    //To obtaine a File reference
    var files = e.target.files;
    let index: number = 0;
    let fileLength: number = 0;
    fileLength = files.length;
    // this.filename=files[0].name;
    // Loop through the FileList and then to render image files as thumbnails.
    for (var i = 0, f; f = files[i]; i++) {
      //instantiate a FileReader object to read its contents into memory
      var fileReader = new FileReader();
      index = i;
      // Closure to capture the file information and apply validation.
      fileReader.onload = (function (readerEvt, urls, fileErrorMsgArr, index, fileLength) {
        return function (e) {

          let fileType = "";
          let fileSize = "";
          let alertstring = '';

          //To check file type according to upload conditions
          if (readerEvt.type == "image/jpeg" || readerEvt.type == "image/png" || readerEvt.type == "image/jpg" || readerEvt.type == "image/bmp" || readerEvt.type == "image/gif") {
            fileType = "";
          }
          else {
            fileErrorMsgArr.push({ "issue": "filetype", "filename": readerEvt.name, "fileSize": 0 });
            fileType = ("<li>" + "The file(" + readerEvt.name
              + ") does not match the upload conditions, You can only upload jpeg/png/jpg/bmp/gif files" + "</li>");
          }

          //To check file Size according to upload conditions
          if (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024 <= 5) {
            fileSize = "";
          }
          else {
            fileErrorMsgArr.push({ "issue": "filesize", "filename": readerEvt.name, "fileSize": (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) });

            fileSize = ("<li>" + "The file(" + readerEvt.name
              + ") does not match the upload conditions, Your file size is: "
              + (readerEvt.size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2)
              + " MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
          }
          if (fileType == "" && fileSize == "" && urls.length == 0) {
            urls.push({ id: index, file: readerEvt, file_path: e.target.result, file_name: readerEvt.name, file_mode: 'new' });
          }
          else if (fileType == "" && fileSize == "" && urls.length > 0 && urls.filter(abc => abc.file_name == readerEvt.name).length == 0) {
            urls.push({ id: index, file: readerEvt, file_path: e.target.result, file_name: readerEvt.name, file_mode: 'new' });
          }
          alertstring = alertstring + fileType + fileSize;
          if (alertstring != '') {
            this.filename = "No file chosen";//this.translate.instant('common.nofilechosen');
            //toastr.error(alertstring);
            if (index == (fileLength - 1))
              jQuery("#fileErrorMsg").click();
          }
        };
      })(f, this.urls, this.fileErrorMsgArr, index, fileLength);

      // Read in the image file as a data URL.
      // readAsDataURL: The result property will contain the file/blob's data encoded as a data URL.
      // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
      fileReader.readAsDataURL(f);
      // this.urls.forEach(element=>{
      //   this.preview( this.urls[element].fil);
      // })

      // fileReader.onload = (_event) => { 
      //   this.previewUrl = fileReader.result; 
      // }
      // this.preview(f[i]);
    }
    this.resetFileInput(this.file_image);

  }
  // preview(item:any) {
  //   // Show preview 
  //   var mimeType = item.type;
  //   if (mimeType.match(/image\/*/) == null) {
  //     return;
  //   }

  //   var reader = new FileReader();      
  //   reader.readAsDataURL(this.fileData); 
  //   reader.onload = (_event) => { 
  //     this.previewUrl = reader.result; 
  //   }
  // }
  // ================================
  // Remove attached image 
  // =================================
  public galleryImageDeleteId: any;
  public galleryRecordInfo: any;
  public galleryVideoDeleteId: any;
  public galleryAudioDeleteId: any;
  public galleryPdfDeleteId: any;
  deleteImage(event: any) {
    if (event.file == undefined) {
      this.galleryImageDeleteId = event.id;
      this.galleryRecordInfo = event;
      jQuery('#delete_gallery_Image').modal({ backdrop: 'static', keyboard: false });
      // this.urls.splice(this.urls.findIndex(x => x.file_name == event.file_name), 1);
    }
    else {
      this.urls.splice(this.urls.findIndex(x => x.file_name == event.file_name), 1);
    }

  }
  // ======================================
  // Remove gallery Image from database 
  // ======================================
  removegalleryImage() {
    this.progress.show();
    this.httpServices.request('delete', 'gigs/draft-gallery-delete/' + this.galleryImageDeleteId, null, null, null).subscribe((data) => {
      this.urls.splice(this.urls.findIndex(x => x.file_name == this.galleryRecordInfo.file_name), 1);
      toastr.success("Record deleted successfully.");
      jQuery('#delete_gallery_Image').modal('hide');
      this.progress.hide();
      this.galleryRecordInfo = "";
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // ============================
  // Remove attached video 
  // ===========================
  deleteVideo(video_data: any) {
    if (video_data[0].id != undefined && video_data[0].id != null && video_data[0].id != 0) {
      this.galleryVideoDeleteId = video_data[0].id;
      jQuery('#delete_gallery_Video').modal({ backdrop: 'static', keyboard: false });
    }
    else {
      this.resetFileVideoInput(this.file_video);
      this.video_data = "";
      this.video_data = undefined;
    }

  }
  removegalleryVideo() {
    this.progress.show();
    this.httpServices.request('delete', 'gigs/draft-gallery-delete/' + this.galleryVideoDeleteId, null, null, null).subscribe((data) => {
      toastr.success("Record deleted successfully.");
      jQuery('#delete_gallery_Video').modal('hide');
      this.progress.hide();
      this.resetFileVideoInput(this.file_video);
      this.video_data = "";
      this.video_data = undefined;
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // =========================
  // Remove attached audio 
  // =========================
  deleteAudio(audio_data: any) {
    if (audio_data[0].id != undefined && audio_data[0].id != null && audio_data[0].id != 0) {
      this.galleryAudioDeleteId = audio_data[0].id;
      jQuery('#delete_gallery_Audio').modal({ backdrop: 'static', keyboard: false });
    }
    else {
      this.resetFileAudioInput(this.file_audio);
      this.audio_data = "";
      this.audio_data = undefined;
    }

  }
  removegalleryAudio() {
    this.progress.show();
    this.httpServices.request('delete', 'gigs/draft-gallery-delete/' + this.galleryAudioDeleteId, null, null, null).subscribe((data) => {
      toastr.success("Record deleted successfully.");
      jQuery('#delete_gallery_Audio').modal('hide');
      this.progress.hide();
      this.resetFileAudioInput(this.file_audio);
      this.audio_data = "";
      this.audio_data = undefined;
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // =======================
  // Remove attached pdf 
  // ========================
  deletePdf(pdf_data: any) {
    if (pdf_data[0].id != undefined && pdf_data[0].id != null && pdf_data[0].id != 0) {
      this.galleryPdfDeleteId = pdf_data[0].id;
      jQuery('#delete_gallery_Pdf').modal({ backdrop: 'static', keyboard: false });
    }
    else {
      this.resetFilePdfInput(this.file_pdf);
      this.pdf_data = "";
      this.pdf_data = undefined;
    }

  }
  removegalleryPdf() {
    this.progress.show();
    this.httpServices.request('delete', 'gigs/draft-gallery-delete/' + this.galleryPdfDeleteId, null, null, null).subscribe((data) => {
      toastr.success("Record deleted successfully.");
      jQuery('#delete_gallery_Pdf').modal('hide');
      this.progress.hide();
      this.resetFilePdfInput(this.file_pdf);
      this.pdf_data = "";
      this.pdf_data = undefined;
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }
  // ========================
  // View attached Pdf 
  // =========================
  click_viewPdfFile(item: any, pdf_data: any) {
    if (item == undefined && pdf_data.length == 1) {
      window.open(pdf_data[0].file_path);
    }
    else {
      window.open(item);
    }
  }
  // ========================
  // View Attached Imgae 
  // =========================
  public image_ViewUrl: any;
  openImage(item: any) {

    this.image_ViewUrl = item;
    jQuery('#imagemodal').modal({ backdrop: 'static', keyboard: false });
  }
  displayFileErrorMsg() {
    let errorMsg: string = "";
    if (this.fileErrorMsgArr.length > 0) {
      this.fileErrorMsgArr.forEach(element => {
        if (element.issue == "filetype") {
          errorMsg = errorMsg + "<li>" + "The file " +
            element.filename + " does not match file type." + "</li>";
        }
        else if (element.issue == "filesize") {
          errorMsg = errorMsg + "<li>" + "The file " +
            element.filename + " having sizze: "
            + element.filesize + " does not match file size condition." + "</li>";
        }
      });
      toastr.error(errorMsg);
    }
  }
  // ==============================================
  // On click of Next and continue of Gig Gallery 
  // ===============================================
  onClickNextAndContinue_GigGallery() {


    //   let gig_gallery_info: any = {
    //     gig_draft_id: 0,
    //     gallery: [
    //       {
    //         gig_gallery_type_id: 0,
    //         file_path: '',
    //         file_path_thumbnail: ''
    //       }
    //     ]
    //   }

    //  // gig_gallery_info.gig_draft_id = this.gig_draft_Id;
    //  gig_gallery_info.gig_draft_id =1;
    //   if (this.video_data != null && this.video_data != undefined && this.video_data != '') {
    //     gig_gallery_info.gallery.push({ gig_gallery_type_id: 2, file_path: this.video_data[0], file_path_thumbnail: this.video_data[0] });
    //   }
    //   if (this.audio_data != null && this.audio_data != undefined && this.audio_data != '') {
    //     gig_gallery_info.gallery.push({ gig_gallery_type_id: 3, file_path: this.audio_data[0], file_path_thumbnail:  this.audio_data[0] });
    //   }
    //   if (this.pdf_data != null && this.pdf_data != undefined && this.pdf_data != '') {
    //     gig_gallery_info.gallery.push({ gig_gallery_type_id: 4, file_path: this.pdf_data[0], file_path_thumbnail: this.pdf_data[0] });
    //   }
    //   if (this.urls != null && this.urls != undefined && this.urls != []) {
    //     this.urls.forEach(element => {
    //       gig_gallery_info.gallery.push({ gig_gallery_type_id: 1, file_path: element.file, file_path_thumbnail: element.file });
    //     })
    //   }
    //   gig_gallery_info.gallery.splice(gig_gallery_info.gallery.findIndex(x=>x.gig_gallery_type_id==0),1);
    if (this.galleryAlreadyPresent == true) {
      this.GalleryUpdate();
    }
    else {
      this.progress.show();
      let formData = new FormData();
      let count = 0;
      if (this.urls != null && this.urls != undefined && this.urls != []) {
        for (var i = 0; i < this.urls.length; i++) {
          // if (this.urls[i].id == 0) {
          formData.append("gallery[" + i + "]gig_gallery_type_id", "1");
          formData.append("gallery[" + i + "]file_path", this.urls[i].file);
          formData.append("gallery[" + i + "]file_path_thumbnail", this.urls[i].file);
          count = i + 1;
          // }
        }
      }
      if (this.video_data != null && this.video_data != undefined && this.video_data != '') {
        if (this.video_data[0].id == undefined) {
          count = count + 1;
          formData.append("gallery[" + count + "]gig_gallery_type_id", "2");
          formData.append("gallery[" + count + "]file_path", this.video_data[0]);
          formData.append("gallery[" + count + "]file_path_thumbnail", this.video_data[0]);
        }
      }
      if (this.audio_data != null && this.audio_data != undefined && this.audio_data != '') {
        if (this.audio_data[0].id == undefined) {
          count = count + 1;
          formData.append("gallery[" + count + "]gig_gallery_type_id", "4");
          formData.append("gallery[" + count + "]file_path", this.audio_data[0]);
          formData.append("gallery[" + count + "]file_path_thumbnail", this.audio_data[0]);
        }
      }
      if (this.pdf_data != null && this.pdf_data != undefined && this.pdf_data != '') {
        if (this.pdf_data[0].id == undefined) {
          count = count + 1;
          formData.append("gallery[" + count + "]gig_gallery_type_id", "3");
          formData.append("gallery[" + count + "]file_path", this.pdf_data[0]);
          formData.append("gallery[" + count + "]file_path_thumbnail", this.pdf_data[0]);
        }
      }
      if (count == 0) {
        this.progress.hide();
        toastr.error("Please provide atleast one media.");
      }
      else {
        formData.append("gig_draft_id", this.gig_draft_Id);
        this.httpServices.request('post', 'gigs/draft-gallery', null, null, formData).subscribe((data) => {
          this.activeTab = "gig_pricing";
          toastr.success("Record saved successfully");
          this.showPricing = true;
          this.showGigInfo = true;
          this.showGallery = true;
          this.pdf_data_source = undefined;
          this.getPackagesList();
          this.getExtraServicesList();
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });
      }

    }

  }
  // =======================================================
  // FAQ Section 
  // =======================================================
  public addNewFaqSection: boolean = false;
  public updateFAQSection: boolean = false;
  public faqQuestion: any;
  public faqAnswer: any;
  public faqList: Array<{ id: number, seq_no: Number, question: string, answer: string, is_open: boolean }> = [];
  public faqInSide: Array<{ seq_no: Number, question: string, answer: string }> = [];
  public faqQuestionUpdate: any;
  public faqAnswerUpdate: any;
  public faqSeqNoUpdate: any;

  // =========================
  // Add New FAQ 
  // =========================
  addNewFAQ() {

    this.faqQuestion = '';
    this.faqAnswer = '';
    this.addNewFaqSection = true;
  }
  // ==============================
  // On Cancel FAQ click 
  // ==============================
  onCancelFaq() {

    this.addNewFaqSection = false;
  }
  // ================================
  // Save New FAQ 
  // ================================
  saveNewFaq() {
    let errorMessage: string = '';
    if (this.faqQuestion == null || this.faqQuestion.toString().trim() == '' || this.faqQuestion == undefined) {
      errorMessage = errorMessage + ("<li>Please enter FAQ question.</li>");
    }
    if (this.faqAnswer == null || this.faqAnswer.toString().trim() == '' || this.faqAnswer == undefined) {
      errorMessage = errorMessage + ("<li>Please enter FAQ answer.</li>");
    }
    if (errorMessage == '') {
      this.faqList.push({ id: 0, seq_no: this.faqList.length + 1, question: this.faqQuestion, answer: this.faqAnswer, is_open: false });
      this.updateFAQSection = true;
      this.addNewFaqSection = false;
    }
    else {
      toastr.error(errorMessage);
    }
  }
  // =================================
  // Cancel Update FAQ Section 
  // ==================================
  cancelUpdateFaqSection(item: any) {
    item.is_open = false;
    // this.updateFAQSection=false;
    //jQuery(".close_collapse").click(function(){
    // jQuery("#accordion_1 .panel-title > a").attr("aria-expanded","false");
    // jQuery(".panel-collapse.in.collapse").removeClass("show");
    //});
  }
  // =============================
  // Update Faq 
  // ============================
  updateFaq(item: any) {

    this.faqList.splice(this.faqList.findIndex(x => x.seq_no == item.seq_no), 1);
    this.faqList.push({ id: item.id, seq_no: item.seq_no, question: item.question, answer: item.answer, is_open: false });
  }

  // ==============================
  // Faq Final Save 
  // ==============================
  faqSaveFinal() {
    if (this.faqListAlreadyPresent == true) {
      this.updateFaqPatch();
    }
    else {
      let gigsInfo: any = {
        gig_draft_id: 0,
        faq: [{
          seq_no: 0,
          question: '',
          answer: ''
        }]
      }
      gigsInfo.gig_draft_id = this.gig_draft_Id;
      this.faqList.forEach(element => {
        gigsInfo.faq.push({ seq_no: element.seq_no, question: element.question, answer: element.answer });
      })
      if (gigsInfo.faq.length > 1) {
        gigsInfo.faq.splice(gigsInfo.faq.findIndex(x => x.seq_no == 0), 1);
      }
      if (gigsInfo.faq.length > 0) {
        this.progress.show();
        this.httpServices.request("post", "gigs/draft-faq", null, null, gigsInfo).subscribe((data) => {
          toastr.success("Records saved successfully.");
          this.activeTab = "gig_requirement";
          this.showGigInfo = true;
          this.showFaq = true;
          this.showPricing = true;
          this.showGallery = true;
          this.showRequirements = true;
          this.progress.hide();
        }, error => {
          this.progress.hide();
          console.log(error);
        });
      } else if (this.faqList.length == 0) {
        toastr.error('FAQ is mandatory, Please enter atleast one FAQ.');
      }
    }

  }
  // ========================================
  // get FaqInfo in edit Mode 
  // ========================================
  public faqListAlreadyPresent: boolean = false;
  getFaqInfo() {

    this.progress.show();
    let url: any;
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id == 5 || this.gig_status_id == 6) {
      url = 'gigs/faq-list/' + this.valueID;
    }
    else {
      url = 'gigs/draft-faq-list/' + this.valueID;
    }
    this.faqList = [];
    this.httpServices.request('get', url, null, null, null).subscribe((data) => {
      if (data != null && data != undefined) {
        this.updateFAQSection = true;
        data.forEach(element => {
          this.faqList.push({ id: element.id, seq_no: element.seq_no, question: element.question, answer: element.answer, is_open: false });
        });
        this.faqListAlreadyPresent = true;

      }
      if (data.length > 0) {
        this.addNewFaqSection = false;
      }
      else {
        this.addNewFaqSection = true;
      }
      this.activeTab = "gig_faq";
      this.showPricing = true;
      this.showGallery = true;
      this.showGigInfo = true;
      this.showFaq = true;
      this.progress.hide();
    });
  }
  // ===========================================
  // Faq Update Patch 
  // ===========================================
  updateFaqPatch() {
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id != 5 && this.gig_status_id != 6) {
      let gigsInfo: any = {
        faq: [{
          id: 0,
          seq_no: 0,
          question: '',
          answer: ''
        }]
      }
      this.faqList.forEach(element => {
        gigsInfo.faq.push({ id: (element.id == null || element.id == undefined || element.id == 0) ? 0 : element.id, seq_no: element.seq_no, question: element.question, answer: element.answer });
      });
      gigsInfo.faq.splice(gigsInfo.faq.findIndex(x => x.seq_no == 0), 1);
      if (gigsInfo.faq.length > 0) {
        this.progress.show();
        this.httpServices.request('patch', "gigs/draft-faq-update/" + this.gig_draft_Id, null, null, gigsInfo).subscribe((data) => {
          toastr.success("Records updated successfully.");
          this.activeTab = "gig_requirement";
          this.showGigInfo = true;
          this.showFaq = true;
          this.showPricing = true;
          this.showGallery = true;
          this.showRequirements = true;
          this.getRequirementInfo();
          this.progress.hide();
        });
      }
      else if (this.faqList.length == 0) {
        toastr.error('FAQ is mandatory, Please enter atleast one FAQ.');
      }
    }
    else {
      let gigsInfo: any = {
        gig_id: 0,
        faq: [{
          seq_no: 0,
          question: '',
          answer: ''
        }]
      }
      this.faqList.forEach(element => {
        gigsInfo.faq.push({ seq_no: element.seq_no, question: element.question, answer: element.answer });
      });
      gigsInfo.faq.splice(gigsInfo.faq.findIndex(x => x.seq_no == 0), 1);

      gigsInfo.gig_id = this.valueID;
      this.httpServices.request('post', 'gigs_extra/active-gig-update', null, null, gigsInfo).subscribe((data) => {
        toastr.success("Records updated successfully.");
        this.activeTab = "gig_requirement";
        this.showGigInfo = true;
        this.showFaq = true;
        this.showPricing = true;
        this.showGallery = true;
        this.showRequirements = true;
        this.activeGigSaved = true;
        this.gig_draft_Id = data.gig_draft_id;
        this.valueID = data.gig_draft_id;
        this.getRequirementInfo();
        this.progress.hide();
      });
    }
  }
  onClickFaqArrow(item: any) {
    if (item.is_open == false) {
      item.is_open = true;
    }
    else {
      item.is_open = false;
    }
  }
  // =========================================
  // Requirement Section 
  // =========================================
  public requirementList: Array<{ id: Number, is_mandatory: boolean, question: string, question_form: Number, is_multiselect: boolean, description: any[] }> = [];
  public showNewRequirementSection: boolean = false;
  public showOptions: boolean = false;
  public is_Required: boolean = false;
  public question_requirement: any;
  public question_form_requirement: any;
  public is_multiselect_requirement: boolean = false;
  public requirement_id: any;
  public requirement_optionList: Array<{ id: Number, requirement_id: Number, optionText: string }> = [];
  public requirement_option_list_temp: Array<{ id: Number, requirement_id: Number, optionText: string }> = [];
  // ====================================
  // On Click of Add New Requirement 
  // ====================================
  addNewRequirement() {
    this.showNewRequirementSection = true;
    this.requirement_id = 0;
    this.is_multiselect_requirement = false;
    this.question_form_requirement = 0;
    this.question_requirement = '';
    this.is_Required = false;
    this.requirement_optionList = [];
  }
  // ======================================
  // Save requirement 
  // ======================================
  save_requirement() {
    let errorMessage: string = '';
    if (this.question_requirement == null || this.question_requirement == undefined || this.question_requirement.toString().trim() == '') {
      errorMessage = errorMessage + ("<li>Please enter question.</li>");
    }
    if (this.question_form_requirement == null || this.question_form_requirement == undefined || this.question_form_requirement == 0 || this.question_form_requirement == "0") {
      errorMessage = errorMessage + ("<li>Please enter question type.</li>");
    }
    if (errorMessage == '') {
      if (this.requirementList.length > 0) {
        this.requirement_id = Math.max.apply(Math, this.requirementList.map(function (o) { return o.id; })) + 1;
      }
      else {
        this.requirement_id = this.requirementList.length + 1;
      }

      this.requirementList.push({
        id: this.requirement_id, is_mandatory: this.is_Required,
        question: this.question_requirement, question_form: this.question_form_requirement, is_multiselect: this.is_multiselect_requirement, description: []
      });

      if (this.question_form_requirement == 2) {
        this.requirement_optionList.forEach(element => {
          this.requirement_option_list_temp.push({ id: element.id, requirement_id: this.requirement_id, optionText: element.optionText });
        }
        )
      }
      this.showNewRequirementSection = false;
    }
    else {
      toastr.error(errorMessage);
    }

  }
  // ========================================
  // update Requirement 
  // ======================================
  update_requirement(requirement_id: any) {
    let errorMessage: string = '';
    if (requirement_id > 0) {
      if (this.question_requirement == null || this.question_requirement == undefined || this.question_requirement.toString().trim() == '') {
        errorMessage = errorMessage + ("<li>Please enter question.</li>");
      }
      if (this.question_form_requirement == null || this.question_form_requirement == undefined || this.question_form_requirement == 0 || this.question_form_requirement == "0") {
        errorMessage = errorMessage + ("<li>Please enter question type.</li>");
      }
      if (errorMessage == '') {
        this.requirementList.splice(this.requirementList.findIndex(element => element.id == requirement_id), 1);
        this.requirementList.push({
          id: requirement_id, is_mandatory: this.is_Required,
          question: this.question_requirement, question_form: this.question_form_requirement, is_multiselect: this.is_multiselect_requirement, description: []
        });
        if (this.question_form_requirement == 2) {
          if (this.requirement_optionList.filter(x => x.requirement_id == requirement_id).length > 0) {
            if (this.requirement_option_list_temp.filter(x => x.requirement_id == requirement_id).length > 0) {
              this.requirement_option_list_temp.splice(this.requirement_option_list_temp.findIndex(element => element.requirement_id == requirement_id));
              this.requirement_optionList.forEach(element => {
                this.requirement_option_list_temp.push({ id: element.id, requirement_id: requirement_id, optionText: element.optionText });
              }
              )
            }
          }
        }
        this.showNewRequirementSection = false;
      }
      else {
        toastr.error(errorMessage);
      }
    }
  }
  // ======================================
  // open requirement in edit mode
  // ======================================
  openRequirementInEditMode(id: any) {

    let filterData = this.requirementList.filter(x => x.id == id)
    this.requirement_id = id;
    this.is_multiselect_requirement = filterData[0].is_multiselect;
    this.question_form_requirement = filterData[0].question_form;
    this.question_requirement = filterData[0].question;
    this.is_Required = filterData[0].is_mandatory;
    if (this.question_form_requirement == 2) {
      this.showOptions = true;
    }
    else {
      this.showOptions = false;
    }
    this.requirement_optionList = [];
    this.requirement_option_list_temp.forEach(element => {
      if (element.requirement_id == this.requirement_id) {
        this.requirement_optionList.push({ id: element.id, requirement_id: element.requirement_id, optionText: element.optionText })
      }
    })
    this.showNewRequirementSection = true;
  }
  // ======================================
  // Remove requirement 
  // ======================================
  RemoveRequirement(requirement_id: any) {
    if (requirement_id > 0) {
      this.requirementList.splice(this.requirementList.findIndex(element => element.id == requirement_id), 1);
      if (this.requirement_option_list_temp.filter(x => x.requirement_id == requirement_id).length > 0) {
        this.requirement_option_list_temp.splice(this.requirement_option_list_temp.findIndex(element => element.requirement_id == requirement_id));
      }
    }
  }
  // =======================================
  // On Change of Question form 
  // =======================================
  onChange_question_form_requirement(requirement_id: any) {
    if (this.question_form_requirement == 2) {
      this.showOptions = true;
      this.requirement_optionList.push({ id: this.requirement_optionList.length + 1, requirement_id: requirement_id, optionText: '' });
      this.requirement_optionList.push({ id: this.requirement_optionList.length + 1, requirement_id: requirement_id, optionText: '' });
    }
    else {
      this.showOptions = false;
    }
  }
  // ===============================
  // On Click of Add New option 
  // ===============================
  onAddNewOptionClick(requirement_id: any) {

    let id = Math.max.apply(Math, this.requirement_optionList.map(function (o) { return o.id; }))
    this.requirement_optionList.push({ id: id + 1, requirement_id: requirement_id, optionText: '' });
  }
  // ===================================
  // Remove option from requirement 
  // ====================================
  removeAddedOption(id: any) {
    this.requirement_optionList.splice(this.requirement_optionList.findIndex(x => x.id == id), 1);
  }
  // =============================================
  // cancel btn in requirement add edit section 
  // ==============================================
  cancelRequirement() {
    this.showNewRequirementSection = false;
  }
  // ======================================
  // Final save requirement
  // ======================================
  final_save_requirement() {
    if (this.requirementListAlreadyPresent == true) {
      this.updateRequirementPatch();
    }
    else {
      let gig_requirement: any = {
        gig_draft_id: 0,
        requirements: [{
          is_mandatory: true,
          question: "",
          question_form_id: 0,
          is_multiselect: true,
          description: [
            {
              description: ""
            }
          ]
        }]
      }

      for (let i = 0; i < this.requirementList.length; i++) {
        for (let k = 0; k < this.requirement_option_list_temp.length; k++) {
          if (this.requirementList[i].id == this.requirement_option_list_temp[k].requirement_id) {
            this.requirementList[i].description.push({ description: this.requirement_option_list_temp[k].optionText });

          }
        }

        gig_requirement.requirements.push({
          is_mandatory: this.requirementList[i].is_mandatory,
          question: this.requirementList[i].question,
          //question_form.question_form_name:this.requirementList[i].question_form_name,
          question_form_id: this.requirementList[i].question_form,
          is_multiselect: this.requirementList[i].is_multiselect,
          description: this.requirementList[i].description
        });
      }
      gig_requirement.gig_draft_id = this.gig_draft_Id;
      gig_requirement.requirements.splice(gig_requirement.requirements.findIndex(x => x.question_form_id == 0), 1);
      this.progress.show()
      this.httpServices.request("post", "gigs/draft-requirements", null, null, gig_requirement).subscribe((data) => {
        this.progress.hide();
        toastr.success("Records Saved Successfully.");
        this.activeTab = "gig_publish";
        this.showRequirements = true;
        this.showFaq = true;
        this.showPricing = true;
        this.showGallery = true;
        this.showGigInfo = true;
        this.showPublish = true;

      }, error => {
        this.progress.hide();
        console.log(error);
      })
    }

  }
  // ========================================
  // update requirement patch 
  // ========================================
  updateRequirementPatch() {
    this.progress.show();
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id != 5 && this.gig_status_id != 6) {
      let requirementsBody: any = {
        requirements: [
          {
            id: 0,
            is_mandatory: true,
            question: "string",
            question_form_id: 0,
            is_multiselect: true,
            description: [
              {
                id: 0,
                description: "string"
              }
            ]
          }
        ]
      }
      for (let i = 0; i < this.requirementList.length; i++) {
        for (let k = 0; k < this.requirement_option_list_temp.length; k++) {
          if (this.requirementList[i].id == this.requirement_option_list_temp[k].requirement_id) {
            this.requirementList[i].description.push({ id: this.requirement_option_list_temp[k].id, description: this.requirement_option_list_temp[k].optionText });

          }
        }

        if (this.requirementAlreadyExistIds.filter(x => x.id == this.requirementList[i].id).length > 0) {
          requirementsBody.requirements.push({
            id: this.requirementList[i].id,
            is_mandatory: this.requirementList[i].is_mandatory,
            question: this.requirementList[i].question,
            //question_form.question_form_name:this.requirementList[i].question_form_name,
            question_form_id: this.requirementList[i].question_form,
            is_multiselect: this.requirementList[i].is_multiselect,
            description: this.requirementList[i].description
          });
        }
        else {
          requirementsBody.requirements.push({
            id: 0,
            is_mandatory: this.requirementList[i].is_mandatory,
            question: this.requirementList[i].question,
            //question_form.question_form_name:this.requirementList[i].question_form_name,
            question_form_id: this.requirementList[i].question_form,
            is_multiselect: this.requirementList[i].is_multiselect,
            description: this.requirementList[i].description
          });
        }

      }
      requirementsBody.requirements.splice(requirementsBody.requirements.findIndex(x => x.question_form_id == 0), 1);
      this.httpServices.request("patch", "gigs/draft-requirements-update/" + this.gig_draft_Id, null, null, requirementsBody).subscribe((data) => {
        this.progress.hide();
        toastr.success("Records Updated Successfully.");
        this.activeTab = "gig_publish";
        this.showRequirements = true;
        this.showFaq = true;
        this.showPricing = true;
        this.showGallery = true;
        this.showGigInfo = true;
        this.showPublish = true;

      }, error => {
        this.progress.hide();
        console.log(error);
      })
    }
    else {
      let requirementsBody: any = {
        gig_id: 0,
        req: [
          {
            is_mandatory: true,
            question: "string",
            question_form_id: 0,
            is_multiselect: true,
            description: [
              {
                description: "string"
              }
            ]
          }
        ]
      }

      requirementsBody.gig_id = this.valueID;
      for (let i = 0; i < this.requirementList.length; i++) {
        for (let k = 0; k < this.requirement_option_list_temp.length; k++) {
          if (this.requirementList[i].id == this.requirement_option_list_temp[k].requirement_id) {
            this.requirementList[i].description.push({ id: this.requirement_option_list_temp[k].id, description: this.requirement_option_list_temp[k].optionText });

          }
        }

        if (this.requirementAlreadyExistIds.filter(x => x.id == this.requirementList[i].id).length > 0) {
          requirementsBody.req.push({
            //id: this.requirementList[i].id,
            is_mandatory: this.requirementList[i].is_mandatory,
            question: this.requirementList[i].question,
            //question_form.question_form_name:this.requirementList[i].question_form_name,
            question_form_id: this.requirementList[i].question_form,
            is_multiselect: this.requirementList[i].is_multiselect,
            description: this.requirementList[i].description
          });
        }
        else {
          requirementsBody.req.push({
            //id: 0,
            is_mandatory: this.requirementList[i].is_mandatory,
            question: this.requirementList[i].question,
            //question_form.question_form_name:this.requirementList[i].question_form_name,
            question_form_id: this.requirementList[i].question_form,
            is_multiselect: this.requirementList[i].is_multiselect,
            description: this.requirementList[i].description
          });
        }

      }
      requirementsBody.req.splice(requirementsBody.req.findIndex(x => x.question_form_id == 0), 1);
      this.httpServices.request("post", "gigs_extra/active-gig-update", null, null, requirementsBody).subscribe((data) => {
        this.progress.hide();
        toastr.success("Records Updated Successfully.");
        this.activeTab = "gig_publish";
        this.showRequirements = true;
        this.showFaq = true;
        this.showPricing = true;
        this.showGallery = true;
        this.showGigInfo = true;
        this.showPublish = true;
        this.activeGigSaved = true;
        this.gig_draft_Id = data.gig_draft_id;
        this.valueID = data.gig_draft_id;
      }, error => {
        this.progress.hide();
        console.log(error);
      })
    }
  }
  // ====================================
  // Pricing section 
  // ====================================
  public packageCount: boolean = false;
  // public extraServicesList: Array<{
  //   id: number, title: string, description: string, is_price_type_applicable: boolean, heading_1: string,
  //   heading_1_master_name: string, heading_2: string, heading_2_master_name: string, help: string, is_selected: boolean, service_price: string, service_days: Number,
  //   extraFastDeliveryDescription: any[],is_custom:boolean
  // }> = [];
  public extraServicesList: extraServicesList = {
    services: [
      {
        id: 0,
        gig_price_extra_service_id: 0,
        title: "",
        description: "",
        is_price_type_applicable: false,
        heading_1: "",
        heading_1_master_name: "",
        heading_2: "",
        heading_2_master_name: "",
        help: "",
        extra_days: [{
          id: 0,
          seq_no: 0,
          extra_days_name: "",
          extra_days_value: 0
        }],
        extra_price: [{
          id: 0,
          seq_no: 0,
          extra_price_name: "",
          extra_price_value: 0
        }],
        is_selected: false,
        service_price: 0,
        service_days: 0,
        extraFastDeliveryDescription: [],
        is_custom: false
      }
    ]
  }
  public extraFast_silver_deliver: any;
  public extra_fast_silver_for: any;
  public extraFast_gold_deliver: any;
  public extra_fast_gold_for: any;
  public extraFast_platinum_deliver: any;
  public extra_fast_platinum_for: any;
  public showExtraPriceSection: boolean = false;
  public extraServiceTitle: any;
  public extraServiceDescription: any;
  public extraServicePrice: any;
  public extraServiceAdditional: any;
  public extraServiceCheck: boolean = false;
  public addextraServiceDone: boolean = false;
  public tempCountOne: any = 0;
  public tempCountTwo: any = 0
  public packageList: packageList = {
    package: [{
      id: 0,
      sub_category_price_scope: {
        id: 0,
        sub_category: {
          id: 0,
          category: {
            id: 0,
            category_name: "",
            is_active: true
          },
          sub_category_name: "",
          is_active: true,
          created_date: "",
          modified_date: ""
        },
        seq_no: 0,
        price_scope_name: "",
        is_heading_visible: true,
        price_scope_control_type: {
          id: 0,
          price_scope_control_type_name: ""
        },
        watermark: "",
        default_value: '',
        is_active: true,
        created_date: "",
        modified_date: ""
      },
      seq_no: 0,
      value: '',
      price_type_details: [
        {
          sub_category_price_scope_id: 0,
          sub_category_price_scope_dtl_id: 0,
          value: '',
          price_type_id: 0
        }
      ],
      txtSilver: "",
      txtGold: "",
      txtPlatinum: "",
      dropdownSilver: 0,
      dropdownGold: 0,
      dropdownPlatinum: 0,
      checkBoxSilver: true,
      checkBoxGold: true,
      checkBoxPlatinum: true
    }]
  };
  public priceScope: priceScope = {
    package: [{
      id: 0,
      gig_sub_category_price_dtl_id_silver: 0,
      gig_sub_category_price_dtl_id_gold: 0,
      gig_sub_category_price_dtl_id_platinum: 0,
      gig_price_id_silver: 0,
      gig_price_id_gold: 0,
      gig_price_id_platinum: 0,
      seq_no: 0,
      is_mandatory: false,
      price_scope_name: "",
      is_heading_visible: false,
      price_scope_control_type: {
        id: 0,
        price_scope_control_type_name: ""
      },
      watermark: "",
      default_value: "",
      sub_category_price_scope_details: [
        {
          id: 0,
          seq_no: 0,
          value: "",
          actual_value:""
        }
      ],
      is_active: false,
      created_date: "",
      modified_date: "",
      txtSilver: "",
      txtGold: "",
      txtPlatinum: "",
      dropdownSilver: 0,
      dropdownGold: 0,
      dropdownPlatinum: 0,
      checkBoxSilver: false,
      checkBoxGold: false,
      checkBoxPlatinum: false
    }]
  }
  // ==================================
  // get Packages list 
  // ==================================
  getPackagesList() {
    this.progress.show();

    this.httpServices.request("get", "subcategories/price-scope?sub_category_id=" + this.getSubCategory.id, null, null, null).subscribe((data) => {
      if (data != null && data != undefined && data != '') {
        //this.packageList.package = data;
        this.priceScope.package = data;
        for (let i = 0; i < this.priceScope.package.length; i++) {
          this.priceScope.package[i].dropdownSilver = 0;
          this.priceScope.package[i].dropdownGold = 0;
          this.priceScope.package[i].dropdownPlatinum = 0;
        }
        // data.forEach(element => {
        //   this.packageList.package.push({id:data.id,sub_category_price_scope.id:data.sub_category_price_scope.id})
        // });

        // for (let i = 0; i < this.packageList.package.length; i++) {
        //   //  this.packageList[i].txtSilver='';
        //   //  this.packageList[i].txtGold='';
        //   //  this.packageList[i].txtPlatinum='';
        //   //  this.packageList[i].dropdownSilver=0;
        //   //  this.packageList[i].dropdownGold=0;
        //   //  this.packageList[i].dropdownPlatinum=0;
        //   //  this.packageList[i].checkBoxSilver=false;
        //   //  this.packageList[i].checkBoxGold=false;
        //   //  this.packageList[i].checkBoxPlatinum=false;
        //   this.packageList.package[i].price_type_details = [{
        //     sub_category_price_scope_id: 0,
        //     sub_category_price_scope_dtl_id: 0,
        //     value: '',
        //     price_type_id: 0

        //   }];
        // }
        //this.getPackageinfo();
        this.progress.hide();
      }
    }, error => {
      this.progress.hide();
      console.log(error);
    })
  }
  // ============================================
  // get package Info 
  // ============================================
  // getPackageinfo() {
  //   this.progress.show();

  //   this.httpServices.request("get", "gigs/draft-pricing-details/" + this.gig_draft_Id, null, null, null).subscribe((data) => {
  //     if (data.filter(x => x.price_type.id == 2 || x.price_type.id == 3).length > 0) {
  //       this.packageCount = true;
  //     }
  //     for (let i = 0; i < data.length; i++) {
  //       this.priceScope.package.forEach(x => {
  //         if (x.id == data[i].price_details[0].sub_category_price_scope.id) {
  //           if (x.price_scope_control_type.id == 1 && data[i].price_type.id == 1) {
  //             x.txtSilver = data[i].price_details[0].value;
  //           }
  //           else if (x.price_scope_control_type.id == 1 && data[i].price_type.id == 2) {
  //             x.txtGold = data[i].price_details[0].value;
  //           }
  //           else if (x.price_scope_control_type.id == 1 && data[i].price_type.id == 3) {
  //             x.txtPlatinum = data[i].price_details[0].value;
  //           }
  //           else if (x.price_scope_control_type.id == 2 && data[i].price_type.id == 1) {
  //             x.dropdownSilver = data[i].price_details[0].sub_category_price_scope_dtl.id;
  //           }
  //           else if (x.price_scope_control_type.id == 2 && data[i].price_type.id == 2) {
  //             x.dropdownGold = data[i].price_details[0].sub_category_price_scope_dtl.id;
  //           }
  //           else if (x.price_scope_control_type.id == 2 && data[i].price_type.id == 3) {
  //             x.dropdownPlatinum = data[i].price_details[0].sub_category_price_scope_dtl.id;
  //           }
  //           else if (x.price_scope_control_type.id == 3 && data[i].price_type.id == 1) {
  //             if (data[i].price_details[0].value == "true") {
  //               x.checkBoxSilver = true;
  //             }
  //             else {
  //               x.checkBoxSilver = false;
  //             }
  //           }
  //           else if (x.price_scope_control_type.id == 3 && data[i].price_type.id == 2) {
  //             if (data[i].price_details[0].value == "true") {
  //               x.checkBoxGold = true;
  //             }
  //             else {
  //               x.checkBoxGold = false;
  //             }
  //           }
  //           else if (x.price_scope_control_type.id == 3 && data[i].price_type.id == 3) {
  //             if (data[i].price_details[0].value == "true") {
  //               x.checkBoxPlatinum = true;
  //             }
  //             else {
  //               x.checkBoxPlatinum = false;
  //             }
  //           }
  //         }
  //       })
  //     }
  //     this.progress.hide();
  //   });

  // }
  // =====================================
  // get extra services info 
  // ======================================
  getExtraServicesInfo() {
    this.progress.show();
    this.httpServices.request("get", "gigs/draft-pricing-extra-services/" + this.gig_draft_Id, null, null, null).subscribe((data) => {

      for (let i = 0; i < this.extraServicesList.services.length; i++) {
        let dataValues: any;
        dataValues = data.filter(x => x.price_extra_service.id == this.extraServicesList.services[i].id);
        if (dataValues != undefined && dataValues != null) {
          this.extraServicesList.services[i].title = dataValues.price_extra_service.title;
          this.extraServicesList.services[i].description = dataValues.price_extra_service.description;
          this.extraServicesList.services[i].is_price_type_applicable = dataValues.price_extra_service.is_price_type_applicable;
          this.extraServicesList.services[i].heading_1 = dataValues.price_extra_service.heading_1;
          this.extraServicesList.services[i].heading_1_master_name = dataValues.price_extra_service.heading_1_master_name;
          this.extraServicesList.services[i].heading_2 = dataValues.price_extra_service.heading_2;
          this.extraServicesList.services[i].heading_2_master_name = dataValues.price_extra_service.heading_2_master_name;
          this.extraServicesList.services[i].help = dataValues.price_extra_service.help;
          this.extraServicesList.services[i].is_selected = dataValues.price_extra_service.is_selected;
          this.extraServicesList.services[i].service_price = dataValues.price_extra_service.service_price;
          this.extraServicesList.services[i].service_days = dataValues.price_extra_service.service_days;
          this.extraServicesList.services[i].is_custom = false;
        }
      }
      this.progress.hide();
    });
  }

  public dropdownSilverText: any;
  public dropdownGoldText: any;
  public dropdownPlatinumText: any;
  public silverExtraFastDays: any;
  public goldExtraFastDays: any;
  public platinumExtraFastDays: any;
  public activeGigSaved: boolean = false;
  onDropDownExtrafastSilverChange() {
    this.priceScope.package.forEach(element => {
      if (element.price_scope_name == "Delivery Time") {
        let dropdownActualValue:any;
        for(let i=0;i<element.sub_category_price_scope_details.length;i++)
        {
          if(element.sub_category_price_scope_details[i].id==element.dropdownSilver)
          {
            dropdownActualValue=element.sub_category_price_scope_details[i].actual_value;
            break;
          }
        }
        if (this.extraFast_silver_deliver > dropdownActualValue) {
          this.extraFast_silver_deliver = 0;
          toastr.error("Extra Fast delivery time should be less than Basic delivery time.");
        }
        // else if(element.dropdownSilver==1)
        // {
        //   this.extraFast_silver_deliver = 0;
        //   toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
        // }

      }
    }
    )
  }
  onDropDownExtrafastGoldChange() {
    this.priceScope.package.forEach(element => {
      if (element.price_scope_name == "Delivery Time") {
        let dropdownActualValue:any;
        for(let i=0;i<element.sub_category_price_scope_details.length;i++)
        {
          if(element.sub_category_price_scope_details[i].id==element.dropdownGold)
          {
            dropdownActualValue=element.sub_category_price_scope_details[i].actual_value;
            break;
          }
        }
        if (this.extraFast_gold_deliver > dropdownActualValue) {
          this.extraFast_gold_deliver = 0;
          toastr.error("Extra Fast delivery time should be less than Basic delivery time.");
        }
        // else if(element.dropdownGold==1)
        // {
        //   this.extraFast_gold_deliver = 0;
        //   toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
        // }
      }
    }
    )
  }
  onDropDownExtrafastPlatinumChange() {
    this.priceScope.package.forEach(element => {
      if (element.price_scope_name == "Delivery Time") {
        let dropdownActualValue:any;
        for(let i=0;i<element.sub_category_price_scope_details.length;i++)
        {
          if(element.sub_category_price_scope_details[i].id==element.dropdownPlatinum)
          {
            dropdownActualValue=element.sub_category_price_scope_details[i].actual_value;
            break;
          }
        }
        if (this.extraFast_platinum_deliver > dropdownActualValue) {
          this.extraFast_platinum_deliver = 0;
          toastr.error("Extra Fast delivery time should be less than Basic delivery time.");
        }

        // else if(element.dropdownPlatinum==1)
        // {
        //   this.extraFast_platinum_deliver = 0;
        //   toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
        // }
      }
    }
    )
  }
  public allowedSilverExtraFast: boolean = false;
  public allowedGoldExtraFast: boolean = false;
  public allowedPlatinumExtraFast: boolean = false;
  onDropDownSilverChange(item: any, dropdownvalue: any, list: any, price_scope_name: any) {
   
    if (price_scope_name == "Delivery Time") {
      let dropdownActualValue:any;
      if(list!=null && list!=undefined && list.length>0)
      {
        for(let i=0;i<list.length;i++){
          if(list[i].id==dropdownvalue)
          {
            dropdownActualValue=list[i].actual_value;
            break;
          }
        }
      }
      this.priceScope.package.forEach(element => {
        if (element.price_scope_name == price_scope_name && dropdownActualValue < this.extraFast_silver_deliver) {
          item.dropdownSilver = 0;
          toastr.error("Extra Fast delivery time should be less than Basic delivery time.");
        }
        else if (element.price_scope_name == price_scope_name && dropdownActualValue == 1) {
          this.allowedSilverExtraFast = true;
          this.extraFast_silver_deliver = 0;
          this.extra_fast_silver_for = "";
          // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
        }
        else if (element.price_scope_name == price_scope_name && dropdownActualValue > this.extraFast_silver_deliver) {
          this.allowedSilverExtraFast = false;
        }
      });
    }
  }
  onDropDownGoldChange(item: any, dropdownvalue: any, list: any, price_scope_name: any) {
   
    if (price_scope_name == "Delivery Time") {
      let dropdownActualValue:any;
      if(list!=null && list!=undefined && list.length>0)
      {
        for(let i=0;i<list.length;i++){
          if(list[i].id==dropdownvalue)
          {
            dropdownActualValue=list[i].actual_value;
            break;
          }
        }
      }
      this.priceScope.package.forEach(element => {
        if (element.price_scope_name == price_scope_name && dropdownActualValue < this.extraFast_gold_deliver) {
          item.dropdownGold = 0;
          toastr.error("Extra Fast delivery time should be less than Basic delivery time.");
        }
        else if (element.price_scope_name == price_scope_name && dropdownActualValue == 1) {
          this.allowedGoldExtraFast = true;
          this.extraFast_gold_deliver = 0;
          this.extra_fast_gold_for = "";
          // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
        }
        else if (element.price_scope_name == price_scope_name && dropdownActualValue > this.extraFast_gold_deliver) {
          this.allowedGoldExtraFast = false;
        }
      });
    }
  }
  onDropDownPlatinumChange(item: any, dropdownvalue: any, list: any, price_scope_name: any) {
    if (price_scope_name == "Delivery Time") {
      let dropdownActualValue:any;
      if(list!=null && list!=undefined && list.length>0)
      {
        for(let i=0;i<list.length;i++){
          if(list[i].id==dropdownvalue)
          {
            dropdownActualValue=list[i].actual_value;
            break;
          }
        }
      }
      this.priceScope.package.forEach(element => {
        if (element.price_scope_name == price_scope_name && dropdownActualValue < this.extraFast_platinum_deliver) {
          item.dropdownPlatinum = 0;
          toastr.error("Extra Fast delivery time should be less than Basic delivery time.");
        }
        else if (element.price_scope_name == price_scope_name && dropdownActualValue == 1) {
          this.allowedPlatinumExtraFast = true;
          this.extraFast_platinum_deliver = 0;
          this.extra_fast_platinum_for = "";
          // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
        }
        else if (element.price_scope_name == price_scope_name && dropdownActualValue > this.extraFast_platinum_deliver) {
          this.allowedPlatinumExtraFast = false;
        }
      });
    }
  }
  // ========================================
  // update active gig price packages 
  // ========================================
  updateActiveGigPackages() {
    let gig_pricing = {
      gig_id: 0,
      gigs_price: {
        is_price_package: true,
        gigs_price: [
          {
            price_type_id: 0,
            price: "",
            price_type_details: [
              {
                sub_category_price_scope_id: 0,
                sub_category_price_scope_dtl_id: 0,
                value: ""
              }
            ]
          }
        ]
      },
      gigs_extra_price: [
        {
          price_extra_service_id: 0,
          price_type_id: 0,
          extra_price: "",
          extra_days_id: 0,
          extra_day: 0
        }
      ],
      extra_service_custom: [
        {
          title: "",
          description: "",
          extra_price: "",
          extra_days: 0,
          extra_day: 0
        }
      ]
    }

    gig_pricing.gig_id = this.valueID;


    gig_pricing.gigs_price.is_price_package = this.packageCount == true ? true : false;
    let price_type_detailsSilver: any = [];
    let price_type_detailsGold: any = [];
    let price_type_detailsPlatinum: any = [];

    for (let i = 0; i < this.priceScope.package.length; i++) {
      if ((this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) && this.packageCount == true) {
        price_type_detailsSilver.push({
          //id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: this.priceScope.package[i].txtSilver,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });

        price_type_detailsGold.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_gold,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: this.priceScope.package[i].txtGold,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });

        price_type_detailsPlatinum.push({
          //id: this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: this.priceScope.package[i].txtPlatinum,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        if (this.priceScope.package[i].price_scope_control_type.id == 4) {
          this.dropdownSilverText = this.priceScope.package[i].txtSilver;
          this.dropdownGoldText = this.priceScope.package[i].txtGold;
          this.dropdownPlatinumText = this.priceScope.package[i].txtPlatinum;
        }
      }
      else if ((this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) && this.packageCount == false) {
        price_type_detailsSilver.push({
          //id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: this.priceScope.package[i].txtSilver,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        if (this.priceScope.package[i].price_scope_control_type.id == 4) {
          this.dropdownSilverText = this.priceScope.package[i].txtSilver;
        }
      }
      else if (this.priceScope.package[i].price_scope_control_type.id == 2 && this.packageCount == true) {
        price_type_detailsSilver.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownSilver,
          value: "",
          is_mandatory: this.priceScope.package[i].is_mandatory
        });

        price_type_detailsGold.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_gold,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownGold,
          value: "",
          is_mandatory: this.priceScope.package[i].is_mandatory
        });

        price_type_detailsPlatinum.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownPlatinum,
          value: "",
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
      }
      else if (this.priceScope.package[i].price_scope_control_type.id == 2 && this.packageCount == false) {
        price_type_detailsSilver.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownSilver,
          value: "",
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
      }
      else if (this.priceScope.package[i].price_scope_control_type.id == 3 && this.packageCount == true) {
        price_type_detailsSilver.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: (this.priceScope.package[i].checkBoxSilver == undefined || this.priceScope.package[i].checkBoxSilver == null || this.priceScope.package[i].checkBoxSilver == false) ? "false" : "true",
          is_mandatory: this.priceScope.package[i].is_mandatory
        });

        price_type_detailsGold.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_gold,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: (this.priceScope.package[i].checkBoxGold == undefined || this.priceScope.package[i].checkBoxGold == null || this.priceScope.package[i].checkBoxGold == false) ? "false" : "true",
          is_mandatory: this.priceScope.package[i].is_mandatory
        });

        price_type_detailsPlatinum.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: (this.priceScope.package[i].checkBoxPlatinum == undefined || this.priceScope.package[i].checkBoxPlatinum == null || this.priceScope.package[i].checkBoxPlatinum == false) ? "false" : "true",
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
      }
      else if (this.priceScope.package[i].price_scope_control_type.id == 3 && this.packageCount == false) {
        price_type_detailsSilver.push({
          // id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: (this.priceScope.package[i].checkBoxSilver == undefined || this.priceScope.package[i].checkBoxSilver == null || this.priceScope.package[i].checkBoxSilver == false) ? "false" : "true",
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
      }

    }
    //if (i == this.priceScope.package.length - 1) {
    gig_pricing.gigs_price.gigs_price.push({
      //id: this.priceScope.package[i].gig_price_id_silver,
      price_type_id: 1,
      price: this.dropdownSilverText,
      price_type_details: price_type_detailsSilver
    });
    if (this.packageCount == true) {
      gig_pricing.gigs_price.gigs_price.push({
        // id: this.priceScope.package[i].gig_price_id_gold,
        price_type_id: 2,
        price: this.dropdownGoldText,
        price_type_details: price_type_detailsGold
      });
      gig_pricing.gigs_price.gigs_price.push({
        //id: this.priceScope.package[i].gig_price_id_platinum,
        price_type_id: 3,
        price: this.dropdownPlatinumText,
        price_type_details: price_type_detailsPlatinum
      });
    }
    let filterDataSilver = price_type_detailsSilver;
    if (this.packageCount == true) {
      let filterDataGold = price_type_detailsGold;
      let filterDataPlatinum = price_type_detailsPlatinum;
      if (filterDataGold.filter(gold => (gold.sub_category_price_scope_dtl_id == null || gold.sub_category_price_scope_dtl_id == 0 || gold.sub_category_price_scope_dtl_id == "0" || gold.sub_category_price_scope_dtl_id == undefined)
        && (gold.value == undefined || gold.value == null || gold.value.toString().trim() == "") && gold.is_mandatory == true).length > 0) {
        this.canSavePackagesGold = false;
      }
      else {
        this.canSavePackagesGold = true;
      }
      if (filterDataPlatinum.filter(plati => (plati.sub_category_price_scope_dtl_id == null || plati.sub_category_price_scope_dtl_id == 0 || plati.sub_category_price_scope_dtl_id == "0" || plati.sub_category_price_scope_dtl_id == undefined)
        && (plati.value == undefined || plati.value == null || plati.value.toString().trim() == "") && plati.is_mandatory == true).length > 0) {
        this.canSavePackagesPlatinum = false;
      }
      else {
        this.canSavePackagesPlatinum = true;
      }
    }
    if (filterDataSilver.filter(silver => (silver.sub_category_price_scope_dtl_id == null || silver.sub_category_price_scope_dtl_id == 0 || silver.sub_category_price_scope_dtl_id == "0" || silver.sub_category_price_scope_dtl_id == undefined)
      && (silver.value == undefined || silver.value == null || silver.value.toString().trim() == "") && silver.is_mandatory == true).length > 0) {
      this.canSavePackagesSilver = false;
    }
    else {
      this.canSavePackagesSilver = true;
    }

    // if (gig_pricing.gig_price.gigs_price.length > 0) {
    gig_pricing.gigs_price.gigs_price.splice(gig_pricing.gigs_price.gigs_price.findIndex(x => x.price_type_id == 0), 1);

    //let extra_pricings = {gig_id:0, gigs_extra_price: [], extra_service_custom: [] };
    //extra_pricings.gig_id=this.valueID;
    gig_pricing.extra_service_custom.splice(0, 1);
    gig_pricing.gigs_extra_price.splice(0, 1);
    for (var extra_price = 0; extra_price < this.extraServicesList.services.length; extra_price++) {
      if (this.extraServicesList.services[extra_price].is_custom == false &&
        this.extraServicesList.services[extra_price].is_selected == true) {


        if (this.extraServicesList.services[extra_price].id == 1) {

          gig_pricing.gigs_extra_price.push({
            price_extra_service_id: this.extraServicesList.services[extra_price].id, price_type_id: 1, extra_price: this.extra_fast_silver_for, extra_days_id: this.extraFast_silver_deliver, extra_day: this.extraFast_silver_deliver
          })
          if (this.packageCount == true) {
            gig_pricing.gigs_extra_price.push({
              price_extra_service_id: this.extraServicesList.services[extra_price].id, price_type_id: 2, extra_price: this.extra_fast_gold_for, extra_days_id: this.extraFast_gold_deliver, extra_day: this.extraFast_gold_deliver
            })
            gig_pricing.gigs_extra_price.push({
              price_extra_service_id: this.extraServicesList.services[extra_price].id, price_type_id: 3, extra_price: this.extra_fast_platinum_for, extra_days_id: this.extraFast_platinum_deliver, extra_day: this.extraFast_platinum_deliver
            })
          }
        }
        else {

          gig_pricing.gigs_extra_price.push({
            // id: this.extraServicesList.services[extra_price].gig_price_extra_service_id == undefined ? 0 :
            // this.extraServicesList.services[extra_price].gig_price_extra_service_id,
            price_extra_service_id: this.extraServicesList.services[extra_price].id,
            price_type_id: 1,
            extra_price: this.extraServicesList.services[extra_price].service_price.toString(),
            extra_days_id: Number(this.extraServicesList.services[extra_price].service_days),
            extra_day: Number(this.extraServicesList.services[extra_price].service_days),
          });
        }
      }
      else if (this.extraServicesList.services[extra_price].is_custom == true &&
        this.extraServicesList.services[extra_price].is_selected == true) {
        gig_pricing.extra_service_custom.push({
          //id: 0,
          title: this.extraServicesList.services[extra_price].title,
          description: this.extraServicesList.services[extra_price].description,
          extra_price: this.extraServicesList.services[extra_price].service_price.toString(),
          extra_days: Number(this.extraServicesList.services[extra_price].service_days),
          extra_day: Number(this.extraServicesList.services[extra_price].service_days),
        });
      }
    }
    this.progress.show();
    if (this.canSavePackagesGold == true && this.canSavePackagesPlatinum == true && this.canSavePackagesSilver == true) {
      this.httpServices.request("post", "gigs_extra/active-gig-update", null, null, gig_pricing).subscribe((data) => {
        this.showGigInfo = true;
        this.showFaq = true;
        this.showPricing = true;
        this.showGallery = true;
        this.activeTab = "gig_faq";
        this.activeGigSaved = true;
        this.gig_draft_Id = data.gig_draft_id;
        this.valueID = data.gig_draft_id;
      });
    }
    else {
      toastr.error("Please fill all mandatory details.");
      this.progress.hide();
    }

    // }

  }
  // ===========================================
  // update pricing packages 
  // ===========================================
  updatePackages() {
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id != 5 && this.gig_status_id != 6) {
      let gig_pricing_package = {
        pricings: []
      };
      let price_type_detailsSilver: any = [];
      let price_type_detailsGold: any = [];
      let price_type_detailsPlatinum: any = [];

      for (let i = 0; i < this.priceScope.package.length; i++) {
        if ((this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) && this.packageCount == true) {
          price_type_detailsSilver.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: null,
            value: this.priceScope.package[i].txtSilver,
            is_mandatory: this.priceScope.package[i].is_mandatory
          });

          price_type_detailsGold.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_gold == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_gold,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: null,
            value: this.priceScope.package[i].txtGold,
            is_mandatory: this.priceScope.package[i].is_mandatory
          });

          price_type_detailsPlatinum.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: null,
            value: this.priceScope.package[i].txtPlatinum,
            is_mandatory: this.priceScope.package[i].is_mandatory
          });
          if (this.priceScope.package[i].price_scope_control_type.id == 4) {
            this.dropdownSilverText = this.priceScope.package[i].txtSilver;
            this.dropdownGoldText = this.priceScope.package[i].txtGold;
            this.dropdownPlatinumText = this.priceScope.package[i].txtPlatinum;
          }
        }
        else if ((this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) && this.packageCount == false) {
          price_type_detailsSilver.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: null,
            value: this.priceScope.package[i].txtSilver,
            is_mandatory: this.priceScope.package[i].is_mandatory
          });
          if (this.priceScope.package[i].price_scope_control_type.id == 4) {
            this.dropdownSilverText = this.priceScope.package[i].txtSilver;
          }
        }
        else if (this.priceScope.package[i].price_scope_control_type.id == 2 && this.packageCount == true) {
          price_type_detailsSilver.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownSilver,
            value: "",
            is_mandatory: this.priceScope.package[i].is_mandatory
          });

          price_type_detailsGold.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_gold == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_gold,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownGold,
            value: "",
            is_mandatory: this.priceScope.package[i].is_mandatory
          });

          price_type_detailsPlatinum.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownPlatinum,
            value: "",
            is_mandatory: this.priceScope.package[i].is_mandatory
          });
        }
        else if (this.priceScope.package[i].price_scope_control_type.id == 2 && this.packageCount == false) {
          price_type_detailsSilver.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownSilver,
            value: "",
            is_mandatory: this.priceScope.package[i].is_mandatory
          });
        }
        else if (this.priceScope.package[i].price_scope_control_type.id == 3 && this.packageCount == true) {
          price_type_detailsSilver.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: null,
            value: (this.priceScope.package[i].checkBoxSilver == undefined || this.priceScope.package[i].checkBoxSilver == null || this.priceScope.package[i].checkBoxSilver == false) ? "false" : "true",
            is_mandatory: this.priceScope.package[i].is_mandatory
          });

          price_type_detailsGold.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_gold == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_gold,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: null,
            value: (this.priceScope.package[i].checkBoxGold == undefined || this.priceScope.package[i].checkBoxGold == null || this.priceScope.package[i].checkBoxGold == false) ? "false" : "true",
            is_mandatory: this.priceScope.package[i].is_mandatory
          });

          price_type_detailsPlatinum.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_platinum,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: null,
            value: (this.priceScope.package[i].checkBoxPlatinum == undefined || this.priceScope.package[i].checkBoxPlatinum == null || this.priceScope.package[i].checkBoxPlatinum == false) ? "false" : "true",
            is_mandatory: this.priceScope.package[i].is_mandatory
          });
        }
        else if (this.priceScope.package[i].price_scope_control_type.id == 3 && this.packageCount == false) {
          price_type_detailsSilver.push({
            id: this.priceScope.package[i].gig_sub_category_price_dtl_id_silver == undefined ? 0 : this.priceScope.package[i].gig_sub_category_price_dtl_id_silver,
            sub_category_price_scope_id: this.priceScope.package[i].id,
            sub_category_price_scope_dtl_id: null,
            value: (this.priceScope.package[i].checkBoxSilver == undefined || this.priceScope.package[i].checkBoxSilver == null || this.priceScope.package[i].checkBoxSilver == false) ? "false" : "true",
            is_mandatory: this.priceScope.package[i].is_mandatory
          });
        }
        if (i == this.priceScope.package.length - 1) {
          gig_pricing_package.pricings.push({
            id: this.priceScope.package[i].gig_price_id_silver == undefined ? 0 : this.priceScope.package[i].gig_price_id_silver,
            price_type_id: 1,
            price: this.dropdownSilverText,
            price_type_details: price_type_detailsSilver
          });
          gig_pricing_package.pricings.push({
            id: this.priceScope.package[i].gig_price_id_gold == undefined ? 0 : this.priceScope.package[i].gig_price_id_gold,
            price_type_id: 2,
            price: this.dropdownGoldText,
            price_type_details: price_type_detailsGold
          });
          gig_pricing_package.pricings.push({
            id: this.priceScope.package[i].gig_price_id_platinum == undefined ? 0 : this.priceScope.package[i].gig_price_id_platinum,
            price_type_id: 3,
            price: this.dropdownPlatinumText,
            price_type_details: price_type_detailsPlatinum
          });
        }


      }
      let filterDataSilver = gig_pricing_package.pricings.filter(element => element.price_type_id == 1);
      if (this.packageCount == true) {
        let filterDataGold = gig_pricing_package.pricings.filter(element => element.price_type_id == 2);
        let filterDataPlatinum = gig_pricing_package.pricings.filter(element => element.price_type_id == 3);
        if (filterDataGold[0].price_type_details.filter(gold => (gold.sub_category_price_scope_dtl_id == null || gold.sub_category_price_scope_dtl_id == 0 || gold.sub_category_price_scope_dtl_id == "0" || gold.sub_category_price_scope_dtl_id == undefined)
          && (gold.value == undefined || gold.value == null || gold.value.toString().trim() == "") && gold.is_mandatory == true).length > 0) {
          this.canSavePackagesGold = false;
        }
        else {
          this.canSavePackagesGold = true;
        }
        if (filterDataPlatinum[0].price_type_details.filter(plati => (plati.sub_category_price_scope_dtl_id == null || plati.sub_category_price_scope_dtl_id == 0 || plati.sub_category_price_scope_dtl_id == "0" || plati.sub_category_price_scope_dtl_id == undefined)
          && (plati.value == undefined || plati.value == null || plati.value.toString().trim() == "") && plati.is_mandatory == true).length > 0) {
          this.canSavePackagesPlatinum = false;
        }
        else {
          this.canSavePackagesPlatinum = true;
        }
      }
      if (filterDataSilver[0].price_type_details.filter(silver => (silver.sub_category_price_scope_dtl_id == null || silver.sub_category_price_scope_dtl_id == 0 || silver.sub_category_price_scope_dtl_id == "0" || silver.sub_category_price_scope_dtl_id == undefined)
        && (silver.value == undefined || silver.value == null || silver.value.toString().trim() == "") && silver.is_mandatory == true).length > 0) {
        this.canSavePackagesSilver = false;
      }
      else {
        this.canSavePackagesSilver = true;
      }
      if (this.canSavePackagesSilver == true && this.canSavePackagesGold == true && this.canSavePackagesPlatinum == true) {
        if (gig_pricing_package.pricings.length > 0) {
          if (this.packageCount == false) {
            gig_pricing_package.pricings.splice(gig_pricing_package.pricings.findIndex(x => x.price_type_details.length == 0), 1);
            gig_pricing_package.pricings.splice(gig_pricing_package.pricings.findIndex(x => x.price_type_details.length == 0), 1);
          }
          this.httpServices.request("patch", "gigs/draft-pricing-update/" + this.valueID, null, null, gig_pricing_package).subscribe((data) => {
            if (data) {
            }
            this.showGigInfo = true;
            this.showFaq = true;
            this.showPricing = true;
            this.showGallery = true;
          });
          // }
          // else {
          //toastr.error("Please fill all mandatory details.");
          this.progress.hide();
          // }

        }
      }
      else {
        toastr.error("Please fill all mandatory details.");
        this.progress.hide();
      }

    }

  }


  // ===========================================
  // Update extra services
  // ===========================================
  updateExtraServices() {
    if (this.activeGigSaved == true) {
      this.gig_status_id = 1;
    }
    if (this.gig_status_id != 5 && this.gig_status_id != 6) {
      let extra_pricings = { pricings: [], extra_service_custom: [] };
      // this.extraServicesList.services.forEach(element=>{
      //   if(element.title=="Extra fast delivery" && element.is_selected==true)
      //   {
      //     if(this.extraFast_silver_deliver==0 && this.extraFast_gold_deliver==0 && this.extraFast_platinum_deliver==0)
      //     {
      //       element.is_selected=false;
      //     }
      //   }
      // })
      for (var extra_price = 0; extra_price < this.extraServicesList.services.length; extra_price++) {
        if (this.extraServicesList.services[extra_price].is_custom == false &&
          this.extraServicesList.services[extra_price].is_selected == true) {
          // need to check condition here
          if (this.extraServicesList.services[extra_price].id == 1) {
            if (this.extraFast_silver_deliver != null && this.extraFast_silver_deliver != undefined && this.extraFast_silver_deliver != 0 && this.extraFast_silver_deliver != "") {
              extra_pricings.pricings.push({
                id: this.extraServicesList.services[extra_price].gig_price_extra_service_id == undefined ? 0 :
                  this.extraServicesList.services[extra_price].gig_price_extra_service_id,
                price_extra_service_id: this.extraServicesList.services[extra_price].id, price_type: 1, extra_price: this.extra_fast_silver_for, extra_days: this.extraFast_silver_deliver, extra_day: this.extraFast_silver_deliver
              })
            }
            if (this.packageCount == true) {
              if (this.extraFast_gold_deliver != null && this.extraFast_gold_deliver != undefined && this.extraFast_gold_deliver != 0 && this.extraFast_gold_deliver != "") {
                extra_pricings.pricings.push({
                  id: this.extraServicesList.services[extra_price].gig_price_extra_service_id == undefined ? 0 :
                    this.extraServicesList.services[extra_price].gig_price_extra_service_id,
                  price_extra_service_id: this.extraServicesList.services[extra_price].id, price_type: 2, extra_price: this.extra_fast_gold_for, extra_days: this.extraFast_gold_deliver, extra_day: this.extraFast_gold_deliver
                })
              }
              if (this.extraFast_platinum_deliver != null && this.extraFast_platinum_deliver != undefined && this.extraFast_platinum_deliver != 0 && this.extraFast_platinum_deliver != "") {
                extra_pricings.pricings.push({
                  id: this.extraServicesList.services[extra_price].gig_price_extra_service_id == undefined ? 0 :
                    this.extraServicesList.services[extra_price].gig_price_extra_service_id,
                  price_extra_service_id: this.extraServicesList.services[extra_price].id, price_type: 3, extra_price: this.extra_fast_platinum_for, extra_days: this.extraFast_platinum_deliver, extra_day: this.extraFast_platinum_deliver
                })
              }
            }
          }
          else {
            extra_pricings.pricings.push({
              id: this.extraServicesList.services[extra_price].gig_price_extra_service_id == undefined ? 0 :
                this.extraServicesList.services[extra_price].gig_price_extra_service_id,
              price_extra_service_id: this.extraServicesList.services[extra_price].id,
              price_type: 1,
              extra_price: this.extraServicesList.services[extra_price].service_price,
              extra_days: Number(this.extraServicesList.services[extra_price].service_days),
              extra_day: Number(this.extraServicesList.services[extra_price].service_days),
            });
          }

        }
        else if (this.extraServicesList.services[extra_price].is_custom == true &&
          this.extraServicesList.services[extra_price].is_selected == true) {
          extra_pricings.extra_service_custom.push({
            id: 0,
            title: this.extraServicesList.services[extra_price].title,
            description: this.extraServicesList.services[extra_price].description,
            extra_price: this.extraServicesList.services[extra_price].service_price,
            extra_days: Number(this.extraServicesList.services[extra_price].service_days),
            extra_day: Number(this.extraServicesList.services[extra_price].service_days),
          });
        }
      }
      if (extra_pricings.pricings.length > 0) {

        this.httpServices.request("patch", "gigs/draft-pricing-extra-services-update/" + this.valueID, null, null, extra_pricings).subscribe((data) => {
          if (data) {
            //toastr.success('')
          }
          toastr.success('Record updated successfully.')
        });
      }
    }

  }

  // ===========================================
  // Save packages 
  // ===========================================
  public canSavePackagesSilver: boolean = true;
  public canSavePackagesGold: boolean = true;
  public canSavePackagesPlatinum: boolean = true;
  savePackages() {

    let gig_pricing_package = {
      gig_draft_id: 0,
      is_price_package: true,
      gigs_price: [
        {
          price_type_id: 0,
          price: "",
          price_type_details: [
            {
              sub_category_price_scope_id: 0,
              sub_category_price_scope_dtl_id: 0,
              value: ""
            }
          ]
        }
      ]
    }
    let price_type_detailsTemp: any = [
      {
        sub_category_price_scope_id: 0,
        sub_category_price_scope_dtl_id: 0,
        value: "",
        price_type_id: 0,
        is_mandatory: false
      }
    ]
    for (let i = 0; i < this.priceScope.package.length; i++) {

      if ((this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) && this.packageCount == true) {
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].txtSilver, price_type_id: 1
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null, value: this.priceScope.package[i].txtSilver,
          price_type_id: 1,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });


        // this.packageList[i].package.price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].txtGold, price_type_id: 2
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: this.priceScope.package[i].txtGold, price_type_id: 2,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        // this.packageList[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].txtPlatinum, price_type_id: 3
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: this.priceScope.package[i].txtPlatinum, price_type_id: 3,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        if (this.priceScope.package[i].price_scope_control_type.id == 4) {
          this.dropdownSilverText = this.priceScope.package[i].txtSilver;
          this.dropdownGoldText = this.priceScope.package[i].txtGold;
          this.dropdownPlatinumText = this.priceScope.package[i].txtPlatinum;
        }
      }
      else if ((this.priceScope.package[i].price_scope_control_type.id == 1 || this.priceScope.package[i].price_scope_control_type.id == 4) && this.packageCount == false) {
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].txtSilver, price_type_id: 1
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: this.priceScope.package[i].txtSilver,
          price_type_id: 1,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        if (this.priceScope.package[i].price_scope_control_type.id == 4) {
          this.dropdownSilverText = this.priceScope.package[i].txtSilver;
        }
      }
      else if (this.priceScope.package[i].price_scope_control_type.id == 2 && this.packageCount == true) {
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].dropdownSilver, price_type_id: 1
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownSilver,
          value: "",
          price_type_id: 1,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].dropdownGold, price_type_id: 2
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownGold,
          value: "",
          price_type_id: 2,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].dropdownPlatinum, price_type_id: 3
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownPlatinum,
          value: "",
          price_type_id: 3,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
      }
      else if (this.priceScope.package[i].price_scope_control_type.id == 2 && this.packageCount == false) {
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].dropdownSilver, price_type_id: 1
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: this.priceScope.package[i].dropdownSilver,
          value: "",
          price_type_id: 1,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
      }
      else if (this.priceScope.package[i].price_scope_control_type.id == 3 && this.packageCount == true) {
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].checkBoxSilver, price_type_id: 1
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: (this.priceScope.package[i].checkBoxSilver == undefined || this.priceScope.package[i].checkBoxSilver == null || this.priceScope.package[i].checkBoxSilver == false) ? "false" : "true",
          price_type_id: 1,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].checkBoxGold, price_type_id: 2
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: (this.priceScope.package[i].checkBoxGold == undefined || this.priceScope.package[i].checkBoxGold == null || this.priceScope.package[i].checkBoxGold == false) ? "false" : "true",
          price_type_id: 2,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].checkBoxPlatinum, price_type_id: 3
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: (this.priceScope.package[i].checkBoxPlatinum == undefined || this.priceScope.package[i].checkBoxPlatinum == null || this.priceScope.package[i].checkBoxPlatinum == false) ? "false" : "true",
          price_type_id: 3,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
      }
      else if (this.priceScope.package[i].price_scope_control_type.id == 3 && this.packageCount == false) {
        // this.packageList.package[i].price_type_details.push({
        //   sub_category_price_scope_id: this.packageList.package[i].sub_category_price_scope.id, sub_category_price_scope_dtl_id: this.packageList.package[i].id,
        //   value: this.packageList.package[i].checkBoxSilver, price_type_id: 1
        // });
        price_type_detailsTemp.push({
          sub_category_price_scope_id: this.priceScope.package[i].id,
          sub_category_price_scope_dtl_id: null,
          value: (this.priceScope.package[i].checkBoxSilver == undefined || this.priceScope.package[i].checkBoxSilver == null || this.priceScope.package[i].checkBoxSilver == false) ? "false" : "true",
          price_type_id: 1,
          is_mandatory: this.priceScope.package[i].is_mandatory
        });
      }

    }

    let filterDataSilver = price_type_detailsTemp.filter(element => element.price_type_id == 1);

    if (filterDataSilver.length > 0) {
      gig_pricing_package.gigs_price = [];
    }
    gig_pricing_package.gigs_price.push({ price_type_id: 1, price: this.dropdownSilverText, price_type_details: filterDataSilver });

    if (this.packageCount == true) {
      let filterDataGold = price_type_detailsTemp.filter(element => element.price_type_id == 2);
      gig_pricing_package.gigs_price.push({ price_type_id: 2, price: this.dropdownGoldText, price_type_details: filterDataGold });
      let filterDataPlatinum = price_type_detailsTemp.filter(element => element.price_type_id == 3);
      gig_pricing_package.gigs_price.push({ price_type_id: 3, price: this.dropdownPlatinumText, price_type_details: filterDataPlatinum });
      if (filterDataGold.filter(gold => (gold.sub_category_price_scope_dtl_id == null || gold.sub_category_price_scope_dtl_id == 0 || gold.sub_category_price_scope_dtl_id == "0" || gold.sub_category_price_scope_dtl_id == undefined)
        && (gold.value == undefined || gold.value == null || gold.value.toString().trim() == "") && gold.is_mandatory == true).length > 0) {
        this.canSavePackagesGold = false;
      }
      else {
        this.canSavePackagesGold = true;
      }
      if (filterDataPlatinum.filter(plati => (plati.sub_category_price_scope_dtl_id == null || plati.sub_category_price_scope_dtl_id == 0 || plati.sub_category_price_scope_dtl_id == "0" || plati.sub_category_price_scope_dtl_id == undefined)
        && (plati.value == undefined || plati.value == null || plati.value.toString().trim() == "") && plati.is_mandatory == true).length > 0) {
        this.canSavePackagesPlatinum = false;
      }
      else {
        this.canSavePackagesPlatinum = true;
      }
    }
    if (filterDataSilver.filter(silver => (silver.sub_category_price_scope_dtl_id == null || silver.sub_category_price_scope_dtl_id == 0 || silver.sub_category_price_scope_dtl_id == "0" || silver.sub_category_price_scope_dtl_id == undefined)
      && (silver.value == undefined || silver.value == null || silver.value.toString().trim() == "") && silver.is_mandatory == true).length > 0) {
      this.canSavePackagesSilver = false;
    }
    else {
      this.canSavePackagesSilver = true;
    }

    gig_pricing_package.gig_draft_id = this.gig_draft_Id;
    if (this.packageCount == true) {
      gig_pricing_package.is_price_package = true;
    }
    else {
      gig_pricing_package.is_price_package = false;
    }
    this.progress.show();
    // gig_pricing_package.gigs_price.splice(gig_pricing_package.gigs_price.findIndex(x => x.price_type_details == null || x.price_type_details == undefined || x.price_type_details.length == 0));
    // gig_pricing_package.gigs_price.splice(gig_pricing_package.gigs_price.findIndex(x => x.price_type_id == 0), 1);
    if (this.canSavePackagesSilver == true && this.canSavePackagesGold == true && this.canSavePackagesPlatinum == true) {
      this.httpServices.request("post", "gigs/draft-pricing", null, null, gig_pricing_package).subscribe((data) => {
        toastr.success("Records saved successfully");
        this.progress.hide();
      }, error => {
        this.progress.hide();
        console.log(error);
      });
    }
    else {
      toastr.error("Please fill all mandatory details.");
      this.progress.hide();
    }
  }
  // ========================================
  // Get Extra services List 
  // ========================================
  public daysListForExtraGig: any;
  public priceListForExtraGig: any;
  getExtraServicesList() {
    this.progress.show();

    this.httpServices.request("get", "extra_services/", null, null, null).subscribe((data) => {
      if (data != null && data != undefined && data != '') {
        // data.forEach(element => {
        //   this.extraServicesList.services.push({
        //     id: element.id, title: element.title, description: element.description, is_price_type_applicable: element.is_price_type_applicable,
        //     heading_1: element.heading_1, heading_1_master_name: element.heading_1_master_name, heading_2: element.heading_2, heading_2_master_name: element.heading_2_master_name,
        //     help: element.help, is_selected: false, service_price: '', service_days: 0, extraFastDeliveryDescription: [],is_custom:false
        //   });
        // });
        this.extraServicesList.services = data;
        this.extraServicesList.services.forEach(element => element.is_custom = false);
        this.daysListForExtraGig = data[0].extra_days;
        this.priceListForExtraGig = data[0].extra_price;
        this.priceScope.package.forEach(element => {
          if (element.price_scope_name == "Delivery Time" && element.dropdownSilver == 1) {
            this.allowedSilverExtraFast = true;
            this.extraFast_silver_deliver = 0;
            this.extra_fast_silver_for = "";
            // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
          }
          if (element.price_scope_name == "Delivery Time" && element.dropdownGold == 1) {
            this.allowedGoldExtraFast = true;
            this.extraFast_gold_deliver = 0;
            this.extra_fast_gold_for = "";
            // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
          }
          if (element.price_scope_name == "Delivery Time" && element.dropdownPlatinum == 1) {
            this.allowedPlatinumExtraFast = true;
            this.extraFast_platinum_deliver = 0;
            this.extra_fast_platinum_for = "";
            // toastr.error("Extra Fast delivery time not allowed as delivery time is just one day.");
          }
        });
      }
      // this.getExtraServicesInfo();
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    })

  }

  cancelcustomService() {
    this.extraServiceTitle = '';
    this.extraServiceDescription = '';
    this.extraServiceCheck = false;
    this.daysListForExtraGig = null;
    this.priceListForExtraGig = null;
    this.extraServicePrice = '';
    this.extraServiceAdditional = 0;
    this.showExtraPriceSection = false;
  }

  addcustomService() {
    this.addextraServiceDone = true;
    this.tempCountOne = this.tempCountOne + 1;
    if (this.extraServiceCheck == true) {
      let extra_service_id: number = 0;
      extra_service_id = this.extraServicesList.services.length + 1;
      this.extraServicesList.services.push({
        id: extra_service_id, gig_price_extra_service_id: 0, title: this.extraServiceTitle, description: this.extraServiceDescription,
        is_price_type_applicable: true,
        heading_1: "for an Extra", heading_1_master_name: '', heading_2: "and additional", heading_2_master_name: '',
        help: '',
        extra_days: this.daysListForExtraGig,
        extra_price: this.priceListForExtraGig,
        is_selected: true, service_price: this.extraServicePrice, service_days: this.extraServiceAdditional,
        extraFastDeliveryDescription: [], is_custom: true
      });
      // this.extraServicesList.services[0].extra_days[0].extra_days_value      
      this.extraServiceTitle = '';
      this.extraServiceDescription = '';
      this.extraServicePrice = '';
      this.extraServiceAdditional = 0;
      this.extraServiceCheck = false;
      this.tempCountTwo = this.tempCountTwo + 1;
      this.showExtraPriceSection = false;
    }
  }
  // =============================
  // on Click of Add gig extra 
  // =============================
  addGigExtra() {

    this.addextraServiceDone = true;
    this.tempCountOne = this.tempCountOne + 1;
    this.showExtraPriceSection = true;
    if (this.extraServiceCheck == true) {
      let extra_service_id: number = 0;
      extra_service_id = this.extraServicesList.services.length + 1;
      this.extraServicesList.services.push({
        id: extra_service_id, gig_price_extra_service_id: 0, title: this.extraServiceTitle, description: this.extraServiceDescription,
        is_price_type_applicable: true,
        heading_1: "for an Extra", heading_1_master_name: '', heading_2: "and additional", heading_2_master_name: '',
        help: '',
        extra_days: this.daysListForExtraGig,
        extra_price: this.priceListForExtraGig,
        is_selected: true, service_price: this.extraServicePrice, service_days: this.extraServiceAdditional,
        extraFastDeliveryDescription: [], is_custom: true
      });
      // this.extraServicesList.services[0].extra_days[0].extra_days_value      
      this.extraServiceTitle = '';
      this.extraServiceDescription = '';
      this.extraServicePrice = '';
      this.extraServiceAdditional = 0;
      this.extraServiceCheck = false;
      this.tempCountTwo = this.tempCountTwo + 1;
    }
  }
  // ============================
  // Save gig Pricing
  // =============================
  saveGigPricing() {
    let flag = true;
    this.extraServicesList.services.forEach(element => {
      if (element.title == "Extra fast delivery" && element.is_selected == true) {
        if (this.extraFast_silver_deliver == 0 && this.extraFast_gold_deliver == 0 && this.extraFast_platinum_deliver == 0) {
          element.is_selected = false;
        }
      }
    })
    this.extraServicesList.services.forEach(element => {

      if (element.is_selected == true) {
        if (element.title == "Extra fast delivery") {
          if (((this.extraFast_silver_deliver == "" || this.extraFast_silver_deliver == "0" || this.extraFast_silver_deliver == 0 || this.extraFast_silver_deliver == undefined || this.extraFast_silver_deliver == null) &&
            this.allowedSilverExtraFast == false) ||
            ((this.extra_fast_silver_for == "" || this.extra_fast_silver_for == "0" || this.extra_fast_silver_for == 0 || this.extra_fast_silver_for == undefined || this.extra_fast_silver_for == null) && this.allowedSilverExtraFast == false)) {
            flag = false;
          }
          if (this.packageCount == true) {
            if (((this.extraFast_gold_deliver == "" || this.extraFast_gold_deliver == "0" || this.extraFast_gold_deliver == 0 || this.extraFast_gold_deliver == undefined || this.extraFast_gold_deliver == null) && this.allowedGoldExtraFast == false) ||
              ((this.extra_fast_gold_for == "" || this.extra_fast_gold_for == "0" || this.extra_fast_gold_for == 0 || this.extra_fast_gold_for == undefined || this.extra_fast_gold_for == null) && this.allowedGoldExtraFast == false)) {
              flag = false;
            }
            if (((this.extraFast_platinum_deliver == "" || this.extraFast_platinum_deliver == "0" || this.extraFast_platinum_deliver == 0 || this.extraFast_platinum_deliver == undefined || this.extraFast_platinum_deliver == null) && this.allowedPlatinumExtraFast == false) ||
              ((this.extra_fast_platinum_for == "" || this.extra_fast_platinum_for == "0" || this.extra_fast_platinum_for == 0 || this.extra_fast_platinum_for == undefined || this.extra_fast_platinum_for == null) && this.allowedPlatinumExtraFast == false)) {
              flag = false;
            }
          }
        }
        else {
          if ((element.service_price == 0 || element.service_price == undefined || element.service_price == null) ||
            (element.service_days == 0 || element.service_days == undefined || element.service_days == null)) {
            flag = false;
          }
        }
      }
    })
    if (flag == true) {
      if (this.pricingListAlreadyPresent == true) {
        if (this.activeGigSaved == true) {
          this.gig_status_id = 1;
        }
        if (this.gig_status_id == 5 || this.gig_status_id == 6) {
          this.updateActiveGigPackages();
          this.getFaqInfo();
        }
        else {
          this.updatePackages();
          if (this.canSavePackagesSilver == true && this.canSavePackagesGold == true && this.canSavePackagesPlatinum == true) {
            if (this.pricingListExtraServiceAlreadyPresent == true) {
              this.updateExtraServices();
              this.getFaqInfo();
            }
            else {
              this.saveExtraServicePackage();
              this.getFaqInfo();
            }
          }
        }
      }
      else {
        this.savePackages();
        if (this.canSavePackagesSilver == true && this.canSavePackagesPlatinum == true && this.canSavePackagesGold == true) {
          if (this.pricingListExtraServiceAlreadyPresent == true) {
            this.updateExtraServices();
            this.getFaqInfo();
          }
          else {
            this.saveExtraServicePackage();
            this.getFaqInfo();
          }
        }
      }
    }
    else {
      toastr.error("Please fill all details in selected services.");

    }
  }

  // ============================
  // Save extra service Pricing 
  // =============================
  saveExtraServicePackage() {
    if (this.addextraServiceDone == true && (this.tempCountOne != this.tempCountTwo) && this.extraServiceCheck == true) {
      this.extraServicesList.services.push({
        id: this.extraServicesList.services.length + 1, gig_price_extra_service_id: 0, title: this.extraServiceTitle,
        description: this.extraServiceDescription, is_price_type_applicable: true,
        heading_1: "for an Extra", heading_1_master_name: '', heading_2: "and additional", heading_2_master_name: '',
        help: '', extra_days: this.daysListForExtraGig, extra_price: this.priceListForExtraGig, is_selected: true,
        service_price: this.extraServicePrice, service_days: this.extraServiceAdditional,
        extraFastDeliveryDescription: [], is_custom: true
      });
    }
    let gig_extra_pricing =
    {
      gig_draft_id: 0,
      gigs_extra_price: [
        {
          price_extra_service_id: 0,
          price_type_id: 0,
          extra_price: "",
          extra_days_id: 0,
          extra_day: 0

        }
      ],
      extra_service_custom: [
        {
          title: "",
          description: "",
          extra_price: "",
          extra_days: 0,
          extra_day: 0
        }
      ]
    }
    let extraServiceListWithoutCustom = [];
    let extraServicelistCustom = [];
    extraServiceListWithoutCustom = this.extraServicesList.services.filter(x => x.is_custom == false);
    extraServicelistCustom = this.extraServicesList.services.filter(x => x.is_custom == true);
    for (let i = 0; i < extraServiceListWithoutCustom.length; i++) {
      if (extraServiceListWithoutCustom[i].id == 1 && extraServiceListWithoutCustom[i].is_selected == true) {
        // extraServiceListWithoutCustom[i].extraFastDeliveryDescription.push({
        //   // title: extraServiceListWithoutCustom[i].title,
        //   // description: extraServiceListWithoutCustom[i].heading_1, 
        //   // extra_price: this.extra_fast_silver_for,
        //   // extra_days: this.extraFast_silver_deliver, price_type_id: 1

        // });
        // extraServiceListWithoutCustom[i].extraFastDeliveryDescription.push({
        //   title: extraServiceListWithoutCustom[i].title,
        //   description: extraServiceListWithoutCustom[i].heading_1, extra_price: this.extra_fast_gold_for,
        //   extra_days: this.extraFast_gold_deliver, price_type_id: 2
        // });
        // extraServiceListWithoutCustom[i].extraFastDeliveryDescription.push({
        //   title: extraServiceListWithoutCustom[i].title,
        //   description: extraServiceListWithoutCustom[i].heading_1, extra_price: this.extra_fast_platinum_for,
        //   extra_days: this.extraFast_platinum_deliver, price_type_id: 3
        // });
        if (this.extraFast_silver_deliver != null && this.extraFast_silver_deliver != undefined && this.extraFast_silver_deliver != "" && this.extraFast_silver_deliver != 0 && this.extraFast_silver_deliver != "0") {
          gig_extra_pricing.gigs_extra_price.push({
            price_extra_service_id: extraServiceListWithoutCustom[i].id, price_type_id: 1, extra_price: this.extra_fast_silver_for, extra_days_id: this.extraFast_silver_deliver, extra_day: this.extraFast_silver_deliver
          })
        }
        if (this.packageCount == true) {
          if (this.extraFast_gold_deliver != null && this.extraFast_gold_deliver != undefined && this.extraFast_gold_deliver != "" && this.extraFast_gold_deliver != 0 && this.extraFast_gold_deliver != "0") {
            gig_extra_pricing.gigs_extra_price.push({
              price_extra_service_id: extraServiceListWithoutCustom[i].id, price_type_id: 2, extra_price: this.extra_fast_gold_for, extra_days_id: this.extraFast_gold_deliver, extra_day: this.extraFast_gold_deliver
            })
          }
          if (this.extraFast_platinum_deliver != null && this.extraFast_platinum_deliver != undefined && this.extraFast_platinum_deliver != "" && this.extraFast_platinum_deliver != 0 && this.extraFast_platinum_deliver != "0") {
            gig_extra_pricing.gigs_extra_price.push({
              price_extra_service_id: extraServiceListWithoutCustom[i].id, price_type_id: 3, extra_price: this.extra_fast_platinum_for, extra_days_id: this.extraFast_platinum_deliver, extra_day: this.extraFast_platinum_deliver
            })
          }
        }

      }
      else if (extraServiceListWithoutCustom[i].id != 1 && extraServiceListWithoutCustom[i].is_selected == true) {
        // extraServiceListWithoutCustom[i].extraFastDeliveryDescription.push({
        //   title: extraServiceListWithoutCustom[i].title,
        //   description: extraServiceListWithoutCustom[i].heading_1, extra_price: extraServiceListWithoutCustom[i].service_price,
        //   extra_days: extraServiceListWithoutCustom[i].service_days, price_type_id: 1
        // });
        gig_extra_pricing.gigs_extra_price.push({
          price_extra_service_id: extraServiceListWithoutCustom[i].id, price_type_id: 1, extra_price: extraServiceListWithoutCustom[i].service_price, extra_days_id: extraServiceListWithoutCustom[i].service_days, extra_day: extraServiceListWithoutCustom[i].service_days
        })
      }

    }
    for (let i = 0; i < extraServicelistCustom.length; i++) {
      if (extraServicelistCustom[i].is_selected == true) {
        gig_extra_pricing.extra_service_custom.push({
          title: extraServicelistCustom[i].title, description: extraServicelistCustom[i].heading_1, extra_price: extraServicelistCustom[i].service_price, extra_days: extraServicelistCustom[i].service_days, extra_day: extraServicelistCustom[i].service_days
        })
      }
    }
    gig_extra_pricing.gigs_extra_price.splice(gig_extra_pricing.gigs_extra_price.findIndex(x => x.price_extra_service_id == 0), 1);
    gig_extra_pricing.extra_service_custom.splice(gig_extra_pricing.extra_service_custom.findIndex(x => x.title == ""), 1);
    // gig_extra_pricing.gig_id = this.gig_draft_Id;
    gig_extra_pricing.gig_draft_id = this.gig_draft_Id;
    this.httpServices.request("post", "gigs/draft-pricing-extra-services", null, null, gig_extra_pricing).subscribe((data) => {
      //toastr.success("Records Saved Successfully.");
      this.activeTab = "gig_faq";
      this.showPricing = true;
      this.showGallery = true;
      this.showGigInfo = true;
      this.showFaq = true;
      this.progress.hide();
    }, error => {
      this.progress.hide();
      console.log(error);
    });
  }


  // ====================================
  // Publish Section 
  // =====================================
  // ====================================
  // On click of finish on Publish tab 
  // ====================================
  publish() {
    if (this.gig_status_id == 2) {
      toastr.error('Gig is already in pending for approval mode.');
      setTimeout(() => { }, 600);
      this.router.navigate(['gig_list'], {});
    }
    else {
      this.progress.show();
      this.httpServices.request("get", "gigs/pre-publish-verify/" + this.gig_draft_Id, null, null, null).subscribe((prePublish) => {
        if (prePublish == null || prePublish == undefined || prePublish.length == 0) {
          this.httpServices.request("patch", "gigs/" + this.gig_draft_Id + "/publish", null, null, null).subscribe((data) => {
            toastr.success("Gig Published Successfully.");
            this.router.navigate(['gig_list'], {});
            this.progress.hide();
          }, error => {
            this.progress.hide();
            console.log(error);
          });
        }
        else {
          this.progress.hide();
          let errorMessage = "";
          for (let i = 0; i < prePublish.length; i++)
            errorMessage = errorMessage + ("<li>" + this.httpMessage.errorMessage_EN[prePublish[i].code] + "</li>");
          toastr.error(errorMessage);
        }
      }, error => {
        this.progress.hide();
        console.log(error);
      });
    }
  }
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }
  onlyNumbersPrice(event: any, controlTypeId: any) {
    if (controlTypeId == 4) {
      var charCode = (event.which) ? event.which : event.keyCode
      if ((charCode >= 48 && charCode <= 57))
        return true;
      return false;
    }
    else {
      return true;
    }
  }
}

interface metaTypeDetails {
  metatypedata:
  [{
    category_id: number,
    sub_category_id: number,
    metadata_type_id: number,
    is_single: boolean,
    is_mandatory: boolean,
    min_value: number,
    max_value: number,
    is_suggest_other: boolean,
    metadata_type_draft_details: [{
      gig_sub_category_metadata_type_draft_id: number,
      metadata_type_dtl_id: number
    }],
    other: string
  }]
}

// interface metaTypeDetails {
//   metatypedata:
//   [{
//     created_date: string,
//     id: number,
//     is_active: boolean,
//     is_allowed_on_request: boolean,
//     is_mandatory: boolean,
//     is_suggest_other: boolean,
//     max_value: number,
//     min_value: number,
//     seq_no: number,
//     metadata_type: {
//       created_date: string,
//       id: number,
//       is_active: boolean,
//       is_mandatory: boolean,
//       is_single: boolean,
//       is_suggest_other: boolean,
//       metadata_type_name: string,
//       modified_date: string
//     },
//     metadata_type_details: [{
//       id: number,
//       metadata_type_dtl: {
//         id: number,
//         is_active: boolean,
//         metadata_type_name: string,
//         seq_no: number
//       },
//       seq_no: number,
//       checked_value: boolean
//     }],
//     sub_category: {
//       category: {
//         id: number,
//         category_name: string,
//         is_active: boolean
//       },
//       id: number,
//       is_active: boolean,
//       modified_date: string,
//       seq_no: number,
//       sub_category_name: string,
//       sub_category_picture: string,
//       sub_category_thumb_picture: string,
//       tagline: string
//     }
//   }]
// }

interface metaSubTypeCheckBoxes {
  metadata_type_draft_details: [{
    gig_sub_category_metadata_type_draft_id: number,
    metadata_type_dtl_id: number
  }],
}
interface imgageList {
  gallery: [
    {
      gig_gallery_type_id: number,
      file_path: string,
      file_path_thumbnail: string
    }
  ]
}
interface priceScope {
  package: [{
    id: number,
    gig_sub_category_price_dtl_id_silver: number,
    gig_sub_category_price_dtl_id_gold: number,
    gig_sub_category_price_dtl_id_platinum: number,
    gig_price_id_silver: number,
    gig_price_id_gold: number,
    gig_price_id_platinum: number,
    seq_no: number,
    is_mandatory: boolean,
    price_scope_name: string,
    is_heading_visible: boolean,
    price_scope_control_type: {
      id: number,
      price_scope_control_type_name: string
    },
    watermark: string,
    default_value: string,
    sub_category_price_scope_details: [
      {
        id: number,
        seq_no: number,
        value: string,
        actual_value:any
      }
    ],
    is_active: boolean,
    created_date: string,
    modified_date: string,
    txtSilver: string,
    txtGold: string,
    txtPlatinum: string,
    dropdownSilver: number,
    dropdownGold: number,
    dropdownPlatinum: number,
    checkBoxSilver: boolean,
    checkBoxGold: boolean,
    checkBoxPlatinum: boolean
  }]
}
interface packageList {
  package: [{
    id: number,
    sub_category_price_scope: {
      id: number,
      sub_category: {
        id: number,
        category: {
          id: number,
          category_name: string,
          is_active: boolean
        },
        sub_category_name: string,
        is_active: boolean,
        created_date: string,
        modified_date: string
      },
      seq_no: number,
      price_scope_name: string,
      is_heading_visible: boolean,
      price_scope_control_type: {
        id: number,
        price_scope_control_type_name: string
      },
      watermark: string,
      default_value: any,
      is_active: boolean,
      created_date: string
      modified_date: string
    },
    seq_no: number,
    value: any
    price_type_details: [
      {
        sub_category_price_scope_id: number,
        sub_category_price_scope_dtl_id: number,
        value: any,
        price_type_id: number
      }
    ]
    txtSilver: string;
    txtGold: string;
    txtPlatinum: string;
    dropdownSilver: number;
    dropdownGold: number;
    dropdownPlatinum: number;
    checkBoxSilver: boolean;
    checkBoxGold: boolean;
    checkBoxPlatinum: boolean
  }]
}
interface metaDataSubTypeList {
  mataData:
  [{
    metaData_draft_id: number,
    created_date: string,
    modified_date: string,
    id: number,
    is_active: boolean,
    is_allowed_on_request: boolean,
    is_mandatory: boolean,
    is_single: boolean,
    is_suggest_other: boolean,
    is_suggest_other_checked_value: boolean,
    is_suggest_other_id: number,
    is_suggest_other_text: string,
    max_value: number,
    min_value: number,
    seq_no: number,
    metadata_type: {
      created_date: string,
      id: number,
      is_active: boolean,
      is_mandatory: boolean,
      is_single: boolean,
      is_suggest_other: boolean,
      metadata_type_name: string,
      modified_date: string
    },
    metadata_type_details: [{
      id: number,
      metadata_type_dtl: {
        id: number,
        is_active: boolean,
        metadata_type_name: string,
        seq_no: number
      },
      seq_no: number,
      checked_value: boolean
    }],
    sub_category: {
      category: {
        id: number,
        category_name: string,
        is_active: boolean
      },
      id: number,
      is_active: boolean,
      modified_date: string,
      seq_no: number,
      sub_category_name: string,
      sub_category_picture: string,
      sub_category_thumb_picture: string,
      tagline: string
    }
  }]
  // [{
  //   id: number,
  //   sub_category_metadata_type: number,
  //   seq_no: number,
  //   metadata_type_dtl: {
  //     id: number,
  //     metadata_type: {
  //       id: number,
  //       metadata_type_name: string,
  //       is_single: boolean,
  //       is_mandatory: boolean,
  //       is_suggest_other: boolean,
  //       is_active: boolean,
  //       created_date: string,
  //       modified_date: string
  //     },
  //     seq_no: number,
  //     metadata_type_name: string,
  //     is_active: boolean,
  //     checkValue: boolean
  //   }
  //   radiovalue: boolean,
  //   checkValue: boolean,
  //   suggestOtherCheck: boolean,
  //   suggestOtherText: string
  // }]
}
interface extraServicesList {
  services: [
    {
      id: number,
      gig_price_extra_service_id: 0,
      title: string,
      description: string,
      is_price_type_applicable: boolean,
      heading_1: string,
      heading_1_master_name: string,
      heading_2: string,
      heading_2_master_name: string,
      help: string,
      extra_days: [{
        id: number,
        seq_no: number,
        extra_days_name: string,
        extra_days_value: number
      }],
      extra_price: [{
        id: number,
        seq_no: number,
        extra_price_name: string,
        extra_price_value: number
      }],
      is_selected: boolean,
      service_price: Number,
      service_days: Number,
      extraFastDeliveryDescription: any[],
      is_custom: boolean
    }]
}
interface searchTags {
  search_Tags: [{
    search_id: number,
    search_keyword: string,
    search_type: string
  }]
}
interface galleryData {
  galleryInfo: [{
    id: number,
    gig_gallery_type: {
      id: number,
      gig_gallery_type_name: string
    },
    file_path: string,
    file_path_thumbnail: string
  }]
}
