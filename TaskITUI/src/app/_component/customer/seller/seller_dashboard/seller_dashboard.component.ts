import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { NgxSpinnerService } from "ngx-spinner";
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { SocialAuthService } from 'angularx-social-login';
import { AuthInfo } from 'src/app/_models/auth-info';

@Component({
  selector: 'app-seller_dashboard',
  templateUrl: './seller_dashboard.component.html',
  styleUrls: ['./seller_dashboard.component.css']
})
export class Seller_dashboardComponent implements OnInit {

  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userId: any;
  public orderCount:any;
  public sellerProfileDetails:any;
  public userName:string;
  public profilePic:any;
  public rating:any;

  constructor(private httpServices: HttpRequestService, private progress: NgxSpinnerService,private encrypt_decrypt: EncryptDecryptService, private authService: SocialAuthService) { }

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    this.userId = this.userAuthInfo.currentUser.user.id
    this.getDashboardCount();
    this.getProfileDetails();
  }

  getDashboardCount(){
    this.progress.show();
    this.httpServices.request('get','sellers/dashboard-count',null,null,null)
    .subscribe((data) =>{
      this.orderCount=data;
      this.progress.hide();
    },error =>{
      console.log(error);
      this.progress.hide();
    });
  }

  getProfileDetails(){
    this.progress.show();
    this.httpServices.request("get",'sellers/info/'+this.userId,'','','').subscribe((data)=>{
      this.sellerProfileDetails=data;

      if(this.sellerProfileDetails!=null && this.sellerProfileDetails!=undefined){
      this.userName= this.sellerProfileDetails.user_name;
      this.profilePic=this.sellerProfileDetails.personal_info.profile_picture;
      }else{
        this.userName= "";
        this.profilePic="assets/img/profile_pic.png";
      }
      //this.rating= this.sellerProfileDetails.rating;
      this.progress.hide();
    },error=>{
      console.log(error);
      this.progress.hide();
    });
  }
}
