/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Seller_offersComponent } from './seller_offers.component';

describe('Seller_offersComponent', () => {
  let component: Seller_offersComponent;
  let fixture: ComponentFixture<Seller_offersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Seller_offersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Seller_offersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
