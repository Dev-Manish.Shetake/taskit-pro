import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { Router } from '@angular/router';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';

declare var jQuery: any;
@Component({
  selector: 'app-seller_offers',
  templateUrl: './seller_offers.component.html',
  styleUrls: ['./seller_offers.component.css']
})
export class Seller_offersComponent implements OnInit {

  constructor(private encrypt_decrypt: EncryptDecryptService, private router: Router, private progress: NgxSpinnerService, private httpServices: HttpRequestService) { }
  public queryParam: any;
  public _queryParams: any;
  public editMode: boolean = false;
  public request_id: any;
  public userAuthInfo: any;
  public offers: offers = {
    result: [
      {
        id: 0,
        gig: {
          id: 0,
          title: "",
          description: "",
          total_views: 0,
          rating: 0,
          rated_no_of_records: 0,
          gallery: [
            {
              id: 0,
              gig_gallery_type: {
                id: 0,
                gig_gallery_type_name: ""
              },
              file_path: "",
              file_path_thumbnail: ""
            }
          ],
          gigs_pricing: {
            gig_price: {
              id: 0,
              price_type: {
                id: 0,
                price_type_name: ""
              },
              price: ""
            },
            gig_price_dtl: [
              {
                id: 0,
                sub_category_price_scope: {
                  id: 0,
                  seq_no: 0,
                  price_scope_name: "",
                  is_heading_visible: false,
                  watermark: "",
                  price_scope_control_type: {
                    id: 0,
                    price_scope_control_type_name: ""
                  }
                },
                sub_category_price_scope_dtl: {
                  id: 0,
                  seq_no: 0,
                  value: "",
                  display_value: "",
                  actual_value: ""
                },
                value: ""
              }]
          },
          created_by: {
            id: 0,
            user_name: "",
            level: {
              id: 0,
              level_name: ""
            },
            rating: "",
            user_info: {
              id: 0,
              first_name: "",
              last_name: "",
              profile_picture: ""
            }
          }
        },
        offer_price: "",
        offer_delivery_time: 0,
        request_offer_details: [
          {
            id: 0,
            sub_category_price_scope_id: 0,
            sub_category_price_scope_dtl_id: 0,
            value: ""
          }
        ],
        gig_description_one: "",
        gig_description_two: "",
        silverCheckBoxDetails: [{
          silverCheckBox: "",
          silverCheckBoxValue: false
        }],
        deliveryTimetoDisplay: "",
        initials: ""
      }]
  }
  public offerInfo: offerInfo = {
    id: 0,
    description: "",
    service_delivery: {
      id: 0,
      seq_no: 0,
      service_delivery_name: "",
      service_delivery_value: 0,
      created_date: "",
      modified_date: ""
    },
    service_delivery_other: 0,
    budget: "",
    seller_offer_count: 0,
    created_by: {
      id: 0,
      user_name: "",
      level: {
        id: 0,
        level_name: ""
      },
      rating: "",
      user_info: {
        id: 0,
        first_name: "",
        last_name: "",
        profile_picture: ""
      }
    },
    request_attachment: [
      {
        id: 0,
        file_path: ""
      }
    ]
  }
  public sortBy: any = "default";
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    if (this.queryParam == null || this.queryParam == undefined) {
      this._queryParams = (sessionStorage.getItem("queryParamsRequest") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      if (this.queryParam != null && this.queryParam != undefined && this._queryParams != "{}") {
        this.editMode = this.queryParam.request_id > 0 ? true : false;
        if (this.editMode == true) {
          this.request_id = this.queryParam.request_id;
          //this.request_id = 32;
          this.getOfferInfo(this.request_id);
          this.getOffers(this.request_id);
        }
      }

    }

  }
  getOffers(request_id) {
    this.progress.show();
    this.httpServices.request('get', "requests/seller-request-offer-list/" + request_id + "?sort_by=" + this.sortBy, null, null, null).subscribe((data) => {
      this.offers.result = data.results;
      // this.offers.result.forEach(elementMain=>

      for (let i = 0; i < this.offers.result.length; i++) {
        this.offers.result[i].offer_price = this.offers.result[i].offer_price == null ? '' : this.offers.result[i].offer_price.replace(".00", "");
        if (this.offers.result[i].gig != null && this.offers.result[i].gig != undefined) {
          this.offers.result[i].initials = this.offers.result[i].gig.created_by.user_name.charAt(0).toUpperCase();

          let silverCheckBoxDetails_Temp: any = [{
            silverCheckBox: "",
            silverCheckBoxValue: false
          }]
          this.offers.result[i].silverCheckBoxDetails = silverCheckBoxDetails_Temp;

          if (this.offers.result[i].gig.description.length > 150) {
            this.offers.result[i].gig_description_one = this.offers.result[i].gig.description.substring(0, 150);
            this.offers.result[i].gig_description_two = this.offers.result[i].gig.description.substring(150, this.offers.result[i].gig.description.length);
          }
          else {
            this.offers.result[i].gig_description_one = this.offers.result[i].gig.description;
          }
          if (this.offers.result[i].gig.gigs_pricing.gig_price.price != null &&
            this.offers.result[i].gig.gigs_pricing.gig_price.price != undefined &&
            this.offers.result[i].gig.gigs_pricing.gig_price.price != "") {
            this.offers.result[i].gig.gigs_pricing.gig_price.price = this.offers.result[i].gig.gigs_pricing.gig_price.price.replace(".00", "");
          }
          //this.offers.result[i].gig.gigs_pricing.gig_price_dtl.forEach(element => {
          for (let k = 0; k < this.offers.result[i].gig.gigs_pricing.gig_price_dtl.length; k++) {

            if (this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name == "Package Name") {
              // this.gig_pricing_block.silverPackageName = element.value;
            }
            else if (this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name == "Package Description") {
              // this.gig_pricing_block.silverPackageDescription = element.value;
            }
            else {
              if (this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name == "Price") {

              }
              else if (this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name == "Delivery Time") {
                this.offers.result[i].deliveryTimetoDisplay = this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.display_value;
              }
              else {
                if (this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl == null || this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl == undefined) {
                  this.offers.result[i].silverCheckBoxDetails.push({ silverCheckBox: this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name, silverCheckBoxValue: this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].value == "true" ? true : false });
                }
                else {
                  this.offers.result[i].silverCheckBoxDetails.push({
                    silverCheckBox: this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.display_value, silverCheckBoxValue: (this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.display_value == "" ||
                      this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.display_value == null || this.offers.result[i].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.display_value == undefined) ? false : true
                  });
                }
              }

            }
          }
        }
      }
      this.progress.hide();
    });
  }

  public isOfferList: boolean = false;
  getOffersList(offersList: any) {
    let tempOfferList: any;
    tempOfferList = offersList.filter(x => x.gig != null);
    if (tempOfferList.length > 0) {
      this.isOfferList = true;
    }
    return tempOfferList;
  }

  getOfferInfo(request_id) {
    this.httpServices.request('get', "requests/seller-request-offer-info/" + request_id, null, null, null).subscribe((data) => {
      this.offerInfo = data;
      if (this.offerInfo.budget != null && this.offerInfo.budget != undefined && this.offerInfo.budget != "") {
        this.offerInfo.budget = this.offerInfo.budget.replace(".00", "");
      }
    });
  }
  public showFullDescription: boolean = false;
  onSeeMoreClick() {
    this.showFullDescription = true;
  }
  onClickRemove(request_offer_id: any) {
    this.progress.show();
    this.httpServices.request('delete', "requests/offer-delete/" + request_offer_id, null, null, null).subscribe((data) => {
      this.getOffers(this.request_id);
    });
  }
  onOrderNowClick(request_offer_id: any) {
    let orderCreateBody: any = {
      request_offer_id: 0,
      gig_id: 0,
      amount: "",
      delivery_time: 0,
      order_gig_price: {
        price_type_id: 0,
        price: "",
        quantity: 0,
        amount: ""
      },
      sub_category_price_details: [
        {
          sub_category_price_scope_id: 0,
          sub_category_price_scope_dtl_id: 0,
          value: ""
        }
      ]
    }
    let singleOffer = this.offers.result.filter(element => element.id == request_offer_id);
    orderCreateBody.request_offer_id = request_offer_id;
    orderCreateBody.gig_id = singleOffer[0].gig.id;
    orderCreateBody.amount = singleOffer[0].gig.gigs_pricing.gig_price.price;
    orderCreateBody.delivery_time = singleOffer[0].deliveryTimetoDisplay;
    orderCreateBody.order_gig_price.price_type_id = singleOffer[0].gig.gigs_pricing.gig_price.price_type.id;
    orderCreateBody.order_gig_price.price = singleOffer[0].gig.gigs_pricing.gig_price.price;
    orderCreateBody.order_gig_price.quantity = 1;
    orderCreateBody.order_gig_price.amount = singleOffer[0].gig.gigs_pricing.gig_price.price;
    for (let k = 0; k < singleOffer[0].gig.gigs_pricing.gig_price_dtl.length; k++) {

      if (singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name == "Package Name") {
        // this.gig_pricing_block.silverPackageName = element.value;
      }
      else if (singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name == "Package Description") {
        // this.gig_pricing_block.silverPackageDescription = element.value;
      }
      else {
        if (singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name == "Price") {

        }
        else if (singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.price_scope_name == "Delivery Time") {
          orderCreateBody.delivery_time = Number(singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.actual_value);
        }
        else {
          if (singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl == null || singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl == undefined) {
            orderCreateBody.sub_category_price_details.push({
              sub_category_price_scope_id: singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.id,
              sub_category_price_scope_dtl_id: null,
              value: singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].value == "true" ? "true" : "false"
            });
          }
          else {
            orderCreateBody.sub_category_price_details.push({
              sub_category_price_scope_id: singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope.id,
              sub_category_price_scope_dtl_id: singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.id,
              value: (singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.display_value == "" ||
                singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.display_value == null ||
                singleOffer[0].gig.gigs_pricing.gig_price_dtl[k].sub_category_price_scope_dtl.display_value == undefined) ? "false" : "true"
            });
          }
        }

      }

    }
    orderCreateBody.sub_category_price_details.splice(orderCreateBody.sub_category_price_details.findIndex(x => x.sub_category_price_scope_id == 0), 1);
    this.progress.show();
    this.httpServices.request('post', "requests/request-offer-order-create", null, null, orderCreateBody).subscribe((data) => {
      let passQueryParam = {
        id: singleOffer[0].gig.id, gig_status_id: 0, price_type_id: singleOffer[0].gig.gigs_pricing.gig_price.price_type.id,
        extrafastAdded: false, activeTab: "confirm_pay", request_id: this.request_id, order_id: data.order_id, order_no: data.order_no
      };
      sessionStorage.setItem("queryParamsCart", JSON.stringify(passQueryParam));
      this.router.navigate(['/cart'], { /*queryParams: passQueryParam /*, skipLocationChange: true*/ });
      this.progress.hide();
    });
  }
  // onChangeSortBy()
  // {
  //   this.progress.show();
  //   this.httpServices.request('get', "requests/seller-request-offer-list/"+this.request_id+"?sort_by="+this.sortBy, null, null, null).subscribe((data) => {
  //     this.offers=data;
  //     this.progress.hide();
  //   });
  // }
  backToManageRequest() {
    this.router.navigate(['/manage_request'], {});
  }
  onClickSeller(seller_id: any) {
    let profile_mode: string;
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == seller_id) {
        profile_mode = 'self';
      } else {
        profile_mode = 'other';
      }

    } else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.seller_profile_id = seller_id;
    this.encrypt_decrypt.seller_profile_mode = profile_mode;
    this.router.navigate(['/seller_profile'], {});
  }
  onGigClick(gigid: any, gig_status_id: any) {
    let passQueryParam = { id: gigid, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], {});
  }
  //==========================================================
  // get star event - to display rating
  //==========================================================
  public printStarEmpty: string = '';
  public printStar: string = '';
  getStar(value: any) {

    if (value == null) {
      value = 0;
    }
    if (value > 5) {
      value = 5;
    }
    let printStar = '';
    for (let i = 0; i < value; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star" style="color: #febf10;"></i>' : printStar + "" + '<i class="fa fa-star" style="color: #febf10;"></i>');

    }
    this.printStar = '';
    this.printStar = printStar;
    //this.printStar = Array(value).fill('whatever');
    //this.printStar = Array(value).fill('rating_stars');
  }


  
  // ========================
  // View Attached Imgae 
  // =========================
  public image_ViewUrl: any;
  openImage(item: any) {
    this.image_ViewUrl = item;
    jQuery('#imagemodal').modal({ backdrop: 'static', keyboard: false });
  }

  // ==================================
  // Display rating 
  // ==================================

  getEmptyStar() {
    this.printStar = '';
    let printStar = '';
    for (let i = 0; i < 5; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStarEmpty = printStar;
  }
}
interface offers {
  result: [
    {
      id: number,
      gig: {
        id: number,
        title: string,
        description: string,
        total_views: number,
        rating: number,
        rated_no_of_records: number,
        gallery: [
          {
            id: number,
            gig_gallery_type: {
              id: number,
              gig_gallery_type_name: string
            },
            file_path: string,
            file_path_thumbnail: string
          }
        ],
        gigs_pricing: {
          gig_price: {
            id: number,
            price_type: {
              id: number,
              price_type_name: string
            },
            price: string
          },
          gig_price_dtl: [
            {
              id: number,
              sub_category_price_scope: {
                id: number,
                seq_no: number,
                price_scope_name: string,
                is_heading_visible: boolean,
                watermark: string,
                price_scope_control_type: {
                  id: number,
                  price_scope_control_type_name: string
                }
              },
              sub_category_price_scope_dtl: {
                id: number,
                seq_no: number,
                value: string,
                display_value: string,
                actual_value: string
              },
              value: string
            }]
        },
        created_by: {
          id: number,
          user_name: string,
          level: {
            id: number,
            level_name: string
          },
          rating: string,
          user_info: {
            id: number,
            first_name: string,
            last_name: string,
            profile_picture: string
          }
        }
      },
      offer_price: string,
      offer_delivery_time: number,
      request_offer_details: [
        {
          id: number,
          sub_category_price_scope_id: number,
          sub_category_price_scope_dtl_id: number,
          value: string
        }
      ],
      gig_description_one: string,
      gig_description_two: string,
      silverCheckBoxDetails: [{
        silverCheckBox: string,
        silverCheckBoxValue: boolean
      }],
      deliveryTimetoDisplay: string,
      initials: string
    }
  ]
}
interface offerInfo {
  id: number,
  description: string,
  service_delivery: {
    id: number,
    seq_no: number,
    service_delivery_name: string,
    service_delivery_value: number,
    created_date: string,
    modified_date: string
  },
  service_delivery_other: number,
  budget: string,
  seller_offer_count: number,
  created_by: {
    id: number,
    user_name: string,
    level: {
      id: number,
      level_name: string
    },
    rating: string,
    user_info: {
      id: number,
      first_name: string,
      last_name: string,
      profile_picture: string
    }
  },
  request_attachment: [
    {
      id: number,
      file_path: string
    }
  ]
}

