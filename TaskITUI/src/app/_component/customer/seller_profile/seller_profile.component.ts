import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FacebookLoginProvider, GoogleLoginProvider, SocialAuthService } from 'angularx-social-login';
import { NgxSpinnerService } from 'ngx-spinner';
import { element } from 'protractor';
import { DateFormatterService } from 'src/app/_common_services/date-formatter.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { AuthInfo } from 'src/app/_models/auth-info';
import { Socialusers } from 'src/app/_models/socialusers';

declare var toastr: any;
declare var jQuery:any;
@Component({
  selector: 'app-seller_profile',
  templateUrl: './seller_profile.component.html',
  styleUrls: ['./seller_profile.component.css']
})
export class Seller_profileComponent implements OnInit {
  constructor(private router: Router, private httpServices: HttpRequestService, private dateFormatter: DateFormatterService, private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService, private authService: SocialAuthService) { }

  public openShow_ProfileModals: boolean = false;
  public openShow_descriptionModals: boolean = false;
  public openShow_languageModals: boolean = false;
  public openShow_skillModals: boolean = false;
  public openShow_educationModals: boolean = false;
  public openShow_certificationModals: boolean = false;

  public loggedIn_sellerDetails: any;
  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userId: any;

  public activeGigs: any;

  public seller_profile_photo: any = undefined;
  public photoCopy: boolean = false;
  public photoScan: any;
  public block = "block";
  public none = "none";
  public seller_data_source: any;

  public languageProfeciencyList: any;
  public getLanguageProfeciency: any = { id: 0, lang_proficiency_name: '' };
  public autoCompLanguageListItem: Array<{ id: string, code: string, lang_name: string }>;
  public languageDetails: any = { id: 0, lang_name: '' };
  public language_id: Number = 0;

  public skillProficiencyList: any;
  public getSkillLevel: any = { id: 0, skill_level_name: '' };
  public autoCompSkillListItem: Array<{ id: string, code: string, skill_name: string }>;
  public skillDetails: any = { id: 0, skill_name: '' };
  public skill_DetailsId: Number = 0;

  public countryList: any;
  public getCountry: any = { id: 0, country_name: '' };
  public autoCompUniversityListItem: Array<{ id: string, code: string, university_name: string }>;
  public universityDetails: any = { id: 0, university_name: '' };
  public autoCompMajorListItem: Array<{ id: string, education_major_name: string }> = [];
  public majorDetails: any = { id: 0, education_major_name: '' };
  public titleList: any;
  public getTitle: any = { id: 0, title_name: '' };
  public yearListForEducation: Array<{ year_id: Number, year: Number }> = [];
  public getYear: any = { id: 0, year: 0 };
  public getYear_year: Date = null;
  public education_Id = 0;
  public printStar: string = '';

  public yearList: Array<{ year_id: Number, year: Number }> = [];
  public getCertificationYear: any = { id: 0, year: 0 };
  public certificateFrom: string;
  public certificateOrAward: string;
  public certification_id: Number = 0;

  public seller_languageDetailsList: Array<{ list_id: any; lang_id: Number, lang_name: string, lang_proficiency_id: Number, lang_proficiency_name: string }> = [];
  public seller_CertificationDetailsList: Array<{ list_id: any; certificate_Award: string, certified_from: string, certification_year: string ,year_id: Number}> = [];
  public seller_skillDetailsList: Array<{ list_id: any, skill_id: Number, skill_name: string, skill_proficiency_id: Number, skill_proficiency_name: string }> = [];
  public seller_educationlDetailsList: Array<{ list_id: any; country_id: Number; country_name: string, university_name: string, education_title_id: Number, education_title_name: string, education_major_Id: Number, education_major_degree: string, education_year: string }> = [];


  public checkvalidation_description: boolean = false;
  public checkvalidation_personalInfo: boolean = false;
  public checkvalidation_languageModal: boolean = false;
  public checkvalidation_skillModal: boolean = false;
  public checkvalidation_educationModal: boolean = false;
  public checkvalidation_certificationModal: boolean = false;

  public description: string;
  public firstName: string;
  public lastName: string;
  public userName: string;
  public is_Seller: boolean = false;
  public queryParam: any;
  public _queryParams: any;
  public maxDate: any;
  public focusedDate: any;
  public account_Created_date:any;
  public out_Of_office:boolean=false;
  public toggleBtn_out_of_office:boolean=false;
  public printStarEmpty: string = '';

  //public skillIds_alreadyExist: Array<{ skill_id: number, skill_proficiency_id:number }> = [];

  ngOnInit() {
    this.maxDate = new Date();
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    //this.userId=this.userAuthInfo.currentUser.user.id;
    this.is_Seller = false;

    if(this.encrypt_decrypt.seller_profile_id == undefined){
      this._queryParams = (sessionStorage.getItem("seller_profile") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      this.is_Seller = this.queryParam.is_Seller;
      this.userId = this.queryParam.seller_profile_id;
    }
    else{
      if(this.encrypt_decrypt.seller_profile_mode=='self'){
        this.is_Seller = true;
      }
      if(this.encrypt_decrypt.seller_profile_id > 0){
        this.userId = this.encrypt_decrypt.seller_profile_id;
        let seller_profile = {seller_profile_id:this.userId, is_Seller: this.is_Seller}
        sessionStorage.setItem("seller_profile", JSON.stringify(seller_profile));
      }
    }
 
    this.getSeller_profileDetails();
    this.getLinkedAccountsData();
    this.getAccount_securityDetails();
    this.getActivegigs_Details();
    this.getYearList();
  }

  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  onlyCharacters_withApostrophe(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code == 39) && //apostrophe
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }
  
  //**************************************
  // GET Language Proficiency List
  //**************************************
  getLanguage_proficiencyList() {
    this.httpServices.request('get', 'languages/profeciency', '', '', null).subscribe((data) => {
      this.languageProfeciencyList = data;
    }, error => {
      console.log(error);
    })
  }

  //**************************************
  // GET Skill Proficiency List
  //**************************************
  getSkill_proficiencyList() {
    this.httpServices.request('get', 'skills/level', '', '', null).subscribe((data) => {
      this.skillProficiencyList = data;
    }, error => {
      console.log(error);
    })
  }

  //**************************************
  // GET Country List
  //**************************************
  getCountrylist() {
    this.httpServices.request('get', 'country/?country_name=', '', '', null).subscribe((data) => {
      if (data.length > 0) {
        this.countryList = data;
      }
    }, error => {
      console.log(error);
    })
  }


  //**********************
  //get Education Title List
  //***********************
  getTitleList() {
    this.httpServices.request('get', 'education/education-title', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.titleList = data;
        }
      });
  }

  //**********************
  //get Year List
  //***********************
  getYearList() {
    var year = new Date().getFullYear();
    for (var i = 1; i < 41; i++) {
      this.yearListForEducation.push({ year_id: 0, year: year - 41 + i });
      this.yearList.push({ year_id: 0, year: year - 41 + i });
    }
  }

  // ========================
  // View Attached Imgae 
  // =========================
  public image_ViewUrl: any;
  openImage(item: any) {
    this.image_ViewUrl = item;
    jQuery('#imagemodal').modal({ backdrop: 'static', keyboard: false });
  }

  //**************************
  // GET  ACTIVE GIG DETAILS
  //**************************
  getActivegigs_Details() {
    this.progress.show();
    // this.httpServices.request('get', 'gigs/profile-list?status_id=5', '', '', null).subscribe((data) => {
    this.httpServices.request('get', 'sellers/gigs-list/' + this.userId, '', '', null).subscribe((data) => {
      this.activeGigs = data;
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    })
  }

  //**********************
  // GET PROFILE DETAILS
  //***********************
  getSeller_profileDetails() {
    this.progress.show();
    this.httpServices.request('get', 'sellers/info/' + this.userId, null, null, null).subscribe((data) => {
      this.loggedIn_sellerDetails = data;

      this.account_Created_date=this.loggedIn_sellerDetails.created_date;
      this.out_Of_office=this.loggedIn_sellerDetails.is_out_of_office;

      let currentYear = new Date().getFullYear();
      this.focusedDate = new Date(currentYear - 1, 11, 31);

      //To get Seller Profile Details
      if (this.loggedIn_sellerDetails.personal_info != null && this.loggedIn_sellerDetails.personal_info != undefined) {
        this.seller_data_source = this.loggedIn_sellerDetails.personal_info.profile_picture;
        this.description = this.loggedIn_sellerDetails.personal_info.description;
        this.firstName = this.loggedIn_sellerDetails.personal_info.first_name;
        this.lastName = this.loggedIn_sellerDetails.personal_info.last_name;
        this.userName = this.loggedIn_sellerDetails.user_name;
      } else {
        this.seller_data_source = "/../assets/img/profile_pic.png";
        this.description = "";
        this.firstName = "";
        this.lastName = "";
        this.userName = "";
      }

        //To get Language Details
      if (this.loggedIn_sellerDetails != null) {
        this.loggedIn_sellerDetails.languages.forEach(element => {
          this.seller_languageDetailsList.push({
            list_id: element.id,
            lang_id: element.lang.id,
            lang_name: element.lang.lang_name,
            lang_proficiency_id: element.lang_proficiency.id,
            lang_proficiency_name: element.lang_proficiency.lang_proficiency_name
          });
        });
      }

      //To Get Skill Details
      if (this.loggedIn_sellerDetails != null) {
        this.loggedIn_sellerDetails.skills.forEach(element => {
          this.seller_skillDetailsList.push({
            list_id: element.id,
            skill_id: element.skill.id,
            skill_name: element.skill.skill_name,
            skill_proficiency_id: element.skill_level.id,
            skill_proficiency_name: element.skill_level.skill_level_name
          });
          
        });
      }

      //To Get Education Details
      if (this.loggedIn_sellerDetails != null) {
        this.loggedIn_sellerDetails.educations.forEach(element => {
          this.seller_educationlDetailsList.push({
            list_id: element.id,
            country_id: element.country.id,
            country_name: element.country.country_name,
            university_name: element.university_name,
            education_title_id: element.education_title.id,
            education_title_name: element.education_title.education_title_name,
            education_major_Id: element.education_major.id,
            education_major_degree: element.education_major.education_major_name,
            education_year: this.dateFormatter.format(new Date(Number(element.education_year), 1, 1), 'yyyy')
          });
        });
      }

      //To Get Certification Details
      if (this.loggedIn_sellerDetails != null) {
        this.loggedIn_sellerDetails.certifications.forEach(element => {
          this.seller_CertificationDetailsList.push({
            list_id: element.id,
            certificate_Award: element.certification_name,
            certified_from: element.certification_from,
            certification_year: element.certification_year,
            year_id:0
          });
        });
      }

      //toastr.success("Seller Profile Fetched Successfully");
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //**********************
  // PROFILE SECTION
  //***********************
  onProfilename_editPencilbtn() {
    this.openShow_ProfileModals = true;
    this.checkvalidation_personalInfo = false;
  }

  cancelProfileModal() {
    this.openShow_ProfileModals = false;
  }

  onUpdate_Profilename() {
    this.checkvalidation_personalInfo = true;
    let seller_profile: any = {
      first_name: '',
      last_name: ''

    }

    seller_profile.first_name = this.firstName;
    seller_profile.last_name = this.lastName;

    if (seller_profile.first_name != null && seller_profile.first_name != undefined && seller_profile.first_name != "" &&
      seller_profile.last_name != null && seller_profile.last_name != undefined && seller_profile.last_name != '') {
      this.progress.show();
      this.httpServices.request("patch", "buyers/personal-info/" + this.loggedIn_sellerDetails.personal_info.id, '', '', seller_profile).subscribe((data) => {
        toastr.success("Profile Name Updated Successfully");
        this.cancelProfileModal();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    }
  }

  //**********************
  //Profile Photo Section
  //***********************
  fileChangedPhoto(event: any) {
    this.progress.show();
    if (event.target.files.length == 0) {
      this.photoCopy = false;
      this.seller_profile_photo = undefined;
      this.photoScan = this.block;
      this.progress.hide();
      return;
    }
    this.photoScan = this.none;
    //To obtaine a File reference

    var files = event.target.files;
    const reader = new FileReader();
    let fileType = "";
    let fileSize = "";
    let alertstring = '';

    //To check file type according to upload conditions
    if (files[0].type == "image/jpeg" || files[0].type == "image/png" || files[0].type == "image/jpg" || files[0].type == "image/bmp" || files[0].type == "image/gif") {

      fileType = "";
    }
    else {

      fileType = ("<li>" + "The file does not match file type." + "</li>");
    }

    //To check file Size according to upload conditions

    if ((files[0].size / 1024 / 1024 / 1024 / 1024 / 1024) <= 5) {

      fileSize = "";
    }
    else {

      fileSize = ("<li>" + "Your file does not match the upload conditions, Your file size is:" + (files[0].size / 1024 / 1024 / 1024 / 1024 / 1024).toFixed(2) + "MB. The maximum file size for uploads should not exceed 5 MB." + "</li>");
    }

    alertstring = alertstring + fileType + fileSize;
    if (alertstring != '') {
      this.progress.hide();
      toastr.error(alertstring);
    }


    const formData = new FormData();
    // if(flag==true && flag1==true){
    if (fileType == "" && fileSize == "") {

      this.seller_profile_photo = event.target.files;
      reader.readAsDataURL(files[0]);
      reader.onload = (_event) => {
        this.seller_data_source = reader.result;
      }
      //this.seller_data_source=event.target.result;
      if (this.seller_profile_photo.fileName != "" || this.seller_profile_photo.fileName != undefined) {
        formData.append("profile_picture", this.seller_profile_photo[0]);
        this.photoCopy = true;
        this.photoScan = this.none;

        this.httpServices.request("patch", "sellers/personal-info/" + this.loggedIn_sellerDetails.personal_info.id, "", "", formData).subscribe((data) => {
          toastr.success("Profile photo Uploaded Successfully");
        }, error => {
          console.log(error);
          this.progress.hide();
        });

      }
      this.progress.hide();
    }
    else {
      //this.resetFileInput(this.file_image);
      this.seller_profile_photo = "";
      this.seller_profile_photo = undefined;
      this.photoCopy = false;
      this.photoScan = this.block;
      this.progress.hide();

    }
  }
  // deleteImage() {
  //   //this.logistic_plan_urls.splice(this.logistic_plan_urls.findIndex(x => x.file_name == event.file_name), 1);
  //   this.seller_profile_photo = "";
  //   this.seller_data_source = undefined;
  //   this.photoCopy = false;
  //   this.photoScan = this.block;
  //   this.seller_data_source=null;
  // }

  //Out of Office
  onTogglebtnOut_of_Office(){
   let outOfoffice:any={
    is_out_of_office:""
   }
    outOfoffice.is_out_of_office=this.out_Of_office==true?"true":"false";
   this.httpServices.request("post","sellers/out-of-office","","",outOfoffice).subscribe((data)=>{
   },error=>{
    console.log(error);
   });
  }

  //**********************
  // DESCRIPTION SECTION
  //***********************
  openDescriptionModal() {
    this.openShow_descriptionModals = true;
  }

  cancelDescriptionModal() {
    this.openShow_descriptionModals = false;
  }

  onUpdate_descriptionBtn() {
    this.progress.show();
    this.checkvalidation_description = true;
    let sellerProfile_description: any = {
      description: ""
    }

    sellerProfile_description.description = this.description;

    if (sellerProfile_description.description != null && sellerProfile_description.description != undefined && sellerProfile_description.description != "") {
      this.httpServices.request('patch', 'sellers/personal-info/' + this.loggedIn_sellerDetails.personal_info.id, '', '', sellerProfile_description).subscribe((data) => {
        toastr.success("Description Updated Successfully");
        this.cancelDescriptionModal();
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    }
  }

  //***************************
  // LANGUAGE SECTION
  //***************************
  cancelLanguageModal() {
    this.openShow_languageModals = false;
  }

  handleLanguageFilter(value: string) {
    this.autoCompLanguageListItem = undefined;
    this.languageDetails.id = 0;
    this.languageDetails.lang_name = '';
    if (value.length > 1) {
      this.httpServices.request('get', 'languages/?lang_name=' + value.replace('&', '~'), '', '', null).subscribe((data) => {
        if (data.length > 0) {
          this.autoCompLanguageListItem = data;
        }
      }, error => {
        console.log(error);
      });
    }
  }

  onSpanLanguageSelected(dataItem: any) {
    this.languageDetails.id = dataItem.id;
    this.languageDetails.lang_name = dataItem.lang_name;
  }

  //on change of language proficiency
  onSelection() {
    this.languageProfeciencyList.forEach(element => {
      if (element.id == this.getLanguageProfeciency.id) {
        this.getLanguageProfeciency.lang_proficiency_name = element.lang_proficiency_name;
      }
    });
  }


  onAddnew_Language() {
    this.openShow_languageModals = true;
    this.checkvalidation_languageModal = false;
    this.languageDetails.id = 0;
    this.languageDetails.lang_name = "";
    this.getLanguageProfeciency.id = 0;
    this.getLanguageProfeciency.lang_name = "";
    this.language_id = 0;
    this.getLanguage_proficiencyList();
  }


  onUpdate_languageModal() {
    this.checkvalidation_languageModal = true;
    
    let seller_language: any = {
      lang_id: 0,
      lang_proficiency_id: 0
    }

    seller_language.lang_id = this.languageDetails.id;
    seller_language.lang_proficiency_id = this.getLanguageProfeciency.id;

    if (seller_language.lang_id != 0 && seller_language.lang_id != null && seller_language.lang_proficiency_id != 0 && seller_language.lang_proficiency_id != null) {
      this.progress.show();
      if (this.seller_languageDetailsList.filter(element => element.list_id == this.language_id &&
        element.lang_id==this.languageDetails.id && 
        element.lang_name==this.languageDetails.lang_name &&
        element.lang_proficiency_id==this.getLanguageProfeciency.id &&
        element.lang_proficiency_name==this.getLanguageProfeciency.lang_proficiency_name).length > 0) {
        toastr.error("You have not changed anything for this record");
       } 
      else if(this.seller_languageDetailsList.filter(element=>element.lang_id==this.languageDetails.id &&
        element.list_id!==this.language_id &&
        element.lang_name==this.languageDetails.lang_name).length > 0){
        toastr.error("Language record already exist");
      }
      else{
        if (this.language_id == 0) {
          this.httpServices.request('post', 'user/languages', '', '', seller_language).subscribe((data) => {
            toastr.success("Language Record Saved Successfully");
            this.seller_languageDetailsList.push({
              list_id: data.id,
              lang_id: data.lang.id,
              lang_name: data.lang.lang_name,
              lang_proficiency_id: data.lang_proficiency.id,
              lang_proficiency_name: data.lang_proficiency.lang_proficiency_name
            })
            this.cancelLanguageModal();
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        } else {
          this.httpServices.request('patch', 'user/languages/' + this.language_id, '', '', seller_language).subscribe((data) => {
            toastr.success("Language Record Updated Successfully");
            this.cancelLanguageModal();
            this.seller_languageDetailsList.forEach(x => {
              if (x.list_id == data.id) {
                x.lang_id = data.lang.id;
                x.lang_name = data.lang.lang_name;
                x.lang_proficiency_id = data.lang_proficiency.id;
                x.lang_proficiency_name = data.lang_proficiency.lang_proficiency_name;
              }
            });
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
      }
      this.progress.hide();
    }
  }


  // ================================
  // On click of particular gig 
  // =================================
  onGigClick(gigid: any, gig_status_id: any) {
    let passQueryParam = { id: gigid, gig_status_id: gig_status_id };
    sessionStorage.setItem("gigs_details_queryParams", JSON.stringify(passQueryParam));
    this.router.navigate(['/gigs_details'], {});
  }

  onEditLanguage_pencilBtn(list: any) {
    this.openShow_languageModals = true;
    if (this.languageProfeciencyList == null || this.languageProfeciencyList == undefined || this.languageProfeciencyList == ''
      || this.languageProfeciencyList.length == 0) {
      this.getLanguage_proficiencyList();
      this.language_id = list.list_id;
      this.languageDetails.id = list.lang_id;
      this.languageDetails.lang_name = list.lang_name;
      this.getLanguageProfeciency.id = list.lang_proficiency_id;
      this.getLanguageProfeciency.lang_proficiency_name = list.lang_proficiency_name;

    } else {
      this.language_id = list.list_id;
      this.languageDetails.id = list.lang_id;
      this.languageDetails.lang_name = list.lang_name;
      this.getLanguageProfeciency.id = list.lang_proficiency_id;
      this.getLanguageProfeciency.lang_proficiency_name = list.lang_proficiency_name;
    }
  }


  onDeleteLanguage(list: any) {
    this.progress.show();
    if (this.seller_languageDetailsList.length > 1) {
      this.seller_languageDetailsList.splice(this.seller_languageDetailsList.findIndex(x => x.list_id == list.list_id), 1);

      this.httpServices.request("delete", "user/languages/" + list.list_id, null, null, null).subscribe((data) => {
        toastr.success("Language Deleted Successfully");
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    } else {
      toastr.error("Atleast One Language is Mandatory");
      this.progress.hide();
    }
  }
  //***************************
  // SKILL SECTION
  //***************************
  cancelSkillModal() {
    this.openShow_skillModals = false;
  }


  onAddnew_Skill() {
    this.checkvalidation_skillModal = false;
    this.openShow_skillModals = true;
    this.skillDetails.id = 0;
    this.skillDetails.skill_name = "";
    this.getSkillLevel.id = 0;
    this.getSkillLevel.skill_level_name = "";
    this.skill_DetailsId = 0;
    this.getSkill_proficiencyList();
  }

  handleSkillFilter(value: string) {
    this.autoCompSkillListItem = undefined;
    this.skillDetails.id = 0;
    this.skillDetails.skill_name = "";
    if (value.length > 1)
      this.httpServices.request('get', 'skills/?skill_name=', '', '', null).subscribe(data => {

        if (data.length > 0)
          this.autoCompSkillListItem = data;
      }, error => {
        //toastr.error(error);
        console.log(error);
      });
  }

  onSpanSkillSelected(dataItem: any) {
    this.skillDetails.id = dataItem.id;
    this.skillDetails.skill_name = dataItem.skill_name;
  }

   //Skill Level Change
   onskillLevelChange() {
    this.skillProficiencyList.forEach(element => {
      if (element.id == this.getSkillLevel.id) {
        this.getSkillLevel.skill_level_name = element.skill_level_name;
      }
    });
  }


  onUpdate_skillModal() {
    this.checkvalidation_skillModal = true;
    let seller_skill: any = {
      skill_id: 0,
      skill_level_id: 0
    }

    seller_skill.skill_id = this.skillDetails.id;
    seller_skill.skill_level_id = this.getSkillLevel.id;

    if (seller_skill.skill_id != 0 && seller_skill.skill_id != null && seller_skill.skill_level_id != 0 && seller_skill.skill_level_id != '') {
      this.progress.show();
    
      if(this.seller_skillDetailsList.filter(x=>x.list_id==this.skill_DetailsId && 
        x.skill_proficiency_id==this.getSkillLevel.id && 
        x.skill_id==this.skillDetails.id && 
        x.skill_name==this.skillDetails.skill_name &&
        x.skill_proficiency_name==this.getSkillLevel.skill_level_name).length>0){
        toastr.error("You have not changed anything for this skill");
      } 
   else if (this.seller_skillDetailsList.filter(element => element.list_id!=this.skill_DetailsId && 
    element.skill_id==this.skillDetails.id &&
    element.skill_name==this.skillDetails.skill_name).length > 0) {
        toastr.error("Skill Already Exists");
      }
      else{

        if (this.skill_DetailsId == 0) {
          this.httpServices.request('post', 'user/skills', '', '', seller_skill).subscribe((data) => {
            toastr.success("Skill Record Added Successfully");
            this.cancelSkillModal();
            this.seller_skillDetailsList.push({
              list_id: data.id,
              skill_id: data.skill.id,
              skill_name: data.skill.skill_name,
              skill_proficiency_id: data.skill_level.id,
              skill_proficiency_name: data.skill_level.skill_level_name
            });
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });

        } else {
         
          this.httpServices.request('patch', 'user/skills/' + this.skill_DetailsId, '', '', seller_skill).subscribe((data) => {
            toastr.success("Skill Record Updated Successfully");
            this.cancelSkillModal();
            this.seller_skillDetailsList.forEach(x => {
              if (x.list_id == data.id) {
                x.skill_id = data.skill.id,
                  x.skill_name = data.skill.skill_name,
                  x.skill_proficiency_id = data.skill_level.id,
                  x.skill_proficiency_name = data.skill_level.skill_level_name
              }
            });
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
        }
      this.progress.hide();
    }
  }


  onDeleteSkill(list: any) {
    this.progress.show();
    if (this.seller_skillDetailsList.length > 1) {
      this.seller_skillDetailsList.splice(this.seller_skillDetailsList.findIndex(x => x.list_id == list.list_id), 1);

      this.httpServices.request('delete', 'user/skills/' + list.list_id, '', '', null).subscribe((data) => {
        toastr.success("Skill Record Deleted Successfully");
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    } else {
      toastr.error("Atleast one skill is mandatory");
      this.progress.hide();
    }

  }

  onEditSkill_pencilBtn(list: any) {
    this.openShow_skillModals = true;
    if (this.skillProficiencyList == null || this.skillProficiencyList == undefined || this.skillProficiencyList.length == 0 || this.skillProficiencyList == '') {
      this.getSkill_proficiencyList();

      this.skill_DetailsId = list.list_id
      this.skillDetails.id = list.skill_id;
      this.skillDetails.skill_name = list.skill_name;
      this.getSkillLevel.id = list.skill_proficiency_id;
      this.getSkillLevel.skill_level_name = list.skill_proficiency_name;
    } else {
      this.skill_DetailsId = list.list_id
      this.skillDetails.id = list.skill_id;
      this.skillDetails.skill_name = list.skill_name;
      this.getSkillLevel.id = list.skill_proficiency_id;
      this.getSkillLevel.skill_level_name = list.skill_proficiency_name;
    }
  }

  //***************************
  // EDUCATION SECTION
  //***************************

  cancelEducationModal() {
    this.openShow_educationModals = false;
  }

  onAddnew_EducationModal() {
    this.openShow_educationModals = true;
    this.checkvalidation_educationModal = false;
    this.getCountrylist();
    this.getTitleList();
    this.getYearList();
    this.getCountry.id = 0;
    this.getCountry.country_name = "";
    this.universityDetails.university_name = "";
    this.getTitle.id = 0;
    this.getTitle.title_name = "";
    this.majorDetails.id = "";
    this.majorDetails.education_major_name = "";
    this.getYear.id = 0;
    this.getYear_year=null;
    this.getYear.year = "";
    this.education_Id = 0;
  }

  handleUniversityFilter(value: string) {
    this.autoCompUniversityListItem = undefined;
    this.universityDetails.university_name = "";
    this.universityDetails.id = 0;
    if (value.length > 1) {
      this.httpServices.request('get', 'user/university-list?university_name=' + value.replace('&', '~'), '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompUniversityListItem = data;
          }
          else {
            this.universityDetails.university_name = value;
          }
        });
    }
    this.universityDetails.university_name = value;
  }

  // On Selecting University 
  onSpanUniversitySelected(dataItem: any) {
    this.universityDetails.id = dataItem.id;
    this.universityDetails.university_name = dataItem.university_name;
  }


  //handle MajorEducation filter
  handleMajorFilter(value: string) {
    this.autoCompMajorListItem = undefined;
    this.majorDetails.id = 0;
    this.majorDetails.education_major_name = "";
    if(value.length>0){
    this.httpServices.request('get', 'education/education-major?education_major_name=' + value.replace('&', '~'), '', '', null).subscribe((data) => {
      if (data.length > 0) {
        this.autoCompMajorListItem = data;
      }
    }, error => {
      console.log(error);
    });
  }
  this.majorDetails.education_major_name = value;
}


  //on Selecting Major Education
  onSpanMajorSelected(dataItem: any) {
    this.majorDetails.id = dataItem.id;
    this.majorDetails.education_major_name = dataItem.education_major_name;
  }

  //on education Title Change
  onTitleChange() {
    this.titleList.forEach(element => {
      if (element.id == this.getTitle.id) {
        this.getTitle.title_name = element.education_title_name;
      }
    });
  }

  onEducationYearChange() {
    if (this.getYear_year != null) {
      this.yearListForEducation.forEach(element => {
        if (element.year == this.getYear_year.getFullYear()) {
          this.getYear.id = element.year_id;
          this.getYear.year = element.year;
        }
      });
    }
  }

  onUpdate_EducationModal() {
    
    this.checkvalidation_educationModal = true;

    if (this.getCountry.id != null && this.getCountry.id != 0 && this.getCountry.id != undefined &&
      this.universityDetails.university_name != null && this.universityDetails.university_name != undefined &&
      this.getTitle.id != null && this.getTitle.id != undefined && this.getTitle.id != 0 &&
      ((this.majorDetails.id == 0 && this.majorDetails.education_major_name != '') || (this.majorDetails.id != 0)) &&
      this.getYear.year != null && this.getYear.year != undefined) {


      if (this.majorDetails.id == 0) {
        let educationMajor: any = {
          education_major_name: ""
        }
        educationMajor.education_major_name = this.majorDetails.education_major_name;
        this.httpServices.request("post", "education/education-major?education_major_name=", "", "", educationMajor).subscribe((data) => {
          this.majorDetails.id = data.id;
          this.addEducationDetails();
        });
      } else {
        this.majorDetails.id;
        this.addEducationDetails();
      }
    }
  }

  addEducationDetails(){

    let seller_Education: any = {
      country_id: 0,
      university_name: '',
      education_title_id: 0,
      education_major_id: 0,
      education_year: 0
    }


    seller_Education.country_id = this.getCountry.id;
    seller_Education.university_name = this.universityDetails.university_name;
    seller_Education.education_title_id = this.getTitle.id;
    seller_Education.education_major_id = this.majorDetails.id;
    seller_Education.education_year = this.getYear.year;


    if (this.getCountry.id != null && this.getCountry.id != 0 && this.getCountry.id != undefined &&
      this.universityDetails.university_name != null && this.universityDetails.university_name != undefined &&
      this.getTitle.id != null && this.getTitle.id != undefined && this.getTitle.id != 0 &&
      ((this.majorDetails.id == 0 && this.majorDetails.education_major_name != '') || (this.majorDetails.id != 0)) &&
      this.getYear.year != null && this.getYear.year != undefined) {
      this.progress.show();

      if (this.seller_educationlDetailsList.filter(x => x.country_id == this.getCountry.id &&
        x.education_major_Id == this.majorDetails.id &&
        x.education_title_id == this.getTitle.id &&
        x.university_name == this.universityDetails.university_name &&
        x.education_year == this.getYear.year).length > 0) {
        toastr.error("This Education Record already in your List")
        this.progress.hide();
      } else {

        if (this.education_Id == 0) {
          this.httpServices.request('post', 'user/educations', '', '', seller_Education).subscribe((data) => {
            toastr.success("Education Record Created Successfully");
            this.cancelEducationModal();

            this.seller_educationlDetailsList.push({
              list_id: data.id,
              country_id: data.country.id,
              country_name: data.country.country_name,
              university_name: data.university_name,
              education_title_id: data.education_title.id,
              education_title_name: data.education_title.education_title_name,
              education_major_Id: data.education_major.id,
              education_major_degree: data.education_major.education_major_name,
              education_year: data.education_year
            });
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });

        } else {
          this.httpServices.request("patch", "user/educations/" + this.education_Id, "", "", seller_Education).subscribe((data) => {
            toastr.success("Education Record Updated Successfully")
            this.cancelEducationModal();

            this.seller_educationlDetailsList.forEach(x => {
              if (x.list_id == data.id) {
                x.country_id = data.country.id,
                  x.country_name = data.country.country_name,
                  x.university_name = data.university_name,
                  x.education_title_id = data.education_title.id,
                  x.education_title_name = data.education_title.education_title_name,
                  x.education_major_Id = data.education_major.id,
                  x.education_major_degree = data.education_major.education_major_name,
                  x.education_year = data.education_year
              }
            });
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
      }
    }
  }


  onDeleteEducation(list: any) {
    this.progress.show();
    if (this.seller_educationlDetailsList.length > 1) {
      this.seller_educationlDetailsList.splice(this.seller_educationlDetailsList.findIndex(x => x.list_id == list.list_id), 1);

      this.httpServices.request('delete', 'user/educations/' + list.list_id, '', '', null).subscribe((data) => {
        toastr.success("Education Record deleted Successfully");
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    } else {
      toastr.error("Atleast One Education is Mandatory");
      this.progress.hide();
    }
  }

  onEditEducation_pencilBtn(list: any) {

    this.getYearList();
    this.openShow_educationModals = true;

    if (this.countryList == 0 || this.countryList == null || this.countryList == undefined &&
      this.titleList == 0 || this.titleList == null || this.titleList == undefined) {
      this.getTitleList();
      this.getCountrylist();
      this.education_Id = list.list_id,
        this.getCountry.id = list.country_id,
        this.getCountry.country_name = list.country_name,
        this.universityDetails.university_name = list.university_name,
        this.getTitle.id = list.education_title_id,
        this.getTitle.title_name = list.education_title_name,
        this.majorDetails.education_major_name = list.education_major_degree,
        this.majorDetails.id = list.education_major_Id,
        this.getYear.year = list.education_year;
        this.getYear_year=new Date(Number(list.education_year),1,1);
    } else {
      this.education_Id = list.list_id,
        this.getCountry.id = list.country_id,
        this.getCountry.country_name = list.country_name,
        this.universityDetails.university_name = list.university_name,
        this.getTitle.id = list.education_title_id,
        this.getTitle.title_name = list.education_title_name,
        this.majorDetails.education_major_name = list.education_major_degree,
        this.majorDetails.id = list.education_major_Id,
        this.getYear.year = list.education_year;
        this.getYear_year=new Date(Number(list.education_year),1,1);
    }
  }


  //***************************
  // CERTIFICATION SECTION
  //***************************
  cancelCertificationModal() {
    this.openShow_certificationModals = false;
  }

  // ====================================
  // On Certification year change
  // ====================================
  public certificateYear: Date = null;
  onCertificationYearChange() {
    if (this.certificateYear != null) {
      this.yearList.forEach(element => {
        if (element.year == this.certificateYear.getFullYear()) {
          this.getCertificationYear.year = element.year;
          this.getCertificationYear.id = element.year_id;
        }
      });
    }
  }

  onAddnew_CertificationModal() {
    this.checkvalidation_certificationModal = false;
    this.openShow_certificationModals = true;
    this.certificateFrom = "";
    this.certificateOrAward = "";
    this.getCertificationYear.id = 0;
    this.getCertificationYear.year = 0;
    this.certification_id = 0;
    this.certificateYear=null;
    this.getYearList();
  }

  // On year Change in Certification section 
  // onCertificationYearChange() {
  //   this.yearList.forEach(element => {
  //     if (element.year_id == this.getCertificationYear.id) {
  //       this.getCertificationYear.year = element.year;
  //     }
  //   });
  // }


  onUpdate_CertificationModal() {
    this.checkvalidation_certificationModal = true;
    let seller_certificate: any = {
      certification_name: "",
      certification_from: "",
      certification_year: 0
    }

    seller_certificate.certification_name = this.certificateOrAward;
    seller_certificate.certification_from = this.certificateFrom;
    //seller_certificate.certification_year = this.getCertificationYear.year;
    seller_certificate.certification_year = this.dateFormatter.format(new Date(Number(this.getCertificationYear.year),1,1), 'yyyy');

    if (seller_certificate.certification_name != null && seller_certificate.certification_name != undefined
      && seller_certificate.certification_from != null && seller_certificate.certification_from != undefined
      && seller_certificate.certification_year != 0 && seller_certificate.certification_year !=1900 && seller_certificate.certification_year != null) {
      this.progress.show();

      if (this.seller_CertificationDetailsList.filter(element => element.certified_from == this.certificateFrom &&
        element.certificate_Award == this.certificateOrAward &&
        element.certification_year == this.getCertificationYear.year).length > 0) {
        toastr.error("This Certificate is already on your list.");
        this.progress.hide();
      }
      else {

        if (this.certification_id == 0) {
          this.httpServices.request('post', 'user/certifications', '', '', seller_certificate).subscribe((data) => {
            toastr.success("Cerification Record Created Successfully");
            this.cancelCertificationModal();

            this.seller_CertificationDetailsList.push({
              list_id: data.id,
              certificate_Award: data.certification_name,
              certified_from: data.certification_from,
              certification_year: data.certification_year,
              year_id:data.year_id
            });
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });

        } else {

          this.httpServices.request("patch", "user/certifications/" + this.certification_id, "", "", seller_certificate).subscribe((data) => {
            toastr.success("Cerification Record Updated Successfully");
            this.cancelCertificationModal();

            this.seller_CertificationDetailsList.forEach(x => {
              if (x.list_id == data.id) {
                x.certificate_Award = data.certification_name;
                x.certified_from = data.certification_from;
                x.certification_year = data.certification_year;
                x.year_id=data.year_id;
              }
            });
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
      }
    }
  }

  onEditcertification_pencilBtn(list: any) {
    this.getYearList();
    this.openShow_certificationModals = true;
    this.certification_id = list.list_id;
    this.certificateOrAward = list.certificate_Award,
    this.certificateFrom = list.certified_from,
    this.getCertificationYear.year = list.certification_year;
    this.certificateYear = new Date(Number(list.certification_year), 1, 1);
  }

  onDeleteCertification(list: any) {
    this.progress.show();
    if (this.seller_CertificationDetailsList.length > 1) {
      this.seller_CertificationDetailsList.splice(this.seller_CertificationDetailsList.findIndex(x => x.list_id == list.list_id), 1);

      this.httpServices.request("delete", "user/certifications/" + list.list_id, "", "", null).subscribe((data) => {
        toastr.success("Certification Record Deleted successfully");
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
    } else {
      toastr.error("Atleast one Certificate is mandatory");
      this.progress.hide();
    }
  }


  // ===============================================
  // Linked Accounts: Google, Facebook
  // ================================================
  linkedAccounts_connect_click(socialProvider: string) {

    let socialPlatformProvider;
    if (socialProvider === 'Facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'Google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }
    //this.saves_response(socialProvider);
    this.authService.signIn(socialPlatformProvider).then(social_users => {
      this.saves_response(social_users);
    });
  }
  saves_response(social_users: Socialusers) {
    //console.log(social_users);
    this.progress.show();
    let login_info = { social_account_id: social_users.email, provider: social_users.provider };
    this.httpServices.request('post', 'sellers/linked-accounts', null, null, login_info).subscribe((data) => {
      if (data) {
        console.log(data);
      }
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }

  //**************************
  // GET  User ACCOUNT DETAILS
  //**************************
  public is_verified_mobile_no:boolean=false;
  public phone_acc_security="";
  getAccount_securityDetails(){
    this.progress.show();
    this.httpServices.request("get","user/user-accounts-verify-details/"+this.userId,"","",null).subscribe((data)=>{
      this.is_verified_mobile_no = data.is_verified_mobile_no;
    },error=>{
      console.log(error);
      this.progress.hide();
    });
  }
  // ==========================================================
  // Accounts Security tab: on click of Phone Number
  // ==========================================================
  open_pop_phone_acc_security() {
    jQuery('#Add_Phone_Number').modal({ backdrop: 'static', keyboard: false });
    this.phone_acc_security = "";
  }

  // ===================================================
  // Accounts Security tab:on click of verify by SMS
  // ====================================================
  public showPhoneValidation: boolean = false;
  public phone_OTP_acc_security: any;
  verifyBySMSClick() {
    if (this.phone_acc_security != null && this.phone_acc_security != "" && this.phone_acc_security != undefined && this.phone_acc_security.length == 10) {
      let mob_number: any = {
        mobile_no: ""
      }
      this.progress.show();
      mob_number.mobile_no = this.phone_acc_security;
      this.httpServices.request('post', 'user/mobile-no-verification-otp', null, null, mob_number).subscribe((data) => {
        jQuery('#Add_Phone_Number').modal("hide");
        jQuery('#verify_OTP_Phone_Number').modal({ backdrop: 'static', keyboard: false });
        this.phone_OTP_acc_security = "";
        this.progress.hide();
      });

      this.showPhoneValidation = false;
    }
    else {
      this.showPhoneValidation = true;
    }
  }

    // ======================================
  // on Click of Verify OTP : Phone number 
  // =======================================
  public checkOTPValidation: boolean = false;
  verifyphoneOTP() {
    let mob_number: any = {
      otp: ""
    }
    mob_number.otp = this.phone_OTP_acc_security;
    this.checkOTPValidation = true;
    if (this.phone_OTP_acc_security != null && this.phone_OTP_acc_security != undefined && this.phone_OTP_acc_security.toString().trim() != '') {
      this.progress.show();
      this.httpServices.request('post', 'user/mobile-no-verify', null, null, mob_number).subscribe((data) => {
        toastr.success("Mobile No Verified");
        jQuery('#verify_OTP_Phone_Number').modal('hide');
        this.is_verified_mobile_no = true;
        this.checkOTPValidation = false;
        this.progress.hide();
      });
    }
  }
  // ===========================================
  // get linked accounts details 
  // ===========================================
  public social_mediaList: social_media = {
    social_media_array: [
      {
        id: 0,
        social_media_name: "",
        is_verified: false
      }
    ]
  }

  // ==================================
  // Display rating 
  // ==================================
  getStar(value: any) {
    if (value == null) {
      value = 0;
    }
    if (value > 5) {
      value = 5;
    }
    let printStar = '';
    for (let i = 0; i < value; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStar = printStar;
  }

  getEmptyStar() {

    let printStar = '';
    for (let i = 0; i < 5; i++) {
      printStar = (printStar == "" ? '<i class="fa fa-star"></i>' : printStar + "" + '<i class="fa fa-star"></i>');
    }
    this.printStarEmpty = printStar;
  }


  getLinkedAccountsData() {
    this.httpServices.request('get', 'social_media/', null, null, null).subscribe((data) => {
      this.httpServices.request('get', 'user/social-accounts-linked/' + this.userId, null, null, null).subscribe((response) => {
        if (response != null) {
          for (let i = 0; i < data.length; i++) {
            if (response.filter(x => x.social_media.id == data[i].id).length > 0) {
              this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: true });
            }
            else {
              this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: false });
            }
          }
        }
        else {
          for (let i = 0; i < data.length; i++) {
            this.social_mediaList.social_media_array.push({ id: data[i].id, social_media_name: data[i].social_media_name, is_verified: false });
          }
        }
        this.social_mediaList.social_media_array.splice(this.social_mediaList.social_media_array.findIndex(x => x.id == 0), 1);
      });
    });
  }
}
interface social_media {
  social_media_array: [
    {
      id: number,
      social_media_name: string,
      is_verified: boolean
    }
  ]
}


