/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Seller_requestComponent } from './seller_request.component';

describe('Seller_requestComponent', () => {
  let component: Seller_requestComponent;
  let fixture: ComponentFixture<Seller_requestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Seller_requestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Seller_requestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
