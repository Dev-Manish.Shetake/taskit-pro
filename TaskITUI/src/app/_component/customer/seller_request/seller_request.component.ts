import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataStateChangeEvent, GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';
import { SortDescriptor, State, process, orderBy, toODataString } from '@progress/kendo-data-query';
import { NgxSpinnerService } from 'ngx-spinner';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
declare var jQuery: any;
declare var toastr: any;
@Component({
  selector: 'app-seller_request',
  templateUrl: './seller_request.component.html',
  styleUrls: ['./seller_request.component.css']
})
export class Seller_requestComponent implements OnInit {

  public active_RequestList: any;
  public offersSubmitted_RequestList: any;
  public gridHeight_OfferSubmit: any;
  public activeRequest_Griddata: GridDataResult | undefined;
  public offersSubmittedRequest_Griddata: GridDataResult | undefined;
  public gridHeight: any;
  public requestCount: number;
  public activeCount: number;
  public sentOfferCount: number;
  public sort: SortDescriptor[] = [];
  public pageSize = 10;
  public skip = 0;
  public totalRecordCount: number = 0;
  public pageNumber: any = 1;
  public state: State = {
    //skip:0,
    //take:10,
  }
  public order_Search: any;
  public userAuthInfo: any;
  public seller_gig_list: gig_list = {
    results: [
      {
        id: 0,
        title: "",
        description: "",
        gallery: [
          {
            id: 0,
            gig_gallery_type: {
              id: 0,
              gig_gallery_type_name: ""
            },
            file_path: "",
            file_path_thumbnail: ""
          }
        ],
        gigs_pricing: [{
          gig_price: {
            id: 0,
            price_type: {
              id: 0,
              price_type_name: ""
            },
            price: ""
          },
          gig_price_dtl: [{
            id: 0,
            sub_category_price_scope: {
              id: 0,
              seq_no: 0,
              price_scope_name: "",
              is_heading_visible: false,
              watermark: "",
              price_scope_control_type: {
                id: 0,
                price_scope_control_type_name: ""
              }
            },
            sub_category_price_scope_dtl: {
              id: 0,
              seq_no: 0,
              value: "",
              display_value: "",
              actual_value: 0
            },
            value: ""
          }]
        }]
      }
    ]
  }
  public selected_gig_data: selected_gig_data = {
    gig_title: "",
    file_path: "",
    txtDescription: "",
    revisionList: [{
      revisionId: 0,
      revisionName: ""
    }],
    deliveryList: [{
      deliveryId: 0,
      deliveryName: ""
    }],
    price: "",
    txtPrice: "",
    selected_revision: 0,
    selected_delivery: 0,
    package_list: [{
      price_type_id: 0,
      price_type_name: "",
      package_price: "",
      package_delivery_time_id: 0,
      package_delivery_time: "",
      package_revisions_id: 0,
      package_revisions: ""
    }]
  }
  constructor(private httpServices: HttpRequestService, private router: Router, private encrypt_decrypt: EncryptDecryptService, private progress: NgxSpinnerService) { }

  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.tab_name = "active_tab";
    this.getrequestCount();
    this.getActiveRequest_gridData();
    //this.getOffersSubmittedRequest_gridData();
  }

  public tab_name: string;
  onTabChange(tab_name) {
    if (tab_name != undefined) {
      this.tab_name = tab_name;
    }
    if (tab_name == "active_tab") {
      this.getActiveRequest_gridData();
    } else {
      this.getOffersSubmittedRequest_gridData();
    }
  }

  //**********************************  
  //  Active Buyer Request Grid Data
  //**********************************
  getActiveRequest_gridData() {
    this.progress.show();
    this.httpServices.request("get", "requests/seller-request-offers?is_sent_offer=false", "", "", null)
      .subscribe((data) => {
        this.active_RequestList = data.results;
        if (data.results.length > 5) {
          this.gridHeight = "auto";
        }
        else {
          this.gridHeight = "270";
        }
        this.loadActive_RequestData(data.count);
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  //A particular method that is always to be followed when using Kendo GRID
  // private loadActive_RequestData(recordCount: number): void {
  //   if (this.active_RequestList.length > 0) {
  //     this.activeRequest_Griddata = {
  //       data: orderBy(this.active_RequestList, this.sort).slice(this.skip, this.skip + this.pageSize),
  //       //In case if gig list page has 40 record....SLICE to split record to next position as Record to be shown for Page is 10.
  //       //So remaining records are sliced to skip btn press.
  //       total: recordCount
  //     };
  //   } else {
  //     this.activeRequest_Griddata = undefined;
  //   }
  // }

  private loadActive_RequestData(recordCount: number): void {
    if (this.active_RequestList.length > 0) {
      this.activeRequest_Griddata = {
        data: orderBy(this.active_RequestList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.activeRequest_Griddata = undefined;
    }
  }

  //Kendo Grid - Sorting For All Gig List
  // To handle sorting on KENDO COLUMNS 
  // public sortActiveRequest_Change(sort: SortDescriptor[]): void {
  //   this.sort = sort;
  //   this.loadActive_RequestData(this.totalRecordCount);
  // }

  // To handle sorting on KENDO COLUMNS 
  public sortActiveRequest_Change(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadActive_RequestData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For All Gig List
  // public activeRequest_DataStateChange(state: DataStateChangeEvent): void {
  //   this.state = state;
  //   this.activeRequest_Griddata = process(this.active_RequestList, this.state);
  // }

  public activeRequest_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.activeRequest_Griddata = process(this.active_RequestList, this.state);
  }

  pageChange(event: PageChangeEvent): void {
    //this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "requests/seller-request-offers?page=" + this.pageNumber + "&page_size=" + this.pageSize;

    // if (this.searchSeller != null && this.searchSeller != undefined && this.searchSeller != '') {
    //   url = url + '&seller_name=' + this.searchSeller
    // }
    // if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
    //   url = url + '&buyer_name=' + this.searchBuyer
    // }
    this.httpServices.request("get", url, "", "", null).subscribe((data) => {
      this.active_RequestList = data.results;
      this.loadActive_RequestData(data.count);
      //this.progress.hide();
    }, error => {
      console.log(error);
      //this.progress.hide();
    });
  }

  //**********************************  
  //  Offers Submitted For Buyer Request Grid Data
  //**********************************

  getOffersSubmittedRequest_gridData() {
    this.progress.show();
    this.httpServices.request("get", "requests/seller-request-offers?is_sent_offer=true", "", "", null)
      .subscribe((data) => {
        this.offersSubmitted_RequestList = data.results;
        if (data.results.length > 5) {
          this.gridHeight_OfferSubmit = "auto";
        }
        else {
          this.gridHeight_OfferSubmit = "270";
        }
        this.loadOffersSubmitted_RequestData(data.count);
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }


  // private loadOffersSubmitted_RequestData(recordCount: number): void {
  //   if (this.offersSubmitted_RequestList.length > 0) {
  //     this.offersSubmittedRequest_Griddata = {
  //       data: orderBy(this.offersSubmitted_RequestList, this.sort).slice(this.skip, this.skip + this.pageSize),
  //       total: recordCount
  //     };
  //   } else {
  //     this.offersSubmittedRequest_Griddata = undefined;
  //   }
  // }
  private loadOffersSubmitted_RequestData(recordCount: number): void {
    if (this.offersSubmitted_RequestList.length > 0) {
      this.offersSubmittedRequest_Griddata = {
        data: orderBy(this.offersSubmitted_RequestList, this.sort).slice(this.skip, this.skip + this.pageSize),
        total: recordCount
      };
    } else {
      this.offersSubmittedRequest_Griddata = undefined;
    }
  }


  //Kendo Grid - Sorting 
  // To handle sorting on KENDO COLUMNS 
  // public sortOffersSubmitted_RequestChange(sort: SortDescriptor[]): void {
  //   this.sort = sort;
  //   this.loadOffersSubmitted_RequestData(this.totalRecordCount);
  // }

  public sortOffersSubmitted_RequestChange(sort: SortDescriptor[]): void {
    this.sort = sort;
    this.loadOffersSubmitted_RequestData(this.totalRecordCount);
  }

  // Kendo Grid - data state change For All Gig List
  // public offersSybmittedRequest_DataStateChange(state: DataStateChangeEvent): void {
  //   this.state = state;
  //   this.offersSubmittedRequest_Griddata = process(this.offersSubmitted_RequestList, this.state);
  // }

  public offersSybmittedRequest_DataStateChange(state: DataStateChangeEvent): void {
    this.state = state;
    this.offersSubmittedRequest_Griddata = process(this.active_RequestList, this.state);
  }

  pageChange_OffersSubmitted(event: PageChangeEvent): void {
    this.progress.show();
    this.pageNumber = (event.skip + this.pageSize) / this.pageSize;
    var url = "requests/seller-request-offers?is_sent_offer=true&page=" + this.pageNumber + "&page_size=" + this.pageSize;

    // if (this.searchSeller != null && this.searchSeller != undefined && this.searchSeller != '') {
    //   url = url + '&seller_name=' + this.searchSeller
    // }
    // if (this.searchBuyer != null && this.searchBuyer != undefined && this.searchBuyer != '') {
    //   url = url + '&buyer_name=' + this.searchBuyer
    // }
    this.httpServices.request("get", url, "", "", null)
      .subscribe((data) => {
        this.offersSubmitted_RequestList = data.results;
        this.loadOffersSubmitted_RequestData(data.count);
        this.progress.hide();
      }, error => {
        console.log(error);
        this.progress.hide();
      });
  }

  // ============================
  // Search Button
  // ============================
  // this.offerInfo.budget = this.offerInfo.budget.replace(".00", "");
  onSearchBtnClick(type: any) {
    this.progress.show();

    if (this.tab_name == "active_tab") {
      //var url = "orders/?order_status_id=" + this.orderStatusId + "&page=1&page_size=" + this.pageSize;
      var url = "requests/seller-request-offers?page=" + this.pageNumber + "&page_size=" + this.pageSize;
      if (this.order_Search != null && this.order_Search != '' && this.order_Search != undefined) {
        url = url + "&search=" + this.order_Search;
        if (type == 'active') {
          url = url + "&is_sent_offer=false";
        }
        else if (type = 'sent') {
          url = url + "&is_sent_offer=true";
        }
      }

      this.httpServices.request("get", url, "", "", null)
        .subscribe((data) => {
          this.active_RequestList = data.results;
          //this.offersSubmitted_RequestList = data.results;
          this.loadActive_RequestData(data.count);
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    } else {
      var url = "requests/seller-request-offers?is_sent_offer=true&page=" + this.pageNumber + "&page_size=" + this.pageSize;
      if (this.order_Search != null && this.order_Search != '' && this.order_Search != undefined) {
        url = url + "&search=" + this.order_Search;
      }

      this.httpServices.request("get", url, "", "", null)
        .subscribe((data) => {
          this.offersSubmitted_RequestList = data.results;
          this.loadOffersSubmitted_RequestData(data.count);
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
    }
  }

  OnbuyernameClick(buyerId) {

    let profile_mode: string = '';
    if (this.userAuthInfo.currentUser.user != undefined) {
      if (this.userAuthInfo.currentUser.user.id == buyerId) {
        profile_mode = 'self';
      }
      else {
        profile_mode = 'other';
      }
    }
    else {
      profile_mode = 'other';
    }
    this.encrypt_decrypt.buyer_profile_id = buyerId
    this.encrypt_decrypt.buyer_profile_mode = profile_mode;
    this.router.navigate(['/buyer_profile'], {});
  }

  // ============================
  // Seller Request Count
  // ============================
  getrequestCount() {
    //request Count for Buyer
    this.httpServices.request("get", "requests/counts", "", "", null).subscribe((data) => {
      this.requestCount = data;
      this.activeCount = data.total_active;
      this.sentOfferCount = data.total_sent_offer;

    }, error => {
      console.log(error);
    });
  }
  public request_offer_id: any;
  onCreateOfferClick(request_offer_id: any, sub_category_id: any) {
    if (request_offer_id > 0) {
      this.request_offer_id = request_offer_id;
      this.progress.show();
      this.httpServices.request("get", "requests/seller-gig-list/" + sub_category_id, null, null, null).subscribe((data) => {
        this.seller_gig_list.results = data.results;
        if (this.seller_gig_list.results.length > 0) {
          if (this.seller_gig_list.results.length > 1) {
            jQuery('#gig_list').modal({ backdrop: 'static', keyboard: false });
          }
          else {
            jQuery('#create_offer').modal({ backdrop: 'static', keyboard: false });
            this.openSinglePayment(this.seller_gig_list.results[0].id);
          }
        }
        this.progress.hide();
      });
    }

  }


  onEditOfferClick(request_offer_id: any) {
    if (request_offer_id > 0) {
      this.request_offer_id = request_offer_id;
      this.progress.show();
      this.httpServices.request("get", "requests/seller-request-offers/" + request_offer_id, null, null, null).subscribe((data) => {
        data.gig.gigs_pricing[0].gig_price_dtl.forEach(element => {
          if (element.sub_category_price_scope.price_scope_name == "Delivery Time") {
            this.sub_category_priceScope_id_delivery = element.sub_category_price_scope.id;
          }
          else if (element.sub_category_price_scope.price_scope_name == "Max Revisions Allowed") {
            this.sub_category_priceScope_id_Revision = element.sub_category_price_scope.id;
          }
        });

        for (let i = 0; i < data.gig.gigs_pricing.length; i++) {
          let delivery_time_id: any;
          let delivery_time_name: any;
          let revision_id: any;
          let revision_name: any;
          data.gig.gigs_pricing[i].gig_price_dtl.forEach(element => {
            if (element.sub_category_price_scope.price_scope_name == "Delivery Time") {
              this.sub_category_priceScope_id_delivery = element.sub_category_price_scope.id;
              delivery_time_id = element.sub_category_price_scope_dtl.id;
              delivery_time_name = element.sub_category_price_scope_dtl.display_value;
            }
            else if (element.sub_category_price_scope.price_scope_name == "Max Revisions Allowed") {
              this.sub_category_priceScope_id_Revision = element.sub_category_price_scope.id;
              revision_id = element.sub_category_price_scope_dtl.id;
              revision_name = element.sub_category_price_scope_dtl.display_value;
            }
          })
  
          this.selected_gig_data.package_list.push({
            price_type_id: data.gig.gigs_pricing[i].gig_price.price_type.id,
            price_type_name: data.gig.gigs_pricing[i].gig_price.price_type.price_type_name,
            package_price: data.gig.gigs_pricing[i].gig_price.price,
            package_delivery_time_id: delivery_time_id,
            package_delivery_time: delivery_time_name,
            package_revisions_id: revision_id,
            package_revisions: revision_name
          })
        }
        this.selected_gig_data.package_list.splice(this.selected_gig_data.package_list.findIndex(x => x.price_type_id == 0), 1);
        this.getDeliveryList();
        this.getRevisionList();

        this.selected_gig_data.selected_delivery = data.request_offer_details.filter(delivery => delivery.sub_category_price_scope_id == this.sub_category_priceScope_id_delivery)[0].sub_category_price_scope_dtl_id;
        this.selected_gig_data.selected_revision = data.request_offer_details.filter(revision => revision.sub_category_price_scope_id == this.sub_category_priceScope_id_Revision)[0].sub_category_price_scope_dtl_id;;
        this.selected_gig_data.txtDescription = data.seller_offer_description;
        this.selected_gig_data.txtPrice = data.offer_price;
        this.selected_gig_data.gig_title = data.gig.title;
        this.selected_gig_data.file_path = data.gig.gallery[0].file_path;
        jQuery('#create_offer').modal({ backdrop: 'static', keyboard: false });
        this.progress.hide();
      });
    }
  }


  onRemoveOfferClick(request_offer_id: any) {
    if (request_offer_id > 0) {
      this.progress.show();
      this.httpServices.request('delete', "requests/offer-delete/" + request_offer_id, null, null, null).subscribe((data) => {
        toastr.success('Offer has been removed successfully.');
      });
    }
  }


  BackToGigList() {
    if (this.seller_gig_list.results.length > 1) {
      jQuery('#create_offer').modal("hide");
      jQuery('#gig_list').modal({ backdrop: 'static', keyboard: false });
      this.packageSelected = false;
    }
    else {
      jQuery('#create_offer').modal("hide");
      this.packageSelected = false;
    }
  }
  public sub_category_priceScope_id_delivery: any;
  public sub_category_priceScope_id_Revision: any;
  public deliveryList: any;
  public revisionList: any;
  public gig_id: any;
  public price_type_id: any;
  openSinglePayment(gig_id: any) {
    if (gig_id > 0) {
      this.gig_id = gig_id;
      let selected_gig = this.seller_gig_list.results.filter(element => element.id == gig_id);
      this.selected_gig_data.gig_title = selected_gig[0].title;
      if (selected_gig[0].gallery.length > 0) {
        this.selected_gig_data.file_path = selected_gig[0].gallery[0].file_path_thumbnail;
      }
      this.selected_gig_data.package_list = [{
        price_type_id: 0,
        price_type_name: "",
        package_price: "",
        package_delivery_time_id: 0,
        package_delivery_time: "",
        package_revisions_id: 0,
        package_revisions: ""
      }]
      for (let i = 0; i < selected_gig[0].gigs_pricing.length; i++) {
        let delivery_time_id: any;
        let delivery_time_name: any;
        let revision_id: any;
        let revision_name: any;
        selected_gig[0].gigs_pricing[i].gig_price_dtl.forEach(element => {
          if (element.sub_category_price_scope.price_scope_name == "Delivery Time") {
            this.sub_category_priceScope_id_delivery = element.sub_category_price_scope.id;
            delivery_time_id = element.sub_category_price_scope_dtl.id;
            delivery_time_name = element.sub_category_price_scope_dtl.display_value;
          }
          else if (element.sub_category_price_scope.price_scope_name == "Max Revisions Allowed") {
            this.sub_category_priceScope_id_Revision = element.sub_category_price_scope.id;
            revision_id = element.sub_category_price_scope_dtl.id;
            revision_name = element.sub_category_price_scope_dtl.display_value;
          }
        })

        this.selected_gig_data.package_list.push({
          price_type_id: selected_gig[0].gigs_pricing[i].gig_price.price_type.id,
          price_type_name: selected_gig[0].gigs_pricing[i].gig_price.price_type.price_type_name,
          package_price: selected_gig[0].gigs_pricing[i].gig_price.price,
          package_delivery_time_id: delivery_time_id,
          package_delivery_time: delivery_time_name,
          package_revisions_id: revision_id,
          package_revisions: revision_name
        })
      }
      this.selected_gig_data.package_list.splice(this.selected_gig_data.package_list.findIndex(x => x.price_type_id == 0), 1);
    }
    this.selected_gig_data.selected_delivery = 0;
    this.selected_gig_data.selected_revision = 0;
    this.selected_gig_data.txtDescription = '';
    this.selected_gig_data.txtPrice = '';
    jQuery('#create_offer').modal({ backdrop: 'static', keyboard: false });
    this.getDeliveryList();
    this.getRevisionList();
  }
  getDeliveryList() {
    if (this.sub_category_priceScope_id_delivery > 0) {
      this.httpServices.request("get", "subcategories/price-scope-details-list?sub_category_price_scope_id=" + this.sub_category_priceScope_id_delivery, null, null, null).subscribe((data) => {
        this.deliveryList = data;
      });
    }
  }
  getRevisionList() {
    if (this.sub_category_priceScope_id_Revision > 0) {
      this.httpServices.request("get", "subcategories/price-scope-details-list?sub_category_price_scope_id=" + this.sub_category_priceScope_id_Revision, null, null, null).subscribe((data) => {
        this.revisionList = data;
      });
    }
  }
  openSelectPackage() {
    jQuery('#select_package').modal({ backdrop: 'static', keyboard: false });
  }
  public deliveryTimeValue: any;
  public revisionValue: any;
  public sub_category_priceScope_dtl_id_delivery: any;
  public sub_category_priceScope_dtl_id_revision: any;
  onDeliveryTimeChange() {
    if (this.selected_gig_data.selected_delivery > 0) {
      this.deliveryList.forEach(element => {
        if (element.id == this.selected_gig_data.selected_delivery) {
          this.deliveryTimeValue = element.actual_value;
          this.sub_category_priceScope_dtl_id_delivery = element.id;
        }
      });
    }
  }
  onRevisionChange() {
    if (this.selected_gig_data.selected_revision > 0) {
      this.revisionList.forEach(element => {
        if (element.id == this.selected_gig_data.selected_revision) {
          this.revisionValue = element.actual_value;
          this.sub_category_priceScope_dtl_id_revision = element.id;
        }
      });
    }
  }
  SendOffer() {
    if (this.packageSelected == false) {
      toastr.error("Please select Package");
    }
    else {
      if (this.selected_gig_data.selected_revision != 0 && this.selected_gig_data.selected_revision != null && this.selected_gig_data.selected_revision != undefined
        && this.selected_gig_data.selected_delivery != 0 && this.selected_gig_data.selected_delivery != null && this.selected_gig_data.selected_delivery != undefined &&
        this.selected_gig_data.txtDescription != null && this.selected_gig_data.txtDescription != undefined && this.selected_gig_data.txtDescription.toString().trim() != "") {
        this.progress.show();
        let offerBody: any = {
          seller_offer_description: "",
          gig_id: 0,
          price_type_id: 0,
          offer_price: "",
          offer_delivery_time: 0,
          details: [
            {
              sub_category_price_scope_id: 0,
              sub_category_price_scope_dtl_id: 0,
              value: ""
            }
          ]
        }
        offerBody.seller_offer_description = this.selected_gig_data.txtDescription;
        offerBody.gig_id = this.gig_id;
        offerBody.price_type_id = this.price_type_id;
        offerBody.offer_price = this.selected_gig_data.txtPrice;
        offerBody.offer_delivery_time = this.deliveryTimeValue;
        let selected_gig_forDetails = this.seller_gig_list.results.filter(element => element.id == this.gig_id);
        for (let i = 0; i < selected_gig_forDetails[0].gigs_pricing.length; i++) {
          if (selected_gig_forDetails[0].gigs_pricing[i].gig_price.price_type.id == this.price_type_id) {
            selected_gig_forDetails[0].gigs_pricing[i].gig_price_dtl.forEach(element => {
              if (element.sub_category_price_scope.price_scope_name == "Delivery Time") {
                offerBody.details.push({
                  sub_category_price_scope_id: element.sub_category_price_scope.id,
                  sub_category_price_scope_dtl_id: this.selected_gig_data.selected_delivery,
                  value: this.deliveryTimeValue
                })
              }
              else if (element.sub_category_price_scope.price_scope_name == "Max Revisions Allowed") {
                offerBody.details.push({
                  sub_category_price_scope_id: element.sub_category_price_scope.id,
                  sub_category_price_scope_dtl_id: this.selected_gig_data.selected_revision,
                  value: this.revisionValue
                })
              }
            });
          }
        }
        offerBody.details.splice(offerBody.details.findIndex(x => x.sub_category_price_scope_id == 0), 1);
        this.httpServices.request("patch", "requests/send-offer/" + this.request_offer_id, null, null, offerBody).subscribe((data) => {
          this.progress.hide();
          toastr.success("Offer has been created successfully.");
          this.BackToGigList();
          this.getrequestCount();
          this.getActiveRequest_gridData();
          jQuery('#gig_list').modal('hide');
        });
      }
      else {
        toastr.error("Please select Delivery time, Revision and enter description");
      }
    }

  }
  public packageSelected: boolean = false;
  selectFinalPackage(price_type_id: any) {
    if (price_type_id > 0) {
      this.price_type_id = price_type_id;
      this.packageSelected = true;
      let singleSelectPackage = this.selected_gig_data.package_list.filter(element => element.price_type_id == price_type_id);
      this.selected_gig_data.txtPrice = singleSelectPackage[0].package_price;
      this.selected_gig_data.selected_delivery = singleSelectPackage[0].package_delivery_time_id;
      this.selected_gig_data.selected_revision = singleSelectPackage[0].package_revisions_id;
      this.revisionList.forEach(element => {
        if (element.id == singleSelectPackage[0].package_revisions_id) {
          this.revisionValue = element.actual_value;
        }
      });
      this.deliveryList.forEach(element => {
        if (element.id == singleSelectPackage[0].package_delivery_time_id) {
          this.deliveryTimeValue = element.actual_value;
        }
      });
      jQuery('#select_package').modal("hide");
    }
    else {
      this.packageSelected = false;
    }
  }
}
interface gig_list {
  results: [
    {
      id: number,
      title: string,
      description: string,
      gallery: [
        {
          id: number,
          gig_gallery_type: {
            id: number,
            gig_gallery_type_name: string
          },
          file_path: string,
          file_path_thumbnail: string
        }
      ],
      gigs_pricing: [{
        gig_price: {
          id: number,
          price_type: {
            id: number,
            price_type_name: string
          },
          price: string
        },
        gig_price_dtl: [{
          id: number,
          sub_category_price_scope: {
            id: number,
            seq_no: number,
            price_scope_name: string,
            is_heading_visible: boolean,
            watermark: string,
            price_scope_control_type: {
              id: number,
              price_scope_control_type_name: string
            }
          },
          sub_category_price_scope_dtl: {
            id: number,
            seq_no: number,
            value: string,
            display_value: string,
            actual_value: number
          },
          value: string
        }]
      }]
    }
  ]
}
interface selected_gig_data {
  gig_title: string,
  file_path: string,
  txtDescription: string,
  revisionList: [{
    revisionId: number,
    revisionName: string
  }],
  deliveryList: [{
    deliveryId: number,
    deliveryName: string
  }],
  price: string,
  txtPrice: string,
  selected_revision: number,
  selected_delivery: number,
  package_list: [{
    price_type_id: number,
    price_type_name: string,
    package_price: string,
    package_delivery_time_id: number,
    package_delivery_time: string,
    package_revisions_id: number,
    package_revisions: string
  }]

}