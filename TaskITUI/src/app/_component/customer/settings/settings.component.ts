import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { NgxSpinnerService } from "ngx-spinner";
import { Router } from '@angular/router';
declare var toastr: any;
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  constructor(private progress: NgxSpinnerService, private router: Router, private httpServices: HttpRequestService, private encrypt_decrypt: EncryptDecryptService) { }
  public checkvalidationChangePassword: boolean = false;
  public autoCompCountryItem: any;
  public newPassword: string = "";
  public confirmPassword: string = "";
  public countryList: any;
  public getCountry: any = { id: 0, country_name: '' };
  public email: string = "";
  public stateList: any;
  public cityList: any;
  public getState: any = { id: 0, state_name: '' };
  public getCity: any = { id: 0, city_name: '' };
  public userAuthInfo: any;
  public userInfo: any = { currentUser: AuthInfo };
  public userId: any;
  public user_emailAddress: any;
  public deactivateReasonList: any = { id: 0, reason: '' };
  public deactivate_Reason_List: any;
  public other_reason: any;
  public currentPassword: any;
  public full_name: any;
  public company_name: any;
  public zip_postal_code: any;
  public address_line_one: any;
  public address_line_two: any;
  public addressId: any;
  public reasonHeadList: any;
  public is_account_active:any;
  ngOnInit() {
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    this.userId = this.userAuthInfo.currentUser.user.id;
    this.user_emailAddress = this.userAuthInfo.currentUser.user.email_address;
    this.is_account_active=this.userAuthInfo.currentUser.user.is_verified_email_address;
    this.getCountryList();
    this.getDeactivateReasonList();
    // this.getBank_accountDetails();
  }
  // ================================
  // get deactivate reason list 
  // ================================
  getDeactivateReasonList() {
    this.httpServices.request('get', 'deactivation_reason/', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.deactivate_Reason_List = data;
        }
      });
  }
  // ==============================
  // get reason list 
  // ===============================
  public tempList: any;
  public lableList: any;
  getReasonList(head_name: any, list: any[]) {
    if (this.tempList != undefined && this.tempList != null && this.tempList != "") {
      if (this.tempList.filter(x => x.head_name == head_name).length > 0) {

      }
      else {
        this.tempList = list.filter(x => x.head_name == head_name);
        return list.filter(x => x.head_name == head_name);
      }
    }
    else {
      this.tempList = list.filter(x => x.head_name == head_name);
      return this.tempList;
    }


  }
  getLableName(head_name: any, list: any[]) {

    if (this.lableList != undefined && this.lableList != null && this.lableList != "") {
      if (this.lableList.filter(x => x.head_name == head_name).length > 0) {

        return "";
      }
      else {
        this.lableList = list.filter(x => x.head_name == head_name);
        return head_name;
      }
    }
    else {
      this.lableList = list.filter(x => x.head_name == head_name);
      return head_name;
    }
  }
  public settoheadName: string = "";
  getLableNameTemp(head_name: any, list: any[]) {
    if (this.settoheadName != head_name) {
      if (this.lableList != undefined && this.lableList != null && this.lableList != "") {
        if (this.lableList.filter(x => x.head_name == head_name).length > 0) {
          this.settoheadName = head_name;
          return "";
        }
        else {
          this.lableList = list.filter(x => x.head_name == head_name);
          return head_name;
        }
      }
      else {
        this.lableList = list.filter(x => x.head_name == head_name);
        return head_name;
      }
    }
    else {
      if (this.lableList.filter(x => x.head_name == head_name).length == 0) {
        this.lableList = list.filter(x => x.head_name == head_name);
        return head_name;
      }
    }
  }
  //****************************
  //get Country List
  //****************************
  getCountryList() {

    this.httpServices.request('get', 'country/', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.countryList = data;
        }
      });

  }
  // ===============================
  // on click of deactivate account 
  // ===============================
  public checkvalidation_deactivteAccount: boolean = false;
  public deactivateAccount:string;
  onDeactivateAcc() {
    this.checkvalidation_deactivteAccount = true;

    let deactivateData: any = {
      email_address: "",
      user_deactivation_reason_id: 0,
      other_reason: ""
    }
    deactivateData.email_address = this.user_emailAddress;
    deactivateData.user_deactivation_reason_id = this.deactivateReasonList.id;
    deactivateData.other_reason = this.other_reason;

    if (this.other_reason == null || this.other_reason == undefined) {
      deactivateData.other_reason = null;
    }
    if (this.user_emailAddress != null && this.user_emailAddress != undefined && this.user_emailAddress != "" &&
      this.deactivateReasonList.id != 0 && this.deactivateReasonList.id != null && this.deactivateReasonList.id != undefined) {
      this.progress.show();
      this.httpServices.request('post', 'password/user-account-deactivate', '', '', deactivateData)
        .subscribe((data) => {
          this.deactivateAccount=data;
          this.progress.hide();
          this.checkvalidation_deactivteAccount = false;
          this.router.navigate(['/start_page'], {});
          toastr.success("This account has been deactivated successfully.");
        }, (error) => {
          this.progress.hide();
        });
    }
  }
  // =========================================
  // on click of save password 
  // =========================================
  onClickSavePassword() {
    this.checkvalidationChangePassword = true;
    if (this.currentPassword != null && this.currentPassword != undefined && this.currentPassword != "" &&
      this.newPassword != null && this.newPassword != undefined && this.newPassword != "" &&
      this.confirmPassword != null && this.confirmPassword != undefined && this.confirmPassword != "") {
      if (this.currentPassword == this.newPassword) {
        toastr.error("Current Password and New Password cannot be the same.");
      }
      else {
        if (this.newPassword == this.confirmPassword) {
          if (this.validatePassword(this.newPassword)) {
            this.progress.show();
            let changePassword: any = {
              current_password: "",
              new_password: ""
            }
            changePassword.current_password = this.currentPassword;
            changePassword.new_password = this.newPassword;
            this.httpServices.request("post", "password/reset", null, null, changePassword).subscribe((data) => {
              toastr.success("Password has been changed successfully.");
              this.checkvalidationChangePassword = false;
              this.progress.hide();
            }, (error) => {
              this.progress.hide();
            });
          }
        } else {
          toastr.error("New Password and Confirm Password should be same.");
        }
      }
    }
  }
  //==========================================================
  // validation for password.
  //==========================================================
  validatePassword(password: any) {
    let errors = [];
    if (password.length < 8) {
      errors.push("Your password must be at least 8 characters");
    }
    if (password.search(/[a-z]/i) < 0) {
      errors.push("one letter. ");
    }
    if (password.search(/[0-9]/) < 0) {
      errors.push("one number. ");
    }
    if (password.search(/[!@#$%&?*]/) < 0) {
      errors.push("one cspecial character.");
    }
    if (errors.length > 0) {
      //toastr.error("Your password must be 8 to 12 character, contains at least one number, one letter.");
      toastr.error("Minimum eight characters, at least one letter, one number and one special character!");
      return false;
    }
    return true;
  }

  // ValidatePassword(inputText: any, id: any) {
  //   if (inputText == '') {
  //     return true;
  //   }
  //   var passwordformat = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
  //   if (inputText.match(passwordformat)) {
  //     return true;
  //   }
  //   else {
  //     toastr.error("Minimum eight characters, at least one letter, one number and one special character!");
  //     document.getElementById(id).focus();
  //     return false;
  //   }
  // }



  //On Country Selection fill state List
  onChangeCountry() {

    if (this.getCountry.id > 0) {
      this.progress.show();
      this.httpServices.request('get', 'state/?country_id=' + this.getCountry.id, '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.stateList = data;
          }
          this.progress.hide();
        });
    }
    else {
      this.getCity.id = 0;
      this.getState.id = 0;
    }
  }
  // ==============================
  // on click of billing info 
  // ==============================
  onBillingInfoClick() {
    this.httpServices.request('get', 'user/addresses-list', '', '', null)
      .subscribe(data => {
        if (data != null && data != undefined && data != "") {
          if (this.stateList == null || this.stateList == undefined || this.stateList == "") {
            this.httpServices.request('get', 'state/', '', '', null)
              .subscribe(response1 => {
                if (response1.length > 0) {
                  this.stateList = response1;
                  this.getState.id = data[0].city.state.id;
                  if (this.cityList == null || this.cityList == undefined || this.cityList != "") {
                    this.httpServices.request('get', 'city/?state_id=' + this.getState.id, '', '', null)
                      .subscribe(response => {
                        if (response.length > 0) {
                          this.cityList = response;
                          this.getCity.id = data[0].city.id;
                        }

                      });
                  }
                  else {
                    this.getCity.id = data[0].city.id;
                  }
                }
              });

          }
          else {
            this.getState.id = data[0].city.state.id;
            if (this.cityList == null || this.cityList == undefined || this.cityList == "") {
              this.httpServices.request('get', 'city/?state_id=' + this.getState.id, '', '', null)
                .subscribe(response => {
                  if (response.length > 0) {
                    this.cityList = response;
                    this.getCity.id = data[0].city.id;
                  }

                });
            }
            else {
              this.getCity.id = data[0].city.id;
            }
          }

          this.addressId = data[0].id;
          this.full_name = data[0].address_nick_name;
          this.company_name = data[0].company_name;
          this.address_line_one = data[0].address_line1;
          this.address_line_two = data[0].address_line2;
          this.getCountry.id = data[0].city.state.country.id;
          this.zip_postal_code = data[0].pincode;
          this.getState.id = data[0].city.state.id;
          this.getCity.id = data[0].city.id;
        }
      });
  }
  // =====================================
  // on save billing info 
  // ======================================
  public checkvalidation_billingInfo: boolean = false;
  onSaveBillingInfo() {

    this.checkvalidation_billingInfo = true;
    let addressData: any = {
      address_nick_name: "",
      company_name: "",
      address_line1: "",
      address_line2: "",
      city_id: 0,
      pincode: 0,
      is_default: false
    }
    addressData.address_nick_name = this.full_name;
    addressData.company_name = this.company_name;
    addressData.address_line1 = this.address_line_one;
    addressData.address_line2 = this.address_line_two;
    addressData.city_id = this.getCity.id;
    addressData.pincode = this.zip_postal_code;
    addressData.is_default = true;

    if (this.full_name != null && this.full_name != undefined && this.full_name != ""
      // && this.company_name != null && this.company_name != undefined && this.company_name != ""
      && this.address_line_one != null && this.address_line_one != undefined && this.address_line_one != ""
      // && this.address_line_two != null && this.address_line_two != undefined && this.address_line_two != ""
      && this.getCity.id != 0 && this.getCity.id != null && this.getCity.id != undefined
      && this.zip_postal_code != 0 && this.zip_postal_code != null && this.zip_postal_code != undefined) {
      this.progress.show();
      if (this.addressId > 0) {
        this.httpServices.request('patch', 'user/addresses/' + this.addressId, '', '', addressData)
          .subscribe(data => {
            toastr.success("Record updated successfullty");
            this.checkvalidation_billingInfo = false;
            this.progress.hide();
          });

      }
      else {
        this.httpServices.request('post', 'user/addresses', '', '', addressData)
          .subscribe(data => {
            toastr.success("Record saved successfullty");
            this.checkvalidation_billingInfo = false;
            this.progress.hide();
          });
      }
    }
  }
  //****************************
  //get State List
  //****************************

  getStateList() {
    this.progress.show();
    this.httpServices.request('get', 'state/', '', '', null)
      .subscribe(data => {
        if (data.length > 0) {
          this.stateList = data;
        }
        this.progress.hide();
      });

  }


  //On State Selection fill state List
  onChangeState() {
    if (this.getState.id > 0) {
      this.progress.show();
      this.httpServices.request('get', 'city/?state_id=' + this.getState.id, '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.cityList = data;
          }
          this.progress.hide();
        });
    }
    else {
      this.getCity.id = 0;
      this.getState.id = 0;
    }
  }


  //==========================================================
  // Bank Account tab Methods
  //==========================================================
  public user_bankDetails: any;
  public checkvalidation_bankDetails: boolean = false;
  public account_holder_name: string;
  public account_no: any;
  public reEnter_account_no: any;
  public bank_name: string;
  public IFSC_code: any;
  public branch_name: string;
  public bank_address: any;
  public flag: boolean = false;
  public bank_acc_Id: number;

  getBank_accountDetails() {
    this.progress.show();
    this.httpServices.request("get", "user/bank-list", "", "", null).subscribe((data) => {
      this.user_bankDetails = data;

      if (data.length > 0 && data != null && data != undefined) {

        this.bank_acc_Id = data[0].id;
        this.account_holder_name = data[0].bank_account_name;
        this.account_no = data[0].bank_account_no;
        this.reEnter_account_no = data[0].bank_account_no;
        this.bank_name = data[0].bank_name;
        this.branch_name = data[0].branch_name;
        this.bank_address = data[0].bank_address;
        this.IFSC_code = data[0].bank_ifsc_code;
        this.progress.hide();
      }
      this.progress.hide();
    }, error => {
      console.log(error);
      this.progress.hide();
    });
  }

  //==========================================================
  // Bank Account Details POST Methods
  //==========================================================
  submitBank_details() {
    this.checkvalidation_bankDetails = true;
    let bank_details: any = {

      bank_account_name: "",
      bank_account_no: "",
      bank_name: "",
      branch_name: "",
      bank_address: "",
      bank_ifsc_code: ""

    }

    bank_details.bank_account_name = this.account_holder_name;
    bank_details.bank_account_no = this.account_no;
    bank_details.bank_name = this.bank_name;
    bank_details.branch_name = this.branch_name;
    bank_details.bank_address = this.bank_address;
    bank_details.bank_ifsc_code = this.IFSC_code;

    if (this.account_no == this.reEnter_account_no) {
      if (this.account_holder_name != null && this.account_holder_name != undefined && this.account_holder_name != "" &&
        this.account_no != null && this.account_no != undefined && this.account_no != "" &&
        this.reEnter_account_no != null && this.reEnter_account_no != undefined && this.reEnter_account_no != "" &&
        this.bank_name != null && this.bank_name != undefined && this.bank_name != "" &&
        this.branch_name != null && this.branch_name != undefined && this.branch_name != "" &&
        // this.bank_address != null && this.bank_address != undefined && this.bank_address != ""&&
        this.IFSC_code != null && this.IFSC_code != undefined && this.IFSC_code != "" 
        ) {

        if (this.bank_acc_Id > 0) {
          this.progress.show();
          this.httpServices.request("patch", "user/bank-list/" + this.bank_acc_Id, "", "", bank_details).subscribe((data) => {
            toastr.success("Details Updated Successfully");
            this.checkvalidation_bankDetails = false;
            //this.flag=true;
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        } else {
          this.httpServices.request("post", "user/bank-list", "", "", bank_details).subscribe((data) => {
            toastr.success("Details Submitted Successfully");
            this.checkvalidation_bankDetails = false;
            this.progress.hide();
          }, error => {
            console.log(error);
            this.progress.hide();
          });
        }
      }
    } else {
      toastr.error("Account number and Re-Enter Account number must be same !")
    }
  }

  //==========================================================
  // Validate Bank Account 
  //==========================================================
  Validate_bankAccountNo(inputText: any, id: any) {
    if (inputText == '') {
      return true;
    }
    //var passwordformat = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    var bankAcc_no = /^\d{9,18}$/;
    if (inputText.match(bankAcc_no)) {
      return true;
    }
    else {
      toastr.error("Please enter valid bank account number with minimum 9 characters or maximum 18 characters!");
      document.getElementById(id).focus();
      return false;
    }
  }

  Validate_pinCode(inputText: any, id: any) {
    if (inputText == '') {
      return true;
    }
    //var passwordformat = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    var pin_code = /[1-9][0-9]{5}/;
    if (inputText.match(pin_code)) {
      return true;
    }
    else {
      toastr.error("Please enter valid pincode");
      document.getElementById(id).focus();
      return false;
    }
  }
  // var patt = /^([0-9]{11})|([0-9]{2}-[0-9]{3}-[0-9]{6})$/;
  // patt.test('acdbdfdsfsf22-333-666666'); // true
  //==========================================================
  // Validate Bank IFSC code
  //==========================================================

  Validate_IFSC_code(inputText: any, id: any) {
    if (inputText == '') {
      return true;
    }

    //var IFSC_code=/^[A-Za-z]{4}[a-zA-Z0-9]{7}$/;
    var IFSC_code = /^[A-Z]{4}0[A-Z0-9]{6}$/;
    if (inputText.match(IFSC_code)) {
      return true;
    }
    else {
      toastr.error("Please enter valid IFSC code !");
      document.getElementById(id).focus();
      return false;
    }
  }
  //As per definitions of IFSC code posted here in other answers, it is first 4 characters as digit and remaining 7 characters as alphanumeric , regex would be

  //   regex = "^[A-Z]{4}0[A-Z0-9]{6}$";
  // Where: 
  // ^ represents the starting of the string.
  // [A-Z]{4} represents the first four characters should be upper case alphabets.
  // 0 represents the fifth character should be 0.
  // [A-Z0-9]{6} represents the next six characters usually numeric, but can also be alphabetic.
  // $ represents the ending of the string.

  //==========================================================
  //  method for accept only integer number.
  //==========================================================
  onlyNumbers(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if ((charCode >= 48 && charCode <= 57))
      return true;
    return false;
  }

  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  onlyCharacters_withApostrophe(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code == 39) && //apostrophe
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }
  //*********************************
  // Save Btn Click for  password in Security Tab
  //*********************************
  // onSaveBtn(form: NgForm) {
  //   if (form != undefined && form.valid) {
  //     this.httpServices.request('post', 'post/', '', '', null)
  //       .subscribe(data => {

  //       })
  //   }
  // }

}
