import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';

@Component({
  selector: 'app-subCategories_screen',
  templateUrl: './subCategories_screen.component.html',
  styleUrls: ['./subCategories_screen.component.css']
})
export class SubCategories_screenComponent implements OnInit {

  public queryParam:any;
  public _queryParam:any;
  public category_Id:any;
  public subCategoriesList:any;
  public categoryName:string;
  public categoryMenubarList:any;
  
  constructor(private progress: NgxSpinnerService, private httpServices: HttpRequestService,private router: Router,private encrypt_decrypt: EncryptDecryptService) { }

  ngOnInit() {
    //this.getCategoriesBar();

    if(this.queryParam==null && this.queryParam==undefined){
      this._queryParam=(sessionStorage.getItem("queryCategory_idParam")|| '{}');
      this.queryParam=JSON.parse(this._queryParam);
      this.category_Id=this.queryParam.category_id;
      this.getSubcategoriesList(this.category_Id);
    }
  }

  //**************************************************
  //Categories Menu Bar same as that on Home Page
  //**************************************************
  // getCategoriesBar(){
  //   this.httpServices.request("get","categories/subcategory-list?is_active=true","","",null).subscribe((data)=>{
  //     this.categoryMenubarList=data;
  //   },error=>{
  //     console.log(error);
  //   });
  // }

  getSubcategoriesList(category_Id:any){
    this.progress.show();
    this.httpServices.request("get","subcategories/list?category_id="+ category_Id,"","",null).subscribe((data)=>{
      this.subCategoriesList=data;

      if(this.subCategoriesList.length > 0){
        this.categoryName=this.subCategoriesList[0].category.category_name;
      }
      this.progress.hide();
      //this.categoryName=this.subCategoriesList.category.category_name;
    },error=>{
      console.log(error);
      this.progress.show();
    });
  }

  //********************************************
  // Onsubcategory click to Search Result Page
  //********************************************
  onSubcategoryClick(sub_category_name:string){
    let passQueryParam={search_keyword:sub_category_name};
    sessionStorage.setItem("searchQueryParams",JSON.stringify(passQueryParam));
    this.encrypt_decrypt.changeDataGigSearch({ flag: true, location: "index_screen"});
    this.router.navigate(['/search_result']);
  }
}
