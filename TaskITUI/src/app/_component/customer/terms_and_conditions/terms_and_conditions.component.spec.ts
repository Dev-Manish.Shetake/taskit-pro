/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Terms_and_conditionsComponent } from './terms_and_conditions.component';

describe('Terms_and_conditionsComponent', () => {
  let component: Terms_and_conditionsComponent;
  let fixture: ComponentFixture<Terms_and_conditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Terms_and_conditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Terms_and_conditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
