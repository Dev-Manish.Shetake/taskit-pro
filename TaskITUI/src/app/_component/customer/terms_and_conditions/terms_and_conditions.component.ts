import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
@Component({
  selector: 'app-terms_and_conditions',
  templateUrl: './terms_and_conditions.component.html',
  styleUrls: ['./terms_and_conditions.component.css']
})
export class Terms_and_conditionsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    jQuery('body').scrollTop(0);
  }

}
