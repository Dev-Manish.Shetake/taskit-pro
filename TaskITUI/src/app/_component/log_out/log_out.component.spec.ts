/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Log_outComponent } from './log_out.component';

describe('Log_outComponent', () => {
  let component: Log_outComponent;
  let fixture: ComponentFixture<Log_outComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Log_outComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Log_outComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
