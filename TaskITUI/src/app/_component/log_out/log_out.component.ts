import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-log_out',
  templateUrl: './log_out.component.html',
  styleUrls: ['./log_out.component.css']
})
export class Log_outComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    localStorage.removeItem("currentUser");
    localStorage.removeItem("userAccess");
    localStorage.removeItem("Token");
  }
  
  login_click(){
    window.location.href = "start_page";
  }
}
