import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { EncryptDecryptService } from '../../_common_services/encrypt-decrypt.service';
import { Load_owl_carouselService } from '../../_common_services/load_owl_carousel.service';
import { OwlOptions } from 'ngx-owl-carousel-o';

declare var jQuery: any;

@Component({
  selector: 'app-start_page',
  templateUrl: './start_page.component.html',
  styleUrls: ['./start_page.component.css']
})
export class Start_pageComponent implements OnInit {

  constructor(private router: Router, private progress: NgxSpinnerService, private httpServices: HttpRequestService,
    private encrypt_decrypt: EncryptDecryptService, private load_owl_carousel: Load_owl_carouselService) {
    this.loadScripts();
    //this.load_owl_carousel.activePlugin();
  }
  loadScripts() {
    // This array contains all the files/CDNs 
    const dynamicScripts = [
      'assets/js/owl.carousel.min.js',
      'assets/js/custom.js'
    ];

    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
  public categoryList: any;
  public searchString: any = { search_keyword: '' };
  public autoCompListItem: Array<{ id: string, search_keyword: string }> = [];
  public gigSearchResult: any;
  public mostPopularservicesList: any;
  public pageSize = 5;

  ngOnInit() {
    //this.load_owl_carousel.activePlugin();
    this.httpServices.request('get', 'categories/subcategory-list?is_active=true', null, null, null).subscribe((data) => {
      this.categoryList = data;
    });
    //this.progress.show();
    // setTimeout(() => {
    //   /* spinner ends after 5 seconds */
    //   this.progress.hide();
    // }, 5000);
    this.mostPopularservices();
  }
  // =======================================
  // On Search button click 
  // =======================================
  public showValidationsearch: boolean = false;
  onSearchClick() {
    if (this.searchString.search_keyword == "" || this.searchString.search_keyword == null || this.searchString.search_keyword == undefined) {
      this.showValidationsearch = true;
    }
    else {
      this.showValidationsearch = false;
      sessionStorage.setItem("searchQueryParams", JSON.stringify(this.searchString));
      this.encrypt_decrypt.changeDataGigSearch({ flag: true, location: "index_screen"});
      this.router.navigate(['/search_result'], {});
    }

  }

  //==========================================================
  // Search complete extendar. 
  //==========================================================
  handleSearchTypeFilter(value: string) {
    this.autoCompListItem = undefined;
    this.searchString.search_keyword = "";
    if (value.length > 0) {
      this.searchString.search_keyword = value;
      this.httpServices.request('get', 'gigs/search-extender?search_keyword=' + value, '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompListItem = data;
          }
        });
    }
  }

  //==========================================================
  //  Search's auto complete on select event       
  //==========================================================
  onSpanSearchTypeSelected(dataitem: any) {
    this.searchString.search_keyword = dataitem.search_keyword;
  }

  //==========================================================
  //  Most Popular Services    
  //==========================================================
  customOptions: OwlOptions = {
    loop: true,
    margin: 20,
    mouseDrag: true,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 200,
    navText: ['&#8249', '&#8250;'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      760: {
        items: 3
      },
      1000: {
        items: 5
      }
    },
    nav: true
  }

  public dynamicSlides: any = [];
  mostPopularservices() {
    this.httpServices.request('get', 'home_screen/popular-services?page=1&page_size=' + this.pageSize, '', '', '').subscribe((data) => {
      this.mostPopularservicesList = data.results;
      if (data.results.length > 0) {
        data.results.forEach(x => {
          this.dynamicSlides.push({
            id: x.id,
            sub_category_name: x.sub_category_name,
            sub_category_picture: "https://fiverr-res.cloudinary.com/q_auto,f_auto,w_550,dpr_1.0/v1/attachments/generic_asset/asset/055f758c1f5b3a1ab38c047dce553860-1598561741669/voiceover-2x.png",//x.sub_category_picture,
            sub_category_thumb_picture: x.sub_category_thumb_picture,
            tagline: x.tagline
          })
        });
      }
    }, error => {
      console.log(error);
    });
  }

  //==========================================================
  // On Categories Bar click to SubCtegories Screen  
  //==========================================================

  onCategory_barClick(category_id: number) {
    let passQuesyParam = { category_id: category_id };
    sessionStorage.setItem("queryCategory_idParam", JSON.stringify(passQuesyParam));
    this.router.navigate(['/subCategories_screen']);
  }

  //********************************************
  // Onsubcategory click to Search Result Page
  //********************************************
  onSubcategoryClick(sub_category_name:string){
    let passQueryParam={search_keyword:sub_category_name};
    sessionStorage.setItem("searchQueryParams",JSON.stringify(passQueryParam));
    this.encrypt_decrypt.changeDataGigSearch({ flag: true, location: "index_screen"});
    this.router.navigate(['/search_result']);
  }
}
