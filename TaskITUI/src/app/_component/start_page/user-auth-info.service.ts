import { Injectable } from '@angular/core';
import { AuthInfo, Token } from 'src/app/_models/auth-info';

@Injectable({
  providedIn: 'root'
})
export class UserAuthInfoService {

  constructor() { }

  get access(): Token {
    let userAccess: Token = JSON.parse(localStorage.getItem("Token"));
    if (userAccess == null || userAccess == undefined)
      userAccess = new Token();
    return userAccess;
  }

  set access(data) {
    localStorage.setItem("Token", JSON.stringify(data));
  }

  get currentUser(): AuthInfo {
    let user: AuthInfo = JSON.parse(localStorage.getItem("currentUser"));
    if (user == null || user == undefined)
      user = new AuthInfo();

    return user;
  }

  set currentUser(data) {
    localStorage.setItem("currentUser", JSON.stringify(data));
  }
  
}
