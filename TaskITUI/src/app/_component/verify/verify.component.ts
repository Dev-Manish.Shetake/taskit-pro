import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpRequestService } from '../../_common_services/httprequest.service';
import { EncryptDecryptService } from 'src/app/_common_services/encrypt-decrypt.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { NgxSpinnerService } from "ngx-spinner";
import * as CryptoJS from 'crypto-js';

declare var toastr: any;
@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {
  public verification_code: any;
  public verificationFlag: boolean;
  public queryParam: any;
  public _queryParams: any;
  constructor(private router: Router, private progress: NgxSpinnerService, private encrypt_decrypt: EncryptDecryptService, private httpServices: HttpRequestService) { }

  public userAuthInfo: any;
  public userInfo: any;
  public key = CryptoJS.enc.Utf8.parse('SOMERANDOMKEY123');
  public user_name: string;
  public password: string;
  public confirm_password: string;
  public userType_id: any;
  public user_id: any;
  public checkValidation_addyourdetails: boolean = false;

  ngOnInit() {
    
    //this.verificationFlag = false;
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;
    if (this.queryParam == null || this.queryParam == undefined) {
      this._queryParams = (sessionStorage.getItem("queryCode") || '{}');
      this.queryParam = JSON.parse(this._queryParams);
      if (this.queryParam != null && this.queryParam != "null" && this.queryParam != undefined) {
        this.verification_code = this.queryParam.code;
      }
    }
    let verify_data = { verification_code: this.verification_code }
    this.httpServices.request("post", "user/verify", null, null, verify_data).subscribe(data => {
      this.verificationFlag = data.is_verified;
      if (data.user_type.user_type_id == 1) {
        this.user_name = data.user_name;
        this.user_id = data.user_id;
        this.userType_id = data.user_type.user_type_id;
        this.verificationFlag = undefined;
      }
      else if (this.userInfo.user.id == data.user_id && data.is_verified == true) {
        this.userInfo.user.is_verified_email_address = true;
        if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
          this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
          localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
        }
        else {
          localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
        }
      }
    });
  }

  encrypt(msgString, key) {
    // msgString is expected to be Utf8 encoded
    var iv = CryptoJS.lib.WordArray.random(16);
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify(msgString), key, {
      iv: iv
    });
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
  }

  // =======================================================
  // Click on Logo 
  // =======================================================
  clickOnLogo() {
    this.router.navigate(['/start_page'], {});
  }

  decrypt(ciphertextStr, key) {
    var ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);
    // split IV and ciphertext
    var iv = ciphertext.clone();
    iv.sigBytes = 16;
    iv.clamp();
    ciphertext.words.splice(0, 4); // delete 4 words = 16 bytes
    ciphertext.sigBytes -= 16;

    // decryption
    var decrypted = CryptoJS.AES.decrypt({ ciphertext: ciphertext }, key, {
      iv: iv
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }

  onBackofficeUser_register() {
    this.checkValidation_addyourdetails = true;
    if (this.user_name != null && this.user_name != undefined && this.user_name != ''
      && this.password != null && this.password != undefined && this.password != ''
      && this.confirm_password != null && this.confirm_password != undefined && this.confirm_password != '') {
      this.progress.show();
      if (this.password == this.confirm_password) {
        let login_info = { user_name: this.user_name, password: this.password }
        this.httpServices.request('patch', 'auth/sign-up-register/' + this.user_id, null, null, login_info).subscribe((data) => {
          toastr.success("User Registration Successfull");
          this.router.navigate(['/start_page'], {});
          this.progress.hide();
        }, error => {
          console.log(error);
          this.progress.hide();
        });
      }
      else {
        this.progress.hide();
        toastr.error("Password and Confirm Password must be same");
      }
    }
  }

  
  ValidatePassword(inputText: any, id: any) {
    if (inputText == '') {
      return true;
    }
    var passwordformat = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    if (inputText.match(passwordformat)) {
      return true;
    }
    else {
      toastr.error("Minimum eight characters, at least one letter, one number and one special character!");
      document.getElementById(id).focus();
      return false;
    }
  }
}
