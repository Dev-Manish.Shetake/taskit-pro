import { User } from "./user";

export class AuthInfo {
    access: number;
    refresh: string;
    user: CurrentUser;
    project_code:string
    // sys_config: sysConfig
}
export class sysConfig {
}


export class CurrentUser {
    id: number;
    email_address: string;
    user_name: string;
    permissions: string[];
    group_name: string;
    group_id: number;
}


export class Token {
    token: string;
}
