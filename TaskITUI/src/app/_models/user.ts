export class User {
    id: number;
    email_address: string;
    password: string;
    name: string;
    lang:string;    
    token: string;
}