import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Start_pageComponent } from './_component/start_page/start_page.component';
import { Become_buyerComponent } from './_component/customer/buyer/become_buyer/become_buyer.component';
import { Become_sellerComponent } from './_component/customer/seller/become_seller/become_seller.component';
import { AboutComponent } from './_component/customer/about/about.component';
import { HomeComponent } from './_component/customer/home/home.component';
import { OverviewComponent } from './_component/customer/overview/overview.component';
import { Personal_infoComponent } from './_component/customer/become_seller/personal_info/personal_info.component';
import { Log_outComponent } from './_component/log_out/log_out.component';
import { New_gigComponent } from './_component/customer/seller/new_gig/new_gig.component';
import { Dashboard_adminComponent } from './_component/admin/dashboard_admin/dashboard_admin.component';
import { Help_and_supportComponent } from './_component/customer/help_and_support/help_and_support.component';
import { Terms_and_conditionsComponent } from './_component/customer/terms_and_conditions/terms_and_conditions.component';
import { ContactComponent } from './_component/customer/contact/contact.component';
import { Buyer_profileComponent } from './_component/customer/buyer_profile/buyer_profile.component';
import { SettingsComponent } from './_component/customer/settings/settings.component';
import { Seller_requestComponent } from './_component/customer/seller_request/seller_request.component';
import { Post_a_requestComponent } from './_component/customer/post_a_request/post_a_request.component';
import { Seller_dashboardComponent } from './_component/customer/seller/seller_dashboard/seller_dashboard.component';
import { Gig_detailsComponent } from './_component/customer/gig_details/gig_details.component';
import { Search_resultComponent } from './_component/customer/search_result/search_result.component';
import { ReviewsComponent } from './_component/admin/reviews/reviews.component';
import { OrdersComponent } from './_component/customer/seller/orders/orders.component';
import { Seller_order_commonComponent } from './_common_component/seller_order_common/seller_order_common.component';
import { Buyer_order_commonComponent } from './_common_component/buyer_order_common/buyer_order_common.component';
import { Buyer_ordersComponent } from './_component/customer/buyer/buyer_orders/buyer_orders.component';
import { Cart_pageComponent } from './_component/customer/cart_page/cart_page.component';
import { Settings_adminComponent } from './_component/admin/settings_admin/settings_admin.component';
import { Gig_listComponent } from './_component/customer/gig_list/gig_list.component';
import { Buyer_dashboardComponent } from './_component/customer/buyer/buyer_dashboard/buyer_dashboard.component';
import {Gig_list_adminComponent} from './_component/admin/gig_list_admin/gig_list_admin.component';
import { FavoriteComponent } from './_component/customer/favorite/favorite.component';
import { Seller_profileComponent } from './_component/customer/seller_profile/seller_profile.component';
import { Users_listComponent } from './_component/admin/users_list/users_list.component';
import { Gigs_categoryComponent } from './_component/admin/gigs_category/gigs_category.component';
import { Gigs_SubcategoryComponent } from './_component/admin/gigs_Subcategory/gigs_Subcategory.component';
import { Total_OrdersComponent } from './_component/admin/orders/total_Orders/total_Orders.component';
import { Completed_OrdersComponent } from './_component/admin/orders/completed_Orders/completed_Orders.component';
import { Pending_OrdersComponent } from './_component/admin/orders/pending_Orders/pending_Orders.component';
import { Cancelled_OrdersComponent } from './_component/admin/orders/cancelled_Orders/cancelled_Orders.component';
import { Declined_OrdersComponent } from './_component/admin/orders/declined_Orders/declined_Orders.component';
import { Dispute_OrdersComponent } from './_component/admin/orders/dispute_Orders/dispute_Orders.component';
import { ProfessionComponent } from './_component/admin/profession/profession.component';
import { Incoming_withdrawalComponent } from './_component/admin/withdrawal/incoming_withdrawal/incoming_withdrawal.component';
import { Paid_withdrawalComponent } from './_component/admin/withdrawal/paid_withdrawal/paid_withdrawal.component';
import { VerifyComponent } from './_component/verify/verify.component';
import { SubCategories_screenComponent } from './_component/customer/subCategories_screen/subCategories_screen.component';
import {Order_detailsComponent} from './_component/customer/order_details/order_details.component';
import { Metadata_listComponent } from './_component/admin/metadata_list/metadata_list.component';
import { Admin_Post_a_requestComponent } from './_component/admin/admin_Post_a_request/admin_Post_a_request.component';
import { Buyer_manage_requestComponent } from './_component/customer/buyer_manage_request/buyer_manage_request.component';
import { Metadata_AddComponent } from './_component/admin/metadata_Add/metadata_Add.component';
import { Edit_Post_a_requestComponent } from './_component/admin/edit_Post_a_request/edit_Post_a_request.component';
import { Gig_offersComponent } from './_component/customer/gig_offers/gig_offers.component';
import { Price_scope_listComponent } from './_component/admin/price_scope_list/price_scope_list.component';
import { Add_price_scope_listComponent } from './_component/admin/add_price_scope_list/add_price_scope_list.component';
import { PaymentComponent } from './_component/customer/payment/payment.component';
import {Seller_offersComponent} from './_component/customer/seller/seller_offers/seller_offers.component';
import { AnalyticsComponent } from './_component/customer/analytics/analytics.component';
import { EarningsComponent } from './_component/customer/earnings/earnings.component';
import { PrivacyPolicyComponent } from './_component/customer/privacy-policy/privacy-policy.component';
import { RefundComponent } from './_component/admin/refund/refund.component';

const routes: Routes = [
  { path: '', component: Start_pageComponent },
  { path: 'order_details', component: Order_detailsComponent },
  { path: 'start_page', component: Start_pageComponent },
  { path: 'about', component: AboutComponent },
  { path: 'become_buyer', component: Become_buyerComponent },
  { path: 'become_seller', component: Become_sellerComponent },
  { path: 'home', component: HomeComponent },
  { path: 'overview', component: OverviewComponent },
  { path: 'personal_info', component: Personal_infoComponent },
  { path: 'log_out', component: Log_outComponent },
  { path: 'new_gig', component: New_gigComponent },
  { path: 'admin_dashboard', component: Dashboard_adminComponent },
  { path: 'help_and_support', component: Help_and_supportComponent },
  { path: 'terms_and_conditions', component: Terms_and_conditionsComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'buyer_profile', component: Buyer_profileComponent },
  { path: 'settings', component: SettingsComponent },
  { path: 'seller_request', component: Seller_requestComponent },
  { path: 'post_a_request', component: Post_a_requestComponent },
  { path: 'seller_dashboard', component: Seller_dashboardComponent },
  { path: 'gigs_details', component: Gig_detailsComponent },
  { path: 'search_result', component: Search_resultComponent },
  { path: 'reviews', component: ReviewsComponent },
  { path: 'seller_orders', component: OrdersComponent },
  { path: 'buyer_orders', component: Buyer_ordersComponent },
  { path: 'cart', component: Cart_pageComponent },
  { path: 'settings_admin', component: Settings_adminComponent },
  { path: 'gig_list', component: Gig_listComponent },
  { path: 'buyer_dashboard', component: Buyer_dashboardComponent },
  { path: 'gig_list_admin', component: Gig_list_adminComponent },
  { path: 'favorite', component: FavoriteComponent },
  { path: 'seller_profile', component: Seller_profileComponent },
  { path: 'users_list', component: Users_listComponent },
  { path: 'gigs_category', component: Gigs_categoryComponent },
  { path: 'gigs_Subcategory', component: Gigs_SubcategoryComponent },
  { path: 'total_Orders', component: Total_OrdersComponent },
  { path: 'completed_Orders', component: Completed_OrdersComponent },
  { path: 'pending_Orders', component: Pending_OrdersComponent },
  { path: 'cancelled_Orders', component: Cancelled_OrdersComponent },
  { path: 'declined_Orders', component: Declined_OrdersComponent },
  { path: 'dispute_Orders', component: Dispute_OrdersComponent },
  { path: 'profession', component: ProfessionComponent },
  { path: 'incoming_withdrawal', component: Incoming_withdrawalComponent },
  { path: 'paid_withdrawal', component: Paid_withdrawalComponent },
  { path: 'verify', component: VerifyComponent },
  { path: 'subCategories_screen', component: SubCategories_screenComponent },
  { path: 'metadata_list', component: Metadata_listComponent },
  { path: 'admin_post_a_request', component: Admin_Post_a_requestComponent },
  { path: 'manage_request', component: Buyer_manage_requestComponent },
  { path: 'metadata_add', component: Metadata_AddComponent },
  { path: 'edit_post_request', component: Edit_Post_a_requestComponent },
  { path: 'gig_offers', component: Gig_offersComponent },
  { path: 'price_scope_list', component: Price_scope_listComponent },
  { path: 'add_price_scope_list', component: Add_price_scope_listComponent },
  { path: 'payment', component: PaymentComponent },
  { path: 'seller_offer', component: Seller_offersComponent },
  { path: 'analytics', component: AnalyticsComponent },
  { path: 'earnings', component: EarningsComponent },
  { path: 'privacy_policy', component: PrivacyPolicyComponent },
  { path: 'refund', component: RefundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
