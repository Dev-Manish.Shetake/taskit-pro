// import { Component } from '@angular/core';
// import { Router, NavigationEnd } from '@angular/router';
// import { filter } from 'rxjs/operators';
// import { EncryptDecryptService } from './_common_services/encrypt-decrypt.service';

import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SocialAuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider } from "angularx-social-login";
import { Socialusers } from '../app/_models/socialusers'
import { NgxSpinnerService } from "ngx-spinner";
import { HttpRequestService } from 'src/app/_common_services/httprequest.service';
import { EncryptDecryptService } from '../app/_common_services/encrypt-decrypt.service';
import { AuthInfo, Token } from 'src/app/_models/auth-info';
import * as CryptoJS from 'crypto-js';
import { NgForm } from '@angular/forms';
import { PopupCloseEvent } from '@progress/kendo-angular-grid';
import { Load_owl_carouselService } from './_common_services/load_owl_carousel.service';
import { $ } from 'protractor';
declare var jQuery: any;
declare var toastr: any;

//declare let ga: Function;
@Component({
  selector: 'app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'TaskITUI';
  public hideMenu: boolean = false;
  public userAuthInfo: any;
  public isLoaded: any = true;
  public rightPanelClass;// ="right-panel";

  response;
  social_users = new Socialusers();
  public after_login: boolean = false;
  public key = CryptoJS.enc.Utf8.parse('SOMERANDOMKEY123');
  public emailAddressForRegister: any;
  public userIdAfterRegister: any;
  public userName_addyourdetails: any;
  public password_addyourdetails: any;
  public confirmPassword_addyourdetails: any;
  public emailAddress_forgotPasswd: string = '';
  public otp_forgotPasswdOTP: any;
  public password_forgotPasswdOTP: any;
  public checkValidation_continue_register: boolean = false;
  public checkValidation_addyourdetails: boolean = false;
  public checkValidation_forgotPasswd: boolean = false;
  public checkValidation_verifyOTP: boolean = false;
  public userInfo: any = { currentUser: AuthInfo };
  public group_id: Number = 0;
  public currentYear: any;
  public showTick: boolean = null;
  public isEmailverified: boolean = false;
  public searchString: any = { search_keyword: '' };
  public autoCompListItem: Array<{ search_id: string, search_keyword: string }> = [];
  public userName: string = "";
  public addDetailsCheck: boolean = false;
  public showSpinner: boolean = false;
  public signIN_email: any;
  public signIN_password: any;

  constructor(private router: Router, private progress: NgxSpinnerService, private httpServices: HttpRequestService,
    private encrypt_decrypt: EncryptDecryptService, private authService: SocialAuthService, private load_owl_carousel: Load_owl_carouselService) {
    // subscribe to router events and send page views to Google Analytics
    // this.router.events.subscribe(event => {
    //   if (event instanceof NavigationEnd) {
    //     ga('set', 'page', event.urlAfterRedirects);
    //     ga('send', 'pageview');
    //   }
    // });
    // const styles = document.createElement('link');
    // styles.href = '/assets/js/toastr/toastr.min.css?v4.0.3';
    // styles.rel = 'stylesheet';
    // styles.type = 'text/css';
    // document.getElementsByTagName('head')[0].appendChild(styles);
    //this.load_owl_carousel.activePlugin();
    this.loadScripts();

    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    if (this.userAuthInfo.currentUser.user != undefined) {
      this.group_id = this.userAuthInfo.currentUser.user.group_id;
    }
  }
  // Method to dynamically load JavaScript 
  loadScripts() {
    // This array contains all the files/CDNs 
    const dynamicScripts = [
      // '/assets/js/owl.carousel.min.js',
      '/assets/js/jquery.meanmenu.js',
      '/assets/js/metisMenu.min.js',
      '/assets/js/metisMenu-active.js',
      '/assets/js/jquery.sticky.js',
      '/assets/js/main.js'
    ];

    for (let i = 0; i < dynamicScripts.length; i++) {
      const node = document.createElement('script');
      node.src = dynamicScripts[i];
      node.type = 'text/javascript';
      node.async = false;
      document.getElementsByTagName('head')[0].appendChild(node);
    }
  }
  public categoryList: any;
  ngOnInit() {
    this.encrypt_decrypt.favData$.subscribe(x => { this.showSignIN(x); })
    this.encrypt_decrypt.cartData$.subscribe(x => { this.showSignINOncart(x); })
    this.encrypt_decrypt.switchSelling$.subscribe(x => { this.group_id = 3; this.switch_selling(x); })
    // toastr.options.closeButton = true;
    // this.hideMenu = false;
    // this.rightPanelClass = 'right-panel';
    this.currentYear = new Date().getFullYear();
    this.userAuthInfo = this.encrypt_decrypt.decryptUserInfo();
    this.userInfo = this.userAuthInfo.currentUser;

    if (this.userAuthInfo.currentUser.user != undefined) {
      this.group_id = this.userAuthInfo.currentUser.user.group_id;
      this.userName = this.userAuthInfo.currentUser.user.user_name;
    }
    this.httpServices.request('get', 'categories/subcategory-list?is_active=true', null, null, null).subscribe((data) => {
      this.categoryList = data;
    });

    if (window.location.pathname == '/verify') {
      let code: any;
      code = window.location.search.split('=').length > 1 ? window.location.search.split('=')[1] : '';
      let passQueryParam = { code: code };
      sessionStorage.setItem("queryCode", JSON.stringify(passQueryParam));
      this.router.navigate(['/verify'], {});
    }
    if (this.userAuthInfo.currentUser.user == undefined || this.userAuthInfo.currentUser.project_code != this.encrypt_decrypt.projectCode) {
      this.after_login = false;
    }
    else {
      this.after_login = true;
    }
    // else if (this.userAuthInfo.currentUser.user == undefined || this.userAuthInfo.currentUser.project_code != this.encrypt_decrypt.projectCode) {
    //   this.router.navigate(['/start_page'], {});
    //   this.after_login = false;
    //   localStorage.removeItem("currentUser");
    //   localStorage.removeItem("userAccess");
    //   localStorage.removeItem("Token");
    // }
    // else {
    // if (this.userAuthInfo.currentUser.user.group_id == 1) {
    //   this.router.navigate(['/admin_dashboard'], {});
    // }
    //   else if (this.userAuthInfo.currentUser.user.group_id == 2) {
    //     this.router.navigate(['/home'], {});
    //   }
    //   else if (this.userAuthInfo.currentUser.user.group_id == 3) {
    //     this.router.navigate(['/seller_dashboard'], {});
    //   }
    //   else {
    //     this.router.navigate(['/home'], {});
    //   }
    //   this.after_login = true;
    // }
    this.routing();
  }


  //==========================================================
  // On Categories Bar click to SubCtegories Screen  
  //==========================================================

  onCategory_barClick(category_id: number) {
    let passQuesyParam = { category_id: category_id };
    sessionStorage.setItem("queryCategory_idParam", JSON.stringify(passQuesyParam));
    this.router.navigate(['/subCategories_screen']);
  }

  setLoginFlag = (val) => {
    this.hideMenu = val;
    this.rightPanelClass = this.hideMenu == true ? '' : 'right-panel'
  }

  routing() {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        let url = urlAfterRedirects.split('?')[0];
        this.showHidePageContent(url);
      });
  }

  showHidePageContent(currentUrl: any) {
    let pagesWithoutHeaderNMenu = ["login", "log_out", "verify", "reset", "notify", "info-page"];
    if (pagesWithoutHeaderNMenu.filter(val => currentUrl.indexOf(val) > -1).length > 0) {
      this.hideMenu = true;
      this.rightPanelClass = '';
    }
    else {
      this.hideMenu = false;
      this.rightPanelClass = 'right-panel';
    }
  }

  //==========================================================
  //  Call social login POP (Google or Facebook)
  //==========================================================
  public socialSignIn(socialProvider: string) {
    let socialPlatformProvider;
    if (socialProvider === 'facebook') {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialProvider === 'google') {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.authService.signIn(socialPlatformProvider).then(social_users => {
      this.saves_response(social_users);
    });
  }

  //==========================================================
  //  Register or Get authenticate in system after social login verification
  //==========================================================
  saves_response(social_users: Socialusers) {
    //console.log(social_users);
    this.progress.show();
    let login_info = { email_address: social_users.email, user_name: social_users.email.substring(0, social_users.email.indexOf('@')), provider: social_users.provider };
    this.httpServices.request('post', 'social_auth/login', null, null, login_info).subscribe((data) => {
      if (data) {
        //console.log(data);        
        this.after_login = true;
        jQuery('#SignIn').modal('hide');
        if (this.encrypt_decrypt.encryptionMode == "encrypt") {
          this.userAuthInfo.access = data.access;
          this.key = CryptoJS.enc.Utf8.parse(data.access.split('.')[1].substring(0, 16));
          data.project_code = this.encrypt_decrypt.projectCode;
          this.userName = data.user.user_name;
          this.userAuthInfo.currentUser = this.encrypt(data, this.key);
        }
        else {
          data.project_code = this.encrypt_decrypt.projectCode;
          this.userAuthInfo.currentUser = data;
          this.userName = this.userAuthInfo.currentUser.user.user_name;
        }
        if (this.userAuthInfo.currentUser.user != undefined) {
          this.group_id = this.userAuthInfo.currentUser.user.group_id;
        }
        this.userInfo = this.userAuthInfo.currentUser;
        if (this.showFavPage == true) {
          if (this.group_id == 3) {
            let switch_user = { switch_user_in: "buyer" }
            this.progress.show();
            this.httpServices.request('post', 'user/switch-user', null, null, switch_user).subscribe((data) => {
              this.userInfo.user.group_id = 2;
              if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
                this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
                localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
              }
              else {
                localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
              }
              this.group_id = 2;
              this.showFavPage = false;
              this.encrypt_decrypt.changeDataTagFav({
                is_login: true, is_Favourite: this.is_favourite
                , gig_Data: this.gig_Data
              });
              this.onSearchClick();
            }, (error) => {
              this.progress.hide();
            });
          }
          else if (this.group_id == 2) {
            this.showFavPage = false;
            this.encrypt_decrypt.changeDataTagFav({
              is_login: true, is_Favourite: this.is_favourite
              , gig_Data: this.gig_Data
            });
            this.onSearchClick();
          }
        }
        else if (this.showCartPage == true) {
          if (this.group_id == 3) {
            let switch_user = { switch_user_in: "buyer" }
            this.progress.show();
            this.httpServices.request('post', 'user/switch-user', null, null, switch_user).subscribe((data) => {
              this.userInfo.user.group_id = 2;
              if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
                this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
                localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
              }
              else {
                localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
              }
              this.group_id = 2;
              this.showCartPage = false;
              this.encrypt_decrypt.changeDataTagCartLogin({
                is_login: true
              });
              //this.onSearchClick();
            }, (error) => {
              this.progress.hide();
            });
          }
          else if (this.group_id == 2) {
            this.showCartPage = false;
            this.encrypt_decrypt.changeDataTagCartLogin({
              is_login: true
            });
            //this.onSearchClick();
          }
        }
        else if (this.userAuthInfo.currentUser.user.group_id == 1) {
          if (window.location.pathname == '/start_page') {
            this.router.navigate(['/admin_dashboard'], {});
            //setTimeout(() => { this.load_owl_carousel.activePlugin(); }, 600)

            //this.loadScripts();
          }
        }
        else if (this.userAuthInfo.currentUser.user.group_id == 2) {
          if (window.location.pathname == '/start_page') {
            this.router.navigate(['/home'], {});
          }
          else if (window.location.pathname == '/cart') {
            this.encrypt_decrypt.changeCart_groupId({
              is_login: true,
              groupId: this.userAuthInfo.currentUser.user.group_id,
              servicePercent: this.userAuthInfo.currentUser.sys_config.order_service_charge
            });
          }
        }
        else if (this.userAuthInfo.currentUser.user.group_id == 3) {
          if (window.location.pathname == '/start_page') {
            this.router.navigate(['/seller_dashboard'], {});
          }
          else if (window.location.pathname == '/cart') {
            this.encrypt_decrypt.changeCart_groupId({
              is_login: true,
              groupId: this.userAuthInfo.currentUser.user.group_id,
              servicePercent: this.userAuthInfo.currentUser.sys_config.order_service_charge
            });
          }
        }
        else {
          this.router.navigate(['/home'], {});
        }
      }
      else {
        jQuery('#SignIn').modal('hide');
      }
      this.progress.hide();
    }, (error) => {
      this.progress.hide();
    });
  }

  // =======================================================
  // Register click event 
  // =======================================================
  register_click() {
    if (document.getElementById("mySidenav_mobile") != undefined) {
      document.getElementById("mySidenav_mobile").style.width = "0";
      jQuery('body').removeClass('nav-open');
    }
    //jQuery('#SignIn').modal({ backdrop: 'static', keyboard: false });
    jQuery('#Register').modal({ backdrop: 'static', keyboard: false });
    jQuery('#SignIn').modal('hide');
    jQuery('#Verify_OTP_modal_pop_modal').modal('hide');
    jQuery('#after_register_modal_pop_modal').modal('hide');
    jQuery('#after_register_modal_pop_modal').modal('hide');

    this.emailAddressForRegister = "";
    this.showTick = null;
  }

  // =======================================================
  // Sign click event 
  // =======================================================
  signIN_click() {
    if (document.getElementById("mySidenav_mobile") != undefined) {
      document.getElementById("mySidenav_mobile").style.width = "0";
      jQuery('body').removeClass('nav-open');
    }
    this.checkvalidation = false;
    this.signIN_email = '';
    this.signIN_password = '';
    jQuery('#SignIn').modal({ backdrop: 'static', keyboard: false });
    document.getElementById('Forgot_Password_div').style.display = 'none';
    document.getElementById('SignIn_div').style.display = 'block';
    jQuery('#Register').modal('hide');
  }
  public showFavPage: boolean = false;
  public gig_Data: any;
  public is_favourite: boolean = false;
  showSignIN(event: any) {
    // if (event == true) {
    //   this.checkvalidation = false;
    //   jQuery('#SignIn').modal({});
    //   document.getElementById('Forgot_Password_div').style.display = 'none';
    //   document.getElementById('SignIn_div').style.display = 'block';
    //   jQuery('#Register').modal('hide');
    //   this.encrypt_decrypt.changeData(false);
    // }
    if (event.flag == true) {
      this.checkvalidation = false;
      this.showFavPage = (event.location == "tagFav" ? true : false);
      this.gig_Data = event.gig_Data;
      this.is_favourite = event.is_Favourite;
      jQuery('#SignIn').modal({ backdrop: 'static', keyboard: false });
      document.getElementById('Forgot_Password_div').style.display = 'none';
      document.getElementById('SignIn_div').style.display = 'block';
      jQuery('#Register').modal('hide');
      this.encrypt_decrypt.changeDataFavourite({ flag: false, location: "", is_Favourite: false, gig_Data: null });
    }
  }
  public showCartPage: boolean = false;
  showSignINOncart(event: any) {
    if (event.flag == true) {
      this.checkvalidation = false;
      this.showCartPage = true;
      jQuery('#SignIn').modal({ backdrop: 'static', keyboard: false });
      document.getElementById('Forgot_Password_div').style.display = 'none';
      document.getElementById('SignIn_div').style.display = 'block';
      jQuery('#Register').modal('hide');
      this.encrypt_decrypt.changeDataCart({ flag: false });
    }
  }
  // =======================================================
  // Switch to buying or selling 
  // =======================================================
  switch_selling_buying() {
    this.userInfo = this.userAuthInfo.currentUser;
    if (document.getElementById("mySidenav_mobile") != undefined) {
      document.getElementById("mySidenav_mobile").style.width = "0";
      jQuery('body').removeClass('nav-open');
    }
    if (document.getElementById('sitch_2_buying_selling').innerHTML == 'Switch to Selling') {
      document.getElementById('sitch_2_buying_selling').innerHTML = 'Switch to Buying';
      let switch_user = { switch_user_in: "seller" }
      this.progress.show();
      this.httpServices.request('post', 'user/switch-user', null, null, switch_user).subscribe((data) => {
        this.userInfo.user.group_id = 3;
        if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
          this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
          localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
        }
        else {
          localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
        }
        this.group_id = 3;
        this.router.navigate(['/seller_dashboard'], {});
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
    else if (document.getElementById('sitch_2_buying_selling').innerHTML == 'Switch to Buying') {
      document.getElementById('sitch_2_buying_selling').innerHTML = 'Switch to Selling';
      let switch_user = { switch_user_in: "buyer" }
      this.progress.show();
      this.httpServices.request('post', 'user/switch-user', null, null, switch_user).subscribe((data) => {
        this.userInfo.user.group_id = 2;
        if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
          this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
          localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
        }
        else {
          localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
        }
        this.group_id = 2;
        this.router.navigate(['/buyer_dashboard'], {});
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
  }

  switch_selling(event: any) {
    if (event.flag == true) {
      this.userInfo = this.userAuthInfo.currentUser;
      document.getElementById('sitch_2_buying_selling').innerHTML = 'Switch to Buying';
      let switch_user = { switch_user_in: "seller" }
      this.progress.show();
      this.httpServices.request('post', 'user/switch-user', null, null, switch_user).subscribe((data) => {
        this.userInfo.user.group_id = 3;
        if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
          this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
          localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
        }
        else {
          localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
        }
        this.group_id = 3;
        if (event.location == 'AllDone') {
          this.router.navigate(['seller_dashboard'], {});
        }
        else {
          this.router.navigate(['/new_gig'], {});
        }

        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
      this.encrypt_decrypt.changeDataSwitchSelling({ flag: false, location: "" });
    }
  }

  switch_buying(event: any) {
    if (event.flag == true) {
      this.userInfo = this.userAuthInfo.currentUser;
      document.getElementById('sitch_2_buying_selling').innerHTML = 'Switch to Selling';
      let switch_user = { switch_user_in: "buyer" }
      this.progress.show();
      this.httpServices.request('post', 'user/switch-user', null, null, switch_user).subscribe((data) => {
        this.userInfo.user.group_id = 2;
        if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
          this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
          localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
        }
        else {
          localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
        }
        this.group_id = 2;
        this.router.navigate(['/new_gig'], {});
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
      this.encrypt_decrypt.changeDataSwitchSelling({ flag: false, location: "" });
    }
  }

  // =======================================================
  // On Click of forgot password link of 'Sign in to TASKIT' popup 
  // =======================================================
  forgot_password_click() {
    jQuery('#SignIn').modal({ backdrop: 'static', keyboard: false });
    this.emailAddress_forgotPasswd = '';
    document.getElementById('Forgot_Password_div').style.display = 'block';
    document.getElementById('SignIn_div').style.display = 'none';
    jQuery('#Register').modal('hide');
  }

  // =======================================================
  // Click on Logo 
  // =======================================================
  clickOnLogo() {
    if (this.userAuthInfo.currentUser.user == undefined) {
      this.router.navigate(['/start_page'], {});
    }
    else {
      this.router.navigate(['/home'], {});
    }
  }

  // ===============================================
  // On Click of Get OTP on 'Forgot pAsswd' popup 
  // ===============================================
  getOTP_ForgotPasswd() {
    this.progress.show();
    this.checkValidation_forgotPasswd = true;
    if (this.emailAddress_forgotPasswd != null && this.emailAddress_forgotPasswd != undefined && this.emailAddress_forgotPasswd != '') {
      let login_info = { email_address: this.emailAddress_forgotPasswd }
      this.httpServices.request('post', 'password/forgot-generate-otp', null, null, login_info).subscribe((data) => {
        jQuery('#Verify_OTP_modal_pop_modal').modal({ backdrop: 'static', keyboard: false });
        this.progress.hide();
        this.checkValidation_forgotPasswd = false;
      }, (error) => {
        this.progress.hide();
        this.checkValidation_forgotPasswd = false;
      });
    }
    else {
      this.progress.hide();
    }
  }

  // ===============================================
  // On Click of verify OTP on 'verify OTP' popup 
  // ===============================================
  verifyOTP_forgotPasswd() {
    this.progress.show();
    this.checkValidation_verifyOTP = true;
    if (this.otp_forgotPasswdOTP != null && this.otp_forgotPasswdOTP != undefined && this.otp_forgotPasswdOTP != ''
      && this.password_forgotPasswdOTP != null && this.password_forgotPasswdOTP != undefined && this.password_forgotPasswdOTP != '') {
      let login_info = { otp: this.otp_forgotPasswdOTP, password: this.password_forgotPasswdOTP }
      this.httpServices.request('post', 'password/forgot-verify-otp', null, null, login_info).subscribe((data) => {
        jQuery('#Verify_OTP_modal_pop_modal').modal('hide');
        jQuery('#SignIn').modal('hide');
        toastr.success("Your password has been changed successfully.");
        this.router.navigate(['/start_page'], {});
        this.signIN_click();
        this.progress.hide();
        this.checkValidation_verifyOTP = false;
      }, (error) => {
        this.progress.hide();
        this.checkValidation_verifyOTP = false;
      });
    }
    else {
      this.progress.hide();
    }
  }

  // ============================================
  // on Click of Register btn on Register popup 
  // ===========================================
  register_with_email_click() {
    // if (this.showTick == false) {
    // this.progress.show();
    this.checkValidation_continue_register = true;
    if (this.emailAddressForRegister != null && this.emailAddressForRegister != undefined && this.emailAddressForRegister != '') {
      let login_info = { email_address: this.emailAddressForRegister }
      this.httpServices.request('post', 'auth/sign-up', null, null, login_info).subscribe((data) => {
        this.userIdAfterRegister = data.user_id;

        this.userName_addyourdetails = "";
        this.password_addyourdetails = "";
        this.confirmPassword_addyourdetails = "";
        jQuery('#after_register_modal_pop_modal').modal({ backdrop: 'static', keyboard: false });
        jQuery('#Register').modal('hide');
        this.progress.hide();
        this.checkValidation_continue_register = false;
      }, (error) => {
        this.checkValidation_continue_register = false;
        this.progress.hide();
      });
    }
    else {
      this.progress.hide();
    }
    //}
  }

  // ========================================
  // On Submit click of Add your details 
  // ======================================
  onSubmit_addyourdetails() {
    this.progress.show();
    this.checkValidation_addyourdetails = true;
    if (this.userName_addyourdetails != null && this.userName_addyourdetails != undefined && this.userName_addyourdetails != ''
      && this.password_addyourdetails != null && this.password_addyourdetails != undefined && this.password_addyourdetails != ''
      && this.confirmPassword_addyourdetails != null && this.confirmPassword_addyourdetails != undefined && this.confirmPassword_addyourdetails != '') {

      if (this.password_addyourdetails == this.confirmPassword_addyourdetails) {
        if (this.addDetailsCheck == true) {
          let login_info = { user_name: this.userName_addyourdetails, password: this.confirmPassword_addyourdetails }
          this.httpServices.request('patch', 'auth/sign-up-register/' + this.userIdAfterRegister, null, null, login_info).subscribe((data) => {
            jQuery('#after_register_modal_pop_modal').modal('hide');
            this.checkValidation_addyourdetails = false;
            let login_info = { email_address: this.emailAddressForRegister, password: this.confirmPassword_addyourdetails };
            this.httpServices.request('post', 'auth/login', null, null, login_info).subscribe((data) => {
              if (data) {
                //console.log(data);
                if (this.encrypt_decrypt.encryptionMode == "encrypt") {
                  this.userAuthInfo.access = data.access;
                  this.key = CryptoJS.enc.Utf8.parse(data.access.split('.')[1].substring(0, 16));
                  data.project_code = this.encrypt_decrypt.projectCode;
                  this.userAuthInfo.currentUser = this.encrypt(data, this.key);
                  this.userName = this.userAuthInfo.currentUser.user.user_name;
                }
                else {
                  data.project_code = this.encrypt_decrypt.projectCode;
                  this.userAuthInfo.currentUser = data;
                  this.userName = this.userAuthInfo.currentUser.user.user_name;
                }
                if (this.userAuthInfo.currentUser.user != undefined) {
                  this.group_id = this.userAuthInfo.currentUser.user.group_id;
                }
                if (this.userAuthInfo.currentUser.user.group_id == 1) {
                  this.router.navigate(['/admin_dashboard'], {});
                }
                else if (this.userAuthInfo.currentUser.user.group_id == 2) {
                  this.router.navigate(['/home'], {});
                }
                else if (this.userAuthInfo.currentUser.user.group_id == 3) {
                  this.router.navigate(['/seller_dashboard'], {});
                }
                else {
                  this.router.navigate(['/home'], {});
                }
                this.after_login = true;
                jQuery('#SignIn').modal('hide');
              }
              else {
                jQuery('#SignIn').modal('hide');
              }
              this.progress.hide();
            }, (error) => {
              this.progress.hide();
            });
          }, (error) => {
            this.progress.hide();
            this.checkValidation_addyourdetails = false;
          });
        }
        else {
          this.progress.hide();
          toastr.error("Please Select Terms of Service");
        }
      }
      else {
        this.progress.hide();
        toastr.error("Password and Confirm Password must be same");
      }
    }
    else {
      this.progress.hide();
    }
  }

  public checkvalidation: boolean = false;

  // =======================================================
  // On Click of continue btn of 'Sign in to TASKIT' popup 
  // =======================================================
  login(user: any, form: NgForm) {
    this.checkvalidation = true;
    let login_info = { email_address: user.email_address, password: user.password };
    if (form.valid) {
      this.progress.show();
      this.httpServices.request('post', 'auth/login', null, null, login_info).subscribe((data) => {
        if (data) {
          //console.log(data);
          if (this.encrypt_decrypt.encryptionMode == "encrypt") {
            this.userAuthInfo.access = data.access;
            this.key = CryptoJS.enc.Utf8.parse(data.access.split('.')[1].substring(0, 16));
            data.project_code = this.encrypt_decrypt.projectCode;
            this.userName = data.user.user_name;
            this.userAuthInfo.currentUser = this.encrypt(data, this.key);
          }
          else {
            data.project_code = this.encrypt_decrypt.projectCode;
            this.userAuthInfo.currentUser = data;
            this.userName = this.userAuthInfo.currentUser.user.user_name;
          }
          if (this.userAuthInfo.currentUser.user != undefined) {
            this.group_id = this.userAuthInfo.currentUser.user.group_id;
          }
          this.userInfo = this.userAuthInfo.currentUser;
          if (this.showFavPage == true) {
            if (this.group_id == 3) {
              let switch_user = { switch_user_in: "buyer" }
              this.progress.show();
              this.httpServices.request('post', 'user/switch-user', null, null, switch_user).subscribe((data) => {
                this.userInfo.user.group_id = 2;
                if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
                  this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
                  localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
                }
                else {
                  localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
                }
                this.group_id = 2;
                this.showFavPage = false;
                this.encrypt_decrypt.changeDataTagFav({
                  is_login: true, is_Favourite: this.is_favourite
                  , gig_Data: this.gig_Data
                });
                this.onSearchClick();
              }, (error) => {
                this.progress.hide();
              });
            }
            else if (this.group_id == 2) {
              this.showFavPage = false;
              this.encrypt_decrypt.changeDataTagFav({
                is_login: true, is_Favourite: this.is_favourite
                , gig_Data: this.gig_Data
              });
              this.onSearchClick();
            }
          }
          else if (this.showCartPage == true) {
            if (this.group_id == 3) {
              let switch_user = { switch_user_in: "buyer" }
              this.progress.show();
              this.httpServices.request('post', 'user/switch-user', null, null, switch_user).subscribe((data) => {
                this.userInfo.user.group_id = 2;
                if (this.encrypt_decrypt.encryptionMode == 'encrypt') {
                  this.key = CryptoJS.enc.Utf8.parse(this.userAuthInfo.access.split('.')[1].substring(0, 16));
                  localStorage.setItem("currentUser", JSON.stringify(this.encrypt(this.userInfo, this.key)));
                }
                else {
                  localStorage.setItem("currentUser", JSON.stringify(this.userInfo));
                }
                this.group_id = 2;
                this.showCartPage = false;
                this.encrypt_decrypt.changeDataTagCartLogin({
                  is_login: true
                });
                //this.onSearchClick();
              }, (error) => {
                this.progress.hide();
              });
            }
            else if (this.group_id == 2) {
              this.showCartPage = false;
              this.encrypt_decrypt.changeDataTagCartLogin({
                is_login: true
              });
              //this.onSearchClick();
            }
          }
          else if (this.userAuthInfo.currentUser.user.group_id == 1) {
            if (window.location.pathname == '/start_page') {
              this.router.navigate(['/admin_dashboard'], {});
              //setTimeout(() => { this.load_owl_carousel.activePlugin(); }, 600)

              //this.loadScripts();
            }
          }
          else if (this.userAuthInfo.currentUser.user.group_id == 2) {
            if (window.location.pathname == '/start_page') {
              this.router.navigate(['/home'], {});
            }
            else if (window.location.pathname == '/cart') {
              this.encrypt_decrypt.changeCart_groupId({
                is_login: true,
                groupId: this.userAuthInfo.currentUser.user.group_id,
                servicePercent: this.userAuthInfo.currentUser.sys_config.order_service_charge
              });
            }
          }
          else if (this.userAuthInfo.currentUser.user.group_id == 3) {
            if (window.location.pathname == '/start_page') {
              this.router.navigate(['/seller_dashboard'], {});
            }
            else if (window.location.pathname == '/cart') {
              this.encrypt_decrypt.changeCart_groupId({
                is_login: true,
                groupId: this.userAuthInfo.currentUser.user.group_id,
                servicePercent: this.userAuthInfo.currentUser.sys_config.order_service_charge
              });
            }
          }
          else {
            this.router.navigate(['/home'], {});
          }
          this.after_login = true;
          jQuery('#SignIn').modal('hide');
        }
        else {
          jQuery('#SignIn').modal('hide');
        }
        this.progress.hide();
      }, (error) => {
        this.progress.hide();
      });
    }
  }

  // =====================================
  // On user menu click 
  // ======================================
  onUserMenuClick(menuName: string) {
    if (document.getElementById("mySidenav_mobile") != undefined) {
      document.getElementById("mySidenav_mobile").style.width = "0";
      jQuery('body').removeClass('nav-open');
    }
    if (menuName == 'become_a_seller') {
      this.router.navigate(['/become_seller'], {});
    }
    else if (menuName == 'log_out') {
      this.progress.show();
      setTimeout(() => { }, 600);
      let user: any = { user_id: this.userAuthInfo.currentUser.user.id }
      this.httpServices.request("post", "auth/log-out", null, null, user).subscribe((data) => {
        this.after_login = false;
        this.hideMenu = true;
        localStorage.removeItem("currentUser");
        localStorage.removeItem("userAccess");
        localStorage.removeItem("Token");
        sessionStorage.removeItem("queryCode")
        sessionStorage.removeItem("queryCategory_idParam")
        sessionStorage.removeItem("searchQueryParams")
        sessionStorage.removeItem("queryParamsRequest")
        sessionStorage.removeItem("queryParams")
        sessionStorage.removeItem("listPageState")
        sessionStorage.removeItem("queryParamsMetadata")
        sessionStorage.removeItem("buyer_profile")
        sessionStorage.removeItem("seller_profile")
        sessionStorage.removeItem("queryParamsFromOrderDetails")
        sessionStorage.removeItem("queryParamsCart")
        sessionStorage.removeItem("queryCategory_idParam")
        sessionStorage.removeItem("searchQueryParams")
        sessionStorage.removeItem("queryCategory_idParam")

        if (this.group_id == 1) {
          this.group_id = 0;
          setTimeout(() => { window.location.href = "/log_out"; }, 600)
        }
        else {
          this.group_id = 0;
          this.router.navigate(['/log_out'], {});
        }
        this.progress.hide();
      }, (error) => {
        this.after_login = false;
        this.hideMenu = true;
        this.group_id = 0;
        localStorage.removeItem("currentUser");
        localStorage.removeItem("userAccess");
        localStorage.removeItem("Token");
        setTimeout(() => { this.router.navigate(['/log_out'], {}) }, 600)
        this.progress.hide();
      });
    }
    else if (menuName == 'my_profile') {
      if (this.group_id == 2) {
        this.encrypt_decrypt.buyer_profile_id = this.userAuthInfo.currentUser.user.id;
        this.encrypt_decrypt.buyer_profile_mode = 'self';
        // this.encrypt_decrypt.changeBuyerProfileData({ flag: true, location: "my_profile", 
        //     seller_profile_id:this.userAuthInfo.currentUser.user.id, seller_profile_mode: 'self'});
        this.router.navigate(['/buyer_profile'], {});
      }
      else if (this.group_id == 3) {
        this.encrypt_decrypt.seller_profile_id = this.userAuthInfo.currentUser.user.id;
        this.encrypt_decrypt.seller_profile_mode = 'self';
        // this.encrypt_decrypt.changeSellerProfileData({ flag: true, location: "my_profile", 
        //     seller_profile_id:this.userAuthInfo.currentUser.user.id, seller_profile_mode: 'self'});
        this.router.navigate(['/seller_profile'], {});
      }
    }
    else if (menuName == 'my_dashboard') {
      if (this.group_id == 2) {
        this.router.navigate(['/buyer_dashboard'], {});
      }
      else if (this.group_id == 3) {
        this.router.navigate(['/seller_dashboard'], {});
      }
    }
    else if (menuName == 'manage_request') {
      this.router.navigate(['/search_result'], {});
    }
    else if (menuName == 'orders') {
      if (this.group_id == 2) {
        this.router.navigate(['/buyer_orders'], {});
      }
      else if (this.group_id == 3) {
        this.router.navigate(['/seller_orders'], {});
      }
    }
    else if (menuName == 'buyer_request') {
      this.router.navigate(['/seller_request'], {});
    }
    else if (menuName == 'post_a_request') {
      sessionStorage.removeItem("post_request_queryParams");
      this.router.navigate(['/post_a_request'], {});
    }
    else if (menuName == 'gigs_admin') {
      this.router.navigate(['/gig_list_admin'], {});
    }
    else if (menuName == 'favorite') {
      this.router.navigate(['/favorite'], {});
    }
    else if (menuName == 'gig_list') {
      this.router.navigate(['/gig_list'], {});
    }
    else if (menuName == 'earnings') {
      this.router.navigate(['/earnings'], {});
    }
  }

  ValidateEmail(inputText: any, id: any) {
    if (inputText == '') {
      return true;
    }
    var mailformat = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    if (inputText.match(mailformat)) {
      return true;
    }
    else {
      toastr.error("You have entered an invalid email address!");
      document.getElementById(id).focus();
      return false;
    }
  }

  ValidatePassword(inputText: any, id: any) {
    if (inputText == '') {
      return true;
    }
    var passwordformat = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;
    if (inputText.match(passwordformat)) {
      return true;
    }
    else {
      toastr.error("Minimum eight characters, at least one letter, one number and one special character!");
      document.getElementById(id).focus();
      return false;
    }
  }


  // =======================================================
  // Verify Registered Email Address
  // =======================================================
  VerifiedRegisterEmail() {
    // this.showSpinner = true;
    if (this.emailAddressForRegister == '') {
      // this.showSpinner = false;
      toastr.error("Please enter email address.");
      return true;
    }
    var mailformat = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    this.progress.show();
    if (this.emailAddressForRegister.match(mailformat)) {
      let email_address = { email_address: this.emailAddressForRegister }
      this.httpServices.request('post', 'user/existed-email-verify', '', '', email_address).subscribe((data) => {
        this.isEmailverified = data;
        // this.showSpinner = false;
        if (data.is_email_exist == true) {
          this.showTick = true;
          this.progress.hide();
          // this.showSpinner = true;
          //toastr.warning("Email already Exist");
        } else {
          // this.showTick = true;
          this.register_with_email_click();
        }
        //toastr.success("Email Verified");
      }, error => {
        this.progress.hide();
        //console.log(error);
      });
      return true;
    }
    else {
      toastr.error("You have entered an invalid email address!");
      this.progress.hide();
      // document.getElementById(id).focus();
      this.showTick = false;
      return false;
    }
  }


  encrypt(msgString, key) {
    // msgString is expected to be Utf8 encoded
    var iv = CryptoJS.lib.WordArray.random(16);
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify(msgString), key, {
      iv: iv
    });
    return iv.concat(encrypted.ciphertext).toString(CryptoJS.enc.Base64);
  }

  decrypt(ciphertextStr, key) {
    var ciphertext = CryptoJS.enc.Base64.parse(ciphertextStr);
    // split IV and ciphertext
    var iv = ciphertext.clone();
    iv.sigBytes = 16;
    iv.clamp();
    ciphertext.words.splice(0, 4); // delete 4 words = 16 bytes
    ciphertext.sigBytes -= 16;

    // decryption
    var decrypted = CryptoJS.AES.decrypt({ ciphertext: ciphertext }, key, {
      iv: iv
    });
    return decrypted.toString(CryptoJS.enc.Utf8);
  }


  // =========================================
  // Get only characters on key press 
  // ==========================================
  onlyCharacters(event: any) {
    var code = (event.which) ? event.which : event.keyCode;
    if (!(code == 32) && // space
      !(code == 39) && //apostrophe
      !(code > 64 && code < 91) && // upper alpha (A-Z)
      !(code > 96 && code < 123)) { // lower alpha (a-z)
      event.preventDefault();
    }
  }

  //==========================================================
  // Search complete extendar. 
  //==========================================================
  handleSearchTypeFilter(value: string) {
    this.autoCompListItem = undefined;
    this.searchString.search_keyword = "";
    if (value.length > 0) {
      this.searchString.search_keyword = value;
      this.httpServices.request('get', 'gigs/search-extender?search_keyword=' + value, '', '', null)
        .subscribe(data => {
          if (data.length > 0) {
            this.autoCompListItem = data;
          }
        });
    }
  }

  //==========================================================
  //  Search's auto complete on select event       
  //==========================================================
  onSpanSearchTypeSelected(dataitem: any) {
    this.searchString.search_keyword = dataitem.search_keyword;
  }

  //==========================================================
  //  search on enter click
  //==========================================================
  enterClick(event: any) {
    var charCode = (event.which) ? event.which : event.keyCode
    if (charCode == 13) {
      document.getElementById('btnSearch').click();
    }
  }

  // =======================================
  // On Search button click 
  // =======================================
  public showValidationsearch: boolean = false;
  onSearchClick() {
    if (this.searchString.search_keyword == "" || this.searchString.search_keyword == null || this.searchString.search_keyword == undefined) {
      this.showValidationsearch = true;
    }
    else {
      this.showValidationsearch = false;
      sessionStorage.setItem("searchQueryParams", JSON.stringify(this.searchString));
      this.encrypt_decrypt.changeDataGigSearch({ flag: true, location: "search_result_screen" });
      this.router.navigate(['/search_result'], {});
      this.searchString.search_keyword = "";
    }
  }

  //********************************************
  // Onsubcategory click to Search Result Page
  //********************************************
  onSubcategoryClick(sub_category_name: string) {
    let passQueryParam = { search_keyword: sub_category_name };
    sessionStorage.setItem("searchQueryParams", JSON.stringify(passQueryParam));
    this.encrypt_decrypt.changeDataGigSearch({ flag: true, location: "index_screen" });
    this.router.navigate(['/search_result']);
  }
}
