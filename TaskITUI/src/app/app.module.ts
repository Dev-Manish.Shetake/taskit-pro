import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpModule } from '@angular/http';
// Import library module
import { NgxSpinnerModule } from "ngx-spinner";

//Import Carousel Module
import { CarouselModule } from 'ngx-owl-carousel-o';

import { Ng2ImgMaxModule } from 'ng2-img-max';
// //Kendo UI
import { UploadModule } from '@progress/kendo-angular-upload';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { GridModule, ExcelModule } from '@progress/kendo-angular-grid';
// import { DialogModule, WindowModule } from '@progress/kendo-angular-dialog';
import { InputsModule } from '@progress/kendo-angular-inputs';
import { DatePickerModule, TimePickerModule } from '@progress/kendo-angular-dateinputs';

import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
// import { NumberFormatOptions } from '@progress/kendo-angular-intl';
// import { TreeViewModule } from '@progress/kendo-angular-treeview';
import { ExcelExportModule } from '@progress/kendo-angular-excel-export';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {
  GoogleLoginProvider,
  FacebookLoginProvider
} from 'angularx-social-login';

import { Load_owl_carouselService } from './_common_services/load_owl_carousel.service';
import { AppComponent } from './app.component';
import { HttpRequestService } from './_common_services/httprequest.service';
import { HttpErrorInterceptorService } from './_common_services/http-error-interceptor.service';
import { AppRoutingModule } from './app-routing.module';
import { DateAgoPipe } from './_pipes/date-ago.pipe';
import { FooterComponent } from './_common_component/footer/footer.component';
import { HeaderComponent } from './_common_component/header/header.component';
import { Start_pageComponent } from './_component/start_page/start_page.component';
import { Become_buyerComponent } from './_component/customer/buyer/become_buyer/become_buyer.component';
import { Become_sellerComponent } from './_component/customer/seller/become_seller/become_seller.component';
import { AboutComponent } from './_component/customer/about/about.component';
import { HomeComponent } from './_component/customer/home/home.component';
import { OverviewComponent } from './_component/customer/overview/overview.component';
import { Personal_infoComponent } from './_component/customer/become_seller/personal_info/personal_info.component';
import { Log_outComponent } from './_component/log_out/log_out.component';
import { New_gigComponent } from './_component/customer/seller/new_gig/new_gig.component';
import { Header_adminComponent } from './_common_component/header_admin/header_admin.component';
import { Footer_adminComponent } from './_common_component/footer_admin/footer_admin.component';
import { Dashboard_adminComponent } from './_component/admin/dashboard_admin/dashboard_admin.component';
import { Help_and_supportComponent } from './_component/customer/help_and_support/help_and_support.component';
import { Terms_and_conditionsComponent } from './_component/customer/terms_and_conditions/terms_and_conditions.component';
import { ContactComponent } from './_component/customer/contact/contact.component';
import { Buyer_profileComponent } from './_component/customer/buyer_profile/buyer_profile.component';
import { SettingsComponent } from './_component/customer/settings/settings.component';
import { Seller_requestComponent } from './_component/customer/seller_request/seller_request.component';
import { Post_a_requestComponent } from './_component/customer/post_a_request/post_a_request.component';
import { Seller_dashboardComponent } from './_component/customer/seller/seller_dashboard/seller_dashboard.component';
import { Gig_detailsComponent } from './_component/customer/gig_details/gig_details.component';
import { Search_resultComponent } from './_component/customer/search_result/search_result.component';
import { ReviewsComponent } from './_component/admin/reviews/reviews.component';
import { OrdersComponent } from './_component/customer/seller/orders/orders.component';
import { Seller_order_commonComponent } from './_common_component/seller_order_common/seller_order_common.component';
import { Buyer_order_commonComponent } from './_common_component/buyer_order_common/buyer_order_common.component';
import { Buyer_ordersComponent } from './_component/customer/buyer/buyer_orders/buyer_orders.component';
import { Cart_pageComponent } from './_component/customer/cart_page/cart_page.component';
import { Settings_adminComponent } from './_component/admin/settings_admin/settings_admin.component';
import { Gig_listComponent } from './_component/customer/gig_list/gig_list.component';
import { Buyer_dashboardComponent } from './_component/customer/buyer/buyer_dashboard/buyer_dashboard.component';
import {Gig_list_adminComponent} from './_component/admin/gig_list_admin/gig_list_admin.component';
import { GetListPageDataService } from './_common_services/previousdata-list.service';
import { FavoriteComponent } from './_component/customer/favorite/favorite.component';
import { Seller_profileComponent } from './_component/customer/seller_profile/seller_profile.component';
import { Users_listComponent } from './_component/admin/users_list/users_list.component';
import { Gigs_categoryComponent } from './_component/admin/gigs_category/gigs_category.component';
import { Gigs_SubcategoryComponent } from './_component/admin/gigs_Subcategory/gigs_Subcategory.component';
import { Total_OrdersComponent } from './_component/admin/orders/total_Orders/total_Orders.component';
import { Multi_autocompleteComponent } from 'src/app/_common_component/multi_autocomplete/multi_autocomplete.component';
import { Completed_OrdersComponent } from './_component/admin/orders/completed_Orders/completed_Orders.component';
import { Pending_OrdersComponent } from './_component/admin/orders/pending_Orders/pending_Orders.component';
import { Cancelled_OrdersComponent } from './_component/admin/orders/cancelled_Orders/cancelled_Orders.component';
import { Declined_OrdersComponent } from './_component/admin/orders/declined_Orders/declined_Orders.component';
import { Dispute_OrdersComponent } from './_component/admin/orders/dispute_Orders/dispute_Orders.component';
import { ProfessionComponent } from './_component/admin/profession/profession.component';
import { Incoming_withdrawalComponent } from './_component/admin/withdrawal/incoming_withdrawal/incoming_withdrawal.component';
import { Paid_withdrawalComponent } from './_component/admin/withdrawal/paid_withdrawal/paid_withdrawal.component';
import { VerifyComponent } from './_component/verify/verify.component';
import { NgxAudioPlayerModule } from 'ngx-audio-player';
import { VgCoreModule } from 'ngx-videogular';
import { SubCategories_screenComponent } from './_component/customer/subCategories_screen/subCategories_screen.component';
import { Metadata_listComponent } from './_component/admin/metadata_list/metadata_list.component';
import { Admin_Post_a_requestComponent } from './_component/admin/admin_Post_a_request/admin_Post_a_request.component';
import { Buyer_manage_requestComponent } from './_component/customer/buyer_manage_request/buyer_manage_request.component';
import { Buyer_manage_request_commonComponent } from './_common_component/buyer_manage_request_common/buyer_manage_request_common.component';
import { Metadata_AddComponent } from './_component/admin/metadata_Add/metadata_Add.component';
import {Order_detailsComponent} from './_component/customer/order_details/order_details.component';
import { Edit_Post_a_requestComponent } from './_component/admin/edit_Post_a_request/edit_Post_a_request.component';
import { Gig_offersComponent } from './_component/customer/gig_offers/gig_offers.component';
import { Price_scope_listComponent } from './_component/admin/price_scope_list/price_scope_list.component';
import { Add_price_scope_listComponent } from './_component/admin/add_price_scope_list/add_price_scope_list.component';
import { PaymentComponent } from './_component/customer/payment/payment.component';
import {Seller_offersComponent} from './_component/customer/seller/seller_offers/seller_offers.component';
import { AnalyticsComponent } from './_component/customer/analytics/analytics.component';
import { EarningsComponent } from './_component/customer/earnings/earnings.component';
import { PrivacyPolicyComponent } from './_component/customer/privacy-policy/privacy-policy.component';
import { RefundComponent } from './_component/admin/refund/refund.component';

@NgModule({
  declarations: [
    AppComponent, DateAgoPipe, FooterComponent, HeaderComponent, Start_pageComponent, Become_buyerComponent, Become_sellerComponent,
    AboutComponent, HomeComponent, OverviewComponent, Personal_infoComponent, Log_outComponent, New_gigComponent, Header_adminComponent,
    Footer_adminComponent, Dashboard_adminComponent, AboutComponent, HomeComponent, OverviewComponent, Personal_infoComponent,
    Log_outComponent, New_gigComponent, Help_and_supportComponent, Terms_and_conditionsComponent, ContactComponent,
    Buyer_profileComponent, SettingsComponent, Seller_requestComponent, Post_a_requestComponent, Seller_dashboardComponent,
    Gig_detailsComponent, Search_resultComponent, ReviewsComponent, OrdersComponent, Seller_order_commonComponent, Buyer_order_commonComponent, Buyer_ordersComponent,
    Cart_pageComponent, Settings_adminComponent, Gig_listComponent, Buyer_dashboardComponent,Gig_list_adminComponent,
    FavoriteComponent,Multi_autocompleteComponent, Seller_profileComponent, Users_listComponent,Gigs_categoryComponent,Gigs_SubcategoryComponent,
    Total_OrdersComponent,Completed_OrdersComponent, Pending_OrdersComponent, Cancelled_OrdersComponent, Declined_OrdersComponent, Dispute_OrdersComponent,
    ProfessionComponent,Incoming_withdrawalComponent,Paid_withdrawalComponent, VerifyComponent, SubCategories_screenComponent, Metadata_listComponent,
    Admin_Post_a_requestComponent, Buyer_manage_requestComponent, Buyer_manage_request_commonComponent, Metadata_AddComponent,
     Order_detailsComponent, Edit_Post_a_requestComponent, Gig_offersComponent, Price_scope_listComponent, Add_price_scope_listComponent,
     PaymentComponent,Seller_offersComponent, AnalyticsComponent, EarningsComponent, PrivacyPolicyComponent, RefundComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    FormsModule,
    HttpClientModule,
    ButtonsModule,
    GridModule,
    UploadModule,
    ExcelModule,
    InputsModule,
    DatePickerModule,
    TimePickerModule,
    DropDownsModule,
    ExcelExportModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    Ng2ImgMaxModule,
    NgxSpinnerModule,
    SocialLoginModule,
    CarouselModule,
    NgxAudioPlayerModule,
    VgCoreModule    
  ],
  providers: [HttpRequestService, Load_owl_carouselService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptorService,
      multi: true,
    },
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              // '112720709682-o9l2t2pd0mtj3cjok6al0t9u0bbbb6ts.apps.googleusercontent.com' //http://localhost:4200
              '96385295421-lfabgr3qqc0bskd004caa1g7jfgqubem.apps.googleusercontent.com' //https://bcspl360.xyz
            )
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('474501760389239')
          }
        ]
      } as SocialAuthServiceConfig,
    },
    GetListPageDataService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
