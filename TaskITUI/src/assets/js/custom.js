(function ($) {
  "use strict";

  var review = $('.player_info_item');
  if (review.length) {
    review.owlCarousel({
      items: 1,
      loop: true,
      dots: false,
      autoplay: true,
      margin: 40,
      autoplayHoverPause: true,
      autoplayTimeout: 5000,
      nav: true,
      navText: [
        '<img src="img/icon/left.svg" alt="">',
        '<img src="img/icon/right.svg" alt="">'

      ],
      responsive: {
        0: {
          margin: 15,
        },
        600: {
          margin: 10,
        },
        1000: {
          margin: 10,
        }
      }
    });
  }
  if (document.getElementById('default-select')) {
    $('select').niceSelect();
  }
  $('.popup-youtube, .popup-vimeo').magnificPopup({
    // disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false
  });


  $(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('#resizing_select').change(function () {
      $("#width_tmp_option").html($('#resizing_select option:selected').text());
      $(this).width($("#width_tmp_select").width());
    });


    $("#imageUpload_custom").change(function (data) {
      var imageFile = data.target.files[0];
      var reader = new FileReader();
      reader.readAsDataURL(imageFile);

      reader.onload = function (evt) {
        $('#imagePreview').attr('src', evt.target.result);
        $('#imagePreview').hide();
        $('#imagePreview').fadeIn(650);
      }
    });

  });


  // menu fixed js code
  $(window).scroll(function () {
    var window_top = $(window).scrollTop() + 1;
    if (window_top > 50) {
      $('.main_menu, .categories').addClass('menu_fixed');
      // $('.categories').removeClass('d-none');
    } else {
      $('.main_menu, .categories').removeClass('menu_fixed');
      // $('.categories').addClass('d-none');
    }
  });

  $('.Best_Seller_section.owl-carousel').owlCarousel({
    loop: true,
    items: 6,
    navigation: false,
    autoplay: false,
    autoplayTimeout: 5000,
    margin: 20,
    autoplayHoverPause: true,
    dots: false,
    responsive: {
      0: {
        items: 1,
      },
      768: {
        items: 3,
      },
      991: {
        items: 4,
      },
    }
  });
  $('.customers-testimonials').owlCarousel({
    loop: true,
    center: true,
    items: 3,
    margin: 0,
    autoplay: true,
    dots: true,
    autoplayTimeout: 8500,
    smartSpeed: 450,
    responsive: {
      0: {
        items: 1
      },
      768: {
        items: 2
      },
      1170: {
        items: 4
      }
    }
  });

  // $('.slider').slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   arrows: false,
  //   speed: 300,
  //   infinite: true,
  //   asNavFor: '.slider-nav-thumbnails',
  //   autoplay:true,
  //   pauseOnFocus: true,
  //   dots: true,
  // });

  // $('.slider-nav-thumbnails').slick({
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   asNavFor: '.slider',
  //   focusOnSelect: true,
  //   infinite: true,
  //   prevArrow: false,
  //   nextArrow: false,
  //   centerMode: true,
  //   responsive: [
  //     {
  //       breakpoint: 480,
  //       settings: {
  //         centerMode: false,
  //       }
  //     }
  //   ]
  // });

  //remove active class from all thumbnail slides
  $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');

  //set active class to first thumbnail slides
  $('.slider-nav-thumbnails .slick-slide').eq(0).addClass('slick-active');

  // On before slide change match active thumbnail to current slide
  $('.slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
    var mySlideNumber = nextSlide;
    $('.slider-nav-thumbnails .slick-slide').removeClass('slick-active');
    $('.slider-nav-thumbnails .slick-slide').eq(mySlideNumber).addClass('slick-active');
  });

  //UPDATED 

  $('.slider').on('afterChange', function (event, slick, currentSlide) {
    $('.content').hide();
    $('.content[data-id=' + (currentSlide + 1) + ']').show();
  });
}(jQuery));

// Sliding Navbar Mobile Start //
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
document.addEventListener("click", function () {
  if (parseInt($('#mySidenav').css('width')) > 0) {
    document.getElementById("mySidenav").style.width = "0";
  }
});




// -------------------------------------------------------------------



function openNav_mobile() {
  document.getElementById("mySidenav_mobile").style.width = "300px";
  $('body').addClass('nav-open');
}

function closeNav_mobile() {
  document.getElementById("mySidenav_mobile").style.width = "0";
  document.getElementById("mySidenav_mobile").style.zIndex = "9999999999";
  $('body').removeClass('nav-open');
}
function close_sidenaveBar_mobile(){
  document.getElementById("mySidenav_mobile").style.width = "0";
  $('body').removeClass('nav-open');
}

$(".sidenav_mobile a.ONclickClose").click(function(){
  document.getElementById("mySidenav_mobile").style.width = "0";
  $('body').removeClass('nav-open');
});

// Sliding Navbar Mobile End //



// For Text Length in Textarea Start

$('#characterLeft').text('140 characters left');
$('#message').keyup(function () {
  var max = 140;
  var len = $(this).val().length;
  if (len >= max) {
    $('#characterLeft').text('You have reached the limit');
    $('#characterLeft').addClass('red');
    $('#btnSubmit').addClass('disabled');
  }
  else {
    var ch = max - len;
    $('#characterLeft').text(ch + ' characters left');
    $('#btnSubmit').removeClass('disabled');
    $('#characterLeft').removeClass('red');
  }
});

$('#characterLeft1').text('600 characters left');
$('#message1').keyup(function () {
  var max1 = 600;
  var len1 = $(this).val().length;
  if (len1 >= max1) {
    $('#characterLeft1').text('You have reached the limit');
    $('#characterLeft1').addClass('red1');
    $('#btnSubmit').addClass('disabled');
  }
  else {
    var ch1 = max1 - len1;
    $('#characterLeft1').text(ch1 + ' characters left');
    $('#btnSubmit').removeClass('disabled');
    $('#characterLeft1').removeClass('red1');
  }
});
// For Text Length in Textarea End
// For Image Start
//document.getElementById('pro-image').addEventListener('change', readImage, false);

//$(".preview-images-zone").sortable();

$(document).on('click', '.image-cancel', function () {
  let no = $(this).data('no');
  $(".preview-image.preview-show-" + no).remove();
});


var num = 4;
function readImage() {
  if (window.File && window.FileList && window.FileReader) {
    var files = event.target.files; //FileList object
    var output = $(".preview-images-zone");

    for (let i = 0; i < files.length; i++) {
      var file = files[i];
      if (!file.type.match('image')) continue;

      var picReader = new FileReader();

      picReader.addEventListener('load', function (event) {
        var picFile = event.target;
        var html = '<div class="preview-image preview-show-' + num + '">' +
          '<div class="image-cancel" data-no="' + num + '">x</div>' +
          '<div class="image-zone"><img id="pro-img-' + num + '" src="' + picFile.result + '"></div>' +
          '</div>';

        output.append(html);
        num = num + 1;
      });

      picReader.readAsDataURL(file);
    }
    $("#pro-image").val('');
  } else {
    console.log('Browser not support');
  }
}
// For Image ENd














