(function ($) {
 "use strict";

	/*----------------------------
	 jQuery MeanMenu
	------------------------------ */
	jQuery('nav#dropdown').meanmenu();	
	/*----------------------------
	 jQuery myTab
	------------------------------ */
	$('#myTab a').on('click', function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		});
		$('#myTab3 a').on('click', function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		});
		$('#myTab4 a').on('click', function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		});
		$('#myTabedu1 a').on('click', function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		});

	  $('#single-product-tab a').on('click', function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		});
	
	$('[data-toggle="tooltip"]').tooltip(); 
	
	$('#sidebarCollapse').on('click', function () {
		 $('#sidebar').toggleClass('active');
	 });
	// Collapse ibox function
	$('#sidebar ul li').on('click', function () {
		var button = $(this).find('i.fa.indicator-mn');
		button.toggleClass('fa-plus').toggleClass('fa-minus');
		
	});
	/*-----------------------------
		Menu Stick
	---------------------------------*/
	$(".sicker-menu").sticky({topSpacing:0});
		
	$('#sidebarCollapse').on('click', function () {
		$("body").toggleClass("mini-navbar");
		SmoothlyMenu();
	});
	$(document).on('click', '.header-right-menu .dropdown-menu', function (e) {
		  e.stopPropagation();
	});
	/*----------------------------
	 wow js active
	------------------------------ */
	 new WOW().init();
	/*----------------------------
	 owl active
	------------------------------ */  
	$("#owl-demo").owlCarousel({
      autoPlay: false, 
	  slideSpeed:2000,
	  pagination:false,
	  navigation:true,	  
      items : 4,
	  /* transitionStyle : "fade", */    /* [This code for animation ] */
	  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
      itemsDesktop : [1199,4],
	  itemsDesktopSmall : [980,3],
	  itemsTablet: [768,2],
	  itemsMobile : [479,1],
	});
	
	/*--------------------------
	 scrollUp
	---------------------------- */	
	// $.scrollUp({
 //        scrollText: '<i class="fa fa-angle-up"></i>',
 //        easingType: 'linear',
 //        scrollSpeed: 900,
 //        animation: 'fade'
 //    });


	function toggleIcon(e) {
		jQuery(e.target).prev('.panel-heading').find(".more-less").toggleClass('fa fa-plus fa fa-minus');
		jQuery(e.target).prev('.panel-heading').find(".Clear_filter").toggleClass('accordion-filter_clear_text_add');
	}
	jQuery('.panel-group').on('hidden.bs.collapse', toggleIcon);
	jQuery('.panel-group').on('shown.bs.collapse', toggleIcon);


    $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });	   
 
})(jQuery); 

	/*--------------------------
	 Show Hide Password
	---------------------------- */	

   function showPassword(inputField,passStatus) {
    // var x = document.getElementById("loginPassword");
    // var passStatus = document.getElementById('pass-status');
    if (inputField.type === "password") {
        inputField.type = "text";
        passStatus.className = 'fa fa-eye';
    } else {
        inputField.type = "password";
        passStatus.className = 'fa fa-eye-slash';
    }

	// document.getElementById("myDropdown2").addEventListener('click', function (event) { 
	// 	event.stopPropagation(); 
	// });
}


// -------------------------- For Searchbar Mobile Start -------------------------------

var searchBar = $(".search-bar");
var searchInput = $(".search-bar input[type='search']");
var searchToggle = $(".search-toggle");

searchToggle.on("click", function (e) {
searchBar.slideToggle(0, function () {
	searchInput.focus();
});
});

// -------------------------- For Searchbar Mobile End ---------------------------------------


const reel = document.querySelector('.tab_reel');
const tab1 = document.querySelector('.tab1');
const tab2 = document.querySelector('.tab2');
const panel1 = document.querySelector('.tab_panel1');
const panel2 = document.querySelector('.tab_panel2');

function slideLeft(e) {
  tab2.classList.remove('active');
  this.classList.add('active');
  reel.style.transform = "translateX(0%)";
}

function slideRight(e) {
  tab1.classList.remove('active');
  this.classList.add('active');
  reel.style.transform = "translateX(-50%)";
}

tab1.addEventListener('click', slideLeft);
tab2.addEventListener('click', slideRight);