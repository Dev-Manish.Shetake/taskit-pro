FROM nginx

RUN rm /etc/nginx/conf.d/default.conf
COPY taskit.conf /etc/nginx/conf.d
COPY --from=taskit-angular-qa /ng-app/dist /taskit/build/
